package com.avanza.meta.view.data;

import com.avanza.core.data.DbObject;

public class SearchRoleViewFilter extends DbObject implements Comparable<SearchRoleViewFilter> {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String metaViewId;
    private String roleId;
    private int filterTypeId;
    private int priority;

    public String getMetaViewId() {
        return metaViewId;
    }

    public void setMetaViewId(String metaViewId) {
        this.metaViewId = metaViewId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public int getFilterTypeId() {
        return filterTypeId;
    }

    public void setFilterTypeId(int filterTypeId) {
        this.filterTypeId = filterTypeId;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public int compareTo(SearchRoleViewFilter o1) {
        return (this.getPriority() < o1.getPriority() ? -1 : (o1.getPriority() == this.getPriority() ? 0 : 1));
    }
}
