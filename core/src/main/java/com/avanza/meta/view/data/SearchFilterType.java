package com.avanza.meta.view.data;

import com.avanza.core.data.DbObject;

public class SearchFilterType extends DbObject {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int id;
    private String name;
    private String description;
    private boolean and;
    private boolean sameActor;
    private boolean sameRole;
    private boolean bottomHierarchy;
    private boolean sameOrgUnit;
    private boolean sameProcessParticipant;
    private boolean sameStateParticipant;
    private Integer joiningFilterTypeId;

    public boolean isSameRole() {
        return sameRole;
    }

    public void setSameRole(boolean sameRole) {
        this.sameRole = sameRole;
    }

    public boolean isBottomHierarchy() {
        return bottomHierarchy;
    }

    public void setBottomHierarchy(boolean bottomHierarchy) {
        this.bottomHierarchy = bottomHierarchy;
    }

    public boolean isSameOrgUnit() {
        return sameOrgUnit;
    }

    public void setSameOrgUnit(boolean sameOrgUnit) {
        this.sameOrgUnit = sameOrgUnit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSameProcessParticipant() {
        return sameProcessParticipant;
    }

    public void setSameProcessParticipant(boolean sameProcessParticipant) {
        this.sameProcessParticipant = sameProcessParticipant;
    }

    public boolean isSameStateParticipant() {
        return sameStateParticipant;
    }

    public void setSameStateParticipant(boolean sameStateParticipant) {
        this.sameStateParticipant = sameStateParticipant;
    }

    public boolean isAnd() {
        return and;
    }

    public void setAnd(boolean and) {
        this.and = and;
    }

    public boolean isSameActor() {
        return sameActor;
    }

    public void setSameActor(boolean sameActor) {
        this.sameActor = sameActor;
    }

    public Integer getJoiningFilterTypeId() {
        return joiningFilterTypeId;
    }

    public void setJoiningFilterTypeId(Integer joiningFilterTypeId) {
        this.joiningFilterTypeId = joiningFilterTypeId;
    }
}
