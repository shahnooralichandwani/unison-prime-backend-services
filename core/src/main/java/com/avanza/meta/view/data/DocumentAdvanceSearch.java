package com.avanza.meta.view.data;

import com.avanza.core.data.DbObject;

public class DocumentAdvanceSearch extends DbObject implements java.io.Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String DocumentAdvanceSearchId;
    private String UserId;
    private String CriteriaName;
    private String MetaEntityId;
    private boolean IsDefault;


    public String getDocumentAdvanceSearchId() {
        return DocumentAdvanceSearchId;
    }

    public void setDocumentAdvanceSearchId(String AdvanceSearchId) {
        this.DocumentAdvanceSearchId = AdvanceSearchId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public void setCriteriaName(String searchCriteriaName) {
        this.CriteriaName = searchCriteriaName;
    }

    public String getCriteriaName() {
        return CriteriaName;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.MetaEntityId = metaEntityId;
    }

    public String getMetaEntityId() {
        return MetaEntityId;
    }

    public void setIsDefault(boolean isDefault) {
        this.IsDefault = isDefault;
    }

    public boolean getIsDefault() {
        return IsDefault;
    }
}
