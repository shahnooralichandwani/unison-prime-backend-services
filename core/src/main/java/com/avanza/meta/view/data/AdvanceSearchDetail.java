package com.avanza.meta.view.data;

import com.avanza.core.data.DbObject;

public class AdvanceSearchDetail extends DbObject implements java.io.Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String AdvanceSearchDetailId;
    private String AdvanceSearchId;
    private String UserId;
    private String ViewAttributeId;
    private String EntityAttributeId;
    private String AttributeValue;
    private String AttributeToValue;
    private String AttributeOperator;
    private String CriteriaOrder;
    private String AttributeCondition;


    public String getAdvanceSearchDetailId() {
        return AdvanceSearchDetailId;
    }

    public void setAdvanceSearchDetailId(String AdvanceSearchDetailId) {
        this.AdvanceSearchDetailId = AdvanceSearchDetailId;
    }

    public String getAdvanceSearchId() {
        return AdvanceSearchId;
    }

    public void setAdvanceSearchId(String AdvanceSearchId) {
        this.AdvanceSearchId = AdvanceSearchId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getViewAttributeId() {
        return ViewAttributeId;
    }

    public void setEntityAttributeId(String EntityAttributeId) {
        this.EntityAttributeId = EntityAttributeId;
    }

    public String getEntityAttributeId() {
        return EntityAttributeId;
    }

    public void setViewAttributeId(String ViewAttributeId) {
        this.ViewAttributeId = ViewAttributeId;
    }

    public String getAttributeValue() {
        return AttributeValue;
    }

    public void setAttributeValue(String AttributeValue) {
        this.AttributeValue = AttributeValue;
    }

    public String getAttributeToValue() {
        return AttributeToValue;
    }

    public void setAttributeToValue(String AttributeToValue) {
        this.AttributeToValue = AttributeToValue;
    }

    public void setAttributeOperator(String AttributeOperator) {
        this.AttributeOperator = AttributeOperator;
    }

    public String getAttributeOperator() {
        return AttributeOperator;
    }

    public void setCriteriaOrder(String CriteriaOrder) {
        this.CriteriaOrder = CriteriaOrder;
    }

    public String getCriteriaOrder() {
        return CriteriaOrder;
    }

    public void setAttributeCondition(String AttributeCondition) {
        this.AttributeCondition = AttributeCondition;
    }

    public String getAttributeCondition() {
        return AttributeCondition;
    }
}
