package com.avanza.workflow.configuration.action;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.HibernateDataBroker;
import com.avanza.core.util.Logger;
import com.avanza.workflow.execution.Action;

/**
 * All methods will be static in this class. This class will loaded in ProcessManager
 *
 * @author hmirza
 */
public class ActionCatalog {

    private static final Logger logger = Logger.getLogger(ActionCatalog.class);
    private static Map<String, ActionDefinition> actionsMap = new Hashtable<String, ActionDefinition>(0);

    // Contains ActionHandlers Objects ( by action code )
    private static Map<String, Action> actionsHandlers = new Hashtable<String, Action>(0);

    public static void load() {
        HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker("HibernateDataBroker");
        List<ActionDefinition> actions = broker.findAll(ActionDefinition.class);
        for (ActionDefinition ad : actions) {
            String id = ad.getActionCode();
            actionsMap.put(id, ad);
            Action actionHandler = null;
            try {
                actionHandler = (Action) Class.forName(ad.getActionHandler()).newInstance();
            } catch (Exception e) {
                logger.LogException("Action Class Not Found, " + ad.getActionHandler(), e);
                continue;
            }
            actionsHandlers.put(ad.getActionCode(), actionHandler);
        }
    }

    public static void reLoad() {

    }

    public static void dispose() {
        actionsMap = new Hashtable<String, ActionDefinition>(0);
    }

    public static ActionDefinition getActionDefination(String actionCode) {

        return actionsMap.get(actionCode);
    }

    public static Action getAction(String actionCode) {

        return actionsHandlers.get(actionCode);
    }

}
