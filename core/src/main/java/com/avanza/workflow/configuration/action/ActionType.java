package com.avanza.workflow.configuration.action;


public enum ActionType {

    TaskAllocatorAction, ValidatorAction, CurrentOrgCalculator, GeneralAction;

    public static ActionType fromName(String type) {
        if (type.equalsIgnoreCase(ActionType.ValidatorAction.name())) return ActionType.ValidatorAction;
        if (type.equalsIgnoreCase(ActionType.CurrentOrgCalculator.name())) return ActionType.CurrentOrgCalculator;
        if (type.equalsIgnoreCase(ActionType.GeneralAction.name())) return ActionType.GeneralAction;
        if (type.equalsIgnoreCase(ActionType.TaskAllocatorAction.name())) return ActionType.TaskAllocatorAction;
        return null;
    }
}
