package com.avanza.workflow.configuration;

public enum AssignmentType {

    Mannual, AutoAssign, None, MannualParallel;

    public static AssignmentType fromName(String type) {
        if (type.equalsIgnoreCase(AssignmentType.Mannual.name())) return AssignmentType.Mannual;
        if (type.equalsIgnoreCase(AssignmentType.AutoAssign.name())) return AssignmentType.AutoAssign;
        if (type.equalsIgnoreCase(AssignmentType.None.name())) return AssignmentType.None;
        return null;
    }
}