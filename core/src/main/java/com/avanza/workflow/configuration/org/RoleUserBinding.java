package com.avanza.workflow.configuration.org;

// Generated Sep 24, 2007 2:05:30 PM by Hibernate Tools 3.2.0.beta8

import java.io.Serializable;

import com.avanza.core.data.DbObject;
import com.avanza.core.security.User;

/**
 * OrgUserRole generated by hbm2java
 */
public class RoleUserBinding extends DbObject implements Serializable {

    // Fields
    private String roleId;

    private String orgUnitId;

    private String loginId;

    private User secUser;

    private Role orgRole;

    private OrganizationalUnit orgUnit;

    private boolean isDefaultOrg;

    // Constructors

    /**
     * default constructor
     */
    public RoleUserBinding() {
    }

    /**
     * full constructor
     */
    public RoleUserBinding(String roleid, String orgunitid, String loginid, User secUser, Role orgRole, OrganizationalUnit orgUnit,
                           boolean isDefaultOrg) {
        this.roleId = roleid;
        this.orgUnitId = orgunitid;
        this.loginId = loginid;
        this.secUser = secUser;
        this.orgRole = orgRole;
        this.orgUnit = orgUnit;
        this.isDefaultOrg = isDefaultOrg;
    }

    public User getSecUser() {
        return this.secUser;
    }

    public void setSecUser(User secUser) {
        this.secUser = secUser;
    }

    public Role getOrgRole() {
        return this.orgRole;
    }

    public void setOrgRole(Role orgRole) {
        this.orgRole = orgRole;
    }

    public OrganizationalUnit getOrgUnit() {
        return this.orgUnit;
    }

    public void setOrgUnit(OrganizationalUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public boolean isIsDefaultOrg() {
        return this.isDefaultOrg;
    }

    public void setIsDefaultOrg(boolean isDefaultOrg) {
        this.isDefaultOrg = isDefaultOrg;
    }

    public boolean isDefaultOrg() {
        return isDefaultOrg;
    }

    public void setDefaultOrg(boolean isDefaultOrg) {
        this.isDefaultOrg = isDefaultOrg;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(String orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

}
