package com.avanza.workflow.configuration.org;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * @author anam
 */

public class EntityOrgMapping implements java.io.Serializable {

    // Fields

    private EntityOrgMappingId id;
    private Boolean isDefault;
    private String createdBy;
    private Date createdOn;
    private String updatedBy;
    private Date updatedOn;
    private String metaentityID;
    private String orgID;

    // Constructors

    public String getMetaentityID() {
        return metaentityID;
    }

    public void setMetaentityID(String metaentityID) {
        this.metaentityID = metaentityID;
    }

    public String getOrgID() {
        return orgID;
    }

    public void setOrgID(String orgID) {
        this.orgID = orgID;
    }

    /**
     * default constructor
     */
    public EntityOrgMapping() {
    }

    /**
     * full constructor
     */
    public EntityOrgMapping(String metaentityID, String orgID, Boolean isDefault,
                            String createdBy, Date createdOn, String updatedBy,
                            Date updatedOn) {
        this.metaentityID = metaentityID;
        this.orgID = orgID;
        this.isDefault = isDefault;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.updatedBy = updatedBy;
        this.updatedOn = updatedOn;
    }


    public EntityOrgMapping(String metaentityID, String orgID) {
        this.metaentityID = metaentityID;
        this.orgID = orgID;
        this.isDefault = this.getIsDefault();
        this.createdBy = this.getCreatedBy();
        this.createdOn = this.getCreatedOn();
        this.updatedBy = this.getUpdatedBy();
        this.updatedOn = this.getUpdatedOn();
    }


    // Property accessors

    public EntityOrgMappingId getId() {
        return this.id;
    }

    public void setId(EntityOrgMappingId id) {
        this.id = id;
    }

    public Boolean getIsDefault() {
        return this.isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(java.util.Date createdOn) {
        java.util.Date myUtilDate = createdOn;
        java.sql.Date mySqlDate = new java.sql.Date(myUtilDate.getTime());
        this.createdOn = mySqlDate;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {

        return this.updatedOn;
    }

    public void setUpdatedOn(java.util.Date updatedOn) {
        java.util.Date myUtilDate = updatedOn;
        java.sql.Date mySqlDate = new java.sql.Date(myUtilDate.getTime());
        this.updatedOn = mySqlDate;
    }

}