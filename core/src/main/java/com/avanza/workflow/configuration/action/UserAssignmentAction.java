package com.avanza.workflow.configuration.action;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import org.hibernate.util.StringHelper; [changed by rehan.ahmed] 

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.db.DbConnection;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.Expression;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.adapter.UnisonAlertsProtocol;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.ui.util.ResourceUtil;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.ProcessParticipant;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

/**
 * @author Shahbaz
 * @desc User Allocator action when new email received by the system
 */
public class UserAssignmentAction extends TaskAllocatorAction {

    private static final Logger logger = Logger.getLogger(UserAssignmentAction.class);

    public Object act(ProcessInstance processInst, ActionBinding action,
                      Object context) {

        logger.logInfo("[ UserAssignmentAction called UserAssignmentAction.act() ]");

        ActionExecutionResult result = new ActionExecutionResult();
        List<String> currentActors = new ArrayList<String>();
        DbConnection dbConn = DataRepository.getDbConnection("UNISON_DB");
        ResultSet resultSet = null;
        try {

            Object object = processInst.getContext().getAttribute("IS_COMPOSED");

            // Check if this is a composed email..
            if (object != null && object.equals(true)) {
                result.setChangePossible(true);
                return result;
            }

            logger.logInfo("Checking if the customer is already contacted previously");
            // check if the customer is already contacted by some user
            Object fromObject = processInst.getContext().getAttribute("RECEIVED_FROM");
            String from = fromObject.toString();

            logger.logInfo("Getting DB Connection");

            logger.logInfo("Getting DB Connection Finish ");


            try {
                String sql = "SELECT TOP 1 CURRENT_ACTOR FROM EDOCUMENT WHERE (RECEIVED_FROM = '" + from + "') AND (SENT_ON IS NOT NULL) ORDER BY SENT_ON DESC";
                resultSet = dbConn.getResults(sql, new ArrayList<Object>(0));

                while (resultSet.next())
                    currentActors.add(resultSet.getString("CURRENT_ACTOR"));

            } catch (Exception e) {
                logger.LogException("[ Exception assigning email to existing customer, UserAssignmentAction.act() ]", e);
                result.setChangePossible(false);
            }


            if (currentActors.size() > 0) {
                processInst.setCurrentActor(currentActors);
                processInst.setCalculatedActor(currentActors);
                result.setChangePossible(true);
                return result;

            }

            logger.logInfo("Customer is not contacted previously, applying round robin");
            // if the customer is not already contacted then get users list
            ProcessEntityBinding binding = processInst.getBinding();
            Set<ProcessParticipant> participants = binding.getProcessParticipantList();
            List<String> roles = new ArrayList<String>(0);
            for (ProcessParticipant participant : participants) {
                String selectExp = participant.getSelectCritera();
                if (selectExp != null && selectExp.length() > 0) {
                    ExpressionParser expParser = new ExpressionParser(selectExp, Boolean.class, FunctionCatalogManager.getInstance().getFunctionCatalog(ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction, ExpressionClass.WorkFlowFunctions));
                    Expression retVal = expParser.parse();
                    ExpressionEvaluator eval = new ExpressionEvaluator();
                    Boolean isTrue = (Boolean) eval.evaluate(retVal);
                    if (isTrue) {
                        roles.add(participant.getRoleId());
                    }
                } else {
                    roles.add(participant.getRoleId());
                }
            }

            // Get All user to apply round robin algorithm in a list
            // This list may vary in different implementations
            List<User> allUsers = new ArrayList<User>(0);

            for (String roleStr : roles) {
                // get all users of that role
                List<User> users = OrganizationManager.getAllUsers(roleStr);
                // iterate on users
                for (User user : users) {

                    allUsers.add(user);
                }
            }

            // Get already assigned users
            HashMap<String, Integer> assignedUser = new HashMap<String, Integer>(0);

            String sql = "SELECT CURRENT_ACTOR, COUNT(CURRENT_ACTOR) AS Counts FROM EDOCUMENT GROUP BY CURRENT_ACTOR";

            resultSet = dbConn.getResults(sql, new ArrayList<Object>(0));

            while (resultSet.next())
                assignedUser.put(resultSet.getString("CURRENT_ACTOR"), resultSet.getInt("Counts"));


            // Applying roundRobin
            String robinId = "";

            for (User user : allUsers) {

                String tempId = user.getLoginId();
                if (!assignedUser.containsKey(tempId)) {
                    robinId = tempId;
                    break;
                }
            }

            // Get user which have minimum edocs assigned
            if (StringHelper.isEmpty(robinId) && assignedUser.size() > 0) {
                robinId = getRobin(assignedUser);
            }

            if (StringHelper.isNotEmpty(robinId)) {
                currentActors.add(robinId);
                sendAlert(robinId, processInst.getContext().getAttribute("EDOCUMENT_SUBJECT").toString());
            }

            if (currentActors.size() > 0) {
                processInst.setCurrentActor(currentActors);
                processInst.setCalculatedActor(currentActors);
            }

            result.setChangePossible(true);

        } catch (Exception e) {
            logger.LogException("[ Exception assigning email to existing customer, UserAssignmentAction.act() ]", e);
            result.setChangePossible(false);

        } finally {
            if (dbConn != null)
                dbConn.close();
            if (resultSet != null)
                try {
                    resultSet.close();
                } catch (Throwable t) {
                }
        }

        return result;
    }

    private String getRobin(HashMap<String, Integer> assignedMap) {

        int minimumAssigned = 0;
        String robinLoginId = null;

        for (Map.Entry<String, Integer> entry : assignedMap.entrySet()) {

            if (minimumAssigned == 0) {
                minimumAssigned = entry.getValue();
                robinLoginId = entry.getKey();
                continue;
            }

            if (entry.getValue() < minimumAssigned) {
                minimumAssigned = entry.getValue();
                robinLoginId = entry.getKey();
            }

        }

        return robinLoginId;

    }

    private void sendAlert(String loginId, String emailSubject) {
        logger.logInfo("[ Sending Alert to the user from  UserAssignmentAction.sendAlert() ]");

        String subject = ResourceUtil.getInstance().getProperty("unison-edocument", "new_email_subject");

        NotificationSender.sendAlert(UnisonAlertsProtocol.ALERTS_CHANNEL, subject, emailSubject, loginId, new Date());
    }
}
