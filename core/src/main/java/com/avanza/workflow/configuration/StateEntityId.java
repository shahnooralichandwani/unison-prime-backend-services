package com.avanza.workflow.configuration;

import java.util.Date;

import com.avanza.core.data.DbObject;
import com.avanza.workflow.escalation.EscalationStrategy;

public class StateEntityId extends DbObject implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7957780705502147670L;

    private ProcessEntityBinding binding;

    private State state;

    public ProcessEntityBinding getBinding() {
        return binding;
    }

    public void setBinding(ProcessEntityBinding binding) {
        this.binding = binding;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}