package com.avanza.workflow.configuration.action;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.avanza.core.calendar.CalendarCatalog;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.function.ContextFunction;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.ExpressionFunction;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.Expression;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;
import com.avanza.core.scheduler.JobID;
import com.avanza.core.scheduler.SchedulerCatalog;
import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ExecutionStrategyJob;
import com.avanza.workflow.execution.ParkedStateExpiryJob;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;
import com.avanza.workflow.execution.WorkflowContextLookup;
import com.avanza.workflow.execution.WorkflowExcutionFunctions;

public class SetParkedStateWithTimer implements Action {


    public Object act(ProcessInstance process, ActionBinding action,
                      Object context) {

        Date parkedTill = process.getDataObject().getAsDate("PARKED_TILL");
        ;
        ScheduleParkedStateExpiryJOB(process, parkedTill, action.getParsedActionParams().get("UNPARKED_STATE_CODE"));
        ActionExecutionResult result = new ActionExecutionResult();
        ApplicationContext.getContext().add(WorkflowContextLookup.PROCESS_INSTANCE_PARAM, process);
        try {
            String script = action.getActionScript();
            if (script != null && script.length() > 0) {
                ExpressionParser expParser = new ExpressionParser(script, void.class, FunctionCatalogManager.getInstance().getFunctionCatalog(
                        ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction, ExpressionClass.WorkFlowFunctions));
                Expression retVal = expParser.parse();
                ExpressionEvaluator eval = ExpressionEvaluator.getInstance();
                eval.evaluate(retVal);
            }
        } catch (Exception e) {
            if (action.getCritical()) result.setChangePossible(false);
            if (action.getSerial()) result.setChangePossible(true);
            result.addError(action, e, false);
            return result;
        }
        result.setChangePossible(true);
        return result;
    }

    private static void ScheduleParkedStateExpiryJOB(ProcessInstance process, Date scheduleDate, String unparkedStateCode) {
        HashMap<String, Object> map = (HashMap<String, Object>) process.getDataObject().getValues();
        map.put("DocumentId", process.getBinding().getMetaEntityId());
        map.put("UNPARKED_STATE_CODE", unparkedStateCode);

        SchedulerCatalog.getScheduler().scheduleJob(new JobID("NULL"),
                "ParkedStateExpiryJOB", ParkedStateExpiryJob.class.getName(),
                map,
                "ParkedStateExpiryJOB", scheduleDate);
    }
}
