package com.avanza.workflow.configuration.action;

import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ProcessInstance;

public class ProcessTATExpiredAction implements Action {

    public Object act(ProcessInstance processInstance, ActionBinding action, Object context) {
        System.out.println("Process TAT Check Action");
        processInstance.setTatExpired();
        return null;
    }


}
