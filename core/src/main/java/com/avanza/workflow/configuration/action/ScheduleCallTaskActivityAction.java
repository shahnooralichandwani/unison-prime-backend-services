package com.avanza.workflow.configuration.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Map;

//import javax.faces.model.SelectItem;

import com.avanza.component.calendar.NotificationStrategy;
import com.avanza.component.calendar.Task;
import com.avanza.component.calendar.TaskPriority;
import com.avanza.component.calendar.TaskType;
import com.avanza.component.calendar.TaskUser;
import com.avanza.core.calendar.task.TaskManager;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.JDBCDataBroker;
import com.avanza.core.data.sequence.SequenceManager;
import com.avanza.core.function.ContextFunction;
import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.config.WebContext;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.ui.meta.MetaViewAttribute;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;
import com.avanza.workflow.execution.TaskContext;
import com.avanza.workflow.logger.WorkflowActivityLogger;
import com.avanza.workflow.logger.WorkflowLog;

public class ScheduleCallTaskActivityAction {//[unisonprime] extends TaskAllocatorAction {

/* [unisonprime]
    private static String CUSTOMER_ENTITY = "Unison.Customer";
    private static final Logger logger = Logger.getLogger(NotifyCustomer.class);
    private TaskManager taskManager;
    private List<SelectItem> usersList = new ArrayList<SelectItem>(0);
    
    private List<SelectItem> selectedUserList;
	private List<SelectItem> addedUsersList = new ArrayList<SelectItem>(0);
	private List<SelectItem> selectedAddedUserList;
    
    
     @SuppressWarnings("null")
	public Object act(ProcessInstance processInst, ActionBinding action, Object context) {
        
        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext ctx = processInst.getContext();
        WebContext wc = ApplicationContext.getContext().get(WebContext.class);
        DataObject obj = processInst.getDataObject();
        String rim = (String)ctx.getAttribute("CUST_RELATION_NUM");
        String custName=(String)ctx.getAttribute("CUSTOMER_NAME");
        String callBackType = "A task to call back Customer :"+custName+" (RIM: "+rim+") with Call Type :"+obj.getAsString("STRING_VAL1")+" has been assigned";
        String user = obj.getAsString("STRING_VAL2");
        Date callBackDateTime = obj.getAsDate("DATE_VAL");
        Boolean isPending=false;
        Boolean isAlert=false;
        Boolean isSms= false;
        Boolean isEmail=false;
        //String createdBy=(String)wc.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
        //String updatedBy=(String)wc.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
        
        String taskId=SequenceManager.getInstance().getNextValue(
				TaskManager.TASK_KEY_GENERATOR);
        
       
      
		String taskPriority="Medium";
        String taskType="InternalSystem";
		Date startDate =Convert.toDate(System.currentTimeMillis());
		String taskStrategyId="General";
		String statusId="Not Started";
		
		
    	TaskManager taskManger = TaskManager.getInstance();
    	
    	String CustomerName= (String)ctx.getAttribute("CUSTOMER_NAME");
    	String RIM= (String)ctx.getAttribute("CUST_RELATION_NUM");
    		
    	UserDbImpl currentUser  = (UserDbImpl)wc.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);

    	Task task = new Task();
    	task.setTaskId(taskId);
    	task.setCompletionDate(callBackDateTime);
    	task.setDueDate(callBackDateTime);
    	task.setStartDate(callBackDateTime);
    	task.setDescription(callBackType);    	
    	task.setOwnerUserId(user);
    	task.setIsNotfAlert(isAlert);
    	task.setIsNotfEmail(isEmail);
    	task.setIsPending(isPending);
    	task.setIsNotfSms(isSms);
    	task.setNotificationStrategy(taskManger.getDefaultNotificationStrategy());
    	task.setTaskPriority(taskManger.getDefaultTaskPriority());
    	task.setTaskStatus(taskManger.getDefaultTaskStatus());
    	task.setTaskType(TaskType.InternalSystem.toString());
    	TaskManager.getInstance().persist(task);
    	
    	Set<TaskUser> set = new HashSet<TaskUser>();
    	TaskUser TU = new TaskUser();
    	TU.setAssignedDate(callBackDateTime);
    	TU.setUser(currentUser);
    	TU.setTask(task);
    	TU.setLoginId(currentUser.getLoginId());
    	set.add(TU);
    	TaskManager.getInstance().persist(TU);
    	String currentActors = user;
    	List<String> users = Convert.toList(currentActors);
    	users.remove(currentUser.getLoginId());
    	
    		TaskUser currentTU = new TaskUser();
    		currentTU.setAssignedDate(new Date());
    		currentTU.setUser(currentUser);
    		currentTU.setTask(task);
    		currentTU.setLoginId(user);
        	set.add(currentTU);
        	TaskManager.getInstance().persist(currentTU);
    	
    	task.setTaskUsers(set);    	
    	wc.setAttribute(SessionKeyLookup.CURRENT_TASK,task);
    	
    	TaskContext taskContext = (TaskContext)context;
    	
    	*//*
    	else{
    		WorkflowActivityLogger.logWorkflowActivity(processInstance,processInstance.getCurrentState().getStateCode(), null, "");
    		taskContext.getParameters().put("LOG", log);
    		log = (WorkflowLog)taskContext.getParameters().get("LOG");
    		WorkflowActivityLogger.updateLogWorkflowActivity(log, appointmentDate); 
    	}
    	return "";
    }
		JDBCDataBroker broker = (JDBCDataBroker)DataRepository.getBroker("JDBC.Unison");
		String query = "INSERT INTO [TASK]([TASK_ID],[TASK_TYPE],[STRATEGY_ID],[START_DATE],[DUE_DATE],[PRIORITY_ID],[DESCRIPTION],[STATUS_ID],[COMPLETION_DATE],[OWNER_USER_ID],[IS_PENDING],[IS_SMS],[IS_EMAIL],[IS_ALERT],[CREATED_ON],[CREATED_BY] ,[UPDATED_ON],[UPDATED_BY]) VALUES ('" + 
		taskId + "','" + taskType+"','" + taskStrategyId + "',GETDATE(),'" + callBackDateTime + "','" + 
		taskPriority + "','" + callBackType + "','" + statusId + "',NULL,'" + user + "'," + isPending + "," + isSms + "," + isEmail + ","+isAlert+ ",'"+createdBy+"',GETDATE(),'" +updatedBy+"')";

      	broker.update(query);
      	
      	String query2= "INSERT INTO [TASK_USER]([TASK_ID],[LOGIN_ID],[ASSIGNED_DATE],[CREATED_ON],[CREATED_BY],[UPDATED_ON],[UPDATED_BY]) VALUES ('" + 
		taskId + "','" + user+"',GETDATE(),GETDATE()'" + callBackDateTime + "','" + 
		createdBy + "',GETDATE()'" + updatedBy + "')";

		broker.update(query2);*//*
		
		
		
    return result; 
}*/

}
		
	
