package com.avanza.workflow.configuration;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.avanza.core.data.DbObject;

/**
 * WfStateTaskParticipant entity. @author MyEclipse Persistence Tools
 */

public class StrategicActionParticipant extends DbObject implements Serializable {


    private static final long serialVersionUID = 1242534L;
    // Fields

    private StrategicActionParticipantId id;
    private String userIds;

    // Constructors

    /**
     * default constructor
     */
    public StrategicActionParticipant() {
    }

    /**
     * full constructor
     */
    public StrategicActionParticipant(StrategicActionParticipantId id,
                                      String createdBy, Timestamp createdOn, String updatedBy,
                                      Timestamp updatedOn) {
        this.id = id;
    }

    // Property accessors

    public StrategicActionParticipantId getId() {
        return this.id;
    }

    public void setId(StrategicActionParticipantId id) {
        this.id = id;
    }

    public String getUserIds() {
        return userIds;
    }

    public void setUserIds(String userIds) {
        this.userIds = userIds;
    }

    public List<String> getUserIdsList() {
        List<String> users = new ArrayList<String>(0);
        if (userIds != null)
            users = Arrays.asList(userIds.split(","));
        return users;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof StrategicActionParticipant) {
            StrategicActionParticipant compairParticipant = (StrategicActionParticipant) obj;
            if (compairParticipant.getId().getProcessAllocId().equals(this.getId().getProcessAllocId()) &&
                    compairParticipant.getId().getProcessCode().equals(this.getId().getProcessCode()) &&
                    compairParticipant.getId().getRoleId().equals(this.getId().getRoleId()) &&
                    compairParticipant.getId().getStrategicActionCode().equals(this.getId().getStrategicActionCode())) {
                return true;
            }
        }
        return false;
    }

}