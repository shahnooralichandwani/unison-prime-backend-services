package com.avanza.workflow.configuration.action;

import com.avanza.core.util.StringHelper;
import com.avanza.workflow.configuration.action.ActionBinding;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskContext;
import com.avanza.workflow.execution.ValidatorAction;
import com.avanza.workflow.execution.ValidatorActionResult;

/**
 * @author Khawaja Muhammad Aamir
 */
public class ComplaintClosedValidator extends ValidatorAction {

    private static String COMP_CLOSED_REASON = "COMPLAINT_CLOSED_REASON";

    public ValidatorActionResult act(ProcessInstance process, ActionBinding action,
                                     Object context) {
        ValidatorActionResult result = new ValidatorActionResult();
        TaskContext taskCtx = (TaskContext) context;
        if (StringHelper.isNotEmpty(taskCtx.getStateCode()) && taskCtx.getStateCode().equalsIgnoreCase("0000000006")) {
            ProcessContext ctx = process.getContext();
            Object reason = ctx.getAttribute(COMP_CLOSED_REASON);
            if ((reason == null) || (StringHelper.isEmpty(reason.toString()))) {
                result.setSuccess(false);
                result.setMessage("Please select Complaint Closed Reason");
                return result;
            }
        }
        result.setSuccess(true);
        return result;
    }
}
