package com.avanza.workflow.configuration.action;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.StringHelper;
import com.avanza.workflow.configuration.EventType;

/**
 * This Class represents a relationship between a Action and State or Transition
 *
 * @author hmirza
 */
@SuppressWarnings("unchecked")
public class ActionBinding extends DbObject implements Comparable, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private long executionSequence;

    private int Id;

    private long eventCode;

    private String escLevelId;

    private String escStrategyId;

    private String actionCode;

    private String stateCode;

    private String transitionCode;

    private String processCode;

    private boolean critical;

    private boolean serial;

    private String actionScript;

    private String actionParam;

    private boolean transitionCritical;

    private String stateTaskCode;

    private String metaEntityId;

    public String getEscLevelId() {
        return escLevelId;
    }

    public void setEscLevelId(String escLevelId) {
        this.escLevelId = escLevelId;
    }

    public String getEscStrategyId() {
        return escStrategyId;
    }

    public void setEscStrategyId(String escStrategyId) {
        this.escStrategyId = escStrategyId;
    }

    public long getEventCode() {
        return eventCode;
    }

    public void setEventCode(long eventCode) {
        this.eventCode = eventCode;
    }

    public int compareTo(Object arg0) {

        // Checks for the Event Type priority first
        if (EventType.fromName(this.getEventCode()).priority() == EventType.fromName(
                ((ActionBinding) arg0).getEventCode()).priority()) {

            // If Event Type priorities are equal than checks for the
            // execution order
            if ((this).getExecutionSequence() == ((ActionBinding) arg0).getExecutionSequence()) {

                return 0;
            }
            return (this).getExecutionSequence() < ((ActionBinding) arg0).getExecutionSequence() ? -1 : 1;

        } else {

            return (EventType.fromName(getEventCode()).priority() < EventType.fromName(
                    ((ActionBinding) arg0).getEventCode()).priority()) ? -1 : 1;
        }

    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public long getExecutionSequence() {
        return executionSequence;
    }

    public void setExecutionSequence(long executionSequence) {
        this.executionSequence = executionSequence;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getTransitionCode() {
        return transitionCode;
    }

    public void setTransitionCode(String transitionCode) {
        this.transitionCode = transitionCode;
    }

    public boolean getCritical() {
        return critical;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getSerial() {
        return serial;
    }

    public void setSerial(boolean serial) {
        this.serial = serial;
    }

    public boolean getTransitionCritical() {
        return transitionCritical;
    }

    public void setTransitionCritical(boolean transitionCritical) {
        this.transitionCritical = transitionCritical;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getActionScript() {
        return actionScript;
    }

    public void setActionScript(String actionScript) {
        this.actionScript = actionScript;
    }

    public String getActionParam() {
        return actionParam;
    }

    public void setActionParam(String actionParam) {
        this.actionParam = actionParam;
    }

    Map<String, String> parsedParams;

    public Map<String, String> getParsedActionParams() {
        if (parsedParams == null) {
            Map<String, String> params;
            if (actionParam != null) {
                params = StringHelper.getPropertiesMapFrom(actionParam);
            } else {
                params = new HashMap<String, String>();
            }
            parsedParams = params;
        }
        return parsedParams;
    }

    public String getStateTaskCode() {
        return stateTaskCode;
    }

    public void setStateTaskCode(String stateTaskCode) {
        this.stateTaskCode = stateTaskCode;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

}
