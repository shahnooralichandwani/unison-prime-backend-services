package com.avanza.workflow.configuration.org;

import java.util.Random;

import org.apache.commons.beanutils.BeanUtils;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DbObject;
import com.avanza.core.interceptor.Auditable;
import com.avanza.core.meta.audit.OrgRoleAudit;
import com.avanza.core.meta.audit.OrgRoleAuditId;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TreeObject;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;

/**
 * Main Role Class
 *
 * @author hmirza
 */

/**
 * 53:osama
 * Extend TreeObject Class previously it was extending Class DBObject 
 * overrides all TreeObject methods 
 * Creates Overloaded Constructor 
 */

public class Role extends TreeObject implements Comparable, Auditable {

    private static final long serialVersionUID = 4096383657368488865L;

    private String roleId;

    private Role parentRole;

    private String rolePrimaryName;

    private String roleSecondaryName;

    public Role() {
        super();
        roleId = StringHelper.EMPTY;
    }


    public Role(String id) {
        super(id);
        roleId = id;
        parentRole = new Role(StringHelper.EMPTY, true);
    }

    public Role(String id, boolean flag) {
        if (flag)
            roleId = id;
        else
            roleId = null;
    }


    public Role getParentRole() {
        return parentRole;
    }

    public void setParentRole(Role parentRole) {
        this.parentRole = parentRole;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
        super.id = roleId;
    }

    public String getRolePrimaryName() {
        return rolePrimaryName;
    }

    public void setRolePrimaryName(String rolePrimaryName) {
        this.rolePrimaryName = rolePrimaryName;
    }

    public String getRoleSecondaryName() {
        return roleSecondaryName;
    }

    public void setRoleSecondaryName(String roleSecondaryName) {
        this.roleSecondaryName = roleSecondaryName;
    }

    public boolean isRoot() {
        return parentRole == null;
    }

    private String value;

    @Override
    public TreeObject clone() {
        Role orgRole = new Role(this.id);
        orgRole.copyValues(this);

        // Now need to copy their children also.
        for (TreeObject tobj : this.childList) {
            TreeObject cloned = ((Role) tobj).clone();
            ((Role) cloned).parentRole = orgRole;
            orgRole.childList.add(cloned);
        }
        return orgRole;
    }

    @Override
    public void copyValues(DbObject copyFrom) {

        if (!(copyFrom instanceof Role)) return;

        Role orgRole = (Role) copyFrom;

        super.copyValues(copyFrom);
        this.rolePrimaryName = orgRole.rolePrimaryName;
        this.roleSecondaryName = orgRole.roleSecondaryName;
        this.parentRole = orgRole.parentRole;
    }

    @Override
    protected TreeObject createObject(String id) {
        // TODO Auto-generated method stub
        return new Role(id);
    }

    @Override
    public <T extends TreeObject> void add(T item) {
        super.add(item);
        ((Role) item).parentRole = this;
    }

    @Override
    public boolean equals(Object obj) {
        Role orgRole = (Role) obj;

        if (orgRole.getId().equalsIgnoreCase(this.getId()) && orgRole.getKey().equalsIgnoreCase(this.getKey()))
            return true;

        return false;
    }

    @Override
    public int hashCode() {
        return roleId.hashCode();
    }

    @Override
    public String getPrimaryName() {
        // TODO Auto-generated method stub
        return rolePrimaryName;
    }

    public void setPrimaryName(String orgNamePrm) {
        this.rolePrimaryName = orgNamePrm;
    }

    @Override
    public String getSecondaryName() {
        // TODO Auto-generated method stub
        return roleSecondaryName;
    }

    public void setSecondaryName(String orgNameSec) {
        this.roleSecondaryName = orgNameSec;
    }

    @Override
    public String getValue() {
        // TODO Auto-generated method stub
        value = this.rolePrimaryName;
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @Override
    public int compareTo(Object arg0) {
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        LocaleInfo currentLocale = webContext.getAttribute(SessionKeyLookup.CurrentLocale);
        if (currentLocale.isPrimary())
            return getRolePrimaryName().compareTo(((Role) arg0).getRolePrimaryName());
        else
            return ((getRoleSecondaryName() != null && ((Role) arg0).getRoleSecondaryName() != null)
                    ? getRoleSecondaryName().compareTo(((Role) arg0).getRoleSecondaryName())
                    : 0);

    }


    @Override
    public Object getAuditEntry(Short revtype) {
        OrgRoleAudit orgRoleAudit = null;
        try {

            Random rand = new Random();

            orgRoleAudit = new OrgRoleAudit();
            orgRoleAudit.setRevtype(revtype);

            BeanUtils.copyProperties(orgRoleAudit, this);
            orgRoleAudit.setOrgRoleAuditId(new OrgRoleAuditId(this.roleId, rand.nextInt()));


        } catch (Exception e) {
            orgRoleAudit = null;
        }
        return orgRoleAudit;
    }


}
