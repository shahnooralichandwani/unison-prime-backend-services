package com.avanza.workflow.configuration;

import java.io.Serializable;

import com.avanza.core.data.DbObject;

/**
 * This class represents a State in Workflow Process
 *
 * @author fkazmi
 */

public class GlobalState extends DbObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String stateCode;

    private String primaryName;

    private String secondaryName;


    public String getPrimaryName() {
        return primaryName;
    }


    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }


    public String getSecondaryName() {
        return secondaryName;
    }


    public void setSecondaryName(String secondaryName) {
        this.secondaryName = secondaryName;
    }


    public String getStateCode() {
        return stateCode;
    }


    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }
}
