package com.avanza.workflow.configuration.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.Expression;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;
import com.avanza.core.security.User;
import com.avanza.workflow.configuration.AssignmentType;
import com.avanza.workflow.configuration.Process;
import com.avanza.workflow.configuration.ProcessCatalog;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.ProcessParticipant;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.Transition;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;
import com.avanza.workflow.execution.TaskContext;
import com.avanza.workflow.execution.TaskLoadManager;
import com.avanza.workflow.execution.WorkflowContextLookup;

public class LoadBalanceTaskAllocation extends TaskAllocatorAction {

    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {
        ActionExecutionResult result = new ActionExecutionResult();
        TaskContext ctx = (TaskContext) context;
        State stat = processInst.getCurrentState();

        try {
            List<String> currentActors = new ArrayList<String>();
            Transition transition = stat.getTransitionbyId(ctx.getTaskCode());
            if (transition != null) {
                if (AssignmentType.fromName(transition.getAssignmentType()) == AssignmentType.AutoAssign) {
                    Set<StateParticipant> stateParticipants = processInst.getStateParticipants(processInst.getContext().getDocumentId(), stat.getStateCode());
                    for (StateParticipant participant : stateParticipants) {
                        String roleid = participant.getRoleId();
                        String selectCritera = participant.getSelectCritera();
                        List<User> users = OrganizationManager.getAllUsers(roleid);
                        for (User user : users) {
                            if (!currentActors.contains(user.getLoginId())) {
                                ExpressionParser expParser = new ExpressionParser(selectCritera, Boolean.class, FunctionCatalogManager.getInstance().getFunctionCatalog(
                                        ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction, ExpressionClass.WorkFlowFunctions));
                                Expression retVal = expParser.parse();
                                ExpressionEvaluator eval = ExpressionEvaluator.getInstance();
                                ApplicationContext.getContext().add(WorkflowContextLookup.USER_IN_ORG_PARAM, user.getLoginId());
                                Boolean isTrue = (Boolean) eval.evaluate(retVal);
                                if (isTrue) {
                                    currentActors.add(user.getLoginId());
                                }
                            }
                        }

                    }
                }
            } else {
                Process proc = ProcessCatalog.getProcessByProcessCode(processInst.getProcessCode());
                for (ProcessEntityBinding binding : proc.getProcBindingList()) {
                    for (ProcessParticipant participant : binding.getProcessParticipantList()) {
                        List<User> users = OrganizationManager.getAllUsers(participant.getRoleId());
                        for (User user : users) {
                            if (!currentActors.contains(user.getLoginId())) {
                                ExpressionParser expParser = new ExpressionParser(participant.getSelectCritera(), Boolean.class, FunctionCatalogManager.getInstance().getFunctionCatalog(
                                        ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction, ExpressionClass.WorkFlowFunctions));
                                Expression retVal = expParser.parse();
                                ExpressionEvaluator eval = ExpressionEvaluator.getInstance();
                                ApplicationContext.getContext().add(WorkflowContextLookup.USER_IN_ORG_PARAM, user.getLoginId());
                                Boolean isTrue = (Boolean) eval.evaluate(retVal);
                                if (isTrue) {
                                    currentActors.add(user.getLoginId());
                                }
                            }
                        }
                    }
                }
            }
            if (currentActors.size() > 0) {
                if (currentActors.size() == 1) {
                    processInst.setCurrentActor(currentActors);
                    processInst.setCalculatedActor(currentActors);
                    TaskLoadManager.incrementUserWorkLoad(currentActors.get(0), processInst.getContext().getDocumentId());
                } else {
                    TaskLoadManager.calculateAndAssign(currentActors, processInst.getContext().getDocumentId(), processInst);
                }
            } else {
                processInst.setSupervisorAsActors(processInst.getContext().getCurrentOrgUnit());
            }
            result.setChangePossible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
