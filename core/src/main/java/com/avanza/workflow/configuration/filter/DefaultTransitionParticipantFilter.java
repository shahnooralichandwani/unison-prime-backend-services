package com.avanza.workflow.configuration.filter;

import java.util.List;
import java.util.Set;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.util.StringHelper;
import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.Transition;
import com.avanza.workflow.configuration.TransitionParticipant;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.Role;

//TODO need to refactor this class for optimization -- shoaib.rehman
public class DefaultTransitionParticipantFilter implements TransitionParticipantFilter {
    public boolean getActivities(Transition transition, Set<StateParticipant> stateParticipant, String userId) {
        for (TransitionParticipant tp : transition.getTransitionParticipantList()) {
            if (isUserExistInRole(userId, tp.getId().getRoleId())) {
                if (tp.getId().getTaskCode().equals(transition.getTransitionCode()) &&
                        tp.getId().getProcessCode().equals(transition.getProcessCode())) {
                    for (StateParticipant sp : stateParticipant) {
                        if (sp.getRoleId().equals(tp.getId().getRoleId()) && sp.getStateCode().equals(tp.getId().getStateCode()) &&
                                sp.getProcessAllocId().equals(tp.getId().getProcessAllocId())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean isRoleInTransitionParticipant(List<Role> roles) {
        DataBroker broker = DataRepository.getBroker(TransitionParticipant.class.getName());
        List<TransitionParticipant> list = null;
        for (Role role : roles) {
            Search search = new Search(TransitionParticipant.class);
            search.addCriterion(Criterion.equal("id.roleId", role.getRoleId()));
            list = broker.find(search);
            if (list != null && list.size() > 0)
                return true;
        }

        return false;
    }

    public boolean isUserExistInRole(String userId, String roleId) {
        List<Role> roles = OrganizationManager.getAllRoles(userId);
        for (Role role : roles) {
            if (role.getRoleId().equals(roleId))
                return true;
        }
        return false;
    }


}
