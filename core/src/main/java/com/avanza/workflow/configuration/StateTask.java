package com.avanza.workflow.configuration;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;

/**
 * WfStateTask entity. @author MyEclipse Persistence Tools
 */

public class StateTask extends DbObject implements Serializable {


    private static final long serialVersionUID = 124234L;

    // Fields

    private StateTaskId id;
    private String stateTransitionCode;
    private String taskName;
    private String parentTaskCode;
    private String parentTaskCodeLabel;
    private Boolean isMandatory;
    private String metaViewId;
    private String metaViewIdLabel;
    private Boolean isParentCompleted;
    private String taskCompletionStrategy;
    private String mandatoryType;
    private String metaEntId;
    private Boolean isPreSave = false;

    private Set<StateTaskParticipant> stateTaskParticipantList = new HashSet<StateTaskParticipant>(0);

    // Constructors


    /**
     * default constructor
     */
    public StateTask() {
    }

    /**
     * minimal constructor
     */
    public StateTask(StateTaskId id, String taskName, Boolean isMandatory,
                     Timestamp createdOn, String createdBy, Timestamp updatedOn,
                     String updatedBy, String metaViewId) {
        this.id = id;
        this.taskName = taskName;
        this.isMandatory = isMandatory;
        this.metaViewId = metaViewId;
    }

    /**
     * full constructor
     */
    public StateTask(StateTaskId id, String stateTransitionCode, String taskName,
                     String parentTaskCode, Boolean isMandatory, Timestamp createdOn,
                     String createdBy, Timestamp updatedOn, String updatedBy, String metaViewId) {
        this.id = id;
        this.taskName = taskName;
        this.parentTaskCode = parentTaskCode;
        this.isMandatory = isMandatory;
        this.metaViewId = metaViewId;
    }

    // Property accessors

    public StateTaskId getId() {
        return this.id;
    }

    public void setId(StateTaskId id) {
        this.id = id;
    }

    public String getTaskName() {
        return this.taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getParentTaskCode() {
        return this.parentTaskCode;
    }

    public void setParentTaskCode(String parentTaskCode) {
        this.parentTaskCode = parentTaskCode;
    }

    public Boolean getIsMandatory() {
        return this.isMandatory;
    }

    public void setIsMandatory(Boolean isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getStateTransitionCode() {
        return stateTransitionCode;
    }

    public void setStateTransitionCode(String stateTransitionCode) {
        this.stateTransitionCode = stateTransitionCode;
    }

    public Set<StateTaskParticipant> getStateTaskParticipantList() {
        return stateTaskParticipantList;
    }

    public void setStateTaskParticipantList(
            Set<StateTaskParticipant> stateTaskParticipantList) {
        this.stateTaskParticipantList = stateTaskParticipantList;
    }

    public Set<StateTaskParticipant> getStateTaskParticipantList(String processAllocId) {
        Set<StateTaskParticipant> tempParticipantsList = new HashSet<StateTaskParticipant>();
        for (StateTaskParticipant participant : stateTaskParticipantList) {
            if (participant.getId().getProcessAllocId().equals(processAllocId))
                tempParticipantsList.add(participant);
        }
        return tempParticipantsList;
    }


    public String getMetaViewId() {
        return metaViewId;
    }

    public void setMetaViewId(String metaViewId) {
        this.metaViewId = metaViewId;
    }

    public Boolean getIsParentCompleted() {
        return isParentCompleted;
    }

    public void setIsParentCompleted(Boolean isParentCompleted) {
        this.isParentCompleted = isParentCompleted;
    }

    public String getTaskCompletionStrategy() {
        return taskCompletionStrategy;
    }

    public void setTaskCompletionStrategy(String taskCompletionStrategy) {
        this.taskCompletionStrategy = taskCompletionStrategy;
    }

    public String getMandatoryType() {
        return mandatoryType;
    }

    public void setMandatoryType(String mandatoryType) {
        this.mandatoryType = mandatoryType;
    }

    public void setMetaViewIdLabel(String metaViewIdLabel) {
        this.metaViewIdLabel = metaViewIdLabel;
    }

    public String getMetaViewIdLabel() {
        return metaViewIdLabel;
    }

    public void setParentTaskCodeLabel(String parentTaskCodeLabel) {
        this.parentTaskCodeLabel = parentTaskCodeLabel;
    }

    public String getParentTaskCodeLabel() {
        return parentTaskCodeLabel;
    }


    public String getMetaEntId() {
        return metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

    public Boolean getIsPreSave() {
        return isPreSave;
    }

    public void setIsPreSave(Boolean isPreSave) {
        this.isPreSave = isPreSave;
    }
}