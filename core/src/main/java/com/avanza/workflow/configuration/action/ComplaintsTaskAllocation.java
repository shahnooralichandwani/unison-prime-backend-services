package com.avanza.workflow.configuration.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.constants.ThreadContextLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.Expression;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;
import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.User;
import com.avanza.core.web.config.WebContext;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.ProcessParticipant;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;
import com.avanza.workflow.execution.TaskLoadManager;

/**
 * @author Khawaja Muhammad Aamir
 * @desc Allocate customer complaints to complaint teams
 */
public class ComplaintsTaskAllocation extends TaskAllocatorAction {

    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {
        // getting the product meta entity id from thread context lookup
        String productEntityId = ApplicationContext.getContext().get(ThreadContextLookup.ProductMetaEntity.toString());
        ActionExecutionResult result = new ActionExecutionResult();

        // getting the webcontext from application context
        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        // get customer who recorded complaint
        DataObject customer = (DataObject) webCtx.getAttribute(SessionKeyLookup.CURRENT_CUSTOMER_KEY);
        if (customer == null) {
            return result;
        }
        // select the branch code of customer
        String branchCode = (String) customer.getValue((String) InboundLoader.getInstance().getCustomerColumnName(
                "BRANCH_CODE"));

        ProcessEntityBinding binding = processInst.getBinding();
        Set<ProcessParticipant> participants = binding.getProcessParticipantList();
        List<String> roles = new ArrayList<String>(0);
        List<String> currentActors = new ArrayList<String>();
        for (ProcessParticipant participant : participants) {
            String selectExp = participant.getSelectCritera();
            if (selectExp != null && selectExp.length() > 0) {
                ExpressionParser expParser = new ExpressionParser(selectExp, Boolean.class, FunctionCatalogManager.getInstance().getFunctionCatalog(
                        ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction, ExpressionClass.WorkFlowFunctions));
                Expression retVal = expParser.parse();
                ExpressionEvaluator eval = new ExpressionEvaluator(productEntityId);
                Boolean isTrue = (Boolean) eval.evaluate(retVal);
                if (isTrue) {
                    roles.add(participant.getRoleId());
                }
            } else {
                roles.add(participant.getRoleId());
            }
        }
        for (String roleStr : roles) {
            // get all users of that role and current org. unit -- shoaib.rehman
            List<User> users = OrganizationManager.getUsers(processInst.getContext().getCurrentOrgUnit(), roleStr);
            // iterate on users
            for (User user : users) {
                if (!currentActors.contains(user.getLoginId()))
                    currentActors.add(user.getLoginId());
            }
        }

        // For Sales-Demo purpose // inserting current customer as actor
		/*UserDbImpl user = (UserDbImpl) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(
	                SessionKeyLookup.CURRENT_USER_KEY);
		 
		if(!(Convert.toCSV(currentActors).contains(user.getLoginId())))
			currentActors.add(user.getLoginId());*/

        /// END For Sales-Demo purpose


        // if there are some users who are in same org unit for which user log
        // complaint
        if (currentActors.size() > 0) {
            // if only one user increase his/her workload
            if (currentActors.size() == 1)
                TaskLoadManager.incrementUserWorkLoad(currentActors.get(0), MetaDataRegistry
                        .getNonAbstractParentEntity(
                                MetaDataRegistry.getMetaEntity(processInst.getContext().getDocumentId())).getId());
            // assign that complaint to selected users
            processInst.setCurrentActor(currentActors);
            processInst.setCalculatedActor(currentActors);
        } else {
            // if there is no user who can handle this complaint then the
            // complain will be assign to supervisor of that branch
            processInst.setSupervisorAsActors(branchCode);
        }
        result.setChangePossible(true);
        return result;
    }
}
