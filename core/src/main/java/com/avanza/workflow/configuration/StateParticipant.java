package com.avanza.workflow.configuration;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;

public class StateParticipant extends DbObject implements Serializable {

    private static final long serialVersionUID = 1L;

    private String stateCode;

    private String processAllocId;

    private String roleId;

    private String selectCritera;

    //private Set<TransitionParticipant> transitionParticipantList = new HashSet<TransitionParticipant>(0);

    public String getSelectCritera() {
        return selectCritera;
    }

    public void setSelectCritera(String selectCritera) {
        this.selectCritera = selectCritera;
    }

    public StateParticipant() {
    }

    public StateParticipant(String statecode, String procCode, String roleid) {

        this.stateCode = statecode;
        this.processAllocId = procCode;
        this.roleId = roleid;
    }

    public String getProcessAllocId() {
        return processAllocId;
    }

    public void setProcessAllocId(String processAllocId) {
        this.processAllocId = processAllocId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

	/*public Set<TransitionParticipant> getTransitionParticipantList() {
		return transitionParticipantList;
	}

	public void setTransitionParticipantList(
			Set<TransitionParticipant> transitionParticipantList) {
		this.transitionParticipantList = transitionParticipantList;
	}*/
}
