package com.avanza.workflow.configuration.org;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.avanza.core.FunctionDelegate;
import com.avanza.core.FunctionDelegate.Arg1;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.HibernateDataBroker;
import com.avanza.core.data.JDBCDataBroker;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.OperatorType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.MetaPicklist;
import com.avanza.core.security.User;
import com.avanza.core.util.CollectionHelper;
import com.avanza.core.util.DateUtil;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.Wrapper;

public class OrganizationManager {

    private static final Logger logger = Logger.getLogger(OrganizationManager.class);
    private static Map<String, OrganizationalUnit> orgUnits = new Hashtable<String, OrganizationalUnit>(
            0);

    private static List<RoleUserBinding> orguserRoles = new ArrayList<RoleUserBinding>(
            0);
    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock readLock = readWriteLock.readLock();
    private static Lock writeLock = readWriteLock.writeLock();
    private static Map<String, Role> roles = new Hashtable<String, Role>();
    private static Date orgUserRoleLastUpdateOn = null;

    public static void load() {
        try {
            writeLock.lock();
            orgUnits = new Hashtable<String, OrganizationalUnit>(0);
            orguserRoles = new ArrayList<RoleUserBinding>(0);
            HibernateDataBroker broker = (HibernateDataBroker) DataRepository
                    .getBroker(OrganizationManager.class.getName());
            Search search = new Search();
            search.addFrom(OrganizationalUnit.class);
            search.addCriterion(Criterion.equal("isActive", true)); // Only Active org units
            List<OrganizationalUnit> orgunits = broker.find(search);
            for (OrganizationalUnit unit : orgunits) {
                String id = unit.getOrgUnitId();
                orgUnits.put(id, unit);
            }
            //--- Change for Active Organization Records
            search = new Search(((Session) DataRepository.getBroker(RoleUserBinding.class.getName()).getSession()).createCriteria(RoleUserBinding.class));
            search.getHqlCriteria().setFetchMode("orgUnit", FetchMode.JOIN);
            search.getHqlCriteria().createAlias("orgUnit", "orgUnit");
            search.getHqlCriteria().add(Restrictions.eq("orgUnit.isActive", true)); //--- Only ACTIVE Units
            List<RoleUserBinding> orguserroles = broker.find((search));
            orguserRoles = orguserroles;
            List<Role> rList = broker.findAll(Role.class);
            for (Role r : rList)
                roles.put(r.getRoleId(), r);
            orgUserRoleLastUpdateOn = getLastUpdateOnDate();

        } finally {
            writeLock.unlock();
        }
    }

    public static void updateOrgRoleCache() {

        logger.logInfo("Executing updateOrgRoleCache() - Start");
        try {

            Date lastUpdateOn = getLastUpdateOnDate();

            if (!lastUpdateOn.equals(orgUserRoleLastUpdateOn)) {
                logger.logInfo("Start - updating org user role maps in cache from DB...");
                Map<String, OrganizationalUnit> tempOrgUnits = new Hashtable<String, OrganizationalUnit>(0);
                List<RoleUserBinding> tempOrguserRoles = new ArrayList<RoleUserBinding>(0);
                List<OrgRoleCategory> tempOrgrolescategory = new ArrayList<OrgRoleCategory>(0);

                HibernateDataBroker broker = (HibernateDataBroker) DataRepository
                        .getBroker(OrganizationManager.class.getName());

                List<OrganizationalUnit> orgunits = broker
                        .findAll(OrganizationalUnit.class);
                for (OrganizationalUnit unit : orgunits) {
                    String id = unit.getOrgUnitId();
                    tempOrgUnits.put(id, unit);
                }
                List<RoleUserBinding> orguserroles = broker.findAll(RoleUserBinding.class);
                tempOrguserRoles = orguserroles;
                List<Role> rList = broker.findAll(Role.class);
                for (Role r : rList)
                    roles.put(r.getRoleId(), r);

                tempOrgrolescategory = broker.findAll(OrgRoleCategory.class);

                orgUnits = new Hashtable<String, OrganizationalUnit>(0);
                orgUnits = tempOrgUnits;

                orguserRoles = new ArrayList<RoleUserBinding>(0);
                orguserRoles = tempOrguserRoles;

                orgRolesCategory = tempOrgrolescategory;

                orgUserRoleLastUpdateOn = lastUpdateOn;

                logger.logInfo("End - updating org user role maps in cache from DB...");
            }

            logger.logInfo("Executing updateOrgRoleCache() - End");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Below method is used to fetch UPDATED_ON
     * of last updated record from ORG_USER_ROLE
     * */
    private static Date getLastUpdateOnDate() {
        try {
            JDBCDataBroker jBroker = (JDBCDataBroker) DataRepository.getBroker("JDBC.Unison");
            String upOnQry = "Select top 1 UPDATED_ON from ORG_USER_ROLE ORDER BY UPDATED_ON  DESC";
            List<Object[]> rstSet = jBroker.executeQuery(upOnQry);
            Object[] obj = rstSet.get(1);
            return (Date) obj[0];

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


    public static void reLoad() {

    }

    public static void dispose() {
        try {
            writeLock.unlock();
            orgUnits = new Hashtable<String, OrganizationalUnit>(0);
        } finally {
            writeLock.unlock();
        }
    }

    public static OrganizationalUnit getOrganizationUnit(String unitid) {
        try {
            readLock.lock();
            return orgUnits.get(unitid);
        } finally {
            readLock.unlock();
        }
    }

    public static List<User> getAllUsers(String roleid) {
        try {
            readLock.lock();
            List<User> users = new Vector<User>();
            for (RoleUserBinding roleuser : orguserRoles) {
                if (roleuser.getRoleId().equalsIgnoreCase(roleid)) {
                    if (!users.contains(roleuser.getSecUser()))
                        users.add(roleuser.getSecUser());
                }
            }
            return users;

        } finally {
            readLock.unlock();
        }
    }

    public static List<User> getAllUsers() {
        try {
            readLock.lock();
            List<User> users = new Vector<User>();
            for (RoleUserBinding roleuser : orguserRoles) {
                User user = roleuser.getSecUser();
                if (!users.contains(user))
                    users.add(user);
            }
            Collections.sort(users);

            return users;

        } finally {
            readLock.unlock();
        }

    }


    public static List<Role> getAllRoles(String userid) {
        //Shuwair
        try {
            readLock.lock();
            List<Role> roles = new Vector<Role>();
            for (RoleUserBinding roleuser : orguserRoles) {
                if (userid.equalsIgnoreCase(roleuser.getSecUser().getLoginId()))
                    roles.add(roleuser.getOrgRole());
            }
            return roles;

        } finally {
            readLock.unlock();
        }
    }

    public static List<Role> getAllRolesDesigner() {
        try {
            readLock.lock();
            List<Role> roles = new Vector<Role>();
            for (RoleUserBinding roleuser : orguserRoles) {
                roles.add(roleuser.getOrgRole());
            }
            return roles;

        } finally {
            readLock.unlock();
        }
    }

    public static Map<String, Role> getRolesByIds(List<String> ids) {
        Map<String, Role> roles = new HashMap<String, Role>();
        for (String id : ids) {
            if (!roles.containsKey(id)) {
                roles.put(id, getRole(id));
            }
        }
        return roles;
    }

    public static List<String> getOrgUnitId(Set<OrganizationalUnit> OrgUnit) {
        //Shuwair
        try {
            readLock.lock();
            List<String> unitsid = new Vector<String>();
            for (OrganizationalUnit units : OrgUnit) {
                unitsid.add(units.getOrgUnitId());
            }
            return unitsid;

        } finally {
            readLock.unlock();
        }
    }

    public static Set<OrganizationalUnit> getUserOrgUnits(String userid) {
        Set<OrganizationalUnit> orgUnits = new HashSet<OrganizationalUnit>();
        for (RoleUserBinding roleuser : orguserRoles) {
            if (userid.equalsIgnoreCase(roleuser.getSecUser().getLoginId()))
                orgUnits.add(roleuser.getOrgUnit());
        }
        return orgUnits;
    }

    public static List<Role> getAllRolesByOrg(String userid, String orgId) {
        try {
            readLock.lock();
            List<Role> roles = new Vector<Role>();
            for (RoleUserBinding roleuser : orguserRoles) {
                if (userid.equalsIgnoreCase(roleuser.getSecUser().getLoginId())
                        && roleuser.getOrgUnitId().equalsIgnoreCase(orgId))
                    roles.add(roleuser.getOrgRole());
            }
            return roles;

        } finally {
            readLock.unlock();
        }
    }

    public static Role getRole(String roleid) {
        try {
            readLock.lock();
            for (RoleUserBinding roleuser : orguserRoles) {
                if (roleid.equalsIgnoreCase(roleuser.getRoleId()))
                    return roleuser.getOrgRole();
            }
            return null;

        } finally {
            readLock.unlock();
        }
    }

    public static boolean isRoleAssigned(String userid, String roleid) {
        try {
            readLock.lock();
            for (RoleUserBinding roleuser : orguserRoles) {
                if (roleid.equalsIgnoreCase(roleuser.getRoleId())) {
                    User usr = roleuser.getSecUser();
                    if (usr.getLoginId().equalsIgnoreCase(userid))
                        return true;
                }
            }
            return false;

        } finally {
            readLock.unlock();
        }

    }

    public static boolean isUserInOrg(String orgid, String userid) {
        try {
            readLock.lock();
            for (RoleUserBinding roleuser : orguserRoles) {

                if (roleuser.getOrgUnitId().equalsIgnoreCase(orgid)) {
                    if (roleuser.getSecUser().getLoginId().equalsIgnoreCase(
                            userid))
                        return true;
                }
            }
            return false;

        } finally {
            readLock.unlock();
        }
    }

    public static boolean isUserInOrg(String orgid, String userid, String roleid) {
        try {
            readLock.lock();
            for (RoleUserBinding roleuser : orguserRoles) {

                if (roleuser.getOrgUnitId().equalsIgnoreCase(orgid)) {
                    if (roleuser.getRoleId().equalsIgnoreCase(roleid)) {
                        if (roleuser.getSecUser().getLoginId()
                                .equalsIgnoreCase(userid))
                            return true;
                    }
                }

            }
            return false;

        } finally {
            readLock.unlock();
        }
    }

    public static List<User> getUsers(String orgid, String roleid) {
        return getUsersOfRoleInAnyOrgUnit(roleid
                , Arrays.asList(orgid));
    }

    public static boolean IsUserValidForRoleInAnyOrgUnit(String userId, String roleid, List<String> orgids) {
        final Wrapper<Boolean> found = new Wrapper<Boolean>(false);
        processUsersOfRoleInAnyOrgUnit(roleid, orgids, new Arg1<Boolean, User>() {
            public Boolean call(User user) {
                found.wrap(true);
                return false;
            }
        });

        return found.unwrap();
    }

    public static List<User> getUsersOfRoleInAnyOrgUnit(String roleid, List<String> orgids) {
        final List<User> users = new ArrayList<User>();
        processUsersOfRoleInAnyOrgUnit(roleid, orgids, new Arg1<Boolean, User>() {
            public Boolean call(User user) {
                users.add(user);
                return true;
            }
        });

        return users;
    }

    private static void processUsersOfRoleInAnyOrgUnit(String roleid, List<String> orgids, FunctionDelegate.Arg1<Boolean, User> processor) {

        try {
            readLock.lock();
            for (RoleUserBinding roleuser : orguserRoles) {

                if (CollectionHelper.find(roleuser.getOrgUnitId(), orgids, true)) {
                    if (roleuser.getRoleId().equalsIgnoreCase(roleid)) {
                        if (!processor.call(roleuser.getSecUser())) {
                            break;
                        }
                    }
                }
            }
        } finally {
            readLock.unlock();
        }

    }

    public static List<User> getOrgTypeUsers(String orgtype, String roleid) {
        try {
            readLock.lock();

            // getting all orgs of specified orgtype
            List<OrganizationalUnit> orgs = new ArrayList<OrganizationalUnit>();
            Iterator itr = orgUnits.keySet().iterator();
            while (itr.hasNext()) {
                OrganizationalUnit unit = orgUnits.get(itr.next());
                if (unit.getOrgUnitType().getUnitTypeId().equalsIgnoreCase(
                        orgtype)) {
                    orgs.add(unit);
                }
            }
            List<User> users = new ArrayList<User>();
            for (OrganizationalUnit or : orgs) {
                String id = or.getOrgUnitId();
                // Getting RoleUserBindings for org
                List<RoleUserBinding> bindinglist = getUserRolesbyOrgUnit(id);
                for (RoleUserBinding rb : bindinglist) {
                    // Filterting on roleid
                    if (rb.getRoleId().equalsIgnoreCase(roleid))
                        users.add(rb.getSecUser());
                }
            }
            return users;
        } finally {
            readLock.unlock();
        }

    }

    public static List<RoleUserBinding> getUserRolesbyOrgUnit(String id) {
        try {
            readLock.lock();
            List<RoleUserBinding> rolebindinglist = new ArrayList<RoleUserBinding>();
            for (RoleUserBinding roleuser : orguserRoles) {
                if (roleuser.getOrgUnitId().equalsIgnoreCase(id))
                    rolebindinglist.add(roleuser);
            }
            return rolebindinglist;

        } finally {
            readLock.unlock();
        }
    }


    public static List<RoleUserBinding> getUserRoles() {
        try {
            readLock.lock();
            List<RoleUserBinding> rolebindinglist = new ArrayList<RoleUserBinding>();
            for (RoleUserBinding roleuser : orguserRoles) {

                rolebindinglist.add(roleuser);
            }
            return rolebindinglist;

        } finally {
            readLock.unlock();
        }

    }

    public static List<OrganizationalUnit> getUserOrgUnits(String userid,
                                                           String roleid) {
        try {
            readLock.lock();
            List<OrganizationalUnit> userOrgs = new ArrayList<OrganizationalUnit>(
                    0);
            for (RoleUserBinding roleuser : orguserRoles) {
                if (roleuser.getLoginId().equalsIgnoreCase(userid)
                        && roleuser.getRoleId().equalsIgnoreCase(roleid))
                    userOrgs.add(roleuser.getOrgUnit());
            }
            return userOrgs;

        } finally {
            readLock.unlock();
        }

    }

    public static List<String> getAllRoles() {
        try {
            readLock.lock();
            List<String> roles = new ArrayList<String>(0);
            for (RoleUserBinding binding : orguserRoles) {
                if (!roles.contains(binding.getRoleId()))
                    roles.add(binding.getRoleId());
            }
            return roles;

        } finally {
            readLock.unlock();
        }
    }


    //Shuwair
    public static List<String> getChildRoles(String parentRoleId) {
        try {
            readLock.lock();
            List<String> parenRoles = new ArrayList<String>(0);
            for (Role role : roles.values()) {
                if (parentRoleId.equals(role.getParentRole().getRoleId()))
                    if (!parenRoles.contains(role.getParentRole() == null ? "" : role.getParentRole().getRoleId()))
                        parenRoles.add(role.getRoleId());
            }
            return parenRoles;

        } finally {
            readLock.unlock();
        }
    }

    //Shuwair
    public static List<String> getChildRoles(List<String> parentRoleIdsList) {
        try {
            readLock.lock();
            List<String> parenRoles = new ArrayList<String>(0);
            for (String parentRoleId : parentRoleIdsList) {
                for (Role role : roles.values()) {
                    if (parentRoleId.equals(role.getParentRole() == null ? "" : role.getParentRole().getRoleId()))
                        if (!parenRoles.contains(role.getRoleId()))
                            parenRoles.add(role.getRoleId());
                }
            }
            return parenRoles;

        } finally {
            readLock.unlock();
        }
    }


    //added by Anam Siddiqui
    public static List<OrganizationalUnit> getAllOrgUnitsonRoles(String role) {
        try {
            readLock.lock();
            List<OrganizationalUnit> orgunits = new ArrayList<OrganizationalUnit>(0);
            List<RoleUserBinding> result = null;

            DataBroker broker;
            broker = DataRepository.getBroker("com");
            Search query = new Search();
            query.addFrom(RoleUserBinding.class);
            query.addCriterion(Criterion.equal("roleId", role));
            result = broker.find(query);

            if (result != null && result.size() > 0) {
                orgunits.add(result.get(0).getOrgUnit());
            }


            return orgunits;

        } finally {
            readLock.unlock();
        }

    }

    public static List<String> getAllOrgUnits(String role) {
        try {
            readLock.lock();
            List<String> orgunits = new ArrayList<String>(0);
            for (RoleUserBinding binding : orguserRoles) {

                if (binding.getRoleId().equalsIgnoreCase(role))
                    if (!orgunits.contains(binding.getOrgUnitId()))
                        orgunits.add(binding.getOrgUnitId());

            }
            return orgunits;

        } finally {
            readLock.unlock();
        }

    }

    public static List<Role> getParentRoles(String roleId) {
        try {
            readLock.lock();
            List<Role> parentRole = new ArrayList<Role>(0);
            Role child = getRole(roleId);
            while (child.getParentRole() != null) {
                parentRole.add(child.getParentRole());
                child = child.getParentRole();
            }
            return parentRole;

        } finally {
            readLock.unlock();
        }

    }

    public static Role getParentRole(String roleId) {
        try {
            readLock.lock();
            return getRole(roleId).getParentRole();
        } finally {
            readLock.unlock();
        }
    }

    public static List<OrganizationalUnit> getOrgUnitByType(
            OrganizationalUnitType type) {
        try {
            readLock.lock();
            List<OrganizationalUnit> result = new ArrayList<OrganizationalUnit>(
                    0);
            for (OrganizationalUnit unit : orgUnits.values()) {
                if (((OrganizationalUnitType) unit.getOrgUnitType()).getUnitTypeId().equalsIgnoreCase(type.getUnitTypeId()))
                    result.add(unit);
            }
            return result;

        } finally {
            readLock.unlock();
        }

    }

    //Added By Anam Siddiqui .. used to populate Org Unit combo alphabetically on the designer page in admin module
    public static List<OrganizationalUnit> getOrgUnitByType(
            String type) {
        try {
            readLock.lock();
            List<OrganizationalUnit> result = new ArrayList<OrganizationalUnit>(
                    0);

            for (OrganizationalUnit unit : orgUnits.values()) {
                if (((OrganizationalUnitType) unit.getOrgUnitType()).getUnitTypeId().equalsIgnoreCase(type))
                    result.add(unit);
            }
            Collections.sort(result, new Comparator<OrganizationalUnit>() {
                public int compare(OrganizationalUnit o1, OrganizationalUnit o2) {
                    return o1.getOrgNamePrm().compareToIgnoreCase(o2.getOrgNamePrm());
                }
            });
            return result;

        } finally {
            readLock.unlock();
        }

    }


    public static List<OrganizationalUnit> getAllOrgUnits() {
        try {
            readLock.lock();
            Vector<OrganizationalUnit> orgUnitList = new Vector<OrganizationalUnit>(
                    orgUnits.values());
            Collections.sort(orgUnitList);
            return orgUnitList;
        } finally {
            readLock.unlock();
        }

    }

    public static List<User> getOrgAllUsers(String orgId) {
        try {
            readLock.lock();
            List<User> allusers = new ArrayList<User>();
            List<RoleUserBinding> bindings = getUserRolesbyOrgUnit(orgId);
            for (RoleUserBinding bindObj : bindings) {
                User user = bindObj.getSecUser();
                if (user != null)
                    if (!allusers.contains(user))
                        allusers.add(bindObj.getSecUser());
            }
            Collections.sort(allusers);
            return allusers;

        } finally {
            readLock.unlock();
        }

    }

    public static Role getRoleByName(String roleName) {
        try {
            readLock.lock();
            for (RoleUserBinding roleuser : orguserRoles) {
                if (roleName.equalsIgnoreCase(roleuser.getOrgRole()
                        .getRolePrimaryName())
                        || roleName.equalsIgnoreCase(roleuser.getOrgRole()
                        .getRoleSecondaryName()))
                    return roleuser.getOrgRole();
            }
            return null;

        } finally {
            readLock.unlock();
        }
    }

    public static void addOrganizationalUnit(OrganizationalUnit orgUnit) {
        try {
            writeLock.lock();
            if (orgUnit != null
                    && StringHelper.isNotEmpty(orgUnit.getOrgUnitId())) {
                if (!orgUnits.containsKey(orgUnit.getOrgUnitId()))
                    orgUnits.put(orgUnit.getOrgUnitId(), orgUnit);
            }
        } finally {
            writeLock.unlock();
        }

    }

    public static void addRoleUserBinding(RoleUserBinding binding) {
        try {
            writeLock.lock();
            if (binding != null && StringHelper.isNotEmpty(binding.getRoleId()))
                if (!orguserRoles.contains(binding))
                    orguserRoles.add(binding);
        } finally {
            writeLock.unlock();
        }
    }

    public static Map<String, Role> getRoles() {
        return roles;
    }

    public static void setRoles(Map<String, Role> roles) {
        OrganizationManager.roles = roles;
    }

    public static Map<String, Role> getRoleMap() {
        return roles;
    }

    public static List<Role> getAllOrgRoles() {

        try {
            readLock.lock();
            List<Role> tempRoles = new Vector<Role>();


            for (Map.Entry<String, Role> entry : roles.entrySet()) {

                tempRoles.add(entry.getValue());
            }

            return tempRoles;

        } finally {
            readLock.unlock();
        }


    }

    // This method is added to perform search based on entityId and product code
    // to handle complaint changes.
    public static OrganizationalUnit getEntityOrgUnitProduct(String entityId, String productCode) {
        DataBroker broker = DataRepository.getBroker(EntityOrgMapping.class.toString());
        Search query = new Search(EntityOrgMapping.class);
        query.setCanRepresent(true);
        query.setBatchSize(1);
        query.addCriterion(SimpleCriterion.getSimpleCriterion(OperatorType.Eq,
                query.getFirstFrom().createColumn("PRODUCT_CODE"), productCode));
        query.addCriterion(SimpleCriterion.getSimpleCriterion(OperatorType.Eq,
                query.getFirstFrom().createColumn("META_ENT_ID"), entityId));

        List<EntityOrgMapping> entityOrgMappingList = broker.find(query);
        if (entityOrgMappingList.size() > 0) {
            return entityOrgMappingList.get(0).getId().getOrgUnit();
        }

        return null;
    }

    public static OrganizationalUnit getEntityOrgUnit(String entityId) {
        DataBroker broker = DataRepository.getBroker(EntityOrgMapping.class.toString());
        Search query = new Search(EntityOrgMapping.class);
        query.setCanRepresent(true);
        query.setBatchSize(1);
        query.addCriterion(Criterion.equal("id.metaEntity.id", entityId));

        // Entity Org Unit Mapping fix
        //query.addCriterion(Criterion.equal("metaEntity", entityId));
        List<EntityOrgMapping> entityOrgMappingList = broker.find(query);
        if (entityOrgMappingList.size() > 0) {
            return entityOrgMappingList.get(0).getId().getOrgUnit();
        }

        return null;
    }


    public static List<User> getUsersByRole(String roleid) {
        final List<User> users = new ArrayList<User>();
        for (RoleUserBinding roleuser : orguserRoles) {
            if (roleuser.getRoleId().equalsIgnoreCase(roleid)) {
                users.add(roleuser.getSecUser());
            }
        }
        return users;
    }

    //Added By Anam Siddiqui .. used to populate Org Unit combo alphabetically on the designer page in admin module
    public static List<OrganizationalUnit> getOrgUnitByParentId(String parentId) {
        List<OrganizationalUnit> result = new ArrayList<OrganizationalUnit>(0);
        for (OrganizationalUnit unit : orgUnits.values()) {
            if (unit.getParentOrgUnit() != null) {
                if ((unit.getParentOrgUnit().getOrgUnitId())
                        .equalsIgnoreCase(parentId))
                    result.add(unit);
            }
        }

        return result;

    }

    // org Unit with Other code 1
    public static String getOrgUnitByOtherCode1(String otherCode1) {
        //List<OrganizationalUnit> result = new ArrayList<OrganizationalUnit>(0);
        String orgUnitId = "";
        for (OrganizationalUnit unit : orgUnits.values()) {
            if (unit.getOtherCode1() != null && unit.getOtherCode1().equalsIgnoreCase(otherCode1)) {
                orgUnitId = unit.getOrgUnitId();
                break;
            }
        }

        return orgUnitId;

    }

    // org unit with other code 2
    public static String getOrgUnitByOtherCode2(String otherCode2) {
        //List<OrganizationalUnit> result = new ArrayList<OrganizationalUnit>(0);
        String orgUnitId = null;
        for (OrganizationalUnit unit : orgUnits.values()) {
            if (unit.getOtherCode2() != null && unit.getOtherCode2().equalsIgnoreCase(otherCode2)) {
                orgUnitId = unit.getOrgUnitId();
                break;
            }
        }
        return orgUnitId;
    }

    /**
     * Below methods have been added for HBL LMS module
     * 9/6/2017
     * Anam.Siddiqui
     */
    public static List<User> getOrgRoleUsers(String orgUnit, String roleid) {
        try {
            readLock.lock();
            List<User> users = new ArrayList<User>();
            List<RoleUserBinding> bindinglist = getUserRolesbyOrgUnit(orgUnit);
            for (RoleUserBinding rb : bindinglist) {
                // Filterting on roleid
                if (rb.getRoleId().equalsIgnoreCase(roleid))
                    users.add(rb.getSecUser());
            }
            Collections.sort(users, new Comparator<User>() {
                public int compare(User u1, User u2) {
                    return u1.getLoginId().compareToIgnoreCase(u2.getLoginId());
                }
            });
            return users;
        } finally {
            readLock.unlock();
        }

    }


    //Anam.Siddiqui
    /**
     * Adding provision org role category
     */
    private static List<OrgRoleCategory> orgRolesCategory = new ArrayList<OrgRoleCategory>(
            0);

    public static List<String> getUserOrgCategory(String roleId) {
        try {
            readLock.lock();
            List<String> usersCategory = new Vector<String>();
            for (OrgRoleCategory roleuserCategory : orgRolesCategory) {
                if (roleuserCategory.getRoleId().equalsIgnoreCase(roleId))
                    usersCategory.add(roleuserCategory.getTreenodeId());
            }
            return usersCategory;

        } finally {
            readLock.unlock();
        }
    }


    public static List<String> getCategoryRole(String categoryId) {
        try {
            readLock.lock();
            List<String> categoryRole = new ArrayList<String>();
            for (OrgRoleCategory roleuserCategory : orgRolesCategory) {
                if (roleuserCategory.getTreenodeId().equalsIgnoreCase(categoryId)) {
                    if (StringHelper.isNotEmpty(roleuserCategory.getRoleId()))
                        categoryRole.add(roleuserCategory.getRoleId());
                }
            }
            return categoryRole;

        } finally {
            readLock.unlock();
        }
    }

    public static List<String> getCategoryOrgUnit(String categoryId) {
        try {
            readLock.lock();
            List<String> categoryOrgUnit = new ArrayList<String>();
            for (OrgRoleCategory roleuserCategory : orgRolesCategory) {
                if (roleuserCategory.getTreenodeId().equalsIgnoreCase(categoryId)) {
                    if (StringHelper.isNotEmpty(roleuserCategory.getOrgUnitId()))
                        categoryOrgUnit.add(roleuserCategory.getOrgUnitId());
                }
            }
            return categoryOrgUnit;

        } finally {
            readLock.unlock();
        }
    }


    public static List<String> getCategoryByRole(String roleId) {
        try {
            readLock.lock();
            List<String> categoryOrgUnit = new ArrayList<String>();
            for (OrgRoleCategory roleuserCategory : orgRolesCategory) {
                if (roleuserCategory.getRoleId().equalsIgnoreCase(roleId)) {
                    if (StringHelper.isNotEmpty(roleuserCategory.getRoleId()))
                        categoryOrgUnit.add(roleuserCategory.getTreenodeId());
                }
            }
            return categoryOrgUnit;

        } finally {
            readLock.unlock();
        }
    }
    //AnamChanges ends here...
}