package com.avanza.workflow.configuration.action;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.Expression;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;
import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.WorkflowContextLookup;


public class ScriptAction implements Action {

    public Object act(ProcessInstance process, ActionBinding action, Object context) {
        ActionExecutionResult result = new ActionExecutionResult();
        ApplicationContext.getContext().add(WorkflowContextLookup.PROCESS_INSTANCE_PARAM, process);
        try {
            String script = action.getActionScript();
            if (script != null && script.length() > 0) {
                ExpressionParser expParser = new ExpressionParser(script, void.class, FunctionCatalogManager.getInstance().getFunctionCatalog(
                        ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction, ExpressionClass.WorkFlowFunctions));
                Expression retVal = expParser.parse();
                ExpressionEvaluator eval = ExpressionEvaluator.getInstance();
                eval.evaluate(retVal);
            }
        } catch (Exception e) {
            if (action.getCritical()) result.setChangePossible(false);
            if (action.getSerial()) result.setChangePossible(true);
            result.addError(action, e, false);
            return result;
        }
        result.setChangePossible(true);
        return result;
    }
}
