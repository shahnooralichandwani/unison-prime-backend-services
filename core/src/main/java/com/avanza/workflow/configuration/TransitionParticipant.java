package com.avanza.workflow.configuration;

import java.io.Serializable;
import java.util.Date;

import com.avanza.core.data.DbObject;

/**
 * TransitionParticipant entity.
 *
 * @author MyEclipse Persistence Tools
 */

public class TransitionParticipant extends DbObject implements Serializable {

    // Fields

    private TransitionParticipantId id;
    private String activityUserFilter;
	/*private Transition transition;
	private StateParticipant stateParticipant; */


    // Property accessors

    public TransitionParticipantId getId() {
        return this.id;
    }

    public void setId(TransitionParticipantId id) {
        this.id = id;
    }

    public String getActivityUserFilter() {
        return this.activityUserFilter;
    }

    public void setActivityUserFilter(String activityUserFilter) {
        this.activityUserFilter = activityUserFilter;
    }

	/*public Transition getTransition() {
		return transition;
	}

	public void setTransition(Transition transition) {
		this.transition = transition;
	}

	public StateParticipant getStateParticipant() {
		return stateParticipant;
	}

	public void setStateParticipant(StateParticipant stateParticipant) {
		this.stateParticipant = stateParticipant;
	}*/

}