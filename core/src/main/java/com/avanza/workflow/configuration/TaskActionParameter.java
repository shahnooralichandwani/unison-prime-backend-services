/**
 *
 */
package com.avanza.workflow.configuration;

import com.avanza.core.data.DbObject;

/**
 * @author rehan.ahmed
 *
 */
public class TaskActionParameter extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 5120342537207421808L;

    private int taskActionParamId;
    private int taskActionId;
    private String paramKey;
    private String paramValue;
    private String documentId;

    public int getTaskActionParamId() {
        return taskActionParamId;
    }

    public void setTaskActionParamId(int taskActionParamId) {
        this.taskActionParamId = taskActionParamId;
    }

    public int getTaskActionId() {
        return taskActionId;
    }

    public void setTaskActionId(int taskActionId) {
        this.taskActionId = taskActionId;
    }

    public String getParamKey() {
        return paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
