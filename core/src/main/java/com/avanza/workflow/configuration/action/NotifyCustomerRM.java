package com.avanza.workflow.configuration.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;
import com.avanza.workflow.execution.WorkflowContextLookup;

public class NotifyCustomerRM extends TaskAllocatorAction {

    private static final Logger logger = Logger.getLogger(NotifyCustomerRM.class);

    @SuppressWarnings({"deprecation", "unchecked"})
    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {
        String actionParams = action.getActionParam();
        HashMap<String, String> parsedParams = parseActionParams(actionParams);
        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext ctx = processInst.getContext();
        String rim = (String) ctx.getAttribute("CUST_RELATION_NUM");
        MetaEntity entity = MetaDataRegistry.getEntityBySystemName(WorkflowContextLookup.CUSTOMER_ENTITY);
        DataBroker broker = DataRepository.getBroker(entity.getSystemName());
        Search cust = new Search();
        String colName = InboundLoader.getInstance().getCustomerColumnName("RM_ID");
        String colRim = InboundLoader.getInstance().getCustomerColumnName("RIM");
        cust.addColumn(entity.getAttribute(colName).getTableColumn());
        cust.addFrom(entity.getSystemName());
        cust.addCriterion(Criterion.equal(colRim, rim));
        List<DataObject> obj = broker.find(cust);
        String emailTemplate = parsedParams.get(InboundLoader.getInstance().getAckChannel("Email"));
        if (StringHelper.isNotEmpty(emailTemplate)) {
            MetaEntity instanceEntity = MetaDataRegistry.getMetaEntity(ctx.getDocumentId());
            MetaEntity parentEntity = MetaDataRegistry.getNonAbstractParentEntity(instanceEntity);
            HashMap map = new HashMap();
            map.put(parentEntity.getSystemName().replace('.', '_'), ctx.getValues());
            map.put("CurrentDate", new Date());
            String processedTemplate = ApplicationLoader.getTemplateConfig().processTemplate(emailTemplate + "_content", map);
            String processedSubject = ApplicationLoader.getTemplateConfig().processTemplate(emailTemplate + "_subject", map);
            if (obj.size() > 0) {
                DataObject customer = obj.get(0);
                String rmId = (String) customer.getValue(colName);
                User user = SecurityManager.getUser(rmId);
                String RmEmail = user.getEmailUrl();
                if (StringHelper.isNotEmpty(RmEmail)) {
                    System.out.println("------------------ RM ID: " + rmId + "------------------");
                    System.out.println("------------------------ Sending Email to RM " + RmEmail + " -----------------------------");
                    NotificationSender.sendEmail(processedSubject + " for RM", processedTemplate, RmEmail, null, InboundLoader.getInstance().getAckChannel("Email"));
                }
            }
        }
        return result;
    }

    private HashMap<String, String> parseActionParams(String actionParams) {
        //CBD-SMTP-CHANNEL=00006;CBD-SMS-CHANNEL=00006
        if (StringHelper.isEmpty(actionParams))
            return null;
        HashMap<String, String> parsedParams = new HashMap<String, String>();
        StringTokenizer st = new StringTokenizer(actionParams, ";");
        while (st.hasMoreTokens()) {
            String token = st.nextToken().trim();
            if (StringHelper.isNotEmpty(token)) {
                StringTokenizer st1 = new StringTokenizer(token, "=");
                if (st1.countTokens() < 2) {
                    logger.logError(String.format("Error Parsing Params String (%1$s)", actionParams));
                    return parsedParams;
                }
                while (st1.hasMoreTokens()) {
                    String innerToken1 = st1.nextToken();
                    String innerToken2 = st1.nextToken();
                    parsedParams.put(innerToken1, innerToken2);
                }
            }
        }
        return parsedParams;
    }
}