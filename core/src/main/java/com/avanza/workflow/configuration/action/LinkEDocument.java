package com.avanza.workflow.configuration.action;

import java.util.ArrayList;
import java.util.List;

import com.avanza.core.FunctionDelegate.Arg1;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.JDBCDataBroker;
import com.avanza.core.inbound.CustomerAreaController;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.DataObjectCollection;
import com.avanza.core.security.User;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.CollectionHelper;
import com.avanza.core.util.Logger;
import com.avanza.core.web.config.WebContext;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

public class LinkEDocument extends TaskAllocatorAction {
    /*
     *	 It will Link the Document with Email
     *
     *
     */
    private static final Logger logger = Logger
            .getLogger(LinkEDocument.class);

    private final String LINK_EDOC_ENT = "0000000143"; // LINK Document with Email

    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {
        logger.logInfo("LinkEDocument Called...");


        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext ctx = processInst.getContext();
        WebContext webCtx = ApplicationContext.getContext().get(
                WebContext.class);
        Object edocId = webCtx.getAttribute("EDOCUMENT_ID");

        if (edocId != null) {
            logger.logInfo("Linking Email..." + edocId);
            MetaEntity entity = MetaDataRegistry.getMetaEntity(LINK_EDOC_ENT);
            DataObject recordLog = new DataObject(entity);
            DataBroker broker = DataRepository.getBroker(entity.getSystemName());
            recordLog.setAttribute("DOCUMENT_ID", processInst.getInstanceId());
            recordLog.setAttribute("EDOC_ID", edocId);
            broker.add(recordLog);

            //-------- Removing from session
            webCtx.removeAttribute("EDOCUMENT_ID");
        }


        result.setChangePossible(true);
        return result;
    }
}