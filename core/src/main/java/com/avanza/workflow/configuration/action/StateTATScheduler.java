package com.avanza.workflow.configuration.action;

import com.avanza.workflow.execution.Action;

import java.util.Calendar;
import java.util.Date;

import com.avanza.core.calendar.CalendarCatalog;
import com.avanza.core.scheduler.JobID;
import com.avanza.core.scheduler.SchedulerCatalog;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.tat.StateTATExpiryJob;

/**
 * @author yasir
 */

public class StateTATScheduler implements Action {

    public Object act(ProcessInstance processInstance, ActionBinding action, Object context) {
        Date entryTime = new Date();
        System.out.println("StateTATScheduler Action Fired");
        String uid = processInstance.getContext().getDocumentId() + "|" + processInstance.getInstanceId() + "|"
                + processInstance.getCurrentState().getStateCode() + "|" + entryTime.toLocaleString();
        long tatTime = processInstance.getCurrentState().getTatInMinutes();
        long Adjustedtime = CalendarCatalog.getDefaultCalendar().getAdjustedTime(entryTime, tatTime);
        Date expiryTime = new Date(Adjustedtime);

        System.out.println("Registering JOB [ STATE TAT ] [Fire Time : " + expiryTime.toLocaleString() + "] for "
                + uid.toString());
        SchedulerCatalog.getScheduler().scheduleJob(new JobID(uid), "JobGroupState", StateTATExpiryJob.class.getName(),
                null, "TriggerGroupState", expiryTime);
        Calendar cal = Calendar.getInstance();
        cal.setTime(expiryTime);
        cal.add(Calendar.MINUTE, -30);
        return "";
    }

}
