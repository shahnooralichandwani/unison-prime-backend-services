package com.avanza.workflow.configuration.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.avanza.core.security.User;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

public class CurrentActorCalculator extends TaskAllocatorAction {

    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {
        ActionExecutionResult result = new ActionExecutionResult();
        try {
            ProcessEntityBinding binding = processInst.getBinding();
            Set<StateParticipant> StateParticipant = binding.getStateParticipantList();
            String currentState = processInst.getCurrentState().getStateCode();
            List<String> calculatedActors = new ArrayList<String>();
            String RoleId = "";
            String StateId = "";
            for (StateParticipant participant : StateParticipant) {
                RoleId = participant.getRoleId();
                StateId = participant.getStateCode();
                if (currentState.equalsIgnoreCase(StateId)) {
                    if (RoleId != null && RoleId.length() > 0) {
                        RoleId = participant.getRoleId();
                        List<User> users = OrganizationManager.getAllUsers(RoleId);
                        for (User user : users) {
                            if (!calculatedActors.contains(user.getLoginId()))
                                calculatedActors.add(user.getLoginId());
                        }
                    }
                }
            }
            processInst.setCalculatedActor(calculatedActors);
            processInst.setCurrentActor(calculatedActors);
            result.setChangePossible(true);
        } catch (Exception e) {
        }
        return result;
    }
	/*@SuppressWarnings("unchecked")
	public String CurrentRole(String processAllID) {
		JDBCDataBroker broker = (JDBCDataBroker) DataRepository
				.getBroker("JDBC.Unison");
		String query = "Select [ROLE_ID] from WF_PROCESS_PARTICIPANT "
				+ "where [PROCESS_ALLOC_ID] = '" + processAllID
				+ "'and [IS_SUPERVISOR]='True' ";
		List result1 = new ArrayList<Object>();
		result1 = broker.executeQuery(query);
		Object[] resultObj1 = null;
		String RoleID = "";

		if (result1 != null && result1.size() > 1) {
			resultObj1 = (Object[]) result1.get(1);
			RoleID = (resultObj1[0] == null ? "" : resultObj1[0].toString());

		}// first if ends
		else {
			String query2 = "Select [ROLE_ID] from WF_PROCESS_PARTICIPANT "
					+ "where [PROCESS_ALLOC_ID] = '"
					+ processAllID
					+ "'and [SELECT_CRITERIA]='True' and [IS_SUPERVISOR]='False' ";
			List result2 = new ArrayList<Object>();
			result2 = broker.executeQuery(query2);
			Object[] resultObj2 = null;
			if (result2 != null && result2.size() > 1) {
				resultObj2 = (Object[]) result2.get(1);
				RoleID = (resultObj2[0] == null ? "" : resultObj2[0].toString());

			}
		}
		return RoleID;
	}*/
}
