package com.avanza.workflow.configuration.action;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import com.avanza.workflow.execution.ConfirmationAction;
import com.avanza.workflow.execution.ConfirmationActionResult;
import com.avanza.workflow.execution.ProcessInstance;

public class ExampleConfirmationAction extends ConfirmationAction {

    public ConfirmationActionResult process(ProcessInstance processInst, ActionBinding action, Object context) {

        ConfirmationActionResult result = new ConfirmationActionResult();
        result.setShowConfirmation(Boolean.TRUE);
        Map<String, String> map = parseParams(action.getActionParam());
        if (map != null) {
            if ("true".equalsIgnoreCase(map.get("showConfirmation"))) {
                result.setMessage(map.get("message"));
                result.setSuccess(Boolean.TRUE);
            } else {
                result.setSuccess(Boolean.TRUE);
            }
        }
        return result;
    }

    public Map<String, String> parseParams(String actionParams) {
        if (actionParams == null)
            return null;
        Map<String, String> map = new HashMap<String, String>();
        StringTokenizer st = new StringTokenizer(actionParams, ";");
        while (st.hasMoreElements()) {
            String s = (String) st.nextElement();
            map.put(s.split("=")[0], s.split("=")[1]);
        }
        return map;
    }
}
