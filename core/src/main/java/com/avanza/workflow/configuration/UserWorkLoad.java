package com.avanza.workflow.configuration;

import com.avanza.core.data.DbObject;

public class UserWorkLoad extends DbObject {

    private String loginId;
    private String mainMetaEntity;
    private String subMetaEntity;
    private int assignedDocuments;

    public int getAssignedDocuments() {
        return assignedDocuments;
    }

    public void setAssignedDocuments(int assignedDocuments) {
        this.assignedDocuments = assignedDocuments;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getMainMetaEntity() {
        return mainMetaEntity;
    }

    public void setMainMetaEntity(String mainMetaEntity) {
        this.mainMetaEntity = mainMetaEntity;
    }

    public String getSubMetaEntity() {
        return subMetaEntity;
    }

    public void setSubMetaEntity(String subMetaEntity) {
        this.subMetaEntity = subMetaEntity;
    }
}
