package com.avanza.workflow.configuration.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.security.User;
import com.avanza.workflow.configuration.AssignmentType;
import com.avanza.workflow.configuration.ProcessParticipant;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.Transition;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.OrganizationalUnit;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;
import com.avanza.workflow.execution.TaskContext;
import com.avanza.workflow.execution.TaskLoadManager;

public class OrgUnitTaskAllocation extends TaskAllocatorAction {

    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {
        ActionExecutionResult result = new ActionExecutionResult();
        TaskContext ctx = (TaskContext) context;
        State stat = processInst.getCurrentState();
        Set<StateParticipant> stateParticipants = processInst.getStateParticipants(processInst.getContext().getDocumentId(), stat.getStateCode());
        List<String> currentActors = new ArrayList<String>();
        Transition transition = stat.getTransitionbyId(ctx.getTaskCode());
        if (transition != null) {
            if (AssignmentType.fromName(transition.getAssignmentType()) == AssignmentType.AutoAssign) {

                for (StateParticipant participant : stateParticipants) {
                    String roleid = participant.getRoleId();
                    List<User> users = OrganizationManager.getAllUsers(roleid);
                    for (User user : users) {
                        List<OrganizationalUnit> unitList = OrganizationManager.getUserOrgUnits(user.getLoginId(), roleid);
                        for (OrganizationalUnit unit : unitList) {
                            if (unit.getOrgUnitId().equalsIgnoreCase(processInst.getContext().getCurrentOrgUnit())) {
                                if (!currentActors.contains(user.getLoginId()))
                                    currentActors.add(user.getLoginId());
                            }
                        }
                    }
                }
            }
        } else {
            Set<ProcessParticipant> procParticipants = processInst.getProcessParticipants(processInst.getContext().getDocumentId());
            List<StateParticipant> filteredParticipants = filterParticipantsList(procParticipants, stateParticipants);
            for (StateParticipant participant : filteredParticipants) {
                String roleid = participant.getRoleId();
                List<User> users = OrganizationManager.getAllUsers(roleid);
                for (User user : users) {
                    List<OrganizationalUnit> unitList = OrganizationManager.getUserOrgUnits(user.getLoginId(), roleid);
                    for (OrganizationalUnit unit : unitList) {
                        if (unit.getOrgUnitId().equalsIgnoreCase(processInst.getContext().getCurrentOrgUnit())) {
                            if (!currentActors.contains(user.getLoginId()))
                                currentActors.add(user.getLoginId());
                        }
                    }
                }
            }
        }

        if (currentActors.size() > 0) {
            if (currentActors.size() == 1)
                TaskLoadManager.incrementUserWorkLoad(currentActors.get(0), MetaDataRegistry.getNonAbstractParentEntity(MetaDataRegistry.getMetaEntity(processInst.getContext().getDocumentId()))
                        .getId());
            processInst.setCurrentActor(currentActors);
            processInst.setCalculatedActor(currentActors);
        } else {
            processInst.setSupervisorAsActors(processInst.getContext().getCurrentOrgUnit());
        }
        result.setChangePossible(true);
        return result;
    }

    private List<StateParticipant> filterParticipantsList(Set<ProcessParticipant> procParticipants, Set<StateParticipant> participants) {
        List<StateParticipant> finalParticipant = new ArrayList<StateParticipant>();
        for (ProcessParticipant procPart : procParticipants) {
            for (StateParticipant statPart : participants) {
                if (statPart.getRoleId().equalsIgnoreCase(procPart.getRoleId())) {
                    if (!finalParticipant.contains(statPart)) {
                        finalParticipant.add(statPart);
                    }
                }
            }
        }
        return finalParticipant;
    }
}