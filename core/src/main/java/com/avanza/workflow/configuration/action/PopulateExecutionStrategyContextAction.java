/**
 *
 */
package com.avanza.workflow.configuration.action;

import com.avanza.core.util.Logger;
import com.avanza.workflow.escalation.EscalationManager;
import com.avanza.workflow.escalation.ExecutionStrategyContext;
import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;

public class PopulateExecutionStrategyContextAction implements Action {
    private Logger logger = Logger
            .getLogger(PopulateExecutionStrategyContextAction.class);

    public Object act(ProcessInstance processInstance, ActionBinding action,
                      Object context) {
        System.out.println("Populate ExecutionStrategyContext Action Fired");
        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext processContext = processInstance.getContext();
        ExecutionStrategyContext strategyContext = (ExecutionStrategyContext) processContext
                .getNonPrsistableAttribute("ExecutionStrategyContext");
        EscalationManager.populateStrategyContext(strategyContext,
                processInstance);
        return result;
    }
}
