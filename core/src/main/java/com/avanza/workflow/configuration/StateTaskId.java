package com.avanza.workflow.configuration;

import java.io.Serializable;

import com.avanza.core.data.DbObject;

/**
 * WfStateTaskId entity. @author MyEclipse Persistence Tools
 */

public class StateTaskId extends DbObject implements Serializable {


    private static final long serialVersionUID = 1234L;
    // Fields

    private String stateTaskCode;
    private String processCode;
    private String stateCode;
    private String stateCodeLabel;

    // Constructors

    /**
     * default constructor
     */
    public StateTaskId() {
    }

    /**
     * full constructor
     */
    public StateTaskId(String stateTaskCode, String processCode,
                       String stateCode) {
        this.stateTaskCode = stateTaskCode;
        this.processCode = processCode;
        this.stateCode = stateCode;
    }

    // Property accessors

    public String getStateTaskCode() {
        return this.stateTaskCode;
    }

    public void setStateTaskCode(String stateTaskCode) {
        this.stateTaskCode = stateTaskCode;
    }

    public String getProcessCode() {
        return this.processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getStateCode() {
        return this.stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof StateTaskId))
            return false;
        StateTaskId castOther = (StateTaskId) other;

        return ((this.getStateTaskCode() == castOther.getStateTaskCode()) || (this
                .getStateTaskCode() != null
                && castOther.getStateTaskCode() != null && this
                .getStateTaskCode().equals(castOther.getStateTaskCode())))
                && ((this.getProcessCode() == castOther.getProcessCode()) || (this
                .getProcessCode() != null
                && castOther.getProcessCode() != null && this
                .getProcessCode().equals(castOther.getProcessCode())))
                && ((this.getStateCode() == castOther.getStateCode()) || (this
                .getStateCode() != null
                && castOther.getStateCode() != null && this
                .getStateCode().equals(castOther.getStateCode())));
    }

    public int hashCode() {
        int result = 17;

        result = 37
                * result
                + (getStateTaskCode() == null ? 0 : this.getStateTaskCode()
                .hashCode());
        result = 37
                * result
                + (getProcessCode() == null ? 0 : this.getProcessCode()
                .hashCode());
        result = 37 * result
                + (getStateCode() == null ? 0 : this.getStateCode().hashCode());
        return result;
    }

    public void setStateCodeLabel(String stateCodeLabel) {
        this.stateCodeLabel = stateCodeLabel;
    }

    public String getStateCodeLabel() {
        return stateCodeLabel;
    }

}