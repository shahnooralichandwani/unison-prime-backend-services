package com.avanza.workflow.configuration.action;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.DataObjectCollection;
import com.avanza.core.web.config.WebContext;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

public class CurrentOrgUnitCalculator extends TaskAllocatorAction {

    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {
        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext ctx = processInst.getContext();
        String productEntity = (String) ctx.getAttribute("PRODUCT_ENTITY_ID");
        String productNumber = (String) ctx.getAttribute("PRODUCT_NUMBER");
        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        DataObject customer = webCtx.getAttribute(SessionKeyLookup.CURRENT_CUSTOMER_KEY);
        try {
            DataObjectCollection collection = customer.getRelatedObjects(productEntity);
            for (DataObject obj : collection) {
                try {
                    String accountNum = obj.getAsString("ACCOUNT_NUM");
                    if (accountNum.equalsIgnoreCase(productNumber)) {
                        ctx.setAttribute("CURRENT_ORG_UNIT", obj.getAsString("BRANCH_CODE"));
                        result.setChangePossible(true);
                        return result;
                    }
                } catch (Exception ex) {
                }
            }
        } catch (Exception e) {
        }
        ctx.setAttribute("CURRENT_ORG_UNIT", "99");
        return result;
    }
}