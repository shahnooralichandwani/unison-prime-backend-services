package com.avanza.workflow.configuration;

import com.avanza.core.data.DbObject;

/**
 * WfStrategicActionId entity. @author MyEclipse Persistence Tools
 */

public class StrategicActionId extends DbObject implements java.io.Serializable {

    private static final long serialVersionUID = 1242398977654L;
    // Fields

    private String strategicActionCode;
    private String processCode;

    // Constructors

    /**
     * default constructor
     */
    public StrategicActionId() {
    }

    /**
     * full constructor
     */
    public StrategicActionId(String strategicActionCode, String processCode) {
        this.strategicActionCode = strategicActionCode;
        this.processCode = processCode;
    }

    // Property accessors

    public String getStrategicActionCode() {
        return this.strategicActionCode;
    }

    public void setStrategicActionCode(String strategicActionCode) {
        this.strategicActionCode = strategicActionCode;
    }

    public String getProcessCode() {
        return this.processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof StrategicActionId))
            return false;
        StrategicActionId castOther = (StrategicActionId) other;

        return ((this.getStrategicActionCode() == castOther
                .getStrategicActionCode()) || (this.getStrategicActionCode() != null
                && castOther.getStrategicActionCode() != null && this
                .getStrategicActionCode().equals(
                        castOther.getStrategicActionCode())))
                && ((this.getProcessCode() == castOther.getProcessCode()) || (this
                .getProcessCode() != null
                && castOther.getProcessCode() != null && this
                .getProcessCode().equals(castOther.getProcessCode())));
    }

    public int hashCode() {
        int result = 17;

        result = 37
                * result
                + (getStrategicActionCode() == null ? 0 : this
                .getStrategicActionCode().hashCode());
        result = 37
                * result
                + (getProcessCode() == null ? 0 : this.getProcessCode()
                .hashCode());
        return result;
    }

}