package com.avanza.workflow.configuration.action;

import com.avanza.core.data.DbObject;

public class TaskActionRelationship extends DbObject {
    private String actionSetId;
    private String taskActionId;

    public String getActionSetId() {
        return actionSetId;
    }

    public void setActionSetId(String actionSetId) {
        this.actionSetId = actionSetId;
    }

    public String getTaskActionId() {
        return taskActionId;
    }

    public void setTaskActionId(String taskActionId) {
        this.taskActionId = taskActionId;
    }

}
