package com.avanza.workflow.configuration;

import com.avanza.core.data.DbObject;
import com.avanza.workflow.configuration.action.ActionBinding;
import com.avanza.workflow.escalation.EscalationStrategy;

import java.io.Serializable;
import java.util.*;

/**
 * This class represents a State in Workflow Process
 *
 * @author hmirza
 */

public class State extends DbObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String processCode;

    private String stateCode;

    private String stateType;

    private GlobalState globalStateInfo;

    private boolean adhoc;

    private boolean start;

    private boolean end;

    private boolean parked;

    private Boolean ackSms;

    private boolean edit;

    private long tatInMinutes;

    private Set<Transition> transitionList = new HashSet<Transition>(0);

    private Set<ActionBinding> actionList = new HashSet<ActionBinding>(0);

    private Set<StateTask> stateTaskList = new HashSet<StateTask>(0);

    private Set<StrategicAction> strategicActionList = new HashSet<StrategicAction>(0);
    ;

    private EscalationStrategy strategy;

    private String escStrategyId;

    private String viewIdentifierColor;

    public GlobalState getGlobalStateInfo() {
        return globalStateInfo;
    }

    public void setGlobalStateInfo(GlobalState globalStateInfo) {
        this.globalStateInfo = globalStateInfo;
    }

    public List<ActionBinding> getSortedStateTaskActionBindings(EventType e, String stateTaskCode) {

        List<ActionBinding> returnList = new Vector<ActionBinding>();
        for (ActionBinding ab : actionList) {

            if (EventType.fromName(ab.getEventCode()) == e && ab.getStateTaskCode().equals(stateTaskCode))
                returnList.add(ab);
        }
        Collections.sort(returnList);

        return returnList;
    }

    public List<ActionBinding> getSortedActionBindings(EventType e) {

        List<ActionBinding> returnList = new Vector<ActionBinding>();
        for (ActionBinding ab : actionList) {

            if (EventType.fromName(ab.getEventCode()) == e)
                returnList.add(ab);
        }

        Collections.sort(returnList);

        return returnList;
    }

    public Set<ActionBinding> getActionList() {
        return actionList;
    }

    public void setActionList(Set<ActionBinding> actionList) {
        this.actionList = actionList;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public boolean getAdhoc() {
        return adhoc;
    }

    public void setAdhoc(boolean adhoc) {
        this.adhoc = adhoc;
    }

    public boolean getStart() {
        return start;
    }

    public void setStart(boolean start) {
        this.start = start;
    }

    public boolean getEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }

    public boolean getEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateType() {
        return stateType;
    }

    public void setStateType(String stateType) {
        this.stateType = stateType;
    }

    public long getTatInMinutes() {
        return tatInMinutes;
    }

    public void setTatInMinutes(long tatInMinutes) {
        this.tatInMinutes = tatInMinutes;
    }

    public Set<Transition> getTransitionList() {
        return transitionList;
    }

    public void setTransitionList(Set<Transition> transitionList) {
        this.transitionList = transitionList;
    }

    public State getNextState(String transCode) {
        for (Transition trans : transitionList) {
            if (trans.getTransitionCode().equalsIgnoreCase(transCode))
                return trans.getNextState();
        }
        return null;
    }

    public boolean isStateInvolved(String transCode) {
        for (Transition trans : transitionList) {
            if (trans.getTransitionCode().equalsIgnoreCase(transCode))
                return true;
        }
        return false;
    }

    public Transition getTransitionbyId(String transCode) {
        for (Transition trans : transitionList) {
            if (trans.getTransitionCode().equalsIgnoreCase(transCode))
                return trans;
        }
        return null;
    }

    public EscalationStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(EscalationStrategy strategy) {
        this.strategy = strategy;
    }

    public String getEscStrategyId() {

        return escStrategyId;
    }

    public void setEscStrategyId(String escStrategyId) {
        this.escStrategyId = escStrategyId;
    }

    public String getViewIdentifierColor() {
        return viewIdentifierColor;
    }

    public void setViewIdentifierColor(String viewIdentifierColor) {
        this.viewIdentifierColor = viewIdentifierColor;
    }

    public boolean getParked() {
        return parked;
    }

    public void setParked(boolean parked) {
        this.parked = parked;
    }


    public Set<StateTask> getStateTaskList() {
        return stateTaskList;
    }

    public void setStateTaskList(Set<StateTask> stateTaskList) {
        this.stateTaskList = stateTaskList;
    }

    public Set<StateTask> getStateTaskList(boolean preSave) {
        Set<StateTask> currentStateTaskList = new HashSet<StateTask>(0);
        if (!preSave) {
            for (StateTask stateTask : stateTaskList) {
                if (stateTask != null && !stateTask.getIsPreSave())
                    currentStateTaskList.add(stateTask);
            }
            return currentStateTaskList;
        } else
            return stateTaskList;

    }

    public Set<StateTask> getMandatoryStateTaskList() {
        Set<StateTask> mandatoryStateTaskList = new HashSet<StateTask>(0);
        for (StateTask stateTask : stateTaskList) {
            if (stateTask.getIsMandatory() && !stateTask.getIsPreSave())
                mandatoryStateTaskList.add(stateTask);
        }
        return mandatoryStateTaskList;
    }

    public Set<StateTask> getPreSaveStateTaskList() {
        Set<StateTask> preSaveStateTaskList = new HashSet<StateTask>(0);
        for (StateTask stateTask : stateTaskList) {
            if (stateTask != null && stateTask.getIsPreSave())
                preSaveStateTaskList.add(stateTask);
        }
        return preSaveStateTaskList;
    }

    public Set<StrategicAction> getStrategicActionList() {
        return strategicActionList;
    }

    public void setStrategicActionList(Set<StrategicAction> strategicActionList) {
        this.strategicActionList = strategicActionList;
    }

    public void setAckSms(Boolean ackSms) {
        this.ackSms = ackSms;
    }

    public Boolean getAckSms() {
        return ackSms;
    }

}
