package com.avanza.workflow.configuration.action;

import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ProcessInstance;

/**
 * @author hmirza
 */

public class StopStateEscalationAction implements Action {

    public Object act(ProcessInstance processInstance, ActionBinding action, Object context) {
        processInstance.getContext().setAttribute("STOP_STATE_ESC", Boolean.TRUE);
        return "";
    }
}
