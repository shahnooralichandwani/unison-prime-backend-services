/**
 *
 */
package com.avanza.workflow.configuration.action;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.main.EdocumentConfigManager;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.unison.admin.template.DBTemplate;
import com.avanza.unison.admin.template.NsTemplate;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ActionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

/**
 * @author rehan.ahmed
 *
 */
public class NotifyMaker extends TaskAllocatorAction {

    private static final Logger logger = Logger.getLogger(NotifyMaker.class);

    @Override
    public Object act(ProcessInstance process, ActionBinding action,
                      Object context) {
        ActionExecutionResult result = new ActionExecutionResult();
        String actionParams = action.getActionParam();
        ProcessContext ctx = process.getContext();
        HashMap<String, String> parsedParams = parseActionParams(actionParams);
        String makerId = (String) ctx.getAttribute("CREATED_BY");
        String emailTemplate = parsedParams.get("templateId");

        try {
            if (StringHelper.isNotEmpty(emailTemplate)) {
                MetaEntity instanceEntity = MetaDataRegistry.getMetaEntity(ctx.getDocumentId());
                MetaEntity parentEntity = MetaDataRegistry.getNonAbstractParentEntity(instanceEntity);
                Map<Object, Object> map = new HashMap<Object, Object>();

                map.put("ART_TITLE", ctx.getValues().get("ART_TITLE"));

                String processedTemplate = ApplicationLoader.getTemplateConfig().processTemplate(emailTemplate + "_content", map);
                String processedSubject = ApplicationLoader.getTemplateConfig().processTemplate(emailTemplate + "_subject", map);
                if (StringHelper.isNotEmpty(makerId)) {
                    User user = SecurityManager.getUser(makerId);
                    System.out.println("------------------------ Sending Email to Maker " + user.getEmailUrl() + " -----------------------------");
                    NotificationSender.sendEmail(processedSubject, processedTemplate, user.getEmailUrl(), null, InboundLoader.getInstance().getAckChannel("Email"));
                }
            }
        } catch (Exception e) {
            logger.logError("Exception while sending email -> NotifyMaker()" + e.getMessage());
        }

        result.setChangePossible(true);
        return result;
    }


    private HashMap<String, String> parseActionParams(String actionParams) {
        //CBD-SMTP-CHANNEL=00006;CBD-SMS-CHANNEL=00006
        HashMap<String, String> parsedParams = new HashMap<String, String>();
        StringTokenizer st = new StringTokenizer(actionParams, ";");
        while (st.hasMoreTokens()) {
            String token = st.nextToken().trim();
            if (StringHelper.isNotEmpty(token)) {
                StringTokenizer st1 = new StringTokenizer(token, "=");
                if (st1.countTokens() < 2) {
                    logger.logError(String.format("Error Parsing Params String (%1$s)", actionParams));
                    return parsedParams;
                }
                while (st1.hasMoreTokens()) {
                    String innerToken1 = st1.nextToken();
                    String innerToken2 = st1.nextToken();
                    parsedParams.put(innerToken1, innerToken2);
                }
            }
        }
        return parsedParams;
    }
}
