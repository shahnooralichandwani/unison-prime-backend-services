package com.avanza.workflow.configuration;

/**
 * This ENUM will represent Possible Event Types, there codes and there
 * priorities. Priority defines that actions associated with which event will be
 * executed first.
 *
 * @author hmirza
 */
public enum EventType {

    START_PROCESS(1, 1), END_PROCESS(2, 1), PROCESS_TAT_EXPIRY(3, 5), // TAT
    // Expiry
    // Event
    // on
    // Process
    ENTER_STATE(4, 2), LEAVE_STATE(5, 2), STATE_TAT_EXPIRY(6, 4), // Tat Expiry
    // Event on
    // State
    ON_TRANSITION(7, 3), BEFORE_PROCESS_TAT_EXPIRY(8, 6),

    BEFORE_STATE_TAT_EXPIRY(9, 3), ESCALATION_EVENT(10, 7),

    PRE_ENTER_STATE(11, 2),
    BEFORE_TASK_SAVING(12, 1), AFTER_TASK_SAVING(13, 1), NOT_USED(-1, 1),
    PRE_SAVE(14, 1), POST_SAVE(15, 1);


    private final int typeCode;

    private final int typePriroity;

    EventType(int tcode, int priority) {
        this.typeCode = tcode;
        this.typePriroity = priority;
    }

    public int typeCode() {
        return this.typeCode;
    }

    public int priority() {
        return this.typePriroity;
    }

    public static EventType fromName(long typeCode) {
        if (typeCode == 1)
            return EventType.START_PROCESS;
        if (typeCode == 2)
            return EventType.END_PROCESS;
        if (typeCode == 3)
            return EventType.PROCESS_TAT_EXPIRY;
        if (typeCode == 4)
            return EventType.ENTER_STATE;
        if (typeCode == 5)
            return EventType.LEAVE_STATE;
        if (typeCode == 6)
            return EventType.STATE_TAT_EXPIRY;
        if (typeCode == 7)
            return EventType.ON_TRANSITION;
        if (typeCode == 8)
            return EventType.BEFORE_PROCESS_TAT_EXPIRY;
        if (typeCode == 9)
            return EventType.BEFORE_STATE_TAT_EXPIRY;
        if (typeCode == 10)
            return EventType.ESCALATION_EVENT;
        if (typeCode == 11)
            return EventType.PRE_ENTER_STATE;
        if (typeCode == 12)
            return EventType.BEFORE_TASK_SAVING;
        if (typeCode == 13)
            return EventType.AFTER_TASK_SAVING;
        if (typeCode == 14)
            return EventType.PRE_SAVE;
        if (typeCode == 15)
            return EventType.POST_SAVE;
        if (typeCode < 0)
            return EventType.NOT_USED;
        return null;
    }

    public static int fromCode(String name) {
        if (name.toString().equalsIgnoreCase(EventType.START_PROCESS.name()))
            return EventType.START_PROCESS.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.BEFORE_PROCESS_TAT_EXPIRY.name()))
            return EventType.BEFORE_PROCESS_TAT_EXPIRY.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.BEFORE_STATE_TAT_EXPIRY.name()))
            return EventType.BEFORE_STATE_TAT_EXPIRY.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.END_PROCESS.name()))
            return EventType.END_PROCESS.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.ENTER_STATE.name()))
            return EventType.ENTER_STATE.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.ESCALATION_EVENT.name()))
            return EventType.ESCALATION_EVENT.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.LEAVE_STATE.name()))
            return EventType.LEAVE_STATE.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.ON_TRANSITION.name()))
            return EventType.ON_TRANSITION.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.PROCESS_TAT_EXPIRY.name()))
            return EventType.PROCESS_TAT_EXPIRY.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.STATE_TAT_EXPIRY.name()))
            return EventType.STATE_TAT_EXPIRY.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.BEFORE_TASK_SAVING.name()))
            return EventType.BEFORE_TASK_SAVING.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.AFTER_TASK_SAVING.name()))
            return EventType.AFTER_TASK_SAVING.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.PRE_SAVE.name()))
            return EventType.PRE_SAVE.typeCode();
        if (name.toString().equalsIgnoreCase(EventType.POST_SAVE.name()))
            return EventType.POST_SAVE.typeCode();
        return 0;
    }

}
