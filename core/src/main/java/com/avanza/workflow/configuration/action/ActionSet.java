package com.avanza.workflow.configuration.action;

import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;

public class ActionSet extends DbObject {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private String description;
    private Set<ActionBinding> actionList = new HashSet<ActionBinding>(0);

    public Set<ActionBinding> getActionList() {
        return actionList;
    }

    public void setActionList(Set<ActionBinding> actionList) {
        this.actionList = actionList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
