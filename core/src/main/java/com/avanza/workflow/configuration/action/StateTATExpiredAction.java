package com.avanza.workflow.configuration.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;


public class StateTATExpiredAction implements Action {

    public Object act(ProcessInstance processInstance, ActionBinding action, Object context) {
        System.out.println("State TAT Check Action");
        //processInstance.setTatExpired();
        List<String> stateCurrentActors = null;
        stateCurrentActors = processInstance.getCurrentActor();
        System.out.println(stateCurrentActors);
        //List<String> users = Convert.toList(processInstance.getCurrentActor());
        String userNames = "";
        for (String user : stateCurrentActors) {
            User Users = SecurityManager.getUser(user);
            userNames += Users.getFullName() + "/ ";
        }
        System.out.println(userNames);
        System.out.println(action.getActionCode());
        String actionParams = action.getActionParam();
        HashMap<String, String> parsedParams = parseActionParams(actionParams);
        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext ctx = processInstance.getContext();
        String templateId = parsedParams.get("templateId");
        String channelId = parsedParams.get("channelId");

        if (StringHelper.isNotEmpty(templateId) && StringHelper.isNotEmpty(channelId)) {
            MetaEntity instanceEntity = MetaDataRegistry.getMetaEntity(ctx.getDocumentId());
            HashMap<Object, Object> map = new HashMap<Object, Object>(0);
            MetaEntity parentEntity = MetaDataRegistry.getNonAbstractParentEntity(instanceEntity);
            map.put(parentEntity.getSystemName().replace('.', '_'), ctx.getValues());
            map.put("CurrentDate", new Date());
            map.put("ManagerName", userNames);
            map.put("ProcessTAT", processInstance.getProcessTAT());
            String processedTemplate = ApplicationLoader.getTemplateConfig().processTemplate(templateId + "_content", map);
            String processedSubject = ApplicationLoader.getTemplateConfig().processTemplate(templateId + "_subject", map);

            for (String user : stateCurrentActors) {
                User usr = SecurityManager.getUser(user);
                String usrEmail = usr.getEmailUrl();
                if (StringHelper.isNotEmpty(usrEmail)) {
                    System.out.println("------------------------ Sending Email to User " + usrEmail + " -----------------------------");
                    NotificationSender.sendEmail(processedSubject, processedTemplate, usrEmail, null, channelId);
                }
            }
        }
        return null;
    }

    private HashMap<String, String> parseActionParams(String actionParams) {
        HashMap<String, String> parsedParams = new HashMap<String, String>();
        StringTokenizer st = new StringTokenizer(actionParams, ";");
        while (st.hasMoreTokens()) {
            String token = st.nextToken().trim();
            if (StringHelper.isNotEmpty(token)) {
                StringTokenizer st1 = new StringTokenizer(token, "=");
                if (st1.countTokens() == 2) {
                    parsedParams.put(st1.nextToken().trim(), st1.nextToken().trim());
                }
            }
        }
        return parsedParams;
    }

}
