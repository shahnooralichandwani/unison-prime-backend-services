package com.avanza.workflow.configuration.action;

import java.util.Date;

import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.logger.ParkedStateManager;

/**
 * @author shoaib.rehman
 */

public class LeaveParkedStateAction implements Action {

    public Object act(ProcessInstance processInstance, ActionBinding action,
                      Object context) {
        //update PARKED_STATE_LOG table for endTime
        //isProcessed still remains false
        ParkedStateManager.updateParkedState(processInstance.getInstanceId(), new Date());
        return null;
    }

}
