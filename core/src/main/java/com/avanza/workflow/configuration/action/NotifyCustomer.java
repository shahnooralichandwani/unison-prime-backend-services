package com.avanza.workflow.configuration.action;

import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

public class NotifyCustomer extends TaskAllocatorAction {


    private static final Logger logger = Logger.getLogger(NotifyCustomer.class);

    @SuppressWarnings("unchecked")
    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {
        String actionParams = action.getActionParam();
        HashMap<String, String> parsedParams = parseActionParams(actionParams);
        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext ctx = processInst.getContext();
        String rim = (String) ctx.getAttribute("CUST_RELATION_NUM");
        String smsTemplate = parsedParams.get(InboundLoader.getInstance().getAckChannel("SMS"));
        String emailTemplate = parsedParams.get("templateId");
        if (StringHelper.isNotEmpty(emailTemplate)) {
            MetaEntity instanceEntity = MetaDataRegistry.getMetaEntity(ctx.getDocumentId());
            MetaEntity parentEntity = MetaDataRegistry.getNonAbstractParentEntity(instanceEntity);
            HashMap map = new HashMap();
            map.put(parentEntity.getSystemName().replace('.', '_'), ctx.getValues());
            String ticket_num = (String) ctx.getAttribute(parentEntity.getPrimaryKeyAttribute().getSystemName());
            map.put("TICKET_NUM", ticket_num);
            map.put("CurrentDate", new Date());
            String processedTemplate = ApplicationLoader.getTemplateConfig().processTemplate(emailTemplate + "_content", map);
            String processedSubject = ApplicationLoader.getTemplateConfig().processTemplate(emailTemplate + "_subject", map);
            String cusomerEmail = (String) ctx.getAttribute("CUST_CALLBACK_EMAIL");
            if (StringHelper.isNotEmpty(cusomerEmail)) {
                System.out.println("------------------ CUSTOMER ID: " + rim + "------------------");
                System.out.println("------------------------ Sending Email to Customer " + cusomerEmail + " -----------------------------");
                NotificationSender.sendEmail(processedSubject, processedTemplate, cusomerEmail, null, InboundLoader.getInstance().getAckChannel("Email"));
            }
        }
        if (StringHelper.isNotEmpty(smsTemplate)) {
            MetaEntity instanceEntity = MetaDataRegistry.getMetaEntity(ctx.getDocumentId());
            MetaEntity parentEntity = MetaDataRegistry.getNonAbstractParentEntity(instanceEntity);
            HashMap map = new HashMap();
            map.put(parentEntity.getSystemName().replace('.', '_'), ctx.getValues());
            map.put("CurrentDate", new Date());
            String processedTemplate = ApplicationLoader.getTemplateConfig().processTemplate(smsTemplate + "_content", map);
            String cusomerMobile = (String) ctx.getAttribute("CUST_CALLBACK_PHONE");
            if (StringHelper.isNotEmpty(cusomerMobile)) {
                System.out.println("------------------ CUSTOMER ID: " + rim + "------------------");
                System.out.println("------------------------ Sending SMS to Customer " + cusomerMobile + " -----------------------------");
                NotificationSender.sendSMS(cusomerMobile, processedTemplate, InboundLoader.getInstance().getAckChannel("SMS"));
            }
        }
        return result;
    }

    private HashMap<String, String> parseActionParams(String actionParams) {
        //CBD-SMTP-CHANNEL=00006;CBD-SMS-CHANNEL=00006
        HashMap<String, String> parsedParams = new HashMap<String, String>();
        StringTokenizer st = new StringTokenizer(actionParams, ";");
        while (st.hasMoreTokens()) {
            String token = st.nextToken().trim();
            if (StringHelper.isNotEmpty(token)) {
                StringTokenizer st1 = new StringTokenizer(token, "=");
                if (st1.countTokens() < 2) {
                    logger.logError(String.format("Error Parsing Params String (%1$s)", actionParams));
                    return parsedParams;
                }
                while (st1.hasMoreTokens()) {
                    String innerToken1 = st1.nextToken();
                    String innerToken2 = st1.nextToken();
                    parsedParams.put(innerToken1, innerToken2);
                }
            }
        }
        return parsedParams;
    }
}