package com.avanza.workflow.configuration;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;

/**
 * WfStrategicAction entity. @author MyEclipse Persistence Tools
 */

public class StrategicAction extends DbObject implements java.io.Serializable {


    private static final long serialVersionUID = 124435342543234L;
    // Fields

    private StrategicActionId id;
    private String stateCode;
    private String stateCodeLabel;
    private String actionName;
    private String meatViewId;
    private String meatViewLabel;
    private String categoryNode;
    private String metaEntId;


    //private Integer order;
    private Set<StrategicActionParticipant> strategicActionParticipantList = new HashSet<StrategicActionParticipant>(0);

    // Constructors

    /**
     * default constructor
     */
    public StrategicAction() {
    }

    /**
     * minimal constructor
     */
    public StrategicAction(StrategicActionId id, String actionName,
                           String meatViewId, Timestamp createdOn, String createdBy,
                           Timestamp updatedOn, String updatedBy) {
        this.id = id;
        this.actionName = actionName;
        this.meatViewId = meatViewId;

    }

    /**
     * full constructor
     */
    public StrategicAction(StrategicActionId id, String stateCode,
                           String actionName, String meatViewId, Timestamp createdOn,
                           String createdBy, Timestamp updatedOn, String updatedBy) {
        this.id = id;
        this.stateCode = stateCode;
        this.actionName = actionName;
        this.meatViewId = meatViewId;
    }

    // Property accessors

    public StrategicActionId getId() {
        return this.id;
    }

    public void setId(StrategicActionId id) {
        this.id = id;
    }

    public String getStateCode() {
        return this.stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateCodeLabel() {
        return this.stateCodeLabel;
    }

    public void setStateCodeLabel(String stateCodeLabel) {
        this.stateCodeLabel = stateCodeLabel;
    }

    public String getActionName() {
        return this.actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getMeatViewId() {
        return this.meatViewId;
    }

    public void setMeatViewId(String meatViewId) {
        this.meatViewId = meatViewId;
    }

    public Set<StrategicActionParticipant> getStrategicActionParticipantList() {
        return strategicActionParticipantList;
    }

    public Set<StrategicActionParticipant> getStrategicActionParticipantList(String processAllocId) {
        Set<StrategicActionParticipant> tempSet = new HashSet<StrategicActionParticipant>();
        for (StrategicActionParticipant participant : strategicActionParticipantList) {
            if (participant.getId().getProcessAllocId().equals(processAllocId))
                tempSet.add(participant);
        }
        return tempSet;
    }

    public void setStrategicActionParticipantList(
            Set<StrategicActionParticipant> strategicActionParticipantList) {
        this.strategicActionParticipantList = strategicActionParticipantList;
    }

    public void setMeatViewLabel(String meatViewLabel) {
        this.meatViewLabel = meatViewLabel;
    }

    public String getMeatViewLabel() {
        return meatViewLabel;
    }

    public String getCategoryNode() {
        return categoryNode;
    }

    public void setCategoryNode(String categoryNode) {
        this.categoryNode = categoryNode;
    }

    public String getMetaEntId() {
        return metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

	/*public void setOrder(Integer order) {
		this.order = order;
	}

	public Integer getOrder() {
		return order;
	}*/
}