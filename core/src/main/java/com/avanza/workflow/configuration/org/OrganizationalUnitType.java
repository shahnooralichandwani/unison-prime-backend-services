package com.avanza.workflow.configuration.org;

import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.StringHelper;

/**
 * This class depicts the Type of Organizational Unit
 *
 * @author hmirza
 */
public class OrganizationalUnitType extends DbObject {

    private static final long serialVersionUID = 133112033021580762L;

    private String unitTypeId;

    private OrganizationalUnitType orgUnitType;

    private String unitTypeNamePrm;

    private String unitTypeNameSec;

    private String hierarchyLevel;

    private Set<OrganizationalUnit> orgUnits = new HashSet<OrganizationalUnit>(0);

    private Set orgUnitTypes = new HashSet(0);

    // Constructors

    /**
     * default constructor
     */
    public OrganizationalUnitType() {
        unitTypeId = StringHelper.EMPTY;
    }

    /**
     * minimal constructor
     */
    public OrganizationalUnitType(String unitTypeId, OrganizationalUnitType orgUnitType, String unitTypeNamePrm, String hierarchyLevel) {
        this.unitTypeId = unitTypeId;
        this.orgUnitType = orgUnitType;
        this.unitTypeNamePrm = unitTypeNamePrm;
        this.hierarchyLevel = hierarchyLevel;
    }

    /**
     * full constructor
     */
    public OrganizationalUnitType(String unitTypeId, OrganizationalUnitType orgUnitType, String unitTypeNamePrm, String unitTypeNameSec,
                                  String hierarchyLevel, Set<OrganizationalUnit> orgUnits, Set orgUnitTypes) {
        this.unitTypeId = unitTypeId;
        this.orgUnitType = orgUnitType;
        this.unitTypeNamePrm = unitTypeNamePrm;
        this.unitTypeNameSec = unitTypeNameSec;
        this.hierarchyLevel = hierarchyLevel;
        this.orgUnits = orgUnits;
        this.orgUnitTypes = orgUnitTypes;
    }

    // Property accessors
    public String getUnitTypeId() {
        return this.unitTypeId;
    }

    public void setUnitTypeId(String unitTypeId) {
        this.unitTypeId = unitTypeId;
    }

    public OrganizationalUnitType getOrgUnitType() {
        return this.orgUnitType;
    }

    public void setOrgUnitType(OrganizationalUnitType orgUnitType) {
        this.orgUnitType = orgUnitType;
    }

    public String getUnitTypeNamePrm() {
        return this.unitTypeNamePrm;
    }

    public void setUnitTypeNamePrm(String unitTypeNamePrm) {
        this.unitTypeNamePrm = unitTypeNamePrm;
    }

    public String getUnitTypeNameSec() {
        return this.unitTypeNameSec;
    }

    public void setUnitTypeNameSec(String unitTypeNameSec) {
        this.unitTypeNameSec = unitTypeNameSec;
    }

    public String getHierarchyLevel() {
        return this.hierarchyLevel;
    }

    public void setHierarchyLevel(String hierarchyLevel) {
        this.hierarchyLevel = hierarchyLevel;
    }

    public Set<OrganizationalUnit> getOrgUnits() {
        return this.orgUnits;
    }

    public void setOrgUnits(Set<OrganizationalUnit> orgUnits) {
        this.orgUnits = orgUnits;
    }

    public Set getOrgUnitTypes() {
        return this.orgUnitTypes;
    }

    public void setOrgUnitTypes(Set orgUnitTypes) {
        this.orgUnitTypes = orgUnitTypes;
    }

    public String toString() {
        return (StringHelper.isEmpty(unitTypeId)) ? StringHelper.EMPTY : unitTypeId;
    }
}
