package com.avanza.workflow.configuration.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

public class DispatchTaskAction extends TaskAllocatorAction {

    @SuppressWarnings("unchecked")
    public Object act(ProcessInstance processInst, ActionBinding action,
                      Object context) {

        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext ctx = processInst.getContext();
        DataObject obj = processInst.getDataObject();
        /*
         * MetaViewAttribute attrib = MetaDataRegistry. Object value =
         * ContextAttributes.get(attrib.getFullMetaAttributeId());
         */
        String rim = (String) ctx.getAttribute("CUST_RELATION_NUM");
        // it is for e-form can be altered for complaint / lead as well
        String ticketNumber = (String) ctx.getAttribute("EFORM_TICKET_NUM");
        // STRING_VAL1 is attachment
        // STRING_VAL2 letter template
        // STRING_VAL3 is subject
        String attachemntFile = obj.getAsString("STRING_VAL1");
        List attachment = new ArrayList<String>();
        attachment.add(attachemntFile);
        String emailContent = "Dear Customer Please find Attachment against your reference number :"
                + ticketNumber;
        String emailSubject = obj.getAsString("STRING_VAL3");

        MetaEntity instanceEntity = MetaDataRegistry.getMetaEntity(ctx
                .getDocumentId());
        MetaEntity parentEntity = MetaDataRegistry
                .getNonAbstractParentEntity(instanceEntity);
        HashMap map = new HashMap();
        map.put(parentEntity.getSystemName().replace('.', '_'), ctx
                .getValues());
        map.put("CurrentDate", new Date());
        String cusomerEmail = (String) ctx.getAttribute("CUST_EMAIL");
        if (StringHelper.isNotEmpty(cusomerEmail)) {
            System.out.println("------------------ CUSTOMER ID: " + rim + "------------------");
            System.out.println("------------------------ Sending Email to Customer " + cusomerEmail + " -----------------------------");
            NotificationSender.sendEmail(emailSubject, emailContent,
                    cusomerEmail, attachment, InboundLoader.getInstance()
                            .getAckChannel("Email"));
        }
        return result;
    }
}