package com.avanza.workflow.configuration.filter;

public interface StateTaskLookup {
    String STATE_TASK_CODE = "STATE_TASK_CODE";

    String STATE_TRANSITION_CODE = "STATE_TRANSITION_CODE";
    String STATE_CODE = "STATE_CODE";
    String PROCESS_CODE = "PROCESS_CODE";
    String PROCESS_ALLOC_ID = "PROCESS_ALLOC_ID";
    String DOC_NUM = "DOC_NUM";
    String STATE_TASK_TYPE = "STATE_TASK_TYPE";
    String STRATEGIC_ACTION_CODE = "STRATEGIC_ACTION_CODE";
    String TYPE_STARTEGIC_ACTION = "StrategicAction";
    String TYPE_STATE_TASK = "StateTask";
    String DOC_STATE_TASK_LIST = "Doc_State_Task_List";
    String obj_id_value = "obj_id_value";
    String TASK_DETAIL_ID = "TASK_DETAIL_ID";
    String task_permission = "Unison.Main.Document.StateTask";

}
