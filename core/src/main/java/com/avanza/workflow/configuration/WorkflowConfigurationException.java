package com.avanza.workflow.configuration;

public class WorkflowConfigurationException extends Exception {

    public WorkflowConfigurationException(String message) {
        super(message);
    }
}
