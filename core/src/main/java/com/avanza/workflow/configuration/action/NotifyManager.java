package com.avanza.workflow.configuration.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.OrganizationalUnit;
import com.avanza.workflow.configuration.org.Role;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

public class NotifyManager extends TaskAllocatorAction {


    @SuppressWarnings("unchecked")
    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {
        String actionParams = action.getActionParam();
        HashMap<String, String> parsedParams = parseActionParams(actionParams);
        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext ctx = processInst.getContext();

        String templateId = parsedParams.get("templateId");
        String channelId = parsedParams.get("channelId");
        int level = Integer.parseInt(parsedParams.get("level"));

        List<String> currentActors = processInst.getCalculatedActor();
        List<String> managers = getActorsManagers(currentActors, processInst, level);
        String managerNames = "";
        for (String user : managers) {
            User managerUser = SecurityManager.getUser(user);
            managerNames += managerUser.getFullName() + "/";
        }
        managerNames = managerNames.substring(0, managerNames.length() - 1);
        if (StringHelper.isNotEmpty(templateId) && StringHelper.isNotEmpty(channelId)) {
            HashMap map = new HashMap();
            MetaEntity instanceEntity = MetaDataRegistry.getMetaEntity(ctx.getDocumentId());
            MetaEntity parentEntity = MetaDataRegistry.getNonAbstractParentEntity(instanceEntity);
            map.put(parentEntity.getSystemName().replace('.', '_'), ctx.getValues());
            map.put("CurrentDate", new Date());
            map.put("ManagerName", managerNames);
            map.put("ProcessTAT", processInst.getProcessTAT());
            String processedTemplate = ApplicationLoader.getTemplateConfig().processTemplate(templateId + "_content", map);
            String processedSubject = ApplicationLoader.getTemplateConfig().processTemplate(templateId + "_subject", map);
            //changed email sending process -- shoaib.rehman
            StringBuffer emailBuffer = new StringBuffer();
            for (String user : managers) {
                if (emailBuffer.length() > 0)
                    emailBuffer.append(",");
                emailBuffer.append(user);
            }
            if (emailBuffer.length() > 0)
                NotificationSender.sendEmail(processedSubject,
                        processedTemplate, emailBuffer.toString(), null, channelId);
        }
        return result;
    }

    private List<String> getActorsManagers(List<String> currentActors, ProcessInstance instance, int level) {
        List<String> managers = new ArrayList<String>(0);

        for (String user : currentActors) {
            List<Role> userAllRoles = OrganizationManager.getAllRoles(user);
            List<String> filteredRoles = new ArrayList<String>(0);
            for (Role role : userAllRoles) {
                if (instance.getPreviousState() != null) {
                    if (instance.getBinding().isStateRoleAssigned(instance.getPreviousState().getStateCode(), role.getRoleId())) {
                        filteredRoles.add(role.getRoleId());
                    } else if (instance.getAssignedRoles().contains(role.getRoleId())) {
                        filteredRoles.add(role.getRoleId());
                    }
                } else {
                    if (instance.getAssignedRoles().contains(role.getRoleId())) {
                        filteredRoles.add(role.getRoleId());
                    }
                }
            }
            for (String roleid : filteredRoles) {
                Role parentRole = OrganizationManager.getParentRole(roleid);
                if (parentRole == null)
                    continue;
                List<OrganizationalUnit> userOrgs = OrganizationManager.getUserOrgUnits(user, roleid);
                for (OrganizationalUnit unit : userOrgs) {
                    List<User> orgRoleUsers = OrganizationManager.getUsers(unit.getOrgUnitId(), parentRole.getRoleId());
                    for (User usr : orgRoleUsers) {
                        if (!managers.contains(usr.getLoginId())) managers.add(usr.getLoginId());
                    }
                }
                if (level == 2) {
                    if (parentRole.getParentRole() != null) {
                        parentRole = parentRole.getParentRole();
                        for (OrganizationalUnit unit : userOrgs) {
                            List<User> orgRoleUsers = OrganizationManager.getUsers(unit.getOrgUnitId(), parentRole.getRoleId());
                            for (User usr : orgRoleUsers) {
                                if (!managers.contains(usr.getLoginId())) managers.add(usr.getLoginId());
                            }
                        }
                    }
                }
            }
        }
        return managers;
    }

    private HashMap<String, String> parseActionParams(String actionParams) {
        HashMap<String, String> parsedParams = new HashMap<String, String>();
        StringTokenizer st = new StringTokenizer(actionParams, ";");
        while (st.hasMoreTokens()) {
            String token = st.nextToken().trim();
            if (StringHelper.isNotEmpty(token)) {
                StringTokenizer st1 = new StringTokenizer(token, "=");
                if (st1.countTokens() == 2) {
                    parsedParams.put(st1.nextToken().trim(), st1.nextToken().trim());
                }
            }
        }
        return parsedParams;
    }
}