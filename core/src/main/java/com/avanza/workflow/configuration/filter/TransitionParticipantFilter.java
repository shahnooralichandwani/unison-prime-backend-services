package com.avanza.workflow.configuration.filter;

import java.util.Set;

import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.Transition;

public interface TransitionParticipantFilter {

    public boolean getActivities(Transition transition, Set<StateParticipant> stateParticipant, String userId);

}
