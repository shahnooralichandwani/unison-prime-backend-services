package com.avanza.workflow.configuration.org;

import java.io.Serializable;

import com.avanza.core.data.DbObject;
import com.avanza.core.meta.MetaEntity;

/**
 * @author anam
 */

public class EntityOrgMappingId extends DbObject implements Serializable {

    // Fields

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private OrganizationalUnit orgUnit;
    private MetaEntity metaEntity;

    // Constructors

    /**
     * default constructor
     */
    public EntityOrgMappingId() {
    }

    /**
     * full constructor
     */
    public EntityOrgMappingId(OrganizationalUnit orgUnit, MetaEntity metaEntity) {
        this.orgUnit = orgUnit;
        this.metaEntity = metaEntity;
    }

    // Property accessors

    public OrganizationalUnit getOrgUnit() {
        return this.orgUnit;
    }

    public void setOrgUnit(OrganizationalUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public MetaEntity getMetaEntity() {
        return this.metaEntity;
    }

    public void setMetaEntity(MetaEntity metaEntity) {
        this.metaEntity = metaEntity;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof EntityOrgMappingId))
            return false;
        EntityOrgMappingId castOther = (EntityOrgMappingId) other;

        return ((this.getOrgUnit() == castOther.getOrgUnit()) || (this
                .getOrgUnit() != null
                && castOther.getOrgUnit() != null && this.getOrgUnit().equals(
                castOther.getOrgUnit())))
                && ((this.getMetaEntity() == castOther.getMetaEntity()) || (this
                .getMetaEntity() != null
                && castOther.getMetaEntity() != null && this
                .getMetaEntity().equals(castOther.getMetaEntity())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getOrgUnit() == null ? 0 : this.getOrgUnit().hashCode());
        result = 37
                * result
                + (getMetaEntity() == null ? 0 : this.getMetaEntity()
                .hashCode());
        return result;
    }

}