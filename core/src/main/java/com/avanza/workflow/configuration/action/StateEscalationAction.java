package com.avanza.workflow.configuration.action;

import java.util.Map;

import com.avanza.workflow.escalation.EscalationManager;
import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ProcessInstance;

/**
 * @author hmirza
 */

public class StateEscalationAction implements Action {

    public Object act(ProcessInstance processInstance, ActionBinding action, Object context) {
        Map<String, String> parsedParams = action.getParsedActionParams();
        String strategyId = parsedParams.get("strategyId");
        if (strategyId != null) {
            EscalationManager.startStrategy(processInstance, EscalationManager
                    .getStrategy(strategyId), true);
        } else {
            EscalationManager.startStrategy(processInstance, true);
        }
        return "";
    }
}
