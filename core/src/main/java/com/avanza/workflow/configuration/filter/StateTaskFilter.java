package com.avanza.workflow.configuration.filter;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.sessiondata.SessionDataManager;
import com.avanza.core.sdo.sessiondata.SessionDataObject;
import com.avanza.core.util.StringHelper;
import com.avanza.ui.util.ResourceUtil;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.configuration.StateTask;
import com.avanza.workflow.configuration.StateTaskParticipant;
import com.avanza.workflow.configuration.Transition;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.Role;

public class StateTaskFilter {

    /**
     * @author Shuwair Sardar
     */

    public static String ALL_TASK = "ALL_TASK";
    public static String ALL_PENDING_TAKS = "ALL_PENDING_TAKS";
    public static String ALL_DONE_TAKS = "ALL_PENDING_TAKS";
    public static String MY_ALL_TASK = "MY_ALL_TASK";
    public static String MY_PENDING_TASK = "MY_PENDING_TASK";
    public static String MY_DONE_TASK = "MY_DONE_TASK";

    public static String MANDATORY_ALL = "All";
    public static String MANDATORY_ATLEASTONE = "AtleastOne";

    public static String SINGLE = "Single";
    public static String MULTIPLE = "Multiple";
    public static String INFINITE = "Infinite";


    public static List<StateTask> getUserPendingTaskList(String processAllocId, State state, String userId, Transition transition, List<DataObject> completedTaskList) {
        if (transition == null)
            return getUserPendingTaskList(processAllocId, state, userId, completedTaskList);
        else
            return getUserPendingTaskList(processAllocId, state, userId, transition.getTransitionCode(), completedTaskList);
    }

    public static List<StateTask> getUserPendingTaskList(String processAllocId, State state, String userId, List<DataObject> completedTaskList) {
        List<StateTask> userStateTasks = new ArrayList<StateTask>(0);
        Set<StateTask> stateTasks = state.getStateTaskList(false);
        for (StateTask stateTask : stateTasks) {
            for (StateTaskParticipant participant : stateTask.getStateTaskParticipantList()) {
                if ((isUserExistInParticipantList(userId, participant.getUserIdsList()) ||
                        (isUserExistInRole(userId, participant.getId().getRoleId()) && participant.getUserIdsList().size() == 0))
                        && StringHelper.isEmpty(stateTask.getStateTransitionCode())
                        && !isTaskCompleted(completedTaskList, stateTask, userId, processAllocId) && participant.getId().getProcessAllocId().equals(processAllocId)) {
                    if (stateTask.getParentTaskCode() != null)
                        if (isParentTaskCompleted(state, completedTaskList, stateTask, userId, processAllocId))
                            stateTask.setIsParentCompleted(true);
                        else
                            stateTask.setIsParentCompleted(false);
                    else {
                        stateTask.setIsParentCompleted(true);
                    }
                    userStateTasks.add(stateTask);
                    break;
                }
            }
        }
        return userStateTasks;
    }

    public static List<StateTask> getUserPreSavePendingTaskList(String processAllocId, State state, String userId, List<DataObject> completedTaskList) {
        List<StateTask> userStateTasks = new ArrayList<StateTask>(0);
        Set<StateTask> stateTasks = state.getPreSaveStateTaskList();
        for (StateTask stateTask : stateTasks) {
            //for(StateTaskParticipant participant : stateTask.getStateTaskParticipantList())
            if (!stateTask.getStateTaskParticipantList().isEmpty()) {
                if (!isPreSaveTaskCompleted(stateTask, processAllocId)) {
                    if (stateTask.getParentTaskCode() != null)
                        if (isPreSaveParentTaskCompleted(state, completedTaskList, stateTask, processAllocId))
                            stateTask.setIsParentCompleted(true);
                        else
                            stateTask.setIsParentCompleted(false);
                    else {
                        stateTask.setIsParentCompleted(true);
                    }
                    userStateTasks.add(stateTask);
                }
            }
        }
        return userStateTasks;
    }

    public static List<StateTask> getUserPendingTaskList(String processAllocId, State state, String userId, String transitionCode, List<DataObject> completedTaskList) {
        List<StateTask> userStateTasks = new ArrayList<StateTask>(0);
        Set<StateTask> stateTasks = state.getStateTaskList(false);
        for (StateTask stateTask : stateTasks) {
            for (StateTaskParticipant participant : stateTask.getStateTaskParticipantList()) {
                if ((isUserExistInParticipantList(userId, participant.getUserIdsList()) ||
                        (isUserExistInRole(userId, participant.getId().getRoleId()) && participant.getUserIdsList().size() == 0))
                        && transitionCode.equals(stateTask.getStateTransitionCode())
                        && !isTaskCompleted(completedTaskList, stateTask, userId, processAllocId) && participant.getId().getProcessAllocId().equals(processAllocId)) {
                    if (stateTask.getParentTaskCode() != null)
                        if (isParentTaskCompleted(state, completedTaskList, stateTask, userId, processAllocId))
                            stateTask.setIsParentCompleted(true);
                        else
                            stateTask.setIsParentCompleted(false);
                    else {
                        stateTask.setIsParentCompleted(true);
                    }
                    userStateTasks.add(stateTask);
                    break;
                }
            }
        }
        return userStateTasks;
    }

    public static List<StateTask> getUserAllTaskList(String processAllocId, State state, String userId, List<DataObject> completedTaskList) {
        List<StateTask> userStateTasks = new ArrayList<StateTask>(0);
        Set<StateTask> stateTasks = state.getStateTaskList(false);
        for (StateTask stateTask : stateTasks) {
            for (StateTaskParticipant participant : stateTask.getStateTaskParticipantList()) {
                if ((isUserExistInParticipantList(userId, participant.getUserIdsList()) ||
                        (isUserExistInRole(userId, participant.getId().getRoleId()) && participant.getUserIdsList().size() == 0))
                        && StringHelper.isEmpty(stateTask.getStateTransitionCode()) && participant.getId().getProcessAllocId().equals(processAllocId)) {
                    if (stateTask.getParentTaskCode() != null)
                        if (isParentTaskCompleted(state, completedTaskList, stateTask, userId, processAllocId))
                            stateTask.setIsParentCompleted(true);
                        else
                            stateTask.setIsParentCompleted(false);
                    else {
                        stateTask.setIsParentCompleted(true);
                    }
                    userStateTasks.add(stateTask);
                    break;
                }
            }
        }
        return userStateTasks;
    }

    public static List<StateTask> getUserAllTaskList(String processAllocId, State state, String userId, String transitionCode, List<DataObject> completedTaskList) {
        List<StateTask> userStateTasks = new ArrayList<StateTask>(0);
        Set<StateTask> stateTasks = state.getStateTaskList(false);
        for (StateTask stateTask : stateTasks) {
            for (StateTaskParticipant participant : stateTask.getStateTaskParticipantList()) {
                if ((isUserExistInParticipantList(userId, participant.getUserIdsList()) ||
                        (isUserExistInRole(userId, participant.getId().getRoleId()) && participant.getUserIdsList().size() == 0))
                        && transitionCode.equals(stateTask.getStateTransitionCode()) && participant.getId().getProcessAllocId().equals(processAllocId)) {
                    if (stateTask.getParentTaskCode() != null)
                        if (isParentTaskCompleted(state, completedTaskList, stateTask, userId, processAllocId))
                            stateTask.setIsParentCompleted(true);
                        else
                            stateTask.setIsParentCompleted(false);
                    else {
                        stateTask.setIsParentCompleted(true);
                    }
                    userStateTasks.add(stateTask);
                    break;
                }
            }
        }
        return userStateTasks;
    }

    public static Map<String, String> getAdhocTaskStats(State state, List<DataObject> completedTaskList, String processAllocId) {
        Map<String, String> userStats = new Hashtable<String, String>(0);
        int allTaskCount = 0;
        Set<StateTask> stateTasks = state.getStateTaskList(false);
        for (StateTask stateTask : stateTasks) {
            if (!stateTask.getStateTaskParticipantList(processAllocId).isEmpty())
                allTaskCount++;
        }

        userStats.put(MY_ALL_TASK, allTaskCount + "");
        int compTask = 0;
        for (DataObject dataObject : completedTaskList) {
            if (dataObject.getAsString("STATE_CODE").equals(state.getStateCode()))
                compTask++;
        }
        userStats.put(MY_DONE_TASK, compTask + "");
        userStats.put(MY_PENDING_TASK, Integer.parseInt(userStats.get(MY_ALL_TASK)) - Integer.parseInt(userStats.get(MY_DONE_TASK)) + "");

        return userStats;
    }

    public static Map<String, String> getUserTaskStats(String processAllocId, State state, String userId, Transition transition, List<DataObject> completedTaskList) {
        if (transition == null)
            return getUserTaskStats(processAllocId, state, userId, completedTaskList);
        else
            return getUserTaskStats(processAllocId, state, userId, transition.getTransitionCode(), completedTaskList);
    }

    public static Map<String, String> getUserTaskStats(String processAllocId, State state, String userId, List<DataObject> completedTaskList) {
        Map<String, String> userStats = new Hashtable<String, String>(0);
        userStats.put(MY_ALL_TASK, getUserAllTaskList(processAllocId, state, userId, completedTaskList).size() + "");
        userStats.put(MY_PENDING_TASK, getUserPendingTaskList(processAllocId, state, userId, completedTaskList).size() + "");
        userStats.put(MY_DONE_TASK, Integer.parseInt(userStats.get(MY_ALL_TASK)) - Integer.parseInt(userStats.get(MY_PENDING_TASK)) + "");
        return userStats;
    }

    public static Map<String, String> getUserTaskStats(String processAllocId, State state, String userId, String transitionCode, List<DataObject> completedTaskList) {
        Map<String, String> userStats = new Hashtable<String, String>(0);
        userStats.put(MY_ALL_TASK, getUserAllTaskList(processAllocId, state, userId, transitionCode, completedTaskList).size() + "");
        userStats.put(MY_PENDING_TASK, getUserPendingTaskList(processAllocId, state, userId, transitionCode, completedTaskList).size() + "");
        userStats.put(MY_DONE_TASK, Integer.parseInt(userStats.get(MY_ALL_TASK)) - Integer.parseInt(userStats.get(MY_PENDING_TASK)) + "");
        return userStats;
    }


    public static boolean isStateTransitionAllowed(State state, String userId, String transitionCode, List<DataObject> completedTaskList, String processAllocId) {
        Set<StateTask> stateTasks = state.getMandatoryStateTaskList();
        for (StateTask stateTask : stateTasks) {
            if ((transitionCode.equals(stateTask.getStateTransitionCode()) || stateTask.getStateTransitionCode() == null)
                    && !isTaskCompleted(completedTaskList, stateTask, userId, processAllocId))
                return false;
        }
        return true;
    }


    public static boolean isTaskCompleted(List<DataObject> completedTaskList, StateTask stateTask, String userId, String processAllocId) {
        //TODO shuwair : completed check

        if (stateTask.getStateTaskParticipantList(processAllocId).isEmpty())
            return true;

        if (stateTask.getTaskCompletionStrategy().equals(INFINITE))
            return false;
        if (stateTask.getTaskCompletionStrategy().equals(MULTIPLE)) {
            for (DataObject completedTask : completedTaskList) {
                if (completedTask.getAsString("STATE_TASK_CODE").equals(stateTask.getId().getStateTaskCode())
                        && completedTask.getAsString("CREATED_BY").trim().equals(userId))
                    return true;
            }
            return false;
        }
        if (stateTask.getTaskCompletionStrategy().equals(SINGLE)) {
            for (DataObject completedTask : completedTaskList) {
                if (completedTask.getAsString("STATE_TASK_CODE").equals(stateTask.getId().getStateTaskCode()))
                    return true;
            }
            return false;
        }

        return false;
    }

    public static boolean isTaskCompleted(List<DataObject> completedTaskList, StateTask stateTask, String processAllocId) {
        if (stateTask.getStateTaskParticipantList(processAllocId).isEmpty())
            return true;

        for (DataObject completedTask : completedTaskList) {
            if (completedTask.getAsString("STATE_TASK_CODE").equals(stateTask.getId().getStateTaskCode()))
                return true;
        }
        return false;
    }


    public static boolean isPreSaveTaskCompleted(StateTask stateTask, String processAllocId) {
        if (stateTask.getStateTaskParticipantList(processAllocId).isEmpty())
            return true;

        for (SessionDataObject completedTask : SessionDataManager.getSessionDataObjectByGroup(SessionKeyLookup.PRESAVE_TASKS).values()) {
            if (completedTask.getDataObject().getAsString(StateTaskLookup.PROCESS_ALLOC_ID).equals(processAllocId)
                    && completedTask.getDataObject().getAsString(StateTaskLookup.STATE_TASK_CODE).equals(stateTask.getId().getStateTaskCode()))
                return true;
        }
        return false;
    }

    public static boolean isMandatoryTaskCompleted(List<DataObject> completedTaskList, StateTask stateTask, String userId, String processAllocId) {
        return isTaskCompleted(completedTaskList, stateTask, userId, processAllocId);
    }

    public static boolean checkMandatory(List<DataObject> completedTaskList, StateTask stateTask, String processAllocId) {
        if (stateTask.getIsMandatory() && MANDATORY_ALL.equals(stateTask.getMandatoryType()) && !stateTask.getTaskCompletionStrategy().equals(INFINITE))
            return true;
        if (stateTask.getIsMandatory() && MANDATORY_ATLEASTONE.equals(stateTask.getMandatoryType()) && !stateTask.getTaskCompletionStrategy().equals(SINGLE)) {
            if (!isTaskCompleted(completedTaskList, stateTask, processAllocId))
                return true;
        }
        return false;
    }

    public static boolean isParentTaskCompleted(State state, List<DataObject> completedTaskList, StateTask stateTask, String userId, String processAllocId) {
        StateTask parentTask = null;
        for (StateTask task : state.getStateTaskList(false)) {
            if (task.getId().getStateTaskCode().equals(stateTask.getParentTaskCode())) {
                parentTask = task;
                break;
            }
        }
        if (parentTask == null)
            return false;
        else
            return isTaskCompleted(completedTaskList, parentTask, userId, processAllocId);
    }


    public static boolean isPreSaveParentTaskCompleted(State state, List<DataObject> completedTaskList, StateTask stateTask, String processAllocId) {
        StateTask parentTask = null;
        for (StateTask task : state.getStateTaskList(false)) {
            if (task.getId().getStateTaskCode().equals(stateTask.getParentTaskCode())) {
                parentTask = task;
                break;
            }
        }
        if (parentTask == null)
            return false;
        else
            return isPreSaveTaskCompleted(parentTask, processAllocId);
    }

    public static boolean isUserExistInRole(String userId, String roleId) {
        List<Role> roles = OrganizationManager.getAllRoles(userId);
        for (Role role : roles) {
            if (role.getRoleId().equals(roleId))
                return true;
        }
        return false;
    }

    public static boolean isUserExistInParticipantList(String userId, List<String> userIds) {
        if (userIds.contains(userId))
            return true;
        return false;
    }

    //Bulk Activity state task - Start
    public static List<DataObject> getCompletedTaskList(String ticketNum, String processCode, String processAllocId) {
        DataBroker broker = DataRepository.getBroker(ResourceUtil.getInstance().getProperty("state_task_entity_sys_name"));
        Search search = new Search();
        search.addFrom(ResourceUtil.getInstance().getProperty("state_task_entity_sys_name"));
        search.addCriterion(SimpleCriterion.equal("DOC_NUM", ticketNum));
        search.addCriterion(SimpleCriterion.equal("PROCESS_CODE", processCode));
        search.addCriterion(SimpleCriterion.equal("PROCESS_ALLOC_ID", processAllocId));
        List<DataObject> docStateTaskList = broker.find(search);

        return docStateTaskList;
    }
    //Bulk Activity state task - End


}
