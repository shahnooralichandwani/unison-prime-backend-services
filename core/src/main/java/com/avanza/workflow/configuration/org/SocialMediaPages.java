package com.avanza.workflow.configuration.org;

import com.avanza.core.data.DbObject;
import com.avanza.core.security.User;

/**
 * Social Media Pages Class
 *
 * @author shafique.rehman
 */

public class SocialMediaPages extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 7494074275048680019L;

    private String pageId;
    private String pagePrimaryName;
    private String pageSecondaryName;
    private String pageType;


    public SocialMediaPages() {
        // TODO Auto-generated constructor stub
    }

    /**
     * full constructor
     */
    public SocialMediaPages(String pageId, String pagePrimaryName, String pageSecondaryName, String pageType) {
        this.pageId = pageId;
        this.pagePrimaryName = pagePrimaryName;
        this.pageSecondaryName = pageSecondaryName;
        this.pageType = pageType;

    }


    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getPagePrimaryName() {
        return pagePrimaryName;
    }

    public void setPagePrimaryName(String pagePrimaryName) {
        this.pagePrimaryName = pagePrimaryName;
    }

    public String getPageSecondaryName() {
        return pageSecondaryName;
    }

    public void setPageSecondaryName(String pageSecondaryName) {
        this.pageSecondaryName = pageSecondaryName;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }


}
