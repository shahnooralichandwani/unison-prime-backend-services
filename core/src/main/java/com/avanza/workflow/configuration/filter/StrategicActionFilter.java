package com.avanza.workflow.configuration.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.StringHelper;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.configuration.StateTask;
import com.avanza.workflow.configuration.StateTaskParticipant;
import com.avanza.workflow.configuration.StrategicAction;
import com.avanza.workflow.configuration.StrategicActionParticipant;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.Role;

/**
 * @author Shuwair Sardar
 */

public class StrategicActionFilter {

    public static List<StrategicAction> getUserStrategicActionList(String processAllocId, String userId, Set<StrategicAction> strategicActionList) {
        List<StrategicAction> userStrategicActions = new ArrayList<StrategicAction>(0);
        for (StrategicAction strategicAction : strategicActionList) {
            for (StrategicActionParticipant participant : strategicAction.getStrategicActionParticipantList(processAllocId)) {
                if ((isUserExistInParticipantList(userId, participant.getUserIdsList()) ||
                        (isUserExistInRole(userId, participant.getId().getRoleId()) && participant.getUserIdsList().size() == 0))
                        && participant.getId().getProcessAllocId().equals(processAllocId)) {

                    userStrategicActions.add(strategicAction);
                }
            }
        }
        return userStrategicActions;
    }

    public static boolean isUserExistInRole(String userId, String roleId) {
        List<Role> roles = OrganizationManager.getAllRoles(userId);
        for (Role role : roles) {
            if (role.getRoleId().equals(roleId))
                return true;
        }
        return false;
    }

    public static boolean isUserExistInParticipantList(String userId, List<String> userIds) {
        if (userIds.contains(userId))
            return true;
        return false;
    }

}
