package com.avanza.workflow.configuration;

import java.util.Set;

import com.avanza.core.data.expression.Search;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.workflow.execution.AvanzaWorkflowEngine;
import com.avanza.workflow.execution.ProcessInstance;

/**
 * This is the main class for callers to manager processes. This class provides an abstraction over underlying workflow engine.
 *
 * @author hmirza
 */

public abstract class WorkFlowEngine {

    public static WorkFlowEngine getInstance(ConfigSection config) {
        // Temp work ( no config exists )
        return new AvanzaWorkflowEngine();
    }

    /*
     * Process Definition Functions
     */

    public abstract Process findProcess(String processId);

    public abstract Set<Process> findProcess(Search searchcriteria);

    public abstract void associateProcess(Process proc, String documentid, Set<String> supervisors, Set<String> creators, Set<String> viewers);

    public abstract void associateProcessParticipant(Process proc, String documentid, String role, boolean isSupervisor, boolean isCreator);

    public abstract void associateStateParticipant(State stat, String role);

    public abstract void associateStateParticipant(State stat, Set<String> role);


    /*
     * Process Execution Functions
     */

    public abstract ProcessInstance getProcessInstance(String instanceId, String documentid);

    public abstract void saveInstance(ProcessInstance procInstance);


}
