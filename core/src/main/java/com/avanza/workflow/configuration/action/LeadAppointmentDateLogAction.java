package com.avanza.workflow.configuration.action;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.avanza.component.calendar.Task;
import com.avanza.component.calendar.TaskType;
import com.avanza.component.calendar.TaskUser;
import com.avanza.core.calendar.task.TaskManager;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.sequence.SequenceManager;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.web.config.WebContext;
import com.avanza.workflow.configuration.action.ActionBinding;
import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskContext;
import com.avanza.workflow.logger.WorkflowActivityLogger;
import com.avanza.workflow.logger.WorkflowLog;

/**
 * @author Khawaja Muhammad Aamir
 */

public class LeadAppointmentDateLogAction implements Action {

    public Object act(ProcessInstance processInstance, ActionBinding action, Object context) {
        ProcessContext ctx = processInstance.getContext();
        TaskManager taskManger = TaskManager.getInstance();
        Date appointmentDate = (Date) ctx.getAttribute("APPOINTMENT_DATE");
        if (appointmentDate != null) {
            String CustomerName = (String) ctx.getAttribute("CUSTOMER_NAME");
            String RIM = (String) ctx.getAttribute("CUST_RELATION_NUM");
            WebContext wc = ApplicationContext.getContext().get(WebContext.class);
            UserDbImpl currentUser = (UserDbImpl) wc.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);

            Task task = new Task();
            task.setTaskId(SequenceManager.getInstance().getNextValue(TaskManager.TASK_KEY_GENERATOR));
            task.setCompletionDate(appointmentDate);
            task.setDueDate(appointmentDate);
            task.setStartDate(appointmentDate);
            task.setDescription("Appointment with " + CustomerName + " (RIM " + RIM + ") ");
            task.setOwnerUserId(currentUser.getLoginId());
            task.setNotificationStrategy(taskManger.getDefaultNotificationStrategy());
            task.setTaskPriority(taskManger.getDefaultTaskPriority());
            task.setTaskStatus(taskManger.getDefaultTaskStatus());
            task.setTaskType(TaskType.General.toString());
            TaskManager.getInstance().persist(task);

            Set<TaskUser> set = new HashSet<TaskUser>();
            TaskUser TU = new TaskUser();
            TU.setAssignedDate(new Date());
            TU.setUser(currentUser);
            TU.setTask(task);
            TU.setLoginId(currentUser.getLoginId());
            set.add(TU);
            TaskManager.getInstance().persist(TU);
            String currentActors = (String) ctx.getAttribute("CURRENT_ACTOR");
            List<String> users = Convert.toList(currentActors);
            users.remove(currentUser.getLoginId());
            for (String user : users) {
                TaskUser currentTU = new TaskUser();
                currentTU.setAssignedDate(new Date());
                currentTU.setUser(currentUser);
                currentTU.setTask(task);
                currentTU.setLoginId(user);
                set.add(currentTU);
                TaskManager.getInstance().persist(currentTU);
            }
            task.setTaskUsers(set);
            wc.setAttribute(SessionKeyLookup.CURRENT_TASK, task);

            TaskContext taskContext = (TaskContext) context;
            WorkflowLog log = null;
            log = (WorkflowLog) taskContext.getParameters().get("LOG");
            if (log != null)
                WorkflowActivityLogger.updateLogWorkflowActivity(log, appointmentDate);
        }
    	/*else{
    		WorkflowActivityLogger.logWorkflowActivity(processInstance,processInstance.getCurrentState().getStateCode(), null, "");
    		taskContext.getParameters().put("LOG", log);
    		log = (WorkflowLog)taskContext.getParameters().get("LOG");
    		WorkflowActivityLogger.updateLogWorkflowActivity(log, appointmentDate); 
    	}*/
        return "";
    }
}

