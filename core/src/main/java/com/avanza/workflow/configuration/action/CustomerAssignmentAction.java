package com.avanza.workflow.configuration.action;

import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

/**
 * @author Shahbaz
 * @desc Customer Assignment action when new email received by the system
 */
public class CustomerAssignmentAction extends TaskAllocatorAction {

    private static final String CUSTOMER_ENTITY = "Unison.Customer";


    private static final Logger logger = Logger.getLogger(CustomerAssignmentAction.class);

    @SuppressWarnings("deprecation")
    public Object act(ProcessInstance processInst, ActionBinding action,
                      Object context) {
        logger.logInfo("[ CustomerAssignmentAction called CustomerAssignmentAction.act() ]");
        ActionExecutionResult result = new ActionExecutionResult();
        try {
            Object object = processInst.getContext().getAttribute("IS_COMPOSED");
            // Check if this is a composed email..
            if (object != null && object.equals(true)) {
                result.setChangePossible(true);
                return result;
            }
            String emailSender = processInst.getContext().getAttribute("RECEIVED_FROM").toString();
            String emailFrom = StringHelper.getSubString(emailSender, "<", ">");
            MetaEntity entity = MetaDataRegistry.getEntityBySystemName(CUSTOMER_ENTITY);
            DataBroker broker = DataRepository.getBroker(entity.getSystemName());
            Search custs = new Search();
            String colEmailId = InboundLoader.getInstance().getCustomerColumnName("EMAIL");
            String colEmailIdSec = InboundLoader.getInstance().getCustomerColumnName("SECEMAIL");
            String colRim = InboundLoader.getInstance().getCustomerColumnName("RIM");
            custs.addColumn(entity.getAttribute(colRim).getTableColumn());
            custs.addFrom(entity.getSystemName());
            custs.addCriterion(Criterion.equal(colEmailId, emailFrom));
            custs.addCriterionOr(Criterion.equal(colEmailIdSec, emailFrom));
            List<DataObject> obj = broker.find(custs);
            for (DataObject dataObject : obj) {
                String rim = dataObject.getAsString(colRim);
                processInst.getContext().setAttribute("RELATIONSHIP_NUM", rim);
                processInst.getContext().setAttribute("CUST_RELATION_NUM", rim);
            }
            result.setChangePossible(true);
        } catch (Exception e) {
            logger.LogException("[ Exception assigning email to existing customer, CustomerAssignmentAction.act() ]", e);
            result.setChangePossible(false);
        }
        return result;
    }
}
