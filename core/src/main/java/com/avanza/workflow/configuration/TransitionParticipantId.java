package com.avanza.workflow.configuration;

/**
 * TransitionParticipantId entity.
 *
 * @author MyEclipse Persistence Tools
 */

public class TransitionParticipantId implements java.io.Serializable {

    // Fields

    private String stateCode;
    private String roleId;
    private String processAllocId;
    private String taskCode;
    private String processCode;

    // Constructors

    /**
     * default constructor
     */
    public TransitionParticipantId() {
    }

    /**
     * full constructor
     */
    public TransitionParticipantId(String stateCode, String roleId,
                                   String processAllocId, String taskCode, String processCode) {
        this.stateCode = stateCode;
        this.roleId = roleId;
        this.processAllocId = processAllocId;
        this.taskCode = taskCode;
        this.processCode = processCode;
    }

    // Property accessors

    public String getStateCode() {
        return this.stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getRoleId() {
        return this.roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getProcessAllocId() {
        return this.processAllocId;
    }

    public void setProcessAllocId(String processAllocId) {
        this.processAllocId = processAllocId;
    }

    public String getTaskCode() {
        return this.taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public String getProcessCode() {
        return this.processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof TransitionParticipantId))
            return false;
        TransitionParticipantId castOther = (TransitionParticipantId) other;

        return ((this.getStateCode() == castOther.getStateCode()) || (this
                .getStateCode() != null
                && castOther.getStateCode() != null && this.getStateCode()
                .equals(castOther.getStateCode())))
                && ((this.getRoleId() == castOther.getRoleId()) || (this
                .getRoleId() != null
                && castOther.getRoleId() != null && this.getRoleId()
                .equals(castOther.getRoleId())))
                && ((this.getProcessAllocId() == castOther.getProcessAllocId()) || (this
                .getProcessAllocId() != null
                && castOther.getProcessAllocId() != null && this
                .getProcessAllocId().equals(
                        castOther.getProcessAllocId())))
                && ((this.getTaskCode() == castOther.getTaskCode()) || (this
                .getTaskCode() != null
                && castOther.getTaskCode() != null && this
                .getTaskCode().equals(castOther.getTaskCode())))
                && ((this.getProcessCode() == castOther.getProcessCode()) || (this
                .getProcessCode() != null
                && castOther.getProcessCode() != null && this
                .getProcessCode().equals(castOther.getProcessCode())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getStateCode() == null ? 0 : this.getStateCode().hashCode());
        result = 37 * result
                + (getRoleId() == null ? 0 : this.getRoleId().hashCode());
        result = 37
                * result
                + (getProcessAllocId() == null ? 0 : this.getProcessAllocId()
                .hashCode());
        result = 37 * result
                + (getTaskCode() == null ? 0 : this.getTaskCode().hashCode());
        result = 37
                * result
                + (getProcessCode() == null ? 0 : this.getProcessCode()
                .hashCode());
        return result;
    }

}