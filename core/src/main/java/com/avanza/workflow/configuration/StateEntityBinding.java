package com.avanza.workflow.configuration;

import java.util.Date;

import com.avanza.core.data.DbObject;
import com.avanza.workflow.escalation.EscalationStrategy;

public class StateEntityBinding extends DbObject implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7957780705502147670L;

    private StateEntityId id;

    private EscalationStrategy strategy;

    private String strategyId;

    private String expression;

    private String entityId;

    private long TAT;

    public StateEntityId getId() {
        return id;
    }

    public void setId(StateEntityId id) {
        this.id = id;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public EscalationStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(EscalationStrategy strategy) {
        this.strategy = strategy;
    }

    public String getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(String strategyId) {
        this.strategyId = strategyId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public long getTAT() {
        return TAT;
    }

    public void setTAT(long TAT) {
        this.TAT = TAT;
    }
}