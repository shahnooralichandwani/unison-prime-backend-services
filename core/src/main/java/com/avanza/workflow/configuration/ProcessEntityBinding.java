package com.avanza.workflow.configuration;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.Role;
import com.avanza.workflow.escalation.EscalationStrategy;

public class ProcessEntityBinding extends DbObject implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private int processAllocId;

    private String processCode;

    private EscalationStrategy strategy;

    private String escStrategyId;

    private String selectExpression;

    private String metaEntityId;

    private long turnAroundTime;

    private Date createdOn;

    private String createdBy;

    private Date updatedOn;

    private String updatedBy;

    private Set<ProcessParticipant> processParticipantList = new HashSet<ProcessParticipant>(0);

    private Set<StateParticipant> stateParticipantList = new HashSet<StateParticipant>(0);

    private Set<StateEntityBinding> stateBindings = new HashSet<StateEntityBinding>(0);

    public Set<StateEntityBinding> getStateBindings() {
        return stateBindings;
    }

    public void setStateBindings(Set<StateEntityBinding> stateBindings) {
        this.stateBindings = stateBindings;
    }

    public boolean isStateRoleAssigned(String stateCode, String roleid) {

        for (StateParticipant rsb : stateParticipantList) {
            if (rsb.getStateCode().equalsIgnoreCase(stateCode)) {
                if (roleid.equalsIgnoreCase(rsb.getRoleId())) {
                    return true;
                }
            }
        }
        return false;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public Set<ProcessParticipant> getProcessParticipantList() {
        return processParticipantList;
    }

    public void setProcessParticipantList(Set<ProcessParticipant> processParticipantList) {
        this.processParticipantList = processParticipantList;
    }

    public Set<StateParticipant> getStateParticipantList() {
        return stateParticipantList;
    }

    public void setStateParticipantList(Set<StateParticipant> stateParticipantList) {
        this.stateParticipantList = stateParticipantList;
    }

    /**
     * default constructor
     */
    public ProcessEntityBinding() {
    }

    /**
     * minimal constructor
     */
    public ProcessEntityBinding(int processAllocId, String processId, String metId, Date createdOn, String createdBy, Date updatedOn, String updatedBy) {
        this.processAllocId = processAllocId;
        this.processCode = processId;
        this.metaEntityId = metId;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
    }

    public ProcessEntityBinding(int processAllocId, String processId, String metId, boolean isSupervisor, boolean canCreate) {
        this.processAllocId = processAllocId;
        this.processCode = processId;
        this.metaEntityId = metId;
    }

    public long getTurnAroundTime() {
        return this.turnAroundTime;
    }

    public void setTurnAroundTime(long turnAroundTime) {
        this.turnAroundTime = turnAroundTime;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

    public List<String> getSupervisorRolesList() {
        List<String> rolesList = new ArrayList<String>(0);
        for (ProcessParticipant participant : this.processParticipantList) {
            if (participant.getIsSupervisor()) {
                rolesList.add(participant.getRoleId());
            }

        }
        return rolesList;
    }

    public int getProcessAllocId() {
        return processAllocId;
    }

    public void setProcessAllocId(int processAllocId) {
        this.processAllocId = processAllocId;
    }

    public String getProcessId() {
        return processCode;
    }

    public void setProcessId(String processId) {
        this.processCode = processId;
    }

    public String getSelectExpression() {
        return selectExpression;
    }

    public void setSelectExpression(String selectExpression) {
        this.selectExpression = selectExpression;
    }

    public EscalationStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(EscalationStrategy strategy) {
        this.strategy = strategy;
    }

    public List<String> getSupervisorRoles(String roleId) {
        List<String> supRoles = new ArrayList<String>(0);
        List<Role> parentRoles = OrganizationManager.getParentRoles(roleId);
        List<String> supervisorsRoleList = getSupervisorRolesList();
        for (String supRole : supervisorsRoleList) {
            for (Role role : parentRoles) {
                if (role.getRoleId().equalsIgnoreCase(supRole))
                    supRoles.add(supRole);
            }
        }
        return supRoles;
    }

    public String getEscStrategyId() {
        return escStrategyId;
    }

    public void setEscStrategyId(String escStrategyId) {
        this.escStrategyId = escStrategyId;
    }

}