package com.avanza.workflow.configuration;

/**
 * WfStateTaskParticipantId entity. @author MyEclipse Persistence Tools
 */

public class StrategicActionParticipantId implements java.io.Serializable {

    // Fields

    private String strategicActionCode;
    private String processAllocId;
    private String processCode;
    private String roleId;

    // Constructors

    /**
     * default constructor
     */
    public StrategicActionParticipantId() {
    }

    /**
     * full constructor
     */
    public StrategicActionParticipantId(String strategicActionCode,
                                        String processAllocId, String processCode, String roleId) {
        this.strategicActionCode = strategicActionCode;
        this.processAllocId = processAllocId;
        this.processCode = processCode;
        this.roleId = roleId;
    }

    // Property accessors


    public String getStrategicActionCode() {
        return strategicActionCode;
    }

    public void setStrategicActionCode(String strategicActionCode) {
        this.strategicActionCode = strategicActionCode;
    }


    public String getProcessAllocId() {
        return this.processAllocId;
    }

    public void setProcessAllocId(String processAllocId) {
        this.processAllocId = processAllocId;
    }

    public String getProcessCode() {
        return this.processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getRoleId() {
        return this.roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof StrategicActionParticipantId))
            return false;
        StrategicActionParticipantId castOther = (StrategicActionParticipantId) other;

        return ((this.getStrategicActionCode() == castOther.getStrategicActionCode()) || (this
                .getStrategicActionCode() != null
                && castOther.getStrategicActionCode() != null && this
                .getStrategicActionCode().equals(castOther.getStrategicActionCode())))
                && ((this.getProcessAllocId() == castOther.getProcessAllocId()) || (this
                .getProcessAllocId() != null
                && castOther.getProcessAllocId() != null && this
                .getProcessAllocId().equals(
                        castOther.getProcessAllocId())))
                && ((this.getProcessCode() == castOther.getProcessCode()) || (this
                .getProcessCode() != null
                && castOther.getProcessCode() != null && this
                .getProcessCode().equals(castOther.getProcessCode())))
                && ((this.getRoleId() == castOther.getRoleId()) || (this
                .getRoleId() != null
                && castOther.getRoleId() != null && this.getRoleId()
                .equals(castOther.getRoleId())));
    }

    public int hashCode() {
        int result = 17;

        result = 37
                * result
                + (getStrategicActionCode() == null ? 0 : this.getStrategicActionCode()
                .hashCode());
        result = 37
                * result
                + (getProcessAllocId() == null ? 0 : this.getProcessAllocId()
                .hashCode());
        result = 37
                * result
                + (getProcessCode() == null ? 0 : this.getProcessCode()
                .hashCode());
        result = 37 * result
                + (getRoleId() == null ? 0 : this.getRoleId().hashCode());
        return result;
    }

}