package com.avanza.workflow.configuration;

/**
 * Main Workflow Exception
 *
 * @author hmirza
 */
public class WorkFlowException extends RuntimeException {

    private static final long serialVersionUID = 1L;


}
