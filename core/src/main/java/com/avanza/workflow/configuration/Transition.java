package com.avanza.workflow.configuration;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import com.avanza.core.data.DbObject;
import com.avanza.workflow.configuration.action.ActionBinding;

/**
 * This class represents Transition between two Process States
 *
 * @author hmirza
 */

public class Transition extends DbObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String transitionCode;

    private String processCode;

    private String taskName;

    /**
     * Starting State
     */
    private State initialState;

    private String initialStateCode;

    private String nextStateCode;

    /**
     * Ending State
     */
    private State nextState;

    private String reasonPicklist;

    private String activityPicklist;

    private String assignmentType;

    private String filterRole;

    private boolean allowBulk;

    private String filterOrgUnit;

    private Set<ActionBinding> actionList = new HashSet<ActionBinding>(0);

    private Set<TransitionParticipant> transitionParticipantList = new HashSet<TransitionParticipant>(0);

    public String getAssignmentType() {
        return assignmentType;
    }

    public void setAssignmentType(String assignmentType) {
        this.assignmentType = assignmentType;
    }

    public Set<ActionBinding> getActionList() {
        return actionList;
    }

    public void setActionList(Set<ActionBinding> actionList) {
        this.actionList = actionList;
    }

    public String getTransitionCode() {
        return transitionCode;
    }

    public void setTransitionCode(String transitionCode) {
        this.transitionCode = transitionCode;
    }

    public State getInitialState() {
        return initialState;
    }

    public void setInitialState(State initialState) {
        this.initialState = initialState;
    }

    public State getNextState() {
        return nextState;
    }

    public void setNextState(State nextState) {
        this.nextState = nextState;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public List<ActionBinding> getSortedActionBindings(EventType e) {

        List<ActionBinding> returnList = new Vector<ActionBinding>();
        for (ActionBinding ab : actionList) {

            if (EventType.fromName(ab.getEventCode()) == e)
                returnList.add(ab);
        }

        Collections.sort(returnList);

        return returnList;
    }

    public String getActivityPicklist() {
        return activityPicklist;
    }

    public void setActivityPicklist(String activityPicklist) {
        this.activityPicklist = activityPicklist;
    }

    public String getReasonPicklist() {
        return reasonPicklist;
    }

    public void setReasonPicklist(String reasonPicklist) {
        this.reasonPicklist = reasonPicklist;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getInitialStateCode() {
        return initialStateCode;
    }

    public void setInitialStateCode(String initialStateCode) {
        this.initialStateCode = initialStateCode;
    }

    public String getNextStateCode() {
        return nextStateCode;
    }

    public void setNextStateCode(String nextStateCode) {
        this.nextStateCode = nextStateCode;
    }

    public Set<TransitionParticipant> getTransitionParticipantList() {
        return transitionParticipantList;
    }

    public void setTransitionParticipantList(
            Set<TransitionParticipant> transitionParticipantList) {
        this.transitionParticipantList = transitionParticipantList;
    }

    //Shuwair
    public String getFilterRole() {
        return filterRole;
    }

    public void setFilterRole(String filterRole) {
        this.filterRole = filterRole;
    }

    public String getFilterOrgUnit() {
        return filterOrgUnit;
    }

    public void setFilterOrgUnit(String filterOrgUnit) {
        this.filterOrgUnit = filterOrgUnit;
    }

    //naushad
    public boolean getAllowBulk() {
        return allowBulk;
    }

    public void setAllowBulk(boolean allowBulk) {
        this.allowBulk = allowBulk;
    }
}
