package com.avanza.workflow.configuration.action;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskContext;

public class ExampleConditionalAction implements Action {

    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {

        ActionExecutionResult result = new ActionExecutionResult();
        Map<String, String> map = parseParams(action.getActionParam());
        if (map != null) {
            ((TaskContext) context).setNextAction(map.get(map.get("response")));
        }
        return result;
    }

    public Map<String, String> parseParams(String actionParams) {
        if (actionParams == null)
            return null;
        Map<String, String> map = new HashMap<String, String>();
        StringTokenizer st = new StringTokenizer(actionParams, ";");
        while (st.hasMoreElements()) {
            String s = (String) st.nextElement();
            map.put(s.split("=")[0], s.split("=")[1]);
        }
        return map;
    }
}
