package com.avanza.workflow.configuration.org;


import java.io.Serializable;

import com.avanza.core.data.DbObject;
import com.avanza.core.security.User;

/**
 * @author shafique.rehman
 */
public class SocialMediaRoleUserBinding extends DbObject implements Serializable {

    // Fields


    /**
     *
     */
    private static final long serialVersionUID = -3695093346421218168L;

    private String smUserRoleId;

    private String roleId;

    private String pageId;

    private String loginId;

    public String getSmUserRoleId() {
        return smUserRoleId;
    }

    public void setSmUserRoleId(String smUserRoleId) {
        this.smUserRoleId = smUserRoleId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }


}
