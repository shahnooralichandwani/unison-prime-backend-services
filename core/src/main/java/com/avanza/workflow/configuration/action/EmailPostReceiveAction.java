package com.avanza.workflow.configuration.action;

import java.util.Date;
import java.util.HashMap;

import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.main.EdocumentConfigManager;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.notificationservice.notification.PriorityType;
import com.avanza.unison.admin.template.DBTemplate;
import com.avanza.unison.admin.template.NsTemplate;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

/**
 * @author Shahbaz
 * @desc Sending auto response to the customer when new email received by the
 * system
 */
public class EmailPostReceiveAction extends TaskAllocatorAction {

    private static final Logger logger = Logger.getLogger(EmailPostReceiveAction.class);

    @SuppressWarnings("unchecked")
    public Object act(ProcessInstance processInst, ActionBinding action,
                      Object context) {
        logger.logInfo("[ EmailPostReceiveAction called EmailPostReceiveAction.act() ]");
        ActionExecutionResult result = new ActionExecutionResult();
        try {
            Object obj = processInst.getContext().getAttribute("IS_COMPOSED");
            // Check if this is a composed email..
            if (obj != null && obj.equals(true)) {
                processInst.getContext().setAttribute("CURRENT_STATE", "0000000006");
                result.setChangePossible(true);
                return result;
            }
            String emailSender = processInst.getContext().getAttribute("RECEIVED_FROM").toString();
            String emailFrom = StringHelper.getSubString(emailSender, "<", ">");
            if (EdocumentConfigManager.isEnableAutoRespond() && ((!emailFrom.equalsIgnoreCase(EdocumentConfigManager.getSystemAccount())) || (!emailFrom.contains(EdocumentConfigManager.getSystemAccount())))) {
                logger.logInfo("[ Sending response to the customer ]");
                // Sending auto response to the sender
                MetaEntity instanceEntity = MetaDataRegistry.getMetaEntity(processInst.getContext().getDocumentId());
                HashMap map = new HashMap();
                map.put(instanceEntity.getSystemName().replace('.', '_'), processInst.getContext().getValues());
                map.put("CurrentDate", new Date());
                NsTemplate template = DBTemplate.getInstance().getTemplate(EdocumentConfigManager.getTemplateId());
                String processedTemplate = ApplicationLoader.getTemplateConfig().processTemplate(template.getTemplateId() + "_content", map);
                String processedSubject = ApplicationLoader.getTemplateConfig().processTemplate(template.getTemplateId() + "_subject", map);
                NotificationSender.sendEmail(processedSubject, processedTemplate, emailFrom, EdocumentConfigManager.getSystemAccount(), null, InboundLoader.getInstance().getAckChannel("Email"), PriorityType.Medium);
                logger.logInfo("[ Auto response configured to be sent to: " + emailSender + " ]");
            }
            result.setChangePossible(true);
        } catch (Exception e) {
            logger.LogException("[ Exception executing Email Post Receive Action, EmailPostReceiveAction.act() ]", e);
            result.setChangePossible(false);
        }
        return result;
    }
}
