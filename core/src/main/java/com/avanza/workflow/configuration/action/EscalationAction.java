package com.avanza.workflow.configuration.action;

import java.util.Map;

import com.avanza.workflow.escalation.EscalationManager;
import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ActionResult;
import com.avanza.workflow.execution.ProcessInstance;

/**
 * @author hmirza
 */

public class EscalationAction implements Action {

    public Object act(ProcessInstance processInstance, ActionBinding action,
                      Object context) {

        System.out.println("Escalation Action Fired");
        ActionResult result = new ActionResult();

        Map<String, String> parsedParams = action.getParsedActionParams();
        String strategyId = parsedParams.get("strategyId");
        if (strategyId != null) {
            EscalationManager.startStrategy(processInstance, EscalationManager
                    .getStrategy(strategyId), false);
        } else {
            EscalationManager.startStrategy(processInstance, false);
        }
        return result;
    }

}
