package com.avanza.workflow.configuration;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.HibernateDataBroker;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.Expression;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.WorkflowContextLookup;


/**
 * All methods will be static in this class. This class will loaded in ProcessManager
 *
 * @author hmirza
 */

public class ProcessCatalog {

    private static Map<String, Process> processMap = new Hashtable<String, Process>(0);

    private static List<ProcessEntityBinding> procEntityBindings = new ArrayList<ProcessEntityBinding>(0);

    private static List<Process> processList = new ArrayList<Process>(0);

    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock readLock = readWriteLock.readLock();
    private static Lock writeLock = readWriteLock.writeLock();

    public static void load() {

        HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker(ProcessCatalog.class.getName());
        processList = broker.findAll(Process.class);
        procEntityBindings = broker.findAll(ProcessEntityBinding.class);
        for (Process proc : processList) {
            for (ProcessEntityBinding binding : procEntityBindings) {
                if (binding.getProcessId().equalsIgnoreCase(proc.getProcessCode()))
                    proc.addProcessEntityBinding(binding);
            }
            processMap.put(proc.getProcessCode(), proc);
        }
    }

    public static void reLoad() {
        //Shuwair
        try {
            readLock.lock();
            processList = new ArrayList<Process>(0);
            procEntityBindings = new ArrayList<ProcessEntityBinding>(0);
            HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker(ProcessCatalog.class.getName());
            processList = broker.findAll(Process.class);
            procEntityBindings = broker.findAll(ProcessEntityBinding.class);
            for (Process proc : processList) {
                for (ProcessEntityBinding binding : procEntityBindings) {
                    if (binding.getProcessId().equalsIgnoreCase(proc.getProcessCode()))
                        proc.addProcessEntityBinding(binding);
                }
                processMap.put(proc.getProcessCode(), proc);
            }
        } finally {
            readLock.unlock();
        }
    }

    public static void dispose() {

    }

    /**
     * @param metaEntityId process is bound against it. see process binding.
     * @param ctx          purpose to pass context, is to add it into ApplicationContext.
     *                     which is required by FEL for SelectExpression evaluation;
     * @return NULL will be returned if SelectExpression result is false.
     */
    public static Process getProcessByDocumentId(String metaEntityId, ProcessContext ctx) {
        Process process = null;
        for (ProcessEntityBinding binding : procEntityBindings) {
            if (binding.getMetaEntityId().equalsIgnoreCase(metaEntityId)) {
                String selectExpString = binding.getSelectExpression();
                Boolean isTrue = true;
                if (selectExpString != null && selectExpString.length() > 0) {
                    ExpressionParser expParser = new ExpressionParser(selectExpString, Boolean.class, FunctionCatalogManager.getInstance()
                            .getFunctionCatalog(ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction,
                                    ExpressionClass.WorkFlowFunctions));
                    ApplicationContext.getContext().add(WorkflowContextLookup.PROCESS_CONTEXT_PARAM, ctx);
                    Expression retVal = expParser.parse();
                    ExpressionEvaluator eval = ExpressionEvaluator.getInstance();
                    isTrue = (Boolean) eval.evaluate(retVal);
                }
                if (isTrue) {
                    for (Process proc : processList) {
                        if (proc.getProcessCode().equalsIgnoreCase(binding.getProcessCode())) {
                            process = proc;
                            return process;
                        }
                    }
                }
            }
        }
        return process;
    }


    public static Process getProcessByDocumentId(String metaEntityId) {

        /* Do not use this function unless this uses Expression Evaluation API to get the process
         * Shahbaz
         *
         */
        Process process = null;
        for (ProcessEntityBinding binding : procEntityBindings) {
            if (binding.getMetaEntityId().equalsIgnoreCase(metaEntityId)) {
                for (Process proc : processList) {
                    if (proc.getProcessCode().equalsIgnoreCase(binding.getProcessCode())) {
                        process = proc;
                        return process;
                    }
                }
            }
        }
        return process;
    }
    
    /*
    public static ProcessEntityBinding getProcessEntityBindingByDocumentId(String metaEntityId) {
    	
    	ProcessEntityBinding entityBinding = null;
        for (ProcessEntityBinding binding : procEntityBindings) {
            if (binding.getMetaEntityId().equalsIgnoreCase(metaEntityId)) 
            {
                return binding;
        	}
        }
        return entityBinding;
    }*/

    public static Process getProcessByProcessCode(String processCode) {

        if (processCode == null) return null;

        return processMap.get(processCode);
    }

    public static boolean canCreateProcess(String roleid, String metaentid) {
        if (isAnyAssociatedProcess(metaentid)) {
            for (ProcessEntityBinding binding : procEntityBindings) {
                for (ProcessParticipant participant : binding.getProcessParticipantList()) {
                    if (participant.getRoleId().equalsIgnoreCase(roleid)) if (participant.getCanCreate()) return true;
                }
            }
        }
        return false;
    }

    public static boolean isAnyAssociatedProcess(String metaEntityId) {
        for (ProcessEntityBinding binding : procEntityBindings) {
            if (binding.getMetaEntityId().equalsIgnoreCase(metaEntityId)) return true;
        }
        return false;
    }

    public static List<Process> getAllProcessByDocumentId(String metaEntityId) {
        List<Process> processes = new ArrayList<Process>(0);
        for (ProcessEntityBinding binding : procEntityBindings) {
            if (binding.getMetaEntityId().equalsIgnoreCase(metaEntityId)) {
                for (Process proc : processList) {
                    if (proc.getProcessCode().equalsIgnoreCase(binding.getProcessCode())) {
                        processes.add(proc);
                    }
                }
            }
        }
        return processes;
    }

    public static List<Process> getAllChildProcessByDocumentId(String metaEntityId) {

        Set<MetaEntity> childSet = MetaDataRegistry.getMetaEntity(metaEntityId).getChildList();
        List<Process> processes = new ArrayList<Process>(0);
        ;

        for (MetaEntity ent : childSet) {
            processes.addAll(getAllProcessByDocumentId(ent.getId()));
        }

        return processes;
    }

    public static State getStateFromProcess(String stateCode, String processCode) {
        return ProcessCatalog.getProcessByProcessCode(processCode).getState(stateCode);
    }

    public static ProcessEntityBinding getProcessEntityBindingByDocumentId(String metaEntityId) {

        ProcessEntityBinding entityBinding = null;
        for (ProcessEntityBinding binding : procEntityBindings) {
            if (binding.getMetaEntityId().equalsIgnoreCase(metaEntityId)) {
                return binding;
            }
        }
        return entityBinding;
    }


}
