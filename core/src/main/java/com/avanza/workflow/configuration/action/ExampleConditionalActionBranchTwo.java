package com.avanza.workflow.configuration.action;

import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessInstance;

public class ExampleConditionalActionBranchTwo implements Action {

    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {

        ActionExecutionResult result = new ActionExecutionResult();
        System.out.println("Branch Two Action called...");
        return result;
    }

}
