/**
 * This class is used to validate the transaction before proceeding
 * with the activity on any document. It takes the process containing
 * DataObject with all attribute values as well as it retrieves the
 * transaction parameters against document entity Id in Key/Value format
 * from the database to perform the transaction.
 */
package com.avanza.workflow.configuration.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.core.constants.ThreadContextLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.OperatorType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.ui.util.ResourceUtil;
import com.avanza.workflow.configuration.TaskActionParameter;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.ValidatorAction;
import com.avanza.workflow.execution.ValidatorActionResult;

/**
 * @author rehan.ahmed
 */
public class TransactionValidator extends ValidatorAction {

    private static Logger logger = Logger.getLogger(TransactionValidator.class);

    private DataBroker broker;
    private Map<String, String> keyValueMap;
    private final String KEY_MAPPING = "MAPPING";
    private final String KEY_SUCCESS = "SUCCESS";
    private final String KEY_TRAN_ENTITY = "TRAN_ENTITY";
    private DataObject response;

    @Override
    /**
     * This method is used to retrieve the Transaction parameters
     * against document entity and task. It calls the transaction w.r.t to parameter
     * value defined with associated task. Additionally it checks the transaction
     * condition value to proceed the activity or not.
     */
    public ValidatorActionResult act(ProcessInstance process,
                                     ActionBinding action, Object context) {

        ValidatorActionResult result = new ValidatorActionResult();

        try {
            if (process != null) {
                logger.logInfo("TransactionValidator -> process not NULL.");
                Search search = new Search(TaskActionParameter.class);
                search.addCriterion(new SimpleCriterion<Object>(OperatorType.Eq, "taskActionId", null, action.getId()));
                search.addCriterion(new SimpleCriterion<Object>(OperatorType.Eq, "documentId", null, process.getBinding().getMetaEntityId()));

                //Get transaction parameter
                broker = DataRepository.getBroker(TaskActionParameter.class.getName());
                List<TaskActionParameter> taskActionParams = broker.find(search);
                keyValueMap = new HashMap<String, String>();

                for (TaskActionParameter param : taskActionParams) {
                    keyValueMap.put(param.getParamKey(), param.getParamValue());
                }

                //Transaction
                MetaEntity entity = MetaDataRegistry.getMetaEntity((String) keyValueMap.get(KEY_TRAN_ENTITY));
                response = new DataObject(entity);
                broker = DataRepository.getBroker("Unison.Transaction");
                broker.add(response);

                this.SetResponseFieldToDocumentField(keyValueMap.get(KEY_MAPPING), process);
                if (ApplicationContext.getContext().get(ThreadContextLookup.ExternalResponseCode.toString()) != null) {
                    if (ApplicationContext.getContext().get(ThreadContextLookup.ExternalResponseCode.toString()).equals(keyValueMap.get(KEY_SUCCESS))) {
                        result.setSuccess(Boolean.TRUE);
                        return result;
                    }
                }
            }
        } catch (Exception e) {
            logger.logError(e.getMessage());
        }
        result.setSuccess(Boolean.FALSE);
        result.setMessage(ResourceUtil.getInstance().getProperty("external_response_messages", ApplicationContext.getContext().get(ThreadContextLookup.ExternalResponseCode.toString())));
        return result;
    }

    /**
     * This method is used to parse the parameter value by specified character
     * and set the source field into destination field.
     */
    private void SetResponseFieldToDocumentField(Object fieldMapVal, ProcessInstance instance) {
        logger.logInfo("SetResponseFieldToDocumentField(...)");
        try {
            if (fieldMapVal != null) {
                String[] commaSeprated = fieldMapVal.toString().split(",");
                if (commaSeprated.length > 0) {
                    for (String mappings : commaSeprated) {
                        if (mappings.contains("|")) {
                            String[] keyValueMapping = mappings.split("\\|");
                            instance.getContext().setAttribute(keyValueMapping[1], response.getValue(keyValueMapping[0]));
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.logError(e.getMessage());
        }
    }

}
