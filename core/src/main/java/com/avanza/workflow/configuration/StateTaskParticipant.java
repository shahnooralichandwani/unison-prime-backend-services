package com.avanza.workflow.configuration;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.StringHelper;

/**
 * WfStateTaskParticipant entity. @author MyEclipse Persistence Tools
 */

public class StateTaskParticipant extends DbObject implements Serializable {


    private static final long serialVersionUID = 1242534L;
    // Fields

    private StateTaskParticipantId id;
    private String userIds;
    private String createdBy;
    private Timestamp createdOn;
    private String updatedBy;
    private Timestamp updatedOn;

    // Constructors

    /**
     * default constructor
     */
    public StateTaskParticipant() {
    }

    /**
     * full constructor
     */
    public StateTaskParticipant(StateTaskParticipantId id,
                                String createdBy, Timestamp createdOn, String updatedBy,
                                Timestamp updatedOn) {
        this.id = id;
    }

    // Property accessors

    public StateTaskParticipantId getId() {
        return this.id;
    }

    public void setId(StateTaskParticipantId id) {
        this.id = id;
    }

    public String getUserIds() {
        return userIds;
    }

    public void setUserIds(String userIds) {
        this.userIds = userIds;
    }

    public List<String> getUserIdsList() {
        List<String> users = new ArrayList<String>(0);
        if (StringHelper.isNotEmpty(userIds))
            users = Arrays.asList(userIds.split(","));
        return users;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof StateTaskParticipant) {
            StateTaskParticipant compairParticipant = (StateTaskParticipant) obj;
            if (compairParticipant.getId().getProcessAllocId().equals(this.getId().getProcessAllocId()) &&
                    compairParticipant.getId().getProcessCode().equals(this.getId().getProcessCode()) &&
                    compairParticipant.getId().getRoleId().equals(this.getId().getRoleId()) &&
                    compairParticipant.getId().getStateTaskCode().equals(this.getId().getStateTaskCode()) &&
                    compairParticipant.getId().getStateCode().equals(this.getId().getStateCode())) {
                return true;
            }
        }
        return false;
    }
}