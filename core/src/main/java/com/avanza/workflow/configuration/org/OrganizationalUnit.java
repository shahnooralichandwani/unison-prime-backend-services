package com.avanza.workflow.configuration.org;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DbObject;
import com.avanza.core.interceptor.Auditable;
import com.avanza.core.meta.audit.OrgRoleAudit;
import com.avanza.core.meta.audit.OrgRoleAuditId;
import com.avanza.core.meta.audit.OrgUnitAudit;
import com.avanza.core.meta.audit.OrgUnitAuditId;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TreeObject;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;

/**
 * This class represents any logical or physical group of an organization.
 *
 * @author hmirza
 */
public class OrganizationalUnit extends TreeObject implements Comparable, Auditable {

    private static final long serialVersionUID = 1075298696631837312L;

    private String orgUnitId;

    private OrganizationalUnitType orgUnitType;

    private OrganizationalUnit parentOrgUnit;

    private String orgNamePrm;

    private String orgNameSec;

    private String orgFullName;

    private long hierarchyLevel;

    private String otherCode1;

    private String otherCode2;

    private String otherCode3;

    private String orgAddress;

    private String orgPhone;

    private String orgFax;

    private String orgEmail;

    private String orgShortNamePrm;

    private String orgShortNameSec;

    private String value;

    //----Added by Nida - for Disable Organization via Designer
    private Boolean isActive;

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    private Set orgUnits = new HashSet(0);

    private Set orgUserRoles = new HashSet(0);

    // Constructors

    /**
     * default constructor
     */
    public OrganizationalUnit() {
        super();
        orgUnitId = StringHelper.EMPTY;
        orgUnitType = new OrganizationalUnitType();
        parentOrgUnit = new OrganizationalUnit(StringHelper.EMPTY, true);
    }

    public OrganizationalUnit(String id) {
        super(id);
        orgUnitId = id;
        orgUnitType = new OrganizationalUnitType();
        parentOrgUnit = new OrganizationalUnit(StringHelper.EMPTY, true);
    }

    public OrganizationalUnit(String id, boolean flag) {
        if (flag)
            orgUnitId = id;
        else
            orgUnitId = null;
    }

    /**
     * minimal constructor
     */
    public OrganizationalUnit(String orgUnitId, OrganizationalUnitType orgUnitType, OrganizationalUnit orgUnit, String orgNamePrm, long hierarchyLevel) {
        this.orgUnitId = orgUnitId;
        this.orgUnitType = orgUnitType;
        this.parentOrgUnit = orgUnit;
        this.orgNamePrm = orgNamePrm;
        this.hierarchyLevel = hierarchyLevel;
    }

    /**
     * full constructor
     */
    public OrganizationalUnit(String orgUnitId, OrganizationalUnitType orgUnitType, OrganizationalUnit orgUnit, String orgNamePrm, String orgNameSec,
                              String orgFullName, long hierarchyLevel, Set orgUnits, Set orgUserRoles) {
        this.orgUnitId = orgUnitId;
        this.orgUnitType = orgUnitType;
        this.parentOrgUnit = orgUnit;
        this.orgNamePrm = orgNamePrm;
        this.orgNameSec = orgNameSec;
        this.orgFullName = orgFullName;
        this.hierarchyLevel = hierarchyLevel;
        this.orgUnits = orgUnits;
        this.orgUserRoles = orgUserRoles;
    }

    // Property accessors
    public String getOrgUnitId() {
        return this.orgUnitId;
    }

    public void setOrgUnitId(String orgUnitId) {
        this.orgUnitId = orgUnitId;
        super.id = orgUnitId;
    }

    public OrganizationalUnitType getOrgUnitType() {
        return this.orgUnitType;
    }

    public void setOrgUnitType(OrganizationalUnitType orgUnitType) {
        this.orgUnitType = orgUnitType;
    }

    public OrganizationalUnit getParentOrgUnit() {
        return this.parentOrgUnit;
    }

    public void setParentOrgUnit(OrganizationalUnit orgUnit) {
        this.parentOrgUnit = orgUnit;
    }

    public String getOrgNamePrm() {
        return this.orgNamePrm;
    }

    public void setOrgNamePrm(String orgNamePrm) {
        this.orgNamePrm = orgNamePrm;
    }

    public String getOrgNameSec() {
        return this.orgNameSec;
    }

    public void setOrgNameSec(String orgNameSec) {
        this.orgNameSec = orgNameSec;
    }

    public String getOrgFullName() {
        return this.orgFullName;
    }

    public void setOrgFullName(String orgFullName) {
        this.orgFullName = orgFullName;
    }

    public long getHierarchyLevel() {
        return this.hierarchyLevel;
    }

    public void setHierarchyLevel(long hierarchyLevel) {
        this.hierarchyLevel = hierarchyLevel;
    }

    public Set getOrgUnits() {
        return this.orgUnits;
    }

    public void setOrgUnits(Set orgUnits) {
        this.orgUnits = orgUnits;
    }

    public Set getOrgUserRoles() {
        return this.orgUserRoles;
    }

    public void setOrgUserRoles(Set orgUserRoles) {
        this.orgUserRoles = orgUserRoles;
    }

    public String getOtherCode1() {
        return otherCode1;
    }

    public void setOtherCode1(String otherCode1) {
        this.otherCode1 = otherCode1;
    }

    public String getOtherCode2() {
        return otherCode2;
    }

    public void setOtherCode2(String otherCode2) {
        this.otherCode2 = otherCode2;
    }

    public String getOtherCode3() {
        return otherCode3;
    }

    public void setOtherCode3(String otherCode3) {
        this.otherCode3 = otherCode3;
    }

    public String getOrgAddress() {
        return orgAddress;
    }

    public void setOrgAddress(String orgAddress) {
        this.orgAddress = orgAddress;
    }

    public String getOrgPhone() {
        return orgPhone;
    }

    public void setOrgPhone(String orgPhone) {
        this.orgPhone = orgPhone;
    }

    public String getOrgFax() {
        return orgFax;
    }

    public void setOrgFax(String orgFax) {
        this.orgFax = orgFax;
    }

    public String getOrgEmail() {
        return orgEmail;
    }

    public void setOrgEmail(String orgEmail) {
        this.orgEmail = orgEmail;
    }

    public String getOrgShortNamePrm() {
        return orgShortNamePrm;
    }

    public void setOrgShortNamePrm(String orgShortNamePrm) {
        this.orgShortNamePrm = orgShortNamePrm;
    }

    public String getOrgShortNameSec() {
        return orgShortNameSec;
    }

    public void setOrgShortNameSec(String orgShortNameSec) {
        this.orgShortNameSec = orgShortNameSec;
    }

    @Override
    public String getPrimaryName() {
        return orgNamePrm;
    }

    public void setPrimaryName(String orgNamePrm) {
        this.orgNamePrm = orgNamePrm;
    }

    @Override
    public String getSecondaryName() {
        return orgNameSec;
    }

    public void setSecondaryName(String orgNameSec) {
        this.orgNameSec = orgNameSec;
    }

    @Override
    public TreeObject clone() {
        OrganizationalUnit orgUnit = new OrganizationalUnit(this.id);
        orgUnit.copyValues(this);

        // Now need to copy their children also.
        for (TreeObject tobj : this.childList) {
            TreeObject cloned = ((OrganizationalUnit) tobj).clone();
            ((OrganizationalUnit) cloned).parentOrgUnit = orgUnit;
            orgUnit.childList.add(cloned);
        }
        return orgUnit;
    }

    public OrganizationalUnit getParentNode() {
        return parentOrgUnit;
    }

    public void setParentNode(OrganizationalUnit parentOrgUnit) {
        this.parentOrgUnit = parentOrgUnit;
    }

    @Override
    public void copyValues(DbObject copyFrom) {

        if (!(copyFrom instanceof OrganizationalUnit)) return;

        OrganizationalUnit orgUnit = (OrganizationalUnit) copyFrom;

        super.copyValues(copyFrom);
        this.orgNamePrm = orgUnit.orgNamePrm;
        this.orgNameSec = orgUnit.orgNameSec;
        this.parentOrgUnit = orgUnit.parentOrgUnit;
    }

    @Override
    protected TreeObject createObject(String id) {

        return new OrganizationalUnit(id);
    }

    @Override
    public boolean equals(Object obj) {

        OrganizationalUnit orgUnit = (OrganizationalUnit) obj;

        if (orgUnit.getId().equalsIgnoreCase(this.getId()) && orgUnit.getKey().equalsIgnoreCase(this.getKey()))
            return true;

        return false;
    }

    @Override
    public <T extends TreeObject> void add(T item) {
        super.add(item);
        ((OrganizationalUnit) item).parentOrgUnit = this;
    }

    @Override
    public int hashCode() {
        return orgUnitId.hashCode();
    }

    @Override
    public String getValue() {
        value = this.orgNamePrm;
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isRoot() {
        return parentOrgUnit == null;
    }

    public String toString() {
        return (StringHelper.isEmpty(orgUnitId)) ? StringHelper.EMPTY : orgUnitId;
    }

    public int compareTo(Object arg0) {

        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        LocaleInfo currentLocale = webContext.getAttribute(SessionKeyLookup.CurrentLocale);
        if (currentLocale.isPrimary())
            return getOrgNamePrm().compareTo(((OrganizationalUnit) arg0).getOrgNamePrm());
        else
            return ((getOrgNameSec() != null && ((OrganizationalUnit) arg0).getOrgNameSec() != null)
                    ? getOrgNameSec().compareTo(((OrganizationalUnit) arg0).getOrgNameSec())
                    : 0);
    }


    @Override
    public Object getAuditEntry(Short revtype) {
        OrgUnitAudit organizationalUnitAudit = null;
        try {


            Random rand = new Random();

            organizationalUnitAudit = new OrgUnitAudit();
            organizationalUnitAudit.setRevtype(revtype);

            BeanUtils.copyProperties(organizationalUnitAudit, this);
            organizationalUnitAudit.setOrgUnitAuditId(new OrgUnitAuditId(this.orgUnitId, rand.nextInt()));

            //secUserV.setId(new SecUserVId(this.loginId, CounterUtils.getNextAuditTableKey()));
        } catch (Exception e) {
            organizationalUnitAudit = null;
        }
        return organizationalUnitAudit;
    }


}
