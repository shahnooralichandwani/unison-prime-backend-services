package com.avanza.workflow.configuration.action;

import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Map;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.ui.meta.MetaViewAttribute;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

public class SendMessageCustomerTaskAction extends TaskAllocatorAction {


    private static String CUSTOMER_ENTITY = "Unison.Customer";
    private static final Logger logger = Logger.getLogger(NotifyCustomer.class);

    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {

        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext ctx = processInst.getContext();
        ProcessEntityBinding metaEntId = processInst.getBinding();
        DataObject obj = processInst.getDataObject();
     /*   MetaViewAttribute attrib = MetaDataRegistry.
        Object value = ContextAttributes.get(attrib.getFullMetaAttributeId());*/
        String rim = (String) ctx.getAttribute("CUST_RELATION_NUM");
        MetaEntity entity = MetaDataRegistry.getEntityBySystemName(CUSTOMER_ENTITY);
        DataBroker broker = DataRepository.getBroker(entity.getSystemName());
        String emailContent = obj.getAsString("STRING_VAL2");
        String emailSubject = obj.getAsString("STRING_VAL1");

        MetaEntity instanceEntity = MetaDataRegistry.getMetaEntity(ctx.getDocumentId());
        MetaEntity parentEntity = MetaDataRegistry.getNonAbstractParentEntity(instanceEntity);
        HashMap map = new HashMap();
        map.put(parentEntity.getSystemName().replace('.', '_'), ctx.getValues());
        map.put("CurrentDate", new Date());

//	            if(obj.size()>0)
        {
//	               
            String cusomerEmail = (String) ctx.getAttribute("CUST_EMAIL");
//	              
            if (StringHelper.isNotEmpty(cusomerEmail)) {
                System.out.println("------------------ CUSTOMER ID: " + rim + "------------------");
                System.out.println("------------------------ Sending Email to Customer " + cusomerEmail + " -----------------------------");
                NotificationSender.sendEmail(emailSubject, emailContent, cusomerEmail, null, InboundLoader.getInstance().getAckChannel("Email"));
                //NotificationSender.sende
            }
        }


        return result;
    }


}