package com.avanza.workflow.configuration.org;

import java.sql.Timestamp;

import com.avanza.core.data.DbObject;

/**
 * OrgRoleCategory entity. @author MyEclipse Persistence Tools
 * anam.siddiqui
 * below file is added to provide an extension to work-flow assignment based on product being tagged to a role.
 * as for now required in HBL -LMS module.
 */

public class OrgRoleCategory extends DbObject implements java.io.Serializable {

    // Fields

    private String orgRoleCatId;
    private String treenodeId;
    private String roleId;
    private String orgUnitId;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;

    // Constructors

    /**
     * default constructor
     */
    public OrgRoleCategory() {
    }

    /**
     * minimal constructor
     */
    public OrgRoleCategory(String orgRoleCatId) {
        this.orgRoleCatId = orgRoleCatId;
    }

    /**
     * full constructor
     */
    public OrgRoleCategory(String orgRoleCatId, String treenodeId,
                           String roleId, String orgUnitId, Timestamp createdOn,
                           String createdBy, Timestamp updatedOn, String updatedBy) {
        this.orgRoleCatId = orgRoleCatId;
        this.treenodeId = treenodeId;
        this.roleId = roleId;
        this.orgUnitId = orgUnitId;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
    }

    // Property accessors

    public String getOrgRoleCatId() {
        return this.orgRoleCatId;
    }

    public void setOrgRoleCatId(String orgRoleCatId) {
        this.orgRoleCatId = orgRoleCatId;
    }

    public String getTreenodeId() {
        return this.treenodeId;
    }

    public void setTreenodeId(String treenodeId) {
        this.treenodeId = treenodeId;
    }

    public String getRoleId() {
        return this.roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getOrgUnitId() {
        return this.orgUnitId;
    }

    public void setOrgUnitId(String orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

}