package com.avanza.workflow.configuration.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.avanza.core.sdo.Attribute;
import com.avanza.core.security.User;
import com.avanza.workflow.configuration.EventType;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.ProcessParticipant;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.Transition;
import com.avanza.workflow.configuration.WorkflowConfigurationException;
import com.avanza.workflow.configuration.action.ActionBinding;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.OrganizationalUnit;
import com.avanza.workflow.escalation.EscalationStrategy;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;
import com.avanza.workflow.execution.TaskContext;

public class SetEformCordinator extends TaskAllocatorAction {

    public Object act(ProcessInstance processInst, ActionBinding action, Object context) {


        ActionExecutionResult result = new ActionExecutionResult();
        String[] custBranchCode = null;
        String CORD_ROLE = "0000000008";
        List<String> currentUser = new ArrayList<String>(0);
        Map objDetails = null;
        List<String> str = new ArrayList<String>(0);
        try {
            objDetails = (Map) processInst.getContext().getValues();
            String branchCode = (String) objDetails.get("CURRENT_ORG_UNIT").toString();
            currentUser.add(objDetails.get("CALCULATED_ACTOR").toString().trim());
            if (!currentUser.contains(objDetails.get("CURRENT_ACTOR").toString().trim()))
                currentUser.add(objDetails.get("CURRENT_ACTOR").toString().trim());
            custBranchCode = branchCode.split("-");

        } catch (Exception e) {
            System.out.print("");
        }

        List<User> calculatedUsers = OrganizationManager.getAllUsers(CORD_ROLE);
        for (User u : calculatedUsers) {
            List<OrganizationalUnit> unitList = OrganizationManager.getUserOrgUnits(u.getLoginId(), CORD_ROLE);
            for (OrganizationalUnit unit : unitList) {
                if (unit.getOrgUnitId().equalsIgnoreCase(custBranchCode[0].trim()) && !str.contains(u.getLoginId())) {
                    str.add(u.getLoginId());
                    processInst.setCurrentActor(str);
                }
            }
        }
        processInst.setCalculatedActor(currentUser);
        result.setChangePossible(true);
        return result;
    }

    private Set<ProcessParticipant> filterParticipantsList(Set<ProcessParticipant> procParticipants, List<String> roles) {
        Set<ProcessParticipant> finalParticipant = new HashSet<ProcessParticipant>();
        for (String role : roles) {
            for (ProcessParticipant processPart : procParticipants) {
                if (processPart.getRoleId().equalsIgnoreCase(role)) {
                    if (!finalParticipant.contains(processPart)) {
                        finalParticipant.add(processPart);
                    }
                }
            }
        }
        return finalParticipant;
    }


}
