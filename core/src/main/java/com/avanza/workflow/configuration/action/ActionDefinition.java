package com.avanza.workflow.configuration.action;

import com.avanza.core.data.DbObject;

/**
 * @author hmirza
 */
public class ActionDefinition extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String actionCode;

    private String actionName;

    private String actionType;

    private String actionHandler;

    private String deployModule;

    public String getDeployModule() {
        return deployModule;
    }

    public void setDeployModule(String deployModule) {
        this.deployModule = deployModule;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getActionHandler() {
        return actionHandler;
    }

    public void setActionHandler(String actionHandler) {
        this.actionHandler = actionHandler;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }


    public String getActionType() {
        return actionType;
    }


    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}
