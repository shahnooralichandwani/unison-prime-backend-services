package com.avanza.workflow.configuration;

/**
 * WfStateTaskParticipantId entity. @author MyEclipse Persistence Tools
 */

public class StateTaskParticipantId implements java.io.Serializable {

    // Fields

    private String stateTaskCode;
    private String stateCode;
    private String processAllocId;
    private String processCode;
    private String roleId;

    // Constructors

    /**
     * default constructor
     */
    public StateTaskParticipantId() {
    }

    /**
     * full constructor
     */
    public StateTaskParticipantId(String stateTaskCode, String stateCode,
                                  String processAllocId, String processCode, String roleId) {
        this.stateTaskCode = stateTaskCode;
        this.stateCode = stateCode;
        this.processAllocId = processAllocId;
        this.processCode = processCode;
        this.roleId = roleId;
    }

    // Property accessors

    public String getStateTaskCode() {
        return this.stateTaskCode;
    }

    public void setStateTaskCode(String stateTaskCode) {
        this.stateTaskCode = stateTaskCode;
    }

    public String getStateCode() {
        return this.stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getProcessAllocId() {
        return this.processAllocId;
    }

    public void setProcessAllocId(String processAllocId) {
        this.processAllocId = processAllocId;
    }

    public String getProcessCode() {
        return this.processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getRoleId() {
        return this.roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof StateTaskParticipantId))
            return false;
        StateTaskParticipantId castOther = (StateTaskParticipantId) other;

        return ((this.getStateTaskCode() == castOther.getStateTaskCode()) || (this
                .getStateTaskCode() != null
                && castOther.getStateTaskCode() != null && this
                .getStateTaskCode().equals(castOther.getStateTaskCode())))
                && ((this.getStateCode() == castOther.getStateCode()) || (this
                .getStateCode() != null
                && castOther.getStateCode() != null && this
                .getStateCode().equals(castOther.getStateCode())))
                && ((this.getProcessAllocId() == castOther.getProcessAllocId()) || (this
                .getProcessAllocId() != null
                && castOther.getProcessAllocId() != null && this
                .getProcessAllocId().equals(
                        castOther.getProcessAllocId())))
                && ((this.getProcessCode() == castOther.getProcessCode()) || (this
                .getProcessCode() != null
                && castOther.getProcessCode() != null && this
                .getProcessCode().equals(castOther.getProcessCode())))
                && ((this.getRoleId() == castOther.getRoleId()) || (this
                .getRoleId() != null
                && castOther.getRoleId() != null && this.getRoleId()
                .equals(castOther.getRoleId())));
    }

    public int hashCode() {
        int result = 17;

        result = 37
                * result
                + (getStateTaskCode() == null ? 0 : this.getStateTaskCode()
                .hashCode());
        result = 37 * result
                + (getStateCode() == null ? 0 : this.getStateCode().hashCode());
        result = 37
                * result
                + (getProcessAllocId() == null ? 0 : this.getProcessAllocId()
                .hashCode());
        result = 37
                * result
                + (getProcessCode() == null ? 0 : this.getProcessCode()
                .hashCode());
        result = 37 * result
                + (getRoleId() == null ? 0 : this.getRoleId().hashCode());
        return result;
    }

}