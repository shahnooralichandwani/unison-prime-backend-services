package com.avanza.workflow.configuration.action;

import java.util.Date;

import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.logger.ParkedStateManager;

/**
 * @author shoaib.rehman
 */

public class EnterParkedStateAction implements Action {

    public Object act(ProcessInstance processInstance, ActionBinding action, Object context) {
        //TODO insert an entry in PARKED_STATE_LOG table here
        ParkedStateManager.insertParkedStateLog(processInstance.getInstanceId(), new Date());
        return null;
    }

}
