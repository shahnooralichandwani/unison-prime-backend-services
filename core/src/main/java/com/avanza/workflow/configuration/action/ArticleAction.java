/**
 *
 */
package com.avanza.workflow.configuration.action;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

//import javax.faces.context.FacesContext;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.web.config.WebContext;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.OrganizationalUnit;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;

/**
 * @author rehan.ahmed
 *
 */
public class ArticleAction { //extends TaskAllocatorAction{
/*
	@Override
	public Object act(ProcessInstance process, ActionBinding action,
			Object context) {
		ActionExecutionResult result = new ActionExecutionResult();		
		ProcessContext ctx = process.getContext();
		WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        DataObject customer = webCtx.getAttribute(SessionKeyLookup.CURRENT_CUSTOMER_KEY);
        String currentState = process.getCurrentState().getStateCode();
        
   
        if(currentState != null && currentState.equals("0000000112")){
        	// Update PUBLISHED_DATE of ARTICLE
            process.getContext().setAttribute("PUBLISHED_DATE", Calendar.getInstance().getTime());
            result.setChangePossible(true);
    		return result;
        }else{
        	//State Participant
            ProcessEntityBinding binding = process.getBinding();
    		Set<StateParticipant> participantList = binding.getStateParticipantList();
    		
            //Assign to maker on initiation
            UserDbImpl cUser = (UserDbImpl) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SessionKeyLookup.CURRENT_USER_KEY);
            //
            
            for(StateParticipant participant : participantList){
            	if(participant.getStateCode().equalsIgnoreCase(currentState)){
            		List<OrganizationalUnit> uUnits= OrganizationManager.getUserOrgUnits(cUser.getLoginId(), participant.getRoleId());
            		if(uUnits != null && uUnits.size() > 0){
            			process.getContext().setCurrentOrgUnit(uUnits.get(0).getOrgUnitId());
            		}        		
            	}
            }		
            process.getContext().setCurrentActor(cUser.getLoginId());
            process.getContext().setCalculatedActor(cUser.getLoginId());
            result.setChangePossible(true);
    		return result;
        }
        
        
        
	}

 */

}
