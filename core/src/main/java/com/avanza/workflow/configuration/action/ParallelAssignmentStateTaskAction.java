package com.avanza.workflow.configuration.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.constants.ThreadContextLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.Expression;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;
import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.User;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.web.config.WebContext;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.ProcessParticipant;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskAllocatorAction;
import com.avanza.workflow.execution.TaskContext;
import com.avanza.workflow.execution.TaskLoadManager;

/**
 * @author Khawaja Muhammad Aamir
 * @desc Allocate customer complaints to complaint teams
 */
public class ParallelAssignmentStateTaskAction extends TaskAllocatorAction {

    public Object act(ProcessInstance processInst, ActionBinding action,
                      Object context) {
        ActionExecutionResult result = new ActionExecutionResult();
        String NEXT_STATE = action.getParsedActionParams().get("NEXT_STATE");
        ProcessContext ctx = processInst.getContext();
        WebContext webCtx = ApplicationContext.getContext().get(
                WebContext.class);
        String userId = ((UserDbImpl) webCtx
                .getAttribute(SessionKeyLookup.CURRENT_USER_KEY)).getLoginId();
        Boolean isParallel = (Boolean) ctx.getAttribute("IS_PARALLEL");
        String assignerId = (String) ctx.getAttribute("PARALLEL_ASSIGNER");
        if (isParallel) {
            List<String> currentActors = processInst.getCurrentActor();
            if (currentActors.size() > 1) {
                if (currentActors.contains(userId)) {
                    currentActors.remove(userId);
                    processInst.setCurrentActor(currentActors);
                    processInst.setCalculatedActor(currentActors);
                }
            } else {
                ctx.setCurrentActor(assignerId);
                ctx.setCalculatedActor(assignerId);
                currentActors.clear();
                currentActors.add(assignerId);
                processInst.setCurrentActor(currentActors);
                processInst.setCalculatedActor(currentActors);
                ctx.setAttribute("IS_PARALLEL", false);
                processInst.moveToState(NEXT_STATE, (TaskContext) context);
            }
        }
        result.setChangePossible(true);
        return result;
    }
}
