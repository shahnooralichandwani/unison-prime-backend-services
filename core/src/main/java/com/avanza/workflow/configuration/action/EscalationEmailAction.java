/**
 *
 */
package com.avanza.workflow.configuration.action;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.Wrapper;
import com.avanza.notificationservice.notification.NotificationSender;
import com.avanza.workflow.escalation.EscalationManager;
import com.avanza.workflow.escalation.ExecutionStrategyContext;
import com.avanza.workflow.execution.Action;
import com.avanza.workflow.execution.ActionExecutionResult;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessInstance;

/**
 * @author faraz
 *
 */
public class EscalationEmailAction implements Action {
    private Logger logger = Logger
            .getLogger(EscalationEmailAction.class);

    private Map<String, User> calculateActors(ProcessInstance processInstance,
                                              ExecutionStrategyContext strategyContext) {

        // parents
        Map<String, User> users = EscalationManager.calculateParentActors(
                strategyContext, processInstance);

        // merge current actors
        for (String id : Convert.toList(strategyContext.getCurrentActors())) {
            users.put(id, SecurityManager.getUser(id));
        }

        return users;
    }

    private void initializeEmail(ProcessInstance processInstance,
                                 String templateId, Collection<User> users,
                                 Wrapper<String> subjectWrapper, Wrapper<String> bodyWrapper) {
        ProcessContext processContext = processInstance.getContext();
        MetaEntity instanceEntity = MetaDataRegistry
                .getMetaEntity(processContext.getDocumentId());
        HashMap<Object, Object> map = new HashMap<Object, Object>();
        MetaEntity parentEntity = MetaDataRegistry
                .getNonAbstractParentEntity(instanceEntity);

        // manager names.
        String managerNames = "";
        for (User user : users) {
            managerNames += user.getFullName() + "/";
        }
        map.put("ManagerName", managerNames);

        map.put(parentEntity.getSystemName().replace('.', '_'), processContext
                .getValues());
        map.put("CurrentDate", new Date());

        map.put("ProcessTAT", processInstance.getProcessTAT());

        bodyWrapper.wrap(ApplicationLoader.getTemplateConfig().processTemplate(
                templateId + "_content", map));
        subjectWrapper.wrap(ApplicationLoader.getTemplateConfig()
                .processTemplate(templateId + "_subject", map));
    }

    public Object act(ProcessInstance processInstance, ActionBinding action,
                      Object context) {
        System.out.println("Strategy Execution Email Action Fired");
        ActionExecutionResult result = new ActionExecutionResult();
        ProcessContext processContext = processInstance.getContext();
        ExecutionStrategyContext strategyContext = (ExecutionStrategyContext) processContext
                .getNonPrsistableAttribute("ExecutionStrategyContext");
        if (strategyContext == null) {
            logger
                    .logWarning("no strategy context is attacted with process context. getNonPrsistableAttribute('ExecutionStrategyContext')");
            return result;
        }

        Map<String, String> parsedParams = action.getParsedActionParams();
        String templateId = parsedParams.get("templateId");
        String channelId = parsedParams.get("channelId");

        Collection<User> users = calculateActors(processInstance,
                strategyContext).values();

        if (StringHelper.isNotEmpty(templateId)
                && StringHelper.isNotEmpty(channelId)) {
            Wrapper<String> subjectWrapper = new Wrapper<String>();
            Wrapper<String> bodyWrapper = new Wrapper<String>();
            initializeEmail(processInstance, templateId, users, subjectWrapper,
                    bodyWrapper);
            //changed email sending process -- shoaib.rehman
            StringBuffer emailBuffer = new StringBuffer();
            for (User user : users) {
                if (emailBuffer.length() > 0)
                    emailBuffer.append(",");
                emailBuffer.append(user.getEmailUrl());
            }
            if (emailBuffer.length() > 0)
                NotificationSender.sendEmail(subjectWrapper.unwrap(),
                        bodyWrapper.unwrap(), emailBuffer.toString(), null, channelId);
        }
        return result;
    }
}
