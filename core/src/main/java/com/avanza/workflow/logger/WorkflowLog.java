package com.avanza.workflow.logger;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.avanza.core.calendar.CalendarCatalog;
import com.avanza.core.data.DbObject;
import com.avanza.core.util.Convert;
import com.avanza.workflow.configuration.State;


public class WorkflowLog extends DbObject implements Serializable {

    private String processCode;

    private Date logDateTime;

    private String refDocumentNumber;

    private String logId;

    private String metaEntityId;

    private String currentState;

    private String nextState;

    private String possibleActors;

    private String lockActor;

    private Date lockTime;

    private Date stateEntryTime;

    private Date stateLeavingTime;

    private long stateTime;

    private long processTime;

    private long processRemainingTime;

    private boolean isStateTatExpired;

    private boolean isProcessTatExpired;

    private String taskCode;

    private boolean isStartActivity;

    private boolean isEndActivity;

    private String finalNotes;

    private Date appointmentDate;

    // private Set<ActionLog> actionLogs = new HashSet<ActionLog>(0);

    // public Set<ActionLog> getActionLogs() {
    // return actionLogs;
    // }
    //
    //
    // public void setActionLogs(Set<ActionLog> actionLogs) {
    // this.actionLogs = actionLogs;
    // }
    //
    // private void addActionLog(ActionLog log) {
    // this.actionLogs.add(log);
    // }


    public Date getAppointmentDate() {
        return appointmentDate;
    }


    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }


    public void setStateTatExpired(boolean isStateTatExpired) {
        this.isStateTatExpired = isStateTatExpired;
    }


    public void setProcessTatExpired(boolean isProcessTatExpired) {
        this.isProcessTatExpired = isProcessTatExpired;
    }


    public void setStartActivity(boolean isStartActivity) {
        this.isStartActivity = isStartActivity;
    }


    public void setEndActivity(boolean isEndActivity) {
        this.isEndActivity = isEndActivity;
    }


    public void setIsProcessTatExpired(boolean isProcessTatExpired) {
        this.isProcessTatExpired = isProcessTatExpired;
    }


    public void setProcessRemainingTime(long processRemainingTime) {
        this.processRemainingTime = processRemainingTime;
    }


    public String getCurrentState() {
        return currentState;
    }


    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }


    public boolean getIsEndActivity() {
        return isEndActivity;
    }


    public void setIsEndActivity(boolean isEndActivity) {
        this.isEndActivity = isEndActivity;
    }


    public boolean getIsProcessTatExpired() {
        return isProcessTatExpired;
    }

    public void setIsProcessTatExpired(long processTat, Date date) {
        Date expiryTime = new Date(CalendarCatalog.getDefaultCalendar().getAdjustedTime(date, processTat));
        if (expiryTime.before(Calendar.getInstance().getTime()))
            this.isProcessTatExpired = true;
        else
            this.isProcessTatExpired = false;
    }

    public boolean getIsStartActivity() {
        return isStartActivity;
    }


    public void setIsStartActivity(boolean isStartActivity) {
        this.isStartActivity = isStartActivity;
    }


    public boolean getIsStateTatExpired() {
        return isStateTatExpired;
    }


    public void setIsStateTatExpired(boolean isStateTatExpired) {
        this.isStateTatExpired = isStateTatExpired;
    }


    public String getLockActor() {
        return lockActor;
    }


    public void setLockActor(String lockActor) {
        this.lockActor = lockActor;
    }


    public Date getLockTime() {
        return lockTime;
    }


    public void setLockTime(Date lockTime) {
        this.lockTime = lockTime;
    }


    public Date getLogDateTime() {
        return logDateTime;
    }


    public void setLogDateTime(Date logDateTime) {
        this.logDateTime = logDateTime;
    }


    public String getMetaEntityId() {
        return metaEntityId;
    }


    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }


    public String getNextState() {
        return nextState;
    }


    public void setNextState(String nextState) {
        this.nextState = nextState;
    }


    public String getPossibleActors() {
        return possibleActors;
    }


    public void setPossibleActors(String possibleActors) {
        this.possibleActors = possibleActors;
    }

    public void setPossibleActors(List<String> possibleActors) {
        this.possibleActors = Convert.toCSV(possibleActors);
    }

    public String getProcessCode() {
        return processCode;
    }


    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }


    public long getProcessTime() {
        return processTime;
    }


    public void setProcessTime(Date startdatetime) {
        long min = CalendarCatalog.getDefaultCalendar().getWorkingTime(startdatetime, Calendar.getInstance().getTime());
        this.processTime = min;
    }


    public String getRefDocumentNumber() {
        return refDocumentNumber;
    }


    public void setRefDocumentNumber(String refDocumentNumber) {
        this.refDocumentNumber = refDocumentNumber;
    }


    public Date getStateEntryTime() {
        return stateEntryTime;
    }


    public void setStateEntryTime(Date stateEntryTime) {
        this.stateEntryTime = stateEntryTime;
    }


    public Date getStateLeavingTime() {
        return stateLeavingTime;
    }


    public void setStateLeavingTime(Date stateLeavingTime) {
        this.stateLeavingTime = stateLeavingTime;
    }


    public long getStateTime() {
        return stateTime;
    }


    public void setStateTime(long stateTime) {
        this.stateTime = stateTime;
    }


    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public long getProcessRemainingTime() {
        return processRemainingTime;
    }

    public void setProcessRemainingTime(long processTat, Date date) {
        long expiry = CalendarCatalog.getDefaultCalendar().getAdjustedTime(date, processTat);
        long current = Calendar.getInstance().getTimeInMillis();
        Date expiryDate = new Date(expiry);
        Date currentDate = new Date(current);
        long min = CalendarCatalog.getDefaultCalendar().getWorkingTime(currentDate, expiryDate);
        this.processRemainingTime = min;
    }


    public void setProcessTime(long processTime) {
        this.processTime = processTime;
    }


    public void setStateTime(Date stateEntryTime) {
        long min = CalendarCatalog.getDefaultCalendar().getWorkingTime(stateEntryTime, Calendar.getInstance().getTime());
        this.stateTime = min;

    }


    public void setCurrentState(State currentState) {
        if (currentState != null)
            this.currentState = currentState.getStateCode();
        else
            this.currentState = "";
    }

    public void setIsEndActivity(State currentState) {
        if (currentState != null)
            this.isEndActivity = currentState.getEnd();
        else
            this.isEndActivity = false;
    }

    public void setIsStartActivity(State currentState) {
        if (currentState != null)
            this.isStartActivity = currentState.getStart();
        else
            this.isStartActivity = false;
    }


    public void setIsStateTatExpired(Date stateEntryTime, State currentState) {
        if (currentState != null) {
            long expiry = CalendarCatalog.getDefaultCalendar().getAdjustedTime(stateEntryTime, currentState.getTatInMinutes());
            long current = Calendar.getInstance().getTimeInMillis();
            Date expiryDate = new Date(expiry);
            Date currentDate = new Date(current);

            if (expiryDate.before(currentDate))
                this.isStateTatExpired = true;
            else
                this.isStateTatExpired = false;
        } else {
            isStateTatExpired = false;
        }

    }


    public String getFinalNotes() {
        return finalNotes;
    }


    public void setFinalNotes(String finalNotes) {
        this.finalNotes = finalNotes;
    }


    public String getLogId() {
        return logId;
    }


    public void setLogId(String logId) {
        this.logId = logId;
    }
}
