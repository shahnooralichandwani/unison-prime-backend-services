package com.avanza.workflow.logger;

import java.io.Serializable;
import java.util.Observable;

/**
 * @author shafique.rehman
 */
public class ActionObservable extends Observable implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private ActionLogDetail actionLogDetail;

    public void addDetail(ActionLogDetail actionLogDetail) {
        this.actionLogDetail = actionLogDetail;
        setChanged();
    }

    public ActionLogDetail getActionLogDetail() {
        return actionLogDetail;
    }

    public void setActionLogDetail(ActionLogDetail actionLogDetail) {
        this.actionLogDetail = actionLogDetail;
    }


}