package com.avanza.workflow.logger;

import java.io.Serializable;
import java.util.Date;

import com.avanza.core.data.DbObject;


public class ActionLog extends DbObject implements Serializable {

    private int seqNo;

    private String processCode;

    private Date logDateTime;

    private String refDocumentNumber;

    private String actionCode;


    public String getActionCode() {
        return actionCode;
    }


    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }


    public int getSeqNo() {
        return seqNo;
    }


    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }


    public Date getLogDateTime() {
        return logDateTime;
    }


    public void setLogDateTime(Date logDateTime) {
        this.logDateTime = logDateTime;
    }


    public String getProcessCode() {
        return processCode;
    }


    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }


    public String getRefDocumentNumber() {
        return refDocumentNumber;
    }


    public void setRefDocumentNumber(String refDocumentNumber) {
        this.refDocumentNumber = refDocumentNumber;
    }


}
