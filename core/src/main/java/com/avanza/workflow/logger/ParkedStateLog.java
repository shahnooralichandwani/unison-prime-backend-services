package com.avanza.workflow.logger;

import java.io.Serializable;
import java.util.Date;

import com.avanza.core.data.DbObject;

/**
 * @author shoaib.rehman
 */
public class ParkedStateLog extends DbObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5427316118047457695L;

    private String id;

    private String referenceDocument;

    private Date startTime;

    private Date endTime;

    private Float interval;

    private Boolean isProcessed;

    public ParkedStateLog() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ParkedStateLog(String id, String referenceDocument, Date startTime,
                          Date entTime, Float interval, boolean isProcessed) {
        super();
        this.id = id;
        this.referenceDocument = referenceDocument;
        this.startTime = startTime;
        this.endTime = entTime;
        this.interval = interval;
        this.isProcessed = isProcessed;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferenceDocument() {
        return referenceDocument;
    }

    public void setReferenceDocument(String referenceDocument) {
        this.referenceDocument = referenceDocument;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Float getInterval() {
        return interval;
    }

    public void setInterval(Float interval) {
        this.interval = interval;
    }

    public Boolean getIsProcessed() {
        return isProcessed;
    }

    public void setIsProcessed(Boolean isProcessed) {
        this.isProcessed = isProcessed;
    }

}
