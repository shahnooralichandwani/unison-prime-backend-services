package com.avanza.workflow.logger;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.avanza.core.calendar.CalendarCatalog;
import com.avanza.core.data.DbObject;
import com.avanza.core.util.Convert;
import com.avanza.workflow.configuration.State;


public class ActionLogDetail extends DbObject implements Serializable {


    /**
     * @author shafique.rehman
     */
    private static final long serialVersionUID = 1L;

    private String actionLogId;

    private String taskActionId;

    private String wfLogId;

    private String wfEscLogId;

    private String taskDetailId;

    private String refDocumentNumber;

    private String ExceptionMsg;

    private Date logDateTime;

    private String strValue1;
    private String strValue2;
    private String strValue3;
    private String strValue4;
    private String strValue5;
    private String strValue6;


    public String getActionLogId() {
        return actionLogId;
    }

    public void setActionLogId(String actionLogId) {
        this.actionLogId = actionLogId;
    }

    public String getTaskActionId() {
        return taskActionId;
    }

    public void setTaskActionId(String taskActionId) {
        this.taskActionId = taskActionId;
    }

    public String getWfLogId() {
        return wfLogId;
    }

    public void setWfLogId(String wfLogId) {
        this.wfLogId = wfLogId;
    }

    public String getWfEscLogId() {
        return wfEscLogId;
    }

    public void setWfEscLogId(String wfEscLogId) {
        this.wfEscLogId = wfEscLogId;
    }

    public String getTaskDetailId() {
        return taskDetailId;
    }

    public void setTaskDetailId(String taskDetailId) {
        this.taskDetailId = taskDetailId;
    }

    public String getRefDocumentNumber() {
        return refDocumentNumber;
    }

    public void setRefDocumentNumber(String refDocumentNumber) {
        this.refDocumentNumber = refDocumentNumber;
    }

    public String getExceptionMsg() {
        return ExceptionMsg;
    }

    public void setExceptionMsg(String exceptionMsg) {
        ExceptionMsg = exceptionMsg;
    }

    public Date getLogDateTime() {
        return logDateTime;
    }

    public void setLogDateTime(Date logDateTime) {
        this.logDateTime = logDateTime;
    }

    public String getStrValue1() {
        return strValue1;
    }

    public void setStrValue1(String strValue1) {
        this.strValue1 = strValue1;
    }

    public String getStrValue2() {
        return strValue2;
    }

    public void setStrValue2(String strValue2) {
        this.strValue2 = strValue2;
    }

    public String getStrValue3() {
        return strValue3;
    }

    public void setStrValue3(String strValue3) {
        this.strValue3 = strValue3;
    }

    public String getStrValue4() {
        return strValue4;
    }

    public void setStrValue4(String strValue4) {
        this.strValue4 = strValue4;
    }

    public String getStrValue5() {
        return strValue5;
    }

    public void setStrValue5(String strValue5) {
        this.strValue5 = strValue5;
    }

    public String getStrValue6() {
        return strValue6;
    }

    public void setStrValue6(String strValue6) {
        this.strValue6 = strValue6;
    }


}
