package com.avanza.workflow.logger;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.CounterUtils;
import com.avanza.core.util.StringHelper;
//import com.avanza.ui.designer.DesignerConfigCatalog;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.TaskContext;

public class WorkflowActivityLogger {

    public static WorkflowLog logWorkflowActivity(ProcessInstance instance, String stateCode, String taskCode, String finalNotes, Date previous, boolean isParallel) {
        return logWorkflowActivity(instance, stateCode, taskCode, finalNotes, false, null, previous, isParallel); //-- Changed By Nida
    }

    public static WorkflowLog logWorkflowActivity(ProcessInstance instance, String stateCode, String taskCode, String finalNotes, Date previous) {
        return logWorkflowActivity(instance, stateCode, taskCode, finalNotes, false, null, previous, false); //-- Changed By Nida
    }

    public static WorkflowLog logWorkflowActivity(ProcessInstance instance, String stateCode, String taskCode, String finalNotes) {
        return logWorkflowActivity(instance, stateCode, taskCode, finalNotes, false, null, null, false); //-- Changed By Nida
    }

    public static WorkflowLog logWorkflowActivity(ProcessInstance instance, String stateCode, String taskCode, String finalNotes, boolean isAdhocorSupervisor, TaskContext ctx, Date previousStateEntry, boolean isParallel) { //-- Changed By Nida
        State previousState = instance.getPreviousState();
        if (previousState == null) previousState = instance.getProcessState(stateCode);
        WorkflowLog log = new WorkflowLog();

        //   log.setLogId(CounterUtils.getNextWorkflowLogId());
        log.setCurrentState(previousState);
        log.setIsEndActivity(instance.getCurrentState());
        if (isAdhocorSupervisor)
            log.setLockActor(ctx.getUserId());
        else {
            Object currentUserObject = ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);
            if (currentUserObject != null)
                log.setLockActor(((UserDbImpl) currentUserObject).getLoginId());
        }

        log.setLockTime((Date) instance.getContext().getAttribute("UPDATED_ON"));
        log.setLogDateTime(Calendar.getInstance().getTime());
        log.setMetaEntityId(instance.getContext().getDocumentId());
        log.setNextState(stateCode);
        if (instance.getCurrentActor().size() > 0)
            log.setPossibleActors(instance.getCurrentActor());
        else
            log.setPossibleActors(((UserDbImpl) ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY)).getLoginId());
        log.setProcessCode(instance.getProcessCode());
        log.setRefDocumentNumber(instance.getInstanceId());
        log.setIsStartActivity(instance.getCurrentState());
        log.setStateEntryTime(instance.getStateEntryTime());
        log.setStateLeavingTime(new Date());
        if (StringHelper.isEmpty(taskCode))
            taskCode = null;
        log.setTaskCode(taskCode);
        log.setFinalNotes(finalNotes);
        //  log.setIsParallel(isParallel? true : false);
        if (instance.getCurrentState() == null) {
            log.setStateTime(0);
            log.setProcessTime(0);
            log.setProcessRemainingTime(0);
            log.setIsStateTatExpired(false);
            log.setIsProcessTatExpired(false);
        } else {
            if (previousStateEntry != null)
                log.setStateTime(previousStateEntry);
            else
                log.setStateTime(instance.getStateEntryTime());
            log.setProcessTime(instance.getProcessStartTime());
            log.setProcessRemainingTime(instance.getProcessTAT(), instance.getProcessStartTime());
            log.setIsStateTatExpired(instance.getStateEntryTime(), instance.getCurrentState());
            log.setIsProcessTatExpired(instance.getProcessTAT(), instance.getProcessStartTime());
        }

        if (instance.getProcessState(stateCode).getEnd()) {
            log.setIsEndActivity(true);
        }

        //
        /*Anam.Siddiqui external Datasource change for LMS begins here
         * below if condition is added to check if the logged entry is for lead process
         * if yes then fetch broker against workflowlog class as defined in configuration_instance
         * table otherwise go for the default broker
         */
        DataBroker broker = null;
     /* [unison-prime]   if(instance.getProcessCode().equalsIgnoreCase(DesignerConfigCatalog.getProperty("EXTERNAL_DOC_PROCESS_CODE"))){
        	broker = DataRepository.getBroker(WorkflowLog.class.getName());
        }
        else*/
        broker = DataRepository.getBroker(WorkflowActivityLogger.class.getName());

        //ends here

        // Nasir: state entry time and state leave time is not same now.
        if (previousState != null && previousStateEntry != null) {
            log.setStateEntryTime(getPreviousStateLeaveTime(log, previousState));
        }
        broker.add(log);
        return log;
    }


    public static void updateLogWorkflowActivity(WorkflowLog _Log, Date _AppointmentDate) {
        _Log.setAppointmentDate(_AppointmentDate);
        DataBroker broker = DataRepository.getBroker(WorkflowActivityLogger.class.getName());
        broker.update(_Log);
    }

    public static Date getPreviousStateLeaveTime(WorkflowLog log, State previousState) {
        Search query = new Search(WorkflowLog.class);
        query.addCriterion(Criterion.equal("refDocumentNumber", log.getRefDocumentNumber()));
        query.addCriterion(Criterion.equal("nextState", previousState.getStateCode()));
        DataBroker broker = DataRepository.getBroker(WorkflowActivityLogger.class.getName());
        List<WorkflowLog> logs = broker.find(query);
        if (logs != null && logs.size() > 0) {
            WorkflowLog updateLog = logs.get(0);
            return updateLog.getStateLeavingTime();
        }
        return new Date();//error removed; state entry time is set to null otherwise -- shoaib.rehman

    }
}