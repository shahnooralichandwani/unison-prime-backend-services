package com.avanza.workflow.logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import com.avanza.core.campaign.CampaignCatalog;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.scheduler.JobID;
import com.avanza.core.scheduler.SchedulerCatalog;
import com.avanza.core.util.CounterUtils;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.configuration.ConfigurationCache;
import com.avanza.integrationutility.core.Constants;
import com.avanza.workflow.execution.ActionLogExecutionJob;


/**
 * @author shafique.rehman
 */
public class ActionActivityLogger implements Observer {

    private static final Logger logger = Logger.getLogger(ActionActivityLogger.class);

    private List<Observable> observablesList = new ArrayList<Observable>();


    @Override
    public void update(Observable o, Object arg) {
        observablesList.add(o);

        if (ApplicationLoader.ACTION_LOG_CONFIG_VALUE == 1) {
            if (observablesList.size() == Integer.parseInt(arg.toString()))
                schedule();
        } else
            this.saveAtionLog((ActionObservable) o);

    }

    public void schedule() {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ActionsList", observablesList);

        SchedulerCatalog.getScheduler().scheduleJob(new JobID("NULL"),
                "ActionLogExecutionJob", ActionLogExecutionJob.class.getName(),
                (HashMap<String, Object>) map,
                "ActionLogExecutionJob", new Date());
    }

    public List<Observable> getObservablesList() {
        return observablesList;
    }

    public void setObservablesList(List<Observable> observablesList) {
        this.observablesList = observablesList;
    }

    public ActionObservable saveAtionLog(ActionObservable actionObservable) {

        logger.logInfo("[Persisting ActionLogDetail instance to database...]");

        ActionLogDetail actionLogDetail = actionObservable.getActionLogDetail();
        actionLogDetail.setActionLogId(CounterUtils.getNextActionLogId());

        DataBroker broker = DataRepository.getBroker(ActionActivityLogger.class.getName());
        broker.add(actionLogDetail);

        return actionObservable;
    }

}