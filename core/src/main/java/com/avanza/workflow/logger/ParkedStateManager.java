package com.avanza.workflow.logger;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.sequence.SequenceManager;

/**
 * @author shoaib.rehman
 */
public class ParkedStateManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8838291330656281024L;

    /**
     * Add parked state log with end time and interval null -
     * processed flag is also 0 since its not used in
     * rescheduling yet
     *
     * @param instanceId
     * @param startTime
     * @param endTime
     * @param interval
     * @param isProcessed
     */
    public static void insertParkedStateLog(String instanceId, Date startTime) {
        DataBroker broker = DataRepository.getBroker(ParkedStateLog.class.getName());
        ParkedStateLog log = new ParkedStateLog(SequenceManager.getInstance().getNextValue("PARKED_STATE_LOG"),
                instanceId, startTime, null, 0f, Boolean.FALSE);
        broker.add(log);
    }

    /**
     * Updates parked state log with end time, where
     * end time is null (indicating last log entry which
     * needs to be updated)
     *
     * @param instanceId
     * @param endTime
     */
    public static void updateParkedState(String instanceId, Date endTime) {
        DataBroker broker = DataRepository.getBroker(ParkedStateLog.class.getName());
        Search search = createCriteria(instanceId, Boolean.TRUE);
        List<Object> list = broker.find(search);
        if (list.size() == 0)
            return;
        ParkedStateLog log = (ParkedStateLog) list.get(0);
        float interval = (endTime.getTime() - log.getStartTime().getTime()) / 1000 / 60;
        log.setEndTime(endTime);
        log.setInterval(interval);
        broker.update(log);
    }

    /**
     * Creates criteria for fetching records
     *
     * @param instanceId
     * @param includeNullEndTime
     * @return Search criteria
     */
    private static Search createCriteria(String instanceId, boolean includeNullEndTime) {
        Search search = new Search(ParkedStateLog.class);
        search.addCriterion(Criterion.equal("referenceDocument", instanceId));
        search.addCriterion(Criterion.equal("isProcessed", Boolean.FALSE));
        if (includeNullEndTime)
            search.addCriterion(Criterion.nullCriterion("endTime"));
        return search;
    }

    /**
     * Updates parked state log with processed flag,
     * when end time for entry is not added and
     * rescheduling has been done
     *
     * @param instanceId
     * @param endTime
     * @param isProcessed
     * @return interval lapsed in parked state
     */
    public static float updateParkedState(String instanceId, Date endTime, boolean isProcessed) {
        float interval = 0;
        DataBroker broker = DataRepository.getBroker(ParkedStateLog.class.getName());
        Search search = createCriteria(instanceId, Boolean.FALSE);
        List<Object> list = broker.find(search);
        if (list.size() == 0)
            return interval;
        for (Object object : list) {
            ParkedStateLog log = (ParkedStateLog) object;
            interval += ((endTime.getTime() - log.getStartTime().getTime()) / 1000f / 60f);
            if (log.getEndTime() == null)//there could be cases where end time is already present
                log.setEndTime(endTime);
            log.setInterval((float) (endTime.getTime() - log.getStartTime().getTime()) / 1000 / 60);
            log.setIsProcessed(isProcessed);
            broker.update(log);
        }
        return interval;
    }

    /**
     * Updates parked state log with processed flag,
     * when rescheduling has been done
     *
     * @param instanceId
     * @param isProcessed
     * @return interval lapsed in parked state
     */
    public static float updateParkedState(String instanceId, boolean isProcessed) {
        float interval = 0;
        DataBroker broker = DataRepository.getBroker(ParkedStateLog.class.getName());
        Search search = createCriteria(instanceId, Boolean.FALSE);
        List<Object> list = broker.find(search);
        if (list.size() == 0)
            return interval;
        for (Object object : list) {
            ParkedStateLog log = (ParkedStateLog) object;
            interval += log.getInterval();
            log.setIsProcessed(isProcessed);
            broker.update(log);
        }
        return interval;
    }

}
