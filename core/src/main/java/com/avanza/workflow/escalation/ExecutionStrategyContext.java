package com.avanza.workflow.escalation;

import java.util.HashMap;
import java.util.Map;

public class ExecutionStrategyContext {

    private Map<String, Object> map;

    public ExecutionStrategyContext(String strategyId, int sequence,
                                    boolean isStateBinded) {
        map = new HashMap<String, Object>();
        setStrategyId(strategyId);
        setStrategySequence(sequence);
        setIsStateBinded(isStateBinded);
    }

    public ExecutionStrategyContext(Map<String, Object> map) {
        this.map = map;
    }

    /**
     * e.g. Complaint Id, EForm Id
     */
    public String getInstanceId() {
        return (String) map.get("InstanceId");
    }

    /**
     * e.g. Complaint Id, EForm Id
     */
    public void setInstanceId(String val) {
        innerSet("InstanceId", val);
    }

    /**
     * Meta Entity Id
     */
    public String getDocumentId() {
        return (String) map.get("DocumentId");
    }

    /**
     * Meta Entity Id
     */
    public void setDocumentId(String val) {
        innerSet("DocumentId", val);
    }

    public String getOrgUnitId() {
        return (String) map.get("OrgUnitId");
    }

    public void setOrgUnitId(String val) {
        innerSet("OrgUnitId", val);
    }

    public String getProcessStateCode() {
        return (String) map.get("ProcessStateCode");
    }

    public void setProcessStateCode(String val) {
        innerSet("ProcessStateCode", val);
    }

    public String getProcessCode() {
        return (String) map.get("ProcessCode");
    }

    public void setProcessCode(String val) {
        innerSet("ProcessCode", val);
    }

    public String getStrategyId() {
        return (String) map.get("StrategyId");
    }

    public void setStrategyId(String val) {
        innerSet("StrategyId", val);
    }

    public int getStrategySequence() {
        return Integer.parseInt(map.get("StrategySequence").toString());
    }

    public void setStrategySequence(int val) {
        innerSet("StrategySequence", val);
    }

    public boolean getIsStateBinded() {
        return Boolean.parseBoolean(map.get("IsStateBinded").toString());
    }

    public void setIsStateBinded(boolean val) {
        innerSet("IsStateBinded", val);
    }

    public String getCurrentActors() {
        return (String) map.get("CurrentActors");
    }

    public void setCurrentActors(String val) {
        innerSet("CurrentActors", val);
    }

    /**
     * Parent Actors will be calculated for these roles.
     */
    public String getRoles() {
        return (String) map.get("Roles");
    }

    public void setRoles(String val) {
        innerSet("Roles", val);
    }

    public Map<String, Object> getMap() {
        return map;
    }

    private void innerSet(String key, Object val) {
        map.put(key, val);
    }

}
