package com.avanza.workflow.escalation;

import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;

public class EscalationStrategy extends DbObject {

    private String strategyId;

    private String strategyName;

    private String description;

    private Set<EscalationLevel> levels = new HashSet<EscalationLevel>(0);

    public Set<EscalationLevel> getLevels() {
        return levels;
    }

    public void setLevels(Set<EscalationLevel> levels) {
        this.levels = levels;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStrategyId() {
        return strategyId;
    }


    public void setStrategyId(String strategyId) {
        this.strategyId = strategyId;
    }


    public String getStrategyName() {
        return strategyName;
    }


    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public EscalationLevel getNextEscalationLevel(int currentLevelSeq) {
        if (currentLevelSeq <= 0)
            return getEscalationLevel(1);
        else
            return getEscalationLevel(currentLevelSeq + 1);
    }

    public EscalationLevel getEscalationLevel(int levelSeq) {
        for (EscalationLevel level : levels)
            if ((level.getLevelSeq() == levelSeq) && !level.isDeleted()) return level;
        return null;
    }

    public EscalationLevel getEscalationLevel(String levelId) {
        for (EscalationLevel level : levels)
            if (level.getLevelId().equalsIgnoreCase(levelId)) return level;
        return null;
    }
}
