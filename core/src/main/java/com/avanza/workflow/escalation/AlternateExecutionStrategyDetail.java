package com.avanza.workflow.escalation;

import com.avanza.core.data.DbObject;

public class AlternateExecutionStrategyDetail extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 1043600388056508969L;

    private String id;

    private String operator;

    private String value;

    private boolean isCustomerAttribute;

    private AlternateExecutionStrategy strategy;

    private String entityAttributeSystemName;

    public String getEntityAttributeSystemName() {
        return entityAttributeSystemName;
    }

    public void setEntityAttributeSystemName(String entityAttributeSystemName) {
        this.entityAttributeSystemName = entityAttributeSystemName;
    }

    public AlternateExecutionStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(AlternateExecutionStrategy strategy) {
        this.strategy = strategy;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean getIsCustomerAttribute() {
        return isCustomerAttribute;
    }

    public void setIsCustomerAttribute(boolean isCustomerAttribute) {
        this.isCustomerAttribute = isCustomerAttribute;
    }

}
