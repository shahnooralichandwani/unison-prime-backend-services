package com.avanza.workflow.escalation;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import com.avanza.core.data.DbObject;
import com.avanza.workflow.configuration.action.ActionBinding;
import com.avanza.workflow.configuration.action.ActionSet;

public class EscalationLevel extends DbObject {

    public static String Id = "escalation_level";

    private String levelId;

    private String strategyId;

    private long levelExpiryTime;

    private int levelSeq;

    private String ccType;

    private int orgSearchLevel;

    private int roleSearchLevel;

    private boolean deleted;

    private ActionSet actionSet;

    private String actionSetId;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public int getOrgSearchLevel() {
        return orgSearchLevel;
    }


    public void setOrgSearchLevel(int orgSearchLevel) {
        this.orgSearchLevel = orgSearchLevel;
    }


    public int getRoleSearchLevel() {
        return roleSearchLevel;
    }


    public void setRoleSearchLevel(int roleSearchLevel) {
        this.roleSearchLevel = roleSearchLevel;
    }


    public String getCcType() {
        return ccType;
    }


    public void setCcType(String ccType) {
        this.ccType = ccType;
    }


    public long getLevelExpiryTime() {
        return levelExpiryTime;
    }


    public void setLevelExpiryTime(long levelExpiryTime) {
        this.levelExpiryTime = levelExpiryTime;
    }


    public String getLevelId() {
        return levelId;
    }


    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }


    public String getStrategyId() {
        return strategyId;
    }


    public void setStrategyId(String strategyId) {
        this.strategyId = strategyId;
    }

    public int getLevelSeq() {
        return levelSeq;
    }


    public void setLevelSeq(int levelSeq) {
        this.levelSeq = levelSeq;
    }

    public ActionSet getActionSet() {
        return actionSet;
    }

    public void setActionSet(ActionSet actionSet) {
        this.actionSet = actionSet;
    }

    public String getActionSetId() {
        return actionSetId;
    }

    public void setActionSetId(String actionSetId) {
        this.actionSetId = actionSetId;
    }

    public List<ActionBinding> getSortedActionBindings() {

        List<ActionBinding> returnList = new Vector<ActionBinding>();
        if (actionSet != null)
            for (ActionBinding ab : actionSet.getActionList()) {
                returnList.add(ab);
            }

        Collections.sort(returnList);

        return returnList;
    }
}
