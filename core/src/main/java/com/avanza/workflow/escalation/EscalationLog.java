package com.avanza.workflow.escalation;

import java.util.Date;

import com.avanza.core.data.DbObject;

public class EscalationLog extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = -3226461791796531034L;

    private String id;

    private String metaEntityId;

    private String refDocNumber;

    private String escLevelId;

    private String escStrategyId;

    private Date expiryStartTime;

    private String actorManager;

    private String notifiedPersons;

    private boolean current;

    private String stateCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getActorManager() {
        return actorManager;
    }


    public void setActorManager(String actorManager) {
        this.actorManager = actorManager;
    }


    public boolean getCurrent() {
        return current;
    }


    public void setCurrent(boolean current) {
        this.current = current;
    }


    public String getEscLevelId() {
        return escLevelId;
    }


    public void setEscLevelId(String escLevelId) {
        this.escLevelId = escLevelId;
    }


    public String getEscStrategyId() {
        return escStrategyId;
    }


    public void setEscStrategyId(String escStrategyId) {
        this.escStrategyId = escStrategyId;
    }


    public Date getExpiryStartTime() {
        return expiryStartTime;
    }


    public void setExpiryStartTime(Date expiryStartTime) {
        this.expiryStartTime = expiryStartTime;
    }


    public String getMetaEntityId() {
        return metaEntityId;
    }


    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }


    public String getNotifiedPersons() {
        return notifiedPersons;
    }


    public void setNotifiedPersons(String notifiedPersons) {
        this.notifiedPersons = notifiedPersons;
    }


    public String getRefDocNumber() {
        return refDocNumber;
    }


    public void setRefDocNumber(String refDocNumber) {
        this.refDocNumber = refDocNumber;
    }


}
