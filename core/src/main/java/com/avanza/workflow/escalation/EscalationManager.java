package com.avanza.workflow.escalation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import javax.faces.context.FacesContext;

import com.avanza.core.calendar.CalendarCatalog;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.OrderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.scheduler.JobID;
import com.avanza.core.scheduler.JobImpl;
import com.avanza.core.scheduler.SchedulerCatalog;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.util.CollectionHelper;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.configuration.StateEntityBinding;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.OrganizationalUnit;
import com.avanza.workflow.configuration.org.Role;
import com.avanza.workflow.execution.ExecutionStrategyJob;
import com.avanza.workflow.execution.ProcessInstance;

public class EscalationManager {

    private static Logger logger = Logger.getLogger(EscalationManager.class);

    public static EscalationStrategy getStrategy(String id) {
        DataBroker broker = DataRepository.getBroker(EscalationStrategy.class
                .toString());
        return broker.findById(EscalationStrategy.class, id);
    }

    public static EscalationStrategy getStrategy(ProcessInstance instance,
                                                 boolean isStateBinded) {
        EscalationStrategy strategy;
        if (isStateBinded) {
            // Get state level strategy
            strategy = instance.getCurrentState().getStrategy();
            for (StateEntityBinding binding : instance.getBinding()
                    .getStateBindings()) {
                if (instance.getCurrentState().getStateCode().equals(
                        binding.getId().getState().getStateCode())
                        && instance.getContext().getDocumentId().equals(
                        binding.getEntityId())) {
                    // Strategy overridden
                    strategy = binding.getStrategy();
                    break;
                }
            }
        } else {
            // process level strategy.
            strategy = instance.getStrategy();
            //TODO: check if alternate strategies are defined in for this document -- shoaib.rehman
            DataBroker broker = DataRepository.getBroker(AlternateExecutionStrategy.class.getName());
            Search search = new Search(AlternateExecutionStrategy.class);
            search.addCriterion(Criterion.equal("entity.id", instance.getBinding().getMetaEntityId()));
            search.addOrder("order", OrderType.ASC);
            List<AlternateExecutionStrategy> list = broker.find(search);
            if (list.size() > 0) {
                boolean found = true;
                AlternateExecutionStrategy alternate = list.get(0);
                for (AlternateExecutionStrategyDetail detail : alternate.getDetail()) {
                    if (detail.getEntityAttributeSystemName().split("\\.")[0].equals(SessionKeyLookup.CURRENT_OBJ)) {
                        if (!instance.getContext().getValues().get(detail.getEntityAttributeSystemName().split("\\.")[1]).equals(detail.getValue())) {
                            found = false;
                            break;
                        }
                    } else {
					/* [unison-prime]	DataObject customer  = (DataObject)FacesContext.getCurrentInstance()
														   .getExternalContext()
														   .getSessionMap()
														   .get(detail.getEntityAttributeSystemName().split("\\.")[0]);
						if(!customer.getValue(detail.getEntityAttributeSystemName().split("\\.")[1]).equals(detail.getValue())){
							found = false;
							break;
						}*/
                    }
                }
                if (found)
                    strategy = alternate.getStrategy();
            }

        }
        return strategy;
    }

    public static void startStrategy(ProcessInstance instance,
                                     boolean isStateBinded) {
        startStrategy(instance, getStrategy(instance, isStateBinded),
                isStateBinded);
    }

    /**
     * This method should be used only if passing Strategy is not configured.
     * otherwise let Strategy be selected by the system.
     */
    public static void startStrategy(ProcessInstance instance,
                                     EscalationStrategy strategy, boolean isStateBinded) {
        ScheduleStrategy(null, strategy, instance, isStateBinded);
    }

    public static void proceedToNextStrategyLevel(ProcessInstance instance,
                                                  ExecutionStrategyContext currentContext) {
        EscalationStrategy strategy = getStrategy(currentContext
                .getStrategyId());
        ScheduleStrategy(currentContext, strategy, instance, currentContext
                .getIsStateBinded());
    }

    public static void populateStrategyContext(
            ExecutionStrategyContext strategyContext, ProcessInstance instance) {
        strategyContext.setOrgUnitId(instance.getContext().getCurrentOrgUnit());
        strategyContext.setDocumentId(instance.getContext().getDocumentId());
        strategyContext.setInstanceId(instance.getContext().getInstanceId());
        strategyContext.setProcessCode(instance.getProcessCode());
        strategyContext.setProcessStateCode(instance.getCurrentState()
                .getStateCode());

        strategyContext.setCurrentActors(Convert.toCSV(instance
                .getCurrentActor()));
        strategyContext.setRoles(Convert.toCSV(calculateRolesForCurrentActors(
                instance).keySet()));
    }

    private static void ScheduleStrategy(
            ExecutionStrategyContext strategyContext,
            EscalationStrategy strategy, ProcessInstance instance,
            boolean isStateBinded) {

        //getting next level
        int nextSequence = (strategyContext == null) ? 1 : strategyContext
                .getStrategySequence() + 1;
        EscalationLevel nextLevel = strategy.getEscalationLevel(nextSequence);
        if (nextLevel == null) {
            // all levels have been executed for this strategy.
            return;
        }


        //update context.
        if (strategyContext == null) {
            //if it is just being started,
            //then create and populate strategy context.
            strategyContext = new ExecutionStrategyContext(strategy
                    .getStrategyId(), nextSequence, isStateBinded);
            populateStrategyContext(strategyContext, instance);
        } else {
            //move strategy context to next level.
            strategyContext.setStrategySequence(nextSequence);
        }

        Date scheduleDate = new Date(CalendarCatalog.getDefaultCalendar()
                .getAdjustedTime(Calendar.getInstance().getTime(),
                        nextLevel.getLevelExpiryTime()));

        SchedulerCatalog.getScheduler().scheduleJob(new JobID("NULL"),
                "ExecutionStrategy", ExecutionStrategyJob.class.getName(),
                (HashMap<String, Object>) strategyContext.getMap(),
                "ExecutionStrategy", scheduleDate);

        logger.logInfo("next level is:" + nextLevel.getActionSet().getName() + " at time," + scheduleDate);
    }

    private static Map<String, Role> calculateParentRoles(
            Collection<Role> roles, int searchLevel) {
        Map<String, Role> parentRoles = new HashMap<String, Role>();
        Map<String, Role> immediateParentRoles = new HashMap<String, Role>();
        for (int i = 1; i <= searchLevel; i++) {
            Collection<Role> rolesIterator = (i == 1) ? roles
                    : new ArrayList<Role>(immediateParentRoles.values());

            // for keeping only immediate parents.
            immediateParentRoles.clear();

            for (Role role : rolesIterator) {
                Role parentRole = role.getParentRole();
                if (parentRole != null) {
                    immediateParentRoles.put(parentRole.getRoleId(), parentRole);
                    parentRoles.put(parentRole.getRoleId(), parentRole);
                }
            }
        }

        return parentRoles;

    }

    private static Map<String, OrganizationalUnit> calculateParentOrgUnits(
            OrganizationalUnit orgUnit, int searchLevel) {
        Map<String, OrganizationalUnit> parentOrgUnits = new HashMap<String, OrganizationalUnit>();
        for (int i = 1; i <= searchLevel; i++) {
            OrganizationalUnit parentOrgUnit = orgUnit.getParentOrgUnit();
            if (parentOrgUnit != null) {
                parentOrgUnits.put(parentOrgUnit.getId(), parentOrgUnit);
            }
        }

        return parentOrgUnits;

    }

    /**
     * Before calling this method, we are assuming that actor's roles have been
     * populated in strategy context.
     */
    public static Map<String, User> calculateParentActors(
            ExecutionStrategyContext strategyContext, ProcessInstance instance) {

        Map<String, Role> roles;
        roles = OrganizationManager.getRolesByIds(Convert
                .toList(strategyContext.getRoles()));

        EscalationStrategy strategy = getStrategy(strategyContext
                .getStrategyId());

        EscalationLevel strategyLevel = strategy
                .getEscalationLevel(strategyContext.getStrategySequence());

        Collection<Role> parentRoles = calculateParentRoles(roles.values(),
                strategyLevel.getRoleSearchLevel()).values();

        OrganizationalUnit orgUnit = OrganizationManager
                .getOrganizationUnit(strategyContext.getOrgUnitId());

        Collection<OrganizationalUnit> parentOrgUnits = calculateParentOrgUnits(
                orgUnit, strategyLevel.getOrgSearchLevel()).values();

        Map<String, User> users;

        // calculate users for parent roles and current orgUnit.
        users = calculateActors(parentRoles, Arrays.asList(orgUnit));

        // calculate users for parent roles and parent orgUnits.
        CollectionHelper.mergeMap(users, calculateActors(parentRoles,
                parentOrgUnits));

        if (users.size() == 0 && (strategyLevel.getRoleSearchLevel() > 0) && (strategyLevel.getOrgSearchLevel() > 0)) {
            //no parent is found at upper level.
            //add supervisors instead.
            for (String supervisor : instance.getSupervisors()) {
                users.put(supervisor, SecurityManager.getUser(supervisor));
            }
        }

        return users;

    }

    private static Map<String, User> calculateActors(Collection<Role> roles,
                                                     Collection<OrganizationalUnit> orgUnits) {
        HashMap<String, User> users = new HashMap<String, User>();
        for (Role role : roles) {
            for (OrganizationalUnit orgUnit : orgUnits) {
                List<User> orgRoleUsers = OrganizationManager.getUsers(orgUnit
                        .getOrgUnitId(), role.getRoleId());
                for (User user : orgRoleUsers) {
                    users.put(user.getLoginId(), user);
                }
            }
        }
        return users;

    }

    public static Map<String, User> calculateActors(Collection<Role> roles) {
        HashMap<String, User> usersMap = new HashMap<String, User>();
        for (Role role : roles) {
            List<User> roleUsers = OrganizationManager.getAllUsers(role.getRoleId());
            for (User user : roleUsers) {
                usersMap.put(user.getLoginId(), user);
            }
        }
        return usersMap;
    }

    private static Map<String, Role> calculateRolesForCurrentActors(
            ProcessInstance instance) {

        Map<String, Role> mergedRoles = new HashMap<String, Role>();
        for (String userId : instance.getCurrentActor()) {
            for (Role role : OrganizationManager.getAllRoles(userId)) {
                mergedRoles.put(role.getRoleId(), role);
            }
        }

        Map<String, Role> filteredRoles = new HashMap<String, Role>();

        State state = instance.getCurrentState();
        for (Role role : mergedRoles.values()) {
            if (instance.getBinding().isStateRoleAssigned(state.getStateCode(),
                    role.getRoleId())) {
                filteredRoles.put(role.getRoleId(), role);
            }
        }
        return filteredRoles;
    }

    /**
     * Used to reschedule strategy for the time interval
     * lapsed in parked state
     *
     * @param strategyContext
     * @param interval
     * @return True, if rescheduling done, False otherwise
     */
    public static boolean rescheduleStrategy(ExecutionStrategyContext strategyContext,
                                             float interval) {
        if (interval == 0)
            return Boolean.FALSE;
        Date scheduleDate = new Date(CalendarCatalog.getDefaultCalendar()
                .getAdjustedTime(Calendar.getInstance().getTime(),
                        (long) interval));

        SchedulerCatalog.getScheduler().scheduleJob(new JobID("NULL"),
                "ExecutionStrategy", ExecutionStrategyJob.class.getName(),
                (HashMap<String, Object>) strategyContext.getMap(),
                "ExecutionStrategy", scheduleDate);
        return Boolean.TRUE;
    }

}
