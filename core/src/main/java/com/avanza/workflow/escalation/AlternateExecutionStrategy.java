package com.avanza.workflow.escalation;

import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.meta.MetaEntity;

public class AlternateExecutionStrategy extends DbObject implements Comparable<AlternateExecutionStrategy> {

    /**
     *
     */
    private static final long serialVersionUID = -3581734512184737097L;

    private String id;

    private MetaEntity entity;

    private EscalationStrategy strategy;

    private Set<AlternateExecutionStrategyDetail> detail = new HashSet<AlternateExecutionStrategyDetail>();

    private Integer order;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MetaEntity getEntity() {
        return entity;
    }

    public void setEntity(MetaEntity entity) {
        this.entity = entity;
    }

    public EscalationStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(EscalationStrategy strategy) {
        this.strategy = strategy;
    }

    public Set<AlternateExecutionStrategyDetail> getDetail() {
        return detail;
    }

    public void setDetail(Set<AlternateExecutionStrategyDetail> detail) {
        this.detail = detail;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Override
    public int compareTo(AlternateExecutionStrategy o) {
        if (order < o.getOrder())
            return -1;
        else if (order > o.getOrder())
            return 1;
        return 0;
    }
}
