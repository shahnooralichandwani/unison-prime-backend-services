package com.avanza.workflow.execution;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.avanza.workflow.configuration.action.ActionBinding;


public class ActionExecutionResult extends ActionResult implements Serializable {

    private static final long serialVersionUID = 1L;
    private boolean changePossible;
    private ActionError criticalError;
    private List<ActionError> errorList = new ArrayList<ActionError>(0);
    private ActionResult actionResult;


    public ActionError getCriticalError() {
        return criticalError;
    }

    public void setCriticalError(ActionBinding binding, Exception exp, boolean error) {
        criticalError = new ActionError(binding, exp, error);
    }

    public void addError(ActionBinding binding, Exception exp, boolean error) {
        ActionError actError = new ActionError(binding, exp, error);
        this.errorList.add(actError);
    }

    public boolean isChangePossible() {
        return changePossible;
    }


    public void setChangePossible(boolean changePossible) {
        this.changePossible = changePossible;
    }

    public class ActionError {

        private ActionBinding binding;
        private Exception excpetion;
        private boolean systemError;

        public ActionError(ActionBinding binding, Exception exp, boolean error) {
            this.binding = binding;
            this.excpetion = exp;
            this.systemError = error;
        }

        public ActionBinding getBinding() {
            return binding;
        }

        public void setBinding(ActionBinding binding) {
            this.binding = binding;
        }

        public Exception getExcpetion() {
            return excpetion;
        }

        public void setExcpetion(Exception excpetion) {
            this.excpetion = excpetion;
        }

        public boolean isSystemError() {
            return systemError;
        }

        public void setSystemError(boolean systemError) {
            this.systemError = systemError;
        }

    }


    public List<ActionError> getErrorList() {
        return errorList;
    }

    public ActionResult getActionResult() {
        return actionResult;
    }

    public void setActionResult(ActionResult actionResult) {
        this.actionResult = actionResult;
    }


}
