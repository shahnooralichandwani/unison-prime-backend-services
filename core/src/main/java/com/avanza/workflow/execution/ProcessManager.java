package com.avanza.workflow.execution;

import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.workflow.configuration.Process;
import com.avanza.workflow.configuration.ProcessCatalog;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.WorkFlowEngine;

/**
 * This is a singleton Class for Managing execution of a process.
 *
 * @author hmirza
 */
public class ProcessManager {

    private static ProcessManager procExecManager;

    private static WorkFlowEngine wfEngine;

    private ProcessManager() {

        ConfigSection config = null;
        ProcessManager.wfEngine = WorkFlowEngine.getInstance(config);
    }

    public static ProcessManager getInstance() {

        if (procExecManager == null) {

            procExecManager = new ProcessManager();
        }
        return ProcessManager.procExecManager;
    }

    /*
     * Process Definition Functions
     */

    public Process findProcess(String processId) {

        return wfEngine.findProcess(processId);
    }

    /**
     * Related actions will be executed, and DataObject (composed in ProcessContext) will be persisted.
     *
     * @param processCode
     * @param ctx
     * @param binding
     * @param taskctx     it is required by Action. it is also used to hold the validation result (taskctx.getMessage) and WorkflowLog object (taskctx.getParameters())
     * @return
     */

    public ProcessInstance createInstance(ProcessContext ctx, ProcessEntityBinding binding) {
        TaskContext taskctx = new TaskContext("System", ctx.getInstanceId(), "", null);
        return createInstance(binding.getProcessCode(), ctx, binding, taskctx);
    }

    public ProcessInstance createInstance(String processCode, ProcessContext ctx, ProcessEntityBinding binding, TaskContext taskctx) {

        Process proc = ProcessCatalog.getProcessByProcessCode(processCode);
        ProcessInstance procInst = new ProcessInstanceImpl(proc, ctx, ctx.getInstanceId(), proc.getStartState(), null, binding);
        boolean init = procInst.initiate(taskctx);
        if (!init)
            return null;
        return procInst;
    }

    public ProcessInstance populateInstance(String processCode, ProcessContext ctx) {

        Process proc = ProcessCatalog.getProcessByProcessCode(processCode);
        ProcessInstance procInst = new ProcessInstanceImpl(proc, ctx, ctx.getInstanceId(), proc.getAssociatedBinding(ctx.getDocumentId()));
        return procInst;
    }

    void saveInstance(ProcessInstance procInst) {
        this.wfEngine.saveInstance(procInst);
    }


    // public ProcessInstance getProcessInstance(String instanceId) {
    //
    // return ProcessManager.wfEngine.getProcessInstance(instanceId);
    // }
}