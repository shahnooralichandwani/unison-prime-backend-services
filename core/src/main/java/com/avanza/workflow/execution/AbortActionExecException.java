package com.avanza.workflow.execution;


public class AbortActionExecException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public AbortActionExecException(String message) {
        super(message);
    }
}
