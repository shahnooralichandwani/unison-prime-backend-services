package com.avanza.workflow.execution.tat;

import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.scheduler.JobImpl;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.workflow.configuration.Process;
import com.avanza.workflow.configuration.ProcessCatalog;
import com.avanza.workflow.execution.ActionExecutionManager;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessContextImpl;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.ProcessManager;


public class StateTATExpiryJob extends JobImpl {

    private Logger logger = Logger.getLogger(StateTATExpiryJob.class);

    public void executeInner() {
        /*
         * Shahbaz
         * Transaction handling done
         * Issue reported by yasir for boubyan project
         * Problem was invalid DataObject state found on certain level
         */
        TransactionContext txnContext = ApplicationContext.getContext().getTxnContext();
        txnContext.beginTransaction();
        try {
            System.out.println("StateTATExpiryJob Event Occured");
            StringTokenizer st = new StringTokenizer(getId(), "|");
            String documentId = st.nextToken();
            String instanceId = st.nextToken();
            Search instanceSearch = new Search();
            MetaEntity entity = MetaDataRegistry.getMetaEntity(documentId);
            if (entity == null) return;
            MetaEntity parentEntity = MetaDataRegistry.getNonAbstractParentEntity(entity);
            Map<String, MetaAttribute> attributes = entity.getAttributeList(true);

            for (MetaAttribute attrib : attributes.values()) {
                instanceSearch.addColumn(attrib.getTableColumn());
            }
            MetaAttribute primaryAttribute = entity.getPrimaryKeyAttribute();
            instanceSearch.addFrom(parentEntity.getSystemName());
            instanceSearch.addCriterion(SimpleCriterion.equal(primaryAttribute.getSystemName(), instanceId));
            DataBroker broker = DataRepository.getBroker(entity.getSystemName());
            List<DataObject> dataObject = broker.find(instanceSearch);
            if (dataObject.size() > 0) {
                ProcessContext ctx = new ProcessContextImpl(dataObject.get(0));
                Process process = null;
                String processCode = dataObject.get(0).getProcessCode();

                if (processCode != null)
                    process = ProcessCatalog.getProcessByProcessCode(processCode);

                else {
                    txnContext.rollback();
                    return;
                }
                // Shahbaz
                // Process should be get from the dataobject instead from EL
                // Process process = ProcessCatalog.getProcessByDocumentId(documentId, ctx);
                ProcessInstance inst = ProcessManager.getInstance().populateInstance(process.getProcessCode(), ctx);
                String stateCode = st.nextToken();
                if (inst.getCurrentState().getStateCode().equalsIgnoreCase(stateCode))
                    ActionExecutionManager.getInstance().notifyStateTATExpiry(inst);
            }

            txnContext.commit();
        } catch (Exception e) {
            logger.LogException("Exception occoured in StateTATExpiryJob:%s.", e);

            // If Exception Occourred then Rollback
            txnContext.rollback();
        } catch (Error e) {
            logger.LogException("Error occoured in StateTATExpiryJob:%s.", e);

            // If Error Occoured then Rollback
            txnContext.rollback();
        } finally {

            // Release the context.
            txnContext.release();
        }

    }
}
