package com.avanza.workflow.execution;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.scheduler.JobImpl;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.workflow.escalation.EscalationLevel;
import com.avanza.workflow.escalation.EscalationManager;
import com.avanza.workflow.escalation.EscalationStrategy;
import com.avanza.workflow.escalation.ExecutionStrategyContext;
import com.avanza.workflow.logger.ParkedStateManager;

public class ExecutionStrategyJob extends JobImpl {

    private Logger logger = Logger.getLogger(ExecutionStrategyJob.class);

    private static ProcessContext getProcessContext(
            ExecutionStrategyContext strategyContext) {
        Search instanceSearch = new Search();
        MetaEntity entity = MetaDataRegistry.getMetaEntity(strategyContext
                .getDocumentId());
        MetaEntity parentEntity = MetaDataRegistry
                .getNonAbstractParentEntity(entity);
        MetaAttribute primaryAttribute = entity.getPrimaryKeyAttribute();
        instanceSearch.addFrom(parentEntity.getSystemName());
        instanceSearch.addCriterion(SimpleCriterion.equal(primaryAttribute
                .getSystemName(), strategyContext.getInstanceId()));
        DataBroker broker = DataRepository.getBroker(entity.getSystemName());
        List<DataObject> dataObject = broker.find(instanceSearch);
        return new ProcessContextImpl(dataObject.get(0));

    }

    @Override
    public void executeInner() {
        TransactionContext txnContext = ApplicationContext.getContext()
                .getTxnContext();

        ExecutionStrategyContext strategyContext = new ExecutionStrategyContext(
                getDataMap());

        String processCode = strategyContext.getProcessCode();

        if (processCode == null) {
            logger.logWarning("no process code found");
            return;
        }

        ProcessContext processContext = getProcessContext(strategyContext);

        ProcessInstance instance = ProcessManager.getInstance()
                .populateInstance(processCode, processContext);

        if (strategyContext.getIsStateBinded()) {
            // state binded strategy.
            if (!strategyContext.getProcessStateCode().equalsIgnoreCase(
                    instance.getCurrentState().getStateCode())) {
                // state has been changed so scheduling of binded strategy will
                // be discontinued.
                return;
            }
        } else {
            if (instance.getCurrentState().getEnd()) {
                // process has been finished.
                return;
            }
            //reschedule strategy if document is in parked state -- shoaib.rehman
            else if (instance.getCurrentState().getParked()) {
                logger.logInfo("it is in parking state.");
                Date currentDate = new Date();
                //update previous log's end time
                //get interval (in minutes) spared in parked state
                float interval = ParkedStateManager.updateParkedState(instance.getInstanceId(), currentDate, Boolean.TRUE);
                //reschedule strategy for required interval
                EscalationManager.rescheduleStrategy(strategyContext, interval);
                //insert parked state log for current date time
                ParkedStateManager.insertParkedStateLog(instance.getInstanceId(), currentDate);
                return;
            } else {
                //check if any non-processed parked log is present
                float interval = ParkedStateManager.updateParkedState(instance.getInstanceId(), Boolean.TRUE);
                //reschedule strategy for required interval
                if (EscalationManager.rescheduleStrategy(strategyContext, interval))//return if rescheduling done
                    return;
            }
        }

        EscalationStrategy strategy = EscalationManager
                .getStrategy(strategyContext.getStrategyId());
        if (strategy == null) {
            logger.logWarning("strategy with id '%s' is not defined",
                    strategyContext.getStrategyId());
            return;
        }

        EscalationLevel strategyLevel = strategy
                .getEscalationLevel(strategyContext.getStrategySequence());
        if (strategyLevel == null) {
            logger
                    .logWarning(
                            "strategy sequence number '%s' for strategy id '%s' is not defined",
                            strategyContext.getStrategySequence(),
                            strategyContext.getStrategyId());
            return;
        }

        // execute binded actions of particular strategy level.
        txnContext.beginTransaction();
        ActionExecutionResult result;
        try {
            // attach strategy context with process context so strategy
            // information can be used by related actions.
            instance.getContext().setNonPrsistableAttribute(
                    "ExecutionStrategyContext", strategyContext);
            result = ActionExecutionManager
                    .getInstance()
                    .notifyExecutionStrategyFired(instance, null, strategyLevel);
            txnContext.commit();

        } catch (Exception e) {
            logger
                    .LogException(
                            "Exception occoured in PostStateExpiryEscalationJob:%s.",
                            e);
            txnContext.rollback();
            return;
        } finally {
            txnContext.release();
        }

        if (result != null && result.isChangePossible()) {
            // level can be changed. so move to next level.
            EscalationManager
                    .proceedToNextStrategyLevel(instance, strategyContext);
        }

    }
}
