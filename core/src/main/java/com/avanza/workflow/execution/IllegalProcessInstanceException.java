package com.avanza.workflow.execution;

import com.avanza.workflow.configuration.WorkFlowException;

/**
 * Represents Illegal Process Instances
 *
 * @author hmirza
 */
public class IllegalProcessInstanceException extends WorkFlowException {


    private static final long serialVersionUID = 1L;

}
