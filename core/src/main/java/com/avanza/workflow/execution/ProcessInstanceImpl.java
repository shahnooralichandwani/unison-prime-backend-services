package com.avanza.workflow.execution;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.Attribute;
import com.avanza.core.sdo.DataObjectCollection;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.audit.AuditManager;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.workflow.configuration.EventType;
import com.avanza.workflow.configuration.Process;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.ProcessParticipant;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.Transition;
import com.avanza.workflow.configuration.TransitionParticipant;
import com.avanza.workflow.configuration.WorkflowConfigurationException;
import com.avanza.workflow.configuration.action.ActionBinding;
import com.avanza.workflow.configuration.action.ActionCatalog;
import com.avanza.workflow.configuration.action.ActionDefinition;
import com.avanza.workflow.configuration.filter.DefaultTransitionParticipantFilter;
import com.avanza.workflow.configuration.filter.TransitionParticipantFilter;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.Role;
import com.avanza.workflow.escalation.EscalationStrategy;
import com.avanza.workflow.execution.ActionExecutionResult.ActionError;
import com.avanza.workflow.execution.tat.TATScheduler;
import com.avanza.workflow.logger.WorkflowActivityLogger;
import com.avanza.workflow.logger.WorkflowLog;

class ProcessInstanceImpl implements ProcessInstance {

    private static final Logger logger = Logger.getLogger(ProcessInstance.class);

    public static String PROCESS_INSTANCE_COUNTER = "ProcessInstanceId";

    private Process process;

    private ProcessContext context;

    private String instanceId;

    private State currentState;

    private State previousState;

    private ProcessEntityBinding binding;

    private DataObject obj;
    @SuppressWarnings("unused")
    private List<String> current_actorList = new ArrayList<String>(0);

    @SuppressWarnings("unused")
    private List<String> calculated_actorList = new ArrayList<String>(0);

    public ProcessInstanceImpl(Process proc, ProcessContext cont, String instid, State currSt, State prevStat, ProcessEntityBinding binding) {
        this.process = proc;
        this.context = cont;
        this.instanceId = instid;
        this.previousState = prevStat;
        this.binding = binding;
        setCurrentState(currSt);
        setProcessCode(proc.getProcessCode());
        setProcessStartTime(Calendar.getInstance().getTime());
        this.context.setAttribute("IS_TAT_EXPIRED", false);

    }

    public boolean initiate(TaskContext ctx) {
        /*
         * Shahbaz
         * First notify start process actions
         * then persist the object
         * notify enter state actions
         * and finally if all above successfully done, log workflow activity
         */
        logger.logInfo("[Initializing process...]");
        logger.logInfo("[Notifying start process actions...]");
        ActionExecutionResult result = ActionExecutionManager.getInstance().notifyStartProcess(this, ctx);

        if (result.isChangePossible()) {
            logger.logInfo("[Persisting instance to database...]");
            this.persitInstance();

            /*
             *
             * Shahbaz
             * Notifies enter state event actions for current state i.e. Initiated
             * Notification work must be done here.
             *
             */

            logger.logInfo("[Notifying enter state actions...]");
            ActionExecutionManager.getInstance().notifyPostStateChange(this, ctx);
		    	/*ActionExecutionResult postStateResult = ActionExecutionManager.getInstance().notifyPostStateChange(this, ctx);
		    	if (postStateResult.isChangePossible()) {
	    	    	logger.logInfo("[Persisting instance to database after ENTER_STATE...]");    	
		    	this.persitInstance();
		    	} else {
					ctx.addMessage(WorkflowContextLookup.CONTEXT_MESSAGE, result.getCriticalError().getExcpetion().getMessage());
					return false;
				}*/

            logger.logInfo("[Logging workflow activity...]");

            WorkflowLog log = WorkflowActivityLogger.logWorkflowActivity(this, this.getCurrentState().getStateCode(), null, "");
            ctx.getParameters().put("LOG", log);

            logger.logInfo("[Scheduling Process and State TATs...]");
            TATScheduler.scheduleProcessTAT(this);
            TATScheduler.scheduleStateTAT(this);
        } else {
            ctx.addMessage(WorkflowContextLookup.CONTEXT_MESSAGE, result.getCriticalError().getExcpetion().getMessage());
            return false;
        }
        return true;
    }

    private void setProcessStartTime(Date dateTime) {
        this.context.setProcessStartTime(dateTime);
    }

    private void setProcessCode(String processCode) {
        this.context.setProcessCode(processCode);

    }

    public ProcessInstanceImpl(Process proc, ProcessContext cont, String instid, ProcessEntityBinding binding) {
        this.process = proc;
        this.context = cont;
        this.instanceId = instid;
        this.binding = binding;
        this.currentState = this.process.getState((String) this.context.getAttribute("CURRENT_STATE"));
        this.current_actorList = Convert.toList((String) this.context.getAttribute("CURRENT_ACTOR"));
        this.calculated_actorList = Convert.toList((String) this.context.getAttribute("CALCULATED_ACTOR"));
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public void setAttribute(String attribId, Attribute attrib) {


        context.setAttribute(attribId, attrib);
    }

    public boolean moveToNextState(TaskContext context) {
        return moveToNextState(context, false);
    }

    public boolean moveToNextState(TaskContext context, boolean isMannual) {
        logger.logInfo(String.format("Move To Next State Called on ProcessInstance:%1$s, TaskCode:%2$s and UserId:%3$s ProcessInstance.moveToNextState(TaskContext)", context.processInstanceId,
                context.taskCode, context.userId));

        if ((isMannual && this.getCurrentActor().size() == 0) || (!isMannual && this.getCurrentActor().size() > 1)) {
            UserDbImpl user = ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);
            this.lockProcess(user.getLoginId());
            MetaEntity entity = MetaDataRegistry.getMetaEntity(this.getContext().getDocumentId());
            MetaEntity parent = MetaDataRegistry.getNonAbstractParentEntity(entity);
            TaskLoadManager.incrementUserWorkLoad(user.getLoginId(), parent.getId());
        }

        if (this.getCurrentActor().size() > 0)
            TaskLoadManager
                    .decrementUserWorkLoad(this.getCurrentActor().get(0), MetaDataRegistry.getNonAbstractParentEntity(MetaDataRegistry.getMetaEntity(this.getContext().getDocumentId())).getId());
/*this.setCalculatedActor(this.getCurrentActor());
this.getCalculatedActor();*/
        logger.logInfo("Executing Pre State Actions");
        ActionExecutionResult preStatResult = ActionExecutionManager.getInstance().notifyPreStateChange(this, context);
        if (preStatResult.isChangePossible()) {
            logger.logInfo("Executing On Transition Actions");
            this.previousState = this.currentState;
            //	this.calculated_actorList=this.getCurrentActor();
            Date previousEntryTime = getStateEntryTime();

            // resolved bug for Task Allocation
            setCurrentState(process.getTransitionEndState(context.taskCode));
            ActionExecutionResult transResult = ActionExecutionManager.getInstance().notifyOnTransition(this, context);
            if (transResult.isChangePossible()) {
                logger.logInfo("Logging Workflow Activity");
                // result for preEnterState checked
                // PRE_ENTER_STATE event added -- shoaib.rehman
                transResult = ActionExecutionManager.getInstance()
                        .notifyPreEnterState(this, context);
                if (transResult.isChangePossible()) {
                    TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
                    txContext.beginTransaction();
                    WorkflowLog log = null;
                    try {
                        log = WorkflowActivityLogger
                                .logWorkflowActivity(this, process
                                        .getTransitionEndState(context.taskCode)
                                        .getStateCode(), context.taskCode, context
                                        .getFinalNotes(), previousEntryTime, context.isParallel); //-- Changed By Nida
                        Map<String, Object> map = new HashMap<String, Object>();
                        map.put("LOG", log);
                        context.setParameters(map);
                        // Since state is changed therefore state level escalation
                        // should be stopped;
                        //setting STOP_STATE_ESC to true -- shoaib.rehman
                        //setStopStateEscalation();
                        persitInstance();
                        txContext.commit();
                    } catch (RuntimeException ex) {
                        txContext.rollback();
                        throw ex;
                    }

                    setTaskContextParameters(context);
                    TATScheduler.scheduleStateTAT(this);
                    /*
                     * Shahbaz Auditing done here if the previous state is
                     * 'EDIT'
                     */

                    if (getPreviousState().getEdit()) {
                        //[unison-prime]	    AuditManager.saveChangeLog(log, getContext());
                    }

                    /*
                     * Shahbaz End
                     */


                    // TATScheduler.scheduleStateTAT(this);
                    // TATScheduler.scheduleProcessTAT(this);
                    logger.logInfo("Executing Post State Actions");
                    //Shuwair
                    // ActionExecutionManager.getInstance().notifyPostStateChange(this,
                    // context);
                    ActionExecutionResult postStateResult = ActionExecutionManager
                            .getInstance().notifyPostStateChange(this, context);
                    if (postStateResult.isChangePossible()) {
                        logger
                                .logInfo("[Persisting instance to database after ENTER_STATE...]");
                        // this.persitInstance();
                    } else {
                        context.addMessage(
                                WorkflowContextLookup.CONTEXT_MESSAGE,
                                postStateResult.getCriticalError()
                                        .getExcpetion().getMessage());
                        return false;
                    }
                    if (this.currentState.getEnd()) {
                        ActionExecutionManager.getInstance().notifyEndProcess(
                                this, context);
                    }
                } else {
                    //TODO: necessary to revert states back -- shoaib.rehman
                    setCurrentState(previousState);
                    currentState = previousState;
                    previousState = null;
                    logError(transResult);
                    context.addMessage(WorkflowContextLookup.CONTEXT_MESSAGE,
                            transResult.getCriticalError().getExcpetion()
                                    .getMessage());
                    return false;
                }
            } else {
                //TODO: necessary to revert states back -- shoaib.rehman
                setCurrentState(previousState);
                currentState = previousState;
                previousState = null;
                logError(transResult);
                context.addMessage(WorkflowContextLookup.CONTEXT_MESSAGE, transResult.getCriticalError().getExcpetion().getMessage());
                return false;
            }
        } else {
            logError(preStatResult);
            context.addMessage(WorkflowContextLookup.CONTEXT_MESSAGE, preStatResult.getCriticalError().getExcpetion().getMessage());
            return false;
        }
        return true;
    }

    @Deprecated
    private void setStopStateEscalation() {
        if (true) return;
        context.setAttribute("STOP_STATE_ESC", Boolean.TRUE);
    }

    private void logError(ActionExecutionResult errorResult) {
        logger.logInfo("Error While Executing Actions");
        if (errorResult.isChangePossible()) {
            for (ActionError error : errorResult.getErrorList()) {
                ActionDefinition actiondef = ActionCatalog.getActionDefination(error.getBinding().getActionCode());
                logger.logError("Action Code: %1$s, Action Name: %2$s, System Error: %3$s", error.getBinding().getActionCode(), actiondef.getActionName(), error.isSystemError());
                if (error.getExcpetion() != null)
                    logger.LogException("Action Execution Exception", error.getExcpetion());
            }
        } else {
            ActionDefinition actiondef = ActionCatalog.getActionDefination(errorResult.getCriticalError().getBinding().getActionCode());
            logger.logError("Action Code: %1$s, Action Name: %2$s, System Error: %3$s", errorResult.getCriticalError().getBinding().getActionCode(), actiondef.getActionName(), errorResult
                    .getCriticalError().isSystemError());
            if (errorResult.getCriticalError().getExcpetion() != null)
                logger.LogException("Action Execution Exception", errorResult.getCriticalError().getExcpetion());
        }

    }

    public boolean moveToState(String stateId, TaskContext context) {

        ActionExecutionResult result = ActionExecutionManager.getInstance().notifyPreStateChange(this, context);
        if (result.isChangePossible()) {
            State adhocState = process.getState(stateId);
            Date previousStateEntry = (Date) this.context.getAttribute("STATE_ENTRY_TIME");
            setCurrentState(adhocState);

            //result for preEnterState checked
            //PRE_ENTER_STATE event added -- shoaib.rehman
            result = ActionExecutionManager.getInstance().notifyPreEnterState(this, context);
            if (result.isChangePossible()) {
                //Since state is changed therefore state level escalation should be stopped;
                //setting STOP_STATE_ESC to true -- shoaib.rehman
                setStopStateEscalation();

                TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
                txContext.beginTransaction();
                WorkflowLog log = null;
                try {
                    log = WorkflowActivityLogger.logWorkflowActivity(this, adhocState.getStateCode(), context.taskCode, context.getFinalNotes(), true, context, previousStateEntry, context.isParallel);//-- Changed by Nida)
                    persitInstance();
                    txContext.commit();
                } catch (RuntimeException ex) {
                    txContext.rollback();
                    throw ex;
                }

                setTaskContextParameters(context);

                /*  Shahbaz
                 *  Auditing done here
                 *  if the previous state is 'EDIT'
                 */

                if (getPreviousState().getEdit()) {
                    //[unison-prime]   AuditManager.saveChangeLog(log, getContext());
                }

                /*
                 * Shahbaz
                 * End
                 */

                // TATScheduler.scheduleProcessTAT(this);
                TATScheduler.scheduleStateTAT(this);
                ActionExecutionManager.getInstance().notifyPostStateChange(this, context);
            } else {
                //TODO: necessary to revert states back -- shoaib.rehman
                setCurrentState(previousState);
                currentState = previousState;
                previousState = null;
                logError(result);
                context.addMessage(WorkflowContextLookup.CONTEXT_MESSAGE, result.getCriticalError().getExcpetion().getMessage());
                return false;
            }
        } else {
            logError(result);
            context.addMessage(WorkflowContextLookup.CONTEXT_MESSAGE, result.getCriticalError().getExcpetion().getMessage());
            return false;
        }
        return true;
    }


    /**
     * Method written to fix below issue:
     * Issue: javax.faces.STATE_SAVING_METHOD property in web.xml when changed from
     * server to client; the EDIT_SDO saved in view root does not update -- shoaib.rehman
     *
     * @param context
     */
    private void setTaskContextParameters(TaskContext context) {
        context.getParameters().put("CURRENT_STATE", this.context.getAttribute("CURRENT_STATE"));
        context.getParameters().put("CURRENT_ACTOR", this.context.getAttribute("CURRENT_ACTOR"));
        context.getParameters().put("CALCULATED_ACTOR", this.context.getAttribute("CALCULATED_ACTOR"));
    }

    public void lockProcess(String actorId) {
        List<String> currentActorList = new ArrayList<String>(0);
        currentActorList.add(actorId);
        setCurrentActor(currentActorList);
        persitInstance();
    }

    public void unlockProcess() {
        setCurrentActor(getCalculatedActor());
        persitInstance();
    }

    public long getStateTAT(String stateId) throws WorkflowConfigurationException {
        return process.getState(stateId).getTatInMinutes();
    }

    public long getStateTAT() {
        if (currentState != null) {
            return currentState.getTatInMinutes();
        }
        return 0;
    }

    public long getRemainingStateTAT() {

        return 0;
    }

    public long getRemainingProcessTAT() {

        return 0;
    }

    public boolean isStateTATExpired() {

        return false;
    }

    public boolean isProcessTATExpired() {
        Object value = context.getAttribute("IS_TAT_EXPIRED");
        if (value != null) {
            return Convert.toBoolean(value);
        }
        return false;
    }

    public List<String> getCurrentActor() {
        return context.getCurrentActors();
    }

    public List<String> getCalculatedActor() {
        return context.getCalculatedActors();
    }

    public void setCurrentActor(List<String> actorsids) {
        context.setCurrentActors(actorsids);
    }

    public void setCalculatedActor(List<String> actorsids) {
        context.setCalculatedActors(actorsids);
    }

    public void persitInstance() {
        ProcessManager.getInstance().saveInstance(this);
    }

    public Set<String> getAssignedRoles() {
        Set<String> roles = new HashSet<String>();
        Set<ProcessEntityBinding> bindings = process.getProcBindingList();
        for (ProcessEntityBinding procbind : bindings) {
            for (ProcessParticipant participant : procbind.getProcessParticipantList()) {
                roles.add(participant.getRoleId());
            }
        }
        return roles;
    }

    public Set<State> nextStates() {
        return process.getNextPossibleStateList(currentState);
    }

    public Set<Transition> nextTasks() {

        return currentState.getTransitionList();
    }

    public boolean isOnAdhocState() {
        if (currentState != null) {
            return Convert.toBoolean(currentState.getAdhoc());
        }
        return false;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.previousState = this.currentState;
        this.currentState = currentState;
        this.context.setStateEntryTime(Calendar.getInstance().getTime());
        this.context.setCurrentState(currentState.getStateCode());
    }

    public State getPreviousState() {
        return previousState;
    }

    public void setPreviousState(State previousState) {
        this.previousState = previousState;
    }

    public ProcessContext getContext() {
        return context;
    }

    public void setContext(ProcessContext context) {
        this.context = context;
    }

    public String getProcessCode() {
        return process.getProcessCode();
    }

    public void setSupervisorAsActors(String orgId) {
        List<String> supervisors = process.getSupervisorUserIds(context.getDocumentId(), orgId);
        setCurrentActor(supervisors);
    }

    public List<String> getSupervisors() {
        List<String> supervisors = process.getSupervisorUserIds(context.getDocumentId(), context.getCurrentOrgUnit());
        return supervisors;
    }

    public long getProcessTAT() {
        if (binding.getTurnAroundTime() > 0)
            return binding.getTurnAroundTime();
        else
            return process.getTurnAroundTime();
    }

    public void setTatExpired() {
        this.context.setTatExpired(true);
        this.persitInstance();
    }

    public State getProcessState(String stateCode) {
        return process.getState(stateCode);
    }

    public Date getStateEntryTime() {
        return this.context.getStateEntryTime();
    }

    public Date getProcessStartTime() {
        return this.context.getProcessStartTime();
    }

    public EscalationStrategy getStrategy() {
        return this.binding.getStrategy();
    }

    public List<String> getSupervisorRole(String roleId) {
        return this.binding.getSupervisorRoles(roleId);
    }

    public List<ActionBinding> getSortedActionBindings(EventType e) {
        return this.process.getSortedActionBindings(e);
    }

    public List<ActionBinding> getSortedStateTaskActionBindings(EventType e, String stateTaskCode) {
        return this.process.getSortedStateTaskActionBindings(e, stateTaskCode);
    }

    public Set<ProcessParticipant> getProcessParticipants(String metaEnt) {
        return this.binding.getProcessParticipantList();
    }

    public Set<StateParticipant> getStateParticipants(String metaEnt, String stateCode) {
        Set<StateParticipant> statePartcipant = new HashSet<StateParticipant>(0);
        for (StateParticipant participant : this.binding.getStateParticipantList()) {
            if (participant.getStateCode().equalsIgnoreCase(stateCode)) {
                statePartcipant.add(participant);
            }
        }
        return statePartcipant;
    }

    public ProcessEntityBinding getBinding() {
        return binding;
    }

    @Override
    public List<Transition> getUserActivities(String userId) {
        if ((isUserCurrentActor(userId) || isUserInSupervisorList(userId))
                && !isEnded()) {

            if (this.getCurrentState().getAdhoc()
                    || isUserInSupervisorList(userId)) {
                //TODO Need to be implemented;
                return getTransitionFromDefaultMode(userId);
            } else {
                return getTransitionFromDefaultMode(userId);
            }
        }
        return null;
    }

    public boolean isEnded() {

        boolean retVal = false;
        if (this.getCurrentState() != null) {
            retVal = this.getCurrentState().getEnd();
        }
        return retVal;
    }

    public boolean isUserCurrentActor(String userId) {
        List<String> currentUsers = this.getContext().getCurrentActors();
        List<String> currentCalculatedUsers = this.getContext().getCalculatedActors();
        for (String usrId : currentUsers) {
            if (usrId.equalsIgnoreCase(userId)) {
                return true;
            }

        }
        for (String usrId : currentCalculatedUsers) {
            if (usrId.equalsIgnoreCase(userId)) {
                return true;
            }

        }
        return false;
    }

    public boolean isUserInSupervisorList(String userId) {
        List<String> supervisors = this.getSupervisors();
        if (supervisors != null)
            if (supervisors.contains(userId))
                return true;

        return false;
    }

    public List<Transition> getTransitionFromDefaultMode(String userId) {

        ProcessInstance instance = this;
        List<Transition> filterTasks = new ArrayList<Transition>();
        Set<Transition> tasks = instance.nextTasks();
        boolean isRoleExist = false;
        try {
            DefaultTransitionParticipantFilter defaultFilter = new DefaultTransitionParticipantFilter();
            List<Role> roles = OrganizationManager.getAllRoles(userId);
            isRoleExist = defaultFilter.isRoleInTransitionParticipant(roles);
        } catch (Exception e) {
            logger.LogException("Filter Class Not Found", e);
        }
        if (isRoleExist) {
            Set<StateParticipant> stateParticipant = instance.getStateParticipants(instance.getContext().getDocumentId(), instance.getCurrentState().getStateCode());
            for (Transition transition : tasks) {
                TransitionParticipantFilter filter = null;
                for (TransitionParticipant tp : transition.getTransitionParticipantList()) {
                    try {
                        filter = (TransitionParticipantFilter) Class.forName(tp.getActivityUserFilter()).newInstance();
                    } catch (Exception e) {
                        logger.LogException("Filter Class Not Found", e);
                        continue;
                    }
                }
                if (filter == null) {
                    try {
                        filter = (TransitionParticipantFilter) Class.forName("com.avanza.workflow.configuration.filter.DefaultTransitionParticipantFilter").newInstance();
                    } catch (Exception e) {
                        logger.LogException("Filter Class Not Found", e);
                        continue;
                    }
                }
                if (transition.getTransitionParticipantList().size() > 0 && stateParticipant.size() > 0) {
                    boolean transParticipantExist = filter.getActivities(transition, stateParticipant, userId);
                    if (transParticipantExist) {
                        filterTasks.add(transition);
                    }
                }
            }
            return filterTasks;
        } else {
            for (Transition transition : tasks) {
                filterTasks.add(transition);
            }
            return filterTasks;
        }

    }

    public DataObject getDataObject() {

        return obj;
    }

    public void setDataObject(DataObject obj) {
        this.obj = obj;

    }

}
