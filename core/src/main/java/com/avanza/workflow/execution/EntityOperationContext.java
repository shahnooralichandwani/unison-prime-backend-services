package com.avanza.workflow.execution;

import com.avanza.core.sdo.DataObject;

public class EntityOperationContext extends TaskContext {

    private static final long serialVersionUID = -4689591604028627954L;
    DataObject dataObject;
    String operationCode;
    String metaEntityId;

    public EntityOperationContext(String userid, String taskcode,
                                  String processInstanceid, String stateCode, DataObject dataObject,
                                  String operationCode, String metaEntityId) {
        super(userid, taskcode, processInstanceid, stateCode);
        this.dataObject = dataObject;
        this.operationCode = operationCode;
        this.metaEntityId = metaEntityId;
    }

    public DataObject getDataObject() {
        return dataObject;
    }

    public void setDataObject(DataObject dataObject) {
        this.dataObject = dataObject;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

}
