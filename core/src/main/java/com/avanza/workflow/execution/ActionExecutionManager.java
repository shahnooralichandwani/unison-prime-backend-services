package com.avanza.workflow.execution;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.util.CollectionHelper;
import com.avanza.core.util.Logger;
import com.avanza.workflow.configuration.EventType;
import com.avanza.workflow.configuration.action.ActionBinding;
import com.avanza.workflow.configuration.action.ActionCatalog;
import com.avanza.workflow.escalation.EscalationLevel;
import com.avanza.workflow.logger.ActionActivityLogger;
import com.avanza.workflow.logger.ActionLogDetail;
import com.avanza.workflow.logger.ActionObservable;
import com.avanza.workflow.logger.WorkflowLog;

/**
 * Main Action execution management class
 *
 * @author hmirza
 */
public class ActionExecutionManager {


    private static final Logger logger = Logger
            .getLogger(ActionExecutionManager.class);
    private static ActionExecutionManager aeManager;

    private ActionExecutionManager() {

        // ConfigSection config = null;
    }

    public static ActionExecutionManager getInstance() {

        if (aeManager == null) {
            aeManager = new ActionExecutionManager();
        }
        return aeManager;
    }

    public ActionExecutionResult notifyPreStateChange(ProcessInstance instance,
                                                      TaskContext ctx) {
        return execute(instance, instance.getCurrentState()
                .getSortedActionBindings(EventType.LEAVE_STATE), ctx);
    }

    @SuppressWarnings("unchecked")
    public ActionExecutionResult notifyOnTransition(ProcessInstance instance,
                                                    TaskContext ctx) {
        Vector<ActionBinding> actionsList = new Vector<ActionBinding>();
        actionsList.addAll(instance.getPreviousState().getTransitionbyId(
                ctx.getTaskCode()).getActionList());
        Collections.sort(actionsList);
        return execute(instance, actionsList, ctx);
    }

    /**
     * PRE_ENTER_STATE event acknowledgment
     *
     * @param instance
     * @param ctx
     * @return ActionExecutionResult
     * @author shoaib.rehman
     */
    public ActionExecutionResult notifyPreEnterState(
            ProcessInstance instance, TaskContext ctx) {
        return execute(instance, instance.getCurrentState()
                .getSortedActionBindings(EventType.PRE_ENTER_STATE), ctx);
    }

    public ActionExecutionResult notifyPostStateChange(
            ProcessInstance instance, TaskContext ctx) {
//		instance.getCurrentState().getActionList().get;
        return execute(instance, instance.getCurrentState()
                .getSortedActionBindings(EventType.ENTER_STATE), ctx);
    }

    public ActionExecutionResult notifyStateTATExpiry(ProcessInstance instance) {
        return execute(instance, instance.getCurrentState()
                .getSortedActionBindings(EventType.STATE_TAT_EXPIRY), null);
    }

    public ActionExecutionResult notifyProcessTATExpiry(ProcessInstance instance) {
        return execute(instance, instance
                .getSortedActionBindings(EventType.PROCESS_TAT_EXPIRY), null);
    }

    public ActionExecutionResult notifyPreStateTATExpiry(
            ProcessInstance instance) {
        return execute(instance, instance.getCurrentState()
                        .getSortedActionBindings(EventType.BEFORE_STATE_TAT_EXPIRY),
                null);
    }

    public ActionExecutionResult notifyPreProcessTATExpiry(
            ProcessInstance instance) {
        return execute(instance, instance
                        .getSortedActionBindings(EventType.BEFORE_PROCESS_TAT_EXPIRY),
                null);
    }

    public ActionExecutionResult notifyStartProcess(ProcessInstance instance,
                                                    TaskContext ctx) {
        return execute(instance, instance.getCurrentState()
                .getSortedActionBindings(EventType.START_PROCESS), ctx);
    }

    public ActionExecutionResult notifyEndProcess(ProcessInstance instance,
                                                  TaskContext ctx) {
        return execute(instance, instance.getCurrentState()
                .getSortedActionBindings(EventType.END_PROCESS), ctx);
    }

    public ActionExecutionResult notifyExecutionStrategyFired(ProcessInstance instance,
                                                              TaskContext ctx, EscalationLevel level) {

        return execute(instance, level.getSortedActionBindings(), ctx);
    }

    //Shuwair
    public ActionExecutionResult notifyBeforeTaskSaving(ProcessInstance instance,
                                                        TaskContext ctx, String stateTaskCode) {
        // Nida Siddiqui
        Set<ActionBinding> bindingSet = new HashSet<ActionBinding>();
        List<ActionBinding> bindingList = new ArrayList<ActionBinding>();
        // Adding Actions(binded to Process) that have null stateCode
        CollectionHelper.mergeList(bindingSet, instance.getSortedStateTaskActionBindings(EventType.BEFORE_TASK_SAVING, stateTaskCode));
        // Adding Actions(binded to Current State)
        CollectionHelper.mergeList(bindingSet, instance.getCurrentState().getSortedStateTaskActionBindings(EventType.BEFORE_TASK_SAVING, stateTaskCode));
        bindingList.addAll(bindingSet);
        Collections.sort(bindingList);
        return execute(instance, bindingList, ctx);
    }

    public ActionExecutionResult notifyAfterTaskSaving(ProcessInstance instance,
                                                       TaskContext ctx, String stateTaskCode) {
        return execute(instance, instance.getCurrentState()
                .getSortedStateTaskActionBindings(EventType.AFTER_TASK_SAVING, stateTaskCode), ctx);
    }

    public ActionExecutionResult notifyPreSave(ProcessInstance instance,
                                               EntityOperationContext ctx, String metaEntityId) {
        return execute(instance, getSortedStateTaskActionBindings(
                EventType.PRE_SAVE, metaEntityId), ctx);
    }

    public ActionExecutionResult notifyPostSave(ProcessInstance instance,
                                                EntityOperationContext ctx, String metaEntityId) {
        return execute(instance, getSortedStateTaskActionBindings(
                EventType.POST_SAVE, metaEntityId), ctx);
    }

    @SuppressWarnings("unchecked")
    public ActionExecutionResult execute(ProcessInstance procInst,
                                         List<ActionBinding> ab, Object ctx) {
        ActionExecutionResult result = new ActionExecutionResult();
        // get processed actions from context -- shoaib.rehman
        HashMap<String, ActionBinding> processed = null;
        ActionResult previousActionResponse = null;
        // New Context type added to provide hook point
        boolean isContextAvailable = ctx != null;
        if (isContextAvailable)
            processed = (HashMap<String, ActionBinding>) ((TaskContext) ctx).getParameters().get(WorkflowContextLookup.PROCESSED);

        //Added By Shafique Rehman for action logging feature
        List<ActionObservable> actionObservableList = new ArrayList<ActionObservable>();
        ActionActivityLogger observer = new ActionActivityLogger();
        ActionObservable actionObservable;
        ActionLogDetail actionPojo;

        for (ActionBinding binding : ab) {

            //Added By Shafique Rehman for action logging feature
            actionObservable = new ActionObservable();
            actionPojo = new ActionLogDetail();

            Action action = ActionCatalog.getAction(binding.getActionCode());
            // initiate map if processed actions are null -- shoaib.rehman
            if (processed == null)
                processed = new HashMap<String, ActionBinding>();
                //if action code is already present in processed action, skip it -- shoaib.rehman
            else if (processed.get(binding.getActionCode()) != null)
                continue;
            // if action code returned from previous action is null or same as
            // binding's action code
            // then proceed otherwise skip -- shoaib.rehman
            if (isContextAvailable
                    && !(((TaskContext) ctx).getNextAction() == null || binding
                    .getActionCode().equals(
                            ((TaskContext) ctx).getNextAction())))
                continue;
            if (action != null) {
                // reset next action to null
                if (isContextAvailable) {
                    ((TaskContext) ctx).setNextAction(null);
                    // Adding previousActionResponse to the taskContext
                    ((TaskContext) ctx)
                            .setPreviousActionResponse(previousActionResponse);
                }

                // Added By Shafique Rehman
                actionPojo.setTaskActionId(binding.getId() + "");

                if (procInst != null && procInst.getInstanceId() != null) {
                    actionPojo.setRefDocumentNumber(procInst.getInstanceId());
                }

                actionPojo.setLogDateTime(Calendar.getInstance().getTime());

                // Get instance of workflowlog from task context map --- Shafique Rehman
                if (isContextAvailable && ((TaskContext) ctx).getParameters().get("LOG") != null) {
                    WorkflowLog log = null;
                    log = (WorkflowLog) ((TaskContext) ctx).getParameters().get("LOG");
                    if (log.getLogId() != null) {
                        actionPojo.setWfLogId(log.getLogId());
                    }
                }
                if (procInst != null && procInst.getDataObject() != null) {
                    if (procInst.getDataObject().getIdValue() != null) {
                        actionPojo.setTaskDetailId(procInst.getDataObject().getIdValue());
                    }
                }

                try {
                    ValidatorActionResult validatorResult;
                    if (action instanceof ValidatorAction) {
                        validatorResult = (ValidatorActionResult) action.act(
                                procInst, binding, ctx);

                        // Setting previousActionResponse
                        previousActionResponse = validatorResult;
                        // put action in processed actions -- shoaib.rehman
                        processed.put(binding.getActionCode(), binding);
                        if (validatorResult.isSuccess() == false) {
                            if (binding.getCritical()) {
                                result.setChangePossible(false);
                                result.setCriticalError(binding,
                                        new AbortActionExecException(
                                                validatorResult.getMessage()),
                                        false);
                                // put map in context, for confirmation action
                                // flow -- shoaib.rehman
                                if (isContextAvailable)
                                    ((TaskContext) ctx).getParameters().put(
                                            WorkflowContextLookup.PROCESSED,
                                            processed);
                                result.setActionResult(previousActionResponse);
                                return result;
                            } else {
                                result.setChangePossible(true);
                                result.addError(binding, null, false);
                            }
                        }
                    } else if (action instanceof ConfirmationAction) {
                        ConfirmationActionResult confirmationResult = (ConfirmationActionResult) action
                                .act(procInst, binding, ctx);

                        // Setting previousActionResponse
                        previousActionResponse = confirmationResult;
                        // put action in processed actions -- shoaib.rehman
                        processed.put(binding.getActionCode(), binding);
                        ((TaskContext) ctx).getParameters().put(WorkflowContextLookup.SHOW_CONFIRMATION, confirmationResult.getShowConfirmation());
                        if (!confirmationResult.isSuccess()) {
                            if (binding.getCritical()) {
                                result.setChangePossible(Boolean.FALSE);
                                result
                                        .setCriticalError(binding,
                                                new AbortActionExecException(
                                                        confirmationResult
                                                                .getMessage()),
                                                Boolean.FALSE);
                                // put map in context, for confirmation action
                                // flow -- shoaib.rehman
                                if (isContextAvailable)
                                    ((TaskContext) ctx).getParameters().put(
                                            WorkflowContextLookup.PROCESSED,
                                            processed);
                                result.setActionResult(previousActionResponse);
                                return result;
                            } else {
                                result.setChangePossible(Boolean.TRUE);
                                result.addError(binding, null, Boolean.FALSE);
                            }
                        }
                    } else {

                        // Setting previousActionResponse
                        Object response = action.act(procInst, binding, ctx);
                        if (response != null)
                            previousActionResponse = (ActionResult) response;
                        // put action in processed actions -- shoaib.rehman
                        processed.put(binding.getActionCode(), binding);
                    }


                } catch (Exception e) {
                    logger.LogException("Error Occured While Executing Action",
                            e);

                    // Added By Shafique Rehman
                    actionPojo.setExceptionMsg(e.toString());

                    if (binding.getCritical()) {
                        result.setChangePossible(false);
                        result.setCriticalError(binding, e, true);

                    } else {
                        result.setChangePossible(true);
                        result.addError(binding, e, true);
                    }
                }
            } else {
                result.setChangePossible(false);
            }

            //Added By Shafique Rehman for action logging feature
            if (ApplicationLoader.IS_ENABLE_ACTION_LOG_VALUE == 1) {
                actionObservable.addObserver(observer);
                actionObservable.addDetail(actionPojo);
                actionObservableList.add(actionObservable);
            } else {
                actionObservable = null;
                actionPojo = null;
            }


        }
        result.setChangePossible(true);
        // put map in context, for confirmation action flow -- shoaib.rehman
        if (isContextAvailable)
            ((TaskContext) ctx).getParameters().put(
                    WorkflowContextLookup.PROCESSED, processed);
        result.setActionResult(previousActionResponse);

        //Added By Shafique Rehman for action logging feature
        for (ActionObservable aObs : actionObservableList) {
            aObs.notifyObservers(actionObservableList.size());
        }


        return result;
    }

    public List<ActionBinding> getSortedStateTaskActionBindings(EventType e,
                                                                String metaEntityId) {

        MetaEntity metaEntity = MetaDataRegistry.getMetaEntity(metaEntityId);
        Map<String, ActionBinding> actionList;
        actionList = metaEntity.getActionBinding();
        List<ActionBinding> returnList = new Vector<ActionBinding>();
        for (ActionBinding ab : actionList.values()) {

            if (EventType.fromName(ab.getEventCode()) == e)
                returnList.add(ab);
        }
        Collections.sort(returnList);

        return returnList;
    }

}
