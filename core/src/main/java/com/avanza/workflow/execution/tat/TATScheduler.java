package com.avanza.workflow.execution.tat;

import java.util.Calendar;
import java.util.Date;

import com.avanza.core.calendar.CalendarCatalog;
import com.avanza.core.scheduler.JobID;
import com.avanza.core.scheduler.SchedulerCatalog;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.configuration.StateEntityBinding;
import com.avanza.workflow.execution.ProcessInstance;

public class TATScheduler {

    public static void scheduleProcessTAT(ProcessInstance instance) {
        Date entryTime = new Date();
        String uid = instance.getContext().getDocumentId() + "|" + instance.getInstanceId() + "|"
                + instance.getCurrentState().getStateCode() + "|" + entryTime.toLocaleString();
        long tatTime = instance.getProcessTAT();
        long Adjustedtime = CalendarCatalog.getDefaultCalendar().getAdjustedTime(entryTime, tatTime);
        Date expiryTime = new Date(Adjustedtime);
        System.out.println("Registering JOB [ Process TAT ] [Fire Time :" + expiryTime.toLocaleString() + "] for "
                + uid.toString());
        SchedulerCatalog.getScheduler().scheduleJob(new JobID(uid), "JobGroupProcess",
                ProcessTATExpiryJob.class.getName(), null, "TriggerGroupProcess", expiryTime);

        Calendar cal = Calendar.getInstance();
        cal.setTime(expiryTime);
        cal.add(Calendar.MINUTE, -180);
        // if (entryTime.before(expiryTime)) {
        // System.out.println("Registering JOB [ Pre Process TAT ] [Fire Time :"
        // + cal.getTime().toLocaleString() + "] for " + uid);
        // SchedulerCatalog.getScheduler().scheduleJob(uid + "_Pre",
        // "JobGroupProcess", PreProcessTATExpiryJob.class.getName(), null,
        // "TriggerGroupProcess", cal.getTime());
        // }
    }

    public static void scheduleStateTAT(ProcessInstance instance) {
        Date entryTime = new Date();
        State currentState = instance.getCurrentState();
        String uid = instance.getContext().getDocumentId() + "|" + instance.getInstanceId() + "|"
                + currentState.getStateCode() + "|" + entryTime.toLocaleString();
        long tatTime = currentState.getTatInMinutes();
        //TODO: Get state binding, if any, for the current state of the process -- shoaib.rehman
        for (StateEntityBinding binding : instance.getBinding().getStateBindings()) {
            if (currentState.getStateCode().equals(binding.getId().getState().getStateCode())
                    && instance.getContext().getDocumentId().equals(binding.getEntityId())) {

                tatTime = binding.getTAT();//TAT overridden -- shoaib.rehman
                break;
            }
        }
        long Adjustedtime = CalendarCatalog.getDefaultCalendar().getAdjustedTime(entryTime, tatTime);
        Date expiryTime = new Date(Adjustedtime);
        System.out.println("Registering JOB [ STATE TAT ] [Fire Time : " + expiryTime.toLocaleString() + "] for "
                + uid.toString());
        SchedulerCatalog.getScheduler().scheduleJob(new JobID(uid), "JobGroupState", StateTATExpiryJob.class.getName(),
                null, "TriggerGroupState", expiryTime);
        Calendar cal = Calendar.getInstance();
        cal.setTime(expiryTime);
        cal.add(Calendar.MINUTE, -30);
        // if (entryTime.before(expiryTime)) {
        // System.out.println("Registering JOB [ Pre STATE TAT ] [Fire Time : "
        // + cal.getTime().toLocaleString() + "] for " + uid);
        // SchedulerCatalog.getScheduler().scheduleJob(uid + "_Pre",
        // "JobGroupState", PreStateTATExpiryJob.class.getName(), null,
        // "TriggerGroupState", cal.getTime());
        // }
    }
}
