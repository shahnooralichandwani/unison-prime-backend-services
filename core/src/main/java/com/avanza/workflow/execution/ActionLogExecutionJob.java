package com.avanza.workflow.execution;

import java.util.HashMap;
import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.scheduler.JobImpl;
import com.avanza.core.util.CounterUtils;
import com.avanza.core.util.Logger;
import com.avanza.workflow.escalation.ExecutionStrategyContext;
import com.avanza.workflow.logger.ActionActivityLogger;
import com.avanza.workflow.logger.ActionLogDetail;
import com.avanza.workflow.logger.ActionObservable;

public class ActionLogExecutionJob extends JobImpl {

    private Logger logger = Logger.getLogger(ActionLogExecutionJob.class);

    @SuppressWarnings("unchecked")
    @Override
    public void executeInner() {
        ExecutionStrategyContext strategyContext = new ExecutionStrategyContext(
                getDataMap());

        HashMap<String, Object> actionsMap = (HashMap<String, Object>) strategyContext.getMap();
        List<ActionObservable> observablesList = (List<ActionObservable>) actionsMap.get("ActionsList");

        this.saveAtionLog(observablesList);
    }

    public void saveAtionLog(List<ActionObservable> observablesList) {

        for (ActionObservable actionObservable : observablesList) {

            logger.logInfo("[Persisting ActionLogDetail instance to database...]");

            ActionLogDetail actionLogDetail = actionObservable.getActionLogDetail();
            actionLogDetail.setActionLogId(CounterUtils.getNextActionLogId());

            DataBroker broker = DataRepository.getBroker(ActionActivityLogger.class.getName());
            broker.add(actionLogDetail);
        }
    }


}
