//package com.avanza.workflow.execution.tat;
//
//import java.util.List;
//import java.util.Map;
//import java.util.StringTokenizer;
//
//import org.quartz.JobExecutionContext;
//
//import com.avanza.core.data.ApplicationContext;
//import com.avanza.core.data.DataBroker;
//import com.avanza.core.data.DataRepository;
//import com.avanza.core.data.TransactionContext;
//import com.avanza.core.data.expression.Search;
//import com.avanza.core.data.expression.SimpleCriterion;
//import com.avanza.core.meta.MetaAttribute;
//import com.avanza.core.meta.MetaDataRegistry;
//import com.avanza.core.meta.MetaEntity;
//import com.avanza.core.scheduler.JobImpl;
//import com.avanza.core.sdo.DataObject;
//import com.avanza.core.util.Logger;
//import com.avanza.workflow.configuration.Process;
//import com.avanza.workflow.configuration.ProcessCatalog;
//import com.avanza.workflow.configuration.StateEntityBinding;
//import com.avanza.workflow.escalation.EscalationLevel;
//import com.avanza.workflow.escalation.EscalationManager;
//import com.avanza.workflow.escalation.EscalationStrategy;
//import com.avanza.workflow.execution.ActionExecutionManager;
//import com.avanza.workflow.execution.ProcessContext;
//import com.avanza.workflow.execution.ProcessContextImpl;
//import com.avanza.workflow.execution.ProcessInstance;
//import com.avanza.workflow.execution.ProcessManager;
//
//public class PostStateExpiryEscalationJob extends JobImpl {
//
//	private Logger logger = Logger
//			.getLogger(PostStateExpiryEscalationJob.class);
//
//	@Override
//	public void executeInner() {
//		TransactionContext txnContext = ApplicationContext.getContext()
//				.getTxnContext();
//		txnContext.beginTransaction();
//		try {
//			StringTokenizer st = new StringTokenizer(getId(), "|");
//			String documentId = st.nextToken();
//			String instanceId = st.nextToken();
//			String escalationLevel = st.nextToken();
//			Search instanceSearch = new Search();
//			MetaEntity entity = MetaDataRegistry.getMetaEntity(documentId);
//			MetaEntity parentEntity = MetaDataRegistry
//					.getNonAbstractParentEntity(entity);
//			Map<String, MetaAttribute> attributes = entity
//					.getAttributeList(true);
//			for (MetaAttribute attrib : attributes.values()) {
//				instanceSearch.addColumn(attrib.getTableColumn());
//			}
//			MetaAttribute primaryAttribute = entity.getPrimaryKeyAttribute();
//			instanceSearch.addFrom(parentEntity.getSystemName());
//			instanceSearch.addCriterion(SimpleCriterion.equal(primaryAttribute
//					.getSystemName(), instanceId));
//			DataBroker broker = DataRepository
//					.getBroker(entity.getSystemName());
//			List<DataObject> dataObject = broker.find(instanceSearch);
//			if (dataObject.size() > 0) {
//				Process process = null;
//				ProcessContext ctx = new ProcessContextImpl(dataObject.get(0));
//				String processCode = dataObject.get(0).getProcessCode();
//
//				if (processCode != null)
//					process = ProcessCatalog
//							.getProcessByProcessCode(processCode);
//				else {
//					txnContext.rollback();
//					return;
//				}
//				ProcessInstance inst = ProcessManager.getInstance()
//						.populateInstance(process.getProcessCode(), ctx);
//				//If state is not changed, schedule further escalations -- shoaib.rehman
//				if (!inst.getContext().getIsEscalationStopped()) {
//					
//					//strategy at state level.
//					EscalationStrategy strategy = inst.getCurrentState().getStrategy();
//					
//					//strategy at (state+entity) level. we will give it priority on state level's strategy. 
//					for (StateEntityBinding binding : inst.getBinding().getStateBindings()) {
//						if(inst.getCurrentState().getStateCode().equals(binding.getId().getState().getStateCode())
//								&& inst.getContext().getDocumentId().equals(binding.getEntityId())){
//							strategy = binding.getStrategy();//Strategy overridden -- shoaib.rehman
//							break;
//						}
//					}
//					if(strategy == null)
//						return;
//					EscalationLevel level = strategy.getEscalationLevel(escalationLevel);
//					ActionExecutionManager.getInstance().notifyPostExpiry(inst, null, level);
//					EscalationManager.startEscalation(inst, Boolean.TRUE);
//				}
//			}
//			txnContext.commit();
//		} catch (Exception e) {
//			logger.LogException(
//					"Exception occoured in PostStateExpiryEscalationJob:%s.", e);
//			txnContext.rollback();
//		} catch (Error e) {
//			logger.LogException(
//					"Error occoured in PostStateExpiryEscalationJob:%s.", e);
//			txnContext.rollback();
//		} finally {
//			txnContext.release();
//		}
//	}
//}
