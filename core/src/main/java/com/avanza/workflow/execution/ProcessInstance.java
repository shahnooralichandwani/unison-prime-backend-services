package com.avanza.workflow.execution;

/**
 * This class provides public interface for workflow.
 * This class is different from ProcessManager because it uses Document and Workflow Engine.
 */

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.avanza.core.sdo.Attribute;
import com.avanza.core.sdo.DataObject;
import com.avanza.workflow.configuration.EventType;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.ProcessParticipant;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.Transition;
import com.avanza.workflow.configuration.WorkflowConfigurationException;
import com.avanza.workflow.configuration.action.ActionBinding;
import com.avanza.workflow.escalation.EscalationStrategy;

public interface ProcessInstance {

    public static String Id = "ProcessInstance";

    public abstract String getInstanceId();

    public abstract boolean initiate(TaskContext ctx);

    public abstract void setInstanceId(String instanceId);

    public abstract void setAttribute(String attribId, Attribute attrib);

    public abstract boolean moveToNextState(TaskContext context);

    public abstract boolean moveToNextState(TaskContext context, boolean isMannual);

    // Jump to any state
    public abstract boolean moveToState(String stateId, TaskContext context);

    public abstract long getStateTAT(String stateId) throws WorkflowConfigurationException;

    public abstract long getStateTAT();

    public abstract void setTatExpired();

    public abstract long getRemainingStateTAT();

    public abstract long getRemainingProcessTAT();

    public abstract long getProcessTAT();

    public abstract boolean isStateTATExpired();

    public abstract boolean isProcessTATExpired();

    public abstract List<String> getCurrentActor();

    public abstract void setCurrentActor(List<String> actorsids);

    public abstract void setCalculatedActor(List<String> actorsids);

    public abstract Set<String> getAssignedRoles();

    public abstract Set<Transition> nextTasks();

    public Set<State> nextStates();

    public void persitInstance();

    public List<String> getCalculatedActor();

    public abstract boolean isOnAdhocState();

    public abstract void lockProcess(String ActorId);

    public abstract State getCurrentState();

    public abstract State getPreviousState();

    public abstract void setPreviousState(State previousState);

    public abstract ProcessContext getContext();

    public abstract void setContext(ProcessContext context);

    abstract void setCurrentState(State stat);

    public abstract String getProcessCode();

    public abstract void unlockProcess();

    public void setSupervisorAsActors(String orgId);

    public State getProcessState(String stateCode);

    public Date getStateEntryTime();

    public Date getProcessStartTime();

    public EscalationStrategy getStrategy();

    public List<String> getSupervisorRole(String roleId);

    public List<ActionBinding> getSortedActionBindings(EventType e);

    public List<ActionBinding> getSortedStateTaskActionBindings(EventType e, String stateTaskCode);

    public List<String> getSupervisors();

    public Set<ProcessParticipant> getProcessParticipants(String metaEnt);

    public Set<StateParticipant> getStateParticipants(String metaEnt, String stateCode);

    public ProcessEntityBinding getBinding();

    public List<Transition> getUserActivities(String userId);

    public DataObject getDataObject();

    public void setDataObject(DataObject obj);
}