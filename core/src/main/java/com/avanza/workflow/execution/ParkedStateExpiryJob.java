package com.avanza.workflow.execution;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.scheduler.JobImpl;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.workflow.escalation.EscalationLevel;
import com.avanza.workflow.escalation.EscalationManager;
import com.avanza.workflow.escalation.EscalationStrategy;
import com.avanza.workflow.escalation.ExecutionStrategyContext;
import com.avanza.workflow.logger.ParkedStateManager;

public class ParkedStateExpiryJob extends JobImpl {

    private Logger logger = Logger.getLogger(ParkedStateExpiryJob.class);

    private static ProcessContext getProcessContext(
            ExecutionStrategyContext strategyContext) {
        Search instanceSearch = new Search();
        MetaEntity entity = MetaDataRegistry.getMetaEntity(strategyContext.getDocumentId());
        MetaEntity parentEntity = MetaDataRegistry
                .getNonAbstractParentEntity(entity);
        MetaAttribute primaryAttribute = entity.getPrimaryKeyAttribute();
        instanceSearch.addFrom(parentEntity.getSystemName());
        instanceSearch.addCriterion(SimpleCriterion.equal(primaryAttribute
                .getSystemName(), (String) strategyContext.getMap().get("DOC_NUM")));
        DataBroker broker = DataRepository.getBroker(entity.getSystemName());
        List<DataObject> dataObject = broker.find(instanceSearch);
        return new ProcessContextImpl(dataObject.get(0));

    }

    @Override
    public void executeInner() {
        TransactionContext txnContext = ApplicationContext.getContext()
                .getTxnContext();

        ExecutionStrategyContext strategyContext = new ExecutionStrategyContext(
                getDataMap());

        String processCode = (String) strategyContext.getMap().get("PROCESS_CODE");

        if (processCode == null) {
            logger.logWarning("no process code found");
            return;
        }

        ProcessContext processContext = getProcessContext(strategyContext);

        ProcessInstance instance = ProcessManager.getInstance()
                .populateInstance(processCode, processContext);

        if (instance.getCurrentState().getParked()) {
            logger.logInfo("it is in parking state.");
            ApplicationContext.getContext().add(WorkflowContextLookup.PROCESS_INSTANCE_PARAM, instance);
            WorkflowExcutionFunctions.moveToState((String) strategyContext.getMap().get("UNPARKED_STATE_CODE"));
            return;
        } else {
            return;
        }

    }
}
