package com.avanza.workflow.execution;

public class ConfirmationActionResult extends ValidatorActionResult {

    public ConfirmationActionResult(boolean success, String msg, Exception e, boolean showConfirmation) {
        super(success, msg, e);
        this.showConfirmation = showConfirmation;
    }

    public ConfirmationActionResult() {
        super();
    }

    private boolean showConfirmation;

    public boolean getShowConfirmation() {
        return showConfirmation;
    }

    public void setShowConfirmation(boolean showConfirmation) {
        this.showConfirmation = showConfirmation;
    }

}
