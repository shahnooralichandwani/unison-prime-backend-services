package com.avanza.workflow.execution;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class contains context information for Task execution
 *
 * @author fkazmi
 */

public class TaskContext implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 7256234781180727484L;

    String userId;
    String taskCode;
    String stateCode;
    String processInstanceId;
    Date taskDate;
    String finalNotes;
    Map<String, Object> parameters = new HashMap<String, Object>();
    Map<String, String> messages = new HashMap<String, String>(0);
    String nextAction;
    Object previousActionResponse;
    boolean isParallel;


    public TaskContext(String userid, String taskcode, String processInstanceid, String stateCode) {
        this.userId = userid;
        this.taskCode = taskcode;
        this.processInstanceId = processInstanceid;
        this.taskDate = new Date();
        this.stateCode = stateCode;
    }

    public TaskContext(String userid, String taskcode, String processInstanceid, Object finalNotes, String stateCode) {
        this.userId = userid;
        this.taskCode = taskcode;
        this.stateCode = stateCode;
        this.processInstanceId = processInstanceid;
        this.taskDate = new Date();
        this.finalNotes = finalNotes != null ? (String) finalNotes : "";
    }

    //--- Nida Added for Parallel Assignment
    public TaskContext(String userid, String taskcode, String processInstanceid, Object finalNotes, Object isParallel, String stateCode) {
        this.userId = userid;
        this.taskCode = taskcode;
        this.stateCode = stateCode;
        this.processInstanceId = processInstanceid;
        this.taskDate = new Date();
        this.finalNotes = finalNotes != null ? (String) finalNotes : "";
        this.isParallel = isParallel != null && (Boolean) isParallel ? true : false;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public Date getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(Date taskDate) {
        this.taskDate = taskDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFinalNotes() {
        return finalNotes;
    }

    public void setFinalNotes(String finalNotes) {
        this.finalNotes = finalNotes;
    }

    public void addMessage(String key, String message) {
        messages.put(key, message);
    }

    public String getMessage(String key) {
        return messages.get(key);
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getNextAction() {
        return nextAction;
    }

    public void setNextAction(String nextAction) {
        this.nextAction = nextAction;
    }

    public Object getPreviousActionResponse() {
        return previousActionResponse;
    }

    public void setPreviousActionResponse(Object previousActionResponse) {
        this.previousActionResponse = previousActionResponse;
    }

}
