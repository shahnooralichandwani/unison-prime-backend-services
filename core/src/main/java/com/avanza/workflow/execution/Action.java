package com.avanza.workflow.execution;

import com.avanza.workflow.configuration.action.ActionBinding;

/**
 * Main Action Interface
 *
 * @author hmirza
 */
public interface Action {

    public Object act(ProcessInstance process, ActionBinding action, Object context);
}
