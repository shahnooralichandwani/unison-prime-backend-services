package com.avanza.workflow.execution;

import com.avanza.workflow.configuration.action.ActionBinding;

/**
 * @author hmirza
 */

public abstract class TaskAllocatorAction implements Action {

    public abstract Object act(ProcessInstance process, ActionBinding action, Object context);

}
