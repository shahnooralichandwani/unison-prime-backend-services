package com.avanza.workflow.execution;


import com.avanza.workflow.configuration.action.ActionBinding;

/**
 * @author hmirza
 */
public abstract class ValidatorAction implements Action {

    public abstract ValidatorActionResult act(ProcessInstance process, ActionBinding action, Object context);
}
