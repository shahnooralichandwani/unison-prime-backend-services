package com.avanza.workflow.execution;

import java.util.ArrayList;
import java.util.List;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.OrganizationalUnit;


public class WorkflowExcutionFunctions {

    // Script >> UserInOrg('orgid')
    public static Boolean UserInOrg(String orgid) {
        String userid = ApplicationContext.getContext().get(WorkflowContextLookup.USER_IN_ORG_PARAM);
        return OrganizationManager.isUserInOrg(orgid, userid);
    }

    // Script >> setFieldValue('PRIORITY','HIGH')
    public static void setFieldValue(String attributeId, Object newValue) {
        ProcessInstance instance = ApplicationContext.getContext().get(WorkflowContextLookup.PROCESS_INSTANCE_PARAM);
        instance.getContext().setAttribute(attributeId, newValue);
    }

    // // Script >> setCurrentState('StateCode1')
    // public static void setCurrentState(String stateCode) {
    // ProcessInstance instance = ApplicationContext.getContext().get(WorkflowContextLookup.PROCESS_INSTANCE_PARAM);
    // instance.setCurrentState(instance.getProcessState(stateCode));
    // }

    // Script >> moveToState('StateCode1')
    public static void moveToState(String stateCode) {
        try {
            ProcessInstance instance = ApplicationContext.getContext().get(WorkflowContextLookup.PROCESS_INSTANCE_PARAM);
            TaskContext ctx = new TaskContext("System", null, instance.getInstanceId(), stateCode);
            instance.moveToState(stateCode, ctx);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Script >> setPriority('Low')
    public static void setPriority(String priority) {
        ProcessInstance instance = ApplicationContext.getContext().get(WorkflowContextLookup.PROCESS_INSTANCE_PARAM);
        instance.getContext().setDocumentPriority(priority);
        instance.getContext().saveContext();
    }

    // Script >> setPossibleActors('sara.ahmed','admin','awad.salim')
    public static void setPossibleActors(String... userids) {
        ProcessInstance instance = ApplicationContext.getContext().get(WorkflowContextLookup.PROCESS_INSTANCE_PARAM);
        List<String> userIdList = new ArrayList<String>(0);
        for (String user : userIdList) {
            userIdList.add(user);
        }
        instance.setCurrentActor(userIdList);
    }

    // Script >> lockActor('admin')
    public static void lockActor(String userid) {
        ProcessInstance instance = ApplicationContext.getContext().get(WorkflowContextLookup.PROCESS_INSTANCE_PARAM);
        List<String> userList = instance.getCurrentActor();
        if (userList.contains(userid)) {
            instance.setCalculatedActor(userList);
            List<String> userIdList = new ArrayList<String>(0);
            userIdList.add(userid);
            instance.setCurrentActor(userIdList);
        } else {
            // TODO: what to do?
        }
    }

    public static boolean checkContextValue(String key, Object value) {
        ProcessContext ctx = ApplicationContext.getContext().get(WorkflowContextLookup.PROCESS_CONTEXT_PARAM);
        if (ctx.getAttribute(key).equals(value)) return true;
        return false;
    }

    public static boolean isProcessEnded() {
        ProcessInstance instance = ApplicationContext.getContext().get(WorkflowContextLookup.PROCESS_INSTANCE_PARAM);
        return instance.getCurrentState().getEnd();
    }

    public static Boolean isUserInRole(String... pRoles) {
        boolean retVal = false;
        UserDbImpl user = (UserDbImpl) ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);

        for (int i = 0; i < pRoles.length; i++) {

            if (OrganizationManager.isRoleAssigned(user.getLoginId(), pRoles[i])) {
                retVal = true;
                break;

            }
        }

        return retVal;

    }

    public static Boolean isUserInOrg(String... orgids) {

        boolean retVal = false;
        UserDbImpl user = (UserDbImpl) ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);

        for (int i = 0; i < orgids.length; i++) {

            if (OrganizationManager.isUserInOrg(orgids[i], user.getLoginId())) {
                retVal = true;
                break;
            }

        }

        return retVal;
    }

}
