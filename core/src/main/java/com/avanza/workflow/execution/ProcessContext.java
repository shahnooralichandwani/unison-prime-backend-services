package com.avanza.workflow.execution;

import java.util.Date;
import java.util.List;
import java.util.Map;


public interface ProcessContext {

    public void saveContext();

    public Object getAttribute(String attribId);

    public Object getNonPrsistableAttribute(String attribId);

    public void setNonPrsistableAttribute(String attribId, Object attrib);

    public void setAttribute(String attribId, Object attrib);

    public String getInstanceId();

    public String getDocumentId();

    public String getCurrentOrgUnit();

    public void setDocumentPriority(String priority);

    public Date getStateEntryTime();

    public Date getProcessStartTime();

    public void setProcessStartTime(Date dateTime);

    public void setProductEntity(Object entity);

    public String getCustomerRim();

    public String getProductEntity();

    public String getTargetBranch();

    public String getProcessCode();

    public Map<String, Object> getValues();

    @Deprecated
    public boolean getIsEscalationStopped();

    @Deprecated
    public void setIsEscalationStopped(boolean escalationStopped);

    public List<String> getCurrentActors();

    public void setCurrentActors(List<String> actorIds);

    public List<String> getCalculatedActors();

    public void setCalculatedActors(List<String> actorIds);

    public void setCurrentActor(Object currentActor);

    public String getCurrentActor();

    public void setCalculatedActor(Object calculatedActor);

    public String getCalculatedActor();

    public void setTatExpired(Object value);

    public boolean getTatExpired();

    public void setStateEntryTime(Object dateTime);

    public void setCurrentState(Object currentState);

    public String getCurrentState();

    public void setProcessCode(Object processCode);

    public void setCurrentOrgUnit(Object currentOrgUnit);
}
