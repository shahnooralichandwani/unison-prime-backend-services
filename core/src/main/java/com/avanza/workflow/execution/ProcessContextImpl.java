package com.avanza.workflow.execution;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.DataObjectCollection;
import com.avanza.core.util.Convert;


public class ProcessContextImpl implements ProcessContext {

    private DataObject dataobject;
    private HashMap<String, Object> nonPrsistableMap = new HashMap<String, Object>(0);


    public ProcessContextImpl(DataObject obj) {
        this.dataobject = obj;
    }

    public Object getAttribute(String attribId) {
        return dataobject.getValue(attribId);
    }

    public void saveContext() {
        dataobject.save();
    }

    public void setAttribute(String attribId, Object attribvalue) {
        dataobject.setValue(attribId, attribvalue);
    }

    public String getInstanceId() {
        return dataobject.getIdValue();
    }

    public String getProcessCode() {
        return dataobject.getProcessCode();
    }

    public String getDocumentId() {
        return dataobject.getMetaEntity().getId();
    }

    public String getCurrentOrgUnit() {
        return dataobject.getCurrentOrgUnit();
    }

    public void setDocumentPriority(String priority) {
        setAttribute("DOC_PRIORITY", priority);
    }

    public Date getStateEntryTime() {
        return dataobject.getStateEntryTime();
    }

    public Date getProcessStartTime() {
        return dataobject.getProcessStartTime();
    }

    public void setProcessStartTime(Date dateTime) {
        setAttribute("PROCESS_START_TIME", dateTime);
    }

    public String getCustomerRim() {
        return dataobject.getCustomerRIM();
    }

    public String getProductEntity() {
        return dataobject.getProductMetaEntity();
    }

    public void setProductEntity(Object entity) {
        setAttribute("PRODUCT_ENTITY_ID", entity);
    }

    public String getTargetBranch() {
        return dataobject.getAsString("BRANCH_CODE");
    }

    public Map<String, Object> getValues() {
        return dataobject.getValues();
    }

    @Override
    public boolean getIsEscalationStopped() {
        return dataobject.getAsBoolean("STOP_STATE_ESC");
    }

    @Override
    public void setIsEscalationStopped(boolean escalationStopped) {
        setAttribute("STOP_STATE_ESC", escalationStopped);
    }

    @Override
    public List<String> getCurrentActors() {
        List<String> userids = new ArrayList<String>();
        Object value = this.getAttribute("CURRENT_ACTOR");
        if (value != null) {
            String ActorList = value.toString();
            userids = Convert.toList(ActorList);
        }
        return userids;
    }

    @Override
    public void setCurrentActors(List<String> actorIds) {
        String ActorsStr = Convert.toCSV(actorIds);
        this.setAttribute("CURRENT_ACTOR", ActorsStr);
        //setActors(actorIds,true); // -- disabled:riyaz.ahmed[[BAFL] - [15959] - eform processing delay (10-12 seconds)]
    }

    @Override
    public List<String> getCalculatedActors() {
        List<String> userids = new ArrayList<String>();
        Object value = this.getAttribute("CALCULATED_ACTOR");
        if (value != null) {
            String ActorList = value.toString();
            userids = Convert.toList(ActorList);
        }
        return userids;
    }

    @Override
    public void setCalculatedActors(List<String> actorIds) {
        String ActorsStr = Convert.toCSV(actorIds);
        this.setAttribute("CALCULATED_ACTOR", ActorsStr);
        //setActors(actorIds,true); // -- disabled:riyaz.ahmed[[BAFL] - [15959] - eform processing delay (10-12 seconds)]
    }

    private void setActors(List<String> actorIds, boolean isCurrent) {
        MetaEntity subEntity = MetaDataRegistry.getEntityBySystemName(MetaDataRegistry.getNonAbstractParentEntity(dataobject.getMetaEntity()).getSystemName() + "Actor");
        DataObjectCollection collection = dataobject.getRelatedObjects(subEntity.getId());
        ;

        //Delete existing actors
        for (DataObject object : collection) {
            if (object.getAsBoolean("IS_CURRENT") == isCurrent) {
                object.markDelete();
            }
        }

        //Add New actors
        for (String actorId : actorIds) {
            DataObject actor = new DataObject(subEntity);
            actor.setValue("USER_ID", actorId);
            actor.setValue("IS_CURRENT", isCurrent);
            collection.add(actor);
        }

    }

    @Override
    public Object getNonPrsistableAttribute(String attribId) {
        return nonPrsistableMap.get(attribId);
    }

    @Override
    public void setNonPrsistableAttribute(String attribId, Object attrib) {
        nonPrsistableMap.put(attribId, attrib);

    }

    @Override
    public void setCurrentOrgUnit(Object currentOrgUnit) {
        setAttribute("CURRENT_ORG_UNIT", currentOrgUnit);
    }

    @Override
    public void setProcessCode(Object processCode) {
        setAttribute("PROCESS_CODE", processCode);
    }

    @Override
    public void setStateEntryTime(Object dateTime) {
        setAttribute("STATE_ENTRY_TIME", dateTime);
    }

    @Override
    public void setTatExpired(Object value) {
        setAttribute("IS_TAT_EXPIRED", value);
    }

    @Override
    public boolean getTatExpired() {
        return Boolean.getBoolean(dataobject.getAsString("IS_TAT_EXPIRED"));
    }

    @Override
    public void setCurrentState(Object currentState) {
        setAttribute("CURRENT_STATE", currentState);
    }

    @Override
    public String getCurrentState() {
        return dataobject.getAsString("CURRENT_STATE");
    }

    @Override
    public void setCurrentActor(Object currentActor) {
        setAttribute("CURRENT_ACTOR", currentActor);
    }

    @Override
    public void setCalculatedActor(Object calculatedActor) {
        setAttribute("CALCULATED_ACTOR", calculatedActor);
    }

    public String getCalculatedActor() {
        return dataobject.getAsString("CALCULATED_ACTOR");
    }

    @Override
    public String getCurrentActor() {
        return dataobject.getAsString("CURRENT_ACTOR");
    }
}
