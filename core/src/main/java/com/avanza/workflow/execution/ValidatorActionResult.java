package com.avanza.workflow.execution;

public class ValidatorActionResult extends ActionResult {

    private boolean success;

    private String message;

    private Exception validationException;

    public ValidatorActionResult() {

    }

    public ValidatorActionResult(boolean success, String msg, Exception e) {
        this.message = msg;
        this.success = success;
        this.validationException = e;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Exception getValidationException() {
        return validationException;
    }

    public void setValidationException(Exception validationException) {
        this.validationException = validationException;
    }
}
