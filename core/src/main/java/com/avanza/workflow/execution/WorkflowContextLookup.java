package com.avanza.workflow.execution;


public class WorkflowContextLookup {

    public static String USER_IN_ORG_PARAM = "userInOrgUser";
    public static String PROCESS_INSTANCE_PARAM = "ProcessInstance";
    public static String PROCESS_CONTEXT_PARAM = "ProcessContext";
    public static String USER = "User";
    public static String CUSTOMER_ENTITY = "Unison.Customer";
    public static String CONTEXT_MESSAGE = "ContextMessage";
    //added below fields for confirmation dialogue -- shoaib.rehman
    public static String PROCESSED = "processed";
    public static String TASK_CONTEXT = "taskContext";
    public static String SHOW_CONFIRMATION = "showConfirmation";
    public static String EXECUTION_VALID = "isExecutionValid";
    public static String CANCEL_EXECUTION = "cancelExecution";
    //added for conditional actions -- shoaib.rehman
    public static String PREDECESSOR_OUTPUT = "predecessorOuput";
}
