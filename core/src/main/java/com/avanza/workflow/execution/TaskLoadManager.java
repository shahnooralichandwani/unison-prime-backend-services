package com.avanza.workflow.execution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Column;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.OperatorType;
import com.avanza.core.data.expression.Order;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.util.CollectionHelper;
import com.avanza.core.util.StringHelper;
import com.avanza.workflow.configuration.UserWorkLoad;


public class TaskLoadManager {

    public static void calculateAndAssign(List<String> users, String documentId, ProcessInstance instance) {
        calculateAndAssign(users, documentId, instance, "ALL");
    }

    public static void calculateAndAssign(List<String> users, String documentId, ProcessInstance instance, String group) {
        String emptyUser = getUserOfMinimumWorkload(users, documentId, group);
        ;
        System.out.println(emptyUser);
        List<String> arrList = new ArrayList<String>(0);
        arrList.add(emptyUser);
        instance.setCurrentActor(arrList);
        incrementUserWorkLoad(emptyUser, documentId);
    }


    public static String getUserOfMinimumWorkload(List<String> users, String documentId, String group) {
        List<String> sortedUsers = getUserSortedListByWorkload(users, documentId, group);
        String freshUser = CollectionHelper.getFirstMissingItem(users, sortedUsers, true);

        if (!StringHelper.isEmpty(freshUser)) {
            //if user has not been aligned yet.
            //then he will be considered having minimum workload
            return freshUser;
        } else if (sortedUsers.size() > 0) {
            //return first user from the sorted list
            return sortedUsers.get(0);
        }

        return null;
    }

    private static List<String> getUserSortedListByWorkload(List<String> users, String documentId, String group) {
        DataBroker broker = DataRepository.getBroker(UserWorkLoad.class.toString());
        Search search = new Search(UserWorkLoad.class);
        search.setCanRepresent(true);
        Column colLoginId = search.getFirstFrom().createColumn("loginId");
        search.addColumn(colLoginId);
        search.addCriterion(SimpleCriterion.getSimpleCriterion(OperatorType.In, colLoginId, users));
        search.addCriterion(Criterion.equal("mainMetaEntity", documentId));
        search.addCriterion(Criterion.equal("subMetaEntity", group));
        search.addOrder(new Order("assignedDocuments"));
        List<String> sortedUsers = broker.find(search);
        return sortedUsers;
    }

    public synchronized static void incrementUserWorkLoad(String userid, String documentId) {
        incrementUserWorkLoad(userid, documentId, "ALL");
    }

    public synchronized static void incrementUserWorkLoad(String userid, String documentId, String group) {
        List<UserWorkLoad> load = getUserWorkLoad(userid, documentId, group);
        DataBroker broker = DataRepository.getBroker(TaskLoadManager.class.toString());
        if (load.size() > 0) {
            UserWorkLoad workLoad = load.get(0);
            workLoad.setAssignedDocuments(workLoad.getAssignedDocuments() + 1);
            broker.update(workLoad);
        } else {
            UserWorkLoad workLoad = new UserWorkLoad();
            workLoad.setAssignedDocuments(1);
            workLoad.setLoginId(userid);
            workLoad.setMainMetaEntity(documentId);
            workLoad.setSubMetaEntity(group);
            broker.add(workLoad);
        }
    }

    public synchronized static void decrementUserWorkLoad(String userid, String documentId) {
        decrementUserWorkLoad(userid, documentId, "ALL");
    }

    public synchronized static void decrementUserWorkLoad(String userid, String documentId, String group) {
        List<UserWorkLoad> load = getUserWorkLoad(userid, documentId, group);
        DataBroker broker = DataRepository.getBroker(TaskLoadManager.class.toString());
        if (load.size() > 0) {
            UserWorkLoad workLoad = load.get(0);
            workLoad.setAssignedDocuments(workLoad.getAssignedDocuments() - 1);
            broker.update(workLoad);
        } else {
            // No user present with such document
        }
    }

    private static List<UserWorkLoad> getUserWorkLoad(String user, String entityId, String group) {
        Search searchObj = new Search(UserWorkLoad.class);
        searchObj.addColumn("assignedDocuments");
        searchObj.addCriterion(Criterion.equal("loginId", user));
        searchObj.addCriterion(Criterion.equal("mainMetaEntity", entityId));
        searchObj.addCriterion(Criterion.equal("subMetaEntity", group));
        DataBroker broker = DataRepository.getBroker(TaskLoadManager.class.toString());
        List<UserWorkLoad> load = broker.find(searchObj);
        return load;
    }

}
