package com.avanza.workflow.execution;

public class ActionResult {

    protected String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
