package com.avanza.workflow.execution;

import java.util.Set;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.HibernateDataBroker;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.sequence.SequenceManager;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.workflow.configuration.Process;
import com.avanza.workflow.configuration.ProcessCatalog;
import com.avanza.workflow.configuration.ProcessEntityBinding;
import com.avanza.workflow.configuration.State;
import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.WorkFlowEngine;

public class AvanzaWorkflowEngine extends WorkFlowEngine {

    public Process findProcess(String processId) {
        ProcessCatalog.getProcessByProcessCode(processId);
        return null;
    }

    public Set<Process> findProcess(Search searchcriteria) {
        return null;
    }

    public void associateProcess(Process proc, String documentid, Set<String> supervisors, Set<String> creators, Set<String> viewers) {
        HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker("HibernateDataBroker");

        boolean isSupervisor = false;
        boolean isCreater = false;
        for (String viewer : viewers) {
            ProcessEntityBinding procbinding = null;
            if (supervisors.contains(viewer))
                isSupervisor = true;
            if (creators.contains(viewer))
                isCreater = true;
            // procbinding = new ProcessEntityBinding(proc.getProcessCode(),
            // documentid, viewer, isSupervisor, isCreater);
            isSupervisor = false;
            isCreater = false;
            broker.add(procbinding);
        }
    }

    public void associateProcessParticipant(Process proc, String documentid, String role, boolean isSupervisor, boolean isCreater) {
        // ProcessEntityBinding procbinding = new
        // ProcessEntityBinding(proc.getProcessCode(), documentid, role,
        // isSupervisor, isCreater);
        // HibernateDataBroker broker = (HibernateDataBroker)
        // DataRepository.getBroker("HibernateDataBroker");
        // broker.add(procbinding);
    }

    public void associateStateParticipant(State stat, String roleid) {
        StateParticipant binding = new StateParticipant(stat.getStateCode(), stat.getProcessCode(), roleid);
        HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker("HibernateDataBroker");
        broker.add(binding);
    }

    public void associateStateParticipant(State stat, Set<String> role) {
        HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker("HibernateDataBroker");

        for (String rolestr : role) {

            StateParticipant binding = new StateParticipant(stat.getStateCode(), stat.getProcessCode(), rolestr);
            broker.add(binding);
        }
    }

    public ProcessInstance getProcessInstance(String instanceId, String documentid) {

        return null;
    }

    public ProcessInstance createInstance(String documententity, String userid) {
        ProcessInstance procInst = null;
        try {
            ProcessContext context = new ProcessContextImpl(MetaDataRegistry.getMetaEntity(documententity).createEntity());
            Process proc = ProcessCatalog.getProcessByDocumentId(documententity, context);
            if (proc != null) {
                String procid = SequenceManager.getInstance().getNextValue(ProcessInstanceImpl.PROCESS_INSTANCE_COUNTER);
                procInst = new ProcessInstanceImpl(proc, context, procid, null, null, proc.getAssociatedBinding(documententity));
                TaskContext ctx = new TaskContext(userid, context.getInstanceId(), "", null);
                procInst.initiate(ctx);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return procInst;
    }

    public void saveInstance(ProcessInstance procInstance) {
        procInstance.getContext().saveContext();
    }
}
