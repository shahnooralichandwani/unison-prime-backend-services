package com.avanza.workflow.execution;


import java.util.HashMap;

import com.avanza.workflow.configuration.action.ActionBinding;

/**
 * @author shoaib.rehman
 */
public abstract class ConfirmationAction implements Action {

    /**
     * This method is a wrapper to the original Action interface's
     * act method - it ensures that the action calling is valid.
     * Based on the condition it will allow original action to fire otherwise return
     */
    @SuppressWarnings("unchecked")
    public ConfirmationActionResult act(ProcessInstance process, ActionBinding action, Object context) {
        ConfirmationActionResult result = process(process, action, context);
        if (!result.isSuccess()) {
            //override showConfirmation to true even if user
            //made it false deliberately
            result.setShowConfirmation(Boolean.TRUE);
            HashMap<String, ActionBinding> skippedAction = (HashMap<String, ActionBinding>) ((TaskContext) context).getParameters().get(WorkflowContextLookup.PROCESSED);
            if (skippedAction == null) {
                skippedAction = new HashMap<String, ActionBinding>();
                skippedAction.put(action.getActionCode(), action);
                ((TaskContext) context).getParameters().put(WorkflowContextLookup.PROCESSED, skippedAction);
            } else
                skippedAction.put(action.getActionCode(), action);
            ((TaskContext) context).getParameters().put(WorkflowContextLookup.TASK_CONTEXT, context);
        }
        return result;
    }

    public abstract ConfirmationActionResult process(ProcessInstance process, ActionBinding action, Object context);
}
