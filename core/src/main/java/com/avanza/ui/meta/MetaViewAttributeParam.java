package com.avanza.ui.meta;

import java.io.Serializable;

import com.avanza.core.data.DbObject;

public class MetaViewAttributeParam extends DbObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String name;

    private String metaViewAttributeId;

    private String metaViewId;

    private String value;
    //added by muhammad akif
    private String selectCriteria;
    private String selectCriteria2;
    private String keyValue;
    private String filterClass;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMetaViewAttributeId() {
        return metaViewAttributeId;
    }

    public void setMetaViewAttributeId(String metaViewAttributeId) {
        this.metaViewAttributeId = metaViewAttributeId;
    }

    public String getMetaViewId() {
        return metaViewId;
    }

    public void setMetaViewId(String metaViewId) {
        this.metaViewId = metaViewId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSelectCriteria() {
        return selectCriteria;
    }

    public void setSelectCriteria(String selectCriteria) {
        this.selectCriteria = selectCriteria;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public String getFilterClass() {
        return filterClass;
    }

    public void setFilterClass(String filterClass) {
        this.filterClass = filterClass;
    }

    public String getSelectCriteria2() {
        return selectCriteria2;
    }

    public void setSelectCriteria2(String selectCriteria2) {
        this.selectCriteria2 = selectCriteria2;
    }


}