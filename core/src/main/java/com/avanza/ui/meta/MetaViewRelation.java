package com.avanza.ui.meta;

import java.util.Random;

import org.apache.commons.beanutils.BeanUtils;

import com.avanza.core.data.DbObject;
import com.avanza.core.interceptor.Auditable;
import com.avanza.core.meta.audit.MetaViewRelationAudit;
import com.avanza.core.meta.audit.MetaViewRelationAuditId;
import com.avanza.core.meta.audit.OrgRoleAudit;
import com.avanza.core.meta.audit.OrgRoleAuditId;


public class MetaViewRelation extends DbObject implements Auditable {

    private String relationShipId;

    private String parentViewId;

    private String childViewId;

    private int displayOrder;

    private String relationshipType;


    public String getRelationShipId() {
        return relationShipId;
    }


    public void setRelationShipId(String relationShipId) {
        this.relationShipId = relationShipId;
    }


    public String getRelationshipType() {
        return relationshipType;
    }


    public void setRelationshipType(String relationshipType) {
        this.relationshipType = relationshipType;
    }


    public String getParentViewId() {
        return parentViewId;
    }


    public void setParentViewId(String parentViewId) {
        this.parentViewId = parentViewId;
    }


    public String getChildViewId() {
        return childViewId;
    }


    public void setChildViewId(String childViewId) {
        this.childViewId = childViewId;
    }


    public int getDisplayOrder() {
        return displayOrder;
    }


    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }


    @Override
    public Object getAuditEntry(Short revtype) {
        MetaViewRelationAudit metaViewRelationAudit = null;
        try {


            Random rand = new Random();

            metaViewRelationAudit = new MetaViewRelationAudit();
            metaViewRelationAudit.setRevtype(revtype);

            BeanUtils.copyProperties(metaViewRelationAudit, this);
            metaViewRelationAudit.setMetaViewRelationAuditId(new MetaViewRelationAuditId(this.relationShipId, rand.nextInt()));

            //secUserV.setId(new SecUserVId(this.loginId, CounterUtils.getNextAuditTableKey()));
        } catch (Exception e) {
            metaViewRelationAudit = null;
        }
        return metaViewRelationAudit;
    }


}
