package com.avanza.ui.meta;

import java.io.Serializable;

import com.avanza.core.data.DbObject;

public class MetaViewParam extends DbObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String paramName;

    private String viewid;

    private String paramValue;

    public MetaViewParam() {
    }

    public MetaViewParam(String pname, String view, String pvalue) {
        this.paramName = pname;
        this.viewid = view;
        this.paramValue = pvalue;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getViewid() {
        return viewid;
    }

    public void setViewid(String viewid) {
        this.viewid = viewid;
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + ((paramName == null) ? 0 : paramName.hashCode());
        result = PRIME * result + ((paramValue == null) ? 0 : paramValue.hashCode());
        result = PRIME * result + ((viewid == null) ? 0 : viewid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final MetaViewParam other = (MetaViewParam) obj;
        if (paramName == null) {
            if (other.paramName != null)
                return false;
        } else if (!paramName.equals(other.paramName))
            return false;
        if (paramValue == null) {
            if (other.paramValue != null)
                return false;
        } else if (!paramValue.equals(other.paramValue))
            return false;
        if (viewid == null) {
            if (other.viewid != null)
                return false;
        } else if (!viewid.equals(other.viewid))
            return false;
        return true;
    }

}
