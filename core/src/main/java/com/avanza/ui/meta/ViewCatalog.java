package com.avanza.ui.meta;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.HibernateDataBroker;
import com.avanza.core.util.CollectionHelper;
import com.avanza.ui.ViewGenerator;
import com.avanza.ui.ViewType;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ViewCatalog {

    private static Map<String, MetaView> viewsDefinations = new Hashtable<String, MetaView>(0);
    private static Map<String, MetaViewRelation> viewRelations = new Hashtable<String, MetaViewRelation>(0);

    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock readLock = readWriteLock.readLock();
    private static Lock writeLock = readWriteLock.writeLock();

    public static void load() {
        HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker(ViewCatalog.class.getName());
        List<MetaView> views = broker.findAll(MetaView.class);
        for (MetaView view : views) {
            String id = view.getId();
            viewsDefinations.put(id, view);
        }
        List<MetaViewRelation> relations = broker.findAll(MetaViewRelation.class);
        for (MetaViewRelation rel : relations) {
            String id = rel.getRelationShipId();
            viewRelations.put(id, rel);
        }
        fillSubViews();
    }

    private static void fillSubViews() {
        for (MetaView view : viewsDefinations.values()) {
            List<MetaViewRelation> relations = getViewRelations(view.getId());
            for (MetaViewRelation rel : relations) {
                MetaView childView = viewsDefinations.get(rel.getChildViewId());
                MetaView cloneChild = childView.clone();
                cloneChild.setDisplayOrder(rel.getDisplayOrder());
                cloneChild.setParent(view);
                if (cloneChild.getDocumentId() == null || cloneChild.getDocumentId().length() <= 0)
                    cloneChild.setDocumentId(view.getDocumentId());
                view.addSubview(cloneChild);
            }
        }
    }

    public static List<MetaViewRelation> getViewRelations(String parentId) {
        List<MetaViewRelation> relations = new ArrayList<MetaViewRelation>(0);
        for (MetaViewRelation rel : viewRelations.values()) {
            if (rel.getParentViewId().equalsIgnoreCase(parentId))
                relations.add(rel);
        }
        return relations;
    }

    public static void addView(MetaView view) {
        String id = view.getId();
        viewsDefinations.put(id, view);
    }

    public static void reLoad() {
        //Shuwair
        try {
            readLock.lock();
            viewsDefinations = new Hashtable<String, MetaView>(0);
            viewRelations = new Hashtable<String, MetaViewRelation>(0);
            HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker(ViewCatalog.class.getName());
            List<MetaView> views = broker.findAll(MetaView.class);
            for (MetaView view : views) {
                String id = view.getId();
                viewsDefinations.put(id, view);
            }
            List<MetaViewRelation> relations = broker.findAll(MetaViewRelation.class);
            for (MetaViewRelation rel : relations) {
                String id = rel.getRelationShipId();
                viewRelations.put(id, rel);
            }
            fillSubViews();
        } finally {
            readLock.unlock();
        }
    }

    public static void dispose() {

        viewsDefinations = new Hashtable<String, MetaView>(0);
    }

    public static MetaView getMetaView(String viewid) {
        return viewsDefinations.get(viewid);
    }

    // TODO Need to refactor fetch view from metaEntity.
    public static MetaView getMetaView(String metentid, ViewType type, boolean topMost) {
        Collection<MetaView> viewCollection = viewsDefinations.values();
        for (MetaView view : viewCollection) {
            if (view.getDocumentId() == null)
                continue;
            if (view.getDocumentId().equals(metentid) && view.getType().equals(type.getValue())) {
                if (topMost) {
                    if (view.getParent() == null) {
                        // comment it, because display the view while selectCriteria is null or not null
						/*String selectCritera = view.getSelectCriteria();
						if (selectCritera == null || selectCritera.length() <= 0) {
							return view;
						}*/
                        return view;
                    }
                } else {
                    String selectCritera = view.getSelectCriteria();
                    if (selectCritera == null || selectCritera.length() <= 0) {
                        return view;
                    }
                }
            }
        }
        MetaView defaultView = ViewGenerator.generateView(metentid, type);
        viewsDefinations.put(defaultView.getId(), defaultView);
        return defaultView;
    }

    public static List<MetaView> getMetaViews(String documentid) {

        return null;
    }

    public static List<MetaView> getMetaViews(ViewType type) {

        List<MetaView> metaViews = new ArrayList<MetaView>();
        Collection<MetaView> viewCollection = viewsDefinations.values();
        for (MetaView view : viewCollection) {

            if (view.getType().equalsIgnoreCase(type.getValue())) {
                metaViews.add(view);
            }
        }
        return metaViews;
    }

    public static Set getAllViewAttributes(MetaView parentView) {
        HashSet Attributes = new HashSet();
        if (parentView.getSubviews().size() <= 0) {

            Attributes.addAll(CollectionHelper.getNativeHashSet(parentView.getAttributes()));
            return Attributes;
        } else {
            Set subviews = parentView.getSubviews();
            Iterator itr = subviews.iterator();
            while (itr.hasNext()) {
                MetaView subview = (MetaView) itr.next();
                Attributes.addAll(CollectionHelper.getNativeHashSet(getAllViewAttributes(subview)));
            }
        }
        Attributes.addAll(CollectionHelper.getNativeHashSet(parentView.getAttributes()));
        return Attributes;
    }

    public static MetaView getMetaViewBySystemName(String systemName) {
        for (MetaView view : viewsDefinations.values()) {
            if (view.getSystemName().equalsIgnoreCase(systemName))
                return view;
        }
        return null;
    }

    public static MetaView getParentView(String childViewId) {

        for (MetaViewRelation metaViewRelation : viewRelations.values()) {

            if (metaViewRelation.getChildViewId().equalsIgnoreCase(childViewId)) {

                return getMetaView(metaViewRelation.getParentViewId());
            }
        }

        return null;


    }

}
