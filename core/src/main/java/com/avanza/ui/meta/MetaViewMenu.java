package com.avanza.ui.meta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.avanza.core.data.DbObject;

public class MetaViewMenu extends DbObject {

    private static final long serialVersionUID = 2779676904098347695L;

    private String id;

    private String systemName;

    private String viewId;

    private MetaViewMenu parent;

    private String menuType;

    private String displayAttrib;

    private String displayNamePRM;

    private String displayNameSEC;

    private String permissionExp;

    private String menuURL;

    private String actionListener;

    private String urlParams;

    private String search;

    private int displayOrder;

    private String styleClass;

    /**
     * Added By KMA
     */
    private String optionFilterClause;

    private boolean isDefault;

    Set<MetaViewMenu> subMenus = new HashSet<MetaViewMenu>(0);

    public List<MetaViewMenu> getOrderedViewMenus() {
        List<MetaViewMenu> orderedMenus = new ArrayList<MetaViewMenu>();
        orderedMenus.addAll(getSubMenus());
        Collections.sort(orderedMenus, new Comparator<MetaViewMenu>() {

            public int compare(MetaViewMenu o1, MetaViewMenu o2) {
                if (o1.getDisplayOrder() > o2.getDisplayOrder())
                    return 1;
                else if (o1.getDisplayOrder() < o2.getDisplayOrder()) return -1;
                return 0;
            }
        });

        return orderedMenus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public String getDisplayNamePRM() {
        return displayNamePRM;
    }

    public void setDisplayNamePRM(String displayNamePRM) {
        this.displayNamePRM = displayNamePRM;
    }

    public String getDisplayNameSEC() {
        return displayNameSEC;
    }

    public void setDisplayNameSEC(String displayNameSEC) {
        this.displayNameSEC = displayNameSEC;
    }

    public String getPermissionExp() {
        return permissionExp;
    }

    public void setPermissionExp(String permissionExp) {
        this.permissionExp = permissionExp;
    }

    public String getMenuURL() {
        return menuURL;
    }

    public void setMenuURL(String menuURL) {
        this.menuURL = menuURL;
    }

    public String getUrlParams() {
        return urlParams;
    }

    public void setUrlParams(String urlParams) {
        this.urlParams = urlParams;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public MetaViewMenu getParent() {
        return parent;
    }

    public void setParent(MetaViewMenu parent) {
        this.parent = parent;
    }

    public Set<MetaViewMenu> getSubMenus() {
        return subMenus;
    }

    public void setSubMenus(Set<MetaViewMenu> subMenus) {
        this.subMenus = subMenus;
    }

    public String getActionListener() {
        return actionListener;
    }

    public void setActionListener(String actionListener) {
        this.actionListener = actionListener;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getDisplayAttrib() {
        return displayAttrib;
    }

    public void setDisplayAttrib(String displayAttrib) {
        this.displayAttrib = displayAttrib;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String getOptionFilterClause() {
        return optionFilterClause;
    }

    public void setOptionFilterClause(String optionFilterClause) {
        this.optionFilterClause = optionFilterClause;
    }
}
