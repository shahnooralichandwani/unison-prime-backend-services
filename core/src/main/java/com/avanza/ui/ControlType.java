package com.avanza.ui;

import com.avanza.core.CoreException;

/**
 * ControlType Enum
 */
public enum ControlType {

    TextBox, Label, RadioButton, CheckBox, ListBox, ComboBox, RadioPickList, CheckBoxPickList,
    DataGrid, HtmlTable, DataGridHeader, MultiCheckBox, TextArea, OutputText, CalenderControl,
    TreeViewControl, Link, PrimaryLink, KeyValueOutputText, FileUploadControl, ProductSelectionControl,
    AffiliatedAccountLink, GraphicImageIcon, CampaignSelectionControl, StatusControl, CustomControl,
    MaskedOutputText, MaskedTextBox, MaskedComboBox, MaskedProductSelectionControl, MaskedPrimaryLink,
    RDVProductSelectionControl, HBLRDVProductSelectionControl, MaskedRDVProductSelectionControl, RDVListBoxControl, CascadedControl,
    GraphXAxis, FlaggingControl, MarkLink, DetailGraphSum, Notes, NotesDetail, Flag, Timer,
    //added below for list enhancements -- shoaib.rehman
    PageSize, SortColumn, SortType, HtmlEditor,

    //AnamSiddiqi: Below control added as an enhancement to HtmlEditor, to enable export of templates in PDF @ Client machine
    PDFExportControl,
    //Anam Siddiqi: Below control added as an enhancement to ProductSelectionControl, to persist encrypted value of acct/card
    ProductSelectionEncryptControl,
    RichTextArea;

    public static ControlType fromString(String componentType) {
        if (componentType.equalsIgnoreCase(ControlType.TextBox.name())) return ControlType.TextBox;
        if (componentType.equalsIgnoreCase(ControlType.ComboBox.name())) return ControlType.ComboBox;
        if (componentType.equalsIgnoreCase(ControlType.Label.name())) return ControlType.Label;
        if (componentType.equalsIgnoreCase(ControlType.OutputText.name())) return ControlType.OutputText;
        if (componentType.equalsIgnoreCase(ControlType.RadioButton.name())) return ControlType.RadioButton;
        if (componentType.equalsIgnoreCase(ControlType.CheckBox.name())) return ControlType.CheckBox;
        if (componentType.equalsIgnoreCase(ControlType.TextArea.name())) return ControlType.TextArea;
        if (componentType.equalsIgnoreCase(ControlType.CalenderControl.name())) return ControlType.CalenderControl;
        if (componentType.equalsIgnoreCase(ControlType.ListBox.name())) return ControlType.ListBox;
        if (componentType.equalsIgnoreCase(ControlType.TreeViewControl.name())) return ControlType.TreeViewControl;
        if (componentType.equalsIgnoreCase(ControlType.DataGridHeader.name())) return ControlType.DataGridHeader;
        if (componentType.equalsIgnoreCase(ControlType.Link.name())) return ControlType.Link;
        if (componentType.equalsIgnoreCase(ControlType.KeyValueOutputText.name()))
            return ControlType.KeyValueOutputText;
        if (componentType.equalsIgnoreCase(ControlType.FileUploadControl.name())) return ControlType.FileUploadControl;
        if (componentType.equalsIgnoreCase(ControlType.PrimaryLink.name())) return ControlType.PrimaryLink;
        if (componentType.equalsIgnoreCase(ControlType.ProductSelectionControl.name()))
            return ControlType.ProductSelectionControl;
        if (componentType.equalsIgnoreCase(ControlType.RadioPickList.name())) return ControlType.RadioPickList;
        if (componentType.equalsIgnoreCase(ControlType.AffiliatedAccountLink.name()))
            return ControlType.AffiliatedAccountLink;
        if (componentType.equalsIgnoreCase(ControlType.GraphicImageIcon.name())) return ControlType.GraphicImageIcon;
        if (componentType.equalsIgnoreCase(ControlType.CampaignSelectionControl.name()))
            return ControlType.CampaignSelectionControl;
        if (componentType.equalsIgnoreCase(ControlType.HtmlTable.name())) return ControlType.HtmlTable;
        if (componentType.equalsIgnoreCase(ControlType.StatusControl.name())) return ControlType.StatusControl;
        if (componentType.equalsIgnoreCase(ControlType.CustomControl.name())) return ControlType.CustomControl;
        if (componentType.equalsIgnoreCase(ControlType.MaskedOutputText.name())) return ControlType.MaskedOutputText;
        if (componentType.equalsIgnoreCase(ControlType.MaskedTextBox.name())) return ControlType.MaskedTextBox;
        if (componentType.equalsIgnoreCase(ControlType.MaskedComboBox.name())) return ControlType.MaskedComboBox;
        if (componentType.equalsIgnoreCase(ControlType.MaskedProductSelectionControl.name()))
            return ControlType.MaskedProductSelectionControl;
        if (componentType.equalsIgnoreCase(ControlType.MaskedPrimaryLink.name())) return ControlType.MaskedPrimaryLink;
        if (componentType.equalsIgnoreCase(ControlType.RDVProductSelectionControl.name()))
            return ControlType.RDVProductSelectionControl;
        if (componentType.equalsIgnoreCase(ControlType.HBLRDVProductSelectionControl.name()))
            return ControlType.HBLRDVProductSelectionControl;
        if (componentType.equalsIgnoreCase(ControlType.MaskedRDVProductSelectionControl.name()))
            return ControlType.MaskedRDVProductSelectionControl;
        if (componentType.equalsIgnoreCase(ControlType.RDVListBoxControl.name())) return ControlType.RDVListBoxControl;
        if (componentType.equalsIgnoreCase(ControlType.CascadedControl.name())) return ControlType.CascadedControl;
        if (componentType.equalsIgnoreCase(ControlType.GraphXAxis.name())) return ControlType.GraphXAxis;
        if (componentType.equalsIgnoreCase(ControlType.FlaggingControl.name())) return ControlType.FlaggingControl;
        if (componentType.equalsIgnoreCase(ControlType.Notes.name())) return ControlType.Notes;
        if (componentType.equalsIgnoreCase(ControlType.NotesDetail.name())) return ControlType.NotesDetail;
        if (componentType.equalsIgnoreCase(ControlType.Flag.name())) return ControlType.Flag;
        if (componentType.equalsIgnoreCase(ControlType.MarkLink.name())) return ControlType.MarkLink;
        if (componentType.equalsIgnoreCase(ControlType.DetailGraphSum.name())) return ControlType.DetailGraphSum;
        if (componentType.equalsIgnoreCase(ControlType.PageSize.name())) return ControlType.PageSize;
        if (componentType.equalsIgnoreCase(ControlType.SortColumn.name())) return ControlType.SortColumn;
        if (componentType.equalsIgnoreCase(ControlType.SortType.name())) return ControlType.SortType;
        if (componentType.equalsIgnoreCase(ControlType.Timer.name())) return ControlType.Timer;
        if (componentType.equalsIgnoreCase(ControlType.HtmlEditor.name())) return ControlType.HtmlEditor;
        //	Adding PDFExportControl--AnamSiddiqi
        if (componentType.equalsIgnoreCase(ControlType.PDFExportControl.name())) return ControlType.PDFExportControl;
        // Adding ProductSelectionEncryptControl-Anam Siddiqui        
        if (componentType.equalsIgnoreCase(ControlType.ProductSelectionEncryptControl.name()))
            return ControlType.ProductSelectionEncryptControl;
        if (componentType.equalsIgnoreCase(ControlType.RichTextArea.name())) return ControlType.RichTextArea;
        throw new CoreException(String.format("ControlType Not Found for '%s'", componentType));
    }
}
