package com.avanza.ui;

import java.util.List;
import java.util.Map;

import com.avanza.ui.meta.MetaViewAttribute;

public interface UIControl {

    public Map<ControlType, Object> render(MetaViewAttribute attrib, Object value);

    public Map<ControlType, Object> render(List<MetaViewAttribute> attrib, Object value);
}
