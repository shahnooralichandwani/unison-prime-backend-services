package com.avanza.ui;

import java.util.List;
import java.util.Map;

import com.avanza.core.CoreException;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.EvaluationException;
import com.avanza.core.function.expression.Expression;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;
import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.DataObjectCollection;
import com.avanza.core.util.Logger;
import com.avanza.ui.ControlType;
import com.avanza.ui.meta.MetaViewAttribute;

public class FormatterFactory {

    private static final Logger logger = Logger.getLogger(FormatterFactory.class);

    public static DataObjectCollection ApplyFormatters(List<MetaViewAttribute> viewattribs, DataObjectCollection val) {

        logger.logInfo(String.format("Applying Formatter"));
        for (MetaViewAttribute viewAttrib : viewattribs) {
            try {
                MetaEntity entity = MetaDataRegistry.getMetaEntity(viewAttrib.getMetaEntityId());
                String attribId = viewAttrib.getMetaAttributeId();
                MetaAttribute attribute = entity.getAttributeList().get(attribId);
                String formatExp = viewAttrib.getFormatExpression();

                for (DataObject obj : val) {
                    if (formatExp != null && formatExp.length() > 0) {
                        Object value = obj.getValue(attribute.getSystemName());
                        ExpressionParser expParser = new ExpressionParser(formatExp, Object.class,
                                FunctionCatalogManager.getInstance().getFunctionCatalog(
                                        ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction));
                        Expression retVal = expParser.parse();
                        ExpressionEvaluator eval = new ExpressionEvaluator(obj, value);
                        Object newVal = eval.evaluate(retVal);
                        obj.setValue(attribute.getSystemName(), newVal);
                    } else {
                        // if (attribute.getType() == AttributeType.Integer ||
                        // attribute.getType() == AttributeType.Long) {
                        // Object value =
                        // obj.getValue(attribute.getSystemName());
                        // DecimalFormat format = new
                        // DecimalFormat("###,###,###");
                        //
                        // }
                        // if (attribute.getType() == AttributeType.Float ||
                        // attribute.getType() == AttributeType.Double) {
                        //
                        // }
                    }
                }

            } catch (EvaluationException ex) {
                throw new CoreException(ex, String.format(
                        "Exception in formatting [ViewAtribute-%1$s] of [MetaView-%2$s] [Expression-%3$s]", viewAttrib
                                .getId(), viewAttrib.getViewId(), ex.getMessage()));
            } catch (Exception e) {
                throw new CoreException(e, String.format(
                        "Exception in formatting [ViewAtribute-%1$s] of [MetaView-%2$s]", viewAttrib.getId(),
                        viewAttrib.getViewId()));
            }
        }
        return val;
    }

    public static List<Map<String, Object>> ApplyFormatters(List<MetaViewAttribute> viewattribs,
                                                            List<Map<String, Object>> valueList) {

        logger.logInfo(String.format("Applying Formatter"));
        for (MetaViewAttribute viewAttrib : viewattribs) {

            try {
                MetaEntity entity = MetaDataRegistry.getMetaEntity(viewAttrib.getMetaEntityId());
                String attribId = viewAttrib.getMetaAttributeId();
                MetaAttribute attribute = entity.getAttributeList().get(attribId);
                String formatExp = viewAttrib.getFormatExpression();

                for (Map obj : valueList) {
                    if (formatExp != null && formatExp.length() > 0) {
                        Object value = obj.get(attribute.getSystemName());
                        if (value instanceof String && ((String) value).length() <= 0)
                            continue;
                        ExpressionParser expParser = new ExpressionParser(formatExp, Object.class,
                                FunctionCatalogManager.getInstance().getFunctionCatalog(
                                        ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction));
                        Expression retVal = expParser.parse();
                        ExpressionEvaluator eval = new ExpressionEvaluator(obj, value);
                        Object newVal = eval.evaluate(retVal);
                        if (
                                ControlType.fromString(viewAttrib.getComponentType()) == ControlType.GraphicImageIcon ||
                                        ControlType.fromString(viewAttrib.getComponentType()) == ControlType.CheckBox) {
                            obj.put(attribute.getSystemName() + "-formattedValue", newVal);
                        } else {
                            obj.put(attribute.getSystemName(), newVal);
                        }
                    } else {
                        // if (attribute.getType() == AttributeType.Integer ||
                        // attribute.getType() == AttributeType.Long) {
                        // Object value = obj.get(attribute.getSystemName());
                        // DecimalFormat format = new
                        // DecimalFormat("###,###,###");
                        // Object newVal = format.format(value);
                        // obj.put(attribute.getSystemName(), newVal);
                        // }
                        // if (attribute.getType() == AttributeType.Float ||
                        // attribute.getType() == AttributeType.Double) {
                        // Object value = obj.get(attribute.getSystemName());
                        // DecimalFormat format = new
                        // DecimalFormat("#.###,###");
                        // Object newVal = format.format(value);
                        // obj.put(attribute.getSystemName(), newVal);
                        // }
                    }
                }
            } catch (EvaluationException ex) {
                throw new CoreException(ex, String.format(
                        "Exception in formatting [ViewAtribute-%1$s] of [MetaView-%2$s] [Expression-%3$s]", viewAttrib
                                .getId(), viewAttrib.getViewId(), ex.getMessage()));
            } catch (Exception e) {
                throw new CoreException(e, String.format(
                        "Exception in formatting [ViewAtribute-%1$s] of [MetaView-%2$s]", viewAttrib.getId(),
                        viewAttrib.getViewId()));
            }
        }
        return valueList;
    }

    public static DataObject ApplyFormatters(List<MetaViewAttribute> orderedAttributes, DataObject dataObj) {
        logger.logInfo(String.format("Applying Formatter"));
        for (MetaViewAttribute viewAttrib : orderedAttributes) {
            try {
                MetaEntity entity = MetaDataRegistry.getMetaEntity(viewAttrib.getMetaEntityId());
                String attribId = viewAttrib.getMetaAttributeId();
                MetaAttribute attribute = entity.getAttributeList().get(attribId);
                String formatExp = viewAttrib.getFormatExpression();
                if (formatExp != null && formatExp.length() > 0) {
                    Object value = dataObj.getValue(attribute.getSystemName());
                    ExpressionParser expParser = new ExpressionParser(formatExp, Object.class, FunctionCatalogManager
                            .getInstance().getFunctionCatalog(ExpressionClass.ExpressionFunction,
                                    ExpressionClass.ContextFunction));
                    Expression retVal = expParser.parse();
                    ExpressionEvaluator eval = new ExpressionEvaluator(dataObj, value);
                    Object newVal = eval.evaluate(retVal);
                    dataObj.setValue(attribute.getSystemName(), newVal);

                }
            } catch (EvaluationException ex) {
                throw new CoreException(ex, String.format(
                        "Exception in formatting [ViewAtribute-%1$s] of [MetaView-%2$s] [Expression-%3$s]", viewAttrib
                                .getId(), viewAttrib.getViewId(), ex.getMessage()));
            } catch (Exception e) {
                throw new CoreException(e, String.format(
                        "Exception in formatting [ViewAtribute-%1$s] of [MetaView-%2$s]", viewAttrib.getId(),
                        viewAttrib.getViewId()));
            }
        }
        return dataObj;
    }

    public static Map ApplyFormatters(List<MetaViewAttribute> orderedAttributes, Map dataObj) {
        logger.logInfo(String.format("Applying Formatter"));
        for (MetaViewAttribute viewAttrib : orderedAttributes) {
            try {
                MetaEntity entity = MetaDataRegistry.getMetaEntity(viewAttrib.getMetaEntityId());
                String attribId = viewAttrib.getMetaAttributeId();
                System.out.println(viewAttrib.getDisplayNamePimary());
                MetaAttribute attribute = entity.getAttributeList().get(attribId);
                String formatExp = viewAttrib.getFormatExpression();
                if (formatExp != null && formatExp.length() > 0) {
                    Object value = dataObj.get(attribute.getSystemName());
                    ExpressionParser expParser = new ExpressionParser(formatExp, Object.class, FunctionCatalogManager
                            .getInstance().getFunctionCatalog(ExpressionClass.ExpressionFunction,
                                    ExpressionClass.ContextFunction));
                    Expression retVal = expParser.parse();
                    ExpressionEvaluator eval = new ExpressionEvaluator(dataObj, value);
                    Object newVal = eval.evaluate(retVal);
                    dataObj.put(attribute.getSystemName(), newVal);

                } else {
                    // if (attribute.getType() == AttributeType.Integer ||
                    // attribute.getType() == AttributeType.Long) {
                    // Object value = dataObj.get(attribute.getSystemName());
                    // DecimalFormat format = new DecimalFormat("###,###,###");
                    // Object newVal = format.format(value);
                    // dataObj.put(attribute.getSystemName(), newVal);
                    // }
                    // if (attribute.getType() == AttributeType.Float ||
                    // attribute.getType() == AttributeType.Double) {
                    //
                    // }
                }
            } catch (EvaluationException ex) {
                throw new CoreException(ex, String.format(
                        "Exception in formatting [ViewAtribute-%1$s] of [MetaView-%2$s] [Expression-%3$s]", viewAttrib
                                .getId(), viewAttrib.getViewId(), ex.getMessage()));
            } catch (Exception e) {
                throw new CoreException(e, String.format(
                        "Exception in formatting [ViewAtribute-%1$s] of [MetaView-%2$s]", viewAttrib.getId(),
                        viewAttrib.getViewId()));
            }
        }
        return dataObj;
    }
}
