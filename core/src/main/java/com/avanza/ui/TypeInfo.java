package com.avanza.ui;

public class TypeInfo {

    private String key;

    private Object classInstance;

    private String className;


    public TypeInfo(String key, String classname, Object objinstance) {
        this.key = key;
        this.className = classname;
        this.classInstance = objinstance;
    }

    public String getClassName() {
        return className;
    }


    public void setClassName(String className) {
        this.className = className;
    }


    public String getKey() {
        return key;
    }


    public void setKey(String key) {
        this.key = key;
    }


    public Object getClassInstance() {
        return classInstance;
    }


    public void setClassInstance(Object classInstance) {
        this.classInstance = classInstance;
    }

}
