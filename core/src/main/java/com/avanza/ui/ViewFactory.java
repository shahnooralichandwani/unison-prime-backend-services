package com.avanza.ui;

import com.avanza.core.util.StringHelper;

public class ViewFactory extends ComponentFactory<UIView, ViewType> {
    static {
        XML_RENDERER = "ViewRenderer";
    }

    public UIView getControl(ViewType type) {
        UIView retview = getControl(type.getValue());
        if (retview == null) {
            retview = this.getDefView(type);
        }
        return retview;
    }

    public UIView getDefView(ViewType type) {

        return null;
    }

    @Override
    public UIView getControl(ViewType type, String className) {
        try {
            if (StringHelper.isNotEmpty(className)) {
                Object instance = Class.forName(className).newInstance();
                return (UIView) instance;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getControl(type);
    }

}
