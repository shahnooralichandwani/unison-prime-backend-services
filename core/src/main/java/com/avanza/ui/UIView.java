package com.avanza.ui;

import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.DataObjectCollection;
import com.avanza.ui.meta.MetaView;

public interface UIView {

    public Object render(MetaView view, DataObject object, boolean addViewMenuData);

    public Object render(MetaView view, DataObjectCollection objCollection, boolean addViewMenuData);

    public Object render(MetaView view);
}
