package com.avanza.ui.util;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;

/**
 * This class is used to fetch the ResourceBundle property value of the jsf property file.
 *
 * @author kraza
 */

public class ResourceUtil {

    private static ResourceUtil _instance;
    private Map<String, ResourceBundle> resourceBundles;

    public static String property;
    public static String resource;

    protected ResourceUtil() {
        resourceBundles = new HashMap<String, ResourceBundle>();
    }

    public static void load(ConfigSection section) {
        property = section.getTextValue("property");
        resource = section.getTextValue("resource");
    }

    public static ResourceUtil getInstance() {
        if (_instance == null) {
            _instance = new ResourceUtil();
        }
        return _instance;
    }

    public String getProperty(String propertyName, Object... params) {

        return this.getProperty(property, propertyName, params);
    }

    public String getPathProperty(String propertyName, Object... params) {

        return this.getProperty(resource, propertyName, params);
    }

    public String getProperty(String bundleName, String propertyName, Object... params) {

        String retValue = "";
        Locale locale = null;

        WebContext ctx = ApplicationContext.getContext().get(WebContext.class);

        if (ctx != null)
            if (ctx.getAttribute(SessionKeyLookup.CurrentLocale) != null)
                locale = ((LocaleInfo) ctx.getAttribute(SessionKeyLookup.CurrentLocale)).getLocale();
        if (locale == null)
            locale = Locale.ENGLISH;

        try {

            retValue = this.getResourceBundle(bundleName, locale).getString(propertyName);

        } catch (MissingResourceException e) {

            retValue = "key " + propertyName + " not found";
        }

        if (params != null) {

            MessageFormat mf = new MessageFormat(retValue, locale);
            retValue = mf.format(params, new StringBuffer(), null).toString();
        }

        return retValue;
    }

    public ResourceBundle getResourceBundle(String bundleName) {

        LocaleInfo localeInfo = ApplicationContext.getContext().get(WebContext.class).getAttribute(SessionKeyLookup.CurrentLocale);

        ResourceBundle bundle = getResourceBundle(bundleName, localeInfo.getLocale());

        return bundle;
    }

    private ResourceBundle getResourceBundle(String bundleName, Locale locale) {
        ResourceBundle bundle = this.resourceBundles.get(bundleName + locale.toString());

        if (bundle == null) {
            bundle = ResourceBundle.getBundle(bundleName, locale);
            this.resourceBundles.put(bundleName, bundle);
        }
        return bundle;
    }
}
