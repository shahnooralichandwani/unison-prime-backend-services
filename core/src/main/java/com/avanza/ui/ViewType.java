package com.avanza.ui;

import com.avanza.core.CoreException;

/**
 * ViewType Enum
 */
public enum ViewType {

    Section("Section"), DetailView("Detail"), TabPage("TabPage"), ListView("List"), InvertedTabPage("InvertedTabPage"), AjaxPanel("AjaxPanel"),
    GraphView("GraphView"), TaskView("Task"), AdvanceSection("AdvanceSection"), UnknownView("un"), IframeContainer("IframeContainer"),
    Iframe("Iframe"), TabPanelView("TabPanelView"), TabView("TabView"), EditableListView("EditableList");

    /*
     * Iframe can be contained both in a detail view and Iframe Container. When contained in detail view, it will be rendered as a section or list
     * view where as when rendered in iframe container, it will have different effects such as drag drop, multiple views on single row...etc.
     */


    private ViewType(String value) {

        this.value = value;
    }

    private String value;

    public String getValue() {
        return value;
    }

    public static ViewType fromString(String type) {
        if (type != null) {
            if (type.equalsIgnoreCase(ViewType.DetailView.getValue())) return ViewType.DetailView;
            if (type.equalsIgnoreCase(ViewType.ListView.getValue())) return ViewType.ListView;
            if (type.equalsIgnoreCase(ViewType.Section.getValue())) return ViewType.Section;
            if (type.equalsIgnoreCase(ViewType.TabPage.getValue())) return ViewType.TabPage;
            if (type.equalsIgnoreCase(ViewType.InvertedTabPage.getValue())) return ViewType.InvertedTabPage;
            if (type.equalsIgnoreCase(ViewType.AjaxPanel.getValue())) return ViewType.AjaxPanel;
            if (type.equalsIgnoreCase(ViewType.GraphView.getValue())) return ViewType.GraphView;
            if (type.equalsIgnoreCase(ViewType.AdvanceSection.getValue())) return ViewType.AdvanceSection;
            if (type.equalsIgnoreCase(ViewType.IframeContainer.getValue())) return ViewType.IframeContainer;
            if (type.equalsIgnoreCase(ViewType.Iframe.getValue())) return ViewType.Iframe;
            if (type.equalsIgnoreCase(ViewType.TaskView.getValue())) return ViewType.TaskView;
            if (type.equalsIgnoreCase(ViewType.TabPanelView.getValue())) return ViewType.TabPanelView;
            if (type.equalsIgnoreCase(ViewType.TabView.getValue())) return ViewType.TabView;
        }
        throw new CoreException(String.format("ViewType Not Found for ", type));
    }
}
