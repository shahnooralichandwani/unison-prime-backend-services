package com.avanza.ui;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.avanza.core.meta.AttributeScope;
import com.avanza.core.meta.AttributeType;
import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaAttributeImpl;
import com.avanza.core.meta.MetaCounter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.ui.meta.MetaView;
import com.avanza.ui.meta.MetaViewAttribute;
import com.avanza.ui.meta.MetaViewAttributeParam;
import com.avanza.ui.meta.MetaViewParam;
import com.avanza.ui.meta.ViewCatalog;

/**
 * Default View Generator
 */
public class ViewGenerator {

    public static MetaView generateView(String metaEntId, ViewType type) {
        MetaEntity entity = MetaDataRegistry.getMetaEntity(metaEntId);
        MetaView view = new MetaView();
        view.setDefaultview(true);
        view.setDisplayOrder(0);
        view.setCreation("System");
        view.setSystemName("defaultViewfor" + metaEntId);
        view.setDocumentId(metaEntId);
        view.setId((String) MetaDataRegistry.getNextCounterValue(MetaCounter.VIEW_ID_COUNTER));
        view.setMain(true);
        if (entity == null) return view;
        view.setPrimaryName(entity.getPrimaryName());
        view.setSecondaryName(entity.getSecondaryName());
        view.setParams(new HashSet<MetaViewParam>(0));
        view.setParent(null);
        view.setReadOnly(false);
        view.setStayOnSave(false);
        view.setType(type.getValue());
        if (type == ViewType.DetailView) {
            view.setAttributes(new HashSet<MetaViewAttribute>(0));
            view.setSubviews(createSectionView(entity, view));
        }
        if (type == ViewType.ListView) {
            view.setAttributes(createViewAttributes(entity, view.getId()));
            view.setSubviews(new HashSet<MetaView>(0));
        }
        ViewCatalog.addView(view);
        return view;
    }

    private static Set<MetaViewAttribute> createViewAttributes(MetaEntity entity, String viewId) {
        Set<MetaViewAttribute> viewAttribs = new HashSet<MetaViewAttribute>();
        Map<String, MetaAttribute> attribMap = entity.getAttributeList(true);
        MetaViewAttribute attribute;
        int orderCount = 0;
        for (MetaAttribute attrib : attribMap.values()) {
            attribute = new MetaViewAttribute();
            attribute.setComponentType(getControlType(attrib.getType()));
            attribute.setCreation("System");
            attribute.setDisplayNamePimary(attrib.getDisplayName());
            attribute.setDisplayNameSecondary(attrib.getDisplayName());
            attribute.setDisplayOrder(orderCount++);
            attribute.setDocumentId(entity.getId());
            attribute.setId((String) MetaDataRegistry.getNextCounterValue(MetaCounter.VIEW_ID_COUNTER));
            attribute.setMetaAttributeId(attrib.getId());
            attribute.setReadOnly(false);
            attribute.setMetaAttribute((MetaAttributeImpl) attrib);
            attribute.setSystemName("viewAttribSystemNamefor" + attrib.getSystemName());
            attribute.setViewAttribParams(new HashSet<MetaViewAttributeParam>(0));
            attribute.setViewId(viewId);
            if (!attrib.getScope().toString().equals(AttributeScope.Private.toString())) viewAttribs.add(attribute);
        }
        return viewAttribs;
    }

    private static String getControlType(AttributeType type) {

        if (type == AttributeType.String) return ControlType.TextBox.name();
        if (type == AttributeType.Boolean) return ControlType.RadioButton.name();
        if (type == AttributeType.Double) return ControlType.TextBox.name();
        if (type == AttributeType.Float) return ControlType.TextBox.name();
        if (type == AttributeType.Integer) return ControlType.TextBox.name();
        if (type == AttributeType.Long) return ControlType.TextBox.name();
        if (type == AttributeType.LongString) return ControlType.TextBox.name(); // TextArea
        if (type == AttributeType.Date) return ControlType.CalenderControl.name();
        if (type == AttributeType.DateTime) return ControlType.CalenderControl.name();
        if (type == AttributeType.PickList) return ControlType.ComboBox.name();
        if (type == AttributeType.Sequence) return ControlType.TextBox.name();

        return null;
    }

    private static Set<MetaView> createSectionView(MetaEntity entity, MetaView parentView) {
        Set<MetaView> views = new HashSet<MetaView>();
        String sectionViewId = (String) MetaDataRegistry.getNextCounterValue(MetaCounter.VIEW_ID_COUNTER);
        MetaView secionview = new MetaView();
        secionview.setAttributes(createViewAttributes(entity, sectionViewId));
        secionview.setDefaultview(true);
        secionview.setDimension("0x1");
        secionview.setDisplayOrder(0);
        secionview.setCreation("System");
        secionview.setDocumentId(entity.getId());
        secionview.setId(sectionViewId);
        secionview.setMain(true);
        secionview.setPrimaryName(entity.getPrimaryName());
        secionview.setSecondaryName(entity.getSecondaryName());
        secionview.setParams(new HashSet<MetaViewParam>(0));
        secionview.setParent(parentView);
        secionview.setReadOnly(false);
        secionview.setStayOnSave(false);
        secionview.setSubviews(new HashSet<MetaView>(0));
        secionview.setType(ViewType.Section.getValue());
        ViewCatalog.addView(secionview);
        views.add(secionview);
        return views;
    }
}
