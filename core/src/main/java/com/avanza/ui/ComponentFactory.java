package com.avanza.ui;

import java.util.Hashtable;
import java.util.List;

import com.avanza.core.CoreException;
import com.avanza.core.util.configuration.ConfigSection;

public abstract class ComponentFactory<T, E> {

    private Hashtable<String, TypeInfo> typeList = new Hashtable<String, TypeInfo>();
    protected static String XML_RENDERER = "Renderer";
    private static final String XML_KEY = "key";
    private static final String XML_CLASS = "class";

    /**
     * ComponentFactory Configuration Loading *
     */
    public void load(ConfigSection section) {

        /*List<ConfigSection> childList = section.getChildSections(ComponentFactory.XML_RENDERER);
        TypeInfo info = null;
        for (ConfigSection renderersection : childList) {
            String key = renderersection.getTextValue(ComponentFactory.XML_KEY);
            String impClass = renderersection.getTextValue(ComponentFactory.XML_CLASS);
            // boolean isSingleton = broker.getValue(DataBrokerCatalog.XML_SINGLETON, true);
            try {
                Object instance = Class.forName(impClass).newInstance();
                info = new TypeInfo(key, impClass, instance);
                typeList.put(key, info);
            } catch (Exception e) {
                throw new CoreException(e, "ComponentFactory Load Failed");
            }

        }*/

    }

    /**
     * Clear TypeList *
     */
    public void clear() {
        typeList.clear();
    }

    /**
     * Returns Component by name *
     */
    @SuppressWarnings("unchecked")
    protected T getControl(String name) {
        TypeInfo info = typeList.get(name);
        if (info != null) return (T) info.getClassInstance();
        return null;
    }

    public abstract T getControl(E type);

    public abstract T getControl(E type, String className);
}
