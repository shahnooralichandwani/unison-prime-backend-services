package com.avanza.ui;

import com.avanza.core.CoreException;

public class ControlFactory extends ComponentFactory<UIControl, ControlType> {
    static {
        XML_RENDERER = "ControlRenderer";
    }

    /**
     * Returns UIControl by ControlType
     */
    public UIControl getControl(ControlType type) {

        UIControl retVal = this.getControl(type.toString());
        if (retVal == null)
            retVal = this.getDefControl(type);
        return retVal;
    }

    /**
     * Returns DefaultControl for ControlType
     */
    public UIControl getDefControl(ControlType type) {

        String defaultControlClass = null;
        if (type == ControlType.TextBox)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.TextBoxControl";
        if (type == ControlType.PageSize ||
                type == ControlType.SortColumn ||
                type == ControlType.SortType)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.GridFeatureControl";
        if (type == ControlType.Label)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.LabelControl";
        if (type == ControlType.OutputText)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.OutputTextControl";
        if (type == ControlType.ComboBox)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.ComboBoxControl";
        if (type == ControlType.RadioButton)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.RadioButtonControl";
        if (type == ControlType.DataGrid)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.GridControl";
        if (type == ControlType.CheckBox)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.CheckBoxControl";
        if (type == ControlType.MultiCheckBox)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.MiltiCheckBoxControl";
        if (type == ControlType.TextArea)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.TextAreaControl";
        if (type == ControlType.CalenderControl)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.CalenderControl";
        if (type == ControlType.ListBox)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.ListBoxControl";
        if (type == ControlType.TreeViewControl)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.TreeViewControl";
        if (type == ControlType.Link)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.LinkControl";
        if (type == ControlType.KeyValueOutputText)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.KeyValueOutputTextControl";
        if (type == ControlType.FileUploadControl)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.FileUploadControl";
        if (type == ControlType.ProductSelectionControl)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.ProductSelectionControl";
        if (type == ControlType.GraphicImageIcon)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.GraphicImageIconControl";
        if (type == ControlType.CampaignSelectionControl)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.CampaignSelectionControl";
        if (type == ControlType.RadioPickList)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.RadioPickListControl";
        if (type == ControlType.HtmlTable)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.HtmlTableControl";
        if (type == ControlType.StatusControl)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.StatusControl";
        if (type == ControlType.MaskedOutputText)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.MaskedOutputTextControl";
        if (type == ControlType.MaskedTextBox)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.MaskedTextBoxControl";
        if (type == ControlType.MaskedComboBox)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.MaskedComboBoxControl";
        if (type == ControlType.MaskedProductSelectionControl)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.MaskedProductSelectionControl";
        if (type == ControlType.Timer)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.TimerControl";
        if (type == ControlType.HtmlEditor)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.HTMLEditor";
        // Adding for RDV Product Selection Control
        if (type == ControlType.RDVProductSelectionControl)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.RDVProductSelectionControl";
        //Adding PDFExportControl--AnamSiddiqi
        if (type == ControlType.PDFExportControl)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.PDFExportControl";
        //Adding ProductSelectionEncryptControl
        if (type == ControlType.ProductSelectionEncryptControl)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.ProductSelectionEncryptControl";
        if (type == ControlType.RichTextArea)
            defaultControlClass = "com.avanza.unison.ui.jsf.control.RichTextAreaControl";

        try {
            return (UIControl) Class.forName(defaultControlClass).newInstance();
        } catch (Exception e) {
            throw new CoreException(e, String.format("Cannot Create Instance of Default Control Type %1$s", type.name()));
        }
    }

    @Override
    public UIControl getControl(ControlType type, String className) {
        String defaultControlClass = null;
        if (type == ControlType.CustomControl)
            defaultControlClass = className;
        try {
            return (UIControl) Class.forName(defaultControlClass).newInstance();
        } catch (Exception e) {
            throw new CoreException(e, String.format("Cannot Create Instance of Default Control Type %1$s", type.name()));
        }
    }
}
