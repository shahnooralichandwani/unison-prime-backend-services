package com.avanza.ui;

import java.util.List;

import com.avanza.core.util.configuration.ConfigSection;

public class ViewManager {

    private static ControlFactory controlFactory = new ControlFactory();

    private static ViewFactory viewFactory = new ViewFactory();

    private static final String XML_VIEW_TYPE_RENDERER = "ViewType-Renderer";

    private static final String XML_CONTROL_TYPE_RENDERER = "ControlType-Renderer";

    private static final String XML_VIEW_RENDERER = "View-Renderer";

    private static boolean isLoaded;

    public static ControlFactory getControlFactory() {
        return controlFactory;
    }

    public static void setControlFactory(ControlFactory controlFactory) {
        ViewManager.controlFactory = controlFactory;
    }

    public static ViewFactory getViewFactory() {
        return viewFactory;
    }

    public static void setViewFactory(ViewFactory viewFactory) {
        ViewManager.viewFactory = viewFactory;
    }

    private ViewManager() {

    }

    public static void load(ConfigSection section) {

        if (isLoaded)
            return;

        viewFactory.load(section);
        controlFactory.load(section);

        isLoaded = true;
    }

    public static void dispose() {

        viewFactory.clear();
        controlFactory.clear();
        isLoaded = false;
    }

    public static UIView getView(String viewtype) {
        return viewFactory.getControl(ViewType.fromString(viewtype));
    }

    public static UIView getView(String viewtype, String factory) {
        return viewFactory.getControl(ViewType.fromString(viewtype), factory);
    }
}
