package com.avanza.notificationservice.channel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.notificationservice.adapter.ContentFormatter;
import com.avanza.notificationservice.adapter.ProtocolAdapter;

public class ChannelCatalog {

    private static Logger logger = Logger.getLogger(ChannelCatalog.class);
    private static ChannelCatalog instance;
    private Map<String, AdapterInfo> adapterList;
    private Map<String, ChannelInfo> channelList;
    private Map<String, FormatterInfo> formatterList;
    private DataBroker broker;

    public Map<String, AdapterInfo> getAdapterInfos() {
        return adapterList;
    }

    public Map<String, ChannelInfo> getchannelList() {
        return channelList;
    }

    public Map<String, FormatterInfo> getformatterList() {
        return formatterList;
    }

    public static synchronized ChannelCatalog getInstance() {
        if (instance == null) {
            instance = new ChannelCatalog();
            instance.load();
        }
        return instance;
    }

    public void initialize(ConfigSection section) {
    }

    public void dispose() {
    }

    private void load() {
        try {
            broker = DataRepository.getBroker(ChannelCatalog.class.getName());
            loadAdapterList(broker);
            loadChannelList(broker);
            loadFormatterList(broker);
        } catch (Exception e) {
            logger.LogException("Exception while Loading the channel Catalog for Notification.", e);
        }
    }

    private void loadAdapterList(DataBroker databroker) {
        adapterList = new HashMap<String, AdapterInfo>();
        List<AdapterInfo> adpInfos = databroker.findAll(AdapterInfo.class);
        for (AdapterInfo adapterInfo : adpInfos) {
            adapterInfo.initialize();
            adapterList.put(adapterInfo.getAdapterId(), adapterInfo);
        }
    }

    private void loadChannelList(DataBroker databroker) {
        channelList = new HashMap<String, ChannelInfo>();
        List<ChannelInfo> chlInfos = broker.findAll(ChannelInfo.class);
        for (ChannelInfo chlInfo : chlInfos) {
            channelList.put(chlInfo.getAdapterChannelId(), chlInfo);
        }
    }

    private void loadFormatterList(DataBroker databroker) {
        formatterList = new HashMap<String, FormatterInfo>();
        List<FormatterInfo> frmtInfos = broker.findAll(FormatterInfo.class);
        for (FormatterInfo frmtInfo : frmtInfos) {
            frmtInfo.initialize();
            formatterList.put(frmtInfo.getFormatterId(), frmtInfo);
        }
    }

    public ProtocolAdapter createAdapter(String adaptername, ConfigSection args) {
        AdapterInfo info = this.adapterList.get(adaptername.trim());
        if (info == null)
            return null;

        return info.createAdapter(args);
    }

    public ContentFormatter createFormatter(String formattername, ConfigSection args) {
        FormatterInfo info = this.formatterList.get(formattername);
        return info.createFormatter(args);
    }

    public ProtocolAdapter getAdapter(String channelName) {
        ChannelInfo channelInfo = this.channelList.get(channelName.trim());
        return channelInfo.getAdapter();
    }

    public ChannelInfo getChannel(String channelName) {
        return this.channelList.get(channelName.trim());
    }

    public ChannelInfo getChannelById(String channelId) {
        for (ChannelInfo chanel : channelList.values()) {
            if (chanel.getChannelId().equalsIgnoreCase(channelId))
                return chanel;
        }
        return null;
    }

    public ContentFormatter getFormatter(String channelName) {
        ChannelInfo channelInfo = this.channelList.get(channelName);
        return channelInfo.getFormatter();
    }

}