package com.avanza.notificationservice.channel;

import java.util.ArrayList;

import com.avanza.core.data.DataSourceException;
import com.avanza.core.data.DbObject;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.notificationservice.adapter.ProtocolAdapter;

/**
 * NsProtocolAdapter generated by hbm2java
 */

public class AdapterInfo extends DbObject {
    /**
     *
     */
    private static final long serialVersionUID = 6704611340020888531L;
    private static Logger logger = Logger.getLogger(AdapterInfo.class);
    private String adapterId;
    private String adapterName;
    private String impClass;
    protected ArrayList<String> paramsList;
    private Class<?> clazz;

    public String getAdapterId() {
        return this.adapterId;
    }

    public void setAdapterId(String adapterId) {
        this.adapterId = adapterId;
    }

    public String getAdapterName() {
        return this.adapterName;
    }

    public void setAdapterName(String adapterName) {
        this.adapterName = adapterName;
    }

    public String getImpClass() {
        return this.impClass;
    }

    public void setImpClass(String impClass) {
        this.impClass = impClass;
    }

    public String getAdapterParams() {
        return this.adapterName;
    }

    public void setAdapterParams(String adapterParams) {
        this.adapterName = adapterParams;
    }

    public void initialize() {
        try {
            this.clazz = Class.forName(getImpClass());
            checkIsProtocolAdapter(this.clazz);
        } catch (Exception e) {
            logger.LogException("Exception while Loading the Adapter Info for Notification", e);
        } catch (Error e) {
            logger.LogException("Error while Loading the Adapter Info for Notification", e);
        }
    }

    private void checkIsProtocolAdapter(Class<?> clazz) {
        boolean implementAdapter = false;
        Class<?>[] impClassList = clazz.getInterfaces();
        for (int idx = 0; idx < impClassList.length; idx++) {
            if (impClassList[idx] == ProtocolAdapter.class) {
                implementAdapter = true;
                break;
            }
        }
        if (!implementAdapter)
            throw new DataSourceException("Class [%1$s] does not implement [%2$s] interface", clazz.getName(), ProtocolAdapter.class);
    }

    public ProtocolAdapter createAdapter(ConfigSection protocolParams) {
        ProtocolAdapter retVal = null;
        try {
            retVal = (ProtocolAdapter) this.clazz.newInstance();
            retVal.initialize(protocolParams);
        } catch (Exception e) {
        }
        return retVal;
    }

}
