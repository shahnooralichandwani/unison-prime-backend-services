package com.avanza.notificationservice.channel;


public interface ChannelUpListener {

    void notifyChannelUp(String adapterChannelId);
}
