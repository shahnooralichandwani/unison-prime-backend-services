package com.avanza.notificationservice.notification.templateBifercation;


import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.meta.MetaAttribute;
import com.avanza.unison.admin.template.NsTemplate;
import com.avanza.workflow.configuration.action.ActionBinding;

/**
 * NsTemplateBifercations entity. @author MyEclipse Persistence Tools
 */

public class NsTemplateBifercation extends DbObject implements java.io.Serializable {

    // Fields

    private String id;


    private NsTemplate nsTemplate;
    private String templateId;


    private String bifercationType;
    private String documentType;
    private String onEvent;
    private String targetAudience;
    private String processCode;
    private String executionStrategyId;

    private Map<String, NsBifercationTargetEmails> bifercationTargetEmails = new Hashtable<String, NsBifercationTargetEmails>(0);

    // Constructors

    /**
     * default constructor
     */
    public NsTemplateBifercation() {
    }

    public NsTemplateBifercation(String bifercationType,
                                 String documentType, String onEvent, String targetAudience) {
        this.bifercationType = bifercationType;
        this.documentType = documentType;
        this.setOnEvent(onEvent);
        this.targetAudience = targetAudience;
    }

    // Property accessors

    public String getBifercationType() {
        return this.bifercationType;
    }

    public void setBifercationType(String bifercationType) {
        this.bifercationType = bifercationType;
    }

    public String getDocumentType() {
        return this.documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }


    public String getTargetAudience() {
        return this.targetAudience;
    }

    public void setTargetAudience(String targetAudience) {
        this.targetAudience = targetAudience;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public NsTemplate getNsTemplate() {
        return this.nsTemplate;
    }

    public void setNsTemplate(NsTemplate nsTemplate) {
        this.nsTemplate = nsTemplate;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setOnEvent(String onEvent) {
        this.onEvent = onEvent;
    }

    public String getOnEvent() {
        return onEvent;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setExecutionStrategyId(String executionStrategyId) {
        this.executionStrategyId = executionStrategyId;
    }

    public String getExecutionStrategyId() {
        return executionStrategyId;
    }

    public void setBifercationTargetEmails(Map<String, NsBifercationTargetEmails> bifercationTargetEmails) {
        this.bifercationTargetEmails = bifercationTargetEmails;
    }

    private Map<String, NsBifercationTargetEmails> getBifercationTargetEmails() {
        return bifercationTargetEmails;
    }

    public Map<String, NsBifercationTargetEmails> getTargetEmails() {
        if (targetAudience.equals(TemplateAudience.ExternalUser.value.toString()) || targetAudience.equals(TemplateAudience.ExternalUserAndCA.value.toString()))
            return getBifercationTargetEmails();
        return null;
    }

}