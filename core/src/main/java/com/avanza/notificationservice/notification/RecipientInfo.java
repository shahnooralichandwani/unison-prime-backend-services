package com.avanza.notificationservice.notification;

public class RecipientInfo {

    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public RecipientInfo() {
    }

    public RecipientInfo(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return this.address;
    }

}
