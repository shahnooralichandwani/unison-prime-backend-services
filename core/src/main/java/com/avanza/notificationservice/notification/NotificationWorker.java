package com.avanza.notificationservice.notification;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.util.Logger;
import com.avanza.notificationservice.adapter.AdapterStatus;
import com.avanza.notificationservice.adapter.NotificationUpdate;
import com.avanza.notificationservice.adapter.ProtocolAdapter;
import com.avanza.notificationservice.adapter.ThreadQueue;
import com.avanza.notificationservice.channel.ChannelCatalog;
import com.avanza.notificationservice.channel.ChannelUpListener;

@Deprecated
public class NotificationWorker extends ThreadQueue<Notification> implements Runnable, Serializable {
    private static final long serialVersionUID = -1329832028865284696L;
    //private static Logger logger = Logger.getLogger(NotificationWorker.class);

    private ProtocolAdapter protocolAdapter;
    private DataBroker notificationDB;
    private String adapterChannelId;
    private ChannelUpListener channelUpListener;

    private static Map<String, NotificationWorker> workers = new HashMap<String, NotificationWorker>();

    private NotificationWorker(ProtocolAdapter protocol, String adapterChannelId) {
        this.protocolAdapter = protocol;
        this.adapterChannelId = adapterChannelId;
        notificationDB = DataRepository.getBroker(NotificationWorker.class.getName());
    }

    public static NotificationWorker getWorker(String adapterChannelId, NotificationUpdate callback, ChannelUpListener listener) {

        NotificationWorker worker = workers.get(adapterChannelId);
        if (worker == null) {
            worker = new NotificationWorker(ChannelCatalog.getInstance().getAdapter(adapterChannelId), adapterChannelId);
            worker.channelUpListener = listener;
            worker.protocolAdapter.setNotificationUpdate(callback);
            worker.intialize();
            workers.put(adapterChannelId, worker);
        }
        return worker;
    }

    @Override
    protected void processItem(Notification notification) {
        if (protocolAdapter.getStatus() == AdapterStatus.Running)
            protocolAdapter.notify(notification);
        else if (protocolAdapter.getStatus() == AdapterStatus.Down) {
            notification.setNotificationState(NotificationState.Wait);
            notificationDB.persist(notification);
            protocolAdapter.startChannelUpListener(this.channelUpListener, this.adapterChannelId);
        }
    }
}
