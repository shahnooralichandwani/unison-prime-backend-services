package com.avanza.notificationservice.notification;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.core.data.DbObject;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.StringMap;
import com.avanza.notificationservice.channel.ChannelCatalog;
import com.avanza.notificationservice.channel.ChannelInfo;
import com.avanza.ui.util.ResourceUtil;

public class Notification extends DbObject implements Comparable<Notification> {

    /**
     * Time Interval is specified in seconds.
     */
    private static final long serialVersionUID = -6243069812261204236L;
    protected static final String ATTACHMENT_BASE_KEY = "email_attachment_path";

    private String notificationId;
    private String nodeId;
    private int retryCount;
    protected ArrayList<RecipientInfo> recipientToList = new ArrayList<RecipientInfo>(0);
    protected ArrayList<RecipientInfo> recipientCcList = new ArrayList<RecipientInfo>(0);
    protected ArrayList<RecipientInfo> recipientBccList = new ArrayList<RecipientInfo>(0);
    private String subject;
    protected StringMap<String> protocolFields;
    protected NotificationState state;
    protected Map<String, Object> content;
    private String statusText;
    private String locale;
    private String nsFrom;
    private String channelId;
    private String body;
    protected ArrayList<String> documentList = new ArrayList<String>(0);
    private boolean isLogicalPath = true;
    private String sentTime;

    private ChannelInfo adapterChannel;
    private long timeOut;
    private long waitTime;// time priority in seconds.
    private PriorityType priority = PriorityType.Medium;
    private Object externalId;
    private Object externalId1; // used as a RIM in some cases in unison.
    private Object externalId2;
    private Object externalId3;

    /*
     * Shahbaz
     * Callback Handler added
     */
    private String callbackHandler;

    public String getCallbackHanlder() {
        return callbackHandler;
    }

    public void setCallbackHanlder(String callbackHandler) {
        this.callbackHandler = callbackHandler;
    }

    public Object getExternalId() {
        return externalId;
    }

    public void setExternalId(Object externalId) {
        this.externalId = externalId;
    }

    public PriorityType getPriority() {
        return priority;
    }

    public void setPriority(PriorityType priority) {
        this.priority = priority;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }

    public int compareTo(Notification notification) {
        if (this.waitTime < notification.waitTime)
            return -1;
        else if (this.waitTime > notification.waitTime)
            return 1;
        else
            return priority.getValue() > notification.getPriority().getValue() ? 1 : -1;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public NotificationState getNotificationState() {
        return state;
    }

    public void setNotificationState(NotificationState state) {
        this.state = state;
    }

    public String getNsFrom() {
        return nsFrom;
    }

    public void setNsFrom(String nsFrom) {
        this.nsFrom = nsFrom;
    }

    public String getContent() {
        return "";
    }

    public void setContent(String content) {
        // this.content = content;
    }

    public String getSentTime() {
        return sentTime;
    }

    public void setSentTime(String sentTime) {
        this.sentTime = sentTime;
    }

    public ArrayList<RecipientInfo> getRecipientTo() {
        return recipientToList;
    }

    public ArrayList<RecipientInfo> getRecipientCc() {
        return recipientCcList;
    }

    public ArrayList<RecipientInfo> getRecipientBcc() {
        return recipientBccList;
    }

    public String getState() {
        return state.toString();
    }

    public void setState(String strState) {
        this.state = NotificationState.valueOf(strState);
    }

    public String getDocumentList() {
        String retStr = "";
        if (documentList != null) {
            for (int i = 0; i < documentList.size(); i++)
                retStr += documentList.get(i);
        }
        return retStr;
    }

    public Map<String, String> getDocumentPaths() {

        Guard.checkNullOrEmpty(this.notificationId, "notificationId");
        Guard.checkNull(getCreatedOn(), "createdOn");

        String dateString = new SimpleDateFormat("yyyy-MM-dd").format(getCreatedOn());
        Map<String, String> realPaths = new HashMap<String, String>(this.documentList.size());
        ResourceUtil resourceUtil = ResourceUtil.getInstance();

        for (String logicalPath : this.documentList) {
            if (StringHelper.isNotEmpty(logicalPath)) {
                if (isLogicalPath)
                    realPaths.put(logicalPath, resourceUtil.getPathProperty(ATTACHMENT_BASE_KEY, File.separator, ApplicationLoader.BASE_APPLICATION_PATH
                            , logicalPath));
                else
                    realPaths.put(logicalPath, logicalPath);
            }
        }

        return realPaths;
    }

    public void setDocumentList(String documentStr) {
        documentList = new ArrayList<String>();
        String[] strArray = null;
        if (documentStr != null && documentStr != "")
            strArray = documentStr.split(",");
        else
            return;
        for (String s : strArray)
            documentList.add(s);
    }

    public void setDocumentList(String documentStr, boolean isLogicalPath) {

        this.setDocumentList(documentStr);
        this.isLogicalPath = isLogicalPath;
    }

    public String getProtocolFields() {
        return "";
    }

    public void setProtocolFields(String strProtoFields) {

    }

    private String getRecipientList(List<RecipientInfo> recipientList) {
        String retStr = "";
        if (recipientList != null && !recipientList.isEmpty()) {
            for (int i = 0; i < recipientList.size(); i++)
                retStr += recipientList.get(i) + ",";
        } else
            return "";
        return retStr.substring(0, retStr.length() - 1);
    }

    private ArrayList<RecipientInfo> prepareRecipientList(String strRecipientList) {
        String[] strArray = null;
        ArrayList<RecipientInfo> recipientList = new ArrayList<RecipientInfo>();


        if (strRecipientList != null && !"".equalsIgnoreCase(strRecipientList))
            strArray = strRecipientList.split(",");
        else
            return recipientList;
        Map<String, RecipientInfo> recipientMap = new HashMap<String, RecipientInfo>();
        for (String s : strArray) {
            RecipientInfo recipientInfo = new RecipientInfo();
            recipientInfo.setAddress(s);
            recipientMap.put(s, recipientInfo);
        }
        recipientList.addAll(recipientMap.values());
        return recipientList;
    }

    public void setRecipientToList(String recipientToList) {
        this.recipientToList = this.prepareRecipientList(recipientToList);
    }

    public void setRecipientCcList(String recipientCcList) {
        this.recipientCcList = this.prepareRecipientList(recipientCcList);
    }

    public void setRecipientBccList(String recipientBccList) {
        this.recipientBccList = this.prepareRecipientList(recipientBccList);
    }

    public String getRecipientToList() {
        return this.getRecipientList(recipientToList);
    }

    public String getRecipientCcList() {
        return this.getRecipientList(recipientCcList);
    }

    public String getRecipientBccList() {
        return this.getRecipientList(recipientBccList);
    }

    public long getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(long waitTime) {
        this.waitTime = waitTime;
    }


    public ChannelInfo getAdapterChannel() {
        if (adapterChannel == null && StringHelper.isNotEmpty(this.channelId)) {
            adapterChannel = ChannelCatalog.getInstance().getChannel(this.channelId);
        }
        return adapterChannel;
    }

    public boolean isLogicalPath() {
        return isLogicalPath;
    }


    public Object getExternalId1() {
        return externalId1;
    }


    public void setExternalId1(Object externalId1) {
        this.externalId1 = externalId1;
    }


    public Object getExternalId2() {
        return externalId2;
    }


    public void setExternalId2(Object externalId2) {
        this.externalId2 = externalId2;
    }


    public Object getExternalId3() {
        return externalId3;
    }


    public void setExternalId3(Object externalId3) {
        this.externalId3 = externalId3;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }
}
