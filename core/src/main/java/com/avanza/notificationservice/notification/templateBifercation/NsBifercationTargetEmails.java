package com.avanza.notificationservice.notification.templateBifercation;

import java.sql.Timestamp;

import com.avanza.core.data.DbObject;
import com.avanza.workflow.configuration.org.OrganizationalUnit;

/**
 * NsBifercationTargetEmail entity. @author MyEclipse Persistence Tools
 */

public class NsBifercationTargetEmails extends DbObject implements java.io.Serializable {

    // Fields
    private String id;
    private NsTemplateBifercation nsTemplateBifercation;
    private String nsTemplateBifercationId;
    private OrganizationalUnit orgUnit;
    private String orgUnitId;
    private String emailIds;
    private String emailIdsCc;


    // Constructors

    /**
     * default constructor
     */
    public NsBifercationTargetEmails() {
    }


    // Property accessors


    public String getEmailIds() {
        return this.emailIds;
    }

    public void setEmailIds(String emailIds) {
        this.emailIds = emailIds;
    }

    public String getEmailIdsCc() {
        return this.emailIdsCc;
    }

    public void setEmailIdsCc(String emailIdsCc) {
        this.emailIdsCc = emailIdsCc;
    }

    public NsTemplateBifercation getNsTemplateBifercation() {
        return nsTemplateBifercation;
    }


    public void setNsTemplateBifercation(NsTemplateBifercation nsTemplateBifercation) {
        this.nsTemplateBifercation = nsTemplateBifercation;
    }


    public void setOrgUnit(OrganizationalUnit orgUnit) {
        this.orgUnit = orgUnit;
    }


    public OrganizationalUnit getOrgUnit() {
        return orgUnit;
    }


    public void setNsTemplateBifercationId(String nsTemplateBifercationId) {
        this.nsTemplateBifercationId = nsTemplateBifercationId;
    }


    public String getNsTemplateBifercationId() {
        return nsTemplateBifercationId;
    }


    public void setOrgUnitId(String orgUnitId) {
        this.orgUnitId = orgUnitId;
    }


    public String getOrgUnitId() {
        return orgUnitId;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getId() {
        return id;
    }


}