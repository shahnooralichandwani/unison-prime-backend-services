package com.avanza.notificationservice.notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.avanza.core.CoreException;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.main.ClustersConfigLoader;
import com.avanza.core.scheduler.JobID;
import com.avanza.core.scheduler.JobImpl;
import com.avanza.core.scheduler.SchedulerCatalog;
import com.avanza.core.util.Logger;
import com.avanza.notificationservice.adapter.AdapterStatus;
import com.avanza.notificationservice.adapter.NotificationUpdate;
import com.avanza.notificationservice.adapter.ProtocolAdapter;
import com.avanza.notificationservice.channel.ChannelCatalog;
import com.avanza.notificationservice.channel.ChannelUpListener;
import com.avanza.notificationservice.db.DeadNotification;
import com.avanza.notificationservice.db.NotificationImpl;
import com.sun.corba.se.pept.broker.Broker;

public class NotificationDistributor implements NotificationManager,
        NotificationUpdate, ChannelUpListener {

    private static Logger logger = Logger
            .getLogger(NotificationDistributor.class);

    private static final int QUEUE_BATCH_SIZE = 500;

    private DataBroker notificationDB;
    private long timeLapse;

    private static NotificationManager _instance;

    /**
     * The class is responsible for loading the notification asynchronously.
     *
     * @author Farhan Muhammad Amin
     */
    class AsyncNotificationQueueThread extends Thread {
        private List<Notification> notificationList = null;

        public AsyncNotificationQueueThread(List<Notification> notificationList) {
            this.notificationList = notificationList;
        }

        @Override
        public void run() {
            logger
                    .logInfo(
                            "[Processing the pending queue items starts with queue size = %d]",
                            notificationList.size());
            try {
                executeBatchQueueing();
            } catch (Exception ex) {
                logger.LogException(
                        "[Exception occured while batch processing]", ex);
            }
            logger.logInfo("[Processing pending notification ends]");
        }

        /**
         * The function is responsible for batch processing of the notification
         * queue. The function execution is as follows. 1. It iterates through
         * the pending notification. 2. Sets the wait time, retry count and
         * adapter channel timeout. 3. If the current status of the notification
         * is wait, the status is being changed to New and add it to a
         * intermediate list. 4. If the intermediate size of the list equals to
         * the batch size. a. Update the status of the notification in the
         * NS_NOTIFICATION database. b. Queue the notification so notification
         * worker can start processing the notifications. 5. if there are
         * pending item in the list repeat 1.
         */
        private void executeBatchQueueing() {
            logger.logInfo("[executeBatchQueueing .. Starts]");
            if (null != notificationList && 0 < notificationList.size()) {
                List<Notification> intermediateList = new ArrayList<Notification>(
                        QUEUE_BATCH_SIZE);
                for (Notification notification : notificationList) {
                    notification.setWaitTime(0);
                    notification.setRetryCount(notification.getAdapterChannel()
                            .getRetryCount());
                    notification.setTimeOut(notification.getAdapterChannel()
                            .getTimeOut());

                    intermediateList.add(notification);

                    if (notification.getNotificationState().equals(
                            NotificationState.Wait)) {
                        notification
                                .setNotificationState(NotificationState.New);
                        if ((intermediateList.size() % QUEUE_BATCH_SIZE) == 0) {
                            logger
                                    .logInfo("[Intermediate List equals to the batch size .. Update the status and add it to Queue]");
                            notificationDB.persistAll(intermediateList);
                            logger
                                    .logInfo(
                                            "[Notification status of %d items updated successfully]",
                                            intermediateList.size());
                            NotificationDistributor.this
                                    .schedule(intermediateList);
                            logger
                                    .logInfo(
                                            "[Notification queue with size = %d was successfully queued]",
                                            intermediateList.size());
                            intermediateList.clear();
                        }
                    }
                }
                if (intermediateList.size() > 0) {
                    notificationDB.persistAll(intermediateList);
                    NotificationDistributor.this.schedule(intermediateList);
                    intermediateList.clear();
                }
            }
            logger.logInfo("[executeBatchQueueing .. Ends]");
        }
    }

    public static class SendingNotificationJob extends JobImpl {

        @Override
        public void executeInner() {

            Notification notification = (Notification) getAttachedDataByKey("Notification");
            NotificationUpdate notificationUpdate = (NotificationUpdate) getAttachedDataByKey("NotificationUpdate");
            ChannelUpListener channelUpListener = (ChannelUpListener) getAttachedDataByKey("ChannelUpListener");
            ProtocolAdapter protocolAdapter = ChannelCatalog.getInstance()
                    .getAdapter(notification.getChannelId());
            protocolAdapter.setNotificationUpdate(notificationUpdate);
            DataBroker broker = DataRepository.getBroker(Notification.class
                    .getName());
            if (protocolAdapter.getStatus() == AdapterStatus.Running)
                protocolAdapter.notify(notification);
            else if (protocolAdapter.getStatus() == AdapterStatus.Down) {
                notification.setNotificationState(NotificationState.Wait);
                broker.persist(notification);
                protocolAdapter.startChannelUpListener(channelUpListener,
                        notification.getChannelId());
            }

        }

    }

    @Deprecated
    public static class SendingNotificationJobTest extends JobImpl {

        @Override
        public void executeInner() {

            String notificationId = (String) getAttachedDataByKey("NotificationId");
            DataBroker broker = DataRepository.getBroker(NotificationImpl.class
                    .getName());
            Notification notification = broker.findById(NotificationImpl.class,
                    notificationId);
            ProtocolAdapter protocolAdapter = ChannelCatalog.getInstance()
                    .getAdapter(notification.getChannelId());
            if (protocolAdapter.getStatus() == AdapterStatus.Running)
                protocolAdapter.notify(notification);

        }

    }

    private NotificationDistributor() {
    }

    public static synchronized NotificationManager getInstance() {

        if (_instance == null) {
            _instance = new NotificationDistributor();
            ((NotificationDistributor) _instance).load();
        }
        return _instance;
    }

    private void load() {
        // getAllNotification from Database that are not distributed,
        // re-send,retry
        Search query = new Search(NotificationImpl.class);
        query.addCriterion(SimpleCriterion.equal("state", NotificationState.New
                .toString()));
        query.addCriterionOr(SimpleCriterion.equal("state",
                NotificationState.Failed.toString()));
        query.addCriterionOr(SimpleCriterion.equal("state",
                NotificationState.Wait.toString()));
        query.addCriterion(SimpleCriterion.equal("nodeId", ClustersConfigLoader
                .getCurrentClusterInfo().getName()));
        notificationDB = DataRepository.getBroker(NotificationDistributor.class
                .getName());
        query.setCanRepresent(true);
        List<Notification> notificationList = notificationDB.find(query);

        AsyncNotificationQueueThread thread = new AsyncNotificationQueueThread(
                notificationList);
        thread.start();
    }

    public void notifyMessage(Notification msg) {

        try {
            logger
                    .logInfo("'Notification' is going to be queued from notifyMessage "
                            + msg.getNotificationId());
            msg.setNodeId(ClustersConfigLoader.getCurrentClusterInfo()
                    .getName());
            schedule(msg);
            notificationDB.persist(msg);
        } catch (Throwable e) {
            logger.LogException("Error while notifyMessage(Notification Msg)",
                    e);
            throw new CoreException(
                    "Error while notifyMessage(Notification Msg)", e);
        }
    }

    private void schedule(Notification msg) {

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("Notification", msg);
        map.put("NotificationUpdate", this);
        map.put("ChannelUpListener", this);

        SchedulerCatalog.getScheduler().scheduleJob(
                new JobID(msg.getNotificationId()), "Notification",
                SendingNotificationJob.class.getName(), map, "Notification", 0,
                msg.getWaitTime(), msg.getPriority().getValue());

        logger.logInfo("'Notification' has been queued in Scheduler "
                + msg.getNotificationId());
    }

    @Deprecated
    private void scheduleDBStoreTest(Notification msg) {

        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("NotificationId", msg.getNotificationId());

        SchedulerCatalog.getScheduler().scheduleJob(
                new JobID(msg.getNotificationId()), "Notification",
                SendingNotificationJobTest.class.getName(), map,
                "Notification", 0, msg.getWaitTime(),
                msg.getPriority().getValue());

        logger.logInfo("'Notification' has been queued in Scheduler "
                + msg.getNotificationId());
    }

    private void schedule(List<Notification> msgList) {
        logger.logInfo("batch of Notifications is going to be queued.");
        for (Notification msg : msgList) {
            schedule(msg);
        }
    }

    public void update(Notification msg) {
        try {
            if (msg.state == NotificationState.Failed
                    && msg.getRetryCount() > 0) {
                msg.setRetryCount(msg.getRetryCount() - 1);

                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("Notification", msg);
                map.put("NotificationUpdate", this);
                map.put("ChannelUpListener", this);

                SchedulerCatalog.getScheduler().scheduleJob(
                        new JobID(msg.getNotificationId()),
                        "FailedNotification",
                        SendingNotificationJob.class.getName(), map,
                        "FailedNotification", 0,
                        msg.getAdapterChannel().getRetryInterval() * 1000,
                        msg.getPriority().getValue());

                logger
                        .logInfo("'FailedNotification' has been queued in Scheduler "
                                + msg.getNotificationId());

            } else if (msg.state == NotificationState.Failed
                    && msg.getRetryCount() <= 0) {
                notificationDB.persist(DeadNotification
                        .create((NotificationImpl) msg));
                notificationDB.delete((NotificationImpl) msg);
                return;
            }

            notificationDB.persist((NotificationImpl) msg);
        } catch (Throwable t) {
            logger
                    .LogException(
                            "Exception in update(Notification)-NotificationDistributor",
                            t);
        } finally {
            TransactionContext txnContext = ApplicationContext.getContext()
                    .getTxnContext();
            txnContext.release();
        }
    }

    public void dispose() {
    }

    public void notifyChannelUp(String adapterChannelId) {
        // Now we need to re-fetch all the notifications in the waiting state
        // and change their status to New.
        logger.logInfo("notifyChannelUp " + adapterChannelId);
        Search query = new Search(NotificationImpl.class);
        query.addCriterion(SimpleCriterion.equal("state",
                NotificationState.Wait.toString()));
        query
                .addCriterion(SimpleCriterion.equal("channelId",
                        adapterChannelId));
        query.addCriterion(SimpleCriterion.equal("nodeId", ClustersConfigLoader
                .getCurrentClusterInfo().getName()));
        notificationDB = DataRepository.getBroker(NotificationDistributor.class
                .getName());
        List<Notification> notificationList = notificationDB.find(query);

        AsyncNotificationQueueThread asyncNotificationQueueThread = new AsyncNotificationQueueThread(
                notificationList);
        asyncNotificationQueueThread.start();
    }
}
