package com.avanza.notificationservice.notification;


public enum PriorityType {

    Lowest(3), Low(4), Medium(5), High(6), Highest(7);

    private int value;

    private PriorityType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public static PriorityType fromValue(int value) {
        switch (value) {
            case 1:
                return PriorityType.Low;
            case 2:
                return PriorityType.Medium;
            case 3:
                return PriorityType.High;
            case 4:
                return PriorityType.Highest;
            default:
                return PriorityType.Lowest;
        }
    }
}
