package com.avanza.notificationservice.notification.templateBifercation;

/**
 * @author Nida
 * This enum provides bifercation types of Different Templates
 */
public enum BifercationEvent {
    Level1("LVL1"), Level2("LVL2"),
    Level3("LVL3"), Level4("LVL4"),
    Level5("LVL5"), Level6("LVL6"),
    Acknowledgment("ACK"), Close("CLOSE"),
    Initiated("INIT"), Resolved("RESOLVED");

    String value;

    BifercationEvent(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static BifercationEvent getEvent(String value) {
        for (BifercationEvent event : BifercationEvent.values()) {
            if (event.value.equals(value)) return event;
        }
        throw new IllegalArgumentException("BifercationEvent not found. Amputated?");
    }
}
