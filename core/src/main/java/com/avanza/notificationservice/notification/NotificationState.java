package com.avanza.notificationservice.notification;

public enum NotificationState {
    None, New, Sent, Failed, Expired, Wait
}
