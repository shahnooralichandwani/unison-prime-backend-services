package com.avanza.notificationservice.notification;

/**
 * @author shahbaz.ali
 * The implemetations of this interface will serve as a callback  handler for the notifications from ProtocolAdapter
 */
public interface NotificationCallbackHandler {

    public void handle(Notification notification, NotificationState state, Object... object);
}
