package com.avanza.notificationservice.notification.templateBifercation;

import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;

public class NotificationTemplateManager {

    public static NsTemplateBifercation getTemplateOfProcess(
            String processCode, BifercationType type,
            BifercationDocument document, BifercationEvent event) {

        try {
            if (type != null && document != null && event != null) {
                Search search = new Search(NsTemplateBifercation.class);

                DataBroker broker = DataRepository
                        .getBroker(NsTemplateBifercation.class.getName());


                search.addCriterion(Criterion.equal("processCode",
                        processCode));
                search.addCriterion(Criterion.equal("bifercationType", type
                        .getValue()));
                search.addCriterion(Criterion.equal("documentType", document
                        .getValue()));
                search.addCriterion(Criterion
                        .equal("onEvent", event.getValue()));


                Object templateDetail = broker.find(search);
                if (templateDetail != null)
                    return ((List<NsTemplateBifercation>) templateDetail)
                            .get(0);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static NsTemplateBifercation getTemplateOfState(String processCode,
                                                           BifercationType type, BifercationDocument document, String stateCode) {

        try {
            if (type != null && document != null && stateCode != null) {
                Search search = new Search(NsTemplateBifercation.class);

                DataBroker broker = DataRepository
                        .getBroker(NsTemplateBifercation.class.getName());


                search.addCriterion(Criterion.equal("processCode",
                        processCode));
                search.addCriterion(Criterion.equal("bifercationType", type
                        .getValue()));
                search.addCriterion(Criterion.equal("documentType", document
                        .getValue()));
                search.addCriterion(Criterion.equal("onEvent", stateCode));


                Object templateDetail = broker.find(search);
                if (templateDetail != null)
                    return ((List<NsTemplateBifercation>) templateDetail)
                            .get(0);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static NsTemplateBifercation getTemplateOfEscalation(
            String strategyId, BifercationType type,
            BifercationDocument document, BifercationEvent level) {

        try {
            if (type != null && document != null && level != null) {
                Search search = new Search(NsTemplateBifercation.class);

                DataBroker broker = DataRepository
                        .getBroker(NsTemplateBifercation.class.getName());

                search.addCriterion(Criterion.equal("executionStrategyId",
                        strategyId));
                search.addCriterion(Criterion.equal("bifercationType", type
                        .getValue()));
                search.addCriterion(Criterion.equal("documentType", document
                        .getValue()));
                search.addCriterion(Criterion
                        .equal("onEvent", level.getValue()));


                Object templateDetail = broker.find(search);
                if (templateDetail != null)
                    return ((List<NsTemplateBifercation>) templateDetail)
                            .get(0);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
