package com.avanza.notificationservice.notification.templateBifercation;

/**
 * @author Nida
 * This enum provides bifercation types of Different Templates
 */
public enum BifercationDocument {
    Lead("LEAD"), Eform("EFORM"),
    Complaint("COMPLAINT");

    String value;

    BifercationDocument(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static BifercationDocument getType(String value) {
        for (BifercationDocument doc : BifercationDocument.values()) {
            if (doc.value.equals(value)) return doc;
        }
        throw new IllegalArgumentException("BifercationDocument not found. Amputated?");
    }
}
