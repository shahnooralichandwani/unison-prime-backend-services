package com.avanza.notificationservice.notification.templateBifercation;

/**
 * @author Nida
 * This enum provides bifercation types of Different Templates
 */
public enum TemplateAudience {
    ExternalUser("SYS"), Customer("CUSTOMER"),
    CurrentActor("CA"), ExternalUserAndCA("SYS_CA");

    String value;

    TemplateAudience(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TemplateAudience getAud(String value) {
        for (TemplateAudience aud : TemplateAudience.values()) {
            if (aud.value.equals(value)) return aud;
        }
        throw new IllegalArgumentException("TemplateAudience not found. Amputated?");
    }
}
