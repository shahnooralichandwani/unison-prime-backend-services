package com.avanza.notificationservice.notification;


public interface NotificationManager {

    void notifyMessage(Notification notification);

    void dispose();
}
