package com.avanza.notificationservice.notification;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.interaction.email.InteractionChannel;
import com.avanza.core.interaction.email.NotificationType;
import com.avanza.core.meta.MetaCounter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;
import com.avanza.notificationservice.channel.ChannelCatalog;
import com.avanza.notificationservice.channel.ChannelInfo;
import com.avanza.notificationservice.db.NotificationImpl;

/**
 * @author kraza
 */
public class NotificationSender {

    private static SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyy");
    private static SimpleDateFormat df_alert = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    private static boolean isLoaded = false;
    private static String smsFrom;
    private static String emailFrom;
    private static String faxFrom;
    private static String alertsFrom;

    public static void load(ConfigSection cfgSection) {

        if (cfgSection == null || isLoaded) return;

        smsFrom = cfgSection.getValue("sms-from", "unison@avanza.pk");
        emailFrom = cfgSection.getValue("email-from", "unison@avanzasolutions.com");
        faxFrom = cfgSection.getValue("fax-from", "unison@avanza.pk");
        alertsFrom = cfgSection.getValue("alerts-from", "unison@avanza.pk");

        isLoaded = true;
    }

    @Deprecated
    public static void sendSMS(String mobileNumber, String message, String channelId, String subject) {
        sendSMS(mobileNumber, message, channelId, PriorityType.Medium, StringHelper.isEmpty(subject) ? "SMS Notification" : subject);
    }

    @Deprecated
    public static void sendSMS(String mobileNumber, String message, String channelId) {
        sendSMS(mobileNumber, message, channelId, PriorityType.Medium, "SMS Notification");
    }

    @Deprecated
    public static void sendSMS(String mobileNumber, String message, String channelId, PriorityType priority, String subject) {

        Guard.checkNullOrEmpty(mobileNumber, "mobileNumber");
        Guard.checkNullOrEmpty(message, "message");


        //get default channel if not provided
        if (channelId == null) {
            InteractionChannel channel = InteractionChannel.getInteractionChannel(NotificationType.SMS);
            if (channel != null) {
                channelId = channel.getChannelId();
            }
        }
        Guard.checkNullOrEmpty(channelId, "channelId");

        Date date = new Date();
        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        LocaleInfo localeInfo = null;
        if (webCtx != null) localeInfo = webCtx.getAttribute(SessionKeyLookup.CurrentLocale);
        ChannelInfo channelInfo = ChannelCatalog.getInstance().getChannelById(channelId);

        NotificationImpl smsNotify = new NotificationImpl();
        smsNotify.setNotificationId((String) MetaDataRegistry.getNextCounterValue(MetaCounter.SENT_EMAIL_COUNTER));
        smsNotify.setBody(message);
        smsNotify.setRecipientToList(mobileNumber);
        smsNotify.setSentTime(df.format(date));
        smsNotify.setStatusText("Sending SMS - Begin");
        smsNotify.setChannelId(channelInfo.getAdapterChannelId());
        if (localeInfo != null)
            smsNotify.setLocale(localeInfo.getLocale().toString());
        else
            smsNotify.setLocale(Locale.ENGLISH.toString());
        smsNotify.setNotificationState(NotificationState.New);
        smsNotify.setNsFrom(smsFrom);
        smsNotify.setPriority(priority);
        smsNotify.setRetryCount(channelInfo.getRetryCount());
        smsNotify.setTimeOut(channelInfo.getTimeOut());
        smsNotify.setSubject(subject);
        if (webCtx != null) smsNotify.setExternalId1(webCtx.getAttribute(SessionKeyLookup.CURRENT_CUSTOMER_ID_KEY));
        NotificationDistributor.getInstance().notifyMessage(smsNotify);
    }

    @Deprecated
    public static void sendEmail(String subject, String emailBody, String to, List<String> attachments) {
        sendEmail(subject, emailBody, to, attachments, null, PriorityType.Medium);
    }

    @Deprecated
    public static void sendEmail(String subject, String emailBody, String to, List<String> attachments, PriorityType priority) {
        sendEmail(subject, emailBody, to, attachments, null, PriorityType.Medium);
    }

    @Deprecated
    public static void sendEmail(String subject, String emailBody, String to, List<String> attachments, String channelId) {
        sendEmail(subject, emailBody, to, attachments, channelId, PriorityType.Medium);
    }

    @Deprecated
    public static void sendEmail(String subject, String emailBody, String to, List<String> attachments, String channelId, PriorityType priority) {

        Guard.checkNullOrEmpty(subject, "subject");
        Guard.checkNullOrEmpty(emailBody, "emailBody");
        Guard.checkNullOrEmpty(to, "to");


        //get default channel if not provided
        if (channelId == null) {
            InteractionChannel channel = InteractionChannel.getInteractionChannel(NotificationType.Email);
            if (channel != null) {
                channelId = channel.getChannelId();
            }
        }
        Guard.checkNullOrEmpty(channelId, "channelId");

        Date date = new Date();
        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        LocaleInfo localeInfo = null;
        if (webCtx != null) localeInfo = webCtx.getAttribute(SessionKeyLookup.CurrentLocale);
        ChannelInfo channelInfo = ChannelCatalog.getInstance().getChannelById(channelId);

        NotificationImpl emailNotify = new NotificationImpl();
        emailNotify.setBody(emailBody);
        emailNotify.setRecipientToList(to);
        StringBuilder attachmentList = new StringBuilder("");

        if (attachments != null && attachments.size() > 0) {
            for (String attachment : attachments) {
                attachmentList.append(attachment).append(",");
            }
            if (attachmentList.length() > 0) attachmentList.deleteCharAt(attachmentList.length() - 1);
            boolean isLogical = !(attachmentList.toString().indexOf(":\\") >= 0 || attachmentList.toString().indexOf("\\\\") >= 0);
            emailNotify.setDocumentList(attachmentList.toString(), isLogical);
        }
        emailNotify.setNotificationId((String) MetaDataRegistry.getNextCounterValue(MetaCounter.SENT_EMAIL_COUNTER));
        emailNotify.setSentTime(df.format(date));
        emailNotify.setStatusText("Sending Email - Begin");
        emailNotify.setChannelId(channelInfo.getAdapterChannelId());
        if (localeInfo != null)
            emailNotify.setLocale(localeInfo.getLocale().toString());
        else
            emailNotify.setLocale(Locale.ENGLISH.toString());
        emailNotify.setNotificationState(NotificationState.New);
        emailNotify.setNsFrom(emailFrom);
        emailNotify.setPriority(priority);
        emailNotify.setRetryCount(channelInfo.getRetryCount());
        emailNotify.setTimeOut(channelInfo.getTimeOut());
        emailNotify.setSubject(subject);
        if (webCtx != null) emailNotify.setExternalId1(webCtx.getAttribute(SessionKeyLookup.CURRENT_CUSTOMER_ID_KEY));
        NotificationDistributor.getInstance().notifyMessage(emailNotify);
    }

    @Deprecated
    public static void sendFax(String coverText, String faxBody, String to, List<String> attachments) {
        sendFax(coverText, faxBody, to, attachments, null, PriorityType.Medium);
    }

    @Deprecated
    public static void sendFax(String coverText, String faxBody, String to, List<String> attachments, PriorityType priority) {
        sendFax(coverText, faxBody, to, attachments, null, PriorityType.Medium);
    }

    @Deprecated
    public static void sendFax(String coverText, String faxBody, String to, List<String> attachments, String channelId) {
        sendFax(coverText, faxBody, to, attachments, channelId, PriorityType.Medium);
    }

    @Deprecated
    public static void sendFax(String coverText, String faxBody, String to, List<String> attachments, String channelId, PriorityType priority) {

        Guard.checkNullOrEmpty(coverText, "subject");
        Guard.checkNullOrEmpty(faxBody, "emailBody");
        Guard.checkNullOrEmpty(to, "to");


        //get default channel if not provided
        if (channelId == null) {
            InteractionChannel channel = InteractionChannel.getInteractionChannel(NotificationType.Fax);
            if (channel != null) {
                channelId = channel.getChannelId();
            }
        }
        Guard.checkNullOrEmpty(channelId, "channelId");

        Date date = new Date();
        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        LocaleInfo localeInfo = null;
        if (webCtx != null) localeInfo = webCtx.getAttribute(SessionKeyLookup.CurrentLocale);
        ChannelInfo channelInfo = ChannelCatalog.getInstance().getChannelById(channelId);

        NotificationImpl faxNotify = new NotificationImpl();
        faxNotify.setBody(faxBody);
        faxNotify.setRecipientToList(to);
        StringBuilder attachmentList = new StringBuilder("");

        if (attachments != null && attachments.size() > 0) {
            for (String attachment : attachments) {
                attachmentList.append(attachment).append(",");
            }
            if (attachmentList.length() > 0) attachmentList.deleteCharAt(attachmentList.length() - 1);
            boolean isLogical = !(attachmentList.toString().indexOf(":\\") >= 0);
            faxNotify.setDocumentList(attachmentList.toString(), isLogical);
        }
        faxNotify.setNotificationId((String) MetaDataRegistry.getNextCounterValue(MetaCounter.SENT_EMAIL_COUNTER));
        faxNotify.setSentTime(df.format(date));
        faxNotify.setStatusText("Sending Fax - Begin");
        faxNotify.setChannelId(channelInfo.getAdapterChannelId());
        if (localeInfo != null)
            faxNotify.setLocale(localeInfo.getLocale().toString());
        else
            faxNotify.setLocale(Locale.ENGLISH.toString());
        faxNotify.setNotificationState(NotificationState.New);
        faxNotify.setNsFrom(faxFrom);
        faxNotify.setPriority(priority);
        faxNotify.setRetryCount(channelInfo.getRetryCount());
        faxNotify.setTimeOut(channelInfo.getTimeOut());
        faxNotify.setSubject(coverText);
        if (webCtx != null) faxNotify.setExternalId1(webCtx.getAttribute(SessionKeyLookup.CURRENT_CUSTOMER_ID_KEY));
        NotificationDistributor.getInstance().notifyMessage(faxNotify);
    }

    @Deprecated
    public static void sendAlert(String channelId, String subject, String description, String toList, Date occourenceTime) {
        sendAlert(channelId, subject, description, toList, occourenceTime, PriorityType.Medium);
    }

    @Deprecated
    public static void sendAlert(String channelId, String subject, String description, String toList, Date occourenceTime, PriorityType priority) {

        Guard.checkNullOrEmpty(subject, "subject");
        Guard.checkNullOrEmpty(description, "description");
        Guard.checkNullOrEmpty(toList, "toList");

        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        LocaleInfo localeInfo = null;
        if (webCtx != null) localeInfo = webCtx.getAttribute(SessionKeyLookup.CurrentLocale);
        ChannelInfo channelInfo = ChannelCatalog.getInstance().getChannelById(channelId);

        NotificationImpl alertNotify = new NotificationImpl();
        alertNotify.setBody(description);
        alertNotify.setRecipientToList(toList);
        alertNotify.setNotificationId((String) MetaDataRegistry.getNextCounterValue(MetaCounter.ALERT_COUNTER));
        alertNotify.setSentTime(df_alert.format(occourenceTime));
        alertNotify.setStatusText("Sending Alert - Begin");
        alertNotify.setChannelId(channelInfo.getAdapterChannelId());
        if (localeInfo != null)
            alertNotify.setLocale(localeInfo.getLocale().toString());
        else
            alertNotify.setLocale(Locale.ENGLISH.toString());
        alertNotify.setNotificationState(NotificationState.New);
        alertNotify.setNsFrom(alertsFrom);
        alertNotify.setPriority(priority);
        alertNotify.setRetryCount(channelInfo.getRetryCount());
        alertNotify.setTimeOut(channelInfo.getTimeOut());
        alertNotify.setSubject(subject);
        NotificationDistributor.getInstance().notifyMessage(alertNotify);
    }

    /*
     * Shahbaz:
     * Overloaded method for Email Auto reply , 'from' field added
     */
    @Deprecated
    public static void sendEmail(String subject, String emailBody, String to, String from, List<String> attachments, String channelId, PriorityType priority) {

        Guard.checkNullOrEmpty(subject, "subject");
        Guard.checkNullOrEmpty(emailBody, "emailBody");
        Guard.checkNullOrEmpty(to, "to");
        Guard.checkNullOrEmpty(channelId, "channelId");
        Guard.checkNullOrEmpty(from, "from");

        Date date = new Date();
        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        LocaleInfo localeInfo = null;
        if (webCtx != null) localeInfo = webCtx.getAttribute(SessionKeyLookup.CurrentLocale);
        ChannelInfo channelInfo = ChannelCatalog.getInstance().getChannelById(channelId);

        NotificationImpl emailNotify = new NotificationImpl();
        emailNotify.setBody(emailBody);
        emailNotify.setRecipientToList(to);
        StringBuilder attachmentList = new StringBuilder("");

        if (attachments != null && attachments.size() > 0) {
            for (String attachment : attachments) {
                attachmentList.append(attachment).append(",");
            }
            if (attachmentList.length() > 0) attachmentList.deleteCharAt(attachmentList.length() - 1);
            boolean isLogical = !(attachmentList.toString().indexOf(":\\") >= 0 || attachmentList.toString().indexOf("\\\\") >= 0);
            emailNotify.setDocumentList(attachmentList.toString(), isLogical);
        }
        emailNotify.setNotificationId((String) MetaDataRegistry.getNextCounterValue(MetaCounter.SENT_EMAIL_COUNTER));
        emailNotify.setSentTime(df.format(date));
        emailNotify.setStatusText("Sending Email - Begin");
        emailNotify.setChannelId(channelInfo.getAdapterChannelId());
        if (localeInfo != null)
            emailNotify.setLocale(localeInfo.getLocale().toString());
        else
            emailNotify.setLocale(Locale.ENGLISH.toString());
        emailNotify.setNotificationState(NotificationState.New);
        emailNotify.setNsFrom(from);
        emailNotify.setPriority(priority);
        emailNotify.setRetryCount(channelInfo.getRetryCount());
        emailNotify.setTimeOut(channelInfo.getTimeOut());
        emailNotify.setSubject(subject);
        if (webCtx != null) emailNotify.setExternalId1(webCtx.getAttribute(SessionKeyLookup.CURRENT_CUSTOMER_ID_KEY));
        NotificationDistributor.getInstance().notifyMessage(emailNotify);
    }


    public static void sendNotification(String channelId, String subject, String body, String to, List<String> attachments) {
        sendNotification(channelId, subject, body, to, null, attachments, null, null, null);
    }

    //---- added For cc List
    public static void sendNotification(String channelId, String subject, String body, String to, List<String> attachments, String cc) {
        sendNotification(channelId, subject, body, to, null, attachments, null, null, null, cc);
    }

    public static void sendNotification(String channelId, String subject, String body, String to, List<String> attachments, PriorityType priority) {
        sendNotification(channelId, subject, body, to, null, attachments, priority, null, null);
    }

    public static void sendNotification(String channelId, String subject, String body, String to, String from, List<String> attachments) {
        sendNotification(channelId, subject, body, to, from, attachments, null, null, null);
    }

    public static void sendNotification(String channelId, String subject, String body, String to, String from, List<String> attachments, PriorityType priority) {
        sendNotification(channelId, subject, body, to, from, attachments, priority, null, null);
    }


    public static void sendNotification(String channelId, String subject, String body, String to, List<String> attachments, PriorityType priority, Locale locale) {
        sendNotification(channelId, subject, body, to, null, attachments, priority, locale, null);
    }

    public static void sendNotification(String channelId, String subject, String body, String to, String from, List<String> attachments, PriorityType priority, Locale locale) {
        sendNotification(channelId, subject, body, to, from, attachments, priority, locale, null);
    }

    public static void sendNotification(String channelId, String subject, String body, String to, String from, List<String> attachments, PriorityType priority, Locale locale, Date occourenceTime) {
        sendNotification(channelId, subject, body, to, null, from, attachments, priority, locale, occourenceTime);
    }

    //---- added For cc List
    public static void sendNotification(String channelId, String subject, String body, String to, String from, List<String> attachments, PriorityType priority, Locale locale, Date occourenceTime, String cc) {
        sendNotification(channelId, subject, body, to, cc, from, attachments, priority, locale, occourenceTime);
    }

    public static void sendNotification(String channelId, String subject, String body, String to, String cc, String from, List<String> attachments, PriorityType priority, Locale locale, Date occourenceTime) {

        Guard.checkNullOrEmpty(body, "body");
        Guard.checkNullOrEmpty(to, "to");
        Guard.checkNullOrEmpty(channelId, "channelId");


        NotificationImpl notification = new NotificationImpl();
        notification.setBody(body);
        notification.setRecipientToList(to);
        notification.setRecipientCcList(cc);
        notification.setNotificationId((String) MetaDataRegistry.getNextCounterValue("NsNotification"));
        notification.setStatusText("Sending Notification Begin");


        notification.setNotificationState(NotificationState.New);
        notification.setNsFrom((StringHelper.isEmpty(from)) ? emailFrom : from);
        notification.setPriority(priority);
        notification.setSubject(subject);


        //channel infos
        ChannelInfo channelInfo = ChannelCatalog.getInstance().getChannelById(channelId);
        notification.setChannelId(channelInfo.getAdapterChannelId());
        notification.setRetryCount(channelInfo.getRetryCount());
        notification.setTimeOut(channelInfo.getTimeOut());

        //locale
        if (locale != null) {
            notification.setLocale(locale.toString());
        } else {
            notification.setLocale(Locale.ENGLISH.toString());
        }

        //priority
        if (priority != null) {
            notification.setPriority(priority);
        } else {
            notification.setPriority(PriorityType.Medium);
        }

        //attachments
        if (attachments != null && attachments.size() > 0) {
            StringBuilder attachmentList = new StringBuilder("");
            for (String attachment : attachments) {
                attachmentList.append(attachment).append(",");
            }
            if (attachmentList.length() > 0) attachmentList.deleteCharAt(attachmentList.length() - 1);
            boolean isLogical = !(attachmentList.toString().indexOf(":\\") >= 0 || attachmentList.toString().indexOf("\\\\") >= 0);
            notification.setDocumentList(attachmentList.toString(), isLogical);
        }

        NotificationDistributor.getInstance().notifyMessage(notification);
    }

}

