package com.avanza.notificationservice.notification.templateBifercation;

/**
 * @author Nida
 * This enum provides bifercation types of Different Templates
 */
public enum BifercationType {
    Notification("NOTIFY"), Escalation("ESC"),
    Reminder("REM");

    String value;

    BifercationType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static BifercationType getType(String value) {
        for (BifercationType type : BifercationType.values()) {
            if (type.value.equals(value)) return type;
        }
        throw new IllegalArgumentException("BifercationType not found. Amputated?");
    }
}
