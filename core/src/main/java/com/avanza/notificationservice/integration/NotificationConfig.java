package com.avanza.notificationservice.integration;

import com.avanza.core.data.DbObject;
import com.avanza.core.sdo.ActivityTypeCatalog;
import com.avanza.core.sessionhistory.ActivityType;


public class NotificationConfig extends DbObject {

    private String id;

    private String templateId;

    private String selectCritera;

    private String documentId;

    private String channelId;

    private String activityTypeId;
    private ActivityType activityType;


    public String getActivityTypeId() {
        return activityTypeId;
    }


    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }


    public ActivityType getActivityType() {
        if (activityType == null) {
            activityType = ActivityTypeCatalog.getInstance().getActivityFromTree(this.activityTypeId);
        }
        return activityType;
    }


    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }


    public String getChannelId() {
        return channelId;
    }


    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getTemplateId() {
        return templateId;
    }


    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getSelectCritera() {
        return selectCritera;
    }


    public void setSelectCritera(String selectCritera) {
        this.selectCritera = selectCritera;
    }


    public String getDocumentId() {
        return documentId;
    }


    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
}
