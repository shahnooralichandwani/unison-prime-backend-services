package com.avanza.notificationservice.event;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.avanza.core.util.Guard;
import com.avanza.integration.MessageBase;
import com.avanza.integration.MessageFieldBase;

/**
 * @author kraza
 */
public class Event extends MessageBase implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3655685106423741024L;

    private MetaEvent metaEvent;

    public MetaEvent getMetaEvent() {
        return metaEvent;
    }

    public Event(MetaEvent metaEvent) {

        Guard.checkNull(metaEvent, "Event.Event(metaEvent)");
        this.metaEvent = metaEvent;
        this.prepareMessage();
    }

    public Event(MetaEvent metaEvent, HashMap<String, Object> values) {

        this.init(metaEvent);
        this.loadEntity(values);
    }

    private void init(MetaEvent metaEvent) {

        Guard.checkNull(metaEvent, "Event.init(metaEvent)");
        this.metaEvent = metaEvent;
        this.fieldList = new Hashtable<String, MessageFieldBase>();
    }

    protected void prepareMessage() {

        Map<String, MetaEventAttribute> metaList = this.metaEvent.getFieldList(true);

        for (MetaEventAttribute item : metaList.values()) {

            this.fieldList.put(item.getSystemName(), item.createField());
        }
    }

    protected void loadEntity(HashMap<String, Object> values) {

        if (values == null)
            return;

        Map<String, MetaEventAttribute> metaList = this.metaEvent.getFieldList(true);

        for (MetaEventAttribute item : metaList.values()) {
            Object value = values.get(item.getSystemName());
            this.fieldList.put(item.getSystemName(), item.createField(value));
        }
    }
}
