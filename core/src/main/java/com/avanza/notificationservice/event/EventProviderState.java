package com.avanza.notificationservice.event;

public enum EventProviderState {

    None, Initializing, Running, Stop
}
