package com.avanza.notificationservice.event;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.core.CoreInterface;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;

public class MetaEventCatalog implements CoreInterface {

    private static final Logger logger = Logger.getLogger(MetaEventCatalog.class);
    private static MetaEventCatalog _instance;
    private Map<String, MetaEvent> metaEvents;
    private DataBroker broker;

    protected MetaEventCatalog() {
        metaEvents = new HashMap<String, MetaEvent>();
        this.broker = DataRepository.getBroker(MetaEventCatalog.class.getName());
    }

    public static synchronized MetaEventCatalog getInstance() {

        if (_instance == null) {
            _instance = new MetaEventCatalog();
        }
        return _instance;
    }

    public void load(ConfigSection section) {
        logger.logInfo("[Loading Event Catalog]");
        List<MetaEvent> metaEventList = this.broker.findAll(MetaEvent.class);
        for (MetaEvent metaEvent : metaEventList) {
            this.metaEvents.put(metaEvent.getMetaEventId(), metaEvent);
        }
    }

    public void dispose() {
        this.metaEvents.clear();
    }
}