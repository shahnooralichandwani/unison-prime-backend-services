package com.avanza.notificationservice.event;

public interface StateHandler {

    void update(EventProviderState state, String stateText);
}
