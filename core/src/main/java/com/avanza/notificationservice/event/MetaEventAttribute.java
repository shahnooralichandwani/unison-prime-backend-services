package com.avanza.notificationservice.event;

import com.avanza.core.data.DbObject;
import com.avanza.integration.meta.messaging.MetaMessageFieldInterface;

public class MetaEventAttribute extends DbObject implements MetaMessageFieldInterface {

    /**
     *
     */
    private static final long serialVersionUID = -4332336012146656114L;

    private String eventAttribId;
    private String metaEventId;
    private MetaEvent metaEvent;
    private String displayName;
    private String description;
    private String dataType;
    private String calcExp;

    public MetaEventAttribute() {

    }

    public String getEventAttribId() {
        return eventAttribId;
    }

    public void setEventAttribId(String eventAttribId) {
        this.eventAttribId = eventAttribId;
    }

    public String getMetaEventId() {
        return metaEventId;
    }

    public void setMetaEventId(String metaEventId) {
        this.metaEventId = metaEventId;
    }

    public MetaEvent getMetaEvent() {
        return metaEvent;
    }

    public void setMetaEvent(MetaEvent metaEvent) {
        this.metaEvent = metaEvent;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getCalcExp() {
        return calcExp;
    }

    public void setCalcExp(String calcExp) {
        this.calcExp = calcExp;
    }

    public EventAttribute createField(Object value) {
        return new EventAttribute(this, value);
    }

    public String getSystemName() {
        return this.eventAttribId;
    }

    public EventAttribute createField() {

        return new EventAttribute(this, null);

    }
}
