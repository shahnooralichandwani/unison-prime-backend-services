package com.avanza.notificationservice.event;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.avanza.core.data.DbObject;


public class MetaEvent extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 6554212407562942219L;

    private String metaEventId;
    private MetaEvent parentMetaEvent;
    private String eventName;
    private String description;
    private Set<MetaEvent> metaEvents = new HashSet<MetaEvent>(0);
    private Map<String, MetaEventAttribute> metaEventAttribs = new java.util.HashMap<String, MetaEventAttribute>(0);

    private int fieldCount = -1;

    public MetaEvent() {
    }

    public String getMetaEventId() {
        return metaEventId;
    }

    public void setMetaEventId(String metaEventId) {
        this.metaEventId = metaEventId;
    }

    public MetaEvent getParentMetaEvent() {
        return parentMetaEvent;
    }

    public void setParentMetaEvent(MetaEvent metaEvent) {
        this.parentMetaEvent = metaEvent;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<MetaEvent> getMetaEvents() {
        return metaEvents;
    }

    public void setMetaEvents(Set<MetaEvent> metaEvents) {
        this.metaEvents = metaEvents;
    }

    public Map<String, MetaEventAttribute> getMetaEventAttribs() {
        return metaEventAttribs;
    }

    public void setMetaEventAttribs(Map<String, MetaEventAttribute> metaEventAttribs) {
        this.metaEventAttribs = metaEventAttribs;
    }

    public Map<String, MetaEventAttribute> getFieldList(boolean complete) {

        Hashtable<String, MetaEventAttribute> retVal;

        if (!complete) {

            retVal = new Hashtable<String, MetaEventAttribute>(this.metaEventAttribs.size());
            retVal.putAll(this.metaEventAttribs);
            return retVal;
        } else {

            retVal = new Hashtable<String, MetaEventAttribute>(this.getFieldCount());
            Stack<MetaEvent> stack = this.getMessageStack();
            while (!stack.empty()) {

                MetaEvent event = stack.pop();
                retVal.putAll(event.metaEventAttribs);
            }
        }

        return retVal;
    }

    public Map<String, MetaEventAttribute> getFieldList() {

        return this.getFieldList(true);
    }

    private Stack<MetaEvent> getMessageStack() {

        Stack<MetaEvent> stack = new Stack<MetaEvent>();

        MetaEvent event = this;
        while (event != null) {

            stack.push(event);
            event = event.parentMetaEvent;
        }

        return stack;
    }

    public int size(boolean complete) {

        int retVal;

        if (complete) {

            if (this.fieldCount == -1) this.fieldCount = this.getFieldCount();

            retVal = this.fieldCount;
        } else
            retVal = this.metaEventAttribs.size();

        return retVal;
    }

    public int size() {

        return this.size(true);
    }

    private int getFieldCount() {

        MetaEvent event = this;
        int retVal = 0;

        while (event != null) {
            retVal = event.metaEventAttribs.size();
            event = this.parentMetaEvent;
        }

        return retVal;
    }
}
