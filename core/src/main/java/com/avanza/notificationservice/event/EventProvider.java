package com.avanza.notificationservice.event;

import com.avanza.core.util.configuration.ConfigSection;

public interface EventProvider {

    void initialize(String providerName, ConfigSection args, StateHandler handler);

    void dispose();

    boolean run();
}
