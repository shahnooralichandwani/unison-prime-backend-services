package com.avanza.notificationservice.event;

import java.io.Serializable;

import com.avanza.core.util.ConvertException;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.DataType;
import com.avanza.integration.IntegrationException;
import com.avanza.integration.MessageField;
import com.avanza.integration.MessageFieldBase;

/**
 * @author kraza
 */
public class EventAttribute extends MessageFieldBase implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3655685106423741024L;

    private MetaEventAttribute metaEventAttribute;

    public EventAttribute(MetaEventAttribute metaEventAttribute, Object value) {

        this.metaEventAttribute = metaEventAttribute;
        this.setValue(value);
    }

    public void setValue(Object value) {

        if (value == null) {
            this.value = value;
            return;
        }

        this.value = ConvertUtil.ConvertTo(value, DataType.fromString(this.metaEventAttribute.getDataType()).getJavaType());
    }

    public void clear() {

        throw new IntegrationException("EventAttribute.clear() is not implemented");
    }

    public boolean isValid() {

        throw new IntegrationException("EventAttribute.isValid() is not implemented");
    }

    public boolean equals(Object value) {

        try {
            value = ConvertUtil.ConvertTo(value, DataType.fromString(this.metaEventAttribute.getDataType()).getJavaType());
        } catch (ConvertException e) {
            return false;
        }

        return this.value.equals(value);
    }

    public MetaEventAttribute getMetaEventAttribute() {
        return this.metaEventAttribute;
    }

    protected void setMetaEventAttribute(MetaEventAttribute metaEventAttribute) {
        this.metaEventAttribute = metaEventAttribute;
    }

    @Override
    protected Object clone() {
        return new EventAttribute(this.metaEventAttribute, this.value);
    }

    public int compareTo(MessageField obj) {
        throw new IntegrationException("EventAttribute.compareTo(Attribute) is not implemented");
    }
}
