package com.avanza.notificationservice.db;
// Generated Jan 22, 2008 12:03:44 PM by Hibernate Tools 3.2.0.b10

import java.util.ArrayList;

import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.channel.AdapterInfo;


public class AdapterInfoImpl extends AdapterInfo {


    /**
     *
     */
    private static final long serialVersionUID = 5015087818066837645L;

    public AdapterInfoImpl() {
    }

    public String getParamsList() {
        String retStr = "";
        if (paramsList != null && !paramsList.isEmpty()) {
            for (int i = 0; i < paramsList.size(); i++)
                retStr = retStr + paramsList.get(i) + ",";
            return retStr.substring(0, retStr.length() - 1);
        }
        return "";
    }

    public void setParamsList(String paramLst) {

        paramsList = new ArrayList<String>();
        String[] strArray = null;
        if (StringHelper.isNotEmpty(paramLst)) {
            strArray = paramLst.split(",");
        } else return;

        for (String s : strArray)
            paramsList.add(s);
    }
}