package com.avanza.notificationservice.db;

import com.avanza.notificationservice.notification.Notification;


public class DeadNotification extends Notification {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private boolean regenerated = false;

    public boolean isRegenerated() {
        return regenerated;
    }

    public void setRegenerated(boolean regenerated) {
        this.regenerated = regenerated;
    }

    public static DeadNotification create(NotificationImpl notification) {

        DeadNotification dead = new DeadNotification();
        dead.setBody(notification.getBody());
        dead.setChannelId(notification.getChannelId());
        dead.setContent(notification.getContent());
        dead.setDocumentList(notification.getDocumentList(), notification.isLogicalPath());
        dead.setLocale(notification.getLocale());
        dead.setNotificationId(notification.getNotificationId());
        dead.setNotificationState(notification.getNotificationState());
        dead.setNsFrom(notification.getNsFrom());
        dead.setProtocolFields(notification.getProtocolFields());
        dead.setRecipientBccList(notification.getRecipientBccList());
        dead.setRecipientCcList(notification.getRecipientCcList());
        dead.setRecipientToList(notification.getRecipientToList());
        dead.setRegenerated(false);
        dead.setRetryCount(notification.getRetryCount());
        dead.setSentTime(notification.getSentTime());
        dead.setStatusText(notification.getStatusText());
        dead.setSubject(notification.getSubject());
        dead.setTimeOut(notification.getTimeOut());
        dead.setWaitTime(notification.getWaitTime());
        dead.setNodeId(notification.getNodeId());

        return dead;
    }
}
