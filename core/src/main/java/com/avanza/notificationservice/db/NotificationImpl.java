package com.avanza.notificationservice.db;

import java.io.File;
import java.text.SimpleDateFormat;

import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.util.IOHelper;
import com.avanza.core.util.Logger;
import com.avanza.notificationservice.notification.Notification;
import com.avanza.ui.util.ResourceUtil;

public class NotificationImpl extends Notification {


    /**
     *
     */
    private static final long serialVersionUID = 2489917649499059206L;
    private static Logger logger = Logger.getLogger(NotificationImpl.class);

    private DeadNotification deadNotification;
    private String deadNotificationId;

    public DeadNotification getDeadNotification() {
        return deadNotification;
    }

    public void setDeadNotification(DeadNotification deadNotification) {
        this.deadNotification = deadNotification;
    }

    public String getDeadNotificationId() {
        return deadNotificationId;
    }

    public void setDeadNotificationId(String deadNotificationId) {
        this.deadNotificationId = deadNotificationId;
    }

    public void addDocumentAttachment(String filePath) {

        this.addDocumentAttachment(new File(filePath));
    }

    /**
     * use this method to add the document attachment to the notification object.
     *
     * @param file
     */
    public void addDocumentAttachment(File file) {

        IOHelper.existFile(file);

        String fileName = file.getAbsolutePath();
        fileName = fileName.substring(fileName.lastIndexOf(File.separator) + 1);

        String dateString = new SimpleDateFormat("yyyy-MM-dd").format(getCreatedOn());
        String directory = ResourceUtil.getInstance().getPathProperty(ATTACHMENT_BASE_KEY, File.separator, ApplicationLoader.BASE_APPLICATION_PATH,
                dateString, this.getNotificationId(), "");

        IOHelper.moveTo(file, directory, fileName);

        this.documentList.add(fileName);
    }
}
