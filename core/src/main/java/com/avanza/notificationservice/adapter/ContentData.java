package com.avanza.notificationservice.adapter;

import java.util.ArrayList;
import java.util.Map;

import com.avanza.notificationservice.notification.RecipientInfo;

public interface ContentData {

    String getBody();

    void setBody(String body);

    String getLocale();

    ArrayList<RecipientInfo> getRecipientList();

    Object getContent(String key);

    Map<String, Object> getContents();
}
