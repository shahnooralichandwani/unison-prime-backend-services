package com.avanza.notificationservice.adapter;

import com.avanza.core.util.configuration.ConfigSection;

public interface ContentFormatter {

    void initialize(ConfigSection conf);

    void dispose();

    void formatContent(ContentData data);

}
