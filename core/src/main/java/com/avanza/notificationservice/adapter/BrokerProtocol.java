package com.avanza.notificationservice.adapter;

import javax.mail.MessagingException;
import javax.mail.SendFailedException;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.configuration.ConfigurationException;
import com.avanza.notificationservice.channel.ChannelCatalog;
import com.avanza.notificationservice.channel.ChannelInfo;
import com.avanza.notificationservice.db.ChannelInfoImpl;
import com.avanza.notificationservice.notification.Notification;
import com.avanza.notificationservice.notification.NotificationState;

/**
 * TODO REFACTOR Code is giving Exception, need to fix it.
 */
public class BrokerProtocol extends NotificationAdapterBase implements
        ProtocolAdapter {

    private static Logger logger = Logger.getLogger(BrokerProtocol.class);
    private String brokerKey;
    private boolean initialized;

    public void initialize(ConfigSection protocolParams) {
        try {
            adapterChannelId = protocolParams.getTextValue(ChannelInfo.CHANNEL_ID);
            brokerKey = protocolParams.getTextValue("broker");
            super.setStatus(AdapterStatus.Running);
            initialized = true;
        } catch (Throwable e) {
            logger
                    .LogException(
                            "Exception while initializing the Broker Protocol channel adapter",
                            e);
            super.setStatus(AdapterStatus.Down);
            throw new ConfigurationException(
                    "Exception while initializing the SMTPProtocol channel adapter");

        }
    }

    public void reInitialize() {
        try {
            if (getStatus() == AdapterStatus.Running) {
                return;
            }

            if (!initialized) {
                ChannelInfoImpl channelInfo = (ChannelInfoImpl) ChannelCatalog
                        .getInstance().getChannelById(adapterChannelId);
                initialize(channelInfo.getConfigAdapterParams());
                super.setStatus(AdapterStatus.Running);
            }
        } catch (Exception e) {
            logger
                    .LogException(
                            "Exception while initializing the SMTPProtocol channel adapter",
                            e);
            super.setStatus(AdapterStatus.Down);
        }
    }

    private void onConnectionDown(Notification msg) {
        if (msg != null) {
            msg.setNotificationState(NotificationState.Wait);
            msg.setStatusText("Channel, " + adapterChannelId + " is Down");
            notificationUpdate.update(msg);
        }
    }

    public void notify(Notification msg) {

        // if server is down, mark the status of notification as Wait, so it can
        // be notified later.
        if (getStatus() == AdapterStatus.Down) {
            onConnectionDown(msg);
            return;
        }

        // now send email.
        try {
            DataBroker broker = DataRepository.getBroker(brokerKey);
            broker.add(msg);
            msg.setNotificationState(NotificationState.Sent);
            notificationUpdate.update(msg);
        } catch (Exception e) {
            logger.LogException("while sending email",
                    e);
            msg.setNotificationState(NotificationState.Wait);
            msg.setStatusText(e.getMessage());
            super.setStatus(AdapterStatus.Down);
            onConnectionDown(msg);
        }
    }

    public void dispose() {
        setStatus(AdapterStatus.Down);
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

}
