package com.avanza.notificationservice.adapter;

import com.avanza.notificationservice.notification.Notification;

public interface NotificationUpdate {

    void update(Notification msg);
}
