package com.avanza.notificationservice.adapter;

import com.avanza.core.util.Logger;
import com.avanza.notificationservice.channel.ChannelUpListener;


public abstract class NotificationAdapterBase implements ProtocolAdapter, Runnable {

    private static Logger logger = Logger.getLogger(NotificationAdapterBase.class);
    protected NotificationUpdate notificationUpdate;
    protected ChannelUpListener channelUpListener;
    protected String adapterChannelId;
    private AdapterStatus adapterStatus = AdapterStatus.Down;
    protected Thread innerThread;
    protected long retryInterval = 30;

    protected void setStatus(AdapterStatus status) {
        synchronized (this.adapterStatus) {
            this.adapterStatus = status;
        }
    }

    final public AdapterStatus getStatus() {
        if (adapterStatus != AdapterStatus.Running) adapterStatus = AdapterStatus.Down;
        return adapterStatus;
    }

    public NotificationUpdate getNotificationUpdate() {
        return notificationUpdate;
    }

    public void setNotificationUpdate(NotificationUpdate notificationUpdate) {
        this.notificationUpdate = notificationUpdate;
    }

    public void flush() {
    }

    public void startChannelUpListener(ChannelUpListener listener, String adapterChannelId) {

        if (innerThread == null) {
            innerThread = new Thread(this);
            this.channelUpListener = listener;
            this.adapterChannelId = adapterChannelId;
            innerThread.start();
        }
    }

    public void run() {

        while (adapterStatus != AdapterStatus.Running) {
            try {
                Thread.currentThread().sleep(retryInterval * 1000);
            } catch (InterruptedException e) {
                logger.LogException("SmtpProtocol channel listener thread interupted.", e);
            }
            this.reInitialize();
        }

        this.channelUpListener.notifyChannelUp(adapterChannelId);
        innerThread = null;
    }
}
