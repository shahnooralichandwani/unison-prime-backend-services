package com.avanza.notificationservice.adapter;

import java.text.SimpleDateFormat;

import com.avanza.core.alert.Alert;
import com.avanza.core.alert.AlertType;
import com.avanza.core.alert.AlertUser;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.notificationservice.notification.Notification;
import com.avanza.notificationservice.notification.NotificationState;
import com.avanza.notificationservice.notification.RecipientInfo;


public class UnisonAlertsProtocol extends NotificationAdapterBase implements ProtocolAdapter {

    private static final Logger logger = Logger.getLogger(UnisonAlertsProtocol.class);

    public static final String ALERTS_CHANNEL = "ALERTS-CHANNEL";
    private static SimpleDateFormat df_alert = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    private DataBroker broker;

    public void dispose() {

        this.broker = null;
    }

    public void initialize(ConfigSection protocolParams) {

        try {
            this.broker = DataRepository.getBroker(Alert.class.getName());
            super.setStatus(AdapterStatus.Running);
        } catch (Throwable t) {
            logger.LogException("Exception while initializing the UnisonAlertsProtocol Adapter", t);
            super.setStatus(AdapterStatus.Down);
        }
    }

    public void notify(Notification notification) {

        try {
            Alert alert = new Alert();
            alert.setAlertFrom(notification.getNsFrom());
            alert.setAlertType(AlertType.InternalSystem.toString());
            alert.setDescriptionPrm(notification.getBody());
            alert.setSubjectPrm(notification.getSubject());
            alert.setOccourenceTime(df_alert.parse(notification.getSentTime()));
            this.broker.persist(alert);

            for (RecipientInfo rcp : notification.getRecipientTo()) {
                AlertUser alertUser = new AlertUser();
                alertUser.setAlertTo(rcp.getAddress());
                alertUser.setAlert(alert);
                alertUser.setAlertId(alert.getAlertId());
                alertUser.setRead(false);
                this.broker.persist(alertUser);
            }

            notification.setNotificationState(NotificationState.Sent);
            notificationUpdate.update(notification);
        } catch (Throwable e) {
            logger.LogException("Exception while adding unison system alert.", e);
            notification.setNotificationState(NotificationState.Failed);
            notificationUpdate.update(notification);
        }
    }

    public void reInitialize() {

        this.initialize(null);
    }

    @Override
    public boolean isInitialized() {
        // TODO Auto-generated method stub
        return false;
    }

}
