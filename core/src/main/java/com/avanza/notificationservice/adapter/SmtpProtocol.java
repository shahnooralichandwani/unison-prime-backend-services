package com.avanza.notificationservice.adapter;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Map.Entry;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

//import org.apache.commons.codec.binary.Base64;

import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.configuration.ConfigurationCache;
import com.avanza.core.util.configuration.ConfigurationException;
import com.avanza.notificationservice.channel.ChannelCatalog;
import com.avanza.notificationservice.db.ChannelInfoImpl;
import com.avanza.notificationservice.notification.Notification;
import com.avanza.notificationservice.notification.NotificationCallbackHandler;
import com.avanza.notificationservice.notification.NotificationState;
import com.avanza.notificationservice.notification.RecipientInfo;

/**
 * TODO REFACTOR Code is giving Exception, need to fix it.
 */
public class SmtpProtocol extends NotificationAdapterBase implements
        ProtocolAdapter {

    private static Logger logger = Logger.getLogger(SmtpProtocol.class);
    private Properties props;
    private String user;
    private String password;

    private Authenticator getAuthenticator() {
        Authenticator authenticate =
                new javax.mail.Authenticator() {

                    @Override
                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(user, password);
                    }
                };
        return authenticate;
    }

    private ThreadLocal<Session> mailSession = new ThreadLocal<Session>() {
        protected Session initialValue() {
            Session session = Session.getInstance(props, getAuthenticator());
            return session;
        }
    };
    private ThreadLocal<Transport> transport = new ThreadLocal<Transport>() {
        protected Transport initialValue() {
            try {
                return mailSession.get().getTransport();
            } catch (NoSuchProviderException e) {
                logger.LogException("Unable to initialize Transport object", e);
            }

            return null;
        }

        ;

    };
    private String fromEmail;
    private String portNumber;
    private boolean initialized;

    public void initialize(ConfigSection protocolParams) {
        try {
            props = new Properties();

            password = protocolParams.getTextValue("password");
            user = protocolParams.getTextValue("user");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.host", protocolParams.getTextValue("host"));
            props.setProperty("mail.user", user);
            props.setProperty("mail.password", password);
            fromEmail = protocolParams.getTextValue("from");
            portNumber = protocolParams.getTextValue("port");
            props.put("mail.smtp.port", portNumber);
            props.setProperty("mail.transport.protocol", "smtp");
            transport.get().connect();
            super.setStatus(AdapterStatus.Running);

        } catch (Throwable e) {
            initialized = false;
            logger
                    .LogException(
                            "Exception while initializing the SMTPProtocol channel adapter",
                            e);
            super.setStatus(AdapterStatus.Down);
            throw new ConfigurationException(
                    "Exception while initializing the SMTPProtocol channel adapter");

        } finally {
            try {
                transport.get().close();
            } catch (MessagingException e) {
                logger.LogException("Unable to Close Transport object", e);
            }
        }
    }

    public void reInitialize() {
        try {
            if (getStatus() == AdapterStatus.Running) {
                return;
            }

            // Nasir: If adapter channel is not initialized properly then it
            // will initialize it again.
            if (!initialized) {
                ChannelInfoImpl channelInfo = (ChannelInfoImpl) ChannelCatalog
                        .getInstance().getChannelById("CBD-SMTP-CHANNEL");
                initialize(channelInfo.getConfigAdapterParams());
            }
            transport.get().connect();
            super.setStatus(AdapterStatus.Running);
        } catch (Exception e) {
            logger
                    .LogException(
                            "Exception while initializing the SMTPProtocol channel adapter",
                            e);
            super.setStatus(AdapterStatus.Down);
        } finally {
            try {
                transport.get().close();
            } catch (MessagingException e) {
                logger.LogException("Unable to Close Transport object", e);
            }
        }
    }

    private void onConnectionDown(Notification msg) {
        if (msg != null) {
            msg.setNotificationState(NotificationState.Wait);
            msg.setStatusText("Email server is Down");
            notificationUpdate.update(msg);
        }
    }

    public void notify(Notification msg) {

        // first try to connect transport object
        if (getStatus() != AdapterStatus.Down) {
            try {
                transport.get().connect();
            } catch (MessagingException e) {
                setStatus(AdapterStatus.Down);
                logger.LogException("Unable to Connect Transport object", e);
            }

        }
        // if server is down, mark the status of notificatin as Wait, so it can
        // be notified later.
        if (getStatus() == AdapterStatus.Down) {
            onConnectionDown(msg);
            return;
        }

        // now send email.
        try {
            MimeMessage message = new MimeMessage(mailSession.get());
            message.setSubject(msg.getSubject());
            message.setFrom(new InternetAddress(msg.getNsFrom()));
            for (RecipientInfo recipientinfo : msg.getRecipientTo()) {
                message.addRecipient(Message.RecipientType.TO,
                        new InternetAddress(recipientinfo.getAddress()));
            }
            for (RecipientInfo recipientinfo : msg.getRecipientCc()) {
                message.addRecipient(Message.RecipientType.CC,
                        new InternetAddress(recipientinfo.getAddress()));
            }
            for (RecipientInfo recipientinfo : msg.getRecipientBcc()) {
                message.addRecipient(Message.RecipientType.BCC,
                        new InternetAddress(recipientinfo.getAddress()));
            }
            //Sent date None in outlook for emails; fixed -- shoaib.rehman
            message.setSentDate(new Date());
            Multipart multipart = new MimeMultipart();
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(msg.getBody(),
                    "text/html;charset=cp1256;");
            multipart.addBodyPart(messageBodyPart);

            for (Entry<String, String> realPath : msg.getDocumentPaths()
                    .entrySet()) {
                File file = new File(realPath.getValue());
                if (file.exists()) {
                    messageBodyPart = new MimeBodyPart();
                    DataSource datasource = new FileDataSource(realPath
                            .getValue());
                    messageBodyPart.setDataHandler(new DataHandler(datasource));
                    // Arabic File Name is encoded as Base64 encoding scheme so
                    // that File Name is sent correctly
                    byte[] encoded = null;//Base64.encodeBase64(file.getName()
                    //.getBytes("cp1256"));
                    String encodedFileName = new String(encoded);
                    messageBodyPart.setFileName("=?windows-1256?b?"
                            + encodedFileName + "?=");

                    multipart.addBodyPart(messageBodyPart);
                } else {
                    logger
                            .logError(
                                    "Attachment File not found while sending Email Subject: %1$s.",
                                    msg.getSubject());
                }
            }
            message.setContent(multipart);

            // sending email through transport.
            try {


                transport.get().sendMessage(message, message.getAllRecipients());
                msg.setNotificationState(NotificationState.Sent);
                msg.setStatusText("Email Sent Successfully");

                /*
                 * Shahbaz Invoke Callback handler start
                 */
                try {
                    String callbackHandlerId = msg.getCallbackHanlder();
                    List<ConfigSection> callbackSections = ConfigurationCache
                            .getInstance().getConfigSection("NotificationCallback");

                    for (ConfigSection configSection : callbackSections) {
                        String id = configSection.getTextValue("id");
                        if (id != null
                                && id.equalsIgnoreCase(callbackHandlerId)) {
                            if (configSection.getTextValue("implClass") != null) {

                                Object obj = Class
                                        .forName(
                                                configSection
                                                        .getTextValue("implClass"))
                                        .newInstance();

                                if (obj instanceof NotificationCallbackHandler) {
                                    NotificationCallbackHandler refObj = (NotificationCallbackHandler) obj;
                                    refObj.handle(msg, NotificationState.Sent);
                                }

                            }
                        }

                    }
                } catch (Exception e) {
                    logger
                            .LogException(
                                    "Invoking notification callback handler failed while sending email",
                                    e);
                }
                // End
                notificationUpdate.update(msg);
            } catch (SendFailedException se) {
                logger.LogException("SendFailedException while sending email",
                        se);
                msg.setNotificationState(NotificationState.Failed);
                msg.setStatusText("SendFailedException while sending email");
                notificationUpdate.update(msg);
            } catch (MessagingException me) {
                logger.LogException(
                        "Exception while Connecting to email server", me);
                super.setStatus(AdapterStatus.Down);
                onConnectionDown(msg);

                msg.setNotificationState(NotificationState.Wait);
                msg.setStatusText("Exception while Connecting to email server");
                notificationUpdate.update(msg);
            } finally {
                try {
                    transport.get().close();
                } catch (MessagingException e) {
                    logger.LogException("Unable to Close Transport object", e);
                }
            }

        } catch (Exception ex) {
            logger.LogException("Exception while sending email", ex);
            msg.setNotificationState(NotificationState.Failed);
            msg.setStatusText("Exception while sending email");
            notificationUpdate.update(msg);
        } catch (Throwable e) {
            logger.LogException("Error while sending email", e);
            msg.setNotificationState(NotificationState.Failed);
            msg.setStatusText("Error while sending email");
            notificationUpdate.update(msg);
        } finally {

        }
    }

    public void dispose() {
        setStatus(AdapterStatus.Down);
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

}
