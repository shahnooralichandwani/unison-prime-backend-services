package com.avanza.notificationservice.adapter;

import java.util.List;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.Transport;

import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.configuration.ConfigurationCache;
import com.avanza.notificationservice.notification.Notification;
import com.avanza.notificationservice.notification.NotificationCallbackHandler;
import com.avanza.notificationservice.notification.NotificationState;

/**
 * TODO REFACTOR Code is giving Exception, need to fix it.
 */
public class FaxProtocol extends NotificationAdapterBase implements ProtocolAdapter {

    private static Logger logger = Logger.getLogger(FaxProtocol.class);
    private Properties props;
    private Session mailSession;
    private Transport transport;
    private String fromEmail;
    private String portNumber;

    public void initialize(ConfigSection protocolParams) {
        super.setStatus(AdapterStatus.Running);
    }

    public void reInitialize() {
        super.setStatus(AdapterStatus.Running);
    }

    public void notify(Notification msg) {

        msg.setNotificationState(NotificationState.Sent);
        msg.setStatusText("FAX Sent Successfully");
        notificationUpdate.update(msg);

        /*
         * Shahbaz
         * Invoke Callback handler
         * start
         */
        try {
            String callbackHandlerId = msg.getCallbackHanlder();
            List<ConfigSection> callbackSections = ConfigurationCache
                    .getInstance().getConfigSection("NotificationCallback");

            for (ConfigSection configSection : callbackSections) {
                String id = configSection.getTextValue("id");
                if (id != null && id.equalsIgnoreCase(callbackHandlerId)) {
                    if (configSection.getTextValue("implClass") != null) {

                        Object obj = Class.forName(
                                configSection.getTextValue("implClass"))
                                .newInstance();

                        if (obj instanceof NotificationCallbackHandler) {
                            NotificationCallbackHandler refObj = (NotificationCallbackHandler) obj;
                            refObj.handle(msg, NotificationState.Sent);
                        }

                    }
                }

            }
        } catch (Exception e) {
            logger
                    .LogException(
                            "Invoking notification callback handler failed while sending Fax",
                            e);
        }
        // End
    }

    public void dispose() {


    }

    @Override
    public boolean isInitialized() {
        // TODO Auto-generated method stub
        return false;
    }

}
