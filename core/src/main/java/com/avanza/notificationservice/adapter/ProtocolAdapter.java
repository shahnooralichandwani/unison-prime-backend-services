package com.avanza.notificationservice.adapter;

import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.notificationservice.channel.ChannelUpListener;
import com.avanza.notificationservice.notification.Notification;


public interface ProtocolAdapter {

    void initialize(ConfigSection protocolParams);

    void reInitialize();

    void notify(Notification notification);

    void setNotificationUpdate(NotificationUpdate update);

    void startChannelUpListener(ChannelUpListener listener, String adapterChannelId);

    void flush();

    void dispose();

    AdapterStatus getStatus();

    boolean isInitialized();
}