package com.avanza.notificationservice.adapter;

public enum AdapterStatus {
    Running, Down
}