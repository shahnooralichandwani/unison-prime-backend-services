package com.avanza.notificationservice.adapter;

import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.locks.LockSupport;

import com.avanza.core.util.Logger;
import com.avanza.notificationservice.notification.Notification;


// TODO REFACTOR Need to handle synchronization issues & to Review the Logic.
@Deprecated
public abstract class ThreadQueue<T> implements Runnable {

    private static final Logger logger = Logger.getLogger(ThreadQueue.class);
    private final String logIdentifier = "Developer-Logs: ";
    protected Queue<T> queue;
    protected Thread worker;
    private boolean isActive;
    private final int WAIT_TIME_OUT = 30000;
    private static final int INIT_CAPACITY = 50;
    protected Object _lock = new Object();

    public ThreadQueue() {
        isActive = false;
        queue = new PriorityQueue<T>(INIT_CAPACITY);
    }

    public ThreadQueue(int capacity) {
        isActive = false;
        queue = new PriorityQueue<T>(capacity);
    }

    protected abstract void processItem(T item);

    protected void onAddItem(List<T> itemList) {
        for (T singleItem : itemList) {
            if (singleItem instanceof Notification) {
                this.queue.add(singleItem);
            }
        }
    }


    protected void onAddItem(T item) {

        /*
         * PD: logging for debuggin notification halt issue
         */
        logger.logInfo(logIdentifier + "isActive is true and Adding item in the thread Queue");
        if (item instanceof Notification)
            logItemId((Notification) item, "onAddItem");
        //

        this.queue.add(item);
    }

    public void intialize() {
        logger.logDebug("[Initiallizing the ThreadQueue]");
        logger.logInfo("Last Updated: November 15 2011");

        synchronized (this) {
            if (!isActive) {
                worker = new Thread(this, ThreadQueue.class.getSimpleName() + Math.random());
                isActive = true;
                worker.start();
            }
        }
    }

    public void add(List<T> itemList) {
        logger.logInfo(logIdentifier + "Add function called of thread Queue");
        if (isActive) {
            synchronized (this._lock) {
                this._lock.notify();
            }

            synchronized (queue) {
                for (T singleItem : itemList) {
                    onAddItem(singleItem);
                }
            }

            ThreadState(worker);

            if (queue.size() > 0) {
                LockSupport.unpark(this.worker);
                logger.logInfo(logIdentifier + "add function called LockSupport.unpark() of the thread Queue");
                ThreadState(worker);
            }
        }
    }

    public void add(T item) {
        /*
         * PD: logging for debuggin notification halt issue
         */
        ThreadState(worker);
        logger.logInfo(logIdentifier + "Add function called of thread Queue");
        if (item instanceof Notification)
            logItemId((Notification) item, "add");
        //

        if (isActive) {
            synchronized (this._lock) {
                this._lock.notify();
            }
            synchronized (queue) {
                onAddItem(item);
            }
            ThreadState(worker);
            /*
             * Shahbaz
             * previously (queue.size() == 1)
             * Ajman notification thread block issue
             * Need to test this
             */
            if (queue.size() > 0) {
                LockSupport.unpark(this.worker);
                logger.logInfo(logIdentifier + "add function called LockSupport.unpark() of the thread Queue");
                ThreadState(worker);
            }
        }
    }


    @SuppressWarnings("deprecation")
    public void dispose() {
        try {
            /*
             * PD: logging for debuggin notification halt issue
             */
            logger.logInfo(logIdentifier + "Dispose function called of the thread Queue");
            //
            synchronized (this) {
                if (isActive) {
                    isActive = false;
                    LockSupport.unpark(this.worker);
                    worker.join(this.WAIT_TIME_OUT);
                    worker.destroy();
                }
            }
        } catch (Exception e) {
            logger.LogException("Exception while disposing ThreadQueue.", e);
        }
    }

    private void innerProcessQueue() {
        try {
            /*
             * PD: logging for debuggin notification halt issue
             */
            ThreadState(worker);
            logger.logInfo(logIdentifier + "innerProcessQueue function called of the thread Queue");
            //

            while (this.queue.size() > 0) {
                ThreadState(worker);
                logger.logInfo(logIdentifier + "this.queue.size() > 0");

                T item = null;
                synchronized (this.queue) {
                    item = queue.poll();
                    logger.logInfo(logIdentifier + "queue item polled ");
                    try {
                        this.processItem(item);
                    } catch (Exception e) {
                        logger.LogException("Exception while process Item in ThreadQueue.", e);
                    } catch (Error e) {
                        logger.LogException("Error while process Item in ThreadQueue.", e);
                    }
                }
            }
            if (this.queue.size() == 0) {
                logger.logInfo(logIdentifier + "before innerProcessQueue function called LockSupport.park() of the thread Queue");
                LockSupport.park();
                logger.logInfo(logIdentifier + "after innerProcessQueue function called LockSupport.park() of the thread Queue");
                ThreadState(worker);
            }
        } catch (Exception ex) {
            logger.LogException("Exception while processing inner queue.", ex);
        }
    }

    public void run() {
        try {
            /*
             * PD: logging for debuggin notification halt issue
             */
            logger.logInfo(logIdentifier + "run function called of the thread Queue");
            //

            while (this.isActive) {
                ThreadState(worker);
                logger.logInfo(logIdentifier + "isActive true in while loop");
                this.innerProcessQueue();
            }
            this.innerProcessQueue();
        } catch (Exception ex) {
            logger.LogException("Exception in the run method of ThreadQueue:", ex);
        }
    }

    /*
     * PD: function added to log for debugging notification halt issue
     */
    private void logItemId(Notification notification, String method) {
        logger.logInfo(logIdentifier + method + "::Notification Id:" + notification.getNotificationId());
    }

    private void ThreadState(Thread t) {
        //Enum e = t.getState();
        //logger.logInfo(logIdentifier+"  ::Thread State: "+ t.getName() + " is " + e.name());
    }

}
