package com.avanza.component.customer;

import java.util.Date;


public class RecentCustomer extends AccessedCustomer implements java.io.Serializable, Comparable {


    private Date lastSearched;

    // Constructors

    /**
     * default constructor
     */
    public RecentCustomer() {
    }


    public Date getLastSearched() {
        return lastSearched;
    }


    public void setLastSearched(Date lastSearched) {
        this.lastSearched = lastSearched;
    }


    public int compareTo(Object arg0) {

        RecentCustomer recentCustomer = (RecentCustomer) arg0;
        if (this.getLastSearched().after(recentCustomer.getLastSearched()))
            return -1;
        else if (this.getLastSearched().before(recentCustomer.getLastSearched()))
            return 1;

        return 0;
    }
}
