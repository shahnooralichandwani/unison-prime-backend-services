package com.avanza.component.calendar;

import com.avanza.core.CoreException;

public class CalendarTaskException extends CoreException {

    private static final long serialVersionUID = -295037078797890193L;

    public CalendarTaskException(String message) {

        super(message);
    }

    public CalendarTaskException(String format, Object... args) {

        super(format, args);
    }

    public CalendarTaskException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public CalendarTaskException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public CalendarTaskException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public CalendarTaskException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}
