package com.avanza.component.calendar;

public enum TaskType {

    General, InternalSystem;

    private static final String GENERAL = "general";
    private static final String INTERNAL_SYSTEM = "internalSystem";

    public static TaskType valueOf(int i) {

        if (i == 0)
            return General;
        if (i == 1)
            return InternalSystem;
        else return General;
    }

    public static TaskType fromString(String str) {

        if (str.equalsIgnoreCase(GENERAL))
            return General;
        if (str.equalsIgnoreCase(INTERNAL_SYSTEM))
            return InternalSystem;
        else return General;
    }
}
