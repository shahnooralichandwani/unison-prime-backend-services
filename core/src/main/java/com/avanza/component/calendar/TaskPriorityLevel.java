package com.avanza.component.calendar;


public enum TaskPriorityLevel {

    High(1), Medium(2), Low(3);

    private int intValue;

    private TaskPriorityLevel(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static TaskPriorityLevel fromInt(int value) {

        TaskPriorityLevel retVal = null;

        if (value == 1)
            retVal = TaskPriorityLevel.High;
        else if (value == 2)
            retVal = TaskPriorityLevel.Medium;
        else if (value == 3)
            retVal = TaskPriorityLevel.Low;
        else
            throw new CalendarTaskException("[%1$s] is not recognized as valid Task Priority Level Type", value);

        return retVal;
    }

}
