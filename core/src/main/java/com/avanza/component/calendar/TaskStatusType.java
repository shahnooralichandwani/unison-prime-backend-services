package com.avanza.component.calendar;


public enum TaskStatusType {

    NotStarted("NotStarted"), Started("Started"), Postponed("Postponed"), Finished("Finished");

    private String value;

    private TaskStatusType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static TaskStatusType fromString(String value) {

        TaskStatusType retVal = null;

        if (value.equalsIgnoreCase("NotStarted"))
            retVal = TaskStatusType.NotStarted;
        else if (value.equalsIgnoreCase("Started"))
            retVal = TaskStatusType.Started;
        else if (value.equalsIgnoreCase("Postponed"))
            retVal = TaskStatusType.Postponed;
        else if (value.equalsIgnoreCase("Finished"))
            retVal = TaskStatusType.Finished;
        else
            throw new CalendarTaskException("[%1$s] is not recognized as valid Task Status Type", value);

        return retVal;
    }

}