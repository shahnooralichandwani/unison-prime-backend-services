package com.avanza.component.calendar;
// Generated Mar 18, 2008 12:43:17 PM by Hibernate Tools 3.1.0.beta4

import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;


/**
 * TaskPriority generated by hbm2java
 */

public class TaskPriority extends DbObject implements java.io.Serializable {


    // Fields    

    private String priorityId;
    private TaskPriorityLevel priorityLevel;
    private String priorityPrm;
    private String prioritySec;
    private Set tasks = new HashSet(0);
    private boolean isDefault;


    // Constructors

    public boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * default constructor
     */
    public TaskPriority() {
    }

    /**
     * minimal constructor
     */
    public TaskPriority(String priorityId, TaskPriorityLevel priorityLevel, String priorityPrm) {
        this.priorityId = priorityId;
        this.priorityLevel = priorityLevel;
        this.priorityPrm = priorityPrm;
    }

    /**
     * full constructor
     */
    public TaskPriority(String priorityId, TaskPriorityLevel priorityLevel, String priorityPrm, String prioritySec, Set tasks) {
        this.priorityId = priorityId;
        this.priorityLevel = priorityLevel;
        this.priorityPrm = priorityPrm;
        this.prioritySec = prioritySec;
        this.tasks = tasks;
    }


    // Property accessors

    public String getPriorityId() {
        return this.priorityId;
    }

    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    public int getPriorityLevel() {
        return this.priorityLevel.getIntValue();
    }

    public void setPriorityLevel(int priorityLevel) {
        this.priorityLevel = TaskPriorityLevel.fromInt(priorityLevel);
    }

    public String getPriorityPrm() {
        return this.priorityPrm;
    }

    public void setPriorityPrm(String priorityPrm) {
        this.priorityPrm = priorityPrm;
    }

    public String getPrioritySec() {
        return this.prioritySec;
    }

    public void setPrioritySec(String prioritySec) {
        this.prioritySec = prioritySec;
    }

    public Set getTasks() {
        return this.tasks;
    }

    public void setTasks(Set tasks) {
        this.tasks = tasks;
    }

    public TaskPriorityLevel getTaskPriorityLevel() {
        return this.priorityLevel;
    }


}
