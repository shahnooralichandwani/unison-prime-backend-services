package com.avanza.component.messagecentre;

import java.io.Serializable;

import com.avanza.core.data.DbObject;


public class CustomerSegment extends DbObject implements Serializable, Comparable<CustomerSegment> {

    /**
     * @author Muhammad Ali
     */
    private static final long serialVersionUID = 6129254370548885408L;

    private String segmentCode;
    private String segmentName;
    private String otherLangName;
    private String mappingId;

    public String getSegmentCode() {
        return segmentCode;
    }

    public void setSegmentCode(String segmentCode) {
        this.segmentCode = segmentCode;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public String getOtherLangName() {
        return otherLangName;
    }

    public void setOtherLangName(String otherLangName) {
        this.otherLangName = otherLangName;
    }

    public String getMappingId() {
        return mappingId;
    }

    public void setMappingId(String mappingId) {
        this.mappingId = mappingId;
    }

    public int compareTo(CustomerSegment o) {
        return this.getSegmentName().compareTo(((CustomerSegment) o).getSegmentName());
    }
}
