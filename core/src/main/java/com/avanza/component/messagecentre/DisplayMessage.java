package com.avanza.component.messagecentre;
// Generated Apr 10, 2008 10:59:46 AM by Hibernate Tools 3.1.0.beta4

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;


/**
 * DisplayMessage generated by hbm2java
 */

public class DisplayMessage extends DbObject implements java.io.Serializable, Comparable {


    // Fields    

    private int messageId;
    private String messageText;
    private Date effectiveFrom;
    private Date effectiveTo;
    private boolean isGeneral;
    private boolean isCustomerSpecific;
    private boolean isTypeSpecific;
    private Set customerMessages = new HashSet(0);
    private Set customerTypeMessages = new HashSet(0);


    // Constructors

    /**
     * default constructor
     */
    public DisplayMessage() {
    }

    /**
     * minimal constructor
     */
    public DisplayMessage(int messageId, String messageText, Date effectiveFrom, Date effectiveTo, boolean isGeneral, boolean isCustomerSpecific, boolean isTypeSpecific) {
        this.messageId = messageId;
        this.messageText = messageText;
        this.effectiveFrom = effectiveFrom;
        this.effectiveTo = effectiveTo;
        this.isGeneral = isGeneral;
        this.isCustomerSpecific = isCustomerSpecific;
        this.isTypeSpecific = isTypeSpecific;
    }

    /**
     * full constructor
     */
    public DisplayMessage(int messageId, String messageText, Date effectiveFrom, Date effectiveTo, boolean isGeneral, boolean isCustomerSpecific, boolean isTypeSpecific, Set customerMessages, Set customerTypeMessages) {
        this.messageId = messageId;
        this.messageText = messageText;
        this.effectiveFrom = effectiveFrom;
        this.effectiveTo = effectiveTo;
        this.isGeneral = isGeneral;
        this.isCustomerSpecific = isCustomerSpecific;
        this.isTypeSpecific = isTypeSpecific;
        this.customerMessages = customerMessages;
        this.customerTypeMessages = customerTypeMessages;
    }


    // Property accessors

    public int getMessageId() {
        return this.messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessageText() {
        return this.messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public Date getEffectiveFrom() {
        return this.effectiveFrom;
    }

    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public Date getEffectiveTo() {
        return this.effectiveTo;
    }

    public void setEffectiveTo(Date effectiveTo) {
        this.effectiveTo = effectiveTo;
    }

    public boolean isIsGeneral() {
        return this.isGeneral;
    }

    public void setIsGeneral(boolean isGeneral) {
        this.isGeneral = isGeneral;
    }

    public boolean isIsCustomerSpecific() {
        return this.isCustomerSpecific;
    }

    public void setIsCustomerSpecific(boolean isCustomerSpecific) {
        this.isCustomerSpecific = isCustomerSpecific;
    }

    public boolean isIsTypeSpecific() {
        return this.isTypeSpecific;
    }

    public void setIsTypeSpecific(boolean isTypeSpecific) {
        this.isTypeSpecific = isTypeSpecific;
    }

    public Set getCustomerMessages() {
        return this.customerMessages;
    }

    public void setCustomerMessages(Set customerMessages) {
        this.customerMessages = customerMessages;
    }

    public Set getCustomerTypeMessages() {
        return this.customerTypeMessages;
    }

    public void setCustomerTypeMessages(Set customerTypeMessages) {
        this.customerTypeMessages = customerTypeMessages;
    }

    public int compareTo(Object arg0) {

        return this.getMessageText().compareTo(((DisplayMessage) arg0).getMessageText());
    }


}
