package com.avanza.unison.admin.template;


public class TemplateData {

    private String key;
    private String property;
    private String displayName;
    private String majorType;

    public TemplateData(String key, String property, String displayName) {
        this.key = key;
        this.property = property;
        this.displayName = displayName;
    }

    public TemplateData(String majorType, String key, String property, String displayName) {
        this(key, property, displayName);
        this.majorType = majorType;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String toString() {
        return "${" + key + "." + property + "} = " + displayName;
    }

    public String getMajorType() {
        return majorType;
    }

    public void setMajorType(String majorType) {
        this.majorType = majorType;
    }
}
