/**
 *
 */
package com.avanza.unison.admin.template;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.meta.MetaEntity;

/**
 * @author rehan.ahmed
 *
 */
public class DocumentTemplateRelation extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 4096383657368485886L;
    private int relationId;
    //private Set<NsTemplate> listTemplate = new HashSet<NsTemplate>();
    private String templateId;
    private String metaEntId;
    private Set<TemplateParameter> customParam = new HashSet<TemplateParameter>();
    private boolean isDefault;


    public int getRelationId() {
        return relationId;
    }

    public void setRelationId(int relationId) {
        this.relationId = relationId;
    }

    public String getMetaEntId() {
        return metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

    public Set<TemplateParameter> getCustomParam() {
        return customParam;
    }

    public void setCustomParam(Set<TemplateParameter> customParam) {
        this.customParam = customParam;
    }

    //	public void setListTemplate(Set<NsTemplate> listTemplate) {
//		this.listTemplate = listTemplate;
//	}
//	public Set<NsTemplate> getListTemplate() {
//		return listTemplate;
//	}
    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public boolean getIsDefault() {
        return isDefault;
    }

}
