package com.avanza.unison.admin.template;

import java.util.List;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.TreeObject;

public class TemplateTreeNode extends TreeObject {

    private static final long serialVersionUID = 6243244467879902126L;

    private String primaryName;
    private String secondaryName;
    private String value;
    private TemplateTreeNode parentNode;

    public TemplateTreeNode() {
        super();
    }

    public TemplateTreeNode(String id) {
        super(id);
    }

    public TemplateTreeNode(String id, List<TreeObject> childList) {
        super(id, childList);
    }

    @Override
    public TreeObject clone() {
        TemplateTreeNode templateTreeNode = new TemplateTreeNode(this.id);
        templateTreeNode.copyValues(this);

        //Now need to copy their children too
        for (TreeObject tobj : this.childList) {
            TreeObject cloned = ((TemplateTreeNode) tobj).clone();
            ((TemplateTreeNode) cloned).parentNode = templateTreeNode;
            templateTreeNode.childList.add(cloned);
        }
        return templateTreeNode;
    }

    @Override
    public void copyValues(DbObject copyFrom) {
        TemplateTreeNode templateTreeNode = (TemplateTreeNode) copyFrom;
        super.copyValues(copyFrom);

        this.primaryName = templateTreeNode.primaryName;
        this.secondaryName = templateTreeNode.secondaryName;
    }

    @Override
    protected TreeObject createObject(String id) {
        return new TemplateTreeNode(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TemplateTreeNode) {
            TemplateTreeNode templateTreeNode = (TemplateTreeNode) obj;
            if (templateTreeNode.getId().equalsIgnoreCase(this.getId()) && templateTreeNode.getKey().equalsIgnoreCase(this.getKey()))
                return true;
        }
        return false;
    }

    @Override
    public <T extends TreeObject> void add(T item) {
        super.add(item);
        ((TemplateTreeNode) item).parentNode = this;
    }

    @Override
    public String getPrimaryName() {
        return primaryName;
    }

    @Override
    public String getSecondaryName() {
        return secondaryName;
    }

    @Override
    public String getValue() {
        return value;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public void setSecondaryName(String secondaryName) {
        this.secondaryName = secondaryName;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public TemplateTreeNode getParentNode() {
        return parentNode;
    }

    public void setParentNode(TemplateTreeNode parentNode) {
        this.parentNode = parentNode;
    }
}
