/**
 *
 */
package com.avanza.unison.admin.template;

import com.avanza.core.data.DbObject;

/**
 * @author rehan.ahmed
 *
 */
public class TemplateParameter extends DbObject {

    private int relationId;
    private String fieldExpression;

    public int getRelationId() {
        return relationId;
    }

    public void setRelationId(int relationId) {
        this.relationId = relationId;
    }

    public String getFieldExpression() {
        return fieldExpression;
    }

    public void setFieldExpression(String fieldExpression) {
        this.fieldExpression = fieldExpression;
    }


}
