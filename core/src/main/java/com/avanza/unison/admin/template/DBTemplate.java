package com.avanza.unison.admin.template;

import java.util.Iterator;
import java.util.List;

import com.avanza.core.CoreException;
import com.avanza.core.constants.TemplateType;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Order;
import com.avanza.core.data.expression.OrderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.meta.MetaCounter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.notificationservice.integration.NotificationConfig;

public class DBTemplate {

    private static Logger logger = Logger.getLogger(DBTemplate.class);

    private DataBroker broker;
    private static DBTemplate _instance;

    protected DBTemplate() {
        broker = DataRepository.getBroker(DBTemplate.class.getName());
    }

    public static DBTemplate getInstance() {

        if (_instance == null) {
            _instance = new DBTemplate();
        }

        return _instance;
    }

    public List<NsTemplate> getAllTemplate(OrderType orderType, TemplateOrderByMode... orderByMode) {

        Search search = new Search(NsTemplate.class);
        for (int i = 0; i < orderByMode.length; i++)
            search.addOrder(new Order(orderByMode[i].toString(), orderType));

        return this.broker.find(search);
    }

    public void setAllTemplate(List<NsTemplate> templateList) {

        Iterator<NsTemplate> iterator = templateList.iterator();
        while (iterator.hasNext()) {
            NsTemplate template = iterator.next();

            persistTemplate(template);
        }
    }

    public void deleteSelectedTemplate(List<NsTemplate> templateList) {

        Iterator<NsTemplate> iterator = templateList.iterator();
        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        txContext.beginTransaction();
        try {
            while (iterator.hasNext()) {
                NsTemplate template = iterator.next();

                if (template.isSelected())
                    this.broker.delete(template);
            }
            txContext.commit();

        } catch (Exception e) {
            logger.LogException("Exception occured while DBTemplate.deleteSelectedTemplate(List<NsTemplate> templateList)", e);
            txContext.rollback();
            throw new CoreException("Exception occured while Deleting Template", e);

        }
    }

    public List<NsTemplate> getTemplateBy(Search search) {
        return this.broker.find(search);
    }

    public NsTemplate getTemplate(String templateId) {
        return this.broker.findById(NsTemplate.class, templateId);
    }

    public NsTemplate getDefaultTemplate() {
        Search search = new Search();
        search.addFrom(NsTemplate.class);
        search.addCriterion(Criterion.equal("templateType", TemplateType.EMail.toString()));
        List<NsTemplate> retVal = getTemplateBy(search);
        return retVal.size() > 0 ? retVal.get(0) : null;
    }

    public void persistTemplate(NsTemplate template) {

        if (StringHelper.isEmpty(template.getTemplateId()))
            template.setTemplateId((String) MetaDataRegistry.getNextCounterValue(MetaCounter.TEMPLATE_COUNTER));

        this.broker.persist(template);
    }

    public void persistNotificationConfig(NotificationConfig notificationConfig) {

        if (StringHelper.isEmpty(notificationConfig.getId()))
            notificationConfig.setId((String) MetaDataRegistry.getNextCounterValue(MetaCounter.NOTIFICATION_CONFIG_COUNTER));

        this.broker.persist(notificationConfig);
    }

    public List<NotificationConfig> getNotificationConfigByTemplateId(String templateId) {
        Search search = new Search(NotificationConfig.class);
        search.addCriterion(Criterion.equal("templateId", templateId));

        return this.broker.find(search);
    }

    public void deleteNotificationConfig(NotificationConfig notificationConfig) {
        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        txContext.beginTransaction();
        try {
            broker.delete(notificationConfig);
            txContext.commit();
        } catch (Exception e) {
            logger.LogException("Exception occured while DBTemplate.deleteNotificationConfig(NotificationConfig notificationConfig)", e);
            txContext.rollback();
            throw new CoreException("Exception occured while Deleting Call Notification Config", e);
        }
    }

    public NotificationConfig getNotificationConfig(String templateId, String documentId, String channelId) {
        Search search = new Search(NotificationConfig.class);
        search.addCriterion(Criterion.equal("templateId", templateId));
        search.addCriterion(Criterion.equal("documentId", documentId));
        search.addCriterion(Criterion.equal("channelId", channelId));
        List<NotificationConfig> list = this.broker.find(search);
        if (list.size() > 0)
            return list.get(0);
        return null;
    }

    public List<NsTemplate> getEmailFaxTemplates(OrderType asc, TemplateOrderByMode templatetype, TemplateOrderByMode templatename) {
        Search search = new Search();
        search.addFrom(NsTemplate.class);
        search.addCriterion(Criterion.equal("templateType", TemplateType.EMail.toString()));
        search.addCriterionOr(Criterion.equal("templateType", TemplateType.Fax.toString()));
        List<NsTemplate> retVal = getTemplateBy(search);

        return retVal;
    }

    public List<NsTemplate> getTemlateByChannel(String templateChannelId) {
        Search search = new Search();
        search.addFrom(NsTemplate.class);
        search.addCriterion(Criterion.equal("templateType", templateChannelId));
        List<NsTemplate> retVal = getTemplateBy(search);

        return retVal;
    }

}
