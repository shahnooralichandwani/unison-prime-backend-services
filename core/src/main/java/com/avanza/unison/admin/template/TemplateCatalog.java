package com.avanza.unison.admin.template;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.util.Logger;
import com.avanza.core.util.TimeLogger;

public class TemplateCatalog {

    private static final Logger logger = Logger
            .getLogger(TemplateCatalog.class);
    public static TemplateTreeNode templateTreeNode;
    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock writeLock = readWriteLock.writeLock();
    private static Lock readLock = readWriteLock.readLock();

    public static void load() {
        try {
            writeLock.lock();
            TimeLogger timmer = TimeLogger.init();
            timmer.begin(TemplateCatalog.class, "load()");

            if (templateTreeNode == null) {
                templateTreeNode = loadTemplateTree();
                templateTreeNode.add(loadSystemEntityTemplateData());
            }
            timmer.end();
        } finally {
            writeLock.unlock();
        }
    }

    private static TemplateTreeNode loadTemplateTree() {
        logger.logInfo("####### Template Tree Loading InProgress #######");
        MetaDataRegistry.load();
        Collection<MetaEntity> metaEntity = MetaDataRegistry.getEntityList().values();

        TemplateTreeNode rootNode = new TemplateTreeNode("Root");
        rootNode.setValue("Meta Entities");
        TemplateTreeNode majorTypeNode = null;
        TemplateTreeNode entityNode = null;
        String majorType = null;
        Iterator<MetaEntity> itrEntity = metaEntity.iterator();

        while (itrEntity.hasNext()) {
            MetaEntity entity = itrEntity.next();
            if (entity.isAbstract() || !entity.isTemplateEnabled()) continue;

            if (!entity.getMajorType().equals(majorType)) {
                majorType = entity.getMajorType();
                majorTypeNode = rootNode.getChild(majorType);
                if (majorTypeNode == null) {
                    majorTypeNode = new TemplateTreeNode(majorType);
                    rootNode.add(majorTypeNode);
                }
                majorTypeNode.setValue(majorType);
            }
            entityNode = majorTypeNode.getChild(entity.getPrimaryName());
            if (entityNode == null) {
                entityNode = new TemplateTreeNode(entity.getPrimaryName());
                entityNode.setValue(entity.getPrimaryName());
                entityNode.setPrimaryName(entity.getSystemName());
                entityNode.setSecondaryName(entity.getSecondaryName());
                majorTypeNode.add(entityNode);
            }

            Collection<MetaAttribute> metaAttribute = entity.getAttributeList(true).values();
            List<MetaAttribute> list = new ArrayList<MetaAttribute>(0);
            list.addAll(metaAttribute);
            Collections.sort(list);
            Iterator<MetaAttribute> itrAttribute = list.iterator();

            while (itrAttribute.hasNext()) {
                MetaAttribute attribute = itrAttribute.next();
                TemplateTreeNode templateNode = new TemplateTreeNode(attribute.getId());
                templateNode.setPrimaryName(attribute.getSystemName());
                templateNode.setValue(attribute.getDisplayName());
                entityNode.add(templateNode);
            }
        }
        logger.logInfo("####### Template Tree Loading Completed #######");

        return rootNode;
    }

    private static TemplateTreeNode loadSystemEntityTemplateData() {
        /*
         * CURRENT_CUSTOMER_KEY CURRENT_CUSTOMER_ID_KEY CurrentlyloggedinUser
         * CURRENT_USER_KEY CurrentLocale CUSTOMER_SESSION_HISTORY
         * CUSTOMER_SESSION_HISTORY_DETAIL CURRENT_INTERFACE_CHANNEL
         * ACTIVITY_LOG1_KEY ACTIVITY_LOG_DETAIL_KEY
         */
        TemplateTreeNode root = new TemplateTreeNode("System");
        root.setValue("System");
        TemplateTreeNode child;
        TemplateTreeNode leaf;
        child = new TemplateTreeNode("User");
        child.setValue("User");
        root.add(child);
        leaf = new TemplateTreeNode("loginId");
        leaf.setValue("Login Id");
        child.add(leaf);
        leaf = new TemplateTreeNode("fullName");
        leaf.setValue("Full Name");
        child.add(leaf);

        return root;
    }

    public static TemplateTreeNode getTemplateTreeNode() {
        try {
            readLock.lock();
            return templateTreeNode;
        } finally {
            readLock.unlock();
        }
    }
}
