package com.avanza.unison.admin.template;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.core.constants.TemplateType;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.config.WebContext;

public class TemplateFactory {

    private static Logger logger = Logger.getLogger(TemplateFactory.class);

    //Generates the Template url for the given Template
    private static AttachmentModel createTemplateUrl(NsTemplate template, String processedTemplate, String processedSubject, String processedHeader) {

        if (template.getTemplateType().equalsIgnoreCase(TemplateType.Attachment.toString()) && StringHelper.isNotEmpty(template.getParentId())) {
            if (StringHelper.isNotEmpty(template.getGeneratorClass())) {
                try {
                    Class<?> generatorClass = Class.forName(template.getGeneratorClass());
                    TemplateGenerator templateGenerator = (TemplateGenerator) generatorClass.newInstance();
                    Map dataMap = new HashMap();
                    dataMap.put("template", template);
                    dataMap.put("processedTemplate", processedTemplate);
                    dataMap.put("processedSubject", processedSubject);
                    dataMap.put("processedHeader", processedHeader);
                    AttachmentModel model = (AttachmentModel) templateGenerator.processTemplate(dataMap);
                    return model;
                } catch (ClassNotFoundException e) {
                    logger.LogException("Cannot generate the template for %s as the generator Class is not found.", e, template.getTemplateId());
                } catch (InstantiationException e) {
                    logger.LogException("Cannot generate the template for %s as the generator Class is not instantiated.", e, template.getTemplateId());
                } catch (IllegalAccessException e) {
                    logger.LogException("Cannot generate the template for %s as the generator Class is not accessed.", e, template.getTemplateId());
                } catch (Throwable t) {
                    logger.LogException("Cannot generate the template for %s due to unknown exception..", t, template.getTemplateId());
                }
            } else {
                logger.logError("Cannot generate the template for %s as the generator Class is empty.", template.getTemplateId());
            }
        } else {
            logger.logError("Cannot generate the Template Attachment for %s bcz its not of Attachment Type or not a child template.", template.getTemplateId());
        }
        return null;
    }

    public static List<AttachmentModel> generateAttachments(NsTemplate template) {

        List<AttachmentModel> attachmentUrls = new ArrayList<AttachmentModel>();
        String processedTemplate = "";
        String processedSubject = "";
        String processedHeader = "";
        if (!template.getTemplateType().equalsIgnoreCase(TemplateType.Attachment.toString())) {
            WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
            if (ctx != null)
                for (NsTemplate attachment : template.getAttachments()) {
                    HashMap map = ctx.getAttribute("DATA_MAP_FOR_TEMPLATE");
                    processedTemplate = ApplicationLoader.getTemplateConfig().processTemplate(attachment.getTemplateId() + "_content", map);
                    processedSubject = ApplicationLoader.getTemplateConfig().processTemplate(attachment.getTemplateId() + "_subject", map);
                    processedHeader = ApplicationLoader.getTemplateConfig().processTemplate(attachment.getTemplateId() + "_header", map);
                    AttachmentModel model = createTemplateUrl(attachment, processedTemplate, processedSubject, processedHeader);
                    if (model != null)
                        attachmentUrls.add(model);
                }
        } else {
            logger.logError("Cannot generate the Template Attachment for %s bcz the parent template is of Attachment Type.", template.getTemplateId());
        }

        return attachmentUrls;
    }
}
