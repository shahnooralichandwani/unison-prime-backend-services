package com.avanza.unison.admin.template;

public enum TemplateOrderByMode {
    templateId("templateId"), templateName("templateName"), templateType("templateType"), createdOn(
            "createdOn"), updatedOn("updatedOn");

    private String value;

    private TemplateOrderByMode(String value) {
        this.value = value;
    }

    public String toString() {

        return this.value;
    }
}
