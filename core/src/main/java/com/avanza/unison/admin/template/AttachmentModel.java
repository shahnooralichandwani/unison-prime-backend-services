package com.avanza.unison.admin.template;

import java.io.Serializable;


public class AttachmentModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6684122012774790881L;

    private String fileName;

    private String filePath;

    private double fileSize;

    private String mimeType;

    private String realPath;

    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }

    public AttachmentModel(String fileName, String filePath, double filesize, String mimeType) {
        this.fileName = fileName;
        this.filePath = filePath;
        this.fileSize = filesize;
        this.mimeType = mimeType;
    }

    public String getFileName() {
        return fileName;
    }


    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public String getFilePath() {
        return filePath;
    }


    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public double getFileSize() {
        return fileSize;
    }


    public void setFileSize(double fileSize) {
        this.fileSize = fileSize;
    }


    public String getMimeType() {
        return mimeType;
    }


    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }


}
