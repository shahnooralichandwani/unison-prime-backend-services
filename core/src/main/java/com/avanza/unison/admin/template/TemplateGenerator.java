package com.avanza.unison.admin.template;

public interface TemplateGenerator {
    Object processTemplate(Object arg);
}
