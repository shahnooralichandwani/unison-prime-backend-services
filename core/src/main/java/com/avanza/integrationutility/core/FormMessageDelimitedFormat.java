/**
 *
 */
package com.avanza.integrationutility.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.avanza.core.cache.CustomerSessionCacheUtil;
import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.meta.MetaAttributeImpl;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaRelationship;
import com.avanza.core.meta.messaging.Message;
import com.avanza.core.meta.messaging.MessageAction;
import com.avanza.core.meta.messaging.MessageAttribute;
import com.avanza.core.meta.messaging.MessageAttributeType;
import com.avanza.core.meta.messaging.MessageAttributeTypeInterface;
import com.avanza.core.meta.messaging.MetaMessageCatalog;
import com.avanza.core.meta.messaging.enums.MessageAttributeTypeKeys;
import com.avanza.core.meta.messaging.enums.MessagePropertiesKeys;
import com.avanza.core.meta.messaging.exceptions.AttributeTypeNotFoundException;
import com.avanza.core.meta.messaging.exceptions.DelimiterNotFoundException;
import com.avanza.core.meta.messaging.exceptions.MessageNotFoundException;
import com.avanza.core.meta.messaging.exceptions.MetaAttributeAssociationException;
import com.avanza.core.meta.messaging.exceptions.MetaRelationNotFoundException;
import com.avanza.core.meta.messaging.exceptions.TranIDAttribNotFoundException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.DataObjectCollection;
import com.avanza.core.util.Logger;
import com.avanza.core.util.MessageParser;
import com.avanza.core.util.StringHelper;


/**
 * @author shahbaz.ali
 * This class will serve the meta message integration to serialize and deserialize the detail view message 
 * on the basis of metaEntId and instance id passed as an argument in the constructor. 
 *
 */
public class FormMessageDelimitedFormat implements MessageFormat<String, Object> {

    private String responseMsg;
    DataObject dataObject;
    private String transactionId;
    private final Logger logger = Logger.getLogger(FormMessageDelimitedFormat.class);

    public FormMessageDelimitedFormat(DataObject object) {
        this.dataObject = object;

    }


    @Override
    public void generateTransactionId() throws Exception {

        // get attribute type of transaction id of request message
        // if not found throws exception
        // create its instance and call to ganerate transition id
        logger.logInfo("Generating TransactionId.... [DetailMessageDelimitedFormat.generateTransactionId ()]");
        MessageAttribute tranAttribute = MetaMessageCatalog.getTransactionIdAttribute(dataObject.getMetaEntity().getSystemName(), MessageAction.TransactionRequest);
        if (tranAttribute == null || (StringHelper.isEmpty(tranAttribute.getMessageAttributeType().getDefaultValueClass()))) {
            throw new TranIDAttribNotFoundException(String.format("Transaction Id Message Attribute not found for message of Meta Entity %s", dataObject.getMetaEntity().getId()));
        } else {

            Object newObj = Class.forName(tranAttribute.getMessageAttributeType().getDefaultValueClass()).newInstance();


            if (newObj instanceof MessageAttributeTypeInterface) {
                MessageAttributeTypeInterface typeObj = (MessageAttributeTypeInterface) newObj;
                transactionId = (String) typeObj.getDefaultValue();
            }
        }
    }


    @Override
    public String getResponse() {
        return this.responseMsg;
    }


    @Override
    public String getTransactionCode() {

        return null;
    }


    @Override
    public String getTransactionId() {
        return transactionId;
    }


    @Override
    public String serialize() throws Exception {

        /*	  Shahbaz
         */

        logger.logInfo("Serializing Message .... [FormMessageDelimitedFormat.serialize()]");
        Message message = MetaMessageCatalog.getMessage(dataObject.getMetaEntity().getSystemName(), MessageAction.TransactionRequest);

        if (message != null) {
            StringBuilder requestMessage = new StringBuilder();
            HashMap<String, String> messageParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(message.getMessageParams(), StringHelper.COMMA);
            String delimiter = (String) messageParamMap.get(MessagePropertiesKeys.delimiter.toString());

            if (StringHelper.isNotEmpty(delimiter)) {

                for (MessageAttribute messageAttribute : message.getSortedAttributes()) {

                    String attributeValue = getAttributeValue(messageAttribute);
                    requestMessage.append(attributeValue);
                    requestMessage.append(delimiter);
                }
            } else {
                throw new DelimiterNotFoundException(String.format("Delimiter not found as the property of the Message, MessageId: %s", message.getMessageId()));
            }


            return requestMessage.toString();
        } else {
            throw new MessageNotFoundException(String.format("Message not found for the meta entity %s", dataObject.getMetaEntity().getId()));
        }
    }


    @Override
    public Object deserialize() throws Exception {

        logger.logInfo("Deserializing Message .... [FormMessageDelimitedFormat.deserialize()]");
        Message message = MetaMessageCatalog.getMessage(dataObject.getMetaEntity().getSystemName(), MessageAction.TransactionResponse);

        if (message != null) {

            HashMap messageParamMap = (HashMap) StringHelper.getPropertiesMapFrom(message.getMessageParams(), StringHelper.COMMA);
// 		   	    
//		    if(message.hasSubMessage())			
            return parseListResponse(message, messageParamMap);
//		    else
//			return parseDetailResponse(message,messageParamMap);

        } else {
            throw new MessageNotFoundException(String.format("Message not found for the meta entity %s", dataObject.getMetaEntity().getId()));
        }

    }


//	private Object parseDetailResponse(Message message, HashMap messageParamMap ) throws DelimiterNotFoundException {
//	    
//	    String responseCode=null;
//	    MessageResponseObject responseObject= new MessageResponseObject();
//	    
//	    String msgdelimiter= (String) messageParamMap.get(MessagePropertiesKeys.delimiter.toString());
//		if(StringHelper.isNotEmpty(msgdelimiter)){
//
//			MessageParser messageParser= new MessageParser(getResponse());
//
//			for(MessageAttribute messageAttribute: message.getSortedAttributes()){
//
//			    if(messageAttribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.RESPONSE_CODE.toString())){
//				responseCode= messageParser.getNext(msgdelimiter);
//				responseObject.setResponseCode(responseCode);
//			    }
//			    else{
//				MetaAttributeImpl  metaAttrib= messageAttribute.getMetaEntityAttrib();
//				if(metaAttrib!=null){
//				    String value= messageParser.getNext(msgdelimiter);
//				    String metaAttribSysName= metaAttrib.getSystemName();
//				    dataObject.setValue(metaAttribSysName, value);
//				}
//				else{
//				    String value= messageParser.getNext(msgdelimiter);
//				}
//			    }
//			    
//			}
//			responseObject.setResponseObject(dataObject);
//			return responseObject;
//		}else{
//			throw new DelimiterNotFoundException(String.format("Delimiter not found as the property of the Sub Message, MessageId: %s", message.getMessageId()));
//		}
//	}

    private Object parseListResponse(Message message, HashMap messageParamMap) throws DelimiterNotFoundException, MetaAttributeAssociationException, MetaRelationNotFoundException {
        logger.logInfo("Deserializing Message .... [ListMessageDelimitedFormat.deserialize()]");

        List<DataObject> dataObjectList = new ArrayList<DataObject>();
        MessageResponseObject responseObject = new MessageResponseObject();
        String responseCode = null;

        String msgdelimiter = (String) messageParamMap.get(MessagePropertiesKeys.delimiter.toString());

        if (StringHelper.isNotEmpty(msgdelimiter)) {

            MessageParser messageParser = new MessageParser(getResponse());

            for (MessageAttribute messageAttribute : message.getSortedAttributes()) {

                if (messageAttribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.RESPONSE_CODE.toString())) {
                    responseCode = messageParser.getNext(msgdelimiter);
                    responseObject.setResponseCode(responseCode);
                } else {
                    Message subMessage = messageAttribute.getSubMessage();

                    if (subMessage != null) {
                        dataObjectList = handleSubMessage(subMessage, messageParser, msgdelimiter);
                        String subEntityId = subMessage.getMetaEntity().getId();
                        DataObjectCollection dataObjectCollection = new DataObjectCollection(subEntityId, dataObjectList, dataObject);

                        // here we need to verify if the collection entitiy have relation
                        // with the main transaction entity
                        List<MetaRelationship> listRelations = this.dataObject.getMetaEntity().getRelationShipBySubEntity(subEntityId);

                        if (listRelations.size() == 0) {
                            throw new MetaRelationNotFoundException(String.format("Meta Relation not found, Main Entity Id: %s, Sub Entity Id: %s", this.dataObject.getMetaEntity().getId(), subEntityId));
                        } else {
                            MetaRelationship rel = listRelations.get(0);
                            dataObject.addDataObjectCollection(rel.getRelationshipId(), dataObjectCollection);
                        }
                    } else {
                        MetaAttributeImpl metaAttrib = messageAttribute.getMetaEntityAttrib();

                        if (metaAttrib != null) {
                            String value = messageParser.getNext(msgdelimiter);
                            String metaAttribSysName = metaAttrib.getSystemName();
                            dataObject.setValue(metaAttribSysName, value);
                        } else {
                            messageParser.getNext(msgdelimiter);
                        }
                    }
                }

            }
        } else {
            throw new DelimiterNotFoundException(String.format("Delimiter not found as the property of the Sub Message, MessageId: %s", message.getMessageId()));
        }
        dataObject.setValue("TRANSACTION_RESPONSE", "true");


        /*
         * Start
         * Update cache with updated response dataobject to be used in
         * Transaction cOntroller for View rendering.
         */
        CustomerSessionCacheUtil.updateCache(dataObject);
        /*
         * End
         */
        responseObject.setResponseObject(dataObject);
        return responseObject;
    }

    private List<DataObject> handleSubMessage(Message subMessage, MessageParser messageParser, String msgdelimiter) throws DelimiterNotFoundException, MetaAttributeAssociationException {

        String blockSizeString = messageParser.getNext(msgdelimiter);
        int blockSize = Integer.parseInt(blockSizeString);
        List<DataObject> responseList = new ArrayList<DataObject>(blockSize);
        HashMap subMessageParamMap = (HashMap) StringHelper.getPropertiesMapFrom(subMessage.getMessageParams(), StringHelper.COMMA);
        String subMsgdelimiter = (String) subMessageParamMap.get(MessagePropertiesKeys.delimiter.toString());

        if (StringHelper.isNotEmpty(subMsgdelimiter)) {

            for (int i = 0; i < blockSize; i++) {
                DataObject messageDataObject = MetaDataRegistry.getMetaEntity(subMessage.getMetaEntity().getId()).createEntity();

                for (MessageAttribute subMessageAttribute : subMessage.getSortedAttributes()) {
                    String value = messageParser.getNext(subMsgdelimiter);
                    MetaAttributeImpl metaAttrib = subMessageAttribute.getMetaEntityAttrib();
                    if (metaAttrib != null) {
                        String metaAttribSysName = metaAttrib.getSystemName();
                        messageDataObject.setValue(metaAttribSysName, value);
                    } else {
                        throw new MetaAttributeAssociationException(String.format("Message Attribute %s not associated with Meta Entity Attribute as expected", subMessageAttribute.getMessageAttributeId()));
                    }
                }
                responseList.add(messageDataObject);
            }

        } else {
            throw new DelimiterNotFoundException(String.format("Delimiter not found as the property of the Sub Message, MessageId: %s", subMessage.getMessageId()));
        }

        return responseList;

    }

    @Override
    public void setResponse(String res) {
        this.responseMsg = res;

    }

    private String getAttributeValue(MessageAttribute messageAttribute) throws AttributeTypeNotFoundException {

        String attributeValue = "";
        MessageAttributeType messageAttribType = messageAttribute.getMessageAttributeType();

        if (messageAttribType != null) {

            // if message attribute is of transaction Id, set transaction id to its value
            if (messageAttribType.getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.TRANSACTION_ID.toString())) {
                attributeValue = getTransactionId();
            }

            // get value from dataobject if message attribute is mapped to entity attribute
            else if (messageAttribType.getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.ENTITY_MAPPED.toString())) {
                attributeValue = dataObject.getAsString(messageAttribute.getMetaEntityAttrib().getSystemName());
            } else {
                String implClass = messageAttribType.getDefaultValueClass();

                // if any default value class found, get default value from it
                if (implClass != null) {
                    Object newObj = null;
                    try {
                        newObj = Class.forName(implClass).newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (newObj != null && newObj instanceof MessageAttributeTypeInterface) {
                        MessageAttributeTypeInterface formatter = (MessageAttributeTypeInterface) newObj;
                        attributeValue = formatter.getDefaultValue().toString();
                    }
                }

                // get the value from FEL expression
                else {
                    String expression = messageAttribute.getValueExpression();

                    if (!StringHelper.isEmpty(expression)) {
                        attributeValue = (String) ExpressionHelper.getExpressionResult(expression, String.class);
                    }

                }
            }

        } else {
            throw new AttributeTypeNotFoundException(String.format("Message Attribute type undefined, Message Attribute Id: %s", messageAttribute.getMessageAttributeId()));
        }


        return attributeValue;
    }

}
