/**
 *
 */
package com.avanza.integrationutility.core;

import com.avanza.core.meta.messaging.exceptions.InvalidAttributeLengthException;

/**
 * @author shahbaz.ali
 *
 */
public class MessageUtility {


    public static String cushionField(String fieldValue, int fieldSize, char cushionChar) throws InvalidAttributeLengthException {
        if (fieldValue.length() == fieldSize) {
            return fieldValue;
        } else if (fieldValue.length() < fieldSize) {
            while (fieldValue.length() != fieldSize) {
                fieldValue = cushionChar + fieldValue;
            }
            return fieldValue;
        } else {
            throw new InvalidAttributeLengthException(String.format("Invalid message attribute length found, Length defined %d and found %d", fieldSize, fieldValue.length()));
        }

    }

}
