/**
 *
 */
package com.avanza.integrationutility.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.meta.MetaAttributeImpl;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.messaging.Message;
import com.avanza.core.meta.messaging.MessageAction;
import com.avanza.core.meta.messaging.MessageAttribute;
import com.avanza.core.meta.messaging.MessageAttributeType;
import com.avanza.core.meta.messaging.MessageAttributeTypeInterface;
import com.avanza.core.meta.messaging.MetaMessageCatalog;
import com.avanza.core.meta.messaging.enums.MessageAttribPropertyKeys;
import com.avanza.core.meta.messaging.enums.MessageAttributeTypeKeys;
import com.avanza.core.meta.messaging.enums.MessagePropertiesKeys;
import com.avanza.core.meta.messaging.exceptions.DelimiterNotFoundException;
import com.avanza.core.meta.messaging.exceptions.MessageNotFoundException;
import com.avanza.core.meta.messaging.exceptions.MetaAttributeAssociationException;
import com.avanza.core.meta.messaging.exceptions.TranIDAttribNotFoundException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.SecurityKeyInfo;
import com.avanza.core.util.ByteArrayUtil;
import com.avanza.core.util.Cryptographer;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.XmlMessageParser;
import com.google.common.io.CharStreams;

/**
 * @author shahbaz.ali
 * This class will serve the meta message integration to serialize and deserialize  the list view message 
 * on the basis of metaEntId passed as an argument in the constructor. 
 *
 */
public class ListMessageXmlFormat implements MessageFormat<String, Object> {

    private String responseMsg;
    private String metaEntId;
    private String transactionId;
    private final Logger logger = Logger.getLogger(ListMessageXmlFormat.class);
    private SecurityKeyInfo keyInfo;

    public ListMessageXmlFormat(String metaEntId) {
        this(metaEntId, null);
    }

    public ListMessageXmlFormat(String metaEntId, SecurityKeyInfo keyInfo) {
        this.metaEntId = metaEntId;
        this.keyInfo = keyInfo;
    }

    public void generateTransactionId() throws Exception {
        //added below -- shoaib.rehman
        Message message = MetaMessageCatalog.getMessage(metaEntId, MessageAction.ListRequest);
        if (message != null) {
            HashMap<String, String> messageParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(message.getMessageParams(), StringHelper.COMMA);
            if (!"true".equals(messageParamMap.get(MessagePropertiesKeys.isTransaction)))
                return;
        }
        // get attribute type of transaction id of request message
        // if not found throws exception
        // create its instance and call to ganerate transition id
        logger.logInfo("Generating TransactionId.... [ListMessageXmlFormat.generateTransactionId ()]");
        MessageAttribute tranAttribute = MetaMessageCatalog.getTransactionIdAttribute(metaEntId, MessageAction.ListRequest);
        if (tranAttribute == null || (StringHelper.isEmpty(tranAttribute.getMessageAttributeType().getDefaultValueClass()))) {
            throw new TranIDAttribNotFoundException(String.format("Transaction Id Message Attribute not found for message of Meta Entity %s", metaEntId));
        } else {
            Object newObj = Class.forName(tranAttribute.getMessageAttributeType().getDefaultValueClass()).newInstance();
            if (newObj instanceof MessageAttributeTypeInterface) {
                MessageAttributeTypeInterface typeObj = (MessageAttributeTypeInterface) newObj;
                transactionId = (String) typeObj.getDefaultValue();
            }
        }
    }

    public String getResponse() {
        return this.responseMsg;
    }

    public String getTransactionCode() {
        // TODO Auto-generated method stub
        return null;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String serialize() throws Exception {
		/*	  Shahbaz
�	ListMessageFormat will get the ListRequest action Message from MetaMessageCatalogue associated with Meta Entity ID field.
�	ListMessageFormat get the sorted list of Message Attributes.
�	Based on its type it prepares Delimited, Length based or Xml based Request Message.
�	By Iterating through the attributes sorted list ListMessageFormat will prepares the request message.
�	It will look for special attribute type associated with any attribute, if found then append its formatted value to request message. 
�	If no attribute type found then checks for VALUE_EXPRESSION.  
�	Parse the expression using Expression parser. 
�	Append the formatted value to the request String message. 
�	Move to the next attribute in the list.
�	After processing each request attributes, return the request message string
		 */
        logger.logInfo("Serializing Message .... [ListMessageXmlFormat.serialize()]");
        Message message = MetaMessageCatalog.getMessage(metaEntId, MessageAction.ListRequest);
        if (message != null) {
            HashMap<String, String> messageParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(message.getMessageParams(), StringHelper.COMMA);
            String request = messageParamMap.get(MessagePropertiesKeys.topMostRequestElement.toString());
            String namespace = messageParamMap.get(MessagePropertiesKeys.ns.toString());
            String prefix = messageParamMap.get(MessagePropertiesKeys.prefix.toString());


            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();

            // SOAP Envelope
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration(prefix, namespace);

            // SOAP Body
            SOAPBody soapBody = envelope.getBody();
            SOAPElement soapBodyRootElem = soapBody.addChildElement(request, prefix);


            for (MessageAttribute messageAttribute : message.getSortedAttributes()) {
                HashMap<String, String> attribParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(messageAttribute.getMessageAttribParams(), StringHelper.COMMA);
                String xmlTagName = attribParamMap.get(MessageAttribPropertyKeys.xmltagname.toString());

                SOAPElement addedElement = null;

                if (StringHelper.isEmpty(xmlTagName)) {
                    xmlTagName = messageAttribute.getAttributeName();
                }

                SOAPElement soapBodyElem = soapBodyRootElem.addChildElement(xmlTagName, prefix);


                if (StringHelper.isNotEmpty(attribParamMap.get(MessageAttribPropertyKeys.parentTagName.toString()))) {
                    String parentTagName = attribParamMap.get(MessageAttribPropertyKeys.parentTagName.toString());

                    addedElement = soapBodyRootElem.addChildElement(soapBodyElem);


                } else {
                    soapBody.addChildElement(soapBodyElem);
                }
                if (!"true".equals(attribParamMap.get(MessageAttribPropertyKeys.isParentTag.toString()))) {
                    String attributeValue = getAttributeValue(messageAttribute);

                    addedElement.addTextNode(attributeValue);
                }
            }


            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader("SOAPAction", namespace + request);

            soapMessage.saveChanges();

            /* Print the request message */
            System.out.print("Request SOAP Message = ");
            soapMessage.writeTo(System.out);

            //serialize  SOAP Message

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            Source sourceContent = soapMessage.getSOAPPart().getContent();

            System.out.print("\nRequest SOAP Message = ");

            StringWriter writer = new StringWriter();
            transformer.transform(sourceContent, new StreamResult(writer));
            String soapRequest = writer.getBuffer().toString();
            soapRequest.replaceAll("\n|\r", "");
            System.out.print(soapRequest);

            logger.logInfo("Request SOAP Message  =  " + soapRequest);


            return soapRequest;
 			
             
             /*	
			Document doc = new DocumentImpl();
			Element detail = doc.createElement(StringHelper.trimSpaceReplace(request,'_'));
			doc.appendChild(detail);
			for(MessageAttribute messageAttribute: message.getSortedAttributes()){
				HashMap<String,String> attribParamMap= (HashMap<String, String>) StringHelper.getPropertiesMapFrom(messageAttribute.getMessageAttribParams(), StringHelper.COMMA);
				String xmlTagName= attribParamMap.get(MessageAttribPropertyKeys.xmltagname.toString());
				if(StringHelper.isEmpty(xmlTagName)){
					xmlTagName= messageAttribute.getAttributeName();
				}
				Element docAttriubte = doc.createElement(StringHelper.trimSpaceReplace(xmlTagName,'_'));
				if(StringHelper.isNotEmpty(attribParamMap.get(MessageAttribPropertyKeys.parentTagName.toString()))){
					String parentTagName = attribParamMap.get(MessageAttribPropertyKeys.parentTagName.toString());
					NodeList childNodes = doc.getElementsByTagName(parentTagName);
					childNodes.item(childNodes.getLength() - 1).appendChild(docAttriubte);
				} else {
					detail.appendChild(docAttriubte);
				}
				if(!"true".equals(attribParamMap.get(MessageAttribPropertyKeys.isParentTag.toString()))){
					String attributeValue = getAttributeValue(messageAttribute);
					
					
					docAttriubte.appendChild(doc.createTextNode(attributeValue));
				}
			}
		
			//***encrypt message body if required.
			if(message.getSignatureXpath() != null && keyInfo != null){
				
				//serialize body
				XPath xpathEvaluator = XPathFactory.newInstance().newXPath(); 
				Element bodyElement = (Element) xpathEvaluator.evaluate(message.getSignaturedDataXpath()
						, doc,  XPathConstants.NODE);
				
				StringBuilder sbBody = new StringBuilder();
				for(int i=0; i<bodyElement.getChildNodes().getLength(); i++){
					Node node = bodyElement.getChildNodes().item(i);
					if(node instanceof Element){
						sbBody.append(serializeXml((Element) node));
					}
				}
				
				//calculate signatue for body
				String signature =ByteArrayUtil.toHexLiteral(Cryptographer.generateSignature(sbBody.toString().getBytes(Charset.forName("UTF-8"))
						,keyInfo.getAlgorithm()
						,keyInfo.getOurKeyPair().getPrivate()));
				
				//add signature to xml
				Element signatureElement = (Element) xpathEvaluator.evaluate(message.getSignatureXpath()
						, doc,  XPathConstants.NODE);
				signatureElement.removeChild(signatureElement.getFirstChild());
				signatureElement.appendChild(doc.createTextNode(signature));
				
			}
			
			String xml = serializeXml(doc.getDocumentElement());
			return xml;*/


        } else {
            throw new MessageNotFoundException(String.format("Message not found for the meta entity %s", metaEntId));
        }
    }


    private String serializeXml(Element element) throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        XMLSerializer serializer = new XMLSerializer();
        serializer.setOutputByteStream(outStream);

        serializer.asDOMSerializer();
        serializer.serialize(element);

        //reader.
        Reader reader = new InputStreamReader(new ByteArrayInputStream(outStream.toByteArray()), "UTF-8");
        String xml = CharStreams.toString(reader);
        return xml;
    }

    public Object deserialize() throws Exception {
		/* Shahbaz 
	�	ListMessageFormat will initiate the MessageParser by passing the response message as argument to its constructor.
	�	MessageParser depends on the ListMessageFormat type i.e. Delimited, XML or Length based.
	�	ListMessageFormat will get the ListResponse action Message associated with the Meta Entity ID field.
	�	Get the Sorted Message Attributes list of the Message.
	�	Iterating through the attributes sorted list.
	�	If attribute has Sub Message ID then get the ListResponse action Message associated with the Sub Message Id.
	�	Prepare the list of DataObject of size equal to the value of this attribute as it contains number of records the block contains.
	�	Loop number of times equal to the size of the list.
	�	Create DataObject of Meta Entity associated with the Sub Message.
	�	Loop through the message attributes.
	�	Get the next value from MessageParser and set it as Sub Message DataObject Meta Attribute value using Message Attribute name.
	�	On processing the complete message attributes add the DataObject to the list.
	�	Move to the next iteration in the response message.
	�	After all records have been processed, return the List of DataObject objects.
		 */
        logger.logInfo("Deserializing Message .... [ListMessageXmlFormat.deserialize()]");
        Message message = MetaMessageCatalog.getMessage(metaEntId, MessageAction.ListResponse);
        List<DataObject> responseList = new ArrayList<DataObject>();
        MessageResponseObject responseObject = new MessageResponseObject();
        String responseCode = null;
        if (message != null) {
            XmlMessageParser messageParser = new XmlMessageParser(getResponse());
            for (MessageAttribute messageAttribute : message.getSortedAttributes()) {
                if (messageAttribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.RESPONSE_CODE.toString())) {
                    responseCode = messageParser.getAttributeValue(messageAttribute);
                    responseObject.setResponseCode(responseCode);
                } else {
                    Message subMessage = messageAttribute.getSubMessage();
                    if (subMessage != null) {
                        responseList.addAll(handleSubMessage(subMessage, messageParser));
                    }
                }
            }
        } else {
            throw new MessageNotFoundException(String.format("Message not found for the meta entity %s", metaEntId));
        }
        responseObject.setResponseObject(responseList);
        return responseObject;
    }

    private List<DataObject> handleSubMessage(Message subMessage, XmlMessageParser messageParser) throws DelimiterNotFoundException, MetaAttributeAssociationException, XPathExpressionException {
        int blockSize = messageParser.getTagCount((MessageAttribute) subMessage.getSortedAttributes().get(0));
        List<DataObject> responseList = new ArrayList<DataObject>(blockSize);
        for (int i = 0; i < blockSize; i++) {
            DataObject messageDataObject = MetaDataRegistry.getMetaEntity(subMessage.getMetaEntity().getId()).createEntity();
            for (MessageAttribute subMessageAttribute : subMessage.getSortedAttributes()) {
                MetaAttributeImpl metaAttrib = subMessageAttribute.getMetaEntityAttrib();
                if (metaAttrib != null) {
                    if (!subMessageAttribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.WS_ELEMENT_NODE.name()))
                        messageDataObject.setValue(metaAttrib.getSystemName(),
                                messageParser.getAttributeValue(subMessageAttribute, i)
                        );
                }
            }
            responseList.add(messageDataObject);
        }
        return responseList;
    }

    public void setResponse(String res) {
        this.responseMsg = res;
    }

    private String getAttributeValue(MessageAttribute messageAttribute) {
        String attributeValue = "";
        MessageAttributeType messageAttribType = messageAttribute.getMessageAttributeType();
        if (messageAttribType != null) {
            if (messageAttribType.getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.TRANSACTION_ID.toString()))
                attributeValue = getTransactionId();
            else {
                String implClass = messageAttribType.getDefaultValueClass();
                if (implClass != null) {
                    Object newObj = null;
                    try {
                        newObj = Class.forName(implClass).newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (newObj != null && newObj instanceof MessageAttributeTypeInterface) {
                        MessageAttributeTypeInterface formatter = (MessageAttributeTypeInterface) newObj;
                        attributeValue = formatter.getDefaultValue().toString();
                    }
                } else {
                    String expression = messageAttribute.getValueExpression();
                    if (!StringHelper.isEmpty(expression)) {
                        attributeValue = (String) ExpressionHelper.getExpressionResult(expression, String.class);
                    }
                }
            }
        }
        return attributeValue;
    }

    public static void main(String[] str) {


    }
}
