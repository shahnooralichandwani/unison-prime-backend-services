package com.avanza.integrationutility.core;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.avanza.core.CoreException;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.integrationutility.implementations.TransactionConfig;

/**
 * @author shahbaz.ali
 */
public class IntegrationChannelFactory implements ChannelFactory {

    private static Hashtable<String, Map> channelList = new Hashtable<String, Map>();

    private static IntegrationChannelFactory instance;

    private IntegrationChannelFactory() {

    }

    public static IntegrationChannelFactory getInstance() {

        if (instance == null)
            instance = new IntegrationChannelFactory();

        return instance;

    }

    public void load(Object loadingConfigforChannels) {

        ConfigSection section = (ConfigSection) loadingConfigforChannels;

        List<ConfigSection> childList = section.getChildSections(IntegrationChannelsLookupKeys.XML_RENDERER.toString());

        if (childList.size() > 0)
            TransactionConfig.initialize();

        for (ConfigSection renderersection : childList) {

            boolean isEnabled = Boolean.valueOf(renderersection.getTextValue(IntegrationChannelsLookupKeys.ENABLED.toString()));
            String key = renderersection.getTextValue(IntegrationChannelsLookupKeys.KEY.toString());

            if (isEnabled) {
                HashMap<String, Object> map = new HashMap<String, Object>();

                String impClass = renderersection.getTextValue(IntegrationChannelsLookupKeys.CLASS.toString());
                String ipAddress = renderersection.getTextValue(IntegrationChannelsLookupKeys.IP_ADDRESS.toString());
                int port = renderersection.getIntValue(IntegrationChannelsLookupKeys.PORT.toString());
                long connectionTimeout = renderersection.getLongValue(IntegrationChannelsLookupKeys.CONNECTION_TIMEOUT.toString());
                String properties = renderersection.getTextValue(IntegrationChannelsLookupKeys.PROPERTIES.toString());
                String selectorClass = renderersection.getTextValue(IntegrationChannelsLookupKeys.SELECTOR_CLASS.toString());
                long waitTimeOut = renderersection.getLongValue(IntegrationChannelsLookupKeys.WAIT_TIMEOUT.toString());
                IntegrationChannelProperties prop = new IntegrationChannelProperties();
                prop.parse(properties);

                try {

                    TransactionIdentifierSelector selectorInstance = (TransactionIdentifierSelector) Class.forName(selectorClass)
                            .getConstructor(String.class).newInstance(properties);
                    map.put("impClass", impClass);
                    map.put("ipAddress", ipAddress);
                    map.put("port", port);
                    map.put("connectionTimeout", connectionTimeout);
                    map.put("prop", prop);
                    map.put("selectorClass", selectorInstance);
                    map.put("waitTimeOut", waitTimeOut);

                    channelList.put(key, map);
                } catch (Exception e) {
                    throw new CoreException(e, "IntegrationChannelFactory Load Failed");
                }
            }

        }

    }

    public IntegrationChannel getChannel(String key) {

        try {
            //Shahbaz: returning everytime a new instance...
            HashMap map = (HashMap) channelList.get(key);
            IntegrationChannel channelInstance = (IntegrationChannel) Class.forName((String) map.get("impClass")).newInstance();
            IntegrationChannel obj = channelInstance.initialize(map.get("ipAddress").toString()
                    , (Integer) map.get("port"), (Long) map.get("connectionTimeout")
                    , (IntegrationChannelProperties) map.get("prop")
                    , (TransactionIdentifierSelector) map.get("selectorClass")
                    , (Long) map.get("waitTimeOut"));

            return obj;
        } catch (Exception e) {
            throw new CoreException(e, "getChannel Failed");

        }

    }

}
