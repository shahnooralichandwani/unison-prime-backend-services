/**
 *
 */
package com.avanza.integrationutility.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.meta.MetaAttributeImpl;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.messaging.Message;
import com.avanza.core.meta.messaging.MessageAction;
import com.avanza.core.meta.messaging.MessageAttribute;
import com.avanza.core.meta.messaging.MessageAttributeType;
import com.avanza.core.meta.messaging.MessageAttributeTypeInterface;
import com.avanza.core.meta.messaging.MetaMessageCatalog;
import com.avanza.core.meta.messaging.enums.MessageAttributeTypeKeys;
import com.avanza.core.meta.messaging.enums.MessagePropertiesKeys;
import com.avanza.core.meta.messaging.exceptions.DelimiterNotFoundException;
import com.avanza.core.meta.messaging.exceptions.MessageNotFoundException;
import com.avanza.core.meta.messaging.exceptions.MetaAttributeAssociationException;
import com.avanza.core.meta.messaging.exceptions.TranIDAttribNotFoundException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.core.util.MessageParser;
import com.avanza.core.util.StringHelper;

/**
 * @author shahbaz.ali
 * This class will serve the meta message integration to serialize and deserialize  the list view message 
 * on the basis of metaEntId passed as an argument in the constructor. 
 *
 */
public class ListMessageDelimitedFormat implements MessageFormat<String, Object> {

    private String responseMsg;
    private String metaEntId;
    private String transactionId;
    private final Logger logger = Logger.getLogger(ListMessageDelimitedFormat.class);

    public ListMessageDelimitedFormat(String metaEntId) {
        this.metaEntId = metaEntId;
    }


    public void generateTransactionId() throws Exception {

        // get attribute type of transaction id of request message
        // if not found throws exception
        // create its instance and call to ganerate transition id
        logger.logInfo("Generating TransactionId.... [ListMessageDelimitedFormat.generateTransactionId ()]");
        MessageAttribute tranAttribute = MetaMessageCatalog.getTransactionIdAttribute(metaEntId, MessageAction.ListRequest);
        if (tranAttribute == null || (StringHelper.isEmpty(tranAttribute.getMessageAttributeType().getDefaultValueClass()))) {
            throw new TranIDAttribNotFoundException(String.format("Transaction Id Message Attribute not found for message of Meta Entity %s", metaEntId));
        } else {
            Object newObj = Class.forName(tranAttribute.getMessageAttributeType().getDefaultValueClass()).newInstance();

            if (newObj instanceof MessageAttributeTypeInterface) {
                MessageAttributeTypeInterface typeObj = (MessageAttributeTypeInterface) newObj;
                transactionId = (String) typeObj.getDefaultValue();
            }
        }

    }


    public String getResponse() {
        return this.responseMsg;
    }


    public String getTransactionCode() {
        return null;
    }


    public String getTransactionId() {
        return transactionId;
    }


    public String serialize() throws Exception {

		/*	  Shahbaz

	ListMessageFormat will get the ListRequest action Message from MetaMessageCatalogue associated with Meta Entity ID field.
	ListMessageFormat get the sorted list of Message Attributes.
	Based on its type it prepares Delimited, Length based or Xml based Request Message.
	By Iterating through the attributes sorted list ListMessageFormat will prepares the request message.
	It will look for special attribute type associated with any attribute, if found then append its formatted value to request message. 
	If no attribute type found then checks for VALUE_EXPRESSION.  
	Parse the expression using Expression parser. 
	Append the formatted value to the request String message. 
	Move to the next attribute in the list.
	After processing each request attributes, return the request message string

		 */
        logger.logInfo("Serializing Message .... [ListMessageDelimitedFormat.serialize()]");
        Message message = MetaMessageCatalog.getMessage(metaEntId, MessageAction.ListRequest);

        if (message != null) {
            StringBuilder requestMessage = new StringBuilder();
            HashMap<String, String> messageParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(message.getMessageParams(), StringHelper.COMMA);
            String delimiter = (String) messageParamMap.get(MessagePropertiesKeys.delimiter.toString());

            if (StringHelper.isNotEmpty(delimiter)) {

                for (MessageAttribute messageAttribute : message.getSortedAttributes()) {
                    String attributeValue = getAttributeValue(messageAttribute);
                    requestMessage.append(attributeValue);
                    requestMessage.append(delimiter);
                }
            } else {
                throw new DelimiterNotFoundException(String.format("Delimiter not found as the property of the Message, MessageId: %s", message.getMessageId()));
            }


            return requestMessage.toString();
        } else {
            throw new MessageNotFoundException(String.format("Message not found for the meta entity %s", metaEntId));
        }

    }


    public Object deserialize() throws Exception {

		/* Shahbaz 
		ListMessageFormat will initiate the MessageParser by passing the response message as argument to its constructor.
		MessageParser depends on the ListMessageFormat type i.e. Delimited, XML or Length based.
		ListMessageFormat will get the ListResponse action Message associated with the Meta Entity ID field.
		Get the Sorted Message Attributes list of the Message.
		Iterating through the attributes sorted list.
		If attribute has Sub Message ID then get the ListResponse action Message associated with the Sub Message Id.
		Prepare the list of DataObject of size equal to the value of this attribute as it contains number of records the block contains.
		Loop number of times equal to the size of the list.
		Create DataObject of Meta Entity associated with the Sub Message.
		Loop through the message attributes.
		Get the next value from MessageParser and set it as Sub Message DataObject Meta Attribute value using Message Attribute name.
		On processing the complete message attributes add the DataObject to the list.
		Move to the next iteration in the response message.
		After all records have been processed, return the List of DataObject objects.
		 */
        logger.logInfo("Deserializing Message .... [ListMessageDelimitedFormat.deserialize()]");
        Message message = MetaMessageCatalog.getMessage(metaEntId, MessageAction.ListResponse);
        List<DataObject> responseList = new ArrayList<DataObject>();
        MessageResponseObject responseObject = new MessageResponseObject();
        String responseCode = null;

        if (message != null) {

            HashMap messageParamMap = (HashMap) StringHelper.getPropertiesMapFrom(message.getMessageParams(), StringHelper.COMMA);
            String msgdelimiter = (String) messageParamMap.get(MessagePropertiesKeys.delimiter.toString());

            if (StringHelper.isNotEmpty(msgdelimiter)) {

                MessageParser messageParser = new MessageParser(getResponse());

                for (MessageAttribute messageAttribute : message.getSortedAttributes()) {

                    if (messageAttribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.RESPONSE_CODE.toString())) {
                        responseCode = messageParser.getNext(msgdelimiter);
                        responseObject.setResponseCode(responseCode);
                    } else {
                        Message subMessage = messageAttribute.getSubMessage();

                        if (subMessage != null) {
                            responseList.addAll(handleSubMessage(subMessage, messageParser, msgdelimiter));
                        } else {
                            // this makes the pointer to move forward
                            messageParser.getNext(msgdelimiter);

                        }
                    }

                }
            } else {
                throw new DelimiterNotFoundException(String.format("Delimiter not found as the property of the Sub Message, MessageId: %s", message.getMessageId()));
            }


        } else {
            throw new MessageNotFoundException(String.format("Message not found for the meta entity %s", metaEntId));
        }

        responseObject.setResponseObject(responseList);
        return responseObject;
    }


    private List<DataObject> handleSubMessage(Message subMessage, MessageParser messageParser, String msgdelimiter) throws DelimiterNotFoundException, MetaAttributeAssociationException {

        String blockSizeString = messageParser.getNext(msgdelimiter);
        int blockSize = Integer.parseInt(blockSizeString);
        List<DataObject> responseList = new ArrayList<DataObject>(blockSize);
        HashMap subMessageParamMap = (HashMap) StringHelper.getPropertiesMapFrom(subMessage.getMessageParams(), StringHelper.COMMA);
        String subMsgdelimiter = (String) subMessageParamMap.get(MessagePropertiesKeys.delimiter.toString());

        if (StringHelper.isNotEmpty(subMsgdelimiter)) {

            for (int i = 0; i < blockSize; i++) {
                DataObject messageDataObject = MetaDataRegistry.getMetaEntity(subMessage.getMetaEntity().getId()).createEntity();

                for (MessageAttribute subMessageAttribute : subMessage.getSortedAttributes()) {
                    String value = messageParser.getNext(subMsgdelimiter);
                    MetaAttributeImpl metaAttrib = subMessageAttribute.getMetaEntityAttrib();
                    if (metaAttrib != null) {
                        String metaAttribSysName = metaAttrib.getSystemName();
                        messageDataObject.setValue(metaAttribSysName, value);
                    } else {
                        throw new MetaAttributeAssociationException(String.format("Message Attribute %s not associated with Meta Entity Attribute as expected", subMessageAttribute.getMessageAttributeId()));
                    }
                }
                responseList.add(messageDataObject);
            }

        } else {
            throw new DelimiterNotFoundException(String.format("Delimiter not found as the property of the Sub Message, MessageId: %s", subMessage.getMessageId()));
        }

        return responseList;

    }


    public void setResponse(String res) {
        this.responseMsg = res;
    }

    private String getAttributeValue(MessageAttribute messageAttribute) {
        String attributeValue = "";

        MessageAttributeType messageAttribType = messageAttribute.getMessageAttributeType();

        if (messageAttribType != null) {

            if (messageAttribType.getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.TRANSACTION_ID.toString())) {
                attributeValue = transactionId;
            } else {
                String implClass = messageAttribType.getDefaultValueClass();

                // This Attribute have no implClass will be used for Flag purpose,
                // like to flag Respose Code attribute in the message.
                if (implClass != null) {

                    Object newObj = null;
                    try {
                        newObj = Class.forName(implClass).newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (newObj != null && newObj instanceof MessageAttributeTypeInterface) {

                        MessageAttributeTypeInterface formatter = (MessageAttributeTypeInterface) newObj;
                        attributeValue = formatter.getDefaultValue().toString();
                    }
                } else {
                    String expression = messageAttribute.getValueExpression();

                    if (!StringHelper.isEmpty(expression)) {
                        attributeValue = (String) ExpressionHelper.getExpressionResult(expression, String.class);
                    }
                }

            }

        }

        return attributeValue;
    }

}
