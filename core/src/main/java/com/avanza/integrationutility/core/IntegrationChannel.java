package com.avanza.integrationutility.core;


/**
 * @author shahbaz.ali
 * Every IntegrationChannel must override this  class
 */

public abstract class IntegrationChannel {

    /**
     * @return true if connected, false otherwise
     */

    protected abstract boolean Connect();

    /**
     * @param object
     * @throws TransactionNotFound
     * @throws InterruptedException
     */
    public abstract Object send(MessageFormat messageFormat) throws Exception;


    public abstract boolean isConnected();

    protected abstract void notify(String transactionId, String message);

    protected abstract IntegrationChannel initialize(String address, int port,
                                                     long connectionTimeOut, IntegrationChannelProperties prop, TransactionIdentifierSelector tranIdSelector, long waitTimeOut);

    protected abstract String getResponseTranId(String response);

    public abstract Object send(MessageFormat[] messageFormats) throws Exception;
}
