package com.avanza.integrationutility.core;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import com.avanza.core.util.Logger;

/**
 * @author shahbaz.ali
 * Responsible to merge messages if distributes in chunks
 * and return the response
 */
public class MessageResponseHandler extends IoHandlerAdapter {

    private final Logger logger = Logger.getLogger(MessageResponseHandler.class);
    private IntegrationChannel _integrationUtility;

    public MessageResponseHandler(IntegrationChannel integrationUtility) {
        super();
        this._integrationUtility = integrationUtility;
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause)
            throws Exception {
        // TODO Auto-generated method stub
        System.out.println(cause.getMessage());
    }

    @Override
    public void messageReceived(IoSession session, Object message)
            throws Exception {


        logger.logInfo("Response received , (ServerResponseHandler.messageReceived())");

        Boolean isFinished = false; // Flag for checking response is finished or not

        if (session.getAttribute(Constants.MESSAGE_COMPLETED) != null) {
            isFinished = (Boolean) session.getAttribute(Constants.MESSAGE_COMPLETED);
        }

        String msg = null;

        String sessionAttributeMessage = null;
        if (session.getAttribute(Constants.MESSAGE_CHUNK) != null) { // old message
            sessionAttributeMessage = (String) session.getAttribute(Constants.MESSAGE_CHUNK);
            msg = sessionAttributeMessage;  //keep old message
            msg += message.toString(); //updating/concatenating the message
            logger.logInfo("Recieved Part of Old Message. And now we are concatenating it...");
        } else { // new message
            logger.logInfo("Recieved New Message");
            msg = message.toString();
        }
        logger.logInfo("Message Recieved Uptil Now - message:" + msg);


        if (isFinished) {
            logger.logInfo("Response is finished");
            logger.logInfo("Complete message:" + msg);
            String messageWithoutMetaLength = null;

            String metaLength = (String) session.getAttribute(Constants.META_LENGTH);
            if (metaLength != null) {
                Integer metaLengthInt = Integer.parseInt((String) session.getAttribute(Constants.META_LENGTH));
                messageWithoutMetaLength = msg.substring(metaLengthInt);
            } else {
                messageWithoutMetaLength = msg.toString();
            }

            String transactionId = _integrationUtility.getResponseTranId(messageWithoutMetaLength);
            logger.logInfo("transactionId from response - transactionId:" + transactionId);

            if (transactionId == null || transactionId.length() == 0) {
                logger.logError("transaction Id is NULL or empty");
                throw new Exception("transaction Id is NULL or empty");
            }
            //set thread invoked value to true before notify method;
            //response wait time out issue fixed -- shoaib.rehman
            session.setAttribute(Constants.THREAD_INVOKED, true);
            _integrationUtility.notify(transactionId, messageWithoutMetaLength);
            msg = null; // reset message
            session.setAttribute(Constants.MESSAGE_CHUNK, null);
            session.setAttribute(Constants.MESSAGE_COMPLETED, null);

        } else { // response is not Finished
            session.setAttribute(Constants.MESSAGE_CHUNK, null);
            session.setAttribute(Constants.MESSAGE_CHUNK, msg);
            logger.logInfo("Response is not finished");
        }


    }

    @Override
    public void messageSent(IoSession session, Object message) throws Exception {

        logger.logInfo("Message sent successfully, ( ServerResponseHandler.messageSent() )");
        logger.logInfo("Message -->, ( " + message + " )");
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {

    }

    @Override
    public void sessionCreated(IoSession session) throws Exception {

    }

    @Override
    public void sessionIdle(IoSession session, IdleStatus status)
            throws Exception {

    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        // TODO Auto-generated method stub
        super.sessionOpened(session);
    }


}
