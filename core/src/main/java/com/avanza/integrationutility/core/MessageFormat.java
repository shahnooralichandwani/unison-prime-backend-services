/**
 *
 */
package com.avanza.integrationutility.core;


/**
 * @author shahbaz.ali
 *
 *         Service handlers are defined in config file which are specific to the
 *         object or information we get from the server.
 *
 *         All service handlers must implement this interface
 */
// T = Request Type
// U = Response Type
public interface MessageFormat<T, U> {

    public String serialize() throws Exception;

    public U deserialize() throws Exception;

    public String getTransactionCode();

    public String getTransactionId();

    public void generateTransactionId() throws Exception;

    public void setResponse(String res);

    public String getResponse();
}
