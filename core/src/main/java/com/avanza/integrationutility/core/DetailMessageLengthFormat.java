/**
 *
 */
package com.avanza.integrationutility.core;

import java.util.HashMap;

import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.meta.MetaAttributeImpl;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.messaging.Message;
import com.avanza.core.meta.messaging.MessageAction;
import com.avanza.core.meta.messaging.MessageAttribute;
import com.avanza.core.meta.messaging.MessageAttributeType;
import com.avanza.core.meta.messaging.MessageAttributeTypeInterface;
import com.avanza.core.meta.messaging.MetaMessageCatalog;
import com.avanza.core.meta.messaging.enums.MessageAttribPropertyKeys;
import com.avanza.core.meta.messaging.enums.MessageAttributeTypeKeys;
import com.avanza.core.meta.messaging.enums.MessagePropertiesKeys;
import com.avanza.core.meta.messaging.exceptions.AttributeLengthUndefinedException;
import com.avanza.core.meta.messaging.exceptions.MessageLengthExceedsException;
import com.avanza.core.meta.messaging.exceptions.MessageLengthUndefinedException;
import com.avanza.core.meta.messaging.exceptions.MessageNotFoundException;
import com.avanza.core.meta.messaging.exceptions.TranIDAttribNotFoundException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.core.util.MessageParser;
import com.avanza.core.util.StringHelper;


/**
 * @author shahbaz.ali
 * This class will serve the meta message integration to serialize and deserialize the message 
 * on the basis of metaEntId passed as an argument in the constructor. 
 *
 */
public class DetailMessageLengthFormat implements MessageFormat<String, Object> {

    private String responseMsg;
    private String metaEntId;
    private String instanceId;
    private String transactionId;
    private final Logger logger = Logger.getLogger(DetailMessageLengthFormat.class);

    public DetailMessageLengthFormat(String metaEntId, String instanceId) {
        this.metaEntId = metaEntId;
        this.instanceId = instanceId;
    }

    public void generateTransactionId() throws Exception {

        // get attribute type of transaction id of request message
        // if not found throws exception
        // create its instance and call to generate transition id
        logger.logInfo("Generating TransactionId.... [DetailMessageLengthFormat.generateTransactionId ()]");
        MessageAttribute tranAttribute = MetaMessageCatalog.getTransactionIdAttribute(metaEntId, MessageAction.ListRequest);
        if (tranAttribute == null || (StringHelper.isEmpty(tranAttribute.getMessageAttributeType().getDefaultValueClass()))) {
            throw new TranIDAttribNotFoundException(String.format("Transaction Id Message Attribute not found for message of Meta Entity %s", metaEntId));
        } else {

            Object newObj = Class.forName(tranAttribute.getMessageAttributeType().getDefaultValueClass()).newInstance();

            if (newObj instanceof MessageAttributeTypeInterface) {
                MessageAttributeTypeInterface typeObj = (MessageAttributeTypeInterface) newObj;
                transactionId = (String) typeObj.getDefaultValue();
            }
        }

    }


    @Override
    public String getResponse() {
        // TODO Auto-generated method stub
        return this.responseMsg;
    }


    @Override
    public String getTransactionCode() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public String getTransactionId() {
        return transactionId;
    }


    @Override
    public String serialize() throws Exception {

		/*	  Shahbaz

        	�	ListMessageFormat will get the ListRequest action Message from MetaMessageCatalogue associated with Meta Entity ID field.
        	�	ListMessageFormat get the sorted list of Message Attributes.
        	�	Based on its type it prepares Delimited, Length based or Xml based Request Message.
        	�	By Iterating through the attributes sorted list ListMessageFormat will prepares the request message.
        	�	It will look for special attribute type associated with any attribute, if found then append its formatted value to request message. 
        	�	If no attribute type found then checks for VALUE_EXPRESSION.  
        	�	Parse the expression using Expression parser. 
        	�	Append the formatted value to the request String message. 
        	�	Move to the next attribute in the list.
        	�	After processing each request attributes, return the request message string

		 */
        logger.logInfo("Serializing Message .... [DetailMessageLengthFormat.serialize()]");
        Message message = MetaMessageCatalog.getMessage(metaEntId, MessageAction.DetailRequest);

        if (message != null) {

            StringBuilder requestMessageString = new StringBuilder();

            HashMap<String, String> messageParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(message.getMessageParams(), StringHelper.COMMA);
            String msgLengthString = messageParamMap.get(MessagePropertiesKeys.length.toString());

            if (StringHelper.isNotEmpty(msgLengthString)) {
                int msgLength = Integer.parseInt(msgLengthString);

                for (MessageAttribute messageAttribute : message.getSortedAttributes()) {

                    HashMap<String, String> attribParamMap = new HashMap<String, String>();

                    String attributeValue = "";
                    attribParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(messageAttribute.getMessageAttribParams(), StringHelper.COMMA);
                    String lengthString = attribParamMap.get(MessageAttribPropertyKeys.length.toString());

                    if (StringHelper.isEmpty(lengthString)) {
                        throw new AttributeLengthUndefinedException(String.format("Message Attribute Length expected of %s", messageAttribute.getMessageAttributeId()));
                    }

                    int length = Integer.parseInt(lengthString);

                    attributeValue = getAttributeValue(messageAttribute);
                    attributeValue = MessageUtility.cushionField(attributeValue, length, ' ');
                    requestMessageString.append(attributeValue);

                    if (requestMessageString.length() > msgLength) {
                        throw new MessageLengthExceedsException(String.format("Message length %s exceeds the defined limit %d for %s", requestMessageString.length(), msgLength, message.getMessageId()));
                    }

                }
                return requestMessageString.toString();
            } else {
                throw new MessageLengthUndefinedException(String.format("Message length not defined for the message %s", message.getMessageId()));
            }
        } else {
            throw new MessageNotFoundException(String.format("Message not found for the meta entity %s", metaEntId));
        }
    }


    @Override
    public Object deserialize() throws Exception {

		/* Shahbaz 
	�	ListMessageFormat will initiate the MessageParser by passing the response message as argument to its constructor.
	�	MessageParser depends on the ListMessageFormat type i.e. Delimited, XML or Length based.
	�	ListMessageFormat will get the ListResponse action Message associated with the Meta Entity ID field.
	�	Get the Sorted Message Attributes list of the Message.
	�	Iterating through the attributes sorted list.
	�	If attribute has Sub Message ID then get the ListResponse action Message associated with the Sub Message Id.
	�	Prepare the list of DataObject of size equal to the value of this attribute as it contains number of records the block contains.
	�	Loop number of times equal to the size of the list.
	�	Create DataObject of Meta Entity associated with the Sub Message.
	�	Loop through the message attributes.
	�	Get the next value from MessageParser and set it as Sub Message DataObject Meta Attribute value using Message Attribute name.
	�	On processing the complete message attributes add the DataObject to the list.
	�	Move to the next iteration in the response message.
	�	After all records have been processed, return the List of DataObject objects.
		 */
        logger.logInfo("Deserializing Message .... [DetailMessageLengthFormat.deserialize()]");
        Message message = MetaMessageCatalog.getMessage(metaEntId, MessageAction.DetailResponse);

        if (message != null) {

            DataObject dataObject = MetaDataRegistry.getMetaEntity(message.getMetaEntity().getId()).createEntity();
            HashMap<String, String> messageParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(message.getMessageParams(), StringHelper.COMMA);
            String msgLengthString = messageParamMap.get(MessagePropertiesKeys.length.toString());
            MessageResponseObject responseObject = new MessageResponseObject();
            String responseCode = null;

            if (StringHelper.isNotEmpty(msgLengthString)) {

                int msgLength = Integer.parseInt(msgLengthString);

                int read = 0;
                MessageParser messageParser = new MessageParser(getResponse());

                for (MessageAttribute messageAttribute : message.getSortedAttributes()) {

                    HashMap<String, String> attribParamMap = new HashMap<String, String>();

                    attribParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(messageAttribute.getMessageAttribParams(), StringHelper.COMMA);
                    String attribLengthString = attribParamMap.get(MessageAttribPropertyKeys.length.toString());

                    if (StringHelper.isEmpty(attribLengthString)) {
                        throw new AttributeLengthUndefinedException(String.format("Message Attribute Length expected of %s", messageAttribute.getMessageAttributeId()));
                    }

                    int length = Integer.parseInt(attribLengthString);
                    MetaAttributeImpl metaAttrib = messageAttribute.getMetaEntityAttrib();

                    String value = messageParser.getNext(length);

                    if (messageAttribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.RESPONSE_CODE.toString())) {
                        responseCode = value;
                        responseObject.setResponseCode(responseCode);
                    } else if (metaAttrib != null) {
                        String metaAttribSysName = metaAttrib.getSystemName();
                        dataObject.setValue(metaAttribSysName, value);
                    }


                    //check if we are in limit
                    if ((read += value.length()) > msgLength) {
                        throw new MessageLengthExceedsException(String.format("Response message length %s exceeds the defined limit %d for %s while deserializing.", read, msgLength, message.getMessageId()));
                    }


                }
                responseObject.setResponseObject(dataObject);
                return responseObject;
            } else {
                throw new MessageLengthUndefinedException(String.format("Message length not defined for the message %s", message.getMessageId()));
            }

        } else {
            throw new MessageNotFoundException(String.format("Message not found for the meta entity %s", metaEntId));
        }
    }


    @Override
    public void setResponse(String res) {
        this.responseMsg = res;

    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    private String getAttributeValue(MessageAttribute messageAttribute) {
        String attributeValue = "";
        MessageAttributeType messageAttribType = messageAttribute.getMessageAttributeType();

        if (messageAttribType != null) {

            if (messageAttribType.getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.TRANSACTION_ID.toString())) {
                attributeValue = getTransactionId();
            } else if (messageAttribType.getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.DETAIL_MSG_INSTANCE_ID.toString())) {
                attributeValue = getInstanceId();
            } else {
                String implClass = messageAttribType.getDefaultValueClass();

                if (implClass != null) {
                    Object newObj = null;
                    try {
                        newObj = Class.forName(implClass).newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (newObj != null && newObj instanceof MessageAttributeTypeInterface) {
                        MessageAttributeTypeInterface formatter = (MessageAttributeTypeInterface) newObj;
                        attributeValue = formatter.getDefaultValue().toString();
                    }
                } else {
                    String expression = messageAttribute.getValueExpression();

                    if (!StringHelper.isEmpty(expression)) {
                        attributeValue = (String) ExpressionHelper.getExpressionResult(expression, String.class);
                    }

                }

            }
        }
        return attributeValue;

    }

}
