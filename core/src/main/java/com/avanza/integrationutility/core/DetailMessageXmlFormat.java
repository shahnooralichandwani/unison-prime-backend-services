/**
 *
 */
package com.avanza.integrationutility.core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.meta.MetaAttributeImpl;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.messaging.Message;
import com.avanza.core.meta.messaging.MessageAction;
import com.avanza.core.meta.messaging.MessageAttribute;
import com.avanza.core.meta.messaging.MessageAttributeType;
import com.avanza.core.meta.messaging.MessageAttributeTypeInterface;
import com.avanza.core.meta.messaging.MetaMessageCatalog;
import com.avanza.core.meta.messaging.enums.MessageAttribPropertyKeys;
import com.avanza.core.meta.messaging.enums.MessageAttributeTypeKeys;
import com.avanza.core.meta.messaging.enums.MessagePropertiesKeys;
import com.avanza.core.meta.messaging.exceptions.MessageNotFoundException;
import com.avanza.core.meta.messaging.exceptions.TranIDAttribNotFoundException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.XmlMessageParser;
import com.avanza.integration.dbmapper.meta.DataAccessMode;


/**
 * @author shahbaz.ali
 * This class will serve the meta message integration to serialize and deserialize the detail view message 
 * on the basis of metaEntId and instance id passed as an argument in the constructor. 
 *
 */
public class DetailMessageXmlFormat implements MessageFormat<String, Object> {

    private String responseMsg;
    private String metaEntId;
    private String instanceId;
    private String transactionId;
    private final Logger logger = Logger.getLogger(DetailMessageXmlFormat.class);

    public DetailMessageXmlFormat(String metaEntId, String instanceId) {
        this.metaEntId = metaEntId;
        this.instanceId = instanceId;
    }

    @Override
    public void generateTransactionId() throws Exception {
        //added below -- shoaib.rehman
        Message message = MetaMessageCatalog.getMessage(metaEntId, MessageAction.ListRequest);
        if (message != null) {
            HashMap<String, String> messageParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(message.getMessageParams(), StringHelper.COMMA);
            if (!"true".equals(messageParamMap.get(MessagePropertiesKeys.isTransaction)))
                return;
        }
        // get attribute type of transaction id of request message
        // if not found throws exception
        // create its instance and call to ganerate transition id
        logger.logInfo("Generating TransactionId.... [DetailMessageXmlFormat.generateTransactionId ()]");
        MessageAttribute tranAttribute = MetaMessageCatalog.getTransactionIdAttribute(metaEntId, MessageAction.ListRequest);
        if (tranAttribute == null || (StringHelper.isEmpty(tranAttribute.getMessageAttributeType().getDefaultValueClass()))) {
            throw new TranIDAttribNotFoundException(String.format("Transaction Id Message Attribute not found for message of Meta Entity %s", metaEntId));
        } else {
            Object newObj = Class.forName(tranAttribute.getMessageAttributeType().getDefaultValueClass()).newInstance();
            if (newObj instanceof MessageAttributeTypeInterface) {
                MessageAttributeTypeInterface typeObj = (MessageAttributeTypeInterface) newObj;
                transactionId = (String) typeObj.getDefaultValue();
            }
        }
    }

    @Override
    public String getResponse() {
        return this.responseMsg;
    }

    @Override
    public String getTransactionCode() {
        return null;
    }

    @Override
    public String getTransactionId() {
        return transactionId;
    }

    @Override
    public String serialize() throws Exception {

		/*	  Shahbaz
�	DetailMessageFormat will get the DetailRequest action Message from MetaMessageCatalogue associated with Meta Entity ID field.
�	DetailMessageFormat get the sorted list of Message Attributes.
�	Based on its type it prepares Delimited, Length based or Xml based Request Message by Iterating through the attributes sorted list ListMessageFormat will prepares the request message.
�	It will look for special attribute type associated with any attribute, if found then append its formatted value to request message. 
�	If attribute type of Instance ID found then set instance ID as its value.
�	If no attribute type found then checks for VALUE_EXPRESSION.  
�	Parse the expression using Expression parser. 
�	Append the formatted value to the request String message. 
�	Move to the next attribute in the list.
�	After processing each request attributes, return the request message string.
		 */
        logger.logInfo("Serializing Message .... [DetailMessageXmlFormat.serialize()]");
        Message message = MetaMessageCatalog.getMessage(metaEntId, MessageAction.DetailRequest);

        if (message != null) {
            HashMap<String, String> messageParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(message.getMessageParams(), StringHelper.COMMA);
            String request = messageParamMap.get(MessagePropertiesKeys.topMostRequestElement.toString());
            Document doc = new DocumentImpl();
            Element detail = doc.createElement(StringHelper.trimSpaceReplace(request, '_'));
            doc.appendChild(detail);
            for (MessageAttribute messageAttribute : message.getSortedAttributes()) {
                HashMap<String, String> attribParamMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(messageAttribute.getMessageAttribParams(), StringHelper.COMMA);
                String xmlTagName = attribParamMap.get(MessageAttribPropertyKeys.xmltagname.toString());
                if (StringHelper.isEmpty(xmlTagName)) {
                    xmlTagName = messageAttribute.getAttributeName();
                }
                Element docAttriubte = doc.createElement(StringHelper.trimSpaceReplace(xmlTagName, '_'));
                if (StringHelper.isNotEmpty(attribParamMap.get(MessageAttribPropertyKeys.parentTagName.toString()))) {
                    String parentTagName = attribParamMap.get(MessageAttribPropertyKeys.parentTagName.toString());
                    NodeList childNodes = doc.getElementsByTagName(parentTagName);
                    childNodes.item(childNodes.getLength() - 1).appendChild(docAttriubte);
                } else {
                    detail.appendChild(docAttriubte);
                }
                if (!"true".equals(attribParamMap.get(MessageAttribPropertyKeys.isParentTag.toString()))) {
                    String attributeValue = getAttributeValue(messageAttribute);
                    docAttriubte.appendChild(doc.createTextNode(attributeValue));
                }
            }
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            XMLSerializer serializer = new XMLSerializer();
            serializer.setOutputByteStream(outStream);
            try {
                serializer.asDOMSerializer();
                serializer.serialize(doc);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String xml = new String(outStream.toByteArray());
            return xml;
        } else {
            throw new MessageNotFoundException(String.format("Message not found for the meta entity %s", metaEntId));
        }
    }

    @Override
    public Object deserialize() throws Exception {
		/* Shahbaz
 	�	DelimetedMessageFormat will initiate the MessageParser by passing the response message as argument to its constructor. 
 	�	MessageParser depends on the DelimetedMessageFormat type i.e. Delimited, XML or Length based.
	�	DelimetedMessageFormat will get the DetailResponse action Message associated with the Meta Entity ID field.
	�	Create DataObject of Meta Entity associated with Meta Entity ID.
	�	Get the Sorted Message Attributes list of the Message.
	�	Iterating through the attributes sorted list.
	�	Get the next value from MessageParser and set it as DataObject Meta Attribute value using Message Attribute name.
	�	On processing the complete message attributes return the DataObject
		 */
        logger.logInfo("Deserializing Message .... [DetailMessageXmlFormat.deserialize()]");
        Message message = MetaMessageCatalog.getMessage(metaEntId, MessageAction.DetailResponse);
        MessageResponseObject responseObject = new MessageResponseObject();
        DataObject dataObject = MetaDataRegistry.getMetaEntity(message.getMetaEntity().getId()).createEntity();
        String responseCode = null;
        if (message != null) {
            XmlMessageParser messageParser = new XmlMessageParser(getResponse());
            for (MessageAttribute messageAttribute : message.getSortedAttributes()) {
                if (messageAttribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.RESPONSE_CODE.toString())) {
                    responseCode = messageParser.getAttributeValue(messageAttribute);
                    responseObject.setResponseCode(responseCode);
                } else {
                    MetaAttributeImpl metaAttrib = messageAttribute.getMetaEntityAttrib();
                    if (metaAttrib != null) {
                        if (!messageAttribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.WS_ELEMENT_NODE.name()))
                            dataObject.setValue(metaAttrib.getSystemName(),
                                    messageParser.getAttributeValue(messageAttribute)
                            );
                    }
                }
            }
            //explicitly making another data object with above dataObject values map
            //to avoid left menus false behaviour -- shoaib.rehman
            DataObject obj = new DataObject(MetaDataRegistry.getMetaEntity(message.getMetaEntity().getId()), (HashMap<String, Object>) dataObject.getValues(), DataAccessMode.Detail);
            responseObject.setResponseObject(obj);
            return responseObject;
        } else {
            throw new MessageNotFoundException(String.format("Message not found for the meta entity %s", metaEntId));
        }
    }

    @Override
    public void setResponse(String res) {
        this.responseMsg = res;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    private String getAttributeValue(MessageAttribute messageAttribute) {
        String attributeValue = "";
        MessageAttributeType messageAttribType = messageAttribute.getMessageAttributeType();
        if (messageAttribType != null) {
            if (messageAttribType.getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.TRANSACTION_ID.toString())) {
                attributeValue = getTransactionId();
            } else if (messageAttribType.getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.DETAIL_MSG_INSTANCE_ID.toString())) {
                attributeValue = getInstanceId();
            } else {
                String implClass = messageAttribType.getDefaultValueClass();
                if (implClass != null) {
                    Object newObj = null;
                    try {
                        newObj = Class.forName(implClass).newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (newObj != null && newObj instanceof MessageAttributeTypeInterface) {
                        MessageAttributeTypeInterface formatter = (MessageAttributeTypeInterface) newObj;
                        attributeValue = formatter.getDefaultValue().toString();
                    }
                } else {
                    String expression = messageAttribute.getValueExpression();
                    if (!StringHelper.isEmpty(expression)) {
                        attributeValue = (String) ExpressionHelper.getExpressionResult(expression, String.class);
                    }

                }

            }
        }
        return attributeValue;
    }
}
