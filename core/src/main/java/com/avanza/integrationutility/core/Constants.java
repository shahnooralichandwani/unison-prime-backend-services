/**
 *
 */
package com.avanza.integrationutility.core;

/**
 * @author shahbaz.ali
 *
 */
public class Constants {

    public static final String EXPECTED_BYTES = "ExpectedBytes";
    public static final String RECIEVED_BYTES = "RecievedBytes";
    public static final String MESSAGE_COMPLETED = "MessageCompleted";
    public static final String MESSAGE_CHUNK = "MessageChunk";
    public static final String META_LENGTH = "metaLength";
    public static final String THREAD_INVOKED = "ThreadInvoked";

}
