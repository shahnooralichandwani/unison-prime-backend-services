/**
 *
 */
package com.avanza.integrationutility.core;

/**
 * @author shahbaz.ali This class will hold response message returned from the
 *         data source Also contains response code
 */
public class MessageResponseObject {

    String responseCode;
    Object responseObject;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Object getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(Object responseObject) {
        this.responseObject = responseObject;
    }

}
