package com.avanza.integrationutility.core;


public interface ChannelFactory {

    public void load(Object loadingConfigforChannels);

    public IntegrationChannel getChannel(String key);

}
