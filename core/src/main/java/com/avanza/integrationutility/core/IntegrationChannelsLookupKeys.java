package com.avanza.integrationutility.core;

public enum IntegrationChannelsLookupKeys {

    IP_ADDRESS("ipAddress"),
    PORT("port"),
    PROPERTIES("properties"),
    SELECTOR_CLASS("tranIdSelectorClass"),
    CONNECTION_TIMEOUT("connectionTimeOut"),
    XML_RENDERER("IntegrationChannel"),
    CLASS("class"),
    DELIMETER("delimeter"),
    DATE_FORMAT("dateFormat"),
    ENABLED("enabled"),
    KEY("key"),
    WAIT_TIMEOUT("waitTimeOut"),

    //MetaMessageFormat
    TRANSACTION_CODE("tranCode"),
    TRANSITION_ID("tranID");


    private String value;

    IntegrationChannelsLookupKeys(String val) {
        this.value = val;
    }


    public String toString() {
        return this.value;
    }

}
