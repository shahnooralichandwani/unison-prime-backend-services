/**
 *
 */
package com.avanza.integrationutility.core;

/**
 * @author shahbaz.ali
 *
 */
public interface TransactionIdentifierSelector {

    public String getTransitionId(String responseMessage);

}
