package com.avanza.integrationutility.core;

import java.util.HashMap;

import com.avanza.core.util.StringHelper;

public class IntegrationChannelProperties {

    private HashMap<String, String> propertiesMap = new HashMap<String, String>();

    public IntegrationChannelProperties() {

    }

    public String get(String key) {

        return this.propertiesMap.get(key);

    }

    public void put(String key, String Object) {

        this.propertiesMap.put(key, Object);
    }

    public void parse(String properties) {

        propertiesMap = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(properties, StringHelper.COMMA);

    }

}
