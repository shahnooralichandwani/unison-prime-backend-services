package com.avanza.integrationutility.messages;

import java.util.List;

import com.avanza.integrationutility.implementations.ResponseBean;

public class CustomerProfileResponse extends ResponseBean {

    /**
     *
     */
    private static final long serialVersionUID = 52450016599884297L;

    public String getCreditCardsCount() {
        return (String) get("creditCardsCount");
    }

    public void setCreditCardsCount(String creditCardsCount) {
        set("creditCardsCount", creditCardsCount);
    }

    public String getCurrentAccountsCount() {
        return (String) get("currentAccountsCount");
    }

    public void setCurrentAccountsCount(String currentAccountsCount) {
        set("currentAccountsCount", currentAccountsCount);
    }

    public String getFNCAccountsCount() {
        return (String) get("FNCAccountsCount");
    }

    public void setFNCAccountsCount(String accountsCount) {
        set("FNCAccountsCount", accountsCount);
    }

    public String getJAAAccountsCount() {
        return (String) get("JAAAccountsCount");
    }

    public void setJAAAccountsCount(String accountsCount) {
        set("JAAAccountsCount", accountsCount);
    }

    public String getLoanAccountsCount() {
        return (String) get("loanAccountsCount");
    }

    public void setLoanAccountsCount(String loanAccountsCount) {
        set("loanAccountsCount", loanAccountsCount);
    }

    public String getPBSAccountsCount() {
        return (String) get("PBSAccountsCount");
    }

    public void setPBSAccountsCount(String accountsCount) {
        set("PBSAccountsCount", accountsCount);
    }

    public List getProductDetails() {
        return (List) get("productDetails");
    }

    public void setProductDetails(List productDetails) {
        set("productDetails", productDetails);
    }

    public String getSAAAccountsCount() {
        return (String) get("SAAAccountsCount");
    }

    public void setSAAAccountsCount(String accountsCount) {
        set("SAAAccountsCount", accountsCount);
    }

    public String getWSAAccountsCount() {
        return (String) get("WSAAccountsCount");
    }

    public void setWSAAccountsCount(String accountsCount) {
        set("WSAAccountsCount", accountsCount);
    }

}
