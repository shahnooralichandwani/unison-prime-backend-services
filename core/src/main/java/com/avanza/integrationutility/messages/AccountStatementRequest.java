package com.avanza.integrationutility.messages;

import com.avanza.integrationutility.implementations.RequestBean;

public class AccountStatementRequest extends RequestBean {

    /**
     * Saad Shahid
     */
    private static final long serialVersionUID = -5639445512196692002L;

    public String getCIF() {
        return (String) get("cif");
    }

    public void setCIF(String cif) {
        set("cif", cif);
    }

    public String getAcctNumber() {
        return (String) get("acctNumber");
    }

    public void setAcctNumber(String acctNumber) {
        set("acctNumber", acctNumber);
    }

    public String getAcctType() {
        return (String) get("acctType");
    }

    public void setAcctType(String acctType) {
        set("acctType", acctType);
    }

    public String getTxnCount() {
        return (String) get("txnCount");
    }

    public void setTxnCount(String txnCount) {
        set("txnCount", txnCount);
    }
}
