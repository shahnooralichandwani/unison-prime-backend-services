package com.avanza.integrationutility.messages;

import java.util.List;

import com.avanza.integrationutility.implementations.ResponseBean;


public class AccountStatementResponse extends ResponseBean {

    /**
     * Saad Shahid
     */
    private static final long serialVersionUID = 7245021143279988079L;

    public List getAcctTxnDetails() {
        return (List) get("acctTxnDetails");
    }

    public void setAcctTxnDetails(List acctTxnDetails) {
        set("acctTxnDetails", acctTxnDetails);
    }


}
