package com.avanza.integrationutility.messages;

import com.avanza.integrationutility.implementations.BaseBean;

public class AccountStatement extends BaseBean {

    /**
     * Saad Shahid
     */
    private static final long serialVersionUID = -5127548786661701845L;

    public String getTxnDate() {
        return (String) get("txnDate");
    }

    public void setTxnDate(String txnDate) {
        set("txnDate", txnDate);
    }

    public String getSettlementDate() {
        return (String) get("settlementDate");
    }

    public void setSettlementDate(String settlementDate) {
        set("settlementDate", settlementDate);
    }

    public String getTxnType() {
        return (String) get("txnType");
    }

    public void setTxnType(String txnType) {
        set("txnType", txnType);
    }

    public String getTxnDescription() {
        return (String) get("txnDescription");
    }

    public void setTxnDescription(String txnDescription) {
        set("txnDescription", txnDescription);
    }

    public String getTxnAmount() {
        return (String) get("txnAmount");
    }

    public void setTxnAmount(String txnAmount) {
        set("txnAmount", txnAmount);
    }

    public String getTxnCurrency() {
        return (String) get("txnCurrency");
    }

    public void setTxnCurrency(String txnCurrency) {
        set("txnCurrency", txnCurrency);
    }

    public String getCheckNumber() {
        return (String) get("checkNumber");
    }

    public void setCheckNumber(String checkNumber) {
        set("checkNumber", checkNumber);
    }

    public String getOpeningBalance() {
        return (String) get("openingBalance");
    }

    public void setOpeningBalance(String openingBalance) {
        set("openingBalance", openingBalance);
    }

    public String getClosingBalance() {
        return (String) get("closingBalance");
    }

    public void setClosingBalance(String closingBalance) {
        set("closingBalance", closingBalance);
    }
}
