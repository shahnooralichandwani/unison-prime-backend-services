package com.avanza.integrationutility.messages;

import com.avanza.integrationutility.implementations.RequestBean;

public class CustomerProfileRequest extends RequestBean {

    /**
     *
     */
    private static final long serialVersionUID = 4812833284072792547L;

    public String getCustomerNumber() {
        return (String) get("customerNumber");
    }

    public void setCustomerNumber(String customerNumber) {
        set("customerNumber", customerNumber);
    }

}
