package com.avanza.integrationutility.implementations;


/**
 * @author akamran
 * <p>
 * This is the base class for all response beans
 */
public class ResponseBean extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = -121227691149208061L;

    public String getErrCode() {
        return (String) get("errCode");
    }

    public void setErrCode(String errCode) {
        set("errCode", errCode);
    }

    public String getErrDesc() {
        return (String) get("errDesc");
    }

    public void setErrDesc(String errDesc) {
        set("errDesc", errDesc);
    }

    public String getTranCode() {
        return (String) get("tranCode");
    }

    public void setTranCode(String tranCode) {
        set("tranCode", tranCode);
    }

    public String getTranID() {
        return (String) get("tranID");
    }

    public void setTranID(String tranID) {
        set("tranID", tranID);
    }


    public String getRequestIdentifier() {
        return (String) get("requestIdentifier");
    }

    public void setRequestIdentifier(String requestIdentifier) {
        set("requestIdentifier", requestIdentifier);
    }

}
