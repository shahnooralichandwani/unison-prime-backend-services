package com.avanza.integrationutility.implementations;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.future.CloseFuture;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.IoFuture;
import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.integrationutility.core.Constants;
import com.avanza.integrationutility.core.IntegrationChannel;
import com.avanza.integrationutility.core.IntegrationChannelProperties;
import com.avanza.integrationutility.core.MessageFormat;
import com.avanza.integrationutility.core.MessageResponseHandler;
import com.avanza.integrationutility.core.MessageUtility;
import com.avanza.integrationutility.core.TransactionIdentifierSelector;
import com.avanza.integrationutility.exceptions.ConnectionFailedException;

/**
 * @author shahbaz.ali
 * <p>
 * A default socket based implementation of IntegrationChannel
 */
public class SocketIntegrationChannel extends IntegrationChannel {

    Logger logger = Logger.getLogger(SocketIntegrationChannel.class);

    // transId,messageFormat
    private HashMap<String, MessageFormat> transitionsMap = new HashMap<String, MessageFormat>();

    // transId, threadObject
    private HashMap<String, Thread> threadWaitMap = new HashMap<String, Thread>();

    private String remoteAddress;
    private int port;
    private IoSession session;
    private NioSocketConnector connector;
    private long WAIT_TIMEOUT;
    private long CONNECTION_TIMOUT;
    private TransactionIdentifierSelector tranIdSelector;
    private IntegrationChannelProperties properties;

    /**
     * @param remoteAddress
     * @param port
     */
    public SocketIntegrationChannel() {

    }

    public synchronized boolean Connect() {
        logger.logInfo("Connect of DefaultImplementaion Called, ( SocketIntegrationChannel.Connect() )");

        // Here we try to connect to the specified address using port.
        // attach RequestHandler
        // and retry if unsuccessful.

        if (session != null && session.isConnected()) {
            throw new IllegalStateException("Already connected.");
        }

        try {
            connector = new NioSocketConnector(Runtime.getRuntime().availableProcessors() + 1);
            connector.setConnectTimeoutMillis(CONNECTION_TIMOUT);

            SocketAddress socketAddress = new InetSocketAddress(remoteAddress, port);
            IoFilter LOGGING_FILTER = new LoggingFilter();

            IoFilter CODEC_FILTER = new ProtocolCodecFilter(new SimpleCodecFactory(true));

            connector.getFilterChain().addLast("codec", CODEC_FILTER);
            connector.getFilterChain().addLast("logger", LOGGING_FILTER);

            connector.setHandler(new MessageResponseHandler(this));

            logger.logInfo("Establishing connection to " + remoteAddress + ":" + port + " ....");

            ConnectFuture connectFuture = connector.connect(socketAddress);

            connectFuture.await(CONNECTION_TIMOUT);
            if (!connectFuture.isConnected()) {

                logger.logInfo("Connection Failed to the specified host: " + remoteAddress + " at " + port);
                logger.LogException("Connection Failed to the specified host: " + remoteAddress + " at " + port, connectFuture.getException());
                return false;
            }

            logger.logInfo("HURRAY! Connection established with " + remoteAddress + ":" + port);
            session = connectFuture.getSession();

            return true;

        } catch (Exception e) {
            logger.LogException("Exception connecting Integration channel to " + remoteAddress + ":" + port + ", ( SocketIntegrationChannel.Connect() )", e);
            return false;
        }
    }

    public void disconnect() {

        // Get the close future for this session
        CloseFuture closeFuture = session.getCloseFuture();

        // Adding a listener to this close event
        closeFuture.addListener((IoFutureListener<?>) new IoFutureListener<IoFuture>() {
            @Override
            public void operationComplete(IoFuture future) {
                logger.logInfo("Connection closed successfully with " + remoteAddress + ":" + port + "...");
            }
        });

        // Do the close requesting that the pending messages are sent before
        // the session is closed
        logger.logInfo("Disconnecting with " + remoteAddress + ":" + port + "...");
        closeFuture.getSession().close(false);

        // Now wait for the close to be completed
        closeFuture.awaitUninterruptibly();

        // We can now dispose the connector
        connector.dispose();
    }

    public boolean isConnected() {
        return (session != null && session.isConnected());
    }

    private HashMap<String, MessageFormat> getTransactionsMap() {
        return transitionsMap;
    }

    private HashMap<String, Thread> getThreadWaitMap() {
        return threadWaitMap;
    }

    public void setTransactionsMap(HashMap<String, MessageFormat> transitionMap) {
        this.transitionsMap = transitionMap;
    }

    public void setThreadWaitMap(HashMap<String, Thread> threadWaitMap) {
        this.threadWaitMap = threadWaitMap;
    }

    private MessageFormat getCallBackResponse(String transactionId) {
        logger.logInfo("Get Callback response Called, ( SocketIntegrationChannel.getCallBackResponse() )");
        MessageFormat messageFormat = getTransactionsMap().get(transactionId);

        getTransactionsMap().remove(transactionId);
        getThreadWaitMap().remove(transactionId);

        return messageFormat;
    }

    public void notify(String transactionId, String message) {

        logger.logInfo("Response received, setting response and notifying the sleeping thread  ( SocketIntegrationChannel.notify() )");

        if (getTransactionsMap().containsKey(transactionId)) {
            MessageFormat msgFormat = getTransactionsMap().get(transactionId);
            msgFormat.setResponse(message);
            getTransactionsMap().put(transactionId, msgFormat);

            try {
                synchronized (getThreadWaitMap().get(transactionId)) {
                    getThreadWaitMap().get(transactionId).notify();
                }
            } catch (Exception e) {
                logger.LogException("Exception while notifying thread ( SocketIntegrationChannel.notify() )", e);
            }
        } else {
            // invalid transactionID
            logger.logInfo("Invalid Transaction ID recieved from response. ( SocketIntegrationChannel.notify() )");
        }


    }

    private void waitThread(String transactionId, Thread threadObject,
                            MessageFormat messageFormat) throws InterruptedException, TimeoutException {

        getTransactionsMap().put(transactionId, messageFormat);
        getThreadWaitMap().put(transactionId, threadObject);
        threadObject.wait(WAIT_TIMEOUT);

        Boolean isInvoked = (Boolean) session.getAttribute(Constants.THREAD_INVOKED);

        if (!isInvoked) {
            logger.logInfo("Response wait timeout.. could not get response from the server");
            throw new TimeoutException("Response wait timeout.. could not get response from the server");

        }

    }

    protected IntegrationChannel initialize(String address, int port, long connectionTimeOut, IntegrationChannelProperties prop, TransactionIdentifierSelector tranIdSelector, long waitTimeOut) {

        this.CONNECTION_TIMOUT = connectionTimeOut;
        this.remoteAddress = address;
        this.port = port;
        this.properties = prop;
        this.tranIdSelector = tranIdSelector;
        this.WAIT_TIMEOUT = waitTimeOut;
        return this;

    }

    @Override
    public Object send(MessageFormat messageFormat) throws Exception {
        /*
         * Shahbaz
         * This method is responsible of calling serialize method of MessageFormat implementation
         * and send message to the underlying connected HOST.
         * There are two default implmentations provided one is for Beans and other is for Meta Data Object.
         */

        if (!isConnected())
            this.Connect();

        try {
            if (isConnected()) {

                messageFormat.generateTransactionId();
                String transId = messageFormat.getTransactionId();

                String messageToWrite = messageFormat.serialize();

                //Append MetaLength if available from channel
                String metaLength = this.properties.get(Constants.META_LENGTH);

                if (metaLength != null) {
                    try {
                        String withMetaLengh = MessageUtility.cushionField(String.valueOf(messageToWrite.length()), Integer.parseInt(metaLength), '0');
                        messageToWrite = withMetaLengh + messageToWrite;
                    } catch (NumberFormatException nfe) {
                        logger.logInfo("Error while append meta length to request message");
                    }
                }

                //write meta length in axis session to be used while deserializing the response
                session.setAttribute(Constants.META_LENGTH, metaLength);

                // Send message to opened session
                WriteFuture writefuture = session.write(messageToWrite);

                // Wait until the message is completely written out
                writefuture.awaitUninterruptibly(CONNECTION_TIMOUT);

                if (writefuture.isWritten()) {

                    // The message has been written successfully.
                    synchronized (Thread.currentThread()) {
                        session.setAttribute(Constants.THREAD_INVOKED, false);
                        waitThread(transId, Thread.currentThread(), messageFormat);
                    }
                    MessageFormat responseMessageFormat = getCallBackResponse(transId);

                    if (StringHelper.isEmpty(responseMessageFormat.getResponse())) {
                        logger.logInfo(String.format("Response not recieved for STAN %s from the data source Or an invalid STAN recieved", transId));
                        throw new Exception(String.format("Response not recieved for STAN %s from the data source Or an invalid STAN recieved", transId));
                    }

                    return responseMessageFormat.deserialize();
                } else {
                    logger.LogException("Couldn't send message", writefuture.getException());
                    return null;
                }
            } else {
                logger.logInfo("Connection not established, Connect to host before sending message ( SocketIntegrationChannel.send() )");
                throw new ConnectionFailedException("Connection could not be established to " + remoteAddress + ":" + port);
            }

        } catch (InterruptedException inexc) {
            logger.LogException("InterruptedException while sending message ( SocketIntegrationChannel.send() )", inexc);
            throw inexc;

        } catch (TimeoutException toex) {
            logger.LogException("InterruptedException while sending message ( SocketIntegrationChannel.send() )", toex);
            throw toex;

        } catch (Exception e) {
            logger.LogException("Exception while sending message ( SocketIntegrationChannel.send() )", e);
            throw e;
        } finally {
            if (isConnected())
                disconnect();
        }


    }


    @Override
    public Object send(MessageFormat[] messageFormats) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected String getResponseTranId(String response) {

        return tranIdSelector.getTransitionId(response);

    }
}
