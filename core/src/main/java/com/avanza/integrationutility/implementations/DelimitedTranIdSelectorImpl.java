/**
 *
 */
package com.avanza.integrationutility.implementations;

import java.util.HashMap;

import com.avanza.core.util.StringHelper;
import com.avanza.integrationutility.core.TransactionIdentifierSelector;

/**
 * @author shahbaz.ali
 *
 */
public class DelimitedTranIdSelectorImpl implements TransactionIdentifierSelector {

    HashMap<String, String> channelProperties;

    public DelimitedTranIdSelectorImpl(String channelPropertiesstr) {
        this.channelProperties = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(channelPropertiesstr, StringHelper.COMMA);
    }

    @Override
    public String getTransitionId(String responseMessage) {


        String[] splitString = responseMessage.split(channelProperties.get("responseHeaderDelimiter"));
        int tranId_position = Integer.parseInt(channelProperties.get("transactionIdPosition"));

        if (splitString.length > 0) {
            String temp = splitString[tranId_position - 1];
            return temp;
        } else {
            return "";
        }
    }

}
