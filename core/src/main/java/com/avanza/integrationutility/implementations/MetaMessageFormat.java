/**
 *
 */
package com.avanza.integrationutility.implementations;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//import org.hibernate.util.StringHelper; changed by rehan.ahmed

import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.core.util.MessageParser;
import com.avanza.core.util.StringHelper;
import com.avanza.integrationutility.core.IntegrationChannelsLookupKeys;
import com.avanza.integrationutility.core.MessageFormat;

/**
 * @author shahbaz.ali
 * 	       This class provides implementation of MessageFormat for
 *         preparing request from, and parsing response string to DataObject
 *         This also provides support to having multiple responses as
 *         DataObjects. On deserializing, multiple responses are stored in main response
 *         dataobject via its parameter name.
 */

public class MetaMessageFormat implements MessageFormat {

    private Logger logger = Logger.getLogger(MetaMessageFormat.class);
    private DataObject dataObject;
    private String transitionId;
    private String transactonCode;
    private String responseMsg;

    public MetaMessageFormat(
            DataObject dataObject,
            String transactionCode,
            String transitionId) {
        this.dataObject = dataObject;
        this.transactonCode = transactionCode;
        this.transitionId = transitionId;
    }

    public String getResponse() {

        return responseMsg;
    }

    public String getTransactionCode() {
        return this.transactonCode;
    }

    public String serialize() {

        logger.logInfo("Generating message to be send, MetaMessageFormat.generateRequestMessage");
        Transaction transaction = TransactionConfig.getTransaction(getTransactionCode());

        StringBuffer message = new StringBuffer();

        StringBuffer tempMessage = new StringBuffer();
        message.append(TransactionConfig.getProperty("STX"));
        tempMessage.append(TransactionConfig.getProperty("STX"));
        Iterator itr = transaction.getRequestBlock().iterator();
        while (itr.hasNext()) {
            TransactionParameter parameter = (TransactionParameter) itr.next();
            String name = parameter.getName();

            String delimiterPlace = parameter.getDelimiterPlace();
            String delimiter = parameter.getDelimiter();
            String defaultValue = parameter.getDefault();
            String maskValue = parameter.getMask();
            String isMetaValue = parameter.getIsMetaValue();
            String maxLength = parameter.getMaxLength();

            String value = this.dataObject.getAsString(name);

            if (defaultValue != null && !defaultValue.equals("")) {
                value = defaultValue;
            }

            if (delimiterPlace.equalsIgnoreCase("E")) {
                message.append(value).append(delimiter);

                if (maskValue.trim().equals("Y")) {

                    tempMessage.append("****").append(delimiter);
                } else {

                    tempMessage.append(value).append(delimiter);
                }

            } else {
                message.append(delimiter).append(value);
                if (maskValue.trim().equals("Y")) {

                    tempMessage.append(delimiter).append("****");
                } else {

                    tempMessage.append(delimiter).append(value);
                }
            }

        }
        message.append(TransactionConfig.getProperty("ETX"));
        char endMarker = 13;
        try {
            endMarker = (char) Integer.parseInt(TransactionConfig.getProperty("endMarker"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (TransactionConfig.getProperty("enableMetaLength").equals("Y")) {
            StringBuffer message2 = new StringBuffer();
            message2.append(message);
            message = new StringBuffer();
            String cushion = cushionFieldWithZero(message2.length() + "", Integer.parseInt(TransactionConfig.getProperty("metaLength")));
            message.append(cushion);
            message.append(message2);

            message2 = new StringBuffer();
            message2.append(tempMessage);
            tempMessage = new StringBuffer();
            tempMessage.append(cushion);
            tempMessage.append(message2);
        }

        if (TransactionConfig.getProperty("enableMetaLength").equals("N")) {
            message.append(endMarker);
            tempMessage.append(endMarker);
        }

        logger.logInfo("Message succussfully generated, MetaMessageFormat.generateRequestMessage");
        return message.toString();
    }

    @SuppressWarnings("unchecked")
    public List<DataObject> deserialize() {
        logger.logInfo("Deserializing response. ( MetaMessageFormat.deserialize() )");
        List<DataObject> responseBeansList = new ArrayList<DataObject>();

        Transaction transaction = TransactionConfig.getTransaction(getTransactionCode());
        String metaEntId = transaction.getMetaEntityId();
        DataObject responseDataObject = MetaDataRegistry.getEntityBySystemName(metaEntId).createEntity();

        try {
            MessageParser parser = new MessageParser(getResponse());
            Iterator itr = transaction.getResponseBlock().iterator();
            while (itr.hasNext()) {
                TransactionParameter parameter = (TransactionParameter) itr.next();
                String subMetaEntId = parameter.getMetaEntityId();
                if (StringHelper.isNotEmpty(subMetaEntId)) {
                    DataObject responseChildDataObject = MetaDataRegistry.getEntityBySystemName(subMetaEntId).createEntity();
                    List<DataObject> list = new ArrayList<DataObject>();
                    int count = 0;
                    count = Integer.parseInt(setValue(parameter, null, parser));
                    for (int i = 0; i < count; i++) {

                        Iterator subItr = parameter.getParameters().iterator();
                        while (subItr.hasNext()) {
                            TransactionParameter innerParameter = (TransactionParameter) subItr.next();
                            setValue(innerParameter, responseChildDataObject, parser);
                        }
                        list.add(responseChildDataObject);
                    }
                    responseDataObject.setValue(parameter.getName(), list);
                } else {
                    setValue(parameter, responseDataObject, parser);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        logger.logInfo("Successfully parse the response, ( MetaMessageFormat.deserialize() )");
        responseBeansList.add(responseDataObject);
        return responseBeansList;
    }

    private String cushionFieldWithZero(String field, int fieldSize) {
        try {
            if (field.length() == fieldSize) {
                return field;
            } else if (field.length() < fieldSize) {
                while (field.length() != fieldSize) {
                    field = "0" + field;
                }
                return field;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return field;
    }

    private String setValue(TransactionParameter parameter,
                            DataObject responseObject, MessageParser parser) {
        String value = "";
        String name = parameter.getName();
        String delimiterPlace = parameter.getDelimiterPlace();
        String delimiter = parameter.getDelimiter();
        String isMetaValue = parameter.getIsMetaValue();

        int ignore = -1;
        int maxLength = 0;
        try {
            ignore = Integer.parseInt(parameter.getIgnore());
        } catch (NumberFormatException e) {
            logger.LogException("NumberFormatException thrown while sting value to dataobject, ( MetaMessageFormat.setValue() )", e);
        }
        try {
            maxLength = Integer.parseInt(parameter.getMaxLength());
        } catch (NumberFormatException e) {
            logger.LogException("NumberFormatException thrown while sting value to dataobject, ( MetaMessageFormat.setValue() )", e);
        }
        if (delimiterPlace.equalsIgnoreCase("E")) {
            value = parser.getNext(delimiter);
        } else {
            if (isMetaValue.equals("Y")) {

            } else {
                parser.ignore(1);
            }

            value = parser.getNext(maxLength);
        }
        if (responseObject != null)
            responseObject.setValue(name, value);
        if (ignore != -1)
            parser.ignore(ignore);
        return value;
    }

    public void setResponse(String res) {
        this.responseMsg = res;

    }

    public String getTransactionId() {
        return this.transitionId;
    }

    public void generateTransactionId() {
        transitionId = this.getTransactionCode() + java.util.Calendar.getInstance().getTimeInMillis();
        dataObject.getValues().put(IntegrationChannelsLookupKeys.TRANSITION_ID.toString(), transitionId);
    }

}
