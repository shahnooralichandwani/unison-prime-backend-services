package com.avanza.integrationutility.implementations;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author AVerani
 * <p>
 * This is the base class for request and response beans
 */
public class BaseBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6441831726563184657L;

    private HashMap properties = new HashMap();

    public void set(Object key, Object value) {
        properties.put(key, value);
    }

    public Object get(Object key) {
        return properties.get(key);
    }

    public HashMap getMap() {
        return this.properties;
    }

}
