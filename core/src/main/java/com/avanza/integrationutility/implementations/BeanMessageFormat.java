/**
 *
 */
package com.avanza.integrationutility.implementations;

import com.avanza.integrationutility.core.MessageFormat;

/**
 *    @author shahbaz.ali
 *
 * 	       This class provides implementation of MessageFormat for
 *         preparing request from, and parsing response string to Beans
 *         This also provides support to having multiple responses as
 *         ResponseBeans. On deserializing, multiple responses are stored in main response
 *         bean object with the via parameter name.
 */


public class BeanMessageFormat implements MessageFormat<RequestBean, ResponseBean> {

    private RequestBean bean;
    private String responseMsg;

    public BeanMessageFormat(RequestBean bean) {
        this.bean = bean;
    }


    @SuppressWarnings("unchecked")
    public ResponseBean deserialize() {
        Transaction transaction = TransactionConfig.getTransaction(getTransactionCode());

        ResponseBean responseBean = transaction.generateResponseBean(getResponse());

        return responseBean;
    }


    public String getResponse() {
        return responseMsg;
    }


    public String getTransactionCode() {
        return this.bean.getTranCode();
    }


    public String serialize() {
        Transaction transaction = TransactionConfig.getTransaction(getTransactionCode());
        return transaction.generateRequestMessage(bean);
    }


    public void setResponse(String res) {
        this.responseMsg = res;

    }

    public String getTransactionId() {
        return this.bean.getTranID();
    }


    public void generateTransactionId() {
        String tranId = java.util.Calendar.getInstance().getTimeInMillis() + "";
        this.bean.setTranID(tranId);

    }

}
