/**
 *
 */
package com.avanza.integrationutility.implementations;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.avanza.core.util.StringHelper;
import com.avanza.integrationutility.core.TransactionIdentifierSelector;

/**
 * @author shahbaz.ali
 *
 */
public class XmlBasedTranIdSelectorImpl implements
        TransactionIdentifierSelector {

    HashMap<String, String> channelProperties;

    public XmlBasedTranIdSelectorImpl(String channelPropertiesstr) {
        this.channelProperties = (HashMap<String, String>) StringHelper
                .getPropertiesMapFrom(channelPropertiesstr, StringHelper.COMMA);
    }

    @Override
    public String getTransitionId(String responseMessage) {

        try {

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(responseMessage));

            Document doc = db.parse(is);
            NodeList nodes = doc.getChildNodes();

            if (nodes.getLength() > 0) {
                NodeList transaction = nodes.item(0).getChildNodes();
                if (transaction.getLength() > 0)
                    return transaction.item(0).getTextContent();
                else
                    return "";
            } else
                return "";

        } catch (DOMException e) {
            e.printStackTrace();
            return "";
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return "";
        } catch (SAXException e) {
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

}
