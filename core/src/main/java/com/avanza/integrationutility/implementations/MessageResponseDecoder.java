package com.avanza.integrationutility.implementations;

import java.nio.charset.Charset;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderAdapter;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import com.avanza.core.util.Logger;
import com.avanza.integrationutility.core.Constants;
import com.avanza.integrationutility.exceptions.InsufficientResponseSizeException;

/**
 * @author shahbaz.ali
 * This decoder will be responsible to add chunks of message in Mina Session if the message is
 * distributed in chunks
 * only return the response when recievedByte is equal to expectedBytes
 */

public class MessageResponseDecoder extends ProtocolDecoderAdapter {

    private final Logger logger = Logger.getLogger(MessageResponseDecoder.class);

    public void decode(IoSession session, IoBuffer in, ProtocolDecoderOutput out)
            throws Exception {

        Integer bytesExpected = null; // flag for bytesExpected
        Integer bytesRecieved = null; // flag for bytesRecieved
        String metaLengthStr = (String) session.getAttribute(Constants.META_LENGTH);

        if (metaLengthStr != null) {
            Integer metaLength = Integer.parseInt(metaLengthStr);
            String message = in.getString(Charset.forName("ISO-8859-1").newDecoder());
            int length = message.length();

            if (length < metaLength) {
                throw new InsufficientResponseSizeException("Invalid Response size");
            }

            if (session.getAttribute(Constants.EXPECTED_BYTES) == null) { // new message
                bytesExpected = metaLength + Integer.parseInt(message.substring(0, metaLength));
                session.setAttribute(Constants.EXPECTED_BYTES, bytesExpected);
            } else { // old message
                bytesExpected = (Integer) session.getAttribute(Constants.EXPECTED_BYTES);
            }

            if (session.getAttribute(Constants.RECIEVED_BYTES) == null) { // new message
                bytesRecieved = length;
                session.setAttribute(Constants.RECIEVED_BYTES, bytesRecieved);
            } else { // old message
                bytesRecieved = (Integer) session.getAttribute(Constants.RECIEVED_BYTES);
                bytesRecieved += length;
                session.setAttribute(Constants.RECIEVED_BYTES, bytesRecieved);
            }

            Boolean isFinished = false; // flag for checking response is finished or not

            // Checking Response is finished or not
            if (bytesRecieved.equals(bytesExpected)) { // response is finished
                logger.logInfo("Complete reponse is recieved");
                logger.logInfo("bytesExpected:" + bytesExpected + "=bytesRecieved:" + bytesRecieved);
                isFinished = true;
                session.setAttribute(Constants.EXPECTED_BYTES, null);
                session.setAttribute(Constants.RECIEVED_BYTES, null);
            } else { // response is not finished
                logger.logInfo("Complete reponse is not recieved uptil now...");
                logger.logInfo("bytesExpected:" + bytesExpected + "=bytesRecieved:" + bytesRecieved);
            }

            session.setAttribute(Constants.MESSAGE_COMPLETED, isFinished);

            logger.logDebug("is Response Finsihed?" + isFinished);

            out.write(message);

        } else {
            String message = in.getString(Charset.forName("ISO-8859-1").newDecoder());
            session.setAttribute(Constants.MESSAGE_COMPLETED, true);
            out.write(message);
        }


    }
}
