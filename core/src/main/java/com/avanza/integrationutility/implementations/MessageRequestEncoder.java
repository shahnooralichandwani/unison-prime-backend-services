package com.avanza.integrationutility.implementations;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

public class MessageRequestEncoder extends ProtocolEncoderAdapter {

    public void encode(IoSession session, Object message, ProtocolEncoderOutput out)
            throws Exception {
        String receievedMessage = message.toString();

        byte[] bytes = receievedMessage.getBytes();

        int capacity = bytes.length;
        IoBuffer buffer = IoBuffer.allocate(capacity, false);

        buffer.put(bytes);

        buffer.flip();
        out.write(buffer);
    }

}
