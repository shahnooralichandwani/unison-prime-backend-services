package com.avanza.integrationutility.implementations;

/**
 * @author akamran
 */
public class TransactionParameter {

    private String name = "";

    private String defaultValue = "";

    private String maxLength = "";

    private String delimiter = "";

    private String delimiterPlace = "";

    private String bean = "";

    private String ignore = "";

    private String mask = "";

    private String isMetaValue = "";

    private String metaEntityId = "";

    private TransactionParameterCollection parameters;

    public String getDefault() {
        return defaultValue;
    }

    public void setDefault(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public String getDelimiterPlace() {
        return delimiterPlace;
    }

    public void setDelimiterPlace(String delimiterPlace) {
        this.delimiterPlace = delimiterPlace;
    }

    public String getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBean() {
        return bean;
    }

    public void setBean(String beanName) {
        this.bean = beanName;
    }

    public TransactionParameterCollection getParameters() {
        return parameters;
    }

    public void setParameters(TransactionParameterCollection parameters) {
        this.parameters = parameters;
    }

    public void addParameter(TransactionParameter parameter) {
        if (this.parameters == null)
            this.parameters = new TransactionParameterCollection();
        this.parameters.addParameter(parameter);
    }

    public String getIgnore() {
        return ignore;
    }

    public void setIgnore(String ignore) {
        this.ignore = ignore;
    }

    public String getMask() {
        return this.mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public String getIsMetaValue() {
        return isMetaValue;
    }

    public void setIsMetaValue(String isMetaValue) {
        this.isMetaValue = isMetaValue;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

}
