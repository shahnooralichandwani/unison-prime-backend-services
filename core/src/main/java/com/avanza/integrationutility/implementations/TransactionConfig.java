package com.avanza.integrationutility.implementations;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Properties;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.xmlrules.DigesterLoader;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.avanza.core.util.Logger;

/**
 * @author Shahbaz Ali
 */
public class TransactionConfig {
    /**
     * Fetches the variable values
     *
     * @param name
     * : name of the varible, whose value wants to be fetched.
     * @return the value of the specified variable
     */
    private static Properties defaultProps;
    private static TransactionCollection transactions;
    private static InputStream file;
    private static Logger logger = Logger.getLogger(TransactionConfig.class);

    private TransactionConfig() {

    }

    public static void initialize() {
        try {
            logger.logInfo("Initializing Network Integration Configurations and Transactions,  TransactionConfig.initialize()");
            defaultProps = new Properties();

            System.out.println(TransactionConfig.class.getClassLoader());
            file = TransactionConfig.class.getResourceAsStream("/config.properties");

            BufferedInputStream in = new BufferedInputStream(file);
            defaultProps.load(in);
            in.close();

            loadTransactions();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            logger.LogException("FileNotFoundException in TransactionConfig.initialize -- config.properties file not found", e);
        } catch (IOException e) {
            e.printStackTrace();
            logger.LogException("IOException in TransactionConfig.initialize -- unable to read configurations", e);
        }

    }

    public static void loadTransactions() {
        logger.logInfo("Loading transactions from Transaction Config,  TransactionConfig.loadTransactions()");
        InputSource rules = null;
        InputSource file = null;

        rules = new InputSource(TransactionConfig.class.getResourceAsStream("/" + getProperty("digesterFileName")));
        file = new InputSource(TransactionConfig.class.getResourceAsStream("/" + getProperty("transactionsFileName")));

        Digester digester = null;
        try {
            digester = DigesterLoader.createDigester(rules);
            digester.setValidating(false);
            transactions = (TransactionCollection) digester.parse(file);
        } catch (MalformedURLException e) {
            logger.LogException("Invalid URL for transaction format file", e);

        } catch (IOException e) {
            e.printStackTrace();
            logger.LogException("Unable to read transaction format file", e);

        } catch (SAXException e) {
            e.printStackTrace();
            logger.LogException("Unable to parse the transaction format xml", e);

        } catch (Exception e) {
            e.printStackTrace();
            logger.LogException("Unable to parse the transaction format xml", e);
        }
        logger.logInfo("Done... Loading transactions from Transaction Config");
    }

    public static String getProperty(String name) {
        String str = "";
        str = defaultProps.getProperty(name);

        if ((str != null) && !str.equalsIgnoreCase("")) {
            return str.trim();

        }
        return str;
    }

    public static void destroy() {
        defaultProps = null;

    }

    public static Transaction getTransaction(String tranCode) {
        return transactions.getTransaction(tranCode);
    }

}
