package com.avanza.integrationutility.implementations;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
//import org.apache.axis2.AxisFault;
//import org.apache.axis2.Constants;
//import org.apache.axis2.addressing.AddressingConstants;
//import org.apache.axis2.addressing.EndpointReference;
//import org.apache.axis2.client.Options;
//import org.apache.axis2.client.ServiceClient;
//import org.apache.axis2.transport.http.HTTPConstants;
//import org.apache.commons.httpclient.HttpClient;
//import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.xerces.parsers.SAXParser;
import org.apache.xerces.xni.parser.XMLDocumentSource;
import org.xml.sax.InputSource;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.config.ViewContext;
import com.avanza.core.web.config.WebContext;
import com.avanza.integrationutility.core.DetailMessageXmlFormat;
import com.avanza.integrationutility.core.IntegrationChannel;
import com.avanza.integrationutility.core.IntegrationChannelProperties;
import com.avanza.integrationutility.core.ListMessageXmlFormat;
import com.avanza.integrationutility.core.MessageFormat;
import com.avanza.integrationutility.core.TransactionIdentifierSelector;
import com.avanza.integrationutility.exceptions.ConnectionFailedException;


/**
 * @author nasir.nawab
 * <p>
 * A default Web Service based implementation of IntegrationChannel
 */

public class WebServiceIntegrationChannel extends IntegrationChannel {

    Logger logger = Logger.getLogger(WebServiceIntegrationChannel.class);

    private String serviceAddress;
    private int port;
    private TransactionIdentifierSelector tranIdSelector;
    private IntegrationChannelProperties properties;
    private static final String LIST_SERVICE_NAME = "listservicemethod";
    private static final String DETAIL_SERVICE_NAME = "detailservicemethod";
    private long CONNECTION_TIMOUT;

    @Override
    protected boolean Connect() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    protected String getResponseTranId(String response) {
        return tranIdSelector.getTransitionId(response);
    }

    @Override
    protected IntegrationChannel initialize(String address, int port, long connectionTimeOut, IntegrationChannelProperties prop, TransactionIdentifierSelector tranIdSelector, long waitTimeOut) {

        this.serviceAddress = address;
        this.port = port;
        this.properties = prop;
        this.tranIdSelector = tranIdSelector;
        this.CONNECTION_TIMOUT = connectionTimeOut;
        return this;
    }

    @Override
    public boolean isConnected() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    protected void notify(String transactionId, String message) {
        // TODO Auto-generated method stub
    }

    @Override
    public Object send(MessageFormat messageFormat) throws Exception {
	/*	logger.logInfo("Sending Message  ( WebServiceIntegrationChannel.send(MessageFormat messageFormat) )");
		String serviceName="";
		
		try{
			if(serviceAddress != null && StringHelper.isNotEmpty(serviceAddress)){

				//Generate trannsaction id only if a call is a
				//transaction, otherwise skip -- shoaib.rehman
				messageFormat.generateTransactionId();
				String message = messageFormat.serialize();
				String endPoint= serviceAddress;
				
				//Added By Shafique Rehman
				SOAPMessage soapMessage=getSoapMessageFromString(message);
				
				if(messageFormat instanceof ListMessageXmlFormat)
					serviceName=this.properties.get(LIST_SERVICE_NAME);
				if(messageFormat instanceof DetailMessageXmlFormat)
					serviceName=this.properties.get(DETAIL_SERVICE_NAME);
				
				
				//This line is committed because no use of axis2 API for SOAP webservice
//				String responseMessage = callService(endPoint,serviceName,message); 
				
				//Added By Shafique Rehman
				String responseMessage = callSoapService(endPoint,soapMessage);
				
				messageFormat.setResponse(responseMessage);
				
				return  messageFormat.deserialize();
			}
			else {
				logger.logInfo("Service address is not valid ( WebServiceIntegrationChannel.send() )");
				return null;
			}

		}
		catch (AxisFault f) {
		    throw new ConnectionFailedException("Connection could not be established to "+serviceAddress + ":" + serviceName);
		}
		catch (Exception e) {
			logger.LogException("Exception while sending message ( WebServiceIntegrationChannel.send() )", e);
			throw e;
		}*/
        return null;
    }

    public String callService(String endPoint, String serviceName, String arg) throws Exception {
	/*	logger.logInfo("Calling Web Service "+serviceName+"  ( WebServiceIntegrationChannel.callService(String endPoint, String serviceName, String arg) )");
		WebContext webContext = ApplicationContext.getContext().get(WebContext.class.getName());
		ViewContext viewContext = ApplicationContext.getContext().get(ViewContext.class);
		MultiThreadedHttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
		HttpClient httpClient = new HttpClient(httpConnectionManager);
		ServiceClient client = null;
		OMElement response = null;
		String serviceResponse;
		
		try {

			client = new ServiceClient(null, null);
			EndpointReference targetEPR = new EndpointReference(endPoint);
			Options opts = new Options();
			opts.setTimeOutInMilliSeconds(CONNECTION_TIMOUT);
			client.setOptions(opts);
			opts.setTo(targetEPR);
			opts.setAction("urn:"+serviceName);

			OMElement serviceElement = getServiceElement(arg,serviceName);

			client.getOptions().setProperty(HTTPConstants.REUSE_HTTP_CLIENT, Constants.VALUE_TRUE);
			client.getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, httpClient);
			client.getOptions().setProperty(AddressingConstants.WS_ADDRESSING_VERSION,AddressingConstants.Submission.WSA_NAMESPACE);
			response = client.sendReceive(serviceElement);
			serviceResponse = unshellResponseMessage(response.toString());
			logger.logInfo("XML Response: " + serviceResponse);
			
		}
		
		catch (Exception e) {
			logger.LogException("Exception while calling Service ( WebServiceIntegrationChannel.callService(String endPoint, String serviceName, String arg) )", e);
			throw e;
		}
		finally {
			if (client != null) {
				try {
					client.cleanupTransport();
					client.cleanup();
					httpConnectionManager.closeIdleConnections(0);
					httpConnectionManager.shutdown();
				} catch (Exception e) {
					logger.LogException("Exception while cleaning HttpClient ( WebServiceIntegrationChannel.callService(String endPoint, String serviceName, String arg) )", e);
					throw e;
				}
			}


		}
		serviceResponse = serviceResponse.replaceAll("&lt;","<");
		serviceResponse = serviceResponse.replaceAll("&gt;",">");
		
		return serviceResponse;*/
        return null;
    }


    /**
     * @return String
     * @author shafique.rehman
     * //* @param String endPoint,SOAPMessage soapMessage
     */

    public String callSoapService(String endPoint, SOAPMessage soapMessage) throws Exception {

        logger.logInfo("Calling Web Service " + endPoint + "  ( WebServiceIntegrationChannel.callSoapService(String endPoint,SOAPMessage soapMessage)");

        String serviceResponse = null;

        logger.logInfo("Soap Web Service End Point: " + endPoint);

        logger.logInfo("Creating SOAP Connection...");
        // Create SOAP Connection
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = null;

        try {

            soapConnection = soapConnectionFactory.createConnection();

            logger.logInfo("Created SOAP Connection...");

            logger.logInfo("Send SOAP Message to SOAP Server");

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(soapMessage, endPoint);

            logger.logInfo("SOAP Message response recieved..");

            logger.logInfo("Converting SOAPMessage into String..");

            // Print SOAP Response
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            Source sourceContent = soapResponse.getSOAPPart().getContent();
            System.out.print("\nResponse SOAP Message = ");

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            StringWriter writer = new StringWriter();
            transformer.transform(sourceContent, new StreamResult(writer));

            String responseStr = writer.getBuffer().toString();

            logger.logInfo("Response SOAP Message (withoutunshell) =" + responseStr);

            serviceResponse = unshellSOAPResponseMessage(responseStr);
            logger.logInfo("Response SOAP Message (unshell response) =" + serviceResponse);
            System.out.print(responseStr);

        } catch (Exception e) {
            logger.LogException("Exception while calling Service ( WebServiceIntegrationChannel.callSMService(String endPoint, String serviceName, String rim) )", e);
            throw e;
        } finally {

            if (soapConnection != null) {
                try {
                    soapConnection.close();
                } catch (Exception e) {
                    logger.LogException("Exception while cleaning HttpClient ( WebServiceIntegrationChannel.callService(String endPoint, String serviceName, String arg) )", e);
                    throw e;
                }
            }

        }


        return serviceResponse;

    }

    protected String unshellResponseMessage(String str) {
        return str.substring(str.indexOf("ns:return") + 10, str.indexOf("/ns:return") - 1);
    }

    protected String unshellSOAPResponseMessage(String str) {
        return str.substring(str.indexOf("<SOAP-ENV:Body>") + 15, str.indexOf("</SOAP-ENV:Body>"));
    }

    private OMElement getServiceElement(String value, String serviceName) {
        OMFactory factory = OMAbstractFactory.getOMFactory();
        OMNamespace ns = factory.createOMNamespace("http://core.webservice.avanza.com", "ns1");
        OMElement elem = factory.createOMElement(serviceName, ns);
        OMElement childElem = factory.createOMElement("x0", null);
        XMLInputFactory xmlFactory = XMLInputFactory.newInstance();
        XMLStreamReader xmlReader = null;
        try {
            xmlReader = xmlFactory.createXMLStreamReader(new StringReader(value));
        } catch (XMLStreamException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        StAXOMBuilder axOMbuilter = new StAXOMBuilder(xmlReader);
        axOMbuilter.getDocumentElement();
        //childElem.addChild(axOMbuilter.getDocumentElement());
        childElem.setText(value);
        elem.addChild(childElem);

        return elem;
    }


    @Override
    public Object send(MessageFormat[] messageFormats) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }


    private SOAPMessage getSoapMessageFromString(String xml) throws SOAPException, IOException {
        InputStream is = new ByteArrayInputStream(xml.getBytes());
        SOAPMessage message = MessageFactory.newInstance().createMessage(null, is);
        return message;
    }

}
