package com.avanza.integrationutility.implementations;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.avanza.core.util.Guard;
import com.avanza.core.util.Logger;
import com.avanza.core.util.MessageParser;

/**
 * @author akamran
 */
public class Transaction {

    private static Logger logger = Logger.getLogger(Transaction.class);

    private String code;

    private String name;

    private String requestBean;

    private String responseBean;

    private String metaEntityId;

    private TransactionParameterCollection requestBlock = new TransactionParameterCollection();

    private TransactionParameterCollection responseBlock = new TransactionParameterCollection();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TransactionParameterCollection getRequestBlock() {
        return requestBlock;
    }

    public void setRequestBlock(TransactionParameterCollection requestBlock) {
        this.requestBlock = requestBlock;
    }

    public TransactionParameterCollection getResponseBlock() {
        return responseBlock;
    }

    public void setResponseBlock(TransactionParameterCollection responseBlock) {
        this.responseBlock = responseBlock;
    }

    public String generateRequestMessage(RequestBean request) {

        logger.logInfo("Generating message to be send, Transaction.generateRequestMessage");

        StringBuffer message = new StringBuffer();

        StringBuffer tempMessage = new StringBuffer();
        message.append(TransactionConfig.getProperty("STX"));
        tempMessage.append(TransactionConfig.getProperty("STX"));
        Iterator itr = this.requestBlock.iterator();
        while (itr.hasNext()) {
            TransactionParameter parameter = (TransactionParameter) itr.next();
            String name = parameter.getName();

            String delimiterPlace = parameter.getDelimiterPlace();
            String delimiter = parameter.getDelimiter();
            String defaultValue = parameter.getDefault();
            String maskValue = parameter.getMask();
            String isMetaValue = parameter.getIsMetaValue();
            String maxLength = parameter.getMaxLength();

            String value = (String) request.get(name);

            if (defaultValue != null && !defaultValue.equals("")) {
                value = defaultValue;
            }

            if (delimiterPlace.equalsIgnoreCase("E")) {
                message.append(value).append(delimiter);

                if (maskValue.trim().equals("Y")) {

                    tempMessage.append("****").append(delimiter);
                } else {

                    tempMessage.append(value).append(delimiter);
                }

            } else {
                message.append(delimiter).append(value);
                if (maskValue.trim().equals("Y")) {

                    tempMessage.append(delimiter).append("****");
                } else {

                    tempMessage.append(delimiter).append(value);
                }
            }

        }
        message.append(TransactionConfig.getProperty("ETX"));
        char endMarker = 13;
        try {
            endMarker = (char) Integer.parseInt(TransactionConfig.getProperty("endMarker"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (TransactionConfig.getProperty("enableMetaLength").equals("Y")) {
            StringBuffer message2 = new StringBuffer();
            message2.append(message);
            message = new StringBuffer();
            String cushion = cushionFieldWithZero(message2.length() + "", Integer.parseInt(TransactionConfig.getProperty("metaLength")));
            message.append(cushion);
            message.append(message2);

            message2 = new StringBuffer();
            message2.append(tempMessage);
            tempMessage = new StringBuffer();
            tempMessage.append(cushion);
            tempMessage.append(message2);
        }

        if (TransactionConfig.getProperty("enableMetaLength").equals("N")) {
            message.append(endMarker);
            tempMessage.append(endMarker);
        }

        logger.logInfo("Message succussfully generated, Transaction.generateRequestMessage");
        return message.toString();
    }

    static String cushionField(String field, int fieldSize) {
        try {
            if (field.length() == fieldSize) {
                return field;
            } else if (field.length() < fieldSize) {
                while (field.length() != fieldSize) {
                    field += " ";
                }
                return field;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return field;
    }

    static String cushionFieldWithZero(String field, int fieldSize) {
        try {
            if (field.length() == fieldSize) {
                return field;
            } else if (field.length() < fieldSize) {
                while (field.length() != fieldSize) {
                    field = "0" + field;
                }
                return field;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return field;
    }

    public ResponseBean generateResponseBean(String message) {

        Guard.checkNullOrEmpty(message, "Response message is null");

        logger.logDebug("Transaction", "generateResponseBean", "Parsing ICM response");
        MessageParser parser = new MessageParser(message);
        ResponseBean bean = null;
        try {
            bean = (ResponseBean) (Class.forName(this.responseBean).newInstance());
            Iterator itr = this.responseBlock.iterator();
            while (itr.hasNext()) {
                TransactionParameter parameter = (TransactionParameter) itr.next();
                String subBean = parameter.getBean();
                if (subBean != null && !subBean.equals("")) {
                    List list = new ArrayList();
                    int count = 0;
                    count = Integer.parseInt(setValue(parameter, null, parser));
                    for (int i = 0; i < count; i++) {
                        BaseBean innerBean = (BaseBean) (Class.forName(parameter.getBean()).newInstance());
                        Iterator subItr = parameter.getParameters().iterator();
                        while (subItr.hasNext()) {
                            TransactionParameter innerParameter = (TransactionParameter) subItr.next();
                            setValue(innerParameter, innerBean, parser);
                        }
                        list.add(innerBean);
                    }
                    bean.set(parameter.getName(), list);
                } else {
                    setValue(parameter, bean, parser);
                }
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Tracer.traceOut(Level.INFO, "Transaction", "generateResponseBean",
        // "Successfully parse the response");
        logger.logDebug("Transaction", "generateResponseBean", "Successfully parse the response");
        return bean;
    }


    private String setValue(TransactionParameter parameter, BaseBean bean, MessageParser parser) {
        String value = "";
        String name = parameter.getName();
        String delimiterPlace = parameter.getDelimiterPlace();
        String delimiter = parameter.getDelimiter();
        String isMetaValue = parameter.getIsMetaValue();

        int ignore = -1;
        int maxLength = 0;
        try {
            ignore = Integer.parseInt(parameter.getIgnore());
        } catch (NumberFormatException e) {
            //e.printStackTrace();
        }
        try {
            maxLength = Integer.parseInt(parameter.getMaxLength());
        } catch (NumberFormatException e) {
            //e.printStackTrace();
        }
        if (delimiterPlace.equalsIgnoreCase("E")) {
            value = parser.getNext(delimiter);
        } else {
            if (isMetaValue.equals("Y")) {

            } else {
                parser.ignore(1);
            }

            value = parser.getNext(maxLength);
        }
        if (bean != null)
            bean.set(name, value);
        if (ignore != -1)
            parser.ignore(ignore);
        return value;
    }

    public String getRequestBean() {
        return requestBean;
    }

    public void setRequestBean(String requestBean) {
        this.requestBean = requestBean;
    }

    public String getResponseBean() {
        return responseBean;
    }

    public void setResponseBean(String responseBean) {
        this.responseBean = responseBean;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

}
