/**
 *
 */
package com.avanza.integrationutility.implementations;

import java.util.HashMap;

import com.avanza.core.util.StringHelper;
import com.avanza.integrationutility.core.TransactionIdentifierSelector;

/**
 * @author shahbaz.ali
 *
 */
public class LengthBasedTranIdSelectorImpl implements TransactionIdentifierSelector {

    HashMap<String, String> channelProperties;

    public LengthBasedTranIdSelectorImpl(String channelPropertiesStr) {

        this.channelProperties = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(channelPropertiesStr, StringHelper.COMMA);
    }

    @Override
    public String getTransitionId(String responseMessage) {

        int tranId_start = Integer.parseInt(channelProperties.get("tranIdStart"));
        int tranId_end = Integer.parseInt(channelProperties.get("tranIdEnd"));

        return responseMessage.substring(tranId_start - 1, tranId_end);

    }

}
