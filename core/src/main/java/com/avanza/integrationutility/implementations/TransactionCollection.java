package com.avanza.integrationutility.implementations;

import java.util.HashMap;

public class TransactionCollection {

    private HashMap transactions = new HashMap();

    public void addTransaction(Transaction tran) {
        transactions.put(tran.getCode(), tran);
    }

    public Transaction getTransaction(String tranCode) {
        return (Transaction) transactions.get(tranCode);
    }


}
