package com.avanza.integrationutility.implementations;


/**
 * @author akamran
 * <p>
 * This is the base class for all request beans
 */
public class RequestBean extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = -4385204588932717469L;

    public String getTranCode() {
        return (String) get("tranCode");
    }

    public void setTranCode(String tranCode) {
        set("tranCode", tranCode);
    }

    public String getTranID() {
        return (String) get("tranID");
    }

    public void setTranID(String tranID) {
        set("tranID", tranID);
    }


    public String getRequestIdentifier() {
        return (String) get("requestIdentifier");
    }

    public void setRequestIdentifier(String requestIdentifier) {
        set("requestIdentifier", requestIdentifier);
    }

    public String getClientToken() {
        return (String) get("clientToken");
    }

    public void setClientToken(String clientToken) {
        set("clientToken", clientToken);
    }


}
