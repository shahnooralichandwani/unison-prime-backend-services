package com.avanza.integrationutility.implementations;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TransactionParameterCollection {

    private List parameters = new ArrayList();

    public void addParameter(TransactionParameter parameter) {
        parameters.add(parameter);
    }

    public Iterator iterator() {
        return parameters.iterator();
    }

}
