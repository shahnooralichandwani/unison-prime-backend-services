package com.avanza.integrationutility.exceptions;

import com.avanza.core.CoreException;

public class InsufficientResponseSizeException extends CoreException {


    public InsufficientResponseSizeException(String a) {
        super(a);
    }
}
