/**
 *
 */
package com.avanza.integrationutility.exceptions;

/**
 * @author shahbaz.ali
 *
 */
public class TransactionNotFound extends Exception {

    /**
     *
     */
    public TransactionNotFound() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public TransactionNotFound(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public TransactionNotFound(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public TransactionNotFound(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
