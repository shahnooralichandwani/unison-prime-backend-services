package com.avanza.integrationutility.exceptions;

public class TransactionMessageException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public TransactionMessageException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public TransactionMessageException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public TransactionMessageException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public TransactionMessageException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }


}
