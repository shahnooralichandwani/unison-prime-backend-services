package com.avanza.integrationutility.exceptions;

import com.avanza.core.CoreException;

public class MessageFormatException extends CoreException {


    public MessageFormatException(String a) {
        super(a);
    }
}
