package com.avanza.integrationutility.exceptions;

import com.avanza.core.CoreException;

public class ConnectionFailedException extends CoreException {


    public ConnectionFailedException(String a) {
        super(a);
    }
}
