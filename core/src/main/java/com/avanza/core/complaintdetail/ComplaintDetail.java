package com.avanza.core.complaintdetail;

import java.util.LinkedHashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.product.ProductType;

public class ComplaintDetail extends DbObject implements Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -8550951534847723086L;

    protected String complaintDetailId;
    protected String compTicketNum;
    protected String flag;
    protected String notesFlag;
    protected String notes;
    protected String loginUser;
    protected ComplaintDetail complaintDetail;
    protected Set<ComplaintDetail> complaintDetails = new LinkedHashSet<ComplaintDetail>(0);
    //private ValueTreeNode valueTreeNode;


    public ComplaintDetail() {
    }

    public ComplaintDetail getComplaintDetail() {
        return this.complaintDetail;
    }

    public void setComplaintDetail(ComplaintDetail complaintDetail) {
        this.complaintDetail = complaintDetail;
    }


    public String getComplaintDetailId() {
        return this.complaintDetailId;
    }

    public void setComplaintDetailId(String complaintDetailId) {
        this.complaintDetailId = complaintDetailId;
    }

    public String getCompTicketNum() {
        return this.compTicketNum;
    }

    public void setCompTicketNum(String compTicketNum) {
        this.compTicketNum = compTicketNum;
    }

    public String getLoginUser() {
        return this.loginUser;
    }

    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    public String getFlag() {
        return this.flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getNotesFlag() {
        return this.notesFlag;
    }

    public void setNotesFlag(String notesFlag) {
        this.notesFlag = notesFlag;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Set<ComplaintDetail> getComplaintDetails() {
        return this.complaintDetails;
    }

    public void setComplaintDetails(Set<ComplaintDetail> complaintDetails) {
        this.complaintDetails = complaintDetails;
    }



    /*
     * @Override protected Object clone() throws CloneNotSupportedException {
     * ProductType ptype = new ProductType(); ptype.setProductCode(productCode);
     * ptype.setDescription(description); ptype.setDisplayOrder(displayOrder);
     * ptype.setLevelId(levelId); ptype.setParentCode(parentCode);
     * ptype.setProduct(product); ptype.setProductNamePrm(productNamePrm);
     * ptype.setProductNameSec(productNameSec); ptype.setRootNodeId(rootNodeId);
     * ptype.setSystemName(systemName); return ptype; }
     */
}
