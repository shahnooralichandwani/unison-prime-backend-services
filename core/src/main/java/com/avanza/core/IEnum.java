package com.avanza.core;

public interface IEnum<T> {
    T getValue();
}
