package com.avanza.core.draft.manager;

import java.util.ArrayList;
import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.draft.Draft;

public class DraftManager {

    private DataBroker broker;
    private Search search;

    public DraftManager() {
        search = new Search();
        broker = DataRepository.getBroker(Draft.class.getName());
    }


    public Draft getDraft(String id) {
        return this.broker.findById(Draft.class, id);
    }

    public <T> List<T> getDrafts() {

        List draftList = new ArrayList(0);
        search.clear();
        search.addFrom(Draft.class);
        search.addCriterion(SimpleCriterion.equal("currentState", Draft.Status.DISCARD_STATUS));
        draftList = broker.find(search);
        return draftList;
    }


    public <T> List<T> findAll(Class clazz) {

        return broker.findAll(clazz);
    }

    public <T> void persist(T item) {

        broker.persist(item);

    }


    public <T> void update(T item) {
        broker.update(item);
    }

    public <T> void delete(T item) {
        broker.delete(item);
    }
}
