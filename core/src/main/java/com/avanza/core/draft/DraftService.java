package com.avanza.core.draft;

import java.util.List;

import com.avanza.core.draft.Draft;
import com.avanza.core.draft.manager.DraftManager;
import com.avanza.core.util.Logger;


public class DraftService {

    private static final Logger logger = Logger.getLogger(DraftService.class);
    private DraftManager draftManager;

    public DraftService() {
        draftManager = new DraftManager();
    }

    public List<Draft> findDraft() {
        List<Draft> draftList = null;
        try {
            draftList = draftManager.getDrafts();
        } catch (Exception e) {
            // TODO: handle exception
        }
        return draftList;
    }

    public Draft findById(String id) {
        Draft draft = null;
        try {
            draft = draftManager.getDraft(id);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return draft;
    }

    public void presist(Draft draft) {
        try {
            draftManager.persist(draft);
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public void update(Draft draft) {
        try {
            draftManager.update(draft);
        } catch (Exception e) {
            // TODO: handle exception
        }

    }


}