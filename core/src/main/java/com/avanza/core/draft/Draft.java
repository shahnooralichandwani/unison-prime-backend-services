package com.avanza.core.draft;

import com.avanza.core.data.DbObject;


/**
 * Draft entity. @author MyEclipse Persistence Tools
 */
public class Draft extends DbObject implements java.io.Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static final String DRAFT_TICKET_NO = "Draft_Ticket_No";
    // Fields

    private String draftTicketNum;
    private String currentState;
    private String currentActor;
    private String custRelationNum;
    private String productNumber;
    private String productCode;
    private String productEntityId;
    private String customerName;
    private String jsonData;
    private String docMedium;
    private String stringVal;
    private String stringVal2;
    private String stringVal3;
    private String stringVal4;
    private String stringVal5;
    private String parentEntity;


    // Constructors

    /**
     * default constructor
     */
    public Draft() {
    }

    public enum Status {

        DRAFT_STATUS("Draft"),
        DISCARD_STATUS("Discard"),
        FORM_STATUS("Form"),
        CLONE_SATAUS("Clone");


        public void setStatus(String status) {
            this.status = status;
        }

        String status;

        private Status(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

        @Override
        public String toString() {
            return status;
        }

    }


    // Property accessors

    public String getDraftTicketNum() {
        return this.draftTicketNum;
    }

    public void setDraftTicketNum(String draftTicketNum) {
        this.draftTicketNum = draftTicketNum;
    }

    public String getCurrentState() {
        return this.currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getCurrentActor() {
        return this.currentActor;
    }

    public void setCurrentActor(String currentActor) {
        this.currentActor = currentActor;
    }

    public String getCustRelationNum() {
        return this.custRelationNum;
    }

    public void setCustRelationNum(String custRelationNum) {
        this.custRelationNum = custRelationNum;
    }

    public String getProductNumber() {
        return this.productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getProductCode() {
        return this.productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductEntityId() {
        return this.productEntityId;
    }

    public void setProductEntityId(String productEntityId) {
        this.productEntityId = productEntityId;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getJsonData() {
        return this.jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public String getDocMedium() {
        return this.docMedium;
    }

    public void setDocMedium(String docMedium) {
        this.docMedium = docMedium;
    }

    public String getStringVal() {
        return this.stringVal;
    }

    public void setStringVal(String stringVal) {
        this.stringVal = stringVal;
    }

    public String getStringVal2() {
        return this.stringVal2;
    }

    public void setStringVal2(String stringVal2) {
        this.stringVal2 = stringVal2;
    }

    public String getStringVal3() {
        return this.stringVal3;
    }

    public void setStringVal3(String stringVal3) {
        this.stringVal3 = stringVal3;
    }

    public String getStringVal4() {
        return this.stringVal4;
    }

    public void setStringVal4(String stringVal4) {
        this.stringVal4 = stringVal4;
    }

    public String getStringVal5() {
        return this.stringVal5;
    }

    public void setStringVal5(String stringVal5) {
        this.stringVal5 = stringVal5;
    }

    public String getParentEntity() {
        return parentEntity;
    }

    public void setParentEntity(String parentEntity) {
        this.parentEntity = parentEntity;
    }


}


