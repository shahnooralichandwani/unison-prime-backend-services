package com.avanza.core.web.config;

import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.util.Guard;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * This class initiallizes the Web Application, the parameter XML_PROPERTY_FILEPATH must be defined in the init parameters while configuring the
 * servlet. Put the following configuration in web.xml file
 * and also place the init params in context param tag.
 * <p>
 * <context-param>
 * <param-name>xmlConfigFilePath</param-name>
 * <param-value>/unison-conf.xml</param-value>
 * </context-param>
 *
 * <listener>
 * <listener-class>
 * com.avanza.core.web.config.ApplicationContextListener
 * </listener-class>
 * </listener>
 *
 * @author kraza
 */
public class ApplicationContextListener implements ServletContextListener {

    private static final String XML_PROPERTY_FILEPATH = "xmlConfigFilePath";

    public void contextDestroyed(ServletContextEvent servletConfig) {

        // Do all the clean up for the application here.

        ApplicationLoader.destroy();
    }

    public void contextInitialized(ServletContextEvent servletConfig) {

        // Do All the application specific initiallization here.
        String configFilePath = servletConfig.getServletContext().getInitParameter(ApplicationContextListener.XML_PROPERTY_FILEPATH);
        Guard.checkNullOrEmpty(configFilePath,
                "Error while Loading Application: XML_PROPERTY_FILEPATH is not configures in ApplicationLoaderServlet.");

        configFilePath = servletConfig.getServletContext().getRealPath(configFilePath);

        ApplicationLoader.load(configFilePath);

    }
}
