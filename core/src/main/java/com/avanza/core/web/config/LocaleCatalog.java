package com.avanza.core.web.config;

import com.avanza.core.CoreException;
import com.avanza.core.CoreInterface;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class LocaleCatalog implements CoreInterface {

    private static LocaleCatalog _instance = new LocaleCatalog();
    private static boolean isLoaded = false;
    private static final Logger logger = Logger.getLogger(LocaleCatalog.class.getName());

    private static String XML_LOCALE = "Locale";
    private static String XML_LANGUAGE = "language";
    private static String XML_COUNTRY = "country";
    private static String XML_IS_PRIMARY = "isPrimary";
    private static String XML_DEFAULT_ENCODING = "default-encoding";
    private static String XML_ENCODING = "encoding";
    private static String XML_NUM_FORMAT = "number-format";
    private static String XML_DATE_FORMAT = "date-format";

    private List<LocaleInfo> supportedLocales;
    private LocaleInfo primaryLocaleInfo;

    protected LocaleCatalog() {
    }

    public static LocaleCatalog getInstance() {

        return _instance;
    }

    public void dispose() {
    }

    public void load(ConfigSection section) {

        if (isLoaded) return;


        List<ConfigSection> locales = section.getChildSections(XML_LOCALE);
        supportedLocales = new ArrayList<LocaleInfo>();

        for (ConfigSection locale : locales) {

            boolean isPrimary = locale.getBooleanValue(XML_IS_PRIMARY);
            String langCode = locale.getTextValue(XML_LANGUAGE);
            String countryCode = locale.getTextValue(XML_COUNTRY);
            String encoding = locale.getTextValue(XML_ENCODING);
            if (!Charset.isSupported(encoding))
                throw new CoreException("The charset provided %1$s as default for Locale is not supported by JVM.", encoding);

            String numberFormat = locale.getTextValue(XML_NUM_FORMAT);
            String dateFormat = locale.getTextValue(XML_DATE_FORMAT);
            LocaleInfo lacaleInfo = new LocaleInfo(new Locale(langCode, countryCode), encoding, numberFormat, isPrimary, dateFormat);
            supportedLocales.add(lacaleInfo);
            if (isPrimary) {
                primaryLocaleInfo = lacaleInfo;
            }
        }

        if (primaryLocaleInfo == null) {
            throw new CoreException("one locale should be marked a primary");
        }

        isLoaded = true;
    }

    public List<LocaleInfo> getSupportedLocales() {
        return supportedLocales;
    }

    public LocaleInfo getLocale(String language, String country) {

        for (LocaleInfo localeInfo : supportedLocales) {
            if (localeInfo.getCountryCode().equalsIgnoreCase(country) && localeInfo.getLanguageCode().equalsIgnoreCase(language))
                return localeInfo;
        }

        return null;
    }

    public LocaleInfo getDefaultLocale() {

        for (LocaleInfo localeInfo : supportedLocales) {
            if (localeInfo.isPrimary())
                return localeInfo;
        }

        return null;
    }

    public String getDefaultDateFormat() {
        return primaryLocaleInfo.getDateFormat();
    }

}
