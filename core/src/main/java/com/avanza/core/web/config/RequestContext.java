package com.avanza.core.web.config;

import java.io.Serializable;

/**
 * The wrapper to the session map.
 *
 * @author majid
 */
public interface RequestContext extends Serializable {

    <T, U> void setAttribute(T key, U value);

    <T, U> U getAttribute(T key);

    <T, U> U removeAttribute(T key);

    <K, V> boolean isEqual(K key, V value);
}

