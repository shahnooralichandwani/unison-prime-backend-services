package com.avanza.core.web.config;

import java.io.Serializable;
import java.util.Map;

/**
 * The wrapper to the Faces Context map.
 *
 * @author kraza
 */
public interface ViewContext extends Serializable {

    <T, U> void setAttribute(T key, U value);

    <T, U> U getAttribute(T key);

    <T> void removeAttribute(T key);

    Map getAttributesMap();
}
