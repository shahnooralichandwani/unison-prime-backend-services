package com.avanza.core.web.config;

import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.util.Guard;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * This class initiallizes the Web Application, the parameter XML_PROPERTY_FILEPATH must be defined in the init parameters while configuring the
 * servlet.
 *
 * @author kraza
 */
public class ApplicationLoaderServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 2663366462669320847L;

    private static final String XML_PROPERTY_FILEPATH = "xmlConfigFilePath";

    @Override
    public void destroy() {

        // Do all the clean up for the application here.
        ApplicationLoader.destroy();

        super.destroy();
    }

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {

        // Do All the application specific initiallization here.
        String configFilePath = servletConfig.getInitParameter(ApplicationLoaderServlet.XML_PROPERTY_FILEPATH);
        Guard.checkNullOrEmpty(configFilePath,
                "Error while Loading Application: XML_PROPERTY_FILEPATH is not configures in ApplicationLoaderServlet.");

        configFilePath = servletConfig.getServletContext().getRealPath(configFilePath);

        ApplicationLoader.load(configFilePath);

        // Need to call this method otherwise getServletConfig method returns null always.
        super.init(servletConfig);
    }
}
