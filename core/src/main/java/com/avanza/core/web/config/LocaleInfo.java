package com.avanza.core.web.config;

import com.avanza.core.CoreException;

import java.awt.*;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Locale;

/**
 * Contains the Locale specific Info.
 *
 * @author kraza
 */
public final class LocaleInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4440709867426142231L;

    private String name;
    private Locale locale;
    private String encoding;
    private String numberFormat;
    private String dateFormat;
    private boolean rightToLeft;
    private boolean horizontal;
    private boolean primary;
    private ComponentOrientation orientation;
    private LanguageDirection dir;
    private String gridAlign;


    public LocaleInfo(Locale locale, String encoding, String numberFormat, boolean isPrimary, String dateformat) {
        this.locale = locale;

        if (!Charset.isSupported(encoding))
            throw new CoreException("The charset provided %1$s for Locale %2$s is not supported by JVM.", encoding, locale.toString());

        this.encoding = encoding;
        this.numberFormat = numberFormat;
        this.dateFormat = dateformat;
        this.name = this.locale.toString();
        this.primary = isPrimary;
        this.orientation = ComponentOrientation.getOrientation(this.locale);
        this.rightToLeft = !this.orientation.isLeftToRight();
        this.horizontal = this.orientation.isHorizontal();
        this.dir = this.isRightToLeft() ? LanguageDirection.Rtl : LanguageDirection.Ltr;
        this.gridAlign = this.isRightToLeft() ? "right" : "left";
    }


    public String getGridAlign() {
        return gridAlign;
    }

    public String getOppositeGridAlign() {
        if (gridAlign.equals("left")) {
            return "right";
        } else
            return "left";
    }

    public void setGridAlign(String gridAlign) {
        this.gridAlign = gridAlign;
    }

    public boolean isRightToLeft() {
        return rightToLeft;
    }

    public void setRightToLeft(boolean rightToLeft) {
        this.rightToLeft = rightToLeft;
    }

    public Locale getLocale() {
        return locale;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    public ComponentOrientation getOrientation() {
        return orientation;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public void setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
    }

    public String getName() {
        return name;
    }

    public String getDir() {
        return this.dir.toString();
    }

    public String getLanguageCode() {
        return this.locale.getLanguage();
    }

    public String getCountryCode() {
        return this.locale.getCountry();
    }

    public String getEncoding() {
        return encoding;
    }

    public <T> Method getLocalWiseMethod(T object, String property) throws Exception {
        String propertyName = "get" + property.substring(0, 1).toUpperCase() + property.substring(1);
        if (this.isPrimary())
            propertyName += "Prm";
        else
            propertyName += "Sec";

        Method method = object.getClass().getMethod(propertyName, new Class[]{});

        return method;
    }

    public String getNumberFormat() {
        return numberFormat;
    }


    public String getDateFormat() {
        return dateFormat;
    }
}
