/**
 *
 */
package com.avanza.core.web.config;

import java.util.HashMap;

/**
 * @author shahbaz.ali
 */

public class BackgroundThreadContext implements WebContext {

    /**
     *
     */
    private static final long serialVersionUID = 5746373522575754529L;
    private HashMap<String, Object> map = new HashMap<String, Object>();

    @SuppressWarnings("unchecked")
    public <T, U> U getAttribute(T key) {

        return (U) this.map.get(key.toString());
    }

    @SuppressWarnings("unchecked")
    public <T, U> U removeAttribute(T key) {
        U retVal = (U) this.map.get(key.toString());
        this.map.remove(key);
        return retVal;
    }

    public <T, U> void setAttribute(T key, U value) {
        this.map.put(key.toString(), value);

    }

}
