package com.avanza.core.web.config;

/**
 * Language Direction used by HTML Pages to display.
 *
 * @author kraza
 */
public enum LanguageDirection {

    Rtl("rtl"), Ltr("ltr");

    private String value;

    private LanguageDirection(String val) {
        this.value = val;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
