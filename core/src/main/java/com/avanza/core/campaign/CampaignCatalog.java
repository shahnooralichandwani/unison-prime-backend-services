package com.avanza.core.campaign;

import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;


public class CampaignCatalog {

    private static CampaignCatalog instance;
    private String executorRoleId;
    private String supervisorRoleId;
    private String nonCustomerEnitityId;
    private String customerEnitityId;
    private String campaignEntityId;
    private String tmlEntityId;
    private String executorEmailTemplateId;
    private String supervisorEmailTemplateId;
    private String relationShipAttribId;
    private String externalSourceEntityId;


    public static synchronized CampaignCatalog getInstance() {
        if (instance == null) {
            instance = new CampaignCatalog();
            instance.load();
        }
        return instance;
    }

    private void load() {

    }

    public void load(ConfigSection cfgSection) {
        executorRoleId = cfgSection.getValue("executorRoleId", StringHelper.EMPTY);
        supervisorRoleId = cfgSection.getValue("supervisorRoleId", StringHelper.EMPTY);
        nonCustomerEnitityId = cfgSection.getValue("nonCustomerEnitityId", StringHelper.EMPTY);
        customerEnitityId = cfgSection.getValue("customerEnitityId", StringHelper.EMPTY);
        campaignEntityId = cfgSection.getValue("campaignEntityId", StringHelper.EMPTY);
        tmlEntityId = cfgSection.getValue("campaignRuleGroupId", StringHelper.EMPTY);
        executorEmailTemplateId = cfgSection.getValue("executorEmailTemplateId", StringHelper.EMPTY);
        supervisorEmailTemplateId = cfgSection.getValue("supervisorEmailTemplateId", StringHelper.EMPTY);
        relationShipAttribId = cfgSection.getValue("relationShipAttribId", StringHelper.EMPTY);
        externalSourceEntityId = cfgSection.getValue("externalSourceEntityId", StringHelper.EMPTY);
    }

    private CampaignCatalog() {

    }

    public String getExecutorRoleId() {
        return executorRoleId;
    }

    public String getNonCustomerEnitityId() {
        return nonCustomerEnitityId;
    }

    public String getCustomerEnitityId() {
        return customerEnitityId;
    }

    public String getCampaignEntityId() {
        return campaignEntityId;
    }

    public String getTmlEntityId() {
        return tmlEntityId;
    }

    public String getExecutorEmailTemplateId() {
        return executorEmailTemplateId;
    }

    public String getSupervisorEmailTemplateId() {
        return supervisorEmailTemplateId;
    }

    public String getSupervisorRoleId() {
        return supervisorRoleId;
    }

    public String getRelationShipAttribId() {
        return relationShipAttribId;
    }

    public String getExternalSourceEntityId() {
        return externalSourceEntityId;
    }

    public void setExternalSourceEntityId(String externalSourceEntityId) {
        this.externalSourceEntityId = externalSourceEntityId;
    }


}
