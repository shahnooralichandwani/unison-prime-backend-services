package com.avanza.core.serialized;

import java.io.Serializable;
import java.util.Date;

import com.avanza.core.sdo.Attribute;
import com.avanza.core.util.Convert;


public class SerializedAttribute implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String metaAttributeId;
    private String systemName;
    private Object value;

    public SerializedAttribute() {
    }

    public SerializedAttribute(Attribute attrib) {
        this.metaAttributeId = attrib.getMetaAttrib().getId();
        this.systemName = attrib.getMetaAttrib().getSystemName();
        if (attrib.getValue() instanceof Date) {
            this.value = Convert.toDateString((Date) attrib.getValue(), "dd-MMM-yyyy HH:mm:ss");
        } else
            this.value = attrib.getValue();
    }

    public String getMetaAttributeId() {
        return metaAttributeId;
    }

    public void setMetaAttributeId(String metaAttributeId) {
        this.metaAttributeId = metaAttributeId;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

}
