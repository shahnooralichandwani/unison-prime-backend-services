package com.avanza.core.serialized;

import java.util.HashMap;

public interface Parser {
    public String getColumnName();

    public String serialize(HashMap<String, SerializedAttribute> serializedAttribMap);

    public HashMap<String, SerializedAttribute> deSerialize(String jsonString);

}
