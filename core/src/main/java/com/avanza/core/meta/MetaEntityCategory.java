package com.avanza.core.meta;

import com.avanza.core.data.DbObject;
import com.avanza.core.interceptor.Auditable;
import com.avanza.core.meta.audit.MetaEntityCategoryAudit;
import com.avanza.core.meta.audit.MetaEntityCategoryAuditId;
import org.apache.commons.beanutils.BeanUtils;

import java.util.Random;


public class MetaEntityCategory extends DbObject implements Auditable {

    private String entityCategoryId;

    private String metaEntityId;

    private String treeNodeId;

    private String actionClass;


    public String getMetaEntityId() {
        return metaEntityId;
    }

    public String getEntityCategoryId() {
        return entityCategoryId;
    }

    public void setEntityCategoryId(String entityCategoryId) {
        this.entityCategoryId = entityCategoryId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

    public String getTreeNodeId() {
        return treeNodeId;
    }

    public void setTreeNodeId(String treeNodeId) {
        this.treeNodeId = treeNodeId;
    }

    public String getActionClass() {
        return actionClass;
    }

    public void setActionClass(String actionClass) {
        this.actionClass = actionClass;
    }


    @Override
    public Object getAuditEntry(Short revtype) {
        MetaEntityCategoryAudit metaEntityCategoryAudit = null;
        try {


            Random rand = new Random();

            metaEntityCategoryAudit = new MetaEntityCategoryAudit();
            metaEntityCategoryAudit.setRevtype(revtype);

            BeanUtils.copyProperties(metaEntityCategoryAudit, this);
            metaEntityCategoryAudit.setMetaEntityCategoryAuditId(new MetaEntityCategoryAuditId(this.entityCategoryId, rand.nextInt()));

            //secUserV.setId(new SecUserVId(this.loginId, CounterUtils.getNextAuditTableKey()));
        } catch (Exception e) {
            metaEntityCategoryAudit = null;
        }
        return metaEntityCategoryAudit;
    }


}
