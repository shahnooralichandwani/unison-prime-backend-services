package com.avanza.core.meta;

import com.avanza.core.CoreException;

public enum PickListDataType {

    String("String"), Integer("Integer");

    private String value;

    private PickListDataType(String val) {
        this.value = val;
    }

    public String toString() {
        return this.value;
    }

    public static PickListDataType fromString(String value) {

        PickListDataType retVal = null;

        if ("String".equalsIgnoreCase(value))
            retVal = PickListDataType.String;
        else if ("Integer".equalsIgnoreCase(value))
            retVal = PickListDataType.Integer;
        else
            throw new CoreException(
                    "[%1$s] is not recognized as Pick List Type",
                    value);

        return retVal;
    }

}
