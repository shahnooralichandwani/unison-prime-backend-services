package com.avanza.core.meta;

import com.avanza.core.data.DbObject;
import com.avanza.core.interceptor.Auditable;
import com.avanza.core.meta.audit.ValueTreeNodeAuditId;
import com.avanza.core.meta.audit.ValueTreeNodeBaseAudit;
import com.avanza.core.util.TreeObject;
import org.apache.commons.beanutils.BeanUtils;

import java.util.Random;

/**
 * @author fkazmi
 */
public class ValueTreeNode extends ValueTreeNodeBase implements Auditable {

    /**
     *
     */
    private static final long serialVersionUID = 5377413831546221589L;

    public ValueTreeNode() {
        super();
    }

    public ValueTreeNode(String id, String iconPath) {
        super(id, iconPath);
    }

    @Override
    public TreeObject clone() {

        ValueTreeNode valueTreeNode = new ValueTreeNode(this.id, this.iconPath);
        valueTreeNode.copyValues(this);

        // Now need to copy their children also.
        for (TreeObject tobj : this.childList) {
            TreeObject cloned = ((ValueTreeNode) tobj).clone();
            ((ValueTreeNode) cloned).parentNode = valueTreeNode;
            ((ValueTreeNode) cloned).parentId = valueTreeNode.id;
            valueTreeNode.childList.add(cloned);
        }

        return valueTreeNode;
    }

    @Override
    public void copyValues(DbObject copyFrom) {

        ValueTreeNode valueTreeNode = (ValueTreeNode) copyFrom;

        super.copyValues(copyFrom);
        this.primaryName = valueTreeNode.primaryName;
        this.secondaryName = valueTreeNode.secondaryName;
        this.hierarchyLevel = valueTreeNode.hierarchyLevel;
        this.parentId = valueTreeNode.parentId;
        this.productEntityId = valueTreeNode.productEntityId;
        this.otherCode = valueTreeNode.otherCode;
        this.parentNode = valueTreeNode.parentNode;
        this.entityCategorySet = valueTreeNode.entityCategorySet;
        this.iconPath = valueTreeNode.iconPath;
        this.systemName = valueTreeNode.systemName;
        this.displayOrder = valueTreeNode.displayOrder;
        this.rootNodeId = valueTreeNode.rootNodeId;
        this.actionClass = valueTreeNode.actionClass;
    }

    @Override
    public boolean equals(Object obj) {

        ValueTreeNode valueTreeNode = (ValueTreeNode) obj;

        if (valueTreeNode.getId().equalsIgnoreCase(this.getId())
                && valueTreeNode.getKey().equalsIgnoreCase(this.getKey()))
            return true;

        return false;
    }


    @Override
    public Object getAuditEntry(Short revtype) {
        ValueTreeNodeBaseAudit valueTreeNodeBaseAudit = null;
        try {


            Random rand = new Random();

            valueTreeNodeBaseAudit = new ValueTreeNodeBaseAudit();
            valueTreeNodeBaseAudit.setRevtype(revtype);

            BeanUtils.copyProperties(valueTreeNodeBaseAudit, this);
            valueTreeNodeBaseAudit.setValueTreeNodeAuditId(new ValueTreeNodeAuditId(this.id, rand.nextInt()));

            //secUserV.setId(new SecUserVId(this.loginId, CounterUtils.getNextAuditTableKey()));
        } catch (Exception e) {
            valueTreeNodeBaseAudit = null;
        }
        return valueTreeNodeBaseAudit;
    }


}
