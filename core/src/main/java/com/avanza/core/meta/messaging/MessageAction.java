/**
 *
 */
package com.avanza.core.meta.messaging;

/**
 * @author shahbaz.ali
 *
 */
public enum MessageAction {

    ListRequest,
    ListResponse,
    DetailRequest,
    DetailResponse,
    ListSubResponse,
    TransactionRequest,
    TransactionResponse,
}
	