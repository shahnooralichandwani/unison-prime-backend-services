package com.avanza.core.meta.messaging;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * MessageAttributeType entity. @author MyEclipse Persistence Tools
 */

public class MessageAttributeType implements java.io.Serializable {

    // Fields

    private String typeId;
    private String defaultValueClass;
    private String defaultExp;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private String systemName;
    private Set messageAttributes = new HashSet(0);

    // Constructors

    /**
     * default constructor
     */
    public MessageAttributeType() {
    }

    /**
     * minimal constructor
     */
    public MessageAttributeType(String typeId, String systemName,
                                Timestamp createdOn, String createdBy, Timestamp updatedOn,
                                String updatedBy) {
        this.typeId = typeId;
        this.systemName = systemName;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
    }

    /**
     * full constructor
     */
    public MessageAttributeType(String typeId, String systemName,
                                String defaultValueClass, String defaultExp, Timestamp createdOn,
                                String createdBy, Timestamp updatedOn, String updatedBy,
                                Set messageAttributes) {
        this.typeId = typeId;
        this.systemName = systemName;
        this.defaultValueClass = defaultValueClass;
        this.defaultExp = defaultExp;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
        this.messageAttributes = messageAttributes;
    }

    // Property accessors

    public String getTypeId() {
        return this.typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getDefaultValueClass() {
        return this.defaultValueClass;
    }

    public void setDefaultValueClass(String defaultValueClass) {
        this.defaultValueClass = defaultValueClass;
    }

    public String getDefaultExp() {
        return this.defaultExp;
    }

    public void setDefaultExp(String defaultExp) {
        this.defaultExp = defaultExp;
    }

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set getMessageAttributes() {
        return this.messageAttributes;
    }

    public void setMessageAttributes(Set messageAttributes) {
        this.messageAttributes = messageAttributes;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

}