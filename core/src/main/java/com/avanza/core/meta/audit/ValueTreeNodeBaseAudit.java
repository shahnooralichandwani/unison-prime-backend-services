package com.avanza.core.meta.audit;

import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.meta.MetaEntityCategory;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TreeObject;


public class ValueTreeNodeBaseAudit extends TreeObject {

    /**
     * @author Mehran Junejo
     */

    private static final long serialVersionUID = -6299244992703626417L;


    private ValueTreeNodeAuditId valueTreeNodeAuditId;
    private Short revtype;

    protected String parentId;

    protected String value;

    protected String primaryName;

    protected String secondaryName;

    protected String groupId;

    protected ValueTreeNodeBaseAudit parentNode;

    protected int hierarchyLevel;

    protected Long displayOrder;

    protected String systemName;

    protected String productEntityId;

    protected String otherCode;

    protected String rootNodeId;

    protected String actionClass;


    protected String iconPath = "../../images/iconFolder.gif";

    protected Set<MetaEntityCategory> entityCategorySet = new HashSet<MetaEntityCategory>(0);

    public Set<MetaEntityCategory> getEntityCategorySet() {
        return entityCategorySet;
    }


    public void setEntityCategorySet(Set<MetaEntityCategory> entityCategorySet) {
        this.entityCategorySet = entityCategorySet;
    }

    public ValueTreeNodeBaseAudit() {
        super();
        displayOrder = 0L;
    }

    public ValueTreeNodeBaseAudit(String id, String iconPath) {
        super(id);
        this.iconPath = iconPath;
    }

    public void setId(String id) {
        super.id = id;
    }

    public int getHierarchyLevel() {
        return hierarchyLevel;
    }

    public ValueTreeNodeBaseAudit getParentNode() {
        return parentNode;
    }

    public void setParentNode(ValueTreeNodeBaseAudit parentNode) {
        this.parentNode = parentNode;
    }

    @Override
    public TreeObject clone() {

        ValueTreeNodeBaseAudit ValueTreeNodeBase = new ValueTreeNodeBaseAudit(this.id, this.iconPath);
        ValueTreeNodeBase.copyValues(this);

        // Now need to copy their children also.
        for (TreeObject tobj : this.childList) {
            TreeObject cloned = ((ValueTreeNodeBaseAudit) tobj).clone();
            ((ValueTreeNodeBaseAudit) cloned).parentNode = ValueTreeNodeBase;
            ((ValueTreeNodeBaseAudit) cloned).parentId = ValueTreeNodeBase.id;
            ValueTreeNodeBase.childList.add(cloned);
        }

        return ValueTreeNodeBase;
    }

    /**
     * This method only copies the values below in the hierarchy.
     */
    @Override
    public void copyValues(DbObject copyFrom) {

        ValueTreeNodeBaseAudit ValueTreeNodeBase = (ValueTreeNodeBaseAudit) copyFrom;

        super.copyValues(copyFrom);
        this.primaryName = ValueTreeNodeBase.primaryName;
        this.secondaryName = ValueTreeNodeBase.secondaryName;
        this.hierarchyLevel = ValueTreeNodeBase.hierarchyLevel;
        this.parentId = ValueTreeNodeBase.parentId;
        this.productEntityId = ValueTreeNodeBase.productEntityId;
        this.otherCode = ValueTreeNodeBase.otherCode;
        this.parentNode = ValueTreeNodeBase.parentNode;
        this.entityCategorySet = ValueTreeNodeBase.entityCategorySet;
        this.iconPath = ValueTreeNodeBase.iconPath;
        this.systemName = ValueTreeNodeBase.systemName;
        this.displayOrder = ValueTreeNodeBase.displayOrder;
        this.rootNodeId = ValueTreeNodeBase.rootNodeId;
        this.actionClass = ValueTreeNodeBase.actionClass;
    }

    @Override
    protected TreeObject createObject(String id) {

        return new ValueTreeNodeBaseAudit(id, "");
    }

    @Override
    public boolean equals(Object obj) {

        ValueTreeNodeBaseAudit ValueTreeNodeBase = (ValueTreeNodeBaseAudit) obj;

        if (ValueTreeNodeBase.getId().equalsIgnoreCase(this.getId()) && ValueTreeNodeBase.getKey().equalsIgnoreCase(this.getKey()))
            return true;

        return false;
    }

    public void setHierarchyLevel(int hierarchyLevel) {
        this.hierarchyLevel = hierarchyLevel;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Override
    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        value = primaryName;
        this.primaryName = primaryName;
    }

    @Override
    public String getSecondaryName() {
        return secondaryName;
    }

    public void setSecondaryName(String secondaryName) {
        this.secondaryName = secondaryName;
    }

    public String getParentId() {
        return parentId;
    }

    @Override
    public <T extends TreeObject> void add(T item) {
        super.add(item);
        ((ValueTreeNodeBaseAudit) item).setParentId(this.id);
        ((ValueTreeNodeBaseAudit) item).parentNode = this;
        ((ValueTreeNodeBaseAudit) item).hierarchyLevel = this.hierarchyLevel + 1;
    }

    public boolean isRoot() {

        return StringHelper.isEmpty(this.parentId);
    }

    public boolean isLeaf() {

        return (super.childList == null || super.childList.isEmpty());
    }


    @Override
    public String getValue() {

        return this.value;
    }

    public void setValue(String value) {

        this.value = value;
    }

    public String getProductEntityId() {
        return productEntityId;
    }

    public void setProductEntityId(String productEntityId) {
        this.productEntityId = productEntityId;
    }

    public <T extends TreeObject> void addChild(T item) {
        ((ValueTreeNodeBaseAudit) item).setParentId(this.id);
        ((ValueTreeNodeBaseAudit) item).parentNode = this;
        ((ValueTreeNodeBaseAudit) item).hierarchyLevel = this.hierarchyLevel + 1;
        this.childList.add(item);
    }

    public String getOtherCode() {
        return otherCode;
    }

    public void setOtherCode(String otherCode) {
        this.otherCode = otherCode;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public Long getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Long displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getSystemName() {
        return systemName;
    }


    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }


    public String getGroupId() {
        return groupId;
    }


    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }


    public String getRootNode() {
        return rootNodeId;
    }

    public void setRootNode(String rootNode) {
        this.rootNodeId = rootNode;
    }

    public String getActionClass() {
        return actionClass;
    }


    public void setActionClass(String actionClass) {
        this.actionClass = actionClass;
    }

    public ValueTreeNodeAuditId getValueTreeNodeAuditId() {
        return valueTreeNodeAuditId;
    }


    public void setValueTreeNodeAuditId(ValueTreeNodeAuditId valueTreeNodeAuditId) {
        this.valueTreeNodeAuditId = valueTreeNodeAuditId;
    }


    public Short getRevtype() {
        return revtype;
    }


    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }


}
