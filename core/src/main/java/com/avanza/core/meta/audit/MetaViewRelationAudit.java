package com.avanza.core.meta.audit;

import com.avanza.core.data.DbObject;

/**
 * @author Mehran Junejo
 */

public class MetaViewRelationAudit extends DbObject {


    private MetaViewRelationAuditId metaViewRelationAuditId;
    private Short revtype;

    private String relationShipId;

    private String parentViewId;

    private String childViewId;

    private int displayOrder;

    private String relationshipType;


    public String getRelationShipId() {
        return relationShipId;
    }


    public void setRelationShipId(String relationShipId) {
        this.relationShipId = relationShipId;
    }


    public String getRelationshipType() {
        return relationshipType;
    }


    public void setRelationshipType(String relationshipType) {
        this.relationshipType = relationshipType;
    }


    public String getParentViewId() {
        return parentViewId;
    }


    public void setParentViewId(String parentViewId) {
        this.parentViewId = parentViewId;
    }


    public String getChildViewId() {
        return childViewId;
    }


    public void setChildViewId(String childViewId) {
        this.childViewId = childViewId;
    }


    public int getDisplayOrder() {
        return displayOrder;
    }


    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }


    public MetaViewRelationAuditId getMetaViewRelationAuditId() {
        return metaViewRelationAuditId;
    }


    public void setMetaViewRelationAuditId(
            MetaViewRelationAuditId metaViewRelationAuditId) {
        this.metaViewRelationAuditId = metaViewRelationAuditId;
    }


    public Short getRevtype() {
        return revtype;
    }


    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }
}
