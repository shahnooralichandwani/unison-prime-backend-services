/**
 *
 */
package com.avanza.core.meta.messaging.exceptions;

/**
 * @author shahbaz.ali
 *
 */
public class AttributeTagNameUndefinedException extends Exception {

    String message;

    public AttributeTagNameUndefinedException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}
