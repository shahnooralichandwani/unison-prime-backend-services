/**
 *
 */
package com.avanza.core.meta.messaging.enums;

/**
 * @author shahbaz.ali
 *
 */
public enum MessagePropertiesKeys {

    delimiter, endmarker, length, topMostRequestElement, ns, prefix,
    responseFormat, isTransaction
}
