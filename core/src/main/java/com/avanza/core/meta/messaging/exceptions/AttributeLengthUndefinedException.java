/**
 *
 */
package com.avanza.core.meta.messaging.exceptions;

/**
 * @author shahbaz.ali
 *
 */
public class AttributeLengthUndefinedException extends Exception {

    String message;

    public AttributeLengthUndefinedException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}
