package com.avanza.core.meta.audit;


/**
 * @author Mehran Junejo
 */

public class MetaEntityAuditId implements java.io.Serializable {

    // Fields

    private String metaEntityId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public MetaEntityAuditId() {
    }

    /**
     * full constructor
     */
    public MetaEntityAuditId(String metaEntityId, Integer rev) {
        this.metaEntityId = metaEntityId;
        this.rev = rev;
    }

    // Property accessors


    public Integer getRev() {
        return this.rev;
    }


    public String getMetaEntityId() {
        return metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }


    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof MetaEntityAuditId))
            return false;
        MetaEntityAuditId castOther = (MetaEntityAuditId) other;

        return ((this.getMetaEntityId() == castOther.getMetaEntityId()) || (this
                .getMetaEntityId() != null
                && castOther.getMetaEntityId() != null && this.getMetaEntityId().equals(
                castOther.getMetaEntityId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getMetaEntityId() == null ? 0 : this.getMetaEntityId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}