package com.avanza.core.meta.audit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.ui.ViewType;
import com.avanza.ui.meta.MetaViewAttribute;
import com.avanza.ui.meta.MetaViewMenu;
import com.avanza.ui.meta.MetaViewParam;

/**
 * @author Mehran Junejo
 */

public class MetaViewAudit extends DbObject {

    private static final long serialVersionUID = 877374682065898050L;

    private MetaViewAuditId metaViewAuditId;
    private Short revtype;

    private String systemName;

    private String primaryName;

    private String secondaryName;

    private String type;

    private MetaViewAudit parent;

    private String documentId;

    private boolean main;

    private boolean defaultview;

    private boolean readOnly;

    private String factoryClass;

    private String selectCriteria;

    private int displayOrder;

    private int pageSize;

    private String dimension;

    private int rowSize;

    private int colSize;

    private String placeHolderPos;

    private int rowPosition;

    private int colPosition;

    private String aggregateAttribId;

    private String orderByClause;

    private String dataProcessorClass;

    private boolean isConfirmationAction;

    private String iframeUrl;

    /*Edited by Nida for Stay on Page After document Creation-- Start*/
    private boolean stayOnSave;

    public boolean isStayOnSave() {
        return stayOnSave;
    }

    public void setStayOnSave(boolean stayOnSave) {
        this.stayOnSave = stayOnSave;
    }
    /*Edited by Nida for Stay on Page After document Creation-- End*/


    private Set<MetaViewAttribute> attributes = new HashSet<MetaViewAttribute>(0);

    private Set<MetaViewParam> params = new HashSet<MetaViewParam>(0);

    private Set<MetaViewAudit> subviews = new HashSet<MetaViewAudit>(0);

    private Set<MetaViewMenu> viewMenu = new HashSet<MetaViewMenu>(0);

    /*Edited by Bader for Collapaible menus*/
    private boolean isCollapsableMenu;

    private boolean isCollapsedState;

    public List<MetaViewMenu> getOrderedViewMenus() {
        List<MetaViewMenu> orderedMenus = new ArrayList<MetaViewMenu>();
        orderedMenus.addAll(viewMenu);
        Collections.sort(orderedMenus, new Comparator<MetaViewMenu>() {

            public int compare(MetaViewMenu o1, MetaViewMenu o2) {
                if (o1.getDisplayOrder() > o2.getDisplayOrder())
                    return 1;
                else if (o1.getDisplayOrder() < o2.getDisplayOrder()) return -1;
                return 0;
            }
        });

        return orderedMenus;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }


    public MetaViewAuditId getMetaViewAuditId() {
        return metaViewAuditId;
    }

    public void setMetaViewAuditId(MetaViewAuditId metaViewAuditId) {
        this.metaViewAuditId = metaViewAuditId;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String getFactoryClass() {
        return factoryClass;
    }

    public void setFactoryClass(String factoryClass) {
        this.factoryClass = factoryClass;
    }


    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getSecondaryName() {
        return secondaryName;
    }

    public void setSecondaryName(String secondaryName) {
        this.secondaryName = secondaryName;
    }

    public MetaViewAudit getParent() {
        return parent;
    }

    public void setParent(MetaViewAudit parent) {
        this.parent = parent;
    }

    public String getSelectCriteria() {
        return selectCriteria;
    }

    public void setSelectCriteria(String selectCriteria) {
        this.selectCriteria = selectCriteria;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public Set<MetaViewAttribute> getAttributes() {
        return attributes;
    }

    public boolean getIsConfirmationAction() {
        return isConfirmationAction;
    }

    public void setIsConfirmationAction(boolean isConfirmationAction) {
        this.isConfirmationAction = isConfirmationAction;
    }

    public List<MetaViewAttribute> getOrderedAttributes() {
        List<MetaViewAttribute> orderedAttributes = new ArrayList<MetaViewAttribute>();
        orderedAttributes.addAll(attributes);
        Collections.sort(orderedAttributes, new Comparator<MetaViewAttribute>() {

            public int compare(MetaViewAttribute o1, MetaViewAttribute o2) {
                if (o1.getDisplayOrder() > o2.getDisplayOrder())
                    return 1;
                else if (o1.getDisplayOrder() < o2.getDisplayOrder()) return -1;
                return 0;
            }
        });

        return orderedAttributes;
    }

    public void setAttributes(Set<MetaViewAttribute> attributes) {
        this.attributes = attributes;
    }

    public Set<MetaViewParam> getParams() {
        return params;
    }

    public void setParams(Set<MetaViewParam> params) {
        this.params = params;
    }

    public Set<MetaViewAudit> getSubviews() {
        Set<MetaViewAudit> tempSubviews = new HashSet<MetaViewAudit>(0);
        for (MetaViewAudit subview : subviews) {
            if (ViewType.fromString(subview.getType()) == ViewType.TabPanelView || ViewType.fromString(subview.getType()) == ViewType.TabView)
                tempSubviews.addAll(subview.getSubviews());
            else
                tempSubviews.add(subview);
        }
        return tempSubviews;
    }

    public List<MetaViewAudit> getOrderedSubviews() {
        List<MetaViewAudit> orderedViews = new ArrayList<MetaViewAudit>();
        orderedViews.addAll(subviews);
        Collections.sort(orderedViews, new Comparator<MetaViewAudit>() {

            public int compare(MetaViewAudit o1, MetaViewAudit o2) {
                if (o1.getDisplayOrder() > o2.getDisplayOrder())
                    return 1;
                else if (o1.getDisplayOrder() < o2.getDisplayOrder()) return -1;
                return 0;
            }
        });

        return orderedViews;
    }

    public void setSubviews(Set<MetaViewAudit> subviews) {
        this.subviews = subviews;
    }

    public boolean isDefaultview() {
        return defaultview;
    }

    public void setDefaultview(boolean defaultview) {
        this.defaultview = defaultview;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        dimension = dimension.toLowerCase();
        if (dimension.indexOf("x") != -1) {
            String size[] = dimension.split("x");
            if (size.length > 0) {
                int rowSize = Integer.parseInt(size[0]);
                int colSize = Integer.parseInt(size[1]);
                setRowSize(rowSize);
                setColSize(colSize);
            } else {
                setRowSize(0);
                setColSize(0);
            }
        } else {
            setRowSize(0);
            setColSize(0);
        }
        this.dimension = dimension;
    }

    public MetaViewAttribute getAttribute(String metaViewAttributeId) {

        MetaViewAttribute viewAttrib = null;

        for (Object elt : this.getAttributes()) {
            viewAttrib = (MetaViewAttribute) elt;
            if (viewAttrib.getId().equalsIgnoreCase(metaViewAttributeId)) return viewAttrib;
        }

        return viewAttrib;
    }

    public List<MetaViewAttribute> getViewAttributesByComponentType(String componentType) {
        List<MetaViewAttribute> list = new ArrayList<MetaViewAttribute>();
        for (MetaViewAttribute attribute : getOrderedAttributes()) {
            if (attribute.getComponentType().equals(componentType)) {
                list.add(attribute);
            }
        }
        return list;
    }

    public Set<MetaViewAttribute> getViewAttributeList(boolean complete) {

        Set<MetaViewAttribute> retVal;

        if (!complete) {

            retVal = new HashSet<MetaViewAttribute>(this.attributes.size());
            retVal.addAll(this.attributes);
            return retVal;
        } else {
            retVal = new HashSet<MetaViewAttribute>(this.attributes.size());
            retVal.addAll(this.attributes);
            Set<MetaViewAudit> subviews = this.getSubviews();
            for (MetaViewAudit subview : subviews) {
                retVal.addAll(subview.attributes);
            }
        }

        return retVal;
    }

    public Set<MetaViewAttribute> getViewPersistingAttributeList(boolean complete) {

        Set<MetaViewAttribute> retVal;

        if (!complete) {

            retVal = new HashSet<MetaViewAttribute>(this.attributes.size());
            retVal.addAll(this.attributes);
            return retVal;
        } else {
            retVal = new HashSet<MetaViewAttribute>(this.attributes.size());
            retVal.addAll(this.attributes);
            Set<MetaViewAudit> subviews = this.getSubviews();
            for (MetaViewAudit subview : subviews) {
                if (ViewType.fromString(subview.getType()) == ViewType.Section)
                    retVal.addAll(subview.attributes);
            }
        }

        return retVal;
    }

    public MetaViewAudit getTopMostParentView() {
        if (this.getParent() == null)
            return this;
        else {
            MetaViewAudit child;
            MetaViewAudit parView;
            while (true) {
                child = this;
                parView = this.getParent();
                if (parView != null) {
                    child = parView;
                    parView = child.getParent();
                } else {
                    break;
                }
                if (parView == null) return child;
                return parView;
            }
        }
        return null;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isPagingEnabled() {
        return (this.pageSize > 0);
    }

    public String getAggregateAttribId() {
        return aggregateAttribId;
    }

    public void setAggregateAttribId(String aggregateAttribId) {
        this.aggregateAttribId = aggregateAttribId;
    }

    public Set<MetaViewMenu> getViewMenu() {
        return viewMenu;
    }

    public void setViewMenu(Set<MetaViewMenu> viewMenu) {
        this.viewMenu = viewMenu;
    }

    public int getRowSize() {
        return rowSize;
    }

    public void setRowSize(int rowSize) {
        this.rowSize = rowSize;
    }

    public int getColSize() {
        return colSize;
    }

    public void setColSize(int colSize) {
        this.colSize = colSize;
    }

    public MetaViewParam getViewParameter(String paramid) {

        if (paramid != null) {
            for (MetaViewParam param : params) {
                if (param.getParamName().equalsIgnoreCase(paramid)) {
                    return param;
                }
            }
        }
        return null;

    }

    public String getPlaceHolderPos() {
        return placeHolderPos;
    }

    public void setPlaceHolderPos(String placeHolderPos) {
        if (placeHolderPos != null) {
            String pos[] = placeHolderPos.split(",");
            rowPosition = Integer.parseInt(pos[0]);
            colPosition = Integer.parseInt(pos[1]);
        }
        this.placeHolderPos = placeHolderPos;
    }

    public int getRowPosition() {
        return rowPosition;
    }

    public void setRowPosition(int rowPosition) {
        this.rowPosition = rowPosition;
    }

    public int getColPosition() {
        return colPosition;
    }

    public void setColPosition(int colPosition) {
        this.colPosition = colPosition;
    }

    public boolean isSingleListView() {
        int count = 0;
        for (MetaViewAudit view : this.getSubviews()) {
            if (ViewType.fromString(view.getType()) == ViewType.ListView) {
                count++;
            }
        }
        if (this.getParent() != null && ViewType.fromString(this.getParent().getType()) == ViewType.DetailView)
            return false;
        if (count > 1) return false;
        return true;
    }

    public MetaViewParam getParameter(String id) {
        for (MetaViewParam param : params) {
            {
                if (id.equalsIgnoreCase(param.getParamName())) return param;
            }
        }
        return null;
    }

    public String getSystemName() {
        return systemName;
    }


    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public void addSubview(MetaViewAudit view) {
        this.subviews.add(view);
    }

    public MetaViewAudit clone() {
        MetaViewAudit view = new MetaViewAudit();
        view.setAggregateAttribId(aggregateAttribId);
        view.setAttributes(attributes);
        view.setColPosition(colPosition);
        view.setColSize(colSize);
        view.setCreatedBy(this.getCreatedBy());
        view.setDefaultview(defaultview);
        view.setDimension(dimension);
        view.setDisplayOrder(displayOrder);
        view.setDocumentId(documentId);
        view.setFactoryClass(factoryClass);
        //view.setId(id);
        view.setMain(main);
        view.setPageSize(pageSize);
        view.setParams(params);
        view.setParent(parent);
        view.setPlaceHolderPos(placeHolderPos);
        view.setPrimaryName(primaryName);
        view.setReadOnly(readOnly);
        view.setRowPosition(rowPosition);
        view.setRowSize(rowSize);
        view.setSecondaryName(secondaryName);
        view.setSelectCriteria(selectCriteria);
        view.setSubviews(subviews);
        view.setSystemName(systemName);
        view.setType(type);
        view.setViewMenu(viewMenu);
        view.setOrderByClause(orderByClause);
        view.setDataProcessorClass(dataProcessorClass);
        view.setIsCollapsableMenu(isCollapsableMenu);
        view.setIframeUrl(iframeUrl);
        view.setStayOnSave(stayOnSave);
        return view;
    }


    public String getOrderByClause() {
        return orderByClause;
    }


    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getDataProcessorClass() {
        return dataProcessorClass;
    }

    public void setDataProcessorClass(String dataProcessorClass) {
        this.dataProcessorClass = dataProcessorClass;
    }

    public boolean getIsCollapsableMenu() {
        return isCollapsableMenu;
    }

    public void setIsCollapsableMenu(boolean isCollapsableMenu) {
        this.isCollapsableMenu = isCollapsableMenu;
    }

    public boolean getIsCollapsedState() {
        return isCollapsedState;
    }

    public void setIsCollapsedState(boolean isCollapsedState) {
        this.isCollapsedState = isCollapsedState;
    }

    public String getIframeUrl() {
        return iframeUrl;
    }

    public void setIframeUrl(String iframeUrl) {
        this.iframeUrl = iframeUrl;
    }
}
