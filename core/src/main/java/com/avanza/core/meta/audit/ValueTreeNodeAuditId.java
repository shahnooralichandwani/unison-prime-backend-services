package com.avanza.core.meta.audit;

/**
 * @author Mehran Junejo
 */

public class ValueTreeNodeAuditId implements java.io.Serializable {

    // Fields

    private String valueTreeNodeAuditId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public ValueTreeNodeAuditId() {
    }

    /**
     * full constructor
     */
    public ValueTreeNodeAuditId(String roleId, Integer rev) {
        this.valueTreeNodeAuditId = roleId;
        this.rev = rev;
    }

    // Property accessors


    public String getValueTreeNodeAuditId() {
        return valueTreeNodeAuditId;
    }

    public void setValueTreeNodeAuditId(String valueTreeNodeAuditId) {
        this.valueTreeNodeAuditId = valueTreeNodeAuditId;
    }

    public Integer getRev() {
        return this.rev;
    }


    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof ValueTreeNodeAuditId))
            return false;
        ValueTreeNodeAuditId castOther = (ValueTreeNodeAuditId) other;

        return ((this.getValueTreeNodeAuditId() == castOther.getValueTreeNodeAuditId()) || (this
                .getValueTreeNodeAuditId() != null
                && castOther.getValueTreeNodeAuditId() != null && this.getValueTreeNodeAuditId().equals(
                castOther.getValueTreeNodeAuditId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getValueTreeNodeAuditId() == null ? 0 : this.getValueTreeNodeAuditId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}