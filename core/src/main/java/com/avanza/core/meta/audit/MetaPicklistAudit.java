package com.avanza.core.meta.audit;

import java.util.Set;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DbObject;
import com.avanza.core.security.User;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;

/**
 * @author Mehran Junejo
 */

public class MetaPicklistAudit extends DbObject implements Comparable<MetaPicklistAudit> {

    /**
     *
     */
    private static final long serialVersionUID = 4664357203630385579L;

    private MetaPicklistAuditId metaPicklistAuditId;
    private Short revtype;

    private String systemName;

    private String parentId;

    private String PicklistName;// used as an alias to the query.

    private String Type;

    private String TableName;

    private String LinkColumn;

    private String LinkColumnType;

    private String DisplayColPri;

    private String DisplayColSec;

    private String SelectCritera;

    private MetaPicklistAudit parent;

    private String DisplayName;

    private boolean isSystem;
    private boolean isExternal;
    private boolean isKeyRequired;
    private String keyColumn;
    private String keyCounterName;

    private String displayAttrPrm;
    private String displayAttrSec;
    private String attributeKey;

    private Set<MetaPicklistAudit> childPicList;

    public String getSelectCritera() {
        return SelectCritera;
    }

    public void setSelectCritera(String selectCritera) {
        SelectCritera = selectCritera;
    }

    public String getDisplayColPri() {
        return DisplayColPri;
    }

    public void setDisplayColPri(String displayColPri) {
        DisplayColPri = displayColPri;
    }

    public String getDisplayColSec() {
        return DisplayColSec;
    }

    public void setDisplayColSec(String displayColSec) {
        DisplayColSec = displayColSec;
    }

    public String getLinkColumn() {
        return LinkColumn;
    }

    public void setLinkColumn(String linkColumn) {
        LinkColumn = linkColumn;
    }

    public String getPicklistName() {
        return PicklistName;
    }

    public void setPicklistName(String picklistName) {
        PicklistName = picklistName;
    }

    public String getTableName() {
        return TableName;
    }

    public void setTableName(String tableName) {
        TableName = tableName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public MetaPicklistAudit getParent() {
        return parent;
    }

    public void setParent(MetaPicklistAudit parent) {
        this.parent = parent;
    }

    public Set<MetaPicklistAudit> getChildPicList() {
        return childPicList;
    }

    public void setChildPicList(Set<MetaPicklistAudit> childPicList) {
        this.childPicList = childPicList;
    }

    public String getLinkColumnType() {
        return LinkColumnType;
    }

    public void setLinkColumnType(String linkColumnType) {
        LinkColumnType = linkColumnType;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public boolean isSystem() {
        return isSystem;
    }

    public boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(boolean isSystem) {
        this.isSystem = isSystem;
    }

    public boolean isExternal() {
        return isExternal;
    }

    public boolean getIsExternal() {
        return isExternal;
    }

    public void setIsExternal(boolean isExternal) {
        this.isExternal = isExternal;
    }

    public boolean isKeyRequired() {
        return isKeyRequired;
    }

    public boolean getIsKeyRequired() {
        return isKeyRequired;
    }

    public void setIsKeyRequired(boolean isKeyRequired) {
        this.isKeyRequired = isKeyRequired;
    }

    public String getKeyColumn() {
        return keyColumn;
    }

    public void setKeyColumn(String keyColumn) {
        this.keyColumn = keyColumn;
    }

    public String getKeyCounterName() {
        return keyCounterName;
    }

    public void setKeyCounterName(String keyCounterName) {
        this.keyCounterName = keyCounterName;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public int compareTo(MetaPicklistAudit arg0) {
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class.getName());
        boolean isPrimaryLocale = ((LocaleInfo) webContext.getAttribute(SessionKeyLookup.CurrentLocale)).isPrimary();

        return (isPrimaryLocale) ? this.getDisplayColPri().compareToIgnoreCase(((MetaPicklistAudit) arg0).getDisplayColPri()) :
                this.getDisplayColSec().compareToIgnoreCase(((MetaPicklistAudit) arg0).getDisplayColSec());
    }

    public String getDisplayAttrPrm() {
        return displayAttrPrm;
    }

    public void setDisplayAttrPrm(String displayAttrPrm) {
        this.displayAttrPrm = displayAttrPrm;
    }

    public String getDisplayAttrSec() {
        return displayAttrSec;
    }

    public void setDisplayAttrSec(String displayAttrSec) {
        this.displayAttrSec = displayAttrSec;
    }

    public String getAttributeKey() {
        return attributeKey;
    }

    public void setAttributeKey(String attributeKey) {
        this.attributeKey = attributeKey;
    }

    public MetaPicklistAuditId getMetaPicklistAuditId() {
        return metaPicklistAuditId;
    }

    public void setMetaPicklistAuditId(MetaPicklistAuditId metaPicklistAuditId) {
        this.metaPicklistAuditId = metaPicklistAuditId;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }
}
