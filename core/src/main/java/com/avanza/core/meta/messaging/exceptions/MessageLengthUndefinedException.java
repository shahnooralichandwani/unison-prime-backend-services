/**
 *
 */
package com.avanza.core.meta.messaging.exceptions;

/**
 * @author shahbaz.ali
 *
 */
public class MessageLengthUndefinedException extends Exception {

    String message;

    public MessageLengthUndefinedException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}
