package com.avanza.core.meta.audit;

/**
 * @author Mehran Junejo
 */

public class MetaViewRelationAuditId implements java.io.Serializable {

    // Fields

    private String metaViewRelationAuditId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public MetaViewRelationAuditId() {
    }

    /**
     * full constructor
     */
    public MetaViewRelationAuditId(String roleId, Integer rev) {
        this.metaViewRelationAuditId = roleId;
        this.rev = rev;
    }

    // Property accessors


    public String getMetaViewRelationAuditId() {
        return metaViewRelationAuditId;
    }

    public void setMetaViewRelationAuditId(String metaViewRelationAuditId) {
        this.metaViewRelationAuditId = metaViewRelationAuditId;
    }

    public Integer getRev() {
        return this.rev;
    }


    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof MetaViewRelationAuditId))
            return false;
        MetaViewRelationAuditId castOther = (MetaViewRelationAuditId) other;

        return ((this.getMetaViewRelationAuditId() == castOther.getMetaViewRelationAuditId()) || (this
                .getMetaViewRelationAuditId() != null
                && castOther.getMetaViewRelationAuditId() != null && this.getMetaViewRelationAuditId().equals(
                castOther.getMetaViewRelationAuditId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getMetaViewRelationAuditId() == null ? 0 : this.getMetaViewRelationAuditId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}