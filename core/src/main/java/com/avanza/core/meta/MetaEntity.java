package com.avanza.core.meta;

import com.avanza.core.data.DbObject;
import com.avanza.core.interceptor.Auditable;
import com.avanza.core.meta.audit.MetaEntityAudit;
import com.avanza.core.meta.audit.MetaEntityAuditId;
import com.avanza.core.sdo.DataObject;
import com.avanza.integration.dbmapper.meta.DataAccessMode;
import com.avanza.workflow.configuration.action.ActionBinding;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.util.*;

/**
 * @author fkazmi
 */
public class MetaEntity extends DbObject implements Serializable, Auditable {

    private static final long serialVersionUID = 5030324291311643331L;

    // persisted members
    private MetaEntity parent;
    private String parentId;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    private Map<String, MetaAttribute> attribList = new Hashtable<String, MetaAttribute>(0);
    private Map<String, MetaAttribute> attribIdMap = new Hashtable<String, MetaAttribute>(0);
    private Map<String, ActionBinding> actionBinding = new Hashtable<String, ActionBinding>(0);

    private String id;

    private String systemName;

    private String discriminatorCol;

    private String discriminatorValue;

    private String actionLog;

    private String activityLog;

    private String description;

    private String majorType;

    private boolean isAbstract;

    private String tableName;

    private String primaryName;

    private String secondaryName;


    private String categoryNode;

    private String entityName;

    private String documentPriority;
    private String salesPlanId;


    private boolean templateEnabled;

    private long cacheTime;

    // Transient members
    private int attribCount;

    private boolean visibleToChannels = true;

    private Set<MetaEntity> childList;

    /**
     * This Map field uses SubMetaEntityKey as and MetaRelationship as value
     */
    private Map<String, MetaRelationship> relationList = new Hashtable<String, MetaRelationship>();

    private Map<String, MetaRelationship> associationRelations = new Hashtable<String, MetaRelationship>();

    private Map<String, MetaRelationship> subEntityRelations = new Hashtable<String, MetaRelationship>();

    public String getDiscriminatorCol() {
        return discriminatorCol;
    }

    public void setDiscriminatorCol(String discriminatorCol) {
        this.discriminatorCol = discriminatorCol;
    }

    protected MetaEntity() {

        this.attribList = new HashMap<String, MetaAttribute>();
        this.childList = new HashSet<MetaEntity>();
    }

    public MetaEntity(MetaEntity parent) {

        this.setParent(parent);
        this.attribList = new HashMap<String, MetaAttribute>();
        this.childList = new HashSet<MetaEntity>();
    }

    public String getActionLog() {
        return this.actionLog;
    }

    public void setActionLog(String actionLogTable) {
        this.actionLog = actionLogTable;
    }

    public String getActivityLog() {
        return this.activityLog;
    }

    public void setActivityLog(String activityLogTable) {
        this.activityLog = activityLogTable;
    }

    public String getCategoryNode() {
        return this.categoryNode;
    }

    public void setCategoryNode(String categoryNode) {
        this.categoryNode = categoryNode;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSystemName() {
        return this.systemName;
    }

    public String getMajorType() {
        return this.majorType;
    }

    public void setMajorType(String majorType) {
        this.majorType = majorType;
    }

    public String getPrimaryName() {
        return this.primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getSecondaryName() {
        return this.secondaryName;
    }

    public void setSecondaryName(String secondaryName) {
        this.secondaryName = secondaryName;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public boolean getIsAbstract() {
        return isAbstract;
    }

    public MetaEntity getParent() {

        return parent;
    }

    public void setParent(MetaEntity parent) {
        this.parent = parent;
    }

    public boolean isVisibleToChannels() {
        return visibleToChannels;
    }

    public void setVisibleToChannels(boolean visibleToChannels) {
        this.visibleToChannels = visibleToChannels;
    }

    // added for DIB
    public String getDocumentPriority() {
        return documentPriority;
    }

    public void setDocumentPriority(String documentPriority) {
        this.documentPriority = documentPriority;
    }


    public String getSalesPlanId() {
        return salesPlanId;
    }

    public void setSalesPlanId(String salesPlanId) {
        this.salesPlanId = salesPlanId;
    }

    public DataObject createEntity() {

        try {
            DataObject obj = new DataObject(this);
            return obj;
        } catch (Exception e) {
            String msg = String.format("[CreateObject]-Invalid metaEntity Name : %1$s", new Object[]{this.id});
            throw new MetaException(e, msg);

        }
    }

    public DataObject createEntity(HashMap values) {

        try {
            DataObject obj = new DataObject(this, values, DataAccessMode.Detail);
            return obj;
        } catch (Exception e) {
            String msg = String.format("[CreateObject]-Invalid metaEntity Name : %1$s", new Object[]{this.id});
            throw new MetaException(e, msg);

        }
    }

    // Utility/Helper Methods
    public boolean isSubclass(String metaEntityId) {
//TODO: Naushad 
		/*MetaEntity entity = this;
		boolean retVal = false;

		while (entity != null) {

			if (entity.isSameEntity(metaEntityId)) {
				retVal = true;
				break;
			}

			entity = this.parent;
		}*/

        //	return retVal;
        return true;
    }

    public boolean isDerived(String metaEntityId) {

        if (this.isSameEntity(metaEntityId))
            return true;

        for (MetaEntity entity : this.childList) {

            if (entity.isDerived(metaEntityId))
                return true;
        }

        return false;
    }

    public int size(boolean complete) {

        int retVal;

        if (complete) {

            if (this.attribCount == -1)
                this.attribCount = this.getAttributeCount();

            retVal = this.attribCount;
        } else
            retVal = this.attribList.size();

        return retVal;
    }

    public int size() {

        return this.size(true);
    }

    public boolean hasInternalPrimary() {

        for (MetaAttribute meta : this.getAttributeList(true).values()) {

            if (meta.isInternalPrimary()) {
                return true;
            }
        }

        return false;
    }

    public MetaAttribute getInternalPrimaryAttribute() {

        MetaAttribute retVal = null;

        for (MetaAttribute meta : this.getAttributeList(true).values()) {

            if (meta.isInternalPrimary()) {
                retVal = meta;
                break;
            }
        }

        return retVal;
    }

    public MetaAttribute getIdAttribute() {

        MetaAttribute retVal = this.getInternalPrimaryAttribute();

        if (retVal == null) {
            retVal = this.getPrimaryKeyAttribute();
        }

        return retVal;
    }

    public MetaAttribute getPrimaryKeyAttribute() {

        MetaAttribute retVal = null;

        for (MetaAttribute meta : this.getAttributeList(true).values()) {

            if (meta.getIsPrimary()) {
                retVal = meta;
                break;
            }
        }

        if (retVal == null)
            throw new MetaException("Entity [%1$s] must specify one of it's field as Primary Key");

        return retVal;
    }

    public Map<String, MetaAttribute> getAttributeList(boolean complete) {

        Hashtable<String, MetaAttribute> retVal;

        if (!complete) {

            retVal = new Hashtable<String, MetaAttribute>(this.attribList.size());
            retVal.putAll(this.attribList);
            return retVal;
        } else {

            retVal = new Hashtable<String, MetaAttribute>(this.attribCount);
            Stack<MetaEntity> stack = this.getEntityStack();
            while (!stack.empty()) {

                MetaEntity entity = stack.pop();
                retVal.putAll(entity.attribList);
            }
        }

        return retVal;
    }

    public Map<String, MetaAttribute> getAttributeList() {

        return this.getAttributeList(true);
    }

    public List<String> getAttributeNames(boolean complete) {

        ArrayList<String> list = new ArrayList<String>(this.size(complete));

        if (complete) {

            Stack<MetaEntity> stack = this.getEntityStack();

            MetaEntity entity;
            while (!stack.empty()) {
                entity = stack.pop();
                list.addAll(entity.attribList.keySet());
            }
        } else {
            list.addAll(this.attribList.keySet());
        }

        return list;
    }

    public List<String> getAttributeNames() {
        return this.getAttributeNames(true);
    }

    public MetaAttribute getAttribute(String metaAttribId) {

        MetaAttribute retVal = null;
        Stack<MetaEntity> stack = this.getEntityStack();

        MetaEntity entity;
        while (!stack.empty()) {
            entity = stack.pop();
            retVal = entity.getAttribList().get(metaAttribId);
            if (retVal == null)
                retVal = entity.getAttribIdMap().get(metaAttribId);
            if (retVal != null)
                break;
        }

        return retVal;
    }

    public void addAttribute(String metaAttribId, MetaAttribute attrib) {
        this.attribList.put(metaAttribId, attrib);
    }

    public void removeAttribute(String metaAttribId) {
        this.attribList.remove(metaAttribId);
    }

    protected Map<String, MetaAttribute> getAttribList() {

        return attribList;
    }

    protected void setAttribList(Map<String, MetaAttribute> attribList) {

        this.attribList = attribList;
    }

    private boolean isSameEntity(String metaEntityId) {

        return (this.id.compareTo(metaEntityId) == 0);
    }

    private int getAttributeCount() {

        MetaEntity entity = this;
        int retVal = 0;

        while (entity != null) {
            retVal = entity.attribList.size();
            entity = this.parent;
        }

        return retVal;
    }

    private Stack<MetaEntity> getEntityStack() {

        Stack<MetaEntity> stack = new Stack<MetaEntity>();

        MetaEntity entity = this;
        while (entity != null) {

            stack.push(entity);
            entity = entity.parent;
        }

        return stack;
    }

    public Map<String, MetaRelationship> getRelationList() {
        return relationList;
    }

    void setRelationList(Map<String, MetaRelationship> relationList) {
        this.relationList = relationList;
    }

    public boolean isRelated(String subEntityId) {

        for (MetaRelationship rel : relationList.values()) {

            if (rel.getSubEntityId().equalsIgnoreCase(subEntityId))
                return true;
        }

        return false;
    }

    public Set<MetaEntity> getChildList() {
        return childList;
    }

    public void setChildList(Set<MetaEntity> childList) {
        this.childList = childList;
    }

    /**
     * This Functions returns a list because two entities can be associated with
     * each other in more than one relation. Like FundTransfer entity will have
     * two associations with Account (From Account and To Account). This
     * Functions throws exception if no relation with given Entity Id is found
     *
     * @param subEntityId
     * @return
     */
    public List<MetaRelationship> getRelationShipBySubEntity(String subEntityId) {

        List<MetaRelationship> retVal = new Vector<MetaRelationship>();
        for (MetaRelationship rel : relationList.values()) {

            if (rel.getSubEntityId().equalsIgnoreCase(subEntityId))
                retVal.add(rel);
        }

        if (!retVal.isEmpty())
            return retVal;
        else
            throw new MetaException("No Relation Exist between: " + this.getId() + " And " + subEntityId);
    }

    public MetaRelationship getRelationshipByName(String relationshipId) {

        if (!this.doesRelationshipExist(relationshipId))
            throw new MetaException("No Such Meta Relation Exist: " + relationshipId);

        return relationList.get(relationshipId);
    }

    public boolean doesRelationshipExist(String relationshipId) {

        return relationList.containsKey(relationshipId);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final MetaEntity other = (MetaEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public Map<String, MetaRelationship> getAssociationRelations() {
        return associationRelations;
    }

    public void setAssociationRelations(Map<String, MetaRelationship> associationRelations) {
        this.associationRelations = associationRelations;
    }

    public Map<String, MetaRelationship> getSubEntityRelations() {
        return subEntityRelations;
    }

    public void setSubEntityRelations(Map<String, MetaRelationship> subEntityRelations) {
        this.subEntityRelations = subEntityRelations;
    }

    public String getDiscriminatorValue() {
        return discriminatorValue;
    }

    public void setDiscriminatorValue(String discriminatorValue) {
        this.discriminatorValue = discriminatorValue;
    }

    public boolean isTemplateEnabled() {
        return templateEnabled;
    }

    public void setTemplateEnabled(boolean templateEnabled) {
        this.templateEnabled = templateEnabled;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    protected Map<String, MetaAttribute> getAttribIdMap() {
        return attribIdMap;
    }

    protected void setAttribIdMap(Map<String, MetaAttribute> attribIdMap) {
        this.attribIdMap = attribIdMap;
    }

    public long getCacheTime() {
        return cacheTime;
    }

    public void setCacheTime(long cacheTime) {
        this.cacheTime = cacheTime;
    }

    public Map<String, ActionBinding> getActionBinding() {
        return actionBinding;
    }

    public void setActionBinding(Map<String, ActionBinding> actionBinding) {
        this.actionBinding = actionBinding;
    }


    @Override
    public Object getAuditEntry(Short revtype) {
        MetaEntityAudit metaEntityAudit = null;
        try {

            Random rand = new Random();

            metaEntityAudit = new MetaEntityAudit();
            metaEntityAudit.setRevtype(revtype);

            BeanUtils.copyProperties(metaEntityAudit, this);
            metaEntityAudit.setMetaEntityAuditId(new MetaEntityAuditId(this.id, rand.nextInt()));


        } catch (Exception e) {
            metaEntityAudit = null;
        }
        return metaEntityAudit;
    }


}
