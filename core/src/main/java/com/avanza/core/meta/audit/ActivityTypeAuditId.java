package com.avanza.core.meta.audit;

/**
 * @author Mehran Junejo
 */

public class ActivityTypeAuditId implements java.io.Serializable {

    // Fields

    private String activityTypeId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public ActivityTypeAuditId() {
    }

    /**
     * full constructor
     */
    public ActivityTypeAuditId(String activityTypeId, Integer rev) {
        this.activityTypeId = activityTypeId;
        this.rev = rev;
    }

    // Property accessors


    public Integer getRev() {
        return this.rev;
    }


    public String getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof ActivityTypeAuditId))
            return false;
        ActivityTypeAuditId castOther = (ActivityTypeAuditId) other;

        return ((this.getActivityTypeId() == castOther.getActivityTypeId()) || (this
                .getActivityTypeId() != null
                && castOther.getActivityTypeId() != null && this.getActivityTypeId().equals(
                castOther.getActivityTypeId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getActivityTypeId() == null ? 0 : this.getActivityTypeId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}