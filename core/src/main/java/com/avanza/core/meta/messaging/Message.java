package com.avanza.core.meta.messaging;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.avanza.core.meta.MetaEntity;

/**
 * Message entity. @author MyEclipse Persistence Tools
 */

public class Message implements java.io.Serializable {

    // Fields

    private String messageId;
    private MetaEntity metaEntity;
    private MessageFormatType messageFormatType;
    private String action;
    private String messageParams;
    private String messageName;
    private String formaterClass;
    private Date createdOn;
    private String createdBy;
    private Date updatedOn;
    private String updatedBy;
    private String signatureXpath;
    private String signaturedDataXpath;
    private Set<MessageAttribute> messageAttributes = new HashSet<MessageAttribute>(0);


    // Constructors

    /**
     * default constructor
     */
    public Message() {
    }

    /**
     * minimal constructor
     */
    public Message(String messageId, String messageName, Date createdOn,
                   String createdBy, Timestamp updatedOn, String updatedBy) {
        this.messageId = messageId;
        this.messageName = messageName;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
    }

    /**
     * full constructor
     */
    public Message(String messageId, MetaEntity metaEntity,
                   MessageFormatType messageFormatType, String action,
                   String messageParams, String messageName, String formaterClass,
                   Date createdOn, String createdBy, Date updatedOn,
                   String updatedBy, Set<MessageAttribute> messageAttributesForMessageId,
                   Set messageAttributesForSubMessageId) {
        this.messageId = messageId;
        this.metaEntity = metaEntity;
        this.messageFormatType = messageFormatType;
        this.action = action;
        this.messageParams = messageParams;
        this.messageName = messageName;
        this.formaterClass = formaterClass;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
        this.messageAttributes = messageAttributesForMessageId;
    }

    // Property accessors

    public String getMessageId() {
        return this.messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public MetaEntity getMetaEntity() {
        return this.metaEntity;
    }

    public void setMetaEntity(MetaEntity metaEntity) {
        this.metaEntity = metaEntity;
    }

    public MessageFormatType getMessageFormatType() {
        return this.messageFormatType;
    }

    public void setMessageFormatType(MessageFormatType messageFormatType) {
        this.messageFormatType = messageFormatType;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMessageParams() {
        return this.messageParams;
    }

    public void setMessageParams(String messageParams) {
        this.messageParams = messageParams;
    }

    public String getMessageName() {
        return this.messageName;
    }

    public void setMessageName(String messageName) {
        this.messageName = messageName;
    }

    public String getFormaterClass() {
        return this.formaterClass;
    }

    public void setFormaterClass(String formaterClass) {
        this.formaterClass = formaterClass;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set<MessageAttribute> getMessageAttributes() {
        return this.messageAttributes;
    }

    public void setMessageAttributes(Set<MessageAttribute> messageAttributesForMessageId) {
        this.messageAttributes = messageAttributesForMessageId;
    }

    public List<MessageAttribute> getSortedAttributes() {

        ArrayList<MessageAttribute> tempList = new ArrayList<MessageAttribute>();
        tempList.addAll(this.messageAttributes);
        Collections.sort(tempList);

        return tempList;
    }

    public boolean hasSubMessage() {

        boolean subMsgFound = false;

        for (MessageAttribute mAttribute : this.messageAttributes) {
            if (mAttribute.getSubMessage() != null) {
                subMsgFound = true;
                break;
            }
        }

        return subMsgFound;
    }

    public String getSignatureXpath() {
        return signatureXpath;
    }

    public void setSignatureXpath(String signatureXpath) {
        this.signatureXpath = signatureXpath;
    }

    public String getSignaturedDataXpath() {
        return signaturedDataXpath;
    }

    public void setSignaturedDataXpath(String signaturedDataXpath) {
        this.signaturedDataXpath = signaturedDataXpath;
    }
}