package com.avanza.core.meta.messaging;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * MessageFormatType entity. @author MyEclipse Persistence Tools
 */

public class MessageFormatType implements java.io.Serializable {

    // Fields

    private String messageFormatTypeId;
    private String formaterClass;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private Set messages = new HashSet(0);

    // Constructors

    /**
     * default constructor
     */
    public MessageFormatType() {
    }

    /**
     * minimal constructor
     */
    public MessageFormatType(String messageFormatTypeId, Timestamp createdOn,
                             String createdBy, Timestamp updatedOn, String updatedBy) {
        this.messageFormatTypeId = messageFormatTypeId;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
    }

    /**
     * full constructor
     */
    public MessageFormatType(String messageFormatTypeId, String formaterClass,
                             Timestamp createdOn, String createdBy, Timestamp updatedOn,
                             String updatedBy, Set messages) {
        this.messageFormatTypeId = messageFormatTypeId;
        this.formaterClass = formaterClass;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
        this.messages = messages;
    }

    // Property accessors

    public String getMessageFormatTypeId() {
        return this.messageFormatTypeId;
    }

    public void setMessageFormatTypeId(String messageFormatTypeId) {
        this.messageFormatTypeId = messageFormatTypeId;
    }

    public String getFormaterClass() {
        return this.formaterClass;
    }

    public void setFormaterClass(String formaterClass) {
        this.formaterClass = formaterClass;
    }

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set getMessages() {
        return this.messages;
    }

    public void setMessages(Set messages) {
        this.messages = messages;
    }

}