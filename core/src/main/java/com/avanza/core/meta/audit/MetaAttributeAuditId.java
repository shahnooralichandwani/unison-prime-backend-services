package com.avanza.core.meta.audit;

/**
 * @author Mehran Junejo
 */

public class MetaAttributeAuditId implements java.io.Serializable {

    // Fields

    private String metaAttributeId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public MetaAttributeAuditId() {
    }

    /**
     * full constructor
     */
    public MetaAttributeAuditId(String roleId, Integer rev) {
        this.metaAttributeId = roleId;
        this.rev = rev;
    }

    // Property accessors


    public Integer getRev() {
        return this.rev;
    }


    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public String getMetaAttributeId() {
        return metaAttributeId;
    }

    public void setMetaAttributeId(String metaAttributeId) {
        this.metaAttributeId = metaAttributeId;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof MetaAttributeAuditId))
            return false;
        MetaAttributeAuditId castOther = (MetaAttributeAuditId) other;

        return ((this.getMetaAttributeId() == castOther.getMetaAttributeId()) || (this
                .getMetaAttributeId() != null
                && castOther.getMetaAttributeId() != null && this.getMetaAttributeId().equals(
                castOther.getMetaAttributeId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getMetaAttributeId() == null ? 0 : this.getMetaAttributeId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}