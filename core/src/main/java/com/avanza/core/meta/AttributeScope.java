package com.avanza.core.meta;


public enum AttributeScope {

    None(0), Private(1), ReadOnly(2), FirstTimeEdit(3), Edit(4), Mandatory(5);

    private int intValue;

    private AttributeScope(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static AttributeScope fromString(String value) {

        AttributeScope retVal = null;

        if (value.equalsIgnoreCase("None")) retVal = AttributeScope.None;
        if (value.equalsIgnoreCase("Edit")) retVal = AttributeScope.Edit;
        if (value.equalsIgnoreCase("Private")) retVal = AttributeScope.Private;
        if (value.equalsIgnoreCase("ReadOnly")) retVal = AttributeScope.ReadOnly;
        if (value.equalsIgnoreCase("FirstTimeEdit")) retVal = AttributeScope.FirstTimeEdit;
        if (value.equalsIgnoreCase("Mandatory")) retVal = AttributeScope.Mandatory;

        return retVal;
    }
}