package com.avanza.core.meta.messaging;

import java.sql.Timestamp;

import com.avanza.core.meta.MetaAttributeImpl;

/**
 * MessageAttribute entity. @author MyEclipse Persistence Tools
 */

public class MessageAttribute implements java.io.Serializable, Comparable<MessageAttribute> {

    // Fields

    /**
     *
     */
    private static final long serialVersionUID = -3615584051281185336L;
    private String messageAttributeId;
    private MessageAttributeType messageAttributeType;
    private MetaAttributeImpl metaEntityAttrib;
    private Message message;
    private Message subMessage;
    private Integer placeIndicator;
    private String attributeName;
    private String description;
    private String valueExpression;
    private String messageAttribParams;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;

    private MessageAttribute parent;
    private String XMLAttributeName;


    public String getXMLAttributeName() {
        return XMLAttributeName;
    }

    public void setXMLAttributeName(String xMLAttributeName) {
        XMLAttributeName = xMLAttributeName;
    }

    public MessageAttribute getParent() {
        return parent;
    }

    public void setParent(MessageAttribute parent) {
        this.parent = parent;
    }
    // Constructors


    /**
     * default constructor
     */
    public MessageAttribute() {
    }

    /**
     * minimal constructor
     */
    public MessageAttribute(String messageAttributeId, Integer placeIndicator,
                            Timestamp createdOn, String createdBy, Timestamp updatedOn,
                            String updatedBy) {
        this.messageAttributeId = messageAttributeId;
        this.placeIndicator = placeIndicator;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
    }

    /**
     * full constructor
     */
    public MessageAttribute(String messageAttributeId,
                            MessageAttributeType messageAttributeType,
                            MetaAttributeImpl metaEntityAttrib, Message messageByMessageId,
                            Message messageBySubMessageId, Integer placeIndicator,
                            String attributeName, String description, String valueExpression,
                            String messageAttribParams, Timestamp createdOn, String createdBy,
                            Timestamp updatedOn, String updatedBy) {
        this.messageAttributeId = messageAttributeId;
        this.messageAttributeType = messageAttributeType;
        this.metaEntityAttrib = metaEntityAttrib;
        this.message = messageByMessageId;
        this.subMessage = messageBySubMessageId;
        this.placeIndicator = placeIndicator;
        this.attributeName = attributeName;
        this.description = description;
        this.valueExpression = valueExpression;
        this.messageAttribParams = messageAttribParams;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
    }

    // Property accessors

    public String getMessageAttributeId() {
        return this.messageAttributeId;
    }

    public void setMessageAttributeId(String messageAttributeId) {
        this.messageAttributeId = messageAttributeId;
    }

    public MessageAttributeType getMessageAttributeType() {
        return this.messageAttributeType;
    }

    public void setMessageAttributeType(MessageAttributeType messageAttributeType) {
        this.messageAttributeType = messageAttributeType;
    }

    public MetaAttributeImpl getMetaEntityAttrib() {
        return this.metaEntityAttrib;
    }

    public void setMetaEntityAttrib(MetaAttributeImpl metaEntityAttrib) {
        this.metaEntityAttrib = metaEntityAttrib;
    }


    public Integer getPlaceIndicator() {
        return this.placeIndicator;
    }

    public void setPlaceIndicator(Integer placeIndicator) {
        this.placeIndicator = placeIndicator;
    }

    public String getAttributeName() {
        return this.attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValueExpression() {
        return this.valueExpression;
    }

    public void setValueExpression(String valueExpression) {
        this.valueExpression = valueExpression;
    }

    public String getMessageAttribParams() {
        return this.messageAttribParams;
    }

    public void setMessageAttribParams(String messageAttribParams) {
        this.messageAttribParams = messageAttribParams;
    }

    public Timestamp getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public int compareTo(MessageAttribute messageAttribute) {

        if (messageAttribute.getPlaceIndicator() < getPlaceIndicator()) {
            return 1;
        }

        if (messageAttribute.getPlaceIndicator() > getPlaceIndicator()) {
            return -1;
        } else
            return 0;

    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Message getSubMessage() {
        return subMessage;
    }

    public void setSubMessage(Message subMessage) {
        this.subMessage = subMessage;
    }

}