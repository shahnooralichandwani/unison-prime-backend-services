/**
 *
 */
package com.avanza.core.meta.messaging;

import com.avanza.core.util.Logger;

/**
 * @author shahbaz.ali
 *
 */
public class TransactionDateTime implements MessageAttributeTypeInterface {

    private final Logger logger = Logger.getLogger(TransactionDateTime.class);

    @Override
    public Object getDefaultValue() {
        logger.logInfo("Transaction ID.getDefaultValue called");
        return java.util.Calendar.getInstance().getTimeInMillis() + "";
    }

}
