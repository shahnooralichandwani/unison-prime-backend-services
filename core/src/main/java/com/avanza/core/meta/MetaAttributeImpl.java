package com.avanza.core.meta;

import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.interceptor.Auditable;
import com.avanza.core.meta.audit.MetaAttributeAudit;
import com.avanza.core.meta.audit.MetaAttributeAuditId;
import com.avanza.core.sdo.Attribute;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.StringHelper;
import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.util.Random;

/**
 * @author fkazmi
 */
public class MetaAttributeImpl extends MetaAttribute implements Serializable, Auditable {

    private static final long serialVersionUID = 1486375359920892832L;

    @Override
    public Attribute createAttribute() throws MetaException {
        Attribute attrib = null;

        String val = "";
        String expression = this.getDefaultExp();

        if (!StringHelper.isEmpty(expression)) {
            val = (String) ExpressionHelper.getExpressionResult(expression,
                    String.class);
        }

        /*
         * Shahbaz: Bug: Inserting null in case of empty value
         *
         */
        if (StringHelper.isEmpty(val))
            return attrib = new Attribute(this, null);
        else
            return attrib = new Attribute(this, ConvertUtil.parse(val, this
                    .getType().getJavaType()));
    }

    @Override
    public Attribute createAttribute(Object value) throws MetaException {
        Attribute attrib = null;

        if (value != null) {
            attrib = new Attribute(this, value);
        } else {
            attrib = new Attribute(this, ConvertUtil.parse(null, this.getType()
                    .getJavaType()));
        }
        return attrib;
    }


    @Override
    public Object getAuditEntry(Short revtype) {
        MetaAttributeAudit metaAttributeAudit = null;
        try {

            Random rand = new Random();

            metaAttributeAudit = new MetaAttributeAudit();
            metaAttributeAudit.setRevtype(revtype);

            BeanUtils.copyProperties(metaAttributeAudit, this);
            metaAttributeAudit.setMetaAttributeAuditId(new MetaAttributeAuditId(this.getId(), rand.nextInt()));

            //secUserV.setId(new SecUserVId(this.loginId, CounterUtils.getNextAuditTableKey()));
        } catch (Exception e) {
            metaAttributeAudit = null;
        }
        return metaAttributeAudit;
    }


}
