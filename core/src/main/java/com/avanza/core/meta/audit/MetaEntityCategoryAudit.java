package com.avanza.core.meta.audit;

import com.avanza.core.data.DbObject;

/**
 * @author Mehran Junejo
 */

public class MetaEntityCategoryAudit extends DbObject {


    private MetaEntityCategoryAuditId metaEntityCategoryAuditId;
    private Short revtype;

    private String entityCategoryId;

    private String metaEntityId;

    private String treeNodeId;

    private String actionClass;


    public String getMetaEntityId() {
        return metaEntityId;
    }

    public String getEntityCategoryId() {
        return entityCategoryId;
    }

    public void setEntityCategoryId(String entityCategoryId) {
        this.entityCategoryId = entityCategoryId;
    }

    public MetaEntityCategoryAuditId getMetaEntityCategoryAuditId() {
        return metaEntityCategoryAuditId;
    }

    public void setMetaEntityCategoryAuditId(
            MetaEntityCategoryAuditId metaEntityCategoryAuditId) {
        this.metaEntityCategoryAuditId = metaEntityCategoryAuditId;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

    public String getTreeNodeId() {
        return treeNodeId;
    }

    public void setTreeNodeId(String treeNodeId) {
        this.treeNodeId = treeNodeId;
    }

    public String getActionClass() {
        return actionClass;
    }

    public void setActionClass(String actionClass) {
        this.actionClass = actionClass;
    }
}
