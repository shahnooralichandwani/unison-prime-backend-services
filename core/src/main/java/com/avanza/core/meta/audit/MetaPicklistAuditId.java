package com.avanza.core.meta.audit;

/**
 * @author Mehran Junejo
 */

public class MetaPicklistAuditId implements java.io.Serializable {

    // Fields

    private String metaPicklistId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public MetaPicklistAuditId() {
    }

    /**
     * full constructor
     */
    public MetaPicklistAuditId(String roleId, Integer rev) {
        this.metaPicklistId = roleId;
        this.rev = rev;
    }

    // Property accessors


    public Integer getRev() {
        return this.rev;
    }


    public void setRev(Integer rev) {
        this.rev = rev;
    }


    public String getMetaPicklistId() {
        return metaPicklistId;
    }

    public void setMetaPicklistId(String metaPicklistId) {
        this.metaPicklistId = metaPicklistId;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof MetaPicklistAuditId))
            return false;
        MetaPicklistAuditId castOther = (MetaPicklistAuditId) other;

        return ((this.getMetaPicklistId() == castOther.getMetaPicklistId()) || (this
                .getMetaPicklistId() != null
                && castOther.getMetaPicklistId() != null && this.getMetaPicklistId().equals(
                castOther.getMetaPicklistId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getMetaPicklistId() == null ? 0 : this.getMetaPicklistId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}