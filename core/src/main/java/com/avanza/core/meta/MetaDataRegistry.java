package com.avanza.core.meta;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.HibernateDataBroker;
import com.avanza.core.data.sequence.SequenceInfo;
import com.avanza.core.data.sequence.SequenceManager;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.ValueTreeManager;
import com.avanza.core.util.Guard;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TimeLogger;
import com.avanza.integration.dbmapper.meta.DataAccessMode;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author fkazmi
 */

public class MetaDataRegistry {

    private static final Logger logger = Logger.getLogger(MetaDataRegistry.class);

    private static Hashtable<String, MetaEntity> metEntList = new Hashtable<String, MetaEntity>();

    private static Hashtable<String, MetaEntity> metEntSysList = new Hashtable<String, MetaEntity>();

    private static Hashtable<String, MetaCounter> metCounterList = new Hashtable<String, MetaCounter>();

    private static Hashtable<String, ValueTreeNode> valueTreeNodeList = new Hashtable<String, ValueTreeNode>();

    private static boolean isLoaded = false;

    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock readLock = readWriteLock.readLock();
    private static Lock writeLock = readWriteLock.writeLock();

    private MetaDataRegistry() {

    }

    public static void load() {
        if (isLoaded) return;
        try {
            HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker(MetaDataRegistry.class.getName());
            List<MetaEntity> list = broker.findAll(MetaEntity.class);
            for (int i = 0; i < list.size(); i++) {
                MetaEntity ent = list.get(i);
                String Id = ent.getId();
                addMetaEntity(Id, ent);
            }

            List<ValueTreeNode> ValueTreeNodeList = broker.findAll(ValueTreeNode.class);
            Collections.sort(ValueTreeNodeList, new Comparator<ValueTreeNode>() {

                public int compare(ValueTreeNode o1, ValueTreeNode o2) {
                    return o1.getPrimaryName().compareToIgnoreCase(o2.getPrimaryName());
                }
            });
            for (int i = 0; i < ValueTreeNodeList.size(); i++) {
                ValueTreeNode valueTreeNode = (ValueTreeNode) ValueTreeNodeList.get(i);
                String Id = valueTreeNode.getId();
                addValueTreeNodeList(Id, valueTreeNode);
            }
            isLoaded = true;

        } catch (Exception e) {
            throw new MetaException(e, "Error While Loading Meta Data");
        }
    }

    private static void addValueTreeNodeList(String id, ValueTreeNode valueTreeNode) {
        valueTreeNodeList.put(id, valueTreeNode);
    }

    public static void dispose() {
        metEntList = new Hashtable<String, MetaEntity>();
    }

    public static Hashtable<String, MetaEntity> getEntityList() {
        return metEntList;
    }

    public static Hashtable<String, MetaEntity> getEntityList(String brokerKey) {

        Guard.checkNullOrEmpty(brokerKey, "MetaDataRegistry.getEntityList(brokerKey)");
        Hashtable<String, MetaEntity> entities = new Hashtable<String, MetaEntity>();
        for (MetaEntity ent : metEntList.values()) {

            if (!ent.getSystemName().equalsIgnoreCase(brokerKey)) {

                String[] keys = ent.getSystemName().split("\\.");
                int index = keys.length - 2;
                boolean foundFlag = false;

                while (!foundFlag && index >= 0) {

                    foundFlag = brokerKey.equalsIgnoreCase((StringHelper.getCombinedKeyFrom(keys, index)));
                    index--;
                }

                if (foundFlag) entities.put(ent.getSystemName(), ent);

            } else {
                entities.put(ent.getSystemName(), ent);
            }
        }

        return entities;

    }

    public static MetaEntity getMetaEntity(String name) {
        return (MetaEntity) metEntList.get(name);
    }

    public static MetaEntity getEntityBySystemName(String systemName) {
        if (StringHelper.isEmpty(systemName)) return null;
        return (MetaEntity) metEntSysList.get(systemName);
    }

    public static MetaCounter getMetaCounter(String name) {
        return (MetaCounter) metCounterList.get(name);
    }

    public static ValueTreeNode getValueTreeNode(String name) {
        return (ValueTreeNode) valueTreeNodeList.get(name);
    }

    private static void addMetaEntity(String metaEntName, MetaEntity metaEnt) {
        metEntList.put(metaEntName, metaEnt);
        metEntSysList.put(metaEnt.getSystemName(), metaEnt);
    }

    private static void addMetaCounter(String metaCounterName, MetaCounter metCounter) {
        metCounterList.put(metaCounterName, metCounter);
    }

    private static void updateMetaCounter(String counterName, MetaCounter counter) {
        metCounterList.put(counterName, counter);
        HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker("HibernateDataBroker");
        broker.update(counter);

    }

    public List<String> getDerivedEntities(String metEntId) {
        List<String> derivedEntities = new ArrayList<String>();
        Iterator<String> itr = metEntList.keySet().iterator();
        while (itr.hasNext()) {
            MetaEntity ent = (MetaEntity) metEntList.get(itr);
            if (ent.getParent() != null) {
                if (ent.getParent().getSystemName().equals(metEntId)) {
                    derivedEntities.add(ent.getSystemName());
                }
            }
        }
        return derivedEntities;
    }

    public static List<String> getNextCounterValue(String counterName, int bucketSize, String... keyParts) throws MetaException {
        SequenceInfo sequenceInfo = null;
        List<String> value = null;
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("SqGen[%s]", counterName));
        try {
            SequenceManager sequenceManager = SequenceManager.getInstance();
            sequenceInfo = sequenceManager.getSequenceInfo(counterName);
            value = sequenceManager.getNextValue(counterName, bucketSize, keyParts);
        } catch (Exception e) {
            throw new MetaException(e, "Error Occured while getting next counter value");
        } finally {
            timer.end();
        }
        return value;
    }

    public static Object getNextCounterValue(String counterName, String... keyParts) throws MetaException {
        SequenceInfo sequenceInfo = null;
        String value = null;
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("SqGen[%s]", counterName));
        try {
            SequenceManager sequenceManager = SequenceManager.getInstance();
            sequenceInfo = sequenceManager.getSequenceInfo(counterName);
            value = sequenceManager.getNextValue(counterName, keyParts);
        } catch (Exception e) {
            throw new MetaException(e, "Error Occured while getting next counter value");
        } finally {
            timer.end();
        }
        return value;

    }

    public static List<Long> getNextCounterValueAsLong(String counterName, int bucketSize) throws MetaException {
        SequenceInfo sequenceInfo = null;
        List<Long> value = null;
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("SqGenLong[%s]", counterName));
        try {
            SequenceManager sequenceManager = SequenceManager.getInstance();
            sequenceInfo = sequenceManager.getSequenceInfo(counterName);
            value = sequenceManager.getNextValueAsLong(counterName, bucketSize);
        } catch (Exception e) {
            throw new MetaException(e, "Error Occured while getting next counter value");
        } finally {
            timer.end();
        }
        return value;
    }

    public static Long getNextCounterValueAsLong(String counterName) throws MetaException {
        SequenceInfo sequenceInfo = null;
        Long value = null;
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("SqGenLong[%s]", counterName));
        try {
            SequenceManager sequenceManager = SequenceManager.getInstance();
            sequenceInfo = sequenceManager.getSequenceInfo(counterName);
            value = sequenceManager.getNextValueAsLong(counterName);
        } catch (Exception e) {
            throw new MetaException(e, "Error Occured while getting next counter value");
        } finally {
            timer.end();
        }
        return value;
    }

    public static List<DataObject> getDataObjects(MetaEntity selfEntity, List<Map<String, Object>> valueMap, String discCol) {
        return getDataObjects(selfEntity, valueMap, discCol, DataAccessMode.Detail);
    }

    public static List<DataObject> getDataObjects(MetaEntity selfEntity, List<Map<String, Object>> valueMap, String discCol, DataAccessMode accessMode) {
        List<DataObject> result = new ArrayList<DataObject>();
        DataObject object = null;

        for (int i = 0; i < valueMap.size(); i++) {
            object = null;
            String entityType = "";
            if (discCol != null) entityType = (String) ((Map) valueMap.get(i)).get(discCol);

            logger.logDebug(entityType);
            if (!StringHelper.isEmpty(entityType)) {
                MetaEntity entity = MetaDataRegistry.getMetaEntity(entityType);
                object = new DataObject(entity != null ? entity : selfEntity, (HashMap) valueMap.get(i), accessMode);
            } else {
                object = new DataObject(selfEntity, (HashMap) valueMap.get(i), accessMode);
            }

            result.add(object);
        }
        return result;
    }

    public static List<DataObject> getDataObjects(List<Map<String, Object>> valueMap) {

        List<DataObject> result = new ArrayList<DataObject>();
        DataObject object = null;
        for (int i = 0; i < valueMap.size(); i++) {
            object = new DataObject(valueMap.get(i));
            result.add(object);
        }
        return result;
    }

    public static MetaEntity getNonAbstractParentEntity(MetaEntity entity) {
        if (entity != null && (entity.getParent() == null || entity.getParent().isAbstract()))
            return entity;
        else {
            MetaEntity child;
            MetaEntity parEntity = null;
            while (true) {
                child = entity;
                if (entity != null) parEntity = entity.getParent();
                if (parEntity != null) {
                    child = parEntity;
                    parEntity = child.getParent();
                } else {
                    break;
                }
                //null checked to resolve an issue -- shoaib.rehman
                if (parEntity == null || parEntity.isAbstract())
                    return child;
                else
                    return parEntity;
            }

        }
        return null;
    }

    /**
     * This method adds the metaEntity Childs to the specified treeName of the valueTreeNode and caches it.
     *
     * @param treeName
     * @param metaEntityId
     * @return
     */
    public static ValueTreeNode getValueTreeFor(String treeName, String metaEntityId) {

        MetaEntity metaEntity = MetaDataRegistry.getMetaEntity(metaEntityId);
        ValueTreeNode valueTreeNode = ValueTreeManager.getTree(treeName);

        if (valueTreeNode == null) {
            throw new MetaException("The valueTreeNode cannot be null for TreeId: %1$s", treeName);
        }

        // TODO Kazim Add Extra Logic to handle the ValueTreeNodes other than
        // Form Classification.

        // Tree cache is stored here respective to each metaEntity.
        ValueTreeNode cachedTree = (ValueTreeNode) valueTreeNodeList.get(treeName + "_" + metaEntity.getPrimaryName());

        if (cachedTree == null) {

            // Need to re-merge the tree with entity tree subEntities.
            cachedTree = mergeValueTreeAndEntity((ValueTreeNode) valueTreeNode.clone());

            valueTreeNodeList.put(treeName + "_" + metaEntity.getPrimaryName(), cachedTree);
        }

        return cachedTree;
    }

    public static void clearValueTreeFor() {
        valueTreeNodeList.clear();
    }

    /**
     * Adding the metaEntity's to their respective valueTreeNodes.
     *
     * @param metaEntity
     * @param mergedTree
     * @return
     */

    private static ValueTreeNode mergeValueTreeAndEntity(MetaEntity metaEntity, ValueTreeNode mergedTree) {

        for (MetaEntity subEntity : metaEntity.getChildList()) {
            ValueTreeNode item = new ValueTreeNode(subEntity.getCategoryNode() + "." + subEntity.getPrimaryName(), "");
            item.setPrimaryName(subEntity.getPrimaryName());
            item.setSecondaryName(subEntity.getSecondaryName());
            item.setValue(subEntity.getId());
            mergedTree.add(item);

            if (!subEntity.getChildList().isEmpty()) {
                mergedTree = mergeValueTreeAndEntity(subEntity, mergedTree);
            }
        }

        return mergedTree;
    }

    private static ValueTreeNode mergeValueTreeAndEntity(ValueTreeNode mergedTree) {
        List<ValueTreeNode> childs = mergedTree.getChildList();
        ArrayList<ValueTreeNode> tempList = new ArrayList<ValueTreeNode>(0);
        for (ValueTreeNode node : childs) {
            if (node.getEntityCategorySet().size() > 0) {
                for (MetaEntityCategory cat : node.getEntityCategorySet()) {
                    MetaEntity subEntity = MetaDataRegistry.getMetaEntity(cat.getMetaEntityId());
                    ValueTreeNode item = new ValueTreeNode(subEntity.getCategoryNode() + "." + subEntity.getPrimaryName(),
                            "../../images/iconLeaf.gif");
                    item.setPrimaryName(subEntity.getPrimaryName());
                    item.setSecondaryName(subEntity.getSecondaryName());
                    item.setValue((node.getProductEntityId() != null ? node.getProductEntityId() : "N/A") + "|"
                            + (node.getOtherCode() != null ? node.getOtherCode() : "N/A") + "|" + subEntity.getId() + "|"
                            + (node.getParentId() != null ? node.getParentId() : "N/A") + "|" + (node.getId() != null ? node.getId() : "N/A"));
                    //shuwair
                    if (StringHelper.isNotEmpty(cat.getActionClass()))
                        item.setActionClass(cat.getActionClass());

                    tempList.add(item);
                }
                Collections.sort(tempList, new Comparator<ValueTreeNode>() {

                    public int compare(ValueTreeNode o1, ValueTreeNode o2) {
                        return o1.getPrimaryName().compareTo(o2.getPrimaryName());
                    }
                });
                for (ValueTreeNode tempNode : tempList) {
                    node.addChild(tempNode);
                }
                tempList.clear();

            } else {
                node = mergeValueTreeAndEntity(node);
            }
        }
        return mergedTree;
    }

    public static void reload() {
        //Shuwair
        try {
            readLock.lock();
            metEntList = new Hashtable<String, MetaEntity>();
            metEntSysList = new Hashtable<String, MetaEntity>();
            valueTreeNodeList = new Hashtable<String, ValueTreeNode>();

            HibernateDataBroker broker = (HibernateDataBroker) DataRepository.getBroker(MetaDataRegistry.class.getName());
            List<MetaEntity> list = broker.findAll(MetaEntity.class);
            for (int i = 0; i < list.size(); i++) {
                MetaEntity ent = (MetaEntity) list.get(i);
                String Id = ent.getId();
                addMetaEntity(Id, ent);
            }

            List<ValueTreeNode> ValueTreeNodeList = broker.findAll(ValueTreeNode.class);
            for (int i = 0; i < ValueTreeNodeList.size(); i++) {
                ValueTreeNode valueTreeNode = (ValueTreeNode) ValueTreeNodeList.get(i);
                String Id = valueTreeNode.getId();
                addValueTreeNodeList(Id, valueTreeNode);
            }
            isLoaded = true;

        } catch (Exception e) {
            throw new MetaException(e, "Error While Reloading Meta Data");
        } finally {
            //Shuwair
            readLock.unlock();
        }
    }
}
