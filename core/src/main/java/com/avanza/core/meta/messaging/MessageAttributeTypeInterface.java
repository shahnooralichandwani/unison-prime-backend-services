package com.avanza.core.meta.messaging;

public interface MessageAttributeTypeInterface {
    /**
     * @author nasir.nawab
     * Implementation classes of this interface will format Message Attribute value.
     */

    Object getDefaultValue();

}
