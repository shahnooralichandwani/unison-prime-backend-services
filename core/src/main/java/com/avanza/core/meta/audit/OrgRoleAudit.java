package com.avanza.core.meta.audit;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DbObject;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TreeObject;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;
import com.avanza.workflow.configuration.org.Role;

/**
 * Main Role Class
 *
 * @author Mehran.junejo
 */


public class OrgRoleAudit extends TreeObject implements Comparable {

    private static final long serialVersionUID = 4096383657368488865L;

    private OrgRoleAuditId orgRoleAuditId;
    private Short revtype;


    private String roleId;

    private Role parentRole;

    private String rolePrimaryName;

    private String roleSecondaryName;

    public OrgRoleAudit() {
        super();
        roleId = StringHelper.EMPTY;
    }


    public OrgRoleAudit(String id) {
        super(id);
        roleId = id;
        parentRole = new Role(StringHelper.EMPTY, true);
    }

    public OrgRoleAudit(String id, boolean flag) {
        if (flag)
            roleId = id;
        else
            roleId = null;
    }


    public OrgRoleAudit getParentRole() {
        return null;
    }

    public void setParentRole(Role parentRole) {
        this.parentRole = parentRole;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
        super.id = roleId;
    }

    public String getRolePrimaryName() {
        return rolePrimaryName;
    }

    public void setRolePrimaryName(String rolePrimaryName) {
        this.rolePrimaryName = rolePrimaryName;
    }

    public String getRoleSecondaryName() {
        return roleSecondaryName;
    }

    public void setRoleSecondaryName(String roleSecondaryName) {
        this.roleSecondaryName = roleSecondaryName;
    }

    public boolean isRoot() {
        return parentRole == null;
    }

    private String value;

    @Override
    public TreeObject clone() {
        Role orgRole = new Role(this.id);
        orgRole.copyValues(this);

        // Now need to copy their children also.
        for (TreeObject tobj : this.childList) {
            TreeObject cloned = ((Role) tobj).clone();
            ((OrgRoleAudit) cloned).parentRole = orgRole;
            // orgRole.childList.add(cloned);
        }
        return orgRole;
    }

    @Override
    public void copyValues(DbObject copyFrom) {

        if (!(copyFrom instanceof OrgRoleAudit)) return;

        OrgRoleAudit orgRole = (OrgRoleAudit) copyFrom;

        super.copyValues(copyFrom);
        this.rolePrimaryName = orgRole.rolePrimaryName;
        this.roleSecondaryName = orgRole.roleSecondaryName;
        this.parentRole = orgRole.parentRole;
    }

    @Override
    protected TreeObject createObject(String id) {
        // TODO Auto-generated method stub
        return new OrgRoleAudit(id);
    }

    @Override
    public <T extends TreeObject> void add(T item) {
        super.add(item);
        // ((OrgRoleAudit) item).parentRole = this;
    }

    @Override
    public boolean equals(Object obj) {
        OrgRoleAudit orgRole = (OrgRoleAudit) obj;

        if (orgRole.getId().equalsIgnoreCase(this.getId()) && orgRole.getKey().equalsIgnoreCase(this.getKey()))
            return true;

        return false;
    }

    @Override
    public int hashCode() {
        return roleId.hashCode();
    }

    @Override
    public String getPrimaryName() {
        // TODO Auto-generated method stub
        return rolePrimaryName;
    }

    public void setPrimaryName(String orgNamePrm) {
        this.rolePrimaryName = orgNamePrm;
    }

    @Override
    public String getSecondaryName() {
        // TODO Auto-generated method stub
        return roleSecondaryName;
    }

    public void setSecondaryName(String orgNameSec) {
        this.roleSecondaryName = orgNameSec;
    }

    @Override
    public String getValue() {
        // TODO Auto-generated method stub
        value = this.rolePrimaryName;
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public OrgRoleAuditId getOrgRoleAuditId() {
        return orgRoleAuditId;
    }


    public void setOrgRoleAuditId(OrgRoleAuditId orgRoleAuditId) {
        this.orgRoleAuditId = orgRoleAuditId;
    }


    public Short getRevtype() {
        return revtype;
    }


    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }


    @Override
    public int compareTo(Object arg0) {
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        LocaleInfo currentLocale = webContext.getAttribute(SessionKeyLookup.CurrentLocale);
        if (currentLocale.isPrimary())
            return getRolePrimaryName().compareTo(((OrgRoleAudit) arg0).getRolePrimaryName());
        else
            return ((getRoleSecondaryName() != null && ((OrgRoleAudit) arg0).getRoleSecondaryName() != null)
                    ? getRoleSecondaryName().compareTo(((OrgRoleAudit) arg0).getRoleSecondaryName())
                    : 0);

    }


}
