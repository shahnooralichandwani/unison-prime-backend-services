package com.avanza.core.meta.audit;

import java.io.Serializable;

import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.sdo.Attribute;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.StringHelper;

/**
 * @author Mehran Junejo
 */

public class MetaAttributeImplAudit extends MetaAttributeAudit implements Serializable {

    private static final long serialVersionUID = 1486375359920892832L;

//	@Override
//	public Attribute createAttribute() throws MetaException {
//		Attribute attrib = null;
//
//		String val = "";
//		String expression = this.getDefaultExp();
//
//		if (!StringHelper.isEmpty(expression)) {
//			val = (String) ExpressionHelper.getExpressionResult(expression,
//					String.class);
//		}
//
//		/*
//		 * Shahbaz: Bug: Inserting null in case of empty value
//		 * 
//		 */
//		if(StringHelper.isEmpty(val))
//			return attrib = new Attribute(this, null);
//		else
//			return attrib = new Attribute(this, ConvertUtil.parse(val, this
//				.getType().getJavaType()));
//	}

//	@Override
//	public Attribute createAttribute(Object value) throws MetaException {
//		Attribute attrib = null;
//
//		if (value != null) {
//			attrib = new Attribute(this, value);
//		} else {
//			attrib = new Attribute(this, ConvertUtil.parse(null, this.getType()
//					.getJavaType()));
//		}
//		return attrib;
//	}
}