package com.avanza.core.meta.audit;

import java.io.Serializable;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.data.expression.OperatorType;
import com.avanza.core.meta.AttributeScope;
import com.avanza.core.meta.AttributeType;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.Attribute;
import com.avanza.ui.meta.MetaViewAttribute;
import com.avanza.ui.meta.MetaViewMenu;

/**
 * @author Mehran Junejo
 */

public class MetaAttributeAudit extends DbObject implements Serializable, Comparable<MetaAttributeAudit> {


    private MetaAttributeAuditId metaAttributeAuditId;
    private Short revtype;

    private boolean isSearch;

    private boolean isPrimary;

    private boolean internalPrimary;

    private boolean isAudit;

    private boolean isSummary;

    private String displayName;

    private String tableColumn;

    private AttributeType type;

    private String operatorType;

    private String description;

    private String pickListId;

    private String counterName;

    private AttributeScope scope;

    private String constraintExp;

    private String defaultExp;

    private String calculateExp;

    private String metaEntityId;

    private String constrantType;

    private String id;

    private String systemName;

    private boolean isMandatory;

    private boolean templateEnabled;

    private boolean isSerialized;

    private String serializationType;

    /***added for Context Menu 2013-07-11 -NQ**/
    private String menuId;

    private Set<MetaViewAttribute> viewAttributes;

    public MetaAttributeAuditId getMetaAttributeAuditId() {
        return metaAttributeAuditId;
    }

    public void setMetaAttributeAuditId(MetaAttributeAuditId metaAttributeAuditId) {
        this.metaAttributeAuditId = metaAttributeAuditId;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public Set<MetaViewAttribute> getViewAttributes() {
        return viewAttributes;
    }

    public void setViewAttributes(Set<MetaViewAttribute> viewAttributes) {
        this.viewAttributes = viewAttributes;
    }


    /****/
    private MetaViewMenu viewMenu;

    public MetaViewMenu getViewMenu() {
        return viewMenu;
    }

    public void setViewMenu(MetaViewMenu viewMenu) {
        this.viewMenu = viewMenu;
    }

//	public abstract Attribute createAttribute();
//
//    public abstract Attribute createAttribute(Object Value);

    public MetaAttributeAudit() {

    }

    public MetaAttributeAudit(AttributeType type, String id, MetaEntity metaEnt) {

        this.setType(type);
        this.setId(id);
        this.setMetaEntityId(metaEnt.getId());
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /***added for Context Menu**/
    public String getMenuId() {
        return this.menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public AttributeScope getScope() {
        return this.scope;
    }

    public void setScope(AttributeScope scope) {
        this.scope = scope;
    }

    public AttributeType getType() {
        return this.type;
    }

    protected void setType(AttributeType type) {
        this.type = type;
    }

    public OperatorType getOpType() {
        return OperatorType.parseToken(this.operatorType);
    }

    public String getCalculateExp() {
        return this.calculateExp;
    }

    public void setCalculateExp(String calculateExp) {
        this.calculateExp = calculateExp;
    }

    public String getConstraintExp() {
        return this.constraintExp;
    }

    public void setConstraintExp(String constraintExp) {
        this.constraintExp = constraintExp;
    }

    public String getDefaultExp() {
        return this.defaultExp;
    }

    public void setDefaultExp(String defaultExp) {
        this.defaultExp = defaultExp;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSystemName() {
        return this.systemName;
    }

    public String getMetaEntityId() {
        return this.metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

    public String getTableColumn() {
        if (tableColumn == null)
            return "";
        return tableColumn;
    }

    public void setTableColumn(String tableColumn) {
        this.tableColumn = tableColumn;
    }

    public String getCounterName() {
        return this.counterName;
    }

    public void setCounterName(String counterName) {
        this.counterName = counterName;
    }

    public String getPickListId() {
        return this.pickListId;
    }

    public void setPickListId(String pickListId) {
        this.pickListId = pickListId;
    }

    public boolean getIsAudit() {
        return this.isAudit;
    }


    public boolean getIsMandatory() {
        return isMandatory;
    }


    public void setIsMandatory(boolean isMandatory) {
        this.isMandatory = isMandatory;
    }

    public void setIsAudit(boolean isAudit) {
        this.isAudit = isAudit;
    }

    public boolean getIsPrimary() {
        return this.isPrimary;
    }

    public void setIsPrimary(boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public boolean getIsSearch() {
        return this.isSearch;
    }

    public void setIsSearch(boolean isSearch) {
        this.isSearch = isSearch;
    }

    public boolean getIsSummary() {
        return this.isSummary;
    }

    public void setIsSummary(boolean isSummary) {
        this.isSummary = isSummary;
    }

    // Below methods are defined only for Hibernate Serialization to the database
    public String getAttributeType() {
        return this.type.toString();
    }

    public void setAttributeType(String value) {
        this.type = AttributeType.fromString(value);
    }

    public String getOperatorType() {
        return this.operatorType;
    }

    public void setOperatorType(String value) {
        this.operatorType = value;
    }

    public String getAttributeScope() {
        return this.scope.toString();
    }

    public void setAttributeScope(String value) {
        this.scope = AttributeScope.fromString(value);
    }

    // End Hibernate Serialization specific methods


    public String getConstrantType() {
        return constrantType;
    }

    public void setConstrantType(String constrantType) {
        this.constrantType = constrantType;
    }

    public String getFullAttributeId() {
        return this.getMetaEntityId() + "-" + this.getId();
    }

    public boolean isInternalPrimary() {
        return internalPrimary;
    }

    public void setInternalPrimary(boolean internalPrimary) {
        this.internalPrimary = internalPrimary;
    }

    public boolean isTemplateEnabled() {
        return templateEnabled;
    }

    public void setTemplateEnabled(boolean templateEnabled) {
        this.templateEnabled = templateEnabled;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public int compareTo(MetaAttributeAudit metaAttribute) {

        return this.displayName.compareTo(metaAttribute.getDisplayName());
    }

    public boolean getIsSerialized() {
        return isSerialized;
    }

    public void setIsSerialized(boolean isSerialized) {
        this.isSerialized = isSerialized;
    }

    public String getSerializationType() {
        return serializationType;
    }

    public void setSerializationType(String serializationType) {
        this.serializationType = serializationType;
    }

}