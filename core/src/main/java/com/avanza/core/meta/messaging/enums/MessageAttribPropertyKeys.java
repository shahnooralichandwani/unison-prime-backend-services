/**
 *
 */
package com.avanza.core.meta.messaging.enums;

/**
 * @author shahbaz.ali
 *
 */
public enum MessageAttribPropertyKeys {

    length, xmltagname, isParentTag, parentTagName
}
