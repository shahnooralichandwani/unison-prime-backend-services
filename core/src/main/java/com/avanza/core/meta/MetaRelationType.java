package com.avanza.core.meta;

/**
 * This Enum contains three types os relation ships which can exit
 * between Meta Entities in Unison:
 * <p>
 * Direct   - One to Many relationship.
 * Indirect - Many to Many relationship with no attributes on relation entity.
 * Complex  - Many to Many relationship with attributes on relation entity. Complex will
 * be broken into two relations i.e one Direct and One Complex.
 *
 * @author Haris Mirza
 */

public enum MetaRelationType {

    Direct, // one-to-many
    Indirect,  // many-to-one
    Complex; // many-to many implemented as many-to-one.
}
