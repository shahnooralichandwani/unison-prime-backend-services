package com.avanza.core.meta;

import com.avanza.core.data.DbObject;

/**
 * @author fkazmi
 */
public class MetaCounter extends DbObject {

    public static String VIEW_ID_COUNTER = "View_Id_Seq";

    public static String WORKFLOW_LOG_COUNTER = "Workflow_Log_Counter";

    public static String ACTION_LOG_COUNTER = "Action_Log_Counter";

    public static String STAN_ID_COUNTER = "STAN_Id_Seq";

    public static String ACTIVITY_SESSION_KEY_COUNTER = "Sec_Activity_Log";

    public static String NOTIFICATION_COUNTER = "Notification_Id";

    public static String VALIDATE_LOG_COUNTER = "Validate_Script_Log";

    public static String SESSION_HISTORY_COUNTER = "Session_History_Det";
    public static String SM_USER_ROLE_COUNTER = "SM_User_Role";

    public static String EMAIL_COUNTER = "Email_Id_Seq";

    public static String SENT_EMAIL_COUNTER = "EmailKeyGenerator";

    public static String TEMPLATE_COUNTER = "Template_Id_Seq";

    public static String ATTACHMENT_ID = "Attached_Doc_Id";

    public static String SCRIPT_COUNTER = "Script_Id_Seq";

    public static String ROLE_COUNTER = "Org_Role_Seq";

    public static String UNIT_COUNTER = "Org_Unit_Seq";

    public static String ACTIVITY_TYPE_COUNTER = "Activity_Type";

    public static String META_PICK_LIST = "Meta_Pick_list";

    public static String ALERT_COUNTER = "Alert_Counter";

    public static String VALUE_TREE_COUNTER = "Value_Tree_Node";

    public static String PRODUCT_COUNTER = "Product_Counter";

    public static String NOTIFICATION_CONFIG_COUNTER = "Notification_Config_Id";

    public static String DATA_SOURCE_COUNTER = "Data_Source_Key";

    public static String VALIDATION_SCRIPT_COUNTER = "Validation_Script_Id";

    public static String EDOCUMENT_COUNTER = "Edocument_id";

    public static String SEC_POLICY_COUNTER = "SecPolicy_Id";

    public static String COMPLAINT_DETAIL_ID = "Complaint_Detail_Id";
    public static String SESSION_HISTORY_AGENT_ID = "Session_History_Agent_Id";
    public static String ADVANCE_SEARCH_DETAIL_ID = "Advance_Search_Detail_Id";
    public static String DOCUMENT_ADVANCE_SEARCH_ID = "Document_Advance_Search_Id";

    public static String TASK_STRATEGIC_ACTION_CODE = "Task_Strategic_Action_Code";
    public static String TASK_ACTION_DETAIL_ID = "Task_Action_Detail_Id";
    public static String CAMPAIGN_CONTACT_LOG_ID = "Campaign_Contact_Log_Id";

    public static String AUDIT_TABLE_COUNTER = "AUDIT_TABLE_COUNTER";
    public static String UPLOAD_DOC_DETAIL_ID = "UPDLOAD_DOC_DETAIL_COUNTER";

    // Admin Maker Checker- Start
    public static String ADMIN_MODULE_SEQ = "Admin_Module_Seq";
    //Admin Maker Chekcer- End

    private String name;

    private long startValue;

    private long maxValue;

    private String prefix;

    private int incrementBy;

    private boolean isCyclic;


    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIncrementBy() {
        return this.incrementBy;
    }

    public void setIncrementBy(int incrementBy) {
        this.incrementBy = incrementBy;
    }

    public boolean getIsCyclic() {
        return this.isCyclic;
    }

    public void setIsCyclic(boolean isCyclic) {
        this.isCyclic = isCyclic;
    }

    public long getMaxValue() {
        return this.maxValue;
    }

    public void setMaxValue(long maxValue) {
        this.maxValue = maxValue;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public void setPrefix(String prefixValue) {
        this.prefix = prefixValue;
    }

    public long getStartValue() {
        return this.startValue;
    }

    public void setStartValue(long startValue) {
        this.startValue = startValue;
    }
}
