package com.avanza.core.meta;

import com.avanza.core.util.StringHelper;

import java.io.Serializable;

public class PicklistData implements Serializable, Comparable<PicklistData>, Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -7134698181846918076L;

    private String DisplayPrimary;
    private String DisplaySecondary;
    private String Value;
    private boolean selected;

    public PicklistData(String displayPRM, String displaySec, String value) {
        this.DisplayPrimary = displayPRM;
        this.DisplaySecondary = displaySec;
        this.Value = value;
    }

    public String getDisplayPrimary() {
        return DisplayPrimary;
    }

    public void setDisplayPrimary(String displayPrimary) {
        DisplayPrimary = displayPrimary;
    }

    public String getDisplaySecondary() {
        return DisplaySecondary;
    }

    public void setDisplaySecondary(String displaySecondary) {
        DisplaySecondary = displaySecondary;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public int compareTo(PicklistData o) {
        return DisplayPrimary.compareTo(o.DisplayPrimary);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        String displayPrimary = (StringHelper.isEmpty(DisplayPrimary)) ? StringHelper.EMPTY : new String(DisplayPrimary);
        String displaySecondary = (StringHelper.isEmpty(DisplaySecondary)) ? StringHelper.EMPTY : new String(DisplaySecondary);
        String value = (StringHelper.isEmpty(Value)) ? StringHelper.EMPTY : new String(Value);

        PicklistData temp = new PicklistData(displayPrimary, displaySecondary, value);

        return temp;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
