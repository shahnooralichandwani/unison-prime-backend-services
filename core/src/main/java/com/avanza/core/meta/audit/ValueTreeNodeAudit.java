package com.avanza.core.meta.audit;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.TreeObject;

/**
 * @author Mehran Junejo
 */

public class ValueTreeNodeAudit extends ValueTreeNodeBaseAudit {

    /**
     *
     */
    private static final long serialVersionUID = 5377413831546221589L;

    public ValueTreeNodeAudit() {
        super();
    }

    public ValueTreeNodeAudit(String id, String iconPath) {
        super(id, iconPath);
    }

    @Override
    public TreeObject clone() {

        ValueTreeNodeAudit valueTreeNode = new ValueTreeNodeAudit(this.id, this.iconPath);
        valueTreeNode.copyValues(this);

        // Now need to copy their children also.
        for (TreeObject tobj : this.childList) {
            TreeObject cloned = ((ValueTreeNodeAudit) tobj).clone();
            ((ValueTreeNodeAudit) cloned).parentNode = valueTreeNode;
            ((ValueTreeNodeAudit) cloned).parentId = valueTreeNode.id;
            valueTreeNode.childList.add(cloned);
        }

        return valueTreeNode;
    }

    @Override
    public void copyValues(DbObject copyFrom) {

        ValueTreeNodeAudit valueTreeNode = (ValueTreeNodeAudit) copyFrom;

        super.copyValues(copyFrom);
        this.primaryName = valueTreeNode.primaryName;
        this.secondaryName = valueTreeNode.secondaryName;
        this.hierarchyLevel = valueTreeNode.hierarchyLevel;
        this.parentId = valueTreeNode.parentId;
        this.productEntityId = valueTreeNode.productEntityId;
        this.otherCode = valueTreeNode.otherCode;
        this.parentNode = valueTreeNode.parentNode;
        this.entityCategorySet = valueTreeNode.entityCategorySet;
        this.iconPath = valueTreeNode.iconPath;
        this.systemName = valueTreeNode.systemName;
        this.displayOrder = valueTreeNode.displayOrder;
        this.rootNodeId = valueTreeNode.rootNodeId;
        this.actionClass = valueTreeNode.actionClass;
    }

    @Override
    public boolean equals(Object obj) {

        ValueTreeNodeAudit valueTreeNode = (ValueTreeNodeAudit) obj;

        if (valueTreeNode.getId().equalsIgnoreCase(this.getId())
                && valueTreeNode.getKey().equalsIgnoreCase(this.getKey()))
            return true;

        return false;
    }
}
