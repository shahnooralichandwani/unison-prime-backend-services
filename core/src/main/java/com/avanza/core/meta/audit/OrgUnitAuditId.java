package com.avanza.core.meta.audit;

/**
 * @author Mehran Junejo
 */

public class OrgUnitAuditId implements java.io.Serializable {

    // Fields

    private String orgUnitId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public OrgUnitAuditId() {
    }

    /**
     * full constructor
     */
    public OrgUnitAuditId(String orgUnitId, Integer rev) {
        this.orgUnitId = orgUnitId;
        this.rev = rev;
    }

    // Property accessors


    public Integer getRev() {
        return this.rev;
    }


    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public String getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(String orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof OrgUnitAuditId))
            return false;
        OrgUnitAuditId castOther = (OrgUnitAuditId) other;

        return ((this.getOrgUnitId() == castOther.getOrgUnitId()) || (this
                .getOrgUnitId() != null
                && castOther.getOrgUnitId() != null && this.getOrgUnitId().equals(
                castOther.getOrgUnitId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getOrgUnitId() == null ? 0 : this.getOrgUnitId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}