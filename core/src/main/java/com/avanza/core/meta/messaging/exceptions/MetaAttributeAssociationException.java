/**
 *
 */
package com.avanza.core.meta.messaging.exceptions;

/**
 * @author shahbaz.ali
 *
 */
public class MetaAttributeAssociationException extends Exception {

    String message;

    public MetaAttributeAssociationException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}
