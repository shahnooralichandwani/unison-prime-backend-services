package com.avanza.core.meta.messaging.enums;

public enum MessageAttributeTypeKeys {

    TRANSACTION_ID, DETAIL_MSG_INSTANCE_ID,
    RESPONSE_CODE, ENTITY_MAPPED,
    CONSTANT_VALUE, BLOCK_COUNT,
    WS_REPEAT_NODE, WS_ATTRIBUTE, WS_ELEMENT_NODE, WS_TEXT_NODE

}
