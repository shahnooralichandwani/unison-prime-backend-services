package com.avanza.core.meta;

import com.avanza.core.util.StringHelper;

public enum PicklistType {
    Static, Dynamic;

    public static PicklistType fromName(String type) {
        if (StringHelper.isEmpty(type))
            return PicklistType.Static;
        if (type.equalsIgnoreCase(PicklistType.Static.name()))
            return PicklistType.Static;
        if (type.equalsIgnoreCase(PicklistType.Dynamic.name()))
            return PicklistType.Dynamic;
        return null;
    }
}
