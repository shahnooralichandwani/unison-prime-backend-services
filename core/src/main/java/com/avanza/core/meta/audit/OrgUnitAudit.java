package com.avanza.core.meta.audit;

import java.util.HashSet;
import java.util.Set;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DbObject;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TreeObject;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;
import com.avanza.workflow.configuration.org.OrganizationalUnitType;

/**
 * This class represents any logical or physical group of an organization.
 *
 * @author Mehran.Junejo
 */
public class OrgUnitAudit extends TreeObject implements Comparable {

    private static final long serialVersionUID = 1075298696631837312L;

    private OrgUnitAuditId orgUnitAuditId;
    private Short revtype;

    private String orgUnitId;

    private OrganizationalUnitType orgUnitType;

    private OrgUnitAudit parentOrgUnit;

    private String orgNamePrm;

    private String orgNameSec;

    private String orgFullName;

    private long hierarchyLevel;

    private String otherCode1;

    private String otherCode2;

    private String otherCode3;

    private String orgAddress;

    private String orgPhone;

    private String orgFax;

    private String orgEmail;

    private String orgShortNamePrm;

    private String orgShortNameSec;

    private String value;

    private Set orgUnits = new HashSet(0);

    private Set orgUserRoles = new HashSet(0);

    // Constructors

    /**
     * default constructor
     */
    public OrgUnitAudit() {
        super();
        orgUnitId = StringHelper.EMPTY;
        orgUnitType = new OrganizationalUnitType();
        parentOrgUnit = new OrgUnitAudit(StringHelper.EMPTY, true);
    }

    public OrgUnitAudit(String id) {
        super(id);
        orgUnitId = id;
        orgUnitType = new OrganizationalUnitType();
        parentOrgUnit = new OrgUnitAudit(StringHelper.EMPTY, true);
    }

    public OrgUnitAudit(String id, boolean flag) {
        if (flag)
            orgUnitId = id;
        else
            orgUnitId = null;
    }

    /**
     * minimal constructor
     */
    public OrgUnitAudit(String orgUnitId, OrganizationalUnitType orgUnitType, OrgUnitAudit orgUnit, String orgNamePrm, long hierarchyLevel) {
        this.orgUnitId = orgUnitId;
        this.orgUnitType = orgUnitType;
        this.parentOrgUnit = orgUnit;
        this.orgNamePrm = orgNamePrm;
        this.hierarchyLevel = hierarchyLevel;
    }

    /**
     * full constructor
     */
    public OrgUnitAudit(String orgUnitId, OrganizationalUnitType orgUnitType, OrgUnitAudit orgUnit, String orgNamePrm, String orgNameSec,
                        String orgFullName, long hierarchyLevel, Set orgUnits, Set orgUserRoles) {
        this.orgUnitId = orgUnitId;
        this.orgUnitType = orgUnitType;
        this.parentOrgUnit = orgUnit;
        this.orgNamePrm = orgNamePrm;
        this.orgNameSec = orgNameSec;
        this.orgFullName = orgFullName;
        this.hierarchyLevel = hierarchyLevel;
        this.orgUnits = orgUnits;
        this.orgUserRoles = orgUserRoles;
    }

    // Property accessors
    public String getOrgUnitId() {
        return this.orgUnitId;
    }

    public void setOrgUnitId(String orgUnitId) {
        this.orgUnitId = orgUnitId;
        super.id = orgUnitId;
    }

    public OrganizationalUnitType getOrgUnitType() {
        return this.orgUnitType;
    }

    public void setOrgUnitType(OrganizationalUnitType orgUnitType) {
        this.orgUnitType = orgUnitType;
    }

    public OrgUnitAudit getParentOrgUnit() {
        return this.parentOrgUnit;
    }

    public void setParentOrgUnit(OrgUnitAudit orgUnit) {
        this.parentOrgUnit = orgUnit;
    }

    public String getOrgNamePrm() {
        return this.orgNamePrm;
    }

    public void setOrgNamePrm(String orgNamePrm) {
        this.orgNamePrm = orgNamePrm;
    }

    public String getOrgNameSec() {
        return this.orgNameSec;
    }

    public void setOrgNameSec(String orgNameSec) {
        this.orgNameSec = orgNameSec;
    }

    public String getOrgFullName() {
        return this.orgFullName;
    }

    public void setOrgFullName(String orgFullName) {
        this.orgFullName = orgFullName;
    }

    public long getHierarchyLevel() {
        return this.hierarchyLevel;
    }

    public void setHierarchyLevel(long hierarchyLevel) {
        this.hierarchyLevel = hierarchyLevel;
    }

    public Set getOrgUnits() {
        return this.orgUnits;
    }

    public void setOrgUnits(Set orgUnits) {
        this.orgUnits = orgUnits;
    }

    public Set getOrgUserRoles() {
        return this.orgUserRoles;
    }

    public void setOrgUserRoles(Set orgUserRoles) {
        this.orgUserRoles = orgUserRoles;
    }

    public String getOtherCode1() {
        return otherCode1;
    }

    public void setOtherCode1(String otherCode1) {
        this.otherCode1 = otherCode1;
    }

    public String getOtherCode2() {
        return otherCode2;
    }

    public void setOtherCode2(String otherCode2) {
        this.otherCode2 = otherCode2;
    }

    public String getOtherCode3() {
        return otherCode3;
    }

    public void setOtherCode3(String otherCode3) {
        this.otherCode3 = otherCode3;
    }

    public String getOrgAddress() {
        return orgAddress;
    }

    public void setOrgAddress(String orgAddress) {
        this.orgAddress = orgAddress;
    }

    public String getOrgPhone() {
        return orgPhone;
    }

    public void setOrgPhone(String orgPhone) {
        this.orgPhone = orgPhone;
    }

    public String getOrgFax() {
        return orgFax;
    }

    public void setOrgFax(String orgFax) {
        this.orgFax = orgFax;
    }

    public String getOrgEmail() {
        return orgEmail;
    }

    public void setOrgEmail(String orgEmail) {
        this.orgEmail = orgEmail;
    }

    public String getOrgShortNamePrm() {
        return orgShortNamePrm;
    }

    public void setOrgShortNamePrm(String orgShortNamePrm) {
        this.orgShortNamePrm = orgShortNamePrm;
    }

    public String getOrgShortNameSec() {
        return orgShortNameSec;
    }

    public void setOrgShortNameSec(String orgShortNameSec) {
        this.orgShortNameSec = orgShortNameSec;
    }

    @Override
    public String getPrimaryName() {
        return orgNamePrm;
    }

    public void setPrimaryName(String orgNamePrm) {
        this.orgNamePrm = orgNamePrm;
    }

    @Override
    public String getSecondaryName() {
        return orgNameSec;
    }

    public void setSecondaryName(String orgNameSec) {
        this.orgNameSec = orgNameSec;
    }

    @Override
    public TreeObject clone() {
        OrgUnitAudit orgUnit = new OrgUnitAudit(this.id);
        orgUnit.copyValues(this);

        // Now need to copy their children also.
        for (TreeObject tobj : this.childList) {
            TreeObject cloned = ((OrgUnitAudit) tobj).clone();
            ((OrgUnitAudit) cloned).parentOrgUnit = orgUnit;
            orgUnit.childList.add(cloned);
        }
        return orgUnit;
    }

    public OrgUnitAudit getParentNode() {
        return parentOrgUnit;
    }

    public void setParentNode(OrgUnitAudit parentOrgUnit) {
        this.parentOrgUnit = parentOrgUnit;
    }

    @Override
    public void copyValues(DbObject copyFrom) {

        if (!(copyFrom instanceof OrgUnitAudit)) return;

        OrgUnitAudit orgUnit = (OrgUnitAudit) copyFrom;

        super.copyValues(copyFrom);
        this.orgNamePrm = orgUnit.orgNamePrm;
        this.orgNameSec = orgUnit.orgNameSec;
        this.parentOrgUnit = orgUnit.parentOrgUnit;
    }

    @Override
    protected TreeObject createObject(String id) {

        return new OrgUnitAudit(id);
    }

    @Override
    public boolean equals(Object obj) {

        OrgUnitAudit orgUnit = (OrgUnitAudit) obj;

        if (orgUnit.getId().equalsIgnoreCase(this.getId()) && orgUnit.getKey().equalsIgnoreCase(this.getKey()))
            return true;

        return false;
    }

    @Override
    public <T extends TreeObject> void add(T item) {
        super.add(item);
        ((OrgUnitAudit) item).parentOrgUnit = this;
    }

    @Override
    public int hashCode() {
        return orgUnitId.hashCode();
    }

    @Override
    public String getValue() {
        value = this.orgNamePrm;
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isRoot() {
        return parentOrgUnit == null;
    }

    public OrgUnitAuditId getOrgUnitAuditId() {
        return orgUnitAuditId;
    }

    public void setOrgUnitAuditId(OrgUnitAuditId orgUnitAuditId) {
        this.orgUnitAuditId = orgUnitAuditId;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String toString() {
        return (StringHelper.isEmpty(orgUnitId)) ? StringHelper.EMPTY : orgUnitId;
    }

    public int compareTo(Object arg0) {

        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        LocaleInfo currentLocale = webContext.getAttribute(SessionKeyLookup.CurrentLocale);
        if (currentLocale.isPrimary())
            return getOrgNamePrm().compareTo(((OrgUnitAudit) arg0).getOrgNamePrm());
        else
            return ((getOrgNameSec() != null && ((OrgUnitAudit) arg0).getOrgNameSec() != null)
                    ? getOrgNameSec().compareTo(((OrgUnitAudit) arg0).getOrgNameSec())
                    : 0);
    }
}
