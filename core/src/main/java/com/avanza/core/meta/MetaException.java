package com.avanza.core.meta;

import com.avanza.core.CoreException;

public class MetaException extends CoreException {

    private static final long serialVersionUID = -295037078797890193L;

    public MetaException(String message) {

        super(message);
    }

    public MetaException(String format, Object... args) {

        super(format, args);
    }

    public MetaException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public MetaException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public MetaException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public MetaException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}
