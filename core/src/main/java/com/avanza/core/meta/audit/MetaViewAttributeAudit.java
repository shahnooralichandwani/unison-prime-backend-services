package com.avanza.core.meta.audit;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.meta.MetaAttributeImpl;
import com.avanza.ui.meta.MetaViewAttributeParam;

public class MetaViewAttributeAudit extends DbObject implements Serializable, Comparable<MetaViewAttributeAudit> {

    /**
     * @author Mehran Junejo
     */

    private static final long serialVersionUID = 1L;

    private MetaViewAttributeAuditId metaViewAttribAuditId;
    private Short revtype;

    private String systemName;

    private String viewId;

    private String metaAttributeId;

    private MetaAttributeImpl metaAttribute;

    private String componentType;

    private String metaEntityId;

    private String constraintExp;

    private int displayOrder;

    private String styleClass;

    private String displayNamePimary;

    private String displayNameSecondary;

    private boolean displayForeignValue;

    private String foreignDisplayAttributeId;

    private String formatExpression;

    private Set<MetaViewAttributeParam> viewAttribParams = new HashSet<MetaViewAttributeParam>(0);

    private boolean readOnly;

    private boolean isPostback;

    private String postbackExpression;

    private boolean aggregated;

    private String linkURL;

    private String linkParams;

    private boolean renderAsPopup;

    private String clientScriptExp;

    private String factoryClass;

    private String editExpression;

    private String selectExpression;

    private String cascadedAttribId;

    private String cascadedColumn;

    private String events;
    private String eventActionClasses;
    private String eventRerenderComponents;

    public boolean getRenderAsPopup() {
        return renderAsPopup;
    }

    public void setRenderAsPopup(boolean renderAsPopup) {
        this.renderAsPopup = renderAsPopup;
    }

    public MetaViewAttributeAudit() {

    }

    public MetaViewAttributeAudit(MetaViewAttributeAudit attrib) {
        this.componentType = attrib.componentType;
        this.constraintExp = attrib.constraintExp;
        this.displayForeignValue = attrib.displayForeignValue;
        this.displayNamePimary = attrib.displayNamePimary;
        this.displayNameSecondary = attrib.displayNameSecondary;
        this.displayOrder = attrib.displayOrder;
        this.metaEntityId = attrib.metaEntityId;
        this.foreignDisplayAttributeId = attrib.foreignDisplayAttributeId;
        this.formatExpression = attrib.formatExpression;
        this.metaAttributeId = attrib.metaAttributeId;
        this.readOnly = attrib.readOnly;
        this.viewId = attrib.viewId;
        this.viewAttribParams = attrib.viewAttribParams;
        this.styleClass = attrib.styleClass;
        this.renderAsPopup = attrib.renderAsPopup;
        this.factoryClass = attrib.factoryClass;
    }

    public String getDisplayNamePimary() {
        return displayNamePimary;
    }

    public void setDisplayNamePimary(String displayNamePimary) {
        this.displayNamePimary = displayNamePimary;
    }

    public String getDisplayNameSecondary() {
        return displayNameSecondary;
    }

    public void setDisplayNameSecondary(String displayNameSecondary) {
        this.displayNameSecondary = displayNameSecondary;
    }

    public String getDocumentId() {
        return metaEntityId;
    }

    public void setDocumentId(String documentId) {
        this.metaEntityId = documentId;
    }

    public String getFormatExpression() {
        return formatExpression;
    }

    public void setFormatExpression(String formatExpression) {
        this.formatExpression = formatExpression;
    }


    public String getMetaAttributeId() {
        return metaAttributeId;
    }

    public void setMetaAttributeId(String metaAttributeId) {
        this.metaAttributeId = metaAttributeId;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }

    public Set<MetaViewAttributeParam> getViewAttribParams() {
        return viewAttribParams;
    }

    public void setViewAttribParams(Set<MetaViewAttributeParam> viewAttribParams) {
        this.viewAttribParams = viewAttribParams;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public String getComponentType() {
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    public String getFullMetaAttributeId() {
        return this.getDocumentId() + "-" + this.getMetaAttributeId();
    }

    public boolean isDisplayForeignValue() {
        return displayForeignValue;
    }

    public void setDisplayForeignValue(boolean displayForeignValue) {
        this.displayForeignValue = displayForeignValue;
    }

    public String getForeignDisplayAttributeId() {
        return foreignDisplayAttributeId;
    }

    public void setForeignDisplayAttributeId(String foreignDisplayAttributeId) {
        this.foreignDisplayAttributeId = foreignDisplayAttributeId;
    }

    public String getConstraintExp() {
        return constraintExp;
    }

    public void setConstraintExp(String constraintExp) {
        this.constraintExp = constraintExp;
    }

    public MetaViewAttributeAuditId getMetaViewAttribAuditId() {
        return metaViewAttribAuditId;
    }

    public void setMetaViewAttribAuditId(MetaViewAttributeAuditId metaViewAttribAuditId) {
        this.metaViewAttribAuditId = metaViewAttribAuditId;
    }

    public Short getRevtype() {
        return revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public boolean getIsPostback() {
        return isPostback;
    }

    public void setIsPostback(boolean isPostback) {
        this.isPostback = isPostback;
    }

    public String getPostbackExpression() {
        return postbackExpression;
    }

    public void setPostbackExpression(String postbackExpression) {
        this.postbackExpression = postbackExpression;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

    public boolean getAggregated() {
        return aggregated;
    }

    public void setAggregated(boolean aggregated) {
        this.aggregated = aggregated;
    }

    public void setPostback(boolean isPostback) {
        this.isPostback = isPostback;
    }

    public String getLinkURL() {
        return linkURL;
    }

    public void setLinkURL(String linkURL) {
        this.linkURL = linkURL;
    }

    public String getLinkParams() {
        return linkParams;
    }

    public void setLinkParams(String linkParams) {
        this.linkParams = linkParams;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public MetaAttributeImpl getMetaAttribute() {
        return metaAttribute;
    }

    public String getMetaAttribSystemName() {
        return metaAttribute.getSystemName();
    }

    public void setMetaAttribute(MetaAttributeImpl metaAttribute) {
        this.metaAttribute = metaAttribute;
    }

    public String getClientScriptExp() {
        return clientScriptExp;
    }

    public void setClientScriptExp(String clientScriptExp) {
        this.clientScriptExp = clientScriptExp;
    }

    public String getFactoryClass() {
        return factoryClass;
    }

    public void setFactoryClass(String factoryClass) {
        this.factoryClass = factoryClass;
    }

    public String getEditExpression() {
        return editExpression;
    }

    public void setEditExpression(String editableExpression) {
        this.editExpression = editableExpression;
    }

    public String getSelectExpression() {
        return selectExpression;
    }

    public void setSelectExpression(String selectExpression) {
        this.selectExpression = selectExpression;
    }

    public String getCascadedAttribId() {
        return cascadedAttribId;
    }

    public void setCascadedAttribId(String cascadedAttribId) {
        this.cascadedAttribId = cascadedAttribId;
    }

    public String getCascadedColumn() {
        return cascadedColumn;
    }

    public void setCascadedColumn(String cascadedColumn) {
        this.cascadedColumn = cascadedColumn;
    }

    public String getEvents() {
        return events;
    }

    public void setEvents(String events) {
        this.events = events;
    }

    public String getEventActionClasses() {
        return eventActionClasses;
    }

    public void setEventActionClasses(String eventActionClasses) {
        this.eventActionClasses = eventActionClasses;
    }

    public String getEventRerenderComponents() {
        return eventRerenderComponents;
    }

    public void setEventRerenderComponents(String eventRerenderComponents) {
        this.eventRerenderComponents = eventRerenderComponents;
    }

    @Override
    public int compareTo(MetaViewAttributeAudit o) {
        if (displayOrder > o.getDisplayOrder())
            return 1;
        else if (displayOrder < o.getDisplayOrder())
            return -1;
        return 0;
    }


}
