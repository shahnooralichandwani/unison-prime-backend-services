package com.avanza.core.meta.audit;

/**
 * @author Mehran Junejo
 */

public class MetaViewAttributeAuditId implements java.io.Serializable {

    // Fields

    private String metaViewAttributeAuditId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public MetaViewAttributeAuditId() {
    }

    /**
     * full constructor
     */
    public MetaViewAttributeAuditId(String roleId, Integer rev) {
        this.metaViewAttributeAuditId = roleId;
        this.rev = rev;
    }

    // Property accessors


    public String getMetaViewAttributeAuditId() {
        return metaViewAttributeAuditId;
    }

    public void setMetaViewAttributeAuditId(String metaViewAttributeAuditId) {
        this.metaViewAttributeAuditId = metaViewAttributeAuditId;
    }

    public Integer getRev() {
        return this.rev;
    }


    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof MetaViewAttributeAuditId))
            return false;
        MetaViewAttributeAuditId castOther = (MetaViewAttributeAuditId) other;

        return ((this.getMetaViewAttributeAuditId() == castOther.getMetaViewAttributeAuditId()) || (this
                .getMetaViewAttributeAuditId() != null
                && castOther.getMetaViewAttributeAuditId() != null && this.getMetaViewAttributeAuditId().equals(
                castOther.getMetaViewAttributeAuditId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getMetaViewAttributeAuditId() == null ? 0 : this.getMetaViewAttributeAuditId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}