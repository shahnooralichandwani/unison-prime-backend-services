/**
 *
 */
package com.avanza.core.meta.messaging;

import java.util.HashMap;
import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.meta.MetaException;
import com.avanza.core.meta.messaging.enums.MessageAttributeTypeKeys;

/**
 * @author shahbaz.ali
 *
 */
public class MetaMessageCatalog {

    private static HashMap<MessageKey, Message> messagesList = new HashMap<MessageKey, Message>();

    public static void load() {

        try {
            DataBroker broker = DataRepository.getBroker(MetaMessageCatalog.class.getName());
            List<Message> msgList = broker.findAll(Message.class);
            for (Message msg : msgList) {
                addMessages(msg);

            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new MetaException(e,
                    "Error While Loading Meta Message Catalog");
        }

    }

    private static void addMessages(Message msg) {

        String action = msg.getAction();
        String metaEntId = msg.getMetaEntity().getSystemName();
        MessageKey key = new MessageKey(metaEntId, action);
        messagesList.put(key, msg);

    }

    public static Message getMessage(String metaEntId, MessageAction listrequest) {
        MessageKey key = new MessageKey(metaEntId, listrequest.toString());
        return messagesList.get(key);
    }

    public static MessageAttribute getTransactionIdAttribute(String metaEntId, MessageAction listrequest) {
        MessageKey key = new MessageKey(metaEntId, listrequest.toString());
        Message message = messagesList.get(key);

        for (MessageAttribute messageAttribute : message.getSortedAttributes()) {
            if (messageAttribute.getMessageAttributeType() != null && messageAttribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.TRANSACTION_ID.toString())) {
                return messageAttribute;
            }
        }


        return null;
    }

    private static class MessageKey {

        String metaEntId;
        String action;

        public MessageKey(String id, String action) {
            this.metaEntId = id;
            this.action = action;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof MessageKey) {
                MessageKey messageKey = (MessageKey) obj;
                return (messageKey.metaEntId.equalsIgnoreCase(metaEntId) && messageKey.action.equalsIgnoreCase(action));
            } else
                return false;
        }

        @Override
        public int hashCode() {
            return metaEntId.hashCode() + action.hashCode();
        }


    }


}
