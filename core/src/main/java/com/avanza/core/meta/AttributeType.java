package com.avanza.core.meta;

import com.avanza.core.util.Convert;

import java.util.Date;

public enum AttributeType {

    None(0), Boolean(1), Integer(2), Long(3), Float(4), Double(5), Date(6), DateTime(7), String(8), LongString(9), PickList(9), Sequence(10), ValueNode(
            11), Object(12), Attachment(13);

    private int intValue;

    private AttributeType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static AttributeType fromString(String value) {

        AttributeType retVal = null;

        if (value.equalsIgnoreCase("None"))
            retVal = AttributeType.None;
        else if (value.equalsIgnoreCase("String"))
            retVal = AttributeType.String;
        else if (value.equalsIgnoreCase("Integer"))
            retVal = AttributeType.Integer;
        else if (value.equalsIgnoreCase("Date"))
            retVal = AttributeType.Date;
        else if (value.equalsIgnoreCase("DateTime"))
            retVal = AttributeType.DateTime;
        else if (value.equalsIgnoreCase("Double"))
            retVal = AttributeType.Double;
        else if (value.equalsIgnoreCase("Float"))
            retVal = AttributeType.Float;
        else if (value.equalsIgnoreCase("Long"))
            retVal = AttributeType.Long;
        else if (value.equalsIgnoreCase("LongString"))
            retVal = AttributeType.LongString;
        else if (value.equalsIgnoreCase("Object"))
            retVal = AttributeType.Object;
        else if (value.equalsIgnoreCase("PickList"))
            retVal = AttributeType.PickList;
        else if (value.equalsIgnoreCase("Sequence"))
            retVal = AttributeType.Sequence;
        else if (value.equalsIgnoreCase("ValueNode"))
            retVal = AttributeType.ValueNode;
        else if (value.equalsIgnoreCase("Boolean"))
            retVal = AttributeType.Boolean;
        else if (value.equalsIgnoreCase("Attachment"))
            retVal = AttributeType.Attachment;
        else
            throw new MetaException("[%1$s] is not recognized as valid Attribute Type", value);

        return retVal;
    }

    public boolean isSimple() {

        return (this == AttributeType.Boolean) || (this == AttributeType.Integer) || (this == AttributeType.Long) || (this == AttributeType.Float)
                || (this == AttributeType.Double) || (this == AttributeType.Date) || (this == AttributeType.DateTime)
                || (this == AttributeType.String) || (this == AttributeType.LongString);
    }

    public boolean isComplex() {

        return !this.isSimple();
    }

    public Class<?> getJavaType() {

        Class<?> retVal = null;

        switch (this) {

            case Boolean:
                retVal = boolean.class;
                break;

            case Integer:
                retVal = int.class;
                break;

            case Long:
                retVal = long.class;
                break;

            case Float:
                retVal = float.class;
                break;

            case Double:
                retVal = double.class;
                break;

            case Date:
                retVal = Date.class;
                break;

            case DateTime:
                retVal = Date.class;
                break;

            case String:
                retVal = String.class;
                break;

            case Sequence:
                retVal = String.class;
                break;

            case PickList:
                retVal = String.class;
                break;

            case LongString:
                retVal = String.class;
                break;
            case Attachment:
                //retVal = UploadedFile.class;
                break;
            default:
                throw new MetaException("Attribute type [%1$s] don't have equivalent Java data type", this.toString());
        }

        return retVal;
    }

    public Object cast(Object object) {

        Object retVal = null;
        switch (this) {

            case Boolean:
                retVal = Convert.toBoolean(object);
                break;

            case Integer:
                retVal = Convert.toInteger(object);
                break;

            case Long:
                retVal = Convert.toLong(object);
                break;

            case Float:
                retVal = Convert.toFloat(object);
                break;

            case Double:
                retVal = Convert.toDouble(object);
                break;

            case Date:
                retVal = Convert.toDate(object);
                break;

            case DateTime:
                retVal = Convert.toDate(object);
                break;

            case String:
                retVal = (String) object;
                break;


            default:
                throw new MetaException("Attribute type [%1$s] don't have equivalent Java data type", this.toString());
        }

        return retVal;
    }
}

// TO DO: can add further (complex) types like Decimal, Price,
// ArrayedAttribute,
// Email Address, URI etc.
