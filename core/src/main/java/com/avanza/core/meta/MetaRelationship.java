package com.avanza.core.meta;

import com.avanza.core.data.DbObject;

import java.io.Serializable;

/**
 * @author fkazmi
 */
public class MetaRelationship extends DbObject implements Serializable {

    private static final long serialVersionUID = 8868408445645973948L;

    private String relationshipId;
    private String systemName;
    private String mainEntityId;
    private String subEntityId;
    private String mainEntityKey;
    private String relMainEntityKey;
    private String relSubEntityKey;
    private String subEntityKey;
    private String relatedEntityId;
    private String relationType;
    /*Added by Nida for getting related Objects irrespective of state*/
    private boolean alwaysExist;

    public boolean isAlwaysExist() {
        return alwaysExist;
    }

    public void setAlwaysExist(boolean alwaysExist) {
        this.alwaysExist = alwaysExist;
    }

    public String getMainEntityId() {
        return mainEntityId;
    }

    public void setMainEntityId(String mainEntity) {
        this.mainEntityId = mainEntity;
    }

    public String getSubEntityId() {
        return subEntityId;
    }

    public void setSubEntityId(String subEntity) {
        this.subEntityId = subEntity;
    }

    public String getMainEntityKey() {
        return mainEntityKey;
    }

    public void setMainEntityKey(String mainEntityKey) {
        this.mainEntityKey = mainEntityKey;
    }

    public String getSubEntityKey() {
        return subEntityKey;
    }

    public void setSubEntityKey(String subEntityKey) {
        this.subEntityKey = subEntityKey;
    }

    public String getRelatedEntityId() {
        return relatedEntityId;
    }

    public void setRelatedEntityId(String relatedEntityId) {
        this.relatedEntityId = relatedEntityId;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    public String getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(String relationshipId) {
        this.relationshipId = relationshipId;
    }

    public String getRelMainEntityKey() {
        return relMainEntityKey;
    }

    public void setRelMainEntityKey(String relMainEntityKey) {
        this.relMainEntityKey = relMainEntityKey;
    }

    public String getRelSubEntityKey() {
        return relSubEntityKey;
    }

    public void setRelSubEntityKey(String relSubEntityKey) {
        this.relSubEntityKey = relSubEntityKey;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }
}
