package com.avanza.core.meta;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DbObject;
import com.avanza.core.interceptor.Auditable;
import com.avanza.core.meta.audit.MetaPicklistAudit;
import com.avanza.core.meta.audit.MetaPicklistAuditId;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;
import org.apache.commons.beanutils.BeanUtils;

import java.util.Random;
import java.util.Set;

/**
 * @author fkazmi
 */
public class MetaPicklist extends DbObject implements Comparable<MetaPicklist>, Auditable {

    /**
     *
     */
    private static final long serialVersionUID = 4664357203630385579L;

    private String Id;

    private String systemName;

    private String parentId;

    private String PicklistName;// used as an alias to the query.

    private String Type;

    private String TableName = "VALUE_TREE_NODE";

    private String LinkColumn;

    private String LinkColumnType;

    private String DisplayColPri;

    private String DisplayColSec;

    private String SelectCritera;

    private MetaPicklist parent;

    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    private String DisplayName;

    private boolean isSystem;
    private boolean isExternal;
    private boolean isKeyRequired;
    private String keyColumn;
    private String keyCounterName;

    private String displayAttrPrm;
    private String displayAttrSec;
    private String attributeKey;

    private Set<MetaPicklist> childPicList;

    public String getSelectCritera() {
        return SelectCritera;
    }

    public void setSelectCritera(String selectCritera) {
        SelectCritera = selectCritera;
    }

    public String getDisplayColPri() {
        return DisplayColPri;
    }

    public void setDisplayColPri(String displayColPri) {
        DisplayColPri = displayColPri;
    }

    public String getDisplayColSec() {
        return DisplayColSec;
    }

    public void setDisplayColSec(String displayColSec) {
        DisplayColSec = displayColSec;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }

    public String getLinkColumn() {
        return LinkColumn;
    }

    public void setLinkColumn(String linkColumn) {
        LinkColumn = linkColumn;
    }

    public String getPicklistName() {
        return PicklistName;
    }

    public void setPicklistName(String picklistName) {
        PicklistName = picklistName;
    }

    public String getTableName() {
        return TableName;
    }

    public void setTableName(String tableName) {
        TableName = tableName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public MetaPicklist getParent() {
        return parent;
    }

    public void setParent(MetaPicklist parent) {
        this.parent = parent;
    }

    public Set<MetaPicklist> getChildPicList() {
        return childPicList;
    }

    public void setChildPicList(Set<MetaPicklist> childPicList) {
        this.childPicList = childPicList;
    }

    public String getLinkColumnType() {
        return LinkColumnType;
    }

    public void setLinkColumnType(String linkColumnType) {
        LinkColumnType = linkColumnType;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public boolean isSystem() {
        return isSystem;
    }

    public boolean getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(boolean isSystem) {
        this.isSystem = isSystem;
    }

    public boolean isExternal() {
        return isExternal;
    }

    public boolean getIsExternal() {
        return isExternal;
    }

    public void setIsExternal(boolean isExternal) {
        this.isExternal = isExternal;
    }

    public boolean isKeyRequired() {
        return isKeyRequired;
    }

    public boolean getIsKeyRequired() {
        return isKeyRequired;
    }

    public void setIsKeyRequired(boolean isKeyRequired) {
        this.isKeyRequired = isKeyRequired;
    }

    public String getKeyColumn() {
        return keyColumn;
    }

    public void setKeyColumn(String keyColumn) {
        this.keyColumn = keyColumn;
    }

    public String getKeyCounterName() {
        return keyCounterName;
    }

    public void setKeyCounterName(String keyCounterName) {
        this.keyCounterName = keyCounterName;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public int compareTo(MetaPicklist arg0) {
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class.getName());
        boolean isPrimaryLocale = ((LocaleInfo) webContext.getAttribute(SessionKeyLookup.CurrentLocale)).isPrimary();

        return (isPrimaryLocale) ? this.getDisplayColPri().compareToIgnoreCase(((MetaPicklist) arg0).getDisplayColPri()) :
                this.getDisplayColSec().compareToIgnoreCase(((MetaPicklist) arg0).getDisplayColSec());
    }

    public String getDisplayAttrPrm() {
        return displayAttrPrm;
    }

    public void setDisplayAttrPrm(String displayAttrPrm) {
        this.displayAttrPrm = displayAttrPrm;
    }

    public String getDisplayAttrSec() {
        return displayAttrSec;
    }

    public void setDisplayAttrSec(String displayAttrSec) {
        this.displayAttrSec = displayAttrSec;
    }

    public String getAttributeKey() {
        return attributeKey;
    }

    public void setAttributeKey(String attributeKey) {
        this.attributeKey = attributeKey;
    }


    @Override
    public Object getAuditEntry(Short revtype) {
        MetaPicklistAudit metaPicklistAudit = null;
        try {

            Random rand = new Random();

            metaPicklistAudit = new MetaPicklistAudit();
            metaPicklistAudit.setRevtype(revtype);

            BeanUtils.copyProperties(metaPicklistAudit, this);
            metaPicklistAudit.setMetaPicklistAuditId(new MetaPicklistAuditId(this.Id, rand.nextInt()));


        } catch (Exception e) {
            metaPicklistAudit = null;
        }
        return metaPicklistAudit;
    }

}
