package com.avanza.core.meta.messaging;

import com.avanza.core.meta.MetaAttributeImpl;

/**
 * MessageAttributeId entity. @author MyEclipse Persistence Tools
 */

public class MessageAttributeId implements java.io.Serializable {

    // Fields

    private Message message;
    private MetaAttributeImpl metaAttributeImpl;

    // Constructors

    /**
     * default constructor
     */
    public MessageAttributeId() {
    }

    /**
     * full constructor
     */
    public MessageAttributeId(Message message, MetaAttributeImpl metaAttributeImpl) {
        this.message = message;
        this.metaAttributeImpl = metaAttributeImpl;
    }

    // Property accessors

    public Message getMessage() {
        return this.message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public MetaAttributeImpl getMetaAttributeImpl() {
        return this.metaAttributeImpl;
    }

    public void setMetaAttributeImpl(MetaAttributeImpl metaAttributeImpl) {
        this.metaAttributeImpl = metaAttributeImpl;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof MessageAttributeId))
            return false;
        MessageAttributeId castOther = (MessageAttributeId) other;

        return ((this.getMessage() == castOther.getMessage()) || (this
                .getMessage() != null && castOther.getMessage() != null && this
                .getMessage().equals(castOther.getMessage()))) && ((this
                .getMetaAttributeImpl() == castOther.getMetaAttributeImpl()) || (this
                .getMetaAttributeImpl() != null && castOther
                .getMetaAttributeImpl() != null && this.getMetaAttributeImpl()
                .equals(castOther.getMetaAttributeImpl())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (getMessage() == null ? 0 : this.getMessage()
                .hashCode());
        result = 37 * result + (getMetaAttributeImpl() == null ? 0 : this
                .getMetaAttributeImpl().hashCode());
        return result;
    }

}