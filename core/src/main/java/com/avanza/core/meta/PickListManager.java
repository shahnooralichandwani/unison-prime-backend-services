package com.avanza.core.meta;

import com.avanza.core.data.*;
import com.avanza.core.data.expression.Search;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.JdbcDataHelper;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.integration.dbmapper.DbIntegerationDataBroker;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class PickListManager {

    private static final Logger logger = Logger.getLogger(PickListManager.class);

    public static HashMap<String, List<PicklistData>> cachedPicklistData = new HashMap<String, List<PicklistData>>(0);

    private static Hashtable<String, MetaPicklist> metPicklistList = new Hashtable<String, MetaPicklist>();

    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock readLock = readWriteLock.readLock();
    private static Lock writeLock = readWriteLock.writeLock();

    public static void load() {
        try {
            writeLock.lock();
            metPicklistList = new Hashtable<String, MetaPicklist>();
            cachedPicklistData = new HashMap<String, List<PicklistData>>(0);
            HibernateDataBroker broker = (HibernateDataBroker) DataRepository
                    .getBroker(PickListManager.class.getName());

            List<MetaPicklist> picklistList = broker.findAll(MetaPicklist.class);
            for (int i = 0; i < picklistList.size(); i++) {
                MetaPicklist picklist = (MetaPicklist) picklistList.get(i);
                String Id = new String(picklist.getId());
                metPicklistList.put(Id, picklist);
            }

            for (MetaPicklist picklist : metPicklistList.values()) {
                if (PicklistType.fromName(picklist.getType()) == PicklistType.Static) {
                    List<PicklistData> data = getPickListData(null, picklist);
                    cachedPicklistData.put(picklist.getId(), data);
                }
            }
            cachedPicklistData.remove(null);
        } finally {
            writeLock.unlock();
        }
    }

    public static List<MetaPicklist> getMetaPicklistList() {

        List<MetaPicklist> list = new ArrayList<MetaPicklist>();
        try {
            readLock.lock();
            list = new ArrayList<MetaPicklist>(metPicklistList.values());
            Collections.sort(list, new Comparator<MetaPicklist>() {
                public int compare(MetaPicklist o1, MetaPicklist o2) {
                    return o1.getPicklistName().compareToIgnoreCase(o2.getPicklistName());
                }
            });
        } finally {
            readLock.unlock();
        }
        return list;
    }

    public static MetaPicklist getMetaPickList(String name) {
        MetaPicklist list = new MetaPicklist();
        try {
            readLock.lock();
            list = metPicklistList.get(name);
        } finally {
            readLock.unlock();
        }
        return list;
    }

    public static MetaPicklist getMetaPickListBySystemName(String name) {
        try {
            readLock.lock();
            for (MetaPicklist picklist : metPicklistList.values()) {
                if (picklist.getSystemName().equalsIgnoreCase(name))
                    return picklist;
            }
            return null;
        } finally {
            readLock.unlock();
        }
    }

    public static void addPickListData(String picklistId, List<PicklistData> newPicklistData) {
        try {
            writeLock.lock();
            List<PicklistData> data = cachedPicklistData.get(picklistId);
            if (data == null)
                data = new ArrayList<PicklistData>(0);
            data.addAll(newPicklistData);
            cachedPicklistData.put(picklistId, data);
        } finally {
            writeLock.unlock();
        }

    }

    public static List<PicklistData> getPickListData(String pickListId) {
        try {
            readLock.lock();
            if (cachedPicklistData.containsKey(pickListId)) {
                return cachedPicklistData.get(pickListId);
            }
            return getPickListData(null, getMetaPickList(pickListId));
        } finally {
            readLock.unlock();
        }
    }

    public static List<PicklistData> getPickListData(MetaPicklist pickList) {
        try {
            readLock.lock();
            if (cachedPicklistData.containsKey(pickList.getId())) {
                return cachedPicklistData.get(pickList.getId());
            }
            return getPickListData(null, pickList);
        } finally {
            readLock.unlock();
        }
    }

    public static List<PicklistData> getPickListDataFromSelectedValue(String selectedValue, MetaPicklist pickList) {
        try {
            readLock.lock();
            if (cachedPicklistData.containsKey(pickList.getId())) {
                return cachedPicklistData.get(pickList.getId());
            }
            return getPickListData(selectedValue, pickList);
        } finally {
            readLock.unlock();
        }
    }

    private static List<PicklistData> getPickListData(String selectedValue, MetaPicklist picklist) {

        try {
            readLock.lock();
            if (selectedValue != null && selectedValue.equalsIgnoreCase(""))
                return null;
            DataBroker broker = DataRepository.getBroker(picklist.getSystemName());

            /********************************************** JDBC Broker Start **************************************************************/
            if (broker instanceof JDBCDataBroker) {

                String query = "";
                boolean isLinkColumnString = true;

                isLinkColumnString = !AttributeType.Integer.equals(AttributeType.valueOf(picklist.getLinkColumnType()));

                if (picklist.getParentId() == null) {

                    String preparedCritera = JdbcDataHelper.parseCritera(picklist.getSelectCritera());

                    if (preparedCritera != null) {
                        query = String.format(
                                "Select %5$s.%1$s , %5$s.%2$s , %5$s.%3$s from %4$s %5$s where %6$s",
                                new Object[]{
                                        picklist.getLinkColumn(),
                                        picklist.getDisplayColPri(),
                                        picklist.getDisplayColSec(),
                                        picklist.getTableName(),
                                        picklist.getPicklistName(),
                                        preparedCritera});
                    } else {
                        query = String.format(
                                "Select %5$s.%1$s , %5$s.%2$s , %5$s.%3$s from %4$s %5$s ",
                                new Object[]{
                                        picklist.getLinkColumn(),
                                        picklist.getDisplayColPri(),
                                        picklist.getDisplayColSec(),
                                        picklist.getTableName(),
                                        picklist.getPicklistName()});
                    }
                } else {

                    // If the parent exists for the pickList then build the criteria accordingly.
                    String preparedCritera = JdbcDataHelper.parseCritera(picklist.getSelectCritera());

                    if (preparedCritera != null) {
                        selectedValue = isLinkColumnString ? "'"
                                + selectedValue + "'" : selectedValue;
                        query = String.format(
                                "Select %5$s.%1$s , %5$s.%2$s , %5$s.%3$s from %4$s %5$s, %6$s %7$s where %8$s and %7$s.%10$s = %9$s",
                                new Object[]{
                                        picklist.getLinkColumn(),
                                        picklist.getDisplayColPri(),
                                        picklist.getDisplayColSec(),
                                        picklist.getTableName(),
                                        picklist.getPicklistName(),
                                        picklist.getParent().getTableName(),
                                        picklist.getParent().getPicklistName(),
                                        preparedCritera,
                                        selectedValue,
                                        picklist.getParent().getLinkColumn()});
                    } else {
                        query = String.format(
                                "Select %5$s.%1$s , %5$s.%2$s , %5$s.%3$s from %4$s %5$s, %6$s %7$s",
                                new Object[]{
                                        picklist.getLinkColumn(),
                                        picklist.getDisplayColPri(),
                                        picklist.getDisplayColSec(),
                                        picklist.getTableName(),
                                        picklist.getPicklistName(),
                                        picklist.getParent().getTableName(),
                                        picklist.getParent().getPicklistName()});
                    }
                }
                try {
                    List<PicklistData> data = broker.find(query);
                    List<PicklistData> sortedData = getSortedData(data);
                    return sortedData;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            /********************************************** JDBC Broker End **************************************************************/
            /*******************************************MetaData Broker Start**********************************************************/
            if (broker instanceof MetaDataBroker) {

                MetaEntity entity = MetaDataRegistry.getEntityBySystemName(picklist.getSystemName());
                Search search = new Search();
                search.addFrom(entity.getSystemName(), picklist.getPicklistName());
                search.addColumn(entity.getAttribute(picklist.getAttributeKey()).getSystemName());
                search.addColumn(entity.getAttribute(picklist.getDisplayAttrPrm()).getTableColumn());
                search.addColumn(entity.getAttribute(picklist.getDisplayAttrSec()).getTableColumn());
                String preparedCriteria = JdbcDataHelper.parseCritera(picklist.getSelectCritera());
                if (preparedCriteria != null)
                    search.setWhereClause(preparedCriteria);
                try {
                    List<DataObject> obj = broker.find(search);
                    List<PicklistData> data = new ArrayList<PicklistData>();
                    List<PicklistData> sortedData = getSortedData(getDataFromResult(obj, data, entity, picklist));
                    return sortedData;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            /******************************************* METADATA Broker End **********************************************************/
            /*****************************************Transaction Broker Start*******************************************************/
            /*if (broker instanceof SocketDelimitedDataBroker) {
                MetaEntity entity = MetaDataRegistry.getEntityBySystemName(picklist.getSystemName());
                Search search = new Search();
                search.addFrom(entity.getSystemName(), picklist.getPicklistName());
                try {
                    List<DataObject> obj = broker.find(search);
                    String subEntityId = StringHelper.EMPTY;
                    for (MetaRelationship rel : entity.getRelationList().values()) {
                        subEntityId = rel.getSubEntityId();
                    }
                    MetaEntity subEntity = MetaDataRegistry.getMetaEntity(subEntityId);
                    List<PicklistData> data = new ArrayList<PicklistData>();
                    List<PicklistData> sortedData = getSortedData(getDataFromResult(obj, data, subEntity, picklist));
                    return sortedData;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }*/
            /*****************************************Transaction Broker End**********************************************************/
            /***************************************DBIntegration Broker Start*******************************************************/
            if (broker instanceof DbIntegerationDataBroker) {
                MetaEntity entity = MetaDataRegistry.getEntityBySystemName(picklist.getSystemName());
                Search search = new Search();
                search.addFrom(entity.getSystemName(), picklist.getPicklistName());
                try {
                    List<DataObject> obj = broker.find(search);
					/*String subEntityId = StringHelper.EMPTY;
					for (MetaRelationship rel : entity.getRelationList().values()) {
						subEntityId = rel.getSubEntityId();
					}
					MetaEntity SubEntity = MetaDataRegistry.getMetaEntity(subEntityId);*/
                    List<PicklistData> data = new ArrayList<PicklistData>();
                    List<PicklistData> sortedData = getSortedData(getDataFromResult(obj, data, entity, picklist));
                    return sortedData;
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            /*****************************************Transaction Broker End**********************************************************/
            return null;
        } finally {
            readLock.unlock();
        }
    }

    private static List<PicklistData> getSortedData(List<PicklistData> dataList) {
        try {
            readLock.lock();
            Collections.sort(dataList);
            return dataList;
        } finally {
            readLock.unlock();
        }
    }

    private static void getSelectedValueForLevel(String selectedValue, Map<String, String> selectedPicListValues,
                                                 MetaPicklist leafMetaPickList, int level) {
        try {
            readLock.lock();
            String query = "";
            boolean isLinkColumnString = true;

            isLinkColumnString = !AttributeType.Integer.equals(AttributeType.valueOf(leafMetaPickList
                    .getLinkColumnType()));
            String preparedCritera = JdbcDataHelper.parseCritera(leafMetaPickList.getSelectCritera());

            if (leafMetaPickList.getParentId() == null) {

                if (preparedCritera != null) {
                    selectedValue = isLinkColumnString ? "'" + selectedValue + "'" : selectedValue;
                    query = String.format(
                            "Select %5$s.%1$s , %5$s.%2$s , %5$s.%3$s from %4$s %5$s where %6$s and %5$s.%1$s = %7$s",
                            new Object[]{leafMetaPickList.getLinkColumn(), leafMetaPickList.getDisplayColPri(),
                                    leafMetaPickList.getDisplayColSec(), leafMetaPickList.getTableName(),
                                    leafMetaPickList.getPicklistName(), preparedCritera, selectedValue});
                } else {
                    query = String.format(
                            "Select %5$s.%1$s , %5$s.%2$s , %5$s.%3$s from %4$s %5$s where %5$s.%1$s = %6$s",
                            new Object[]{leafMetaPickList.getLinkColumn(), leafMetaPickList.getDisplayColPri(),
                                    leafMetaPickList.getDisplayColSec(), leafMetaPickList.getTableName(),
                                    leafMetaPickList.getPicklistName(), selectedValue});
                }
            } else {

                // If the parent exists for the pickList then build the criteria
                // accordingly.
                if (preparedCritera != null) {
                    selectedValue = isLinkColumnString ? "'" + selectedValue + "'" : selectedValue;
                    query = String
                            .format(
                                    "Select %7$s.%1$s , %7$s.%2$s , %7$s.%3$s from %4$s %5$s, %6$s %7$s where %8$s and %5$s.%1$s = %9$s",
                                    new Object[]{leafMetaPickList.getLinkColumn(),
                                            leafMetaPickList.getDisplayColPri(), leafMetaPickList.getDisplayColSec(),
                                            leafMetaPickList.getTableName(), leafMetaPickList.getPicklistName(),
                                            leafMetaPickList.getParent().getTableName(),
                                            leafMetaPickList.getParent().getPicklistName(), preparedCritera,
                                            selectedValue});
                } else {
                    query = String
                            .format(
                                    "Select %7$s.%1$s , %7$s.%2$s , %7$s.%3$s from %4$s %5$s, %6$s %7$s where %5$s.%1$s = %8$s",
                                    new Object[]{leafMetaPickList.getLinkColumn(),
                                            leafMetaPickList.getDisplayColPri(), leafMetaPickList.getDisplayColSec(),
                                            leafMetaPickList.getTableName(), leafMetaPickList.getPicklistName(),
                                            leafMetaPickList.getParent().getTableName(),
                                            leafMetaPickList.getParent().getPicklistName(), selectedValue});
                }
            }

            JDBCDataBroker jdbcbroker = (JDBCDataBroker) DataRepository.getBroker(leafMetaPickList.getSystemName());
            List<PicklistData> data = jdbcbroker.find(query);
            if (!data.isEmpty()) {
                selectedPicListValues.put(String.valueOf(level), data.get(0).getValue());
            } else {
                selectedPicListValues.put(String.valueOf(level), "");
            }

        } finally {
            readLock.unlock();
        }
    }

    /**
     * This method uses the JDBC data broker to fetch the mapped pick list data.
     * Helper function to fetch the PickList data based on the parentId as
     * string which can be null. And the selected value of the current ComboBox.
     *
     * @param picklistid
     * @return
     */

    // Returns the path into the hierarchy for the given selected value.
    public static Map<String, String> getSelectedValuePath(Object val, MetaPicklist picklist) {

        try {
            readLock.lock();
            Map<String, String> selectedPicListValues = new HashMap<String, String>();

            // Getting the leaf level node.
            MetaPicklist leafMetaPickList = picklist;
            int level = 0;
            while (!leafMetaPickList.getChildPicList().isEmpty()) {
                leafMetaPickList = (MetaPicklist) leafMetaPickList.getChildPicList().iterator().next();
                level++;
            }

            // Now finding the selected id.
            selectedPicListValues.put(String.valueOf(level), (String) ConvertUtil.ConvertTo(val, String.class));
            while (level > 0) {
                level--;
                getSelectedValueForLevel(selectedPicListValues.get(String.valueOf(level + 1)), selectedPicListValues,
                        leafMetaPickList, level);
                leafMetaPickList = leafMetaPickList.getParent();
            }

            return selectedPicListValues;

        } finally {
            readLock.unlock();
        }
    }

    public static int deletePickListItem(MetaPicklist pickList, PicklistData item) {
        try {
            readLock.lock();
            String query = String
                    .format(
                            "DELETE FROM %1$s WHERE %1$s.%2$s = '%3$s' AND %1$s.%4$s = '%5$s' "
                                    + (pickList.isKeyRequired()
                                    && pickList.getLinkColumn().equalsIgnoreCase(pickList.getKeyColumn()) ? "AND %1$s.%6$s = '%7$s'"
                                    : StringHelper.EMPTY), new Object[]{pickList.getTableName(),
                                    pickList.getDisplayColPri(), item.getDisplayPrimary(), pickList.getDisplayColSec(),
                                    item.getDisplaySecondary(), pickList.getLinkColumn(), item.getValue()});
            query = query.replaceAll("= ''", "is null");
            JDBCDataBroker jdbcbroker = (JDBCDataBroker) DataRepository.getBroker(pickList.getSystemName());
            return jdbcbroker.update(query);
        } finally {
            readLock.unlock();
        }
    }

    public static int deletePickListItem(MetaPicklist pickList, List<PicklistData> oldItem, List<PicklistData> newItem) {
        try {
            readLock.lock();
            if (pickList == null || oldItem == null || newItem == null)
                return -1;
            int count = 0;
            int retValue = 0;
            for (int i = 0; i < newItem.size(); i++) {
                PicklistData data = newItem.get(i);
                if (data.isSelected()) {
                    retValue = deletePickListItem(pickList, oldItem.get(count++));
                    newItem.remove(data);
                }
            }
            return retValue;

        } finally {
            readLock.unlock();
        }
    }

    public static int insertPickListItem(MetaPicklist pickList, PicklistData item, UserDbImpl currentCustObj) {
        try {
            readLock.lock();
            Timestamp timestamp = null;
            DataBroker broker = DataRepository.getBroker(pickList.getSystemName());
            int retVal = 0;
            /********************************************** JDBC Broker Start **************************************************************/
            if (broker instanceof JDBCDataBroker) {


                DateFormat formatter2 = new SimpleDateFormat("d-MMMM-yyyy h:mm:ss a", Locale.ENGLISH);
                String now2 = formatter2.format(new Date());

//				DateFormat formatter = new SimpleDateFormat("M/d/yyyy h:mm:ss a");
//				String now = formatter.format(new Date());
//				

                StringBuilder queryBuilder = new StringBuilder();
                queryBuilder.append("INSERT INTO %1$s(");

                queryBuilder.append((pickList.isKeyRequired()) ? "%14$s, " : "%4$s, ");

                queryBuilder.append("%2$s, %3$s");

                if (pickList.isKeyRequired() && !pickList.getLinkColumn().equalsIgnoreCase(pickList.getKeyColumn()))
                    queryBuilder.append(", %4$s");

                queryBuilder.append(pickList.isExternal() ? StringHelper.EMPTY : ", %8$s, %9$s, %10$s, %11$s");
                if (pickList.getTableName().equalsIgnoreCase("Value_Tree_Node"))
                    queryBuilder.append(", %15$s, %16$s");
                queryBuilder.append(") VALUES (");
                queryBuilder.append(pickList.isKeyRequired() ? "'%17$s', " : "'%7$s', ");
                queryBuilder.append("'%5$s', N'%6$s'");
                if (pickList.isKeyRequired() && !pickList.getLinkColumn().equalsIgnoreCase(pickList.getKeyColumn()))
                    queryBuilder.append(", '%7$s'");
                queryBuilder.append(pickList.isExternal() ? StringHelper.EMPTY : ", '%12$s', '%13$s', '%12$s', '%13$s'");
                if (pickList.getTableName().equalsIgnoreCase("Value_Tree_Node"))
                    queryBuilder.append(",	'" + pickList.getPicklistName() + "',	'" + pickList.getPicklistName() + "."
                            + item.getDisplayPrimary() + "'");
                queryBuilder.append(")");

                String query = String.format(queryBuilder.toString(), new Object[]{
                        pickList.getTableName(),
                        pickList.getDisplayColPri(),
                        pickList.getDisplayColSec(),
                        pickList.getLinkColumn(),
                        item.getDisplayPrimary(),
                        item.getDisplaySecondary(),
                        item.getValue(),
                        "CREATED_ON",
                        "CREATED_BY",
                        "UPDATED_ON",
                        "UPDATED_BY",
                        now2,
                        currentCustObj.getLoginId(),
                        pickList.getKeyColumn(),
                        "Group_id",
                        "System_Name",
                        (pickList.isKeyRequired() ? MetaDataRegistry.getNextCounterValue(pickList.getKeyCounterName())
                                : StringHelper.EMPTY)});
                query = query.replaceAll("''", "null");

                retVal = ((JDBCDataBroker) broker).update(query);
            }
            /********************************************** JDBC Broker End **********************************************************/
            /******************************************* MetaData Broker Start *****************************************************/
            if (broker instanceof MetaDataBroker) {
                MetaEntity entity = MetaDataRegistry.getEntityBySystemName(pickList.getSystemName());
                DataObject obj = new DataObject(entity);
                obj.setValue(pickList.getDisplayAttrPrm(), item.getDisplayPrimary());
                obj.setValue(pickList.getDisplayAttrSec(), item.getDisplaySecondary());
                obj.setValue(pickList.getAttributeKey(), item.getValue());
			    /*obj.setValue("CREATED_ON", now);
	             obj.setValue("CREATED_BY", currentCustObj.getLoginId());
	             obj.setValue("UPDATED_ON", now);
	             obj.setValue("UPDATED_BY", currentCustObj.getLoginId());*/
                obj.save();
                //broker.add(obj);
                retVal = 1;
            }
            /********************************************** MetaData Broker End **********************************************************/
            return retVal;

        } finally {
            readLock.unlock();
        }
    }

    public static int updatePickListItem(MetaPicklist pickList, PicklistData oldItem, PicklistData newItem,
                                         UserDbImpl currentCustObj) {
        try {
            readLock.lock();
            DataBroker broker = DataRepository.getBroker(pickList.getSystemName());

            /********************************************** JDBC Broker Start **************************************************************/
            if (broker instanceof JDBCDataBroker) {

                boolean isLinkColumnString = false;
                DateFormat formatter = new SimpleDateFormat("M/d/yyyy h:mm:ss a");
                String now = formatter.format(new Date());
                isLinkColumnString = !AttributeType.Integer.equals(AttributeType.valueOf(pickList.getLinkColumnType()));
                String preparedCritera = JdbcDataHelper.parseCritera(pickList.getSelectCritera());
                String selectedValue = isLinkColumnString ? "'" + newItem.getValue() + "'" : newItem.getValue();
                String query = String.format("UPDATE %1$s SET %1$s.%2$s = '%5$s', %1$s.%3$s = N'%6$s', %1$s.%4$s = '%7$s'"
                        + (pickList.isExternal() ? StringHelper.EMPTY : ", %1$s.%11$s = '%13$s', %1$s.%12$s = '%14$s'")
                        + " WHERE %1$s.%2$s = '%8$s' AND %1$s.%3$s = '%9$s' AND %1$s.%4$s = '%10$s'", new Object[]{
                        pickList.getTableName(), pickList.getDisplayColPri(), pickList.getDisplayColSec(),
                        pickList.getLinkColumn(), newItem.getDisplayPrimary(), newItem.getDisplaySecondary(),
                        newItem.getValue(), oldItem.getDisplayPrimary(), oldItem.getDisplaySecondary(), oldItem.getValue(),
                        "UPDATED_ON", "UPDATED_BY", now, currentCustObj.getLoginId()});
                query = query.replaceAll("''", "null");
                return ((JDBCDataBroker) broker).update(query);
            }
            /********************************************** JDBC Broker End **************************************************************/
            /******************************************* MetaData Broker Start ********************************************************/
            if (broker instanceof MetaDataBroker) {
                MetaEntity entity = MetaDataRegistry.getEntityBySystemName(pickList.getSystemName());
                Search search = new Search();
                search.addFrom(entity.getSystemName(), pickList.getPicklistName());
                search.addColumn(entity.getAttribute(pickList.getAttributeKey()).getTableColumn());
                search.addColumn(entity.getAttribute(pickList.getDisplayAttrPrm()).getTableColumn());
                search.addColumn(entity.getAttribute(pickList.getDisplayAttrSec()).getTableColumn());
                search.addCriterion(com.avanza.core.data.expression.Criterion.equal(entity.getAttribute(pickList.getDisplayAttrPrm()).getTableColumn(),
                        oldItem.getDisplayPrimary()));
                search.addCriterion(com.avanza.core.data.expression.Criterion.equal(entity.getAttribute(pickList.getDisplayAttrSec()).getTableColumn(),
                        oldItem.getDisplaySecondary()));
                search.addCriterion(com.avanza.core.data.expression.Criterion.equal(entity.getAttribute(pickList.getAttributeKey()).getTableColumn(),
                        oldItem.getValue()));

                List<Object> object = broker.find(search);
                if (!object.isEmpty()) {
                    DataObject obj = (DataObject) object.get(0);
                    try {
                        obj.setValue(pickList.getDisplayAttrPrm(), newItem.getDisplayPrimary());
                        obj.setValue(pickList.getDisplayAttrSec(), newItem.getDisplaySecondary());
                        obj.setValue(pickList.getAttributeKey(), newItem.getValue());
                    } catch (Exception e) {
                        System.out.println("Value of Primary key cannot be modified");
                        return -1;
                    }
                    broker.update(obj);
                    return 1;
                }
            }
            /********************************************** MetaData Broker End **********************************************************/
            return -1; // hard-coded for the time being.
        } finally {
            readLock.unlock();
        }

    }

    public static int updatePickListItem(MetaPicklist pickList, List<PicklistData> oldItem, List<PicklistData> newItem,
                                         UserDbImpl currentCustObj) {
        try {
            readLock.lock();
            if (pickList == null || oldItem == null || newItem == null)
                return -1;
            int count = Math.min(oldItem.size(), newItem.size());
            int retValue = 0;
            for (int i = 0; i < count; i++)
                retValue = updatePickListItem(pickList, oldItem.get(i), newItem.get(i), currentCustObj);
            return retValue;

        } finally {
            readLock.unlock();
        }

    }

    public static List<PicklistData> getClonePickListData(List<PicklistData> pickListData, boolean flag)
            throws CloneNotSupportedException {
        try {
            readLock.lock();
            if (pickListData == null || pickListData.size() == 0)
                throw new CloneNotSupportedException("Clone is not supported on empty object");

            List<PicklistData> cloneList = new ArrayList<PicklistData>();
            for (PicklistData data : pickListData)
                if (flag) {
                    if (data.isSelected())
                        cloneList.add((PicklistData) data.clone());
                } else
                    cloneList.add((PicklistData) data.clone());

            return cloneList;

        } finally {
            readLock.unlock();
        }

    }

    public static List<PicklistData> getClonePickListData(List<PicklistData> pickListData)
            throws CloneNotSupportedException {
        try {
            readLock.lock();
            return getClonePickListData(pickListData, false);
        } finally {
            readLock.unlock();
        }
    }

    // This function gets value of pick list, it returns primary name and secondary name of pick list value.
    public static String[] getPickListValue(String value, String pickListId) {
        String[] pickListValue = new String[2];
        // Added to set the selected file type
        if (StringHelper.isNotEmpty(value)) {
            pickListValue[0] = value;
            pickListValue[1] = value;
        }

        MetaPicklist picklist = PickListManager.getMetaPickList(pickListId);
        List<PicklistData> pickListdata = PickListManager.getPickListData(picklist.getId());
        for (PicklistData data : pickListdata) {
            if (data.getValue() != null) {
                if (data.getValue().equalsIgnoreCase(value)) {
                    pickListValue[0] = data.getDisplayPrimary();
                    pickListValue[1] = data.getDisplaySecondary();
                }
            }
        }
        return pickListValue;
    }

    public static List<PicklistData> getDataFromResult(List<DataObject> obj, List<PicklistData> data, MetaEntity entity, MetaPicklist picklist) {
        for (DataObject dataObject : obj) {

            String namePrm = dataObject.getAsString(entity.getAttribute(picklist.getDisplayAttrPrm()).getSystemName());
            String nameSec = dataObject.getAsString(entity.getAttribute(picklist.getDisplayAttrSec()).getSystemName());
            String value = dataObject.getAsString(entity.getAttribute(picklist.getAttributeKey()).getSystemName());
            PicklistData pickList = new PicklistData(namePrm, nameSec, value);
            data.add(pickList);

        }

        return data;
    }
}
