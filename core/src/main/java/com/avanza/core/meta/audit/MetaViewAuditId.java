package com.avanza.core.meta.audit;

/**
 * @author Mehran Junejo
 */

public class MetaViewAuditId implements java.io.Serializable {

    // Fields

    private String metaViewAuditId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public MetaViewAuditId() {
    }

    /**
     * full constructor
     */
    public MetaViewAuditId(String roleId, Integer rev) {
        this.metaViewAuditId = roleId;
        this.rev = rev;
    }

    // Property accessors


    public Integer getRev() {
        return this.rev;
    }


    public String getMetaViewAuditId() {
        return metaViewAuditId;
    }

    public void setMetaViewAuditId(String metaViewAuditId) {
        this.metaViewAuditId = metaViewAuditId;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof MetaViewAuditId))
            return false;
        MetaViewAuditId castOther = (MetaViewAuditId) other;

        return ((this.getMetaViewAuditId() == castOther.getMetaViewAuditId()) || (this
                .getMetaViewAuditId() != null
                && castOther.getMetaViewAuditId() != null && this.getMetaViewAuditId().equals(
                castOther.getMetaViewAuditId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getMetaViewAuditId() == null ? 0 : this.getMetaViewAuditId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}