package com.avanza.core.meta.audit;

/**
 * @author Mehran Junejo
 */

public class MetaEntityCategoryAuditId implements java.io.Serializable {

    // Fields

    private String metaEntityCategoryAuditId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public MetaEntityCategoryAuditId() {
    }

    /**
     * full constructor
     */
    public MetaEntityCategoryAuditId(String roleId, Integer rev) {
        this.metaEntityCategoryAuditId = roleId;
        this.rev = rev;
    }

    // Property accessors


    public Integer getRev() {
        return this.rev;
    }


    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public String getMetaEntityCategoryAuditId() {
        return metaEntityCategoryAuditId;
    }

    public void setMetaEntityCategoryAuditId(String metaEntityCategoryAuditId) {
        this.metaEntityCategoryAuditId = metaEntityCategoryAuditId;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof MetaEntityCategoryAuditId))
            return false;
        MetaEntityCategoryAuditId castOther = (MetaEntityCategoryAuditId) other;

        return ((this.getMetaEntityCategoryAuditId() == castOther.getMetaEntityCategoryAuditId()) || (this
                .getMetaEntityCategoryAuditId() != null
                && castOther.getMetaEntityCategoryAuditId() != null && this.getMetaEntityCategoryAuditId().equals(
                castOther.getMetaEntityCategoryAuditId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getMetaEntityCategoryAuditId() == null ? 0 : this.getMetaEntityCategoryAuditId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}