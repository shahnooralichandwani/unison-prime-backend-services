package com.avanza.core.meta.audit;

/**
 * @author Mehran Junejo
 */

public class OrgRoleAuditId implements java.io.Serializable {

    // Fields

    private String roleId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public OrgRoleAuditId() {
    }

    /**
     * full constructor
     */
    public OrgRoleAuditId(String roleId, Integer rev) {
        this.roleId = roleId;
        this.rev = rev;
    }

    // Property accessors


    public Integer getRev() {
        return this.rev;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof OrgRoleAuditId))
            return false;
        OrgRoleAuditId castOther = (OrgRoleAuditId) other;

        return ((this.getRoleId() == castOther.getRoleId()) || (this
                .getRoleId() != null
                && castOther.getRoleId() != null && this.getRoleId().equals(
                castOther.getRoleId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getRoleId() == null ? 0 : this.getRoleId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}