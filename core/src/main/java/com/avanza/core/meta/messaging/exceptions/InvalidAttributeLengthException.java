/**
 *
 */
package com.avanza.core.meta.messaging.exceptions;

/**
 * @author shahbaz.ali
 *
 */
public class InvalidAttributeLengthException extends Exception {

    String message;

    public InvalidAttributeLengthException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}
