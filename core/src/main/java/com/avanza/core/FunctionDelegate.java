package com.avanza.core;

public interface FunctionDelegate {
    public interface Arg0<Return> {
        Return call();
    }

    public interface Arg1<Return, A> {
        Return call(A arg1);
    }

    public interface Arg2<Return, A, B> {
        Return call(A arg1, B arg2);
    }

    public interface Arg3<Return, A, B, C> {
        Return call(A arg1, B arg2, C arg3);
    }

    public interface Arg4<Return, A, B, C, D> {
        Return call(A arg1, B arg2, C arg3, D arg4);
    }

    public interface Arg5<Return, A, B, C, D, E> {
        Return call(A arg1, B arg2, C arg3, D arg4, E arg5);
    }
}


