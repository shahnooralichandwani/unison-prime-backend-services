package com.avanza.core.exception;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.meta.MetaDataRegistry;

public class DBException {

    /**
     * This class is used to log the exception in database.
     *
     * @author Nasir Nawab
     */

    public static void logException(Exception exception) {
        DataBroker broker = DataRepository.getBroker(DBException.class.getName());
        if (exception.getExceptionId() == null)
            exception.setExceptionId((String) MetaDataRegistry.getNextCounterValue("Exception_Id"));
        broker.add(exception);
    }
}
