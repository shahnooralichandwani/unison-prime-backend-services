package com.avanza.core.exception;

import com.avanza.core.util.configuration.ConfigSection;

/**
 * @author Nasir Nawab
 */
public class ExceptionLoader {

    private static boolean isLoaded = false;
    private static boolean saveException;

    public static void load(ConfigSection cfgSection) {
        if (cfgSection == null || isLoaded) return;

        saveException = cfgSection.getBooleanValue("saveException");

        isLoaded = true;
    }

    public static boolean isSaveException() {
        return saveException;
    }

    public static void setSaveException(boolean saveException) {
        ExceptionLoader.saveException = saveException;
    }

}
