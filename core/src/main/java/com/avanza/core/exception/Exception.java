package com.avanza.core.exception;

import java.util.Date;

import com.avanza.core.data.DbObject;

/**
 * Exception entity. @author MyEclipse Persistence Tools
 */

public class Exception extends DbObject implements java.io.Serializable {

    // Fields

    private String exceptionId;
    private String logLevel;
    private Date exceptionTime;
    private String remoteIp;
    private String loggerName;
    private String moduleName;
    private String className;
    private String methodName;
    private String message;
    private int lineNo;
    private String exceptionName;

    // Constructors

    /**
     * default constructor
     */
    public Exception() {
    }

    /**
     * minimal constructor
     */
    public Exception(String exceptionId) {
        this.exceptionId = exceptionId;
    }

    /**
     * full constructor
     */
    public Exception(String exceptionId, String logLevel,
                     Date exceptionTime, String remoteIp, String loggerName,
                     String moduleName, String className, String methodName,
                     String message) {
        this.exceptionId = exceptionId;
        this.logLevel = logLevel;
        this.exceptionTime = exceptionTime;
        this.remoteIp = remoteIp;
        this.loggerName = loggerName;
        this.moduleName = moduleName;
        this.className = className;
        this.methodName = methodName;
        this.message = message;
    }

    // Property accessors

    public String getExceptionId() {
        return this.exceptionId;
    }

    public void setExceptionId(String exceptionId) {
        this.exceptionId = exceptionId;
    }

    public String getLogLevel() {
        return this.logLevel;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public Date getExceptionTime() {
        return this.exceptionTime;
    }

    public void setExceptionTime(Date exceptionTime) {
        this.exceptionTime = exceptionTime;
    }

    public String getRemoteIp() {
        return this.remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public String getLoggerName() {
        return this.loggerName;
    }

    public void setLoggerName(String loggerName) {
        this.loggerName = loggerName;
    }

    public String getModuleName() {
        return this.moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return this.methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public String getExceptionName() {
        return exceptionName;
    }

    public void setExceptionName(String exceptionName) {
        this.exceptionName = exceptionName;
    }

}