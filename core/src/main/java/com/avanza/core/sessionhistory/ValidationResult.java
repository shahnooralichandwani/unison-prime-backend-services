package com.avanza.core.sessionhistory;

import com.avanza.core.CoreException;
import com.avanza.core.data.sequence.ResetType;

public enum ValidationResult {

    Correct("Correct"), Incorrect("Incorrect"), NotAnswered("NotAnswered");

    private String value;

    private ValidationResult(String val) {
        this.value = val;
    }

    public String toString() {
        return this.value;
    }

    public static ValidationResult fromString(String value) {

        ValidationResult retVal = null;

        if (value.equalsIgnoreCase("Corrrect"))
            retVal = ValidationResult.Correct;
        else if (value.equalsIgnoreCase("Incorrect"))
            retVal = ValidationResult.Incorrect;
        else if (value.equalsIgnoreCase("NotAnswered"))
            retVal = ValidationResult.NotAnswered;
        else
            throw new CoreException("[%1$s] is not recognized as ValidationResult", value);

        return retVal;
    }


}
