package com.avanza.core.sessionhistory;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;

/**
 * InteractionOutcome entity.
 *
 * @author MyEclipse Persistence Tools
 */

public class InteractionOutcome extends DbObject {

    // Fields

    private String interactOutcomeId;
    private String displayTextPrm;
    private String displayTextSec;
    private String otherCode;
    private Date createdOn;
    private String createdBy;
    private Date updatedOn;
    private String updatedBy;
    private Set<SessionHistory> sessionHistories = new HashSet<SessionHistory>(0);

    // Constructors

    /**
     * default constructor
     */
    public InteractionOutcome() {
    }

    /**
     * minimal constructor
     */
    public InteractionOutcome(String interactOutcomeId, String displayTextPrm,
                              Date createdOn, String createdBy, Date updatedOn, String updatedBy) {
        this.interactOutcomeId = interactOutcomeId;
        this.displayTextPrm = displayTextPrm;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
    }

    /**
     * full constructor
     */
    public InteractionOutcome(String interactOutcomeId, String displayTextPrm,
                              String displayTextSec, String otherCode, Date createdOn,
                              String createdBy, Date updatedOn, String updatedBy,
                              Set sessionHistories) {
        this.interactOutcomeId = interactOutcomeId;
        this.displayTextPrm = displayTextPrm;
        this.displayTextSec = displayTextSec;
        this.otherCode = otherCode;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
        this.sessionHistories = sessionHistories;
    }

    // Property accessors

    public String getInteractOutcomeId() {
        return this.interactOutcomeId;
    }

    public void setInteractOutcomeId(String interactOutcomeId) {
        this.interactOutcomeId = interactOutcomeId;
    }

    public String getDisplayTextPrm() {
        return this.displayTextPrm;
    }

    public void setDisplayTextPrm(String displayTextPrm) {
        this.displayTextPrm = displayTextPrm;
    }

    public String getDisplayTextSec() {
        return this.displayTextSec;
    }

    public void setDisplayTextSec(String displayTextSec) {
        this.displayTextSec = displayTextSec;
    }

    public String getOtherCode() {
        return this.otherCode;
    }

    public void setOtherCode(String otherCode) {
        this.otherCode = otherCode;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Set<SessionHistory> getSessionHistories() {
        return this.sessionHistories;
    }

    public void setSessionHistories(Set<SessionHistory> sessionHistories) {
        this.sessionHistories = sessionHistories;
    }

}