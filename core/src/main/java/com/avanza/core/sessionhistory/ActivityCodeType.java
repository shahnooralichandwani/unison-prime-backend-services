package com.avanza.core.sessionhistory;


public enum ActivityCodeType {

    Internal(0), External(1);

    private static final String INTERNAL = "internal";
    private static final String EXTERNAL = "external";

    private int value;

    private ActivityCodeType(int value) {

        this.value = value;
    }

    public int toInteger() {

        return this.value;
    }

    public static ActivityCodeType parse(String typeValue) {

        if (ActivityCodeType.INTERNAL.equalsIgnoreCase(typeValue))
            return ActivityCodeType.Internal;
        else if (ActivityCodeType.EXTERNAL.equalsIgnoreCase(typeValue))
            return ActivityCodeType.External;

        throw new IllegalArgumentException(String.format("[%1$s] is not identified as valid ActivityCodeType value.", typeValue));
    }

    public static ActivityCodeType fromInt(int value) {

        ActivityCodeType retVal;

        switch (value) {

            case 0:
                retVal = ActivityCodeType.Internal;
                break;
            case 1:
                retVal = ActivityCodeType.External;
                break;
            default:
                throw new IllegalArgumentException(String.format("[%1$d] is not identified as valid ActionStatus value.", value));
        }

        return retVal;
    }

}
