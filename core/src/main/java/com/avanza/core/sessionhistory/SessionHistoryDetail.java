package com.avanza.core.sessionhistory;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.avanza.core.data.DbObject;

/**
 * @author kraza
 */
public class SessionHistoryDetail extends DbObject implements Comparable {

    /**
     *
     */
    private static final long serialVersionUID = -2471182021599980599L;

    // TODO Kindly remove the serNo, sno as they are duplicate by actionSeqNo. 
    private short serNo;
    private short sno;
    private String sessionId;
    private Date actionTime; //
    private InterfaceChannel interfaceChannel;
    private SessionHistory sessionHistory;
    private String custRelationNum;
    private String activityTypeId;
    private String refDocNum; //
    private String actionStatus; //
    private String userId;
    private String docTypeId; //
    private String channelId;
    private String activityDescription;
    private ActivityType activityType;
    private int isExternal;
    private int actionSeqNum;
    private String fullName;
    private String branchCode;
    private String segmentCode;
    private boolean changed;
    private String external;
    private String parentActivity1;
    private String parentActivity2;
    private String parentActivity3;
    private String parentActivity4;

    private String rowId = new String("");
    ;
    private String rowNum = new String("");
    private Boolean deleted;

    private Date startTime;
    private Date endTime;

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    public SessionHistoryDetail() {
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getActionTime() {
        return this.actionTime;
    }

    public String getTimeString() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
        if (df.format(new Date()).equalsIgnoreCase(df.format(this.actionTime))) {
            df = new SimpleDateFormat("hh:mm:ss");
            return df.format(this.actionTime);
        } else {
            df = new SimpleDateFormat("hh:mm:ss");
            return df.format(this.actionTime);
        }
    }

    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }

    public InterfaceChannel getInterfaceChannel() {
        return this.interfaceChannel;
    }

    public void setInterfaceChannel(InterfaceChannel interfaceChannel) {
        this.interfaceChannel = interfaceChannel;
    }

    public SessionHistory getSessionHistory() {
        return this.sessionHistory;
    }

    public void setSessionHistory(SessionHistory sessionHistory) {
        this.sessionHistory = sessionHistory;
    }

    public String getCustRelationNum() {
        return this.custRelationNum;
    }

    public void setCustRelationNum(String custRelationNum) {
        this.custRelationNum = custRelationNum;
    }

    public String getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public String getRefDocNum() {
        return this.refDocNum;
    }

    public void setRefDocNum(String refDocNum) {
        this.refDocNum = refDocNum;
    }

    public String getActionStatus() {
        return this.actionStatus;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDocTypeId() {
        return this.docTypeId;
    }

    public void setDocTypeId(String docTypeId) {
        this.docTypeId = docTypeId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public int getIsExternal() {
        return isExternal;
    }

    public void setIsExternal(int isExternal) {
        this.isExternal = isExternal;
    }


    public String getRowNum() {
        return rowNum;
    }

    public void setRowNum(String rowNum) {
        this.rowNum = rowNum;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }


    public short getSerNo() {
        return serNo;
    }

    public void setSerNo(short serNo) {
        this.serNo = serNo;
    }

    public short getSno() {
        return sno;
    }

    public void setSno(short sno) {
        this.sno = sno;
    }

    public int compareTo(Object detailObj) {
        SessionHistoryDetail detail = (SessionHistoryDetail) detailObj;
        int result = new Integer(this.getActionSeqNum()).compareTo(new Integer(detail.getActionSeqNum()));
        if (result == 0)
            result = detail.getActionTime().compareTo(this.getActionTime());

        return result;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((actionStatus == null) ? 0 : actionStatus.hashCode());
        result = prime * result
                + ((actionTime == null) ? 0 : actionTime.hashCode());
        result = prime
                * result
                + ((activityDescription == null) ? 0 : activityDescription
                .hashCode());
        result = prime * result
                + ((activityType == null) ? 0 : activityType.hashCode());
        result = prime * result
                + ((activityTypeId == null) ? 0 : activityTypeId.hashCode());
        result = prime * result
                + ((channelId == null) ? 0 : channelId.hashCode());
        result = prime * result
                + ((custRelationNum == null) ? 0 : custRelationNum.hashCode());
        result = prime * result + ((deleted == null) ? 0 : deleted.hashCode());
        result = prime * result
                + ((docTypeId == null) ? 0 : docTypeId.hashCode());
        result = prime
                * result
                + ((interfaceChannel == null) ? 0 : interfaceChannel.hashCode());
        result = prime * result + isExternal;
        result = prime * result
                + ((refDocNum == null) ? 0 : refDocNum.hashCode());
        result = prime * result + ((rowId == null) ? 0 : rowId.hashCode());
        result = prime * result + ((rowNum == null) ? 0 : rowNum.hashCode());
        result = prime * result + serNo;
        result = prime * result
                + ((sessionHistory == null) ? 0 : sessionHistory.hashCode());
        result = prime * result
                + ((sessionId == null) ? 0 : sessionId.hashCode());
        result = prime * result + sno;
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final SessionHistoryDetail other = (SessionHistoryDetail) obj;
        if (actionStatus == null) {
            if (other.actionStatus != null)
                return false;
        } else if (!actionStatus.equals(other.actionStatus))
            return false;
        if (actionTime == null) {
            if (other.actionTime != null)
                return false;
        } else if (!actionTime.equals(other.actionTime))
            return false;
        if (activityDescription == null) {
            if (other.activityDescription != null)
                return false;
        } else if (!activityDescription.equals(other.activityDescription))
            return false;
        if (activityType == null) {
            if (other.activityType != null)
                return false;
        } else if (!activityType.equals(other.activityType))
            return false;
        if (activityTypeId == null) {
            if (other.activityTypeId != null)
                return false;
        } else if (!activityTypeId.equals(other.activityTypeId))
            return false;
        if (channelId == null) {
            if (other.channelId != null)
                return false;
        } else if (!channelId.equals(other.channelId))
            return false;
        if (custRelationNum == null) {
            if (other.custRelationNum != null)
                return false;
        } else if (!custRelationNum.equals(other.custRelationNum))
            return false;
        if (deleted == null) {
            if (other.deleted != null)
                return false;
        } else if (!deleted.equals(other.deleted))
            return false;
        if (docTypeId == null) {
            if (other.docTypeId != null)
                return false;
        } else if (!docTypeId.equals(other.docTypeId))
            return false;
        if (interfaceChannel == null) {
            if (other.interfaceChannel != null)
                return false;
        } else if (!interfaceChannel.equals(other.interfaceChannel))
            return false;
        if (isExternal != other.isExternal)
            return false;
        if (refDocNum == null) {
            if (other.refDocNum != null)
                return false;
        } else if (!refDocNum.equals(other.refDocNum))
            return false;
        if (rowId == null) {
            if (other.rowId != null)
                return false;
        } else if (!rowId.equals(other.rowId))
            return false;
        if (rowNum == null) {
            if (other.rowNum != null)
                return false;
        } else if (!rowNum.equals(other.rowNum))
            return false;
        if (serNo != other.serNo)
            return false;
        if (sessionHistory == null) {
            if (other.sessionHistory != null)
                return false;
        } else if (!sessionHistory.equals(other.sessionHistory))
            return false;
        if (sessionId == null) {
            if (other.sessionId != null)
                return false;
        } else if (!sessionId.equals(other.sessionId))
            return false;
        if (sno != other.sno)
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return true;
    }

    public int getActionSeqNum() {
        return actionSeqNum;
    }

    public void setActionSeqNum(int actionSeqNum) {
        this.actionSeqNum = actionSeqNum;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getSegmentCode() {
        return segmentCode;
    }

    public void setSegmentCode(String segmentCode) {
        this.segmentCode = segmentCode;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public String getExternal() {
        return external;
    }

    public void setExternal(String external) {
        this.external = external;
    }


    public String getParentActivity1() {
        return parentActivity1;
    }

    public void setParentActivity1(String parentActivity1) {
        this.parentActivity1 = parentActivity1;
    }

    public String getParentActivity2() {
        return parentActivity2;
    }

    public void setParentActivity2(String parentActivity2) {
        this.parentActivity2 = parentActivity2;
    }

    public String getParentActivity3() {
        return parentActivity3;
    }

    public void setParentActivity3(String parentActivity3) {
        this.parentActivity3 = parentActivity3;
    }

    public String getParentActivity4() {
        return parentActivity4;
    }

    public void setParentActivity4(String parentActivity4) {
        this.parentActivity4 = parentActivity4;
    }

}
