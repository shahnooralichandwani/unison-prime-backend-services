package com.avanza.core.sessionhistory;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DbObject;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;

public class ValidateScript extends DbObject implements Cloneable {

    private static final long serialVersionUID = -6435017944152790307L;
    // Fields

    private String validateScriptId;
    private String scriptQuestionPrm;
    private String scriptQuestionSec;
    private String answerExpression;
    private String answer;
    private String mandatory;
    private String displayOrder;
    private ValidateScriptLog validateScriptLog = new ValidateScriptLog();
    private String result = "";
    private String isDisplay;


    // Constructors


    /**
     * default constructor
     */
    public ValidateScript() {

    }

    // Property accessors

    public String getValidateScriptId() {
        return this.validateScriptId;
    }

    public void setValidateScriptId(String validateScriptId) {
        this.validateScriptId = validateScriptId;
    }

    public String getScriptQuestionPrm() {
        return this.scriptQuestionPrm;
    }

    public String getScriptQuestion() {
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class.getName());
        boolean isPrimaryLocale = ((LocaleInfo) webContext.getAttribute(SessionKeyLookup.CurrentLocale)).isPrimary();
        return isPrimaryLocale ? this.scriptQuestionPrm : this.scriptQuestionSec;
    }

    public void setScriptQuestionPrm(String scriptQuestionPrm) {
        this.scriptQuestionPrm = scriptQuestionPrm;
    }

    public String getScriptQuestionSec() {
        return this.scriptQuestionSec;
    }

    public void setScriptQuestionSec(String scriptQuestionSec) {
        this.scriptQuestionSec = scriptQuestionSec;
    }

    public String getAnswerExpression() {
        return this.answerExpression;
    }

    public void setAnswerExpression(String answerExpression) {
        this.answerExpression = answerExpression;
    }

    public ValidateScriptLog getValidateScriptLog() {
        return validateScriptLog;
    }

    public void setValidateScriptLog(ValidateScriptLog validateScriptLog) {
        this.validateScriptLog = validateScriptLog;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
    }

    public void setResultManually(String result) {
        this.result = result;
    }

    public String getMandatory() {
        return mandatory;
    }

    public boolean getDisabled() {
        return "1".equalsIgnoreCase(this.mandatory);
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(String displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        ValidateScript newVal = new ValidateScript();
        newVal.copyValues(this);
        newVal.answer = this.answer;
        newVal.answerExpression = this.answerExpression;
        newVal.displayOrder = this.displayOrder;
        newVal.mandatory = this.mandatory;
        newVal.result = this.result;

        newVal.scriptQuestionPrm = this.scriptQuestionPrm;
        newVal.scriptQuestionSec = this.scriptQuestionSec;
        newVal.validateScriptId = this.validateScriptId;
        newVal.validateScriptLog = this.validateScriptLog;
        //Anam
        newVal.isDisplay = this.isDisplay;
        return super.clone();
    }

    public String getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(String isDisplay) {
        this.isDisplay = isDisplay;
    }
}