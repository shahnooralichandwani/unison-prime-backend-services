package com.avanza.core.sessionhistory;

import com.avanza.core.data.DbObject;

/**
 * @author shafique.rehman
 */
public class SessionDetail extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */

    private String sessDetId;
    private String sessionId;
    private String userId;
    private String nodeIp;
    private String remoteIp;
    private String url;


    public String getSessDetId() {
        return sessDetId;
    }

    public void setSessDetId(String sessDetId) {
        this.sessDetId = sessDetId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNodeIp() {
        return nodeIp;
    }

    public void setNodeIp(String nodeIp) {
        this.nodeIp = nodeIp;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }


}
