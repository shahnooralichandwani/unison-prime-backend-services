package com.avanza.core.sessionhistory;

import com.avanza.core.data.DbObject;

/**
 * IdentityType entity.
 *
 * @author MyEclipse Persistence Tools
 */

public class IdentityType extends DbObject {

    // Fields

    /**
     *
     */
    private static final long serialVersionUID = 5577396385434574346L;

    private String identityTypeId;
    private String displayNamePrm;
    private String displayNameSec;
    private ValidateScriptLog validateScriptLog = new ValidateScriptLog();

    // Constructors

    /**
     * default constructor
     */
    public IdentityType() {
    }

    // Property accessors

    public String getIdentityTypeId() {
        return this.identityTypeId;
    }

    public void setIdentityTypeId(String identityTypeId) {
        this.identityTypeId = identityTypeId;
    }

    public String getDisplayNamePrm() {
        return this.displayNamePrm;
    }

    public void setDisplayNamePrm(String displayNamePrm) {
        this.displayNamePrm = displayNamePrm;
    }

    public String getDisplayNameSec() {
        return this.displayNameSec;
    }

    public void setDisplayNameSec(String displayNameSec) {
        this.displayNameSec = displayNameSec;
    }

    public ValidateScriptLog getValidateScriptLog() {
        return validateScriptLog;
    }

    public void setValidateScriptLog(ValidateScriptLog validateScriptLog) {
        this.validateScriptLog = validateScriptLog;
    }
}