package com.avanza.core.sessionhistory;

import com.avanza.core.callscript.CallScriptLog;
import com.avanza.core.data.DbObject;
import com.avanza.integration.adapter.ChannelAdapter;
import com.avanza.integration.adapter.ChannelTranResponse;
import com.avanza.integration.adapter.ChannelTransaction;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author kraza
 */
public class InterfaceChannel extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 5320150591356156160L;

    public static final String CHANNEL_SOFTPHONE_ID = "004"/*"softPhone"*/;
    public static final String CHANNEL_RENDEZVOUS_ID = "Rendezvous";

    private String channelId;
    private String chanelNamePrm;
    private String chanelNameSec;
    private Set<SessionHistoryDetail> sessionHistoryDetails = new HashSet<SessionHistoryDetail>(0);
    private Set<CallScriptLog> callScriptLogs = new HashSet<CallScriptLog>(0);
    private Set<SessionHistory> sessionHistories = new HashSet<SessionHistory>(0);

    private Map<String, ChannelTranResponse> channelTranResponses = new HashMap<String, ChannelTranResponse>(0);
    private Set<ChannelAdapter> channelAdapters = new HashSet(0);
    private Set<ChannelTransaction> channelTransactions = new HashSet(0);


    public InterfaceChannel() {
    }

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChanelNamePrm() {
        return this.chanelNamePrm;
    }

    public void setChanelNamePrm(String chanelNamePrm) {
        this.chanelNamePrm = chanelNamePrm;
    }

    public String getChanelNameSec() {
        return this.chanelNameSec;
    }

    public void setChanelNameSec(String chanelNameSec) {
        this.chanelNameSec = chanelNameSec;
    }

    public Set<SessionHistoryDetail> getSessionHistoryDetails() {
        return this.sessionHistoryDetails;
    }

    public void setSessionHistoryDetails(
            Set<SessionHistoryDetail> sessionHistoryDetails) {
        this.sessionHistoryDetails = sessionHistoryDetails;
    }

    public Set<CallScriptLog> getCallScriptLogs() {
        return this.callScriptLogs;
    }

    public void setCallScriptLogs(Set<CallScriptLog> callScriptLogs) {
        this.callScriptLogs = callScriptLogs;
    }

    public Set<SessionHistory> getSessionHistories() {
        return this.sessionHistories;
    }

    public void setSessionHistories(Set<SessionHistory> sessionHistories) {
        this.sessionHistories = sessionHistories;
    }

    public Set<ChannelAdapter> getChannelAdapters() {
        return channelAdapters;
    }

    public void setChannelAdapters(Set<ChannelAdapter> channelAdapters) {
        this.channelAdapters = channelAdapters;
    }

    public Map<String, ChannelTranResponse> getChannelTranResponses() {
        return channelTranResponses;
    }

    public void setChannelTranResponses(Map<String, ChannelTranResponse> channelTranResponses) {
        this.channelTranResponses = channelTranResponses;
    }

    public Set<ChannelTransaction> getChannelTransactions() {
        return channelTransactions;
    }

    public void setChannelTransactions(Set<ChannelTransaction> channelTransactions) {
        this.channelTransactions = channelTransactions;
    }
}
