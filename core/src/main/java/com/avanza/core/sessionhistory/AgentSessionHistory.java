package com.avanza.core.sessionhistory;

import java.util.Date;

import com.avanza.core.data.DbObject;

public class AgentSessionHistory extends DbObject implements Cloneable {
    private static final long serialVersionUID = -2714507844039427115L;


    private String sessionId;
    private String agentSessionHistoryId;

    private String custRelationNum;
    private String userId = "";
    private Date startTime;
    private Date endTime;
    private Date validationTime;    //Naushad Added validation time on 4/Sep/2013
    private String fullName;
    private String documentId;
    private String documentType;

    public AgentSessionHistory() {
    }

    public String getAgentSessionHistoryId() {
        return this.agentSessionHistoryId;
    }

    public void setAgentSessionHistoryId(String agentSessionHistoryId) {
        this.agentSessionHistoryId = agentSessionHistoryId;
    }

    public String getSessionId() {
        return this.sessionId;
    }


    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }


    public String getCustRelationNum() {
        return this.custRelationNum;
    }

    public void setCustRelationNum(String custRelationNum) {
        this.custRelationNum = custRelationNum;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    //Naushad Added validation time on 4/Sep/2013
    public Date getValidationTime() {
        return this.validationTime;
    }

    public void setValidationTime(Date validationTime) {
        this.validationTime = validationTime;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }


}
