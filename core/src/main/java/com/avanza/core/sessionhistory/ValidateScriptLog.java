package com.avanza.core.sessionhistory;

import java.util.Date;

import com.avanza.core.data.DbObject;

/**
 * ValidateScriptLog entity.
 *
 * @author MyEclipse Persistence Tools
 */

public class ValidateScriptLog extends DbObject {

    // Fields

    /**
     *
     */
    private static final long serialVersionUID = 4085495321276225208L;

    private String scriptLogId;
    private ValidateScript validateScript;
    private IdentityType identityType;
    private String identityTypeId;
    private String sessionHistory;
    private int correctAnswered;
    private int incorrectAnsered;
    private int notAnswered;
    private String identityNumber;
    private String notes;

    // Constructors

    /**
     * default constructor
     */
    public ValidateScriptLog() {
    }

    /**
     * minimal constructor
     */
    public ValidateScriptLog(IdentityType identityType,
                             String sessionHistory, Byte correctAnswered,
                             Byte incorrectAnsered, Byte notAnswered, String createdBy,
                             Date updatedOn, String updatedBy) {
        this.identityType = identityType;
        this.sessionHistory = sessionHistory;
        this.correctAnswered = correctAnswered;
        this.incorrectAnsered = incorrectAnsered;
        this.notAnswered = notAnswered;

    }

    /**
     * full constructor
     */
    public ValidateScriptLog(ValidateScript validateScript,
                             IdentityType identityType, String sessionHistory,
                             Byte correctAnswered, Byte incorrectAnsered, Byte notAnswered,
                             String identityNumber, String notes, Date createdOn,
                             String createdBy, Date updatedOn, String updatedBy) {
        this.validateScript = validateScript;
        this.identityType = identityType;
        this.sessionHistory = sessionHistory;
        this.correctAnswered = correctAnswered;
        this.incorrectAnsered = incorrectAnsered;
        this.notAnswered = notAnswered;
        this.identityNumber = identityNumber;
        this.notes = notes;

    }

    // Property accessors


    public ValidateScript getValidateScript() {
        return this.validateScript;
    }

    public void setValidateScript(ValidateScript validateScript) {
        this.validateScript = validateScript;
    }

    public IdentityType getIdentityType() {
        return this.identityType;
    }

    public void setIdentityType(IdentityType identityType) {
        this.identityType = identityType;
    }

    public String getSessionHistory() {
        return this.sessionHistory;
    }

    public void setSessionHistory(String sessionHistory) {
        this.sessionHistory = sessionHistory;
    }

    public int getCorrectAnswered() {
        return this.correctAnswered;
    }

    public void setCorrectAnswered(int correctAnswered) {
        this.correctAnswered = correctAnswered;
    }

    public int getIncorrectAnsered() {
        return this.incorrectAnsered;
    }

    public void setIncorrectAnsered(int incorrectAnsered) {
        this.incorrectAnsered = incorrectAnsered;
    }

    public int getNotAnswered() {
        return this.notAnswered;
    }

    public void setNotAnswered(int notAnswered) {
        this.notAnswered = notAnswered;
    }

    public String getIdentityNumber() {
        return this.identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getScriptLogId() {
        return scriptLogId;
    }

    public void setScriptLogId(String scriptLogId) {
        this.scriptLogId = scriptLogId;
    }

    public String getIdentityTypeId() {
        return identityTypeId;
    }

    public void setIdentityTypeId(String identityTypeId) {
        this.identityTypeId = identityTypeId;
    }


}