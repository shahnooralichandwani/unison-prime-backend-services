package com.avanza.core.sessionhistory;

import com.avanza.core.callscript.CallLog;
import com.avanza.core.callscript.CallScriptLog;
import com.avanza.core.data.DbObject;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author kraza
 */
public class SessionHistory extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = -2714507844039427115L;


    private String sessionId;
    private String channelId;
    private String custRelationNum;
    private String userId = "";
    private Date startTime;
    private Date endTime;
    private Date validationTime;    //Naushad Added validation time on 4/Sep/2013
    private String chanelRefId;
    private Set<CallScriptLog> callScriptLogs = new HashSet<CallScriptLog>(0);
    private Set<CallLog> callLogs = new HashSet<CallLog>(0);
    private Set<SessionHistoryDetail> sessionHistoryDetails = new HashSet<SessionHistoryDetail>(0);
    private InterfaceChannel interfaceChannel;
    private String interactionOutcome;
    private Byte isValidated = 0;
    private String validationType = "";
    private Set<ValidateScriptLog> validateScriptLogs = new HashSet<ValidateScriptLog>(0);
    private String fullName;
    private String branchCode;
    private String segmentCode;
    private int histiryDetCount;
    private String callNotes;
    private String cautionBox;
    private boolean cautionBoxFlag;
    private String cli;
    private String ivrAttachedData;
    //Added for ABL :Anam Siddiqi
    private String activitySessionKey;


    public void addHistoryDetails(SessionHistoryDetail details) {
        details.setActionSeqNum(sessionHistoryDetails.size() + 1);
        details.setSno((short) (sessionHistoryDetails.size() + 1));
        details.setSerNo((short) (sessionHistoryDetails.size() + 1));
        sessionHistoryDetails.add(details);
    }

    public void removeHistoryDetails(SessionHistoryDetail details) {
        sessionHistoryDetails.remove(details);
    }

    public SessionHistory() {
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public String getcautionBox() {
        return cautionBox;
    }

    public void setcautionBoxFlag(boolean cautionBoxFlag) {
        this.cautionBoxFlag = cautionBoxFlag;
    }

    public boolean getcautionBoxFlag() {
        return cautionBoxFlag;
    }

    public void setcautionBox(String cautionBox) {
        this.cautionBox = cautionBox;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public InterfaceChannel getInterfaceChannel() {
        return this.interfaceChannel;
    }

    public void setInterfaceChannel(InterfaceChannel interfaceChannel) {
        this.interfaceChannel = interfaceChannel;
    }

    public String getCustRelationNum() {
        return this.custRelationNum;
    }

    public void setCustRelationNum(String custRelationNum) {
        this.custRelationNum = custRelationNum;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    //Naushad Added validation time on 4/Sep/2013
    public Date getValidationTime() {
        return this.validationTime;
    }

    public void setValidationTime(Date validationTime) {
        this.validationTime = validationTime;
    }

    public String getChanelRefId() {
        return this.chanelRefId;
    }

    public void setChanelRefId(String chanelRefId) {
        this.chanelRefId = chanelRefId;
    }

    public Set<CallScriptLog> getCallScriptLogs() {
        return this.callScriptLogs;
    }

    public void setCallScriptLogs(Set<CallScriptLog> callScriptLogs) {
        this.callScriptLogs = callScriptLogs;
    }

    public Set<CallLog> getCallLogs() {
        return this.callLogs;
    }

    public void setCallLogs(Set<CallLog> callLogs) {
        this.callLogs = callLogs;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Byte getIsValidated() {
        return isValidated;
    }

    public void setIsValidated(Byte isValidated) {
        this.isValidated = isValidated;
    }

    public String getValidationType() {
        return validationType;
    }

    public void setValidationType(String validationType) {
        this.validationType = validationType;
    }

    public Set<ValidateScriptLog> getValidateScriptLogs() {
        return validateScriptLogs;
    }

    public void setValidateScriptLogs(Set<ValidateScriptLog> validateScriptLogs) {
        this.validateScriptLogs = validateScriptLogs;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getSegmentCode() {
        return segmentCode;
    }

    public void setSegmentCode(String segmentCode) {
        this.segmentCode = segmentCode;
    }

    public int getHistiryDetCount() {
        return histiryDetCount;
    }

    public void setHistiryDetCount(int histiryDetCount) {
        this.histiryDetCount = histiryDetCount;
    }

    public void setSessionHistoryDetails(
            Set<SessionHistoryDetail> sessionHistoryDetails) {
        this.sessionHistoryDetails = sessionHistoryDetails;
    }

    public Set<SessionHistoryDetail> getSessionHistoryDetails() {
        return sessionHistoryDetails;
    }

    public String getCallNotes() {
        return callNotes;
    }

    public void setCallNotes(String callNotes) {
        this.callNotes = callNotes;
    }

    public String getInteractionOutcome() {
        return interactionOutcome;
    }

    public void setInteractionOutcome(String interactionOutcome) {
        this.interactionOutcome = interactionOutcome;
    }

    public String getCli() {
        return cli;
    }

    public void setCli(String cli) {
        this.cli = cli;
    }

    public String getIvrAttachedData() {
        return ivrAttachedData;
    }

    public void setIvrAttachedData(String ivrAttachedData) {
        this.ivrAttachedData = ivrAttachedData;
    }


    //Added for current activity logging:Anam Siddiqi
    public String getActivitySessionKey() {
        return activitySessionKey;
    }

    public void setActivitySessionKey(String activitySessionKey) {
        this.activitySessionKey = activitySessionKey;
    }
}
