package com.avanza.core.sessionhistory;

/**
 * The action status codes that mapped from the sessionhistory detail table.
 *
 * @author kraza
 */
public enum ActionStatus {

    BeginSession(0), EndSession(1), Unkonwn(2),
    DocumentAdded(3), DocumentUpdated(4), DocumentAddFailure(5), DocumentUpdateFailure(6), Searched(8),
    ValidationFailed(7);

    private static final String BEGIN_SESSION = "beginsession";
    private static final String END_SESSION = "endsession";
    private static final String UNKNOWN = "unknown";
    private static final String DOCUMENT_ADDED = "documentadded";
    private static final String DOCUMENT_UPDATED = "documentupdated";
    private static final String DOCUMENT_ADD_FAILURE = "documentaddfailure";
    private static final String DOCUMENT_UPDATE_FAILURE = "documentaddfailure";
    private static final String VALIDATION_FAILED = "validationfailed";
    private static final String SEARCHED = "searched";

    private int value;

    private ActionStatus(int value) {

        this.value = value;
    }

    public int toInteger() {

        return this.value;
    }

    public static ActionStatus parse(String typeValue) {

        if (ActionStatus.BEGIN_SESSION.equalsIgnoreCase(typeValue))
            return ActionStatus.BeginSession;
        else if (ActionStatus.END_SESSION.equalsIgnoreCase(typeValue))
            return ActionStatus.EndSession;
        else if (ActionStatus.UNKNOWN.equalsIgnoreCase(typeValue))
            return ActionStatus.Unkonwn;
        else if (ActionStatus.DOCUMENT_ADDED.equalsIgnoreCase(typeValue))
            return ActionStatus.DocumentAdded;
        else if (ActionStatus.DOCUMENT_UPDATED.equalsIgnoreCase(typeValue))
            return ActionStatus.DocumentUpdated;
        else if (ActionStatus.DOCUMENT_ADD_FAILURE.equalsIgnoreCase(typeValue))
            return ActionStatus.DocumentAddFailure;
        else if (ActionStatus.DOCUMENT_UPDATE_FAILURE.equalsIgnoreCase(typeValue))
            return ActionStatus.DocumentUpdateFailure;
        else if (ActionStatus.VALIDATION_FAILED.equalsIgnoreCase(typeValue))
            return ActionStatus.ValidationFailed;
        else if (ActionStatus.SEARCHED.equalsIgnoreCase(typeValue))
            return ActionStatus.Searched;

        throw new IllegalArgumentException(String.format("[%1$s] is not identified as valid ActionStatus value.", typeValue));
    }

    public static ActionStatus fromInt(int value) {

        ActionStatus retVal;

        switch (value) {

            case 0:
                retVal = ActionStatus.BeginSession;
                break;
            case 1:
                retVal = ActionStatus.EndSession;
                break;
            case 2:
                retVal = ActionStatus.Unkonwn;
                break;
            case 3:
                retVal = ActionStatus.DocumentAdded;
                break;
            case 4:
                retVal = ActionStatus.DocumentUpdated;
                break;
            case 5:
                retVal = ActionStatus.DocumentAddFailure;
                break;
            case 6:
                retVal = ActionStatus.DocumentUpdateFailure;
                break;
            case 7:
                retVal = ActionStatus.ValidationFailed;
                break;
            case 8:
                retVal = ActionStatus.Searched;
                break;
            default:
                throw new IllegalArgumentException(String.format("[%1$d] is not identified as valid ActionStatus value.", value));
        }

        return retVal;
    }

    @Override
    public String toString() {
        switch (value) {
            case 0:
                return "Begin Session";
            case 1:
                return "End Session";
            case 2:
                return "Unkonwn";
            case 3:
                return "Document Added";
            case 4:
                return "Document Updated";
            case 5:
                return "Document Add Failure";
            case 6:
                return "Document Update Failure";
            case 7:
                return "Validation Failed";
            case 8:
                return "Searched";
            default:
                throw new IllegalArgumentException(String.format("[%1$d] is not identified as valid ActionStatus value.", value));
        }
    }
}