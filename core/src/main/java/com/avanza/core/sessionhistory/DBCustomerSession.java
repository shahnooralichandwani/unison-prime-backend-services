package com.avanza.core.sessionhistory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.avanza.core.CoreException;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.HibernateDataBroker;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.OrderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.meta.MetaCounter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;

/**
 * This class provides the DB access methods for the session History and session history detail.
 *
 * @author kraza
 */

public class DBCustomerSession {


    private static final Logger logger = Logger.getLogger(DBCustomerSession.class);
    private static final String EMPTY = "";

    private static DBCustomerSession _instance = new DBCustomerSession();
    public HibernateDataBroker broker;
    private List<ValidateScript> questionsList = null;
    private List<IdentityType> idTypeList = null;
    private List<InteractionOutcome> callOutComes = null;

    protected DBCustomerSession() {
        broker = (HibernateDataBroker) DataRepository.getBroker(SessionHistory.class.getName());
    }

    public static DBCustomerSession getInstance() {

        return _instance;
    }

    public List<InteractionOutcome> loadCallOutcomes() {

        try {
            if (callOutComes == null) {
                callOutComes = broker.findAll(InteractionOutcome.class);
            }
        } catch (Exception e) {
            logger.LogException("Exception occoured while creating customerSession at DBCustomerSession.createSession.", e);
            throw new CoreException("Exception occoured while creating customerSession at DBCustomerSession.createSession.", e);
        }
        return callOutComes;
    }

    public void createSession(SessionHistory sessionHistory) {

        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        this.broker.getSession();
        txContext.beginTransaction();

        try {
            broker.add(sessionHistory);
            txContext.commit();
        } catch (Exception e) {
            logger.logError("Exception occoured while creating customerSession at DBCustomerSession.createSession.");
            e.printStackTrace();
            txContext.rollback();
            throw new CoreException("Exception occoured while creating customerSession at DBCustomerSession.createSession.", e);
        }
    }

    public void updateSession(SessionHistory sessionHistory) {

        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        this.broker.getSession();
        txContext.beginTransaction();

        try {
            broker.update(sessionHistory);
            txContext.commit();
        } catch (Exception e) {
            logger.logError("Exception occoured while updating customerSession at DBCustomerSession.updatingSession.");
            e.printStackTrace();
            txContext.rollback();
            throw new CoreException("Exception occoured while updating customerSession at DBCustomerSession.updatingSession.", e);
        }
    }

    /**
     * This method gets the last sessionHistory of the customer.
     *
     * @param custRelNo
     * @return
     */
    public SessionHistory getLastSessionFor(String custRelNo) {

        List<SessionHistory> sessionHistory = null;

        Search query = new Search(SessionHistory.class);
        query.addCriterion(Criterion.equal("custRelationNum", custRelNo));
        query.addOrder("startTime", OrderType.ASC);

        sessionHistory = this.broker.find(query);

        if (sessionHistory != null && sessionHistory.isEmpty())
            return sessionHistory.get(0);

        return null;
    }

    public SessionHistory getSessionById(String sessionId) {

        return this.broker.findById(SessionHistory.class, sessionId);
    }

    /**
     * Gets the collection of the session History Details.
     *
     * @param sessionId
     * @return
     */

    public List<ValidateScript> getCustomerValidationQuestions() {

        List<ValidateScript> newQuestionsList = new ArrayList<ValidateScript>();
        try {
            if (true) {//questionsList == null) {
                Search query = new Search(ValidateScript.class);
                query.addOrder("displayOrder", OrderType.ASC);
                questionsList = this.broker.find(query);
            }

            for (ValidateScript script : questionsList) {

                ValidateScript newscript = (ValidateScript) script.clone();

                if (StringHelper.isNotEmpty(newscript.getAnswerExpression()))
                    newscript.setAnswer((String) ExpressionHelper.getExpressionResult(newscript.getAnswerExpression(), String.class));
                else
                    newscript.setAnswer(StringHelper.EMPTY);
                newQuestionsList.add(newscript);
            }
        } catch (Exception e) {
            logger.LogException("Exception occoured while getting validation validation at DBCustomerSession.getCustomerValidationQuestions.", e);
            throw new CoreException("Exception occoured while validation validation at DBCustomerSession.getCustomerValidationQuestions.", e);
        }

        return newQuestionsList;
    }

    public List<IdentityType> getCustomerIdentityTypeList() {

        try {
            if (idTypeList == null) {
                Search query = new Search(IdentityType.class);
                idTypeList = this.broker.find(query);
            }
        } catch (Exception e) {
            logger.LogException("Exception occoured while getting IdentityType at DBCustomerSession.getCustomerValidationQuestions.", e);
            throw new CoreException("Exception occoured while IdentityType at DBCustomerSession.getCustomerValidationQuestions.", e);
        }

        return idTypeList;
    }

    public SessionHistory logValidateScriptLogs(SessionHistory sessionHistory, List<ValidateScript> list) {

        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        this.broker.getSession();
        txContext.beginTransaction();

        for (ValidateScript eachScript : list) {

            ValidateScriptLog log = new ValidateScriptLog();
            log.setCorrectAnswered(Integer.parseInt(
                    ValidationResult.Correct.toString().equalsIgnoreCase(eachScript.getResult()) ? "1" : "0"));
            log.setIncorrectAnsered(Integer.parseInt(
                    ValidationResult.Incorrect.toString().equalsIgnoreCase(eachScript.getResult()) ? "1" : "0"));
            log.setNotAnswered(Integer.parseInt(
                    ValidationResult.NotAnswered.toString().equalsIgnoreCase(eachScript.getResult()) ? "1" : "0"));
            log.setValidateScript(eachScript);
            log.setSessionHistory(sessionHistory.getSessionId());
            log.setScriptLogId(MetaDataRegistry.getNextCounterValue(MetaCounter.VALIDATE_LOG_COUNTER).toString());

            try {
                broker.add(log);
                txContext.commit();
            } catch (Exception e) {
                logger.LogException("Exception in ValidateScripts Logging.", e);
                txContext.rollback();
            }
        }
        return sessionHistory;
    }

    public ValidateScriptLog logIdentityTypeLogs(SessionHistory sessionHistory, int index, String result) {

        ValidateScriptLog log = new ValidateScriptLog();
        try {
            log.setCorrectAnswered(Integer.parseInt(ValidationResult.Correct.toString().equalsIgnoreCase(result) ? "1" : "0"));
            log.setIncorrectAnsered(Integer.parseInt(ValidationResult.Incorrect.toString().equalsIgnoreCase(result) ? "1" : "0"));
            log.setNotAnswered(Integer.parseInt(ValidationResult.NotAnswered.toString().equalsIgnoreCase(result) ? "1" : "0"));
            log.setSessionHistory(sessionHistory.getSessionId());
            log.setScriptLogId(MetaDataRegistry.getNextCounterValue(MetaCounter.VALIDATE_LOG_COUNTER).toString());

            IdentityType type = broker.findById(IdentityType.class, String.valueOf(index));
            log.setIdentityType(type);
            log.setIdentityTypeId(type.getIdentityTypeId());

        } catch (Exception e) {
            logger.LogException("Exception in ValidateScripts Logging - IdentityType Logging.", e);
        }
        return log;
    }

    public List<SessionHistoryDetail> getSessionHistoryDetails(String sessionId) {
        return getSessionHistoryDetails(sessionId, false);
    }

    public List<SessionHistoryDetail> getSessionHistoryDetails(String sessionId, boolean isExternal) {
        List<SessionHistoryDetail> shDetails = null;
        Search query = new Search(SessionHistoryDetail.class);
        query.addCriterion(Criterion.equal("sessionId", sessionId));
        //query.addOrder("isExternal", OrderType.ASC);
        shDetails = this.broker.find(query);
        return shDetails;
    }

    public List<SessionHistoryDetail> getSessionHistoryDetailsFor(String custRelNo) {

        return this.getSessionHistoryDetails(
                this.getLastSessionFor(custRelNo).getSessionId());
    }

    /**
     * Add the detail to session history.
     *
     * @param sessionHistoryDetail
     */
    public void addSessionHistoryDetail(SessionHistoryDetail sessionHistoryDetail) {

        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        this.broker.getSession();
        txContext.beginTransaction();

        try {

            broker.saveOrUpdate(sessionHistoryDetail);
            txContext.commit();
        } catch (Exception e) {
            logger.logError("Exception occoured while adding sessionHistoryDetail at DBCustomerSession.createSession.");
            e.printStackTrace();
            txContext.rollback();
            throw new CoreException("Exception occoured while adding sessionHistoryDetail at DBCustomerSession.createSession.", e);
        }
    }

    public void deleteSessionHistoryDetail(SessionHistoryDetail sessionHistoryDetail) {

        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        this.broker.getSession();
        txContext.beginTransaction();

        try {
            List<SessionHistoryDetail> detail = null;
            Search query = new Search(SessionHistoryDetail.class);
            query.addCriterion(Criterion.equal("sessionId", sessionHistoryDetail.getSessionId()));
            query.addCriterion(Criterion.equal("actionSeqNum", sessionHistoryDetail.getActionSeqNum()));
            detail = this.broker.find(query);
            if (detail.size() > 0) {
                broker.delete(detail.get(0));
                txContext.commit();
            }
        } catch (Exception e) {
            logger.LogException("Exception occoured while deleting sessionHistoryDetail at DBCustomerSession.createSession.", e);
            txContext.rollback();
            throw new CoreException("Exception occoured while deleting sessionHistoryDetail at DBCustomerSession.createSession.", e);
        }
    }

    public void deleteAllActivities(List<SessionHistoryDetail> activityList) {
        try {
            for (SessionHistoryDetail sessionHistoryDetail : activityList) {

                List<SessionHistoryDetail> detail = null;
                Search query = new Search(SessionHistoryDetail.class);
                query.addCriterion(Criterion.equal("sessionId", sessionHistoryDetail.getSessionId()));
                query.addCriterion(Criterion.equal("actionSeqNum", sessionHistoryDetail.getActionSeqNum()));
                detail = this.broker.find(query);
                if (detail.size() > 0) {
                    broker.delete(detail.get(0));
                }

            }
        } catch (Exception e) {
            logger.LogException("Exception occoured while deleting sessionHistoryDetail at DBCustomerSession.createSession.", e);
            throw new CoreException("Exception occoured while deleting sessionHistoryDetail at DBCustomerSession.createSession.", e);
        }
    }

    public void closeSessionInTransaction(SessionHistory sessionHistory, Set<SessionHistoryDetail> details) {
        logger.logInfo("Start Persisting End Session and Activities for Customer: " + sessionHistory.getCustRelationNum());
        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        this.broker.getSession();
        txContext.beginTransaction();
        String flowStatus = "0";
        SessionHistoryDetail sessionHisDet = null;
        try {
            flowStatus = "1";
            if (StringHelper.isEmpty(sessionHistory.getInteractionOutcome()))
                sessionHistory.setInteractionOutcome("000");
            broker.saveOrUpdate(sessionHistory);//broker.add(sessionHistory);
            flowStatus = "2";
            for (SessionHistoryDetail sessionHistoryDetail : details) {
                sessionHisDet = sessionHistoryDetail;
                broker.saveOrUpdate(sessionHistoryDetail);
            }
            flowStatus = "3";
            /*if (sessionHistory.getValidateScriptLogs() != null) {
            	flowStatus = "4";
	            for( ValidateScriptLog eachScript : sessionHistory.getValidateScriptLogs() ){
	                 broker.add( eachScript );
	            }
            }*/

            flowStatus = "5";
            txContext.commit();
            logger.logInfo("End Persisting End Session and Activities for Customer: " + sessionHistory.getCustRelationNum());
        } catch (Exception e) {
            if (sessionHistory != null && sessionHisDet != null) {
                logger.logInfo("Exception occoured in closeSessionInTransaction() at flow : " + flowStatus + " for Customer " + sessionHistory.getCustRelationNum()
                        + " with sessionHistoryId: " + sessionHistory.getFullName() + ", Activity Detail: " + sessionHisDet.getActivityDescription(), e);
            }
            txContext.rollback();
            //throw new CoreException("Exception occoured while creating customerSession at DBCustomerSession.createSession.", e);
        }
    }

    public ActivityType findActivityTypeBy(String id) {

        return broker.findById(ActivityType.class, id);
    }

    public void persistActivityTypeBy(ActivityType activityType) {

        broker.persist(activityType);
    }
}
