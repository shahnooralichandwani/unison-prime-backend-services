package com.avanza.core.sessionhistory;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;

import com.avanza.core.interceptor.Auditable;
import com.avanza.core.meta.ValueTreeNodeBase;
import com.avanza.core.meta.audit.ActivityTypeAudit;
import com.avanza.core.meta.audit.ActivityTypeAuditId;
import com.avanza.core.meta.audit.OrgRoleAudit;
import com.avanza.core.meta.audit.OrgRoleAuditId;
import com.avanza.core.util.StringHelper;

/**
 * @author Mehran Junejo
 */
public class ActivityType extends ValueTreeNodeBase implements Comparable<ActivityType>, Auditable {

    private static final long serialVersionUID = 5320150591356156160L;
    public static final String BEGIN_SESSION = "beginSession";
    public static final String END_SESSION = "endSession";
    public static final String DOCUMENT = "document";
    public static final String DOCUMENT_SEARCH = "documentSearch";
    private String activityTypeId;
    private Set<SessionHistoryDetail> sessionHistoryDetails = new HashSet<SessionHistoryDetail>(0);
    private String otherCode;
    private String docTypeId;
    private String typeDescription; // descriptions
    private boolean isExternal;
    private String operType;
    private String systemName;
    private String parentOne;
    private String parentTwo;
    private String parentThree;
    private String parentFour;
    private boolean isEnabled;
    private boolean isAutoWrapUp;


    public ActivityType() {
        activityTypeId = StringHelper.EMPTY;
        setDisplayOrder(0L);
    }

    public ActivityType(String id) {
        super(id, StringHelper.EMPTY);
    }

    public String getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.id = activityTypeId;
        this.activityTypeId = activityTypeId;
    }

    public Set<SessionHistoryDetail> getSessionHistoryDetails() {
        return sessionHistoryDetails;
    }

    public void setSessionHistoryDetails(Set<SessionHistoryDetail> sessionHistoryDetails) {
        this.sessionHistoryDetails = sessionHistoryDetails;
    }

    public String getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeId(String docTypeId) {
        this.docTypeId = docTypeId;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public boolean getIsExternal() {
        return isExternal;
    }

    public void setIsExternal(boolean isExternal) {
        this.isExternal = isExternal;
    }

    public String getOperType() {
        return operType;
    }

    public void setOperType(String operType) {
        this.operType = operType;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getOtherCode() {
        return otherCode;
    }

    public void setOtherCode(String otherCode) {
        this.otherCode = otherCode;
    }

    public int compareTo(ActivityType obj) {

        if (this.getDisplayOrder() > obj.getDisplayOrder())
            return 1;
        else
            return -1;
    }

    public String toString() {
        return activityTypeId;
    }

    public String getParentOne() {
        return parentOne;
    }

    public String getParentTwo() {
        return parentTwo;
    }

    public String getParentThree() {
        return parentThree;
    }

    public String getParentFour() {
        return parentFour;
    }

    public void setParentOne(String parentOne) {
        this.parentOne = parentOne;
    }

    public void setParentTwo(String parentTwo) {
        this.parentTwo = parentTwo;
    }

    public void setParentThree(String parentThree) {
        this.parentThree = parentThree;
    }

    public void setParentFour(String parentFour) {
        this.parentFour = parentFour;
    }

    public boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public boolean getIsAutoWrapUp() {
        return isAutoWrapUp;
    }

    public void setIsAutoWrapUp(boolean isAutoWrapUp) {
        this.isAutoWrapUp = isAutoWrapUp;
    }


    @Override
    public Object getAuditEntry(Short revtype) {
        ActivityTypeAudit activityTypeAudit = null;
        try {


            Random rand = new Random();

            activityTypeAudit = new ActivityTypeAudit();
            activityTypeAudit.setRevtype(revtype);

            BeanUtils.copyProperties(activityTypeAudit, this);
            activityTypeAudit.setActivityTypeAuditId(new ActivityTypeAuditId(this.activityTypeId, rand.nextInt()));

            //secUserV.setId(new SecUserVId(this.loginId, CounterUtils.getNextAuditTableKey()));
        } catch (Exception e) {
            activityTypeAudit = null;
        }
        return activityTypeAudit;
    }


}