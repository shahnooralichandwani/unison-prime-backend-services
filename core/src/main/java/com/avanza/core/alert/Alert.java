package com.avanza.core.alert;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DbObject;
import com.avanza.core.security.User;
import com.avanza.core.web.config.WebContext;

public class Alert extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = -1894428007648489693L;

    private long alertId;
    private String subjectPrm;
    private String subjectSec;
    private String descriptionPrm;
    private String descriptionSec;

    private String alertFrom;
    private String alertType;
    private Date occourenceTime;
    private Set<AlertUser> alertUsers = new HashSet<AlertUser>(0);

    public String getShortDescPrm() {
        String temp = this.descriptionPrm;
        int descLength = this.descriptionPrm.length();

        if (descLength > 30) {
            temp = descriptionPrm.substring(0, 30);
            temp = temp.concat("...");
        }

        return temp;
    }

    public String getShortSubjectPrm() {
        String temp = subjectPrm;
        int descLength = this.subjectPrm.length();

        if (descLength > 20) {
            temp = subjectPrm.substring(0, 20);
            temp = temp.concat("...");
        }

        return temp;
    }

    public String getShortSubjectSec() {

        return "";
    }

    public String getShortDescSec() {
        return "";
    }

    public long getAlertId() {
        return this.alertId;
    }

    public void setAlertId(long alertId) {
        this.alertId = alertId;
    }

    public String getSubjectPrm() {
        return this.subjectPrm;
    }

    public void setSubjectPrm(String subjectPrm) {
        this.subjectPrm = subjectPrm;
    }

    public String getSubjectSec() {
        return this.subjectSec;
    }

    public void setSubjectSec(String subjectSec) {
        this.subjectSec = subjectSec;
    }

    public String getDescriptionPrm() {
        return this.descriptionPrm;
    }

    public void setDescriptionPrm(String descriptionPrm) {
        this.descriptionPrm = descriptionPrm;
    }

    public String getDescriptionSec() {
        return this.descriptionSec;
    }

    public void setDescriptionSec(String descriptionSec) {
        this.descriptionSec = descriptionSec;
    }

    public String getAlertFrom() {
        return this.alertFrom;
    }

    public void setAlertFrom(String alertFrom) {
        this.alertFrom = alertFrom;
    }

    public String getAlertType() {
        return this.alertType;
    }

    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }

    public Date getOccourenceTime() {
        return this.occourenceTime;
    }

    public void setOccourenceTime(Date occourenceTime) {
        this.occourenceTime = occourenceTime;
    }

    public Set<AlertUser> getAlertUsers() {
        return this.alertUsers;
    }

    public void setAlertUsers(Set<AlertUser> alertUsers) {
        this.alertUsers = alertUsers;
    }

    public boolean getReadStatus() {

        WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
        User user = ctx.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);

        String currentId = user.getLoginId();

        for (AlertUser aUser : alertUsers) {

            if (aUser.getUser().getLoginId().equals(currentId))
                return aUser.isRead();

        }
        return false;
    }

}
