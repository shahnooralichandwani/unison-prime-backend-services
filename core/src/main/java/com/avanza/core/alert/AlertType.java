package com.avanza.core.alert;


public enum AlertType {

    InternalSystem("InternalSystem"), ExternalSystem("ExternalSystem");

    private String value;

    private AlertType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
