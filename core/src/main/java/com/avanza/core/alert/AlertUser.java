package com.avanza.core.alert;

import com.avanza.core.data.DbObject;
import com.avanza.core.security.User;
import com.avanza.core.security.db.UserDbImpl;

public class AlertUser extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 4642675908323901646L;

    private long alertId;
    private String alertTo;
    private UserDbImpl user;
    private Alert alert;
    private boolean read;

    public long getAlertId() {
        return alertId;
    }

    public void setAlertId(long alertId) {
        this.alertId = alertId;
    }

    public String getAlertTo() {
        return alertTo;
    }

    public void setAlertTo(String alertTo) {
        this.alertTo = alertTo;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(UserDbImpl user) {
        this.user = user;
    }

    public Alert getAlert() {
        return this.alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }

    public boolean isRead() {
        return this.read;
    }


    public void setRead(boolean isRead) {
        this.read = isRead;
    }
}
