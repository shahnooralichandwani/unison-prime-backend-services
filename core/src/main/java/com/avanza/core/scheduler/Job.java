package com.avanza.core.scheduler;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


public interface Job extends org.quartz.Job {

    public void execute(JobExecutionContext arg0) throws JobExecutionException;
}
