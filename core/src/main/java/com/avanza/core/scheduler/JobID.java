package com.avanza.core.scheduler;

import com.avanza.core.main.ClustersConfigLoader;
import com.avanza.core.util.StringHelper;

public class JobID {

    private String id;

    public JobID(String id) {
        this.id = id;
    }

    public String getID() {
        return this.id;
    }

    @Override
    public String toString() {
        String name = ClustersConfigLoader.getCurrentClusterInfo().getName();
        if (StringHelper.isEmpty(name))
            name = "prefix";
        return name + "|" + id;
    }
}
