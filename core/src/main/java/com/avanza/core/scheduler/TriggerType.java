package com.avanza.core.scheduler;


public enum TriggerType {
    HourlyTrigger, MinutelyTrigger, SimpleTrigger;
}
