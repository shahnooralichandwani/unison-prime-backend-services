package com.avanza.core.scheduler;


public interface JobExecutionContext {

    public Object getProperty(String key);
}
