package com.avanza.core.scheduler.quartz;

import java.util.Date;

import org.quartz.JobDataMap;
import org.quartz.ScheduleBuilder;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.quartz.TriggerUtils;

import sun.rmi.runtime.NewThreadAction;

import com.avanza.core.scheduler.Trigger;

class QuartzTrigger implements Trigger {

    org.quartz.Trigger quartzTrigger = null;

    public void createHourlyTrigger(String name, String group, int hours) {
        quartzTrigger = TriggerBuilder.newTrigger().withIdentity(name, group)
                .withSchedule(SimpleScheduleBuilder.repeatHourlyForever(hours))
                .build();

    }

    public void createMinutelyTrigger(String name, String group, int minutes) {
        quartzTrigger = TriggerBuilder.newTrigger().withIdentity(name, group)
                .withSchedule(
                        SimpleScheduleBuilder.repeatMinutelyForever(minutes))
                .build();
    }

    public void createTrigger(String name, String group, Date date) {
        quartzTrigger = TriggerBuilder.newTrigger().withIdentity(name, group)
                .startAt(date).build();
    }

    public void createTrigger(String name, String group, int repeatCount,
                              long repeatInterval) {
        createTrigger(name, group, repeatCount, repeatInterval, 5);
    }

    public void createTrigger(String name, String group, int repeatCount,
                              long repeatInterval, int priority) {
        // repeatCount = 0 means, trigger will fire once.
		
		/*
		TriggerBuilder<org.quartz.Trigger> trgBuliler= TriggerBuilder.newTrigger().withIdentity(name, group)
		.withPriority(priority);
		quartzTrigger = trgBuliler.startNow().build();
		*/

        quartzTrigger = TriggerBuilder.newTrigger().withIdentity(name, group)
                .withPriority(priority).withSchedule(
                        SimpleScheduleBuilder.simpleSchedule()
                                .withIntervalInMilliseconds(repeatInterval)
                                .withRepeatCount(repeatCount)).build();

    }

    public Object getTrigger() {
        return quartzTrigger;
    }
}
