package com.avanza.core.scheduler;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import com.avanza.core.CoreException;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.HibernateDataBroker;
import com.avanza.core.data.db.DbInfo;
import com.avanza.core.data.db.StartupDatabase;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.reader.XMLDocumentReader;

public class SchedulerCatalog {

    private static HashMap<String, Scheduler> systemSchedulers = new HashMap<String, Scheduler>(0);
    private static Scheduler defaultScheduler;
    private static Scheduler defaultRAMScheduler;
    private static Scheduler defaultDBScheduler;
    private static String XML_Scheduler = "Scheduler";
    private static String XML_Scheduler_Class = "class";
    private static String XML_Scheduler_Key = "key";
    private static String XML_Scheduler_Default = "isdefault";
    private static String XML_Scheduler_RAM_Default = "isRAMDefault";
    private static String XML_Scheduler_DB_Default = "isDBDefault";
    private static String XML_Scheduler_Props = "prop";
    private static String XML_Scheduler_Props_Key = "key";
    private static String XML_Scheduler_Props_Value = "value";
    private static String XML_Scheduler_Properties = "Properties";
    private static String XML_Scheduler_data_source = "dataSource";

    public static void load(ConfigSection brokerSection) {
        List<ConfigSection> childList = brokerSection.getChildSections(SchedulerCatalog.XML_Scheduler);

        if ((childList != null) && (childList.size() > 0)) {
            for (int idx = 0; idx < childList.size(); idx++) {

                ConfigSection scheduler = childList.get(idx);
                String className = scheduler.getTextValue(SchedulerCatalog.XML_Scheduler_Class);
                String name = scheduler.getTextValue(SchedulerCatalog.XML_Scheduler_Key);
                boolean isDefault = scheduler.getValue(SchedulerCatalog.XML_Scheduler_Default,
                        false);
                boolean isRAMDefault = scheduler.getValue(
                        SchedulerCatalog.XML_Scheduler_RAM_Default, false);
                boolean isDBDefault = scheduler.getValue(SchedulerCatalog.XML_Scheduler_DB_Default,
                        false);

                String user = "", password = "";
                String connectionUri = "";
                String dataSource = scheduler.getValue(SchedulerCatalog.XML_Scheduler_data_source,
                        "");
                if (dataSource != "") {

                    DbInfo dbInfo = DataRepository.getDbInfo(dataSource);
                    user = dbInfo.getUser();
                    password = dbInfo.getPassword();
                    connectionUri = dbInfo.getConnectionUri();
                }
                try {
                    Properties map = new Properties();
                    ConfigSection propertiesSection = scheduler.getChild(SchedulerCatalog.XML_Scheduler_Properties);
                    if (propertiesSection != null) {
                        List<ConfigSection> properties = propertiesSection.getChildSections(SchedulerCatalog.XML_Scheduler_Props);

                        for (ConfigSection sec : properties) {
                            String key = sec.getTextValue(SchedulerCatalog.XML_Scheduler_Props_Key);
                            if (key.equalsIgnoreCase("org.quartz.dataSource." + name + ".user")) {
                                String value = user;
                                map.put(key, value);
                            } else if (key.equalsIgnoreCase("org.quartz.dataSource." + name + ".password")) {
                                String value = password;
                                map.put(key, value);
                            } else if (key.equalsIgnoreCase("org.quartz.dataSource." + name + ".URL")) {
                                String value = connectionUri;
                                map.put(key, value);
                            } else {
                                String value = sec.getValue(
                                        SchedulerCatalog.XML_Scheduler_Props_Value, "");
                                map.put(key, value);
                            }
                        }
                    }
                    Object schedulerObject;

                    schedulerObject = Class.forName(className).getConstructor(
                            new Class[]{Properties.class}).newInstance(new Object[]{map});
                    systemSchedulers.put(name,
                            (com.avanza.core.scheduler.Scheduler) schedulerObject);
                    if (isDefault) {
                        defaultScheduler = (com.avanza.core.scheduler.Scheduler) schedulerObject;
                    }

                    if (isRAMDefault) {
                        defaultRAMScheduler = (com.avanza.core.scheduler.Scheduler) schedulerObject;
                    }

                    if (isDBDefault) {
                        defaultDBScheduler = (com.avanza.core.scheduler.Scheduler) schedulerObject;
                    }

                } catch (ClassNotFoundException e) {
                    throw new CoreException(e, "Scheduler Class Not Found");
                } catch (InstantiationException e) {
                    throw new CoreException(e, "Scheduler Class Can Not Be Instanciated");
                } catch (IllegalAccessException e) {
                    throw new CoreException(e,
                            "Illegal Access Exception While Accessing Scheduler Class");
                } catch (IllegalArgumentException e) {
                    throw new CoreException(e,
                            "Illegal Arugment Exception While Initializing Scheduler Class");
                } catch (SecurityException e) {
                    throw new CoreException(e,
                            "Security Exception While Initializing Scheduler Class");
                } catch (InvocationTargetException e) {
                    throw new CoreException(e,
                            "Invocation Target Exception While Initializing Scheduler Class");
                } catch (NoSuchMethodException e) {
                    throw new CoreException(e,
                            "No Such Method Exception While Initializing Scheduler Class");
                }

            }
        }
    }

    public static Scheduler getScheduler() {
        return defaultScheduler;
    }

    public static Scheduler getRAMScheduler() {
        return defaultRAMScheduler;
    }

    public static Scheduler getDBScheduler() {
        // scheduling state will be kept in DB. so pending scheduled tasks can
        // be recovered in case of system crashed.
        // but it is slow in performance.
        return defaultDBScheduler;
    }

    public static Scheduler getScheduler(String key) {
        if (systemSchedulers.get(key) == null)
            return getScheduler();
        return systemSchedulers.get(key);
    }
}
