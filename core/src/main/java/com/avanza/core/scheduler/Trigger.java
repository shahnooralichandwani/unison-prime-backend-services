package com.avanza.core.scheduler;

import java.util.Date;


public interface Trigger {

    void createMinutelyTrigger(String name, String group, int minutes);

    void createHourlyTrigger(String name, String group, int hours);

    void createTrigger(String name, String group, Date date);

    void createTrigger(String name, String group, int repeatCount, long repeatInterval);

    Object getTrigger();

}
