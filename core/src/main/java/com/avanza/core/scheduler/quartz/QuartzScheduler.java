package com.avanza.core.scheduler.quartz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.calendar.AnnualCalendar;
import org.quartz.impl.matchers.GroupMatcher;

import com.avanza.core.CoreException;
import com.avanza.core.scheduler.JobDetail;
import com.avanza.core.scheduler.JobID;
import com.avanza.core.scheduler.Scheduler;
import com.avanza.core.scheduler.Trigger;

public class QuartzScheduler implements Scheduler {

    private SchedulerFactory sf = null;
    private org.quartz.Scheduler quartzScheduler;
    private String instanceName;
    private volatile long counter;
    private long constructedAt = (new Date()).getTime();

    public QuartzScheduler(Properties props) {
        try {
            instanceName = props
                    .getProperty("org.quartz.scheduler.instanceName");
            sf = new StdSchedulerFactory(props);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public void startScheduler() {
        if (quartzScheduler == null)
            try {
                quartzScheduler = sf.getScheduler();
                quartzScheduler.start();
                quartzScheduler.addCalendar("AvanzaCalendar",
                        new AnnualCalendar(), true, true);

            } catch (SchedulerException e) {
                throw new CoreException(e, "Unable To Start Scheduler");
            }
    }

    public void scheduleJob(JobDetail job, Trigger trigger) {
        startScheduler();
        try {
            quartzScheduler.scheduleJob((org.quartz.JobDetail) job
                    .getJobDetail(), (org.quartz.Trigger) trigger.getTrigger());
        } catch (SchedulerException e) {
            throw new CoreException(e, "Unable To Schedule Job");
        }
    }

    public void shutDownScheduler() {
        if (quartzScheduler != null)
            try {
                quartzScheduler.shutdown();
            } catch (SchedulerException e) {
                throw new CoreException(e, "Unable To Shutdown Scheduler");
            }
    }

    private HashMap getMapPopulatedWithId(HashMap map, String id) {
        // id will be auto generated if not provided in param.
        if (map == null) {
            map = new HashMap<String, Object>();
            map.put("__Id", id);
        } else if (!map.containsKey("__Id")) {
            map.put("__Id", id);
        }

        return map;
    }

    private synchronized String generateId() {
        return constructedAt + "_" + (counter++);
    }

    public void scheduleJob(JobID uid, String groupName, String clasString,
                            HashMap dataMap, String triggerGroup, Date expiry) {
        // The scheduler state of this type of jobs will be persisted into DB.
        // Index will be created on this value so it's size should be smaller as
        // possible.
        //we will auto generate the id and put provided id into dataMap. so index will be created on auto generated value.

        startScheduler();
        try {
            dataMap = getMapPopulatedWithId(dataMap, uid.toString());
            String jobId = generateId();

            QuartzJobDetail detail = new QuartzJobDetail(jobId,
                    groupName, clasString, dataMap);
            QuartzTrigger trigger = new QuartzTrigger();
            trigger.createTrigger(jobId, triggerGroup, expiry);
            quartzScheduler.scheduleJob((org.quartz.JobDetail) detail
                    .getJobDetail(), (org.quartz.Trigger) trigger.getTrigger());
        } catch (SchedulerException e) {
            throw new CoreException(e, "Unable To Schedule Job");
        }
    }

    public void scheduleJob(JobID uid, String groupName, String clasString,
                            HashMap dataMap, String triggerGroup, int repeatCount,
                            long repeatInterval) {
        scheduleJob(uid, groupName, clasString, dataMap, triggerGroup,
                repeatCount, repeatInterval, 5);
    }

    public void scheduleJob(JobID uid, String groupName, String clasString,
                            HashMap dataMap, String triggerGroup, int repeatCount,
                            long repeatInterval, int priority) {
        // If we try to schedule a job from a running job having same Id, we will get "org.quartz.ObjectAlreadyExistsException" exception.
        //so we will auto generate the id and put provided id into dataMap.
        startScheduler();
        try {
            dataMap = getMapPopulatedWithId(dataMap, uid.toString());
            String jobId = generateId();
            QuartzJobDetail detail = new QuartzJobDetail(jobId,
                    groupName, clasString, dataMap);
            QuartzTrigger trigger = new QuartzTrigger();
            trigger.createTrigger(jobId, triggerGroup, repeatCount,
                    repeatInterval, priority);
            quartzScheduler.scheduleJob((org.quartz.JobDetail) detail
                    .getJobDetail(), (org.quartz.Trigger) trigger.getTrigger());
        } catch (SchedulerException e) {
            throw new CoreException(e, "Unable To Schedule Job");
        }
    }

    public void scheduleMinutelyJob(JobID uid, String groupName,
                                    String clasString, int repeatInterval) {
        startScheduler();
        try {

            JobKey key = new JobKey(uid.toString());
            if (!quartzScheduler.checkExists(key)) {
                QuartzJobDetail detail = new QuartzJobDetail(uid.toString(),
                        null, clasString, null);
                QuartzTrigger trigger = new QuartzTrigger();
                trigger.createMinutelyTrigger(uid.toString(), groupName,
                        repeatInterval);
                quartzScheduler.scheduleJob((org.quartz.JobDetail) detail
                        .getJobDetail(), (org.quartz.Trigger) trigger
                        .getTrigger());
            }
        } catch (SchedulerException e) {
            throw new CoreException(e, "Unable To Schedule Job");
        }
    }

    @Override
    public void removeJob(JobID uid, String groupName) {
        startScheduler();
        try {
            JobKey key = new JobKey(uid.toString(), groupName);
            quartzScheduler.deleteJob(key);
        } catch (SchedulerException e) {
            throw new CoreException(e, "Unable To Un-Schedule the Job");
        }

    }

    @Override
    public void disableGroup(String groupName) {
        try {
            quartzScheduler.pauseTriggers(GroupMatcher
                    .<TriggerKey>groupEquals(groupName));
        } catch (SchedulerException e) {
            throw new CoreException(e, "Unable to disable the group "
                    + groupName);
        }
    }

    @Override
    public void enableGroup(String groupName) {
        try {
            quartzScheduler.resumeTriggers(GroupMatcher
                    .<TriggerKey>groupEquals(groupName));
        } catch (SchedulerException e) {
            throw new CoreException(e, "Unable to enable the group "
                    + groupName);
        }

    }

}
