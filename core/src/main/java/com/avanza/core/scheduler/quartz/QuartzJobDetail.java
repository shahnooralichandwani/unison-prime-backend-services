package com.avanza.core.scheduler.quartz;

import java.util.HashMap;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.impl.JobDetailImpl;

import com.avanza.core.CoreException;
import com.avanza.core.scheduler.JobDetail;


class QuartzJobDetail implements JobDetail {

    private org.quartz.JobDetail jobDetail;

    public QuartzJobDetail(String jobName, String groupName, String clasString, HashMap dataMap) {
        try {
            Class clazz = Class.forName(clasString);
            JobBuilder builder = JobBuilder.newJob(clazz).withIdentity(jobName, groupName);
            if (dataMap != null) {
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.putAll(dataMap);
                builder.setJobData(jobDataMap);
            }

            jobDetail = builder.build();
        } catch (ClassNotFoundException e) {
            throw new CoreException(e, "Job Class Not Found");
        }
    }

    public Object getJobDetail() {

        return jobDetail;

    }
}
