package com.avanza.core.scheduler;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.main.ClustersConfigLoader;
import com.avanza.core.util.Logger;
import com.avanza.workflow.execution.ExecutionStrategyJob;

public abstract class JobImpl implements Job {

    private JobExecutionContext execCtx;
    private Logger logger = Logger.getLogger(JobImpl.class);

    public final void execute(JobExecutionContext execCtx)
            throws JobExecutionException {

        try {
            this.execCtx = execCtx;
            System.out.println(this.getClass().getName() + " fired for "
                    + getJobName());
            String nodeName = getNodeName();
            if (!ClustersConfigLoader.getCurrentClusterInfo().getName()
                    .equalsIgnoreCase(nodeName))
                return;
            executeInner();
        } catch (Exception ex) {
            logger.LogException(ex.getMessage(), ex);
        } finally {
            ApplicationContext.getContext().release();
        }

    }

    protected Object getAttachedObject() {
        return getAttachedDataByKey("Object");
    }

    protected Object getAttachedDataByKey(String key) {
        return execCtx.getMergedJobDataMap().get(key);
    }

    protected Map<String, Object> getDataMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        for (String key : execCtx.getMergedJobDataMap().keySet()) {
            map.put(key, execCtx.getMergedJobDataMap().get(key));
        }

        return map;
    }

    protected String getJobName() {
        return execCtx.getJobDetail().getKey().getName();
    }

    protected String getGroupName() {
        return execCtx.getJobDetail().getKey().getGroup();
    }

    private String getFullId() {
        Object obj = getAttachedDataByKey("__Id");
        String id = getJobName();
        if (obj != null) {
            id = getAttachedDataByKey("__Id").toString();
        }
        return id;

    }

    protected String getId() {
        String id = getFullId();
        return id.substring(id.indexOf('|') + 1);
    }

    private String getNodeName() {
        StringTokenizer st = new StringTokenizer(getFullId(), "|");
        return st.nextToken();

    }

    public abstract void executeInner();

}
