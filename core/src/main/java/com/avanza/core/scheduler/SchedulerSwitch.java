package com.avanza.core.scheduler;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.calendar.AnnualCalendar;

import com.avanza.core.CoreException;
import com.avanza.core.scheduler.JobDetail;
import com.avanza.core.scheduler.JobID;
import com.avanza.core.scheduler.Scheduler;
import com.avanza.core.scheduler.Trigger;
import com.avanza.core.util.StringHelper;

public class SchedulerSwitch implements Scheduler {

    private Hashtable<String, String> groupsSchedular = new Hashtable<String, String>();
    private Hashtable<String, String> disabledGroups = new Hashtable<String, String>();

    public SchedulerSwitch(Properties props) {

        for (Map.Entry<Object, Object> prop : props.entrySet()) {
            String propKey = prop.getKey().toString();
            if (propKey.equals("grouping")) {
                for (Map.Entry<String, String> entry : StringHelper
                        .getPropertiesMapFrom(prop.getValue().toString())
                        .entrySet()) {
                    addSchedulerGroups(entry.getKey(), entry.getValue());
                }
            } else if (propKey.equals("disabledGroups")) {
                addDisabledGroups(prop.getValue().toString());
            }
        }
    }

    private void addSchedulerGroups(String schedularName, String groupNames) {
        for (String groupName : groupNames.split(",")) {
            if (!groupsSchedular.containsKey(groupName)) {
                groupsSchedular.put(groupName, schedularName);
            }
        }
    }

    private void addDisabledGroups(String groupNames) {
        for (String groupName : groupNames.split(",")) {
            if (!disabledGroups.containsKey(groupName)) {
                disabledGroups.put(groupName, groupName);
            }
        }
    }

    public void startScheduler() {
        if (SchedulerCatalog.getDBScheduler() != null) {
            SchedulerCatalog.getDBScheduler().startScheduler();
        }
    }

    public void scheduleJob(JobDetail job, Trigger trigger) {

    }

    public void shutDownScheduler() {
        if (SchedulerCatalog.getDBScheduler() != null) {
            SchedulerCatalog.getDBScheduler().shutDownScheduler();
        }

        if (SchedulerCatalog.getRAMScheduler() != null) {
            SchedulerCatalog.getRAMScheduler().shutDownScheduler();
        }
    }

    private Scheduler getDefaultDBScheduler(String groupName) {
        Scheduler sch = null;
        if (groupsSchedular.containsKey(groupName)) {
            sch = SchedulerCatalog.getScheduler(groupsSchedular.get(groupName));
        } else if (SchedulerCatalog.getDBScheduler() != null) {
            sch = SchedulerCatalog.getDBScheduler();
        } else if (SchedulerCatalog.getRAMScheduler() != null) {
            sch = SchedulerCatalog.getRAMScheduler();
        }

        return sch;
    }

    private Scheduler getDefaultRAMScheduler(String groupName) {
        Scheduler sch = null;

        if (groupsSchedular.containsKey(groupName)) {
            sch = SchedulerCatalog.getScheduler(groupsSchedular.get(groupName));
        } else if (SchedulerCatalog.getRAMScheduler() != null) {
            sch = SchedulerCatalog.getRAMScheduler();
        } else if (SchedulerCatalog.getDBScheduler() != null) {
            sch = SchedulerCatalog.getDBScheduler();
        }

        return sch;
    }

    public void scheduleJob(JobID uid, String groupName, String clasString,
                            HashMap dataMap, String triggerGroup, Date expiry) {
        if (disabledGroups.containsKey(groupName)) {
            return;
        }
        getDefaultDBScheduler(groupName).scheduleJob(uid, groupName,
                clasString, dataMap, triggerGroup, expiry);

    }

    public void scheduleJob(JobID uid, String groupName, String clasString,
                            HashMap dataMap, String triggerGroup, int repeatCount,
                            long repeatInterval) {
        scheduleJob(uid, groupName, clasString, dataMap, triggerGroup,
                repeatCount, repeatInterval, 5);
    }

    public void scheduleJob(JobID uid, String groupName, String clasString,
                            HashMap dataMap, String triggerGroup, int repeatCount,
                            long repeatInterval, int priority) {
        if (disabledGroups.containsKey(groupName)) {
            return;
        }
        getDefaultRAMScheduler(groupName).scheduleJob(uid, groupName,
                clasString, dataMap, triggerGroup, repeatCount, repeatInterval,
                priority);
    }

    public void scheduleMinutelyJob(JobID uid, String groupName,
                                    String clasString, int repeatInterval) {
        if (disabledGroups.containsKey(groupName)) {
            return;
        }
        getDefaultRAMScheduler(groupName).scheduleMinutelyJob(uid, groupName,
                clasString, repeatInterval);
    }

    @Override
    public void removeJob(JobID uid, String groupName) {
        // Only RAM based Jobs needs to be removed because they may be
        // repitative in nature.
        // whereas DB Jobs get executed once.
        Scheduler sche = null;
        if (groupsSchedular.containsKey(groupName)) {
            sche = SchedulerCatalog
                    .getScheduler(groupsSchedular.get(groupName));
        } else if (SchedulerCatalog.getRAMScheduler() != null) {
            sche = SchedulerCatalog.getRAMScheduler();
        }

        if (sche != null) {
            sche.removeJob(uid, groupName);
        }
    }

    @Override
    public void disableGroup(String groupName) {
        if (!disabledGroups.containsKey(groupName)) {
            disabledGroups.put(groupName, groupName);
        }

        if (groupsSchedular.containsKey(groupName)) {
            SchedulerCatalog.getScheduler(groupsSchedular.get(groupName))
                    .disableGroup(groupName);
        }
    }

    @Override
    public void enableGroup(String groupName) {
        if (disabledGroups.containsKey(groupName)) {
            disabledGroups.remove(groupName);
        }

        if (groupsSchedular.containsKey(groupName)) {
            SchedulerCatalog.getScheduler(groupsSchedular.get(groupName))
                    .enableGroup(groupName);
        }

    }
}
