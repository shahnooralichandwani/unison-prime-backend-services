package com.avanza.core.scheduler;

import java.util.Date;
import java.util.HashMap;


public interface Scheduler {

    public void startScheduler();

    public void shutDownScheduler();

    public void scheduleJob(JobDetail detail, Trigger trigger);

    public void scheduleJob(JobID uid, String groupName, String clasString, HashMap dataMap, String triggerGroup, Date expiry);

    public void scheduleJob(JobID uid, String groupName, String clasString, HashMap dataMap, String triggerGroup, int repeatCount, long repeatInterval);

    public void scheduleJob(JobID uid, String groupName, String clasString, HashMap dataMap, String triggerGroup, int repeatCount, long repeatInterval, int priority);

    public void scheduleMinutelyJob(JobID uid, String groupName, String clasString, int repeatInterval);

    public void removeJob(JobID uid, String groupName);

    public void disableGroup(String groupName);

    public void enableGroup(String groupName);
}
