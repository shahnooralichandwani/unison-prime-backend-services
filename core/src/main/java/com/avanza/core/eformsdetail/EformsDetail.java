package com.avanza.core.eformsdetail;

import java.util.LinkedHashSet;
import java.util.Set;


import com.avanza.core.data.DbObject;

public class EformsDetail extends DbObject implements Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -8550951534847723086L;

    protected String eformDetailId;
    protected String eformTicketNum;
    protected String flag;
    protected String notesFlag;
    protected String notes;
    protected String loginUser;
    protected EformsDetail eformsDetail;
    protected Set<EformsDetail> eformsDetails = new LinkedHashSet<EformsDetail>(0);
    //private ValueTreeNode valueTreeNode;


    public EformsDetail() {
    }

    public EformsDetail getEformsDetail() {
        return this.eformsDetail;
    }

    public void setEformsDetail(EformsDetail eformsDetail) {
        this.eformsDetail = eformsDetail;
    }


    public String getEformDetailId() {
        return this.eformDetailId;
    }

    public void setEformDetailId(String eformDetailId) {
        this.eformDetailId = eformDetailId;
    }

    public String getEformTicketNum() {
        return this.eformTicketNum;
    }

    public void setEformTicketNum(String eformTicketNum) {
        this.eformTicketNum = eformTicketNum;
    }

    public String getLoginUser() {
        return this.loginUser;
    }

    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    public String getFlag() {
        return this.flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getNotesFlag() {
        return this.notesFlag;
    }

    public void setNotesFlag(String notesFlag) {
        this.notesFlag = notesFlag;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Set<EformsDetail> getComplaintDetails() {
        return this.eformsDetails;
    }

    public void setComplaintDetails(Set<EformsDetail> complaintDetails) {
        this.eformsDetails = complaintDetails;
    }



    /*
     * @Override protected Object clone() throws CloneNotSupportedException {
     * ProductType ptype = new ProductType(); ptype.setProductCode(productCode);
     * ptype.setDescription(description); ptype.setDisplayOrder(displayOrder);
     * ptype.setLevelId(levelId); ptype.setParentCode(parentCode);
     * ptype.setProduct(product); ptype.setProductNamePrm(productNamePrm);
     * ptype.setProductNameSec(productNameSec); ptype.setRootNodeId(rootNodeId);
     * ptype.setSystemName(systemName); return ptype; }
     */
}
