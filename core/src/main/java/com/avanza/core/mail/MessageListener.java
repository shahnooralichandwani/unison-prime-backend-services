/**
 *
 */
package com.avanza.core.mail;

import java.io.IOException;

import javax.mail.MessagingException;

import com.avanza.core.document.Document;

/**
 * @author shahbaz.ali
 *
 */
public interface MessageListener {

    public void messagesReceived(MessageEventObject event) throws MessageNotSupportedException, MessagingException, IOException;

}
