/**
 *
 */
package com.avanza.core.mail;

import com.avanza.core.CoreException;

/**
 * @author shahbaz.ali
 *
 */
public class MessageNotSupportedException extends CoreException {

    private static final long serialVersionUID = 1L;

    public MessageNotSupportedException(String message) {

        super(message);
    }

    public MessageNotSupportedException(String format, Object... args) {

        super(format, args);
    }

    public MessageNotSupportedException(
            RuntimeException innerExcep,
            String message) {

        super(message, innerExcep);
    }

    public MessageNotSupportedException(
            RuntimeException innerExcep,
            String format,
            Object... args) {

        super(innerExcep, format, args);
    }

    public MessageNotSupportedException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public MessageNotSupportedException(
            Exception innerExcep,
            String format,
            Object... args) {

        super(innerExcep, format, args);
    }
}
