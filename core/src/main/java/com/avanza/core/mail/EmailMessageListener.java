package com.avanza.core.mail;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Message.RecipientType;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import com.avanza.core.document.Document;
import com.avanza.core.document.DocumentManager;
import com.avanza.core.document.ERepositoryDocument;
import com.avanza.core.main.EdocumentConfigManager;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;

public class EmailMessageListener implements MessageListener {

    private final Logger logger = Logger.getLogger(EmailMessageListener.class);

    private void parse(String uid, Part part)
            throws MessageNotSupportedException, MessagingException,
            IOException {

        logger.logInfo("[Parse Message and preparing Edocument ( EmailMessageListener.parse )]");
        Message message = (Message) part;
        Map<List, InputStream> attachments = new HashMap<List, InputStream>();
        String messageBody = getMessageBody(message);
        storeAttachments(attachments, message);

        ERepositoryDocument edoc = new ERepositoryDocument();
        List<String> recipients = new ArrayList<String>(0);

        if (message.getRecipients(RecipientType.TO) != null) {

            for (Address address : message.getRecipients(RecipientType.TO)) {
                recipients.add(address.toString());
            }
        } else if (message.getRecipients(RecipientType.CC) != null) {

            for (Address address : message.getRecipients(RecipientType.CC)) {
                recipients.add(address.toString());
            }
        }


        edoc.setToAddress(StringHelper.getTruncatedText(Convert.toCSV(recipients), 500));
        edoc.setFromAddress(message.getFrom()[0].toString());


        edoc.setSentDate(message.getSentDate());
        edoc.setReceivedDate(message.getReceivedDate());

        if (StringHelper.isNotEmpty(messageBody))
            edoc.setDesc(messageBody);
        else
            edoc.setDesc("(blank)");

        if (StringHelper.isNotEmpty(message.getSubject()))
            edoc.setDocumentTitle(StringHelper.getTruncatedText(message.getSubject(), 500));
        else
            edoc.setDocumentTitle("(no subject)");

        edoc.setDocumentType("Unison.Document.Edocument.Email");
        edoc.setDocumentId(uid);
        edoc.setAttachments(attachments);
        edoc.setRepoId("emailStore");
        saveInstance(edoc);
        saveInRepository(edoc);

    }

    private void saveInstance(ERepositoryDocument edoc) {
        logger.logInfo("[Saving document in database ( EmailMessageListener.saveInstance )]");

        DataObject dataObject = EdocumentConfigManager.prepareEdocumentObject(edoc);
        dataObject.setValue("IS_COMPOSED", false);
        EdocumentConfigManager.persistEDocument(dataObject);

    }

    private void saveInRepository(Document doc) {
        logger.logInfo("[Saving document in file repository ( EmailMessageListener.saveInRepository )]");
        DocumentManager.saveDocument(doc, "emailRespository");
    }

    private String getMessageBody(Part p) throws MessagingException,
            IOException, MessageNotSupportedException {

        if (p.isMimeType("text/*")) {

            if (p.isMimeType("text/calendar"))
                throw new MessageNotSupportedException(p.getContentType() + " Mime Type not supported.");

            String s = (String) p.getContent();
            return s;
        }

        if (p.isMimeType("multipart/alternative")) {

            // prefer html text over plain text
            Multipart mp = (Multipart) p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); i++) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null)
                        text = getMessageBody(bp);
                    continue;
                } else if (bp.isMimeType("text/html")) {
                    String s = getMessageBody(bp);
                    if (s != null)
                        return s;
                } else {
                    return getMessageBody(bp);
                }
            }
            return text;
        } else if (p.isMimeType("multipart/*")) {

            Multipart mp = (Multipart) p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                String s = getMessageBody(mp.getBodyPart(i));
                if (s != null)
                    return s;
            }
        }

        return null;
    }

    private void storeAttachments(Map<List, InputStream> attachments, Part p) {

        try {
            Object object = p.getContent();

            if (object instanceof Multipart) {

                MimeMultipart multipart = (MimeMultipart) object;

                for (int k = 0; k < multipart.getCount(); k++) {
                    System.out.println(multipart.getBodyPart(k).getContentType());

                    MimeBodyPart bodyPart = (MimeBodyPart) multipart.getBodyPart(k);

                    if (bodyPart.isMimeType("multipart/*") || bodyPart.isMimeType("text/*"))
                        continue;

                    else {
                        List<String> nametype = new ArrayList<String>(2);
                        nametype.add(bodyPart.getFileName());
                        nametype.add(bodyPart.getContentType());

                        attachments.put(nametype, bodyPart.getInputStream());
                    }
                }

            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    public byte[] getBytesFromStream(InputStream is) throws IOException {

        long length = is.available();

        if (length > Integer.MAX_VALUE) {
            // File is too large
        }

        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {

        }

        is.close();
        return bytes;
    }

    private void saveFile(byte[] bytes, String name) throws Exception {

        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(name));
        bos.write(bytes, 0, bytes.length);
        bos.close();

    }

    // This method will called when new message received
    public void messagesReceived(MessageEventObject event) throws MessageNotSupportedException, MessagingException, IOException {

        logger.logInfo("[Message received called ( EmailMessageListener.messagesReceived )]");

//		try {
        parse(event.getMessageId(), event.getMessage());
//		} catch (MessageNotSupportedException e) {
//			logger.logInfo("[ Mime type not supported : "+event.getMessage().getContentType()+" ]");
//		}

    }

}
