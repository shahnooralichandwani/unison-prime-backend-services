package com.avanza.core.mail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

//import javax.mail.FetchProfile;
//import javax.mail.Folder;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.NoSuchProviderException;
//import javax.mail.Part;
//import javax.mail.Session;
//import javax.mail.Store;
//import javax.mail.UIDFolder;
//import javax.mail.URLName;

import com.avanza.core.main.EdocumentConfigManager;
import com.avanza.core.util.Logger;
//import com.sun.mail.pop3.POP3Folder;


/**
 * @author shahbaz.ali Simple Pop3 Message Poller Service Timely fetch only new
 * messages
 * This class can be used as a separate thread as well..
 */
public class MessagePoller extends Thread {
/*
	Logger logger = Logger.getLogger(MessagePoller.class);

	private long sleepTime = 10000L;
	private boolean polling = false;
	private String protocol;
	private String host;
	private String user;
	private int port;
	private String password;
	private POP3Folder inboxFolder;
	private FetchProfile profile = new FetchProfile();
	private UIDStore uids = new UIDStore();
	private List<MessageListener> listeners = new ArrayList<MessageListener>();

	*//**
     *
     *//*
	public MessagePoller() {

	}

	*//**
     * @param protocol
     * @param host
     * @param port
     * @param user
     * @param password
     *//*
	public MessagePoller(
			String protocol,
			String host,
			int port,
			String user,
			String password) {
		this.protocol = protocol;
		this.host = host;
		this.user = user;
		this.port = port;
		this.password = password;

	}

	public void initialize() {

		try {
			
			logger.logInfo("[Initializing Message Poller Service ( MessagePoller.initialize() )]");

			

			sleepTime = EdocumentConfigManager.getPollIntervalMins() * 60 * 1000;
			polling = true;

			uids.loadfromDB();
			
//Change Nida Siddiqui 
			String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

		    Properties pop3Props = new Properties();

		    pop3Props.setProperty("mail.pop3.socketFactory.class", SSL_FACTORY);
		    pop3Props.setProperty("mail.pop3.socketFactory.fallback", "false");
		    pop3Props.setProperty("mail.pop3.port",  ""+port);
		    pop3Props.setProperty("mail.pop3.socketFactory.port", ""+port);

		   URLName url = new URLName(protocol, host, port, "",user, password);

		   Session session = Session.getInstance(pop3Props, null);
		   Store store = session.getStore(url);
		   store.connect();
			inboxFolder = (POP3Folder) store.getFolder("INBOX");
			profile.add(UIDFolder.FetchProfileItem.UID);

			fetchMessages();

			// this.start();

		} catch (NoSuchProviderException e) {
			logger.LogException("NoSuchProviderException initializing Message Poller Service ( MessagePoller.initialize() )", e);
		} catch (MessagingException e) {
			logger.LogException("MessagingException initializing Message Poller Service ( MessagePoller.initialize() )", e);
		} catch (Exception e) {
			logger.LogException("Exception initializing Message Poller Service ( MessagePoller.initialize() )", e);
		}
		

	}

	@Override
	public void run() {

		try {
			while (polling) {

				fetchMessages();

				Thread.sleep(sleepTime);
			}
		} catch (Exception e) {
			closeInbox();
			polling = false;
		}

	}

	public void closeInbox() {
		try {
			logger.logInfo("[Closing Inbox folder ( MessagePoller.closeInbox() )]");
			inboxFolder.close(false);
			inboxFolder.getStore().close();
			

		} catch (MessagingException e) {
			logger.LogException("Exception closing Inbox Folder", e);
		}

	}

	public void fetchMessages() {

		int newEmails = 0;

		logger.logInfo("[Checking for new messages ( MessagePoller.fetchMessages() )]");

		try {
			if (!inboxFolder.isOpen())
				inboxFolder.open(Folder.READ_ONLY);

			Message[] messages = inboxFolder.getMessages();
			inboxFolder.fetch(messages, profile);

			for (int i = 0; i < messages.length; i++) {
				String uid = inboxFolder.getUID(messages[i]);
				Part newMessage = null;

				if (uids.isNewFromDB(uid)) {
					try {
						newEmails++;
						newMessage = inboxFolder.getMessage(i + 1);

						if (newMessage != null)
							notifyListeners(uid, newMessage);

					} catch (MessageNotSupportedException mnspe) {
						logger.logInfo("[ Mime type not supported : " + newMessage.getContentType() + " MessagePoller.fetchMessages() ]");
					} catch (MessagingException e) {
						logger.LogException("[Exception while parsing message. MessagePoller.fetchMessages] ", e);
					}catch (Exception e) {
						logger.LogException("[Exception while parsing message. MessagePoller.fetchMessages] ", e);
					}
					

				}
			}

		} catch (Exception e) {
			logger.LogException("Exception while fetching messages.", e);
		}
		finally {
			closeInbox();
		}
		logger.logInfo(newEmails + " new message(s) found.");
	}

	private void notifyListeners(String uid, Part message)
			throws MessageNotSupportedException, MessagingException,
			IOException {

		logger.logInfo("[New Message Received, Notifying listeners ( MessagePoller.notifyListeners() )]");
		logger.logInfo("[Subject: " + ((Message) message).getSubject() + " ]");

		MessageEventObject eventObject = new MessageEventObject(uid,message);

		for (MessageListener mListener : listeners) {
			mListener.messagesReceived(eventObject);
		}
	}

	public void addMessageListener(MessageListener listener) {

		logger.logInfo("[Adding message Listener ( MessagePoller.addMessageListener() )]");

		if (listener instanceof MessageListener) {
			listeners.add(listener);
		}
	}*/

}
