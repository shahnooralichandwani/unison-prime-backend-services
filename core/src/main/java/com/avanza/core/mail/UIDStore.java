package com.avanza.core.mail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.JDBCDataBroker;

public class UIDStore {
    private Map uids = new HashMap();

    public void load(File file) throws IOException {
        if (file != null && file.exists()) {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            try {
                String uid = reader.readLine();
                while (uid != null) {
                    uids.put(uid, Boolean.FALSE);
                    uid = reader.readLine();
                }
            } finally {
                reader.close();
            }
        }
    }

    public void loadfromDB() {

        JDBCDataBroker broker = (JDBCDataBroker) DataRepository.getBroker("JDBC.Unison");
        String query = "SELECT EDOCUMENT_REPOSITORY_ID FROM EDOCUMENT";
        List<Object[]> records = broker.executeQuery(query);

        for (int i = 0; i < records.size(); i++)
            uids.put(records.get(i)[0], Boolean.FALSE);

    }

    public void store(File file) throws IOException {

        FileWriter writer = new FileWriter(file, true);
        try {

            Iterator iterator = uids.keySet().iterator();

            while (iterator.hasNext()) {

                String uid = (String) iterator.next();
                if (uids.get(uid) == Boolean.TRUE) {
                    writer.write(uid);
                    writer.write('\n');
                }
            }
        } finally {
            writer.close();
        }
    }

    public boolean isNewFromFile(String uid) {
        boolean inList = uids.containsKey(uid);

        if (!inList)
            uids.put(uid, Boolean.TRUE);

        return !inList;
    }

    public boolean isNewFromDB(String uid) {
        return !uids.containsKey(uid);
    }
}
