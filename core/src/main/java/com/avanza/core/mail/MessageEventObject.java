package com.avanza.core.mail;

import javax.mail.Part;

/**
 * @author shahbaz.ali
 */
public class MessageEventObject {

    String messageId;
    Part message;

    public MessageEventObject(String uid, Part message) {

        this.messageId = uid;
        this.message = message;

    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Part getMessage() {
        return message;
    }

    public void setMessage(Part message) {
        this.message = message;
    }

}
