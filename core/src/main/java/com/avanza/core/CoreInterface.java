package com.avanza.core;

import com.avanza.core.util.configuration.ConfigSection;

public interface CoreInterface {

    void load(ConfigSection section);

    void dispose();
}
