package com.avanza.core.interaction.email;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.StringHelper;

public class EmailLog extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 7901664311591345578L;

    private String messageKey;
    private EmailLog emailLog;
    private String replyOf;
    private InteractionChannel interactionChannel;
    private String channelId;
    private Date recieveTime;
    private Date sentTime;
    private String subject;
    private String message;
    private String header;
    private String referenceKey;
    private String from;
    private String to;
    private String cc;
    private String bcc;
    private Boolean isRead;
    private String notes;

    private boolean selected;
    private String formatedSent;
    private String formatedRecieve;

    private Set<EmailLog> emailLogs = new HashSet<EmailLog>(0);
    private Set<EmailInteractionActivity> emailInteractionActivities = new HashSet<EmailInteractionActivity>(0);

    public EmailLog() {
    }

    public String getMessageKey() {
        return this.messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public EmailLog getEmailLog() {
        return this.emailLog;
    }

    public void setEmailLog(EmailLog emailLog) {
        this.emailLog = emailLog;
    }

    public InteractionChannel getInteractionChannel() {
        return this.interactionChannel;
    }

    public void setInteractionChannel(InteractionChannel interactionChannel) {
        this.interactionChannel = interactionChannel;
    }

    public Date getRecieveTime() {
        return this.recieveTime;
    }

    public void setRecieveTime(Date recieveTime) {
        this.recieveTime = recieveTime;
        if (recieveTime != null)
            this.formatedRecieve = new SimpleDateFormat("EEE dd/MM/yy hh:mm a").format(this.recieveTime);
    }

    public Date getSentTime() {
        return this.sentTime;
    }

    public void setSentTime(Date sentTime) {
        this.sentTime = sentTime;
        if (sentTime != null)
            this.formatedSent = new SimpleDateFormat("EEE dd/MM/yy hh:mm a").format(this.sentTime);
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getHeader() {
        return this.header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getReferenceKey() {
        return this.referenceKey;
    }

    public void setReferenceKey(String referenceKey) {
        this.referenceKey = referenceKey;
    }

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return this.to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCc() {
        return this.cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return this.bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public Boolean getIsRead() {
        return this.isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Set<EmailLog> getEmailLogs() {
        return this.emailLogs;
    }

    public void setEmailLogs(Set<EmailLog> emailLogs) {
        this.emailLogs = emailLogs;
    }

    public Set<EmailInteractionActivity> getEmailInteractionActivities() {
        return this.emailInteractionActivities;
    }

    public void setEmailInteractionActivities(
            Set<EmailInteractionActivity> emailInteractionActivities) {
        this.emailInteractionActivities = emailInteractionActivities;
    }

    public String getReplyOf() {
        return replyOf;
    }

    public void setReplyOf(String replyOf) {
        this.replyOf = replyOf;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getShortSubject() {
        if (StringHelper.isNotEmpty(this.subject) && this.subject.length() > 50) {
            return this.subject.substring(0, 50) + "...";
        }
        return this.subject;
    }

    public void setShortSubject(String subject) {
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isReply() {

        return this.replyOf != null;
    }

    public String getFormatedSent() {
        return formatedSent;
    }

    public String getFormatedRecieve() {
        return formatedRecieve;
    }

    public void setFormatedRecieve(String formatedRecieve) {
        this.formatedRecieve = formatedRecieve;
    }

    public void setFormatedSent(String formatedSent) {
        this.formatedSent = formatedSent;
    }
}
