package com.avanza.core.interaction.email;

public class EmailInteractionActivityId implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5087705146087978547L;

    private String messageKey;
    private String channelId;
    private long serialNum;

    public EmailInteractionActivityId() {
    }

    public EmailInteractionActivityId(String messageKey, String channelId,
                                      long serialNum) {
        this.messageKey = messageKey;
        this.channelId = channelId;
        this.serialNum = serialNum;
    }

    public String getMessageKey() {
        return this.messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public long getSerialNum() {
        return this.serialNum;
    }

    public void setSerialNum(long serialNum) {
        this.serialNum = serialNum;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof EmailInteractionActivityId))
            return false;
        EmailInteractionActivityId castOther = (EmailInteractionActivityId) other;

        return ((this.getMessageKey() == castOther.getMessageKey()) || (this
                .getMessageKey() != null
                && castOther.getMessageKey() != null && this.getMessageKey()
                .equals(castOther.getMessageKey())))
                && ((this.getChannelId() == castOther.getChannelId()) || (this
                .getChannelId() != null
                && castOther.getChannelId() != null && this
                .getChannelId().equals(castOther.getChannelId())))
                && (this.getSerialNum() == castOther.getSerialNum());
    }

    public int hashCode() {
        int result = 17;

        result = 37
                * result
                + (getMessageKey() == null ? 0 : this.getMessageKey()
                .hashCode());
        result = 37 * result
                + (getChannelId() == null ? 0 : this.getChannelId().hashCode());
        result = 37 * result + (int) this.getSerialNum();
        return result;
    }

}
