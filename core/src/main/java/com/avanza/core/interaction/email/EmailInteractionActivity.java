package com.avanza.core.interaction.email;

import com.avanza.core.data.DbObject;

public class EmailInteractionActivity extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 4167576854626702752L;

    private EmailInteractionActivityId id;
    private EmailLog emailLog;
    private InteractionChannel interactionChannel;
    private String activityTypeId;
    private String custRelationNum;
    private String refDocNum;
    private String userId;
    private String docTypeId;
    private String activityDescription;

    public EmailInteractionActivity() {
    }

    public EmailInteractionActivityId getId() {
        return this.id;
    }

    public void setId(EmailInteractionActivityId id) {
        this.id = id;
    }

    public EmailLog getEmailLog() {
        return this.emailLog;
    }

    public void setEmailLog(EmailLog emailLog) {
        this.emailLog = emailLog;
    }

    public InteractionChannel getInteractionChannel() {
        return this.interactionChannel;
    }

    public void setInteractionChannel(InteractionChannel interactionChannel) {
        this.interactionChannel = interactionChannel;
    }

    public String getActivityTypeId() {
        return this.activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public String getCustRelationNum() {
        return this.custRelationNum;
    }

    public void setCustRelationNum(String custRelationNum) {
        this.custRelationNum = custRelationNum;
    }

    public String getRefDocNum() {
        return this.refDocNum;
    }

    public void setRefDocNum(String refDocNum) {
        this.refDocNum = refDocNum;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDocTypeId() {
        return this.docTypeId;
    }

    public void setDocTypeId(String docTypeId) {
        this.docTypeId = docTypeId;
    }

    public String getActivityDescription() {
        return this.activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    public String getChannelId() {
        return this.id.getChannelId();
    }

    public String getMessageKey() {
        return this.id.getMessageKey();
    }

    public long getSerialNum() {
        return this.id.getSerialNum();
    }
}