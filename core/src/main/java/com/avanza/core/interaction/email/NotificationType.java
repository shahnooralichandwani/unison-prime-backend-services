package com.avanza.core.interaction.email;


public enum NotificationType {

    SMS("SMS"), Fax("Fax"), Email("Email");

    String value;

    NotificationType(String val) {
        this.value = val;
    }

    public String toString() {
        return value;
    }
}
