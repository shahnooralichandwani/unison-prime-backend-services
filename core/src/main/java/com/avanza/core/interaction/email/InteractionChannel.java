package com.avanza.core.interaction.email;

import java.util.List;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.DbObject;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;

public class InteractionChannel extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = -6167972543504857113L;

    private String channelId;
    private String chanelNamePrm;
    private String chanelNameSec;

    private static List<InteractionChannel> interactionChannelList;

    public InteractionChannel() {
    }

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChanelNamePrm() {
        return this.chanelNamePrm;
    }

    public void setChanelNamePrm(String chanelNamePrm) {
        this.chanelNamePrm = chanelNamePrm;
    }

    public String getChanelNameSec() {
        return this.chanelNameSec;
    }

    public void setChanelNameSec(String chanelNameSec) {
        this.chanelNameSec = chanelNameSec;
    }

    public static List<InteractionChannel> getInteractionChannelList() {

        if (interactionChannelList != null)
            return interactionChannelList;

        DataBroker broker = DataRepository.getBroker(InteractionChannel.class.getName());
        interactionChannelList = broker.findAll(InteractionChannel.class);

        return interactionChannelList;
    }

    public static InteractionChannel getInteractionChannel(NotificationType notificationSource) {

        for (InteractionChannel channel : getInteractionChannelList()) {
            if (channel.getChanelNamePrm().equalsIgnoreCase(notificationSource.toString()))
                return channel;
        }
        return null;
    }

    public String getChannel() {
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class.getName());
        boolean isPrimaryLocale = ((LocaleInfo) webContext.getAttribute(SessionKeyLookup.CurrentLocale)).isPrimary();
        return (isPrimaryLocale) ? chanelNamePrm : chanelNameSec;
    }
}
