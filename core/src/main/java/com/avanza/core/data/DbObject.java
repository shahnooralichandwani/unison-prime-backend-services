package com.avanza.core.data;

import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;

import java.io.Serializable;
import java.util.Date;

public class DbObject implements Serializable {

    // persisted fields
    private Date createdOn;
    private String createdBy;
    private Date updatedOn;
    private String updatedBy;

    // transient fields
    // private HashMap<String, UpdateInfo> updateList;

    protected DbObject() {

        // this.updateList = new HashMap<String, UpdateInfo>();
    }

    public void setCreation(String userId) {

        Guard.checkNullOrEmpty(userId, "DbObject.setCreation");
        this.createdOn = new Date();
        this.createdBy = userId;
        this.updatedOn = this.createdOn;
        this.updatedBy = userId;

        // auto generate pk if required.
        if (StringHelper.isNotEmpty(getPkCounterName())) {
            setGeneratedPk(MetaDataRegistry.getNextCounterValue(getPkCounterName()));
        }
    }

    public void setUpdation(String userId) {

        Guard.checkNullOrEmpty(userId, "DbObject.setUpdation");
        this.updatedOn = new Date();
        this.updatedBy = userId;
    }

    public String getCreatedBy() {

        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {

        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {

        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {

        this.createdOn = createdOn;
    }

    public String getUpdatedBy() {

        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {

        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {

        return this.updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {

        this.updatedOn = updatedOn;
    }

    public boolean isNew() {

        return ((this.createdOn == null) && (this.createdBy == null));
    }

    public void copyValues(DbObject copyFrom) {
        this.createdBy = copyFrom.createdBy;
        this.createdOn = copyFrom.createdOn;
        this.updatedBy = copyFrom.updatedBy;
        this.updatedOn = copyFrom.updatedOn;
    }

    public boolean isModified() {

        return true;
        // return (this.updateList.size() > 0);
    }

    public String getPkCounterName() {
        return null;
    }

    public void setGeneratedPk(Object generatedPk) {
        new RuntimeException(
                "DbObject.setGeneratedPk must be overridden if you want to generate PK autometicaly");
    }
}
