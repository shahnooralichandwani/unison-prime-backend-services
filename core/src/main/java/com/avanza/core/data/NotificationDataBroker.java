package com.avanza.core.data;

import com.avanza.core.CoreException;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SearchConversionUtil;
import com.avanza.core.sdo.DataException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.TimeLogger;

import java.io.Serializable;
import java.util.List;

/**
 * The broker is exactly the replica of the HibernatDataBroker apart from the
 * session management. The class doesn't implement OpenSessionInView pattern
 * because the broker is supposed to be used in the background threads.
 *
 * @author Farhan M. Amin
 */
public class NotificationDataBroker extends HibernateDataBroker implements
        DataBroker {
    private static final Logger logger = Logger
            .getLogger(NotificationDataBroker.class);
    private static final String ERROR_MSG = "Exception occured %1$s";

    public void enableFilter(final String filterName) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("NotificationDataBroker.enableFilter(%s)",
                filterName.getClass().getSimpleName()));
        Session session = getDataSession();
        try {
            boolean tranActive = session.isTransactionActive();
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();

            if (filterName != null && !"".equalsIgnoreCase(filterName))
                hbSession.enableFilter(filterName);
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t, "enableFilter(final filterName)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    public void disableFilter(final String filterName) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("NotificationDataBroker.disableFilter(%s)",
                filterName.getClass().getSimpleName()));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            if (filterName != null && !"".equalsIgnoreCase(filterName))
                hbSession.disableFilter(filterName);
        } catch (Throwable t) {
            logger
                    .LogException(ERROR_MSG, t,
                            "disableFilter(final filterName)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    public <T> void add(final T item) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("NotificationDataBroker.add(%s)", item
                .getClass().getSimpleName()));
        Session session = getDataSession();
        try {

            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            boolean tranActive = session.isTransactionActive();
            session.beginTransaction();

            if (item instanceof DbObject) {
                DbObject temp = (DbObject) item;
                this.setUserSpecificData(temp, false);
            }

            hbSession.save(item);
            if (!tranActive) {
                session.commit();
            }
        } catch (Throwable t) {
            ApplicationContext.getContext().getTxnContext().rollback();
            logger.LogException(ERROR_MSG, t, "add(final T item)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    public <T> void saveOrUpdate(final T item) {
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            boolean tranActive = session.isTransactionActive();
            session.beginTransaction();
            if (item instanceof DbObject) {
                DbObject temp = (DbObject) item;
                this.setUserSpecificData(temp, false);
            }
            hbSession.merge(item);
            if (!tranActive) {
                session.commit();
            }
        } catch (Throwable t) {
            ApplicationContext.getContext().getTxnContext().rollback();
            logger.LogException(ERROR_MSG, t, "saveOrUpdate(final T item)");
            throw new CoreException(t);
        } finally {
        }
    }

    public <T> void delete(final T item) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("NotificationDataBroker.delete(%s)", item
                .getClass().getSimpleName()));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            boolean tranActive = session.isTransactionActive();
            session.beginTransaction();
            if (item instanceof DbObject) {
                DbObject temp = (DbObject) item;
                this.setUserSpecificData(temp, true);
            }
            hbSession.delete(item);
            if (!tranActive) {
                session.commit();
            }
        } catch (Throwable t) {
            ApplicationContext.getContext().getTxnContext().rollback();
            logger.LogException(ERROR_MSG, t, "delete(final T item)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    public <T> List<T> find(String query) {

        throw new DataSourceException(
                "Method NotificationDataBroker.find(String) not implemented");
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> List<T> find(Search query) {
        TimeLogger timmer = TimeLogger.init();
        if (query.getHqlCriteria() != null)
            timmer.begin(String.format("NotificationDataBroker.find(%s)", query
                    .getHqlCriteria().toString()));
        else
            timmer.begin(String.format("NotificationDataBroker.find(%s)", query
                    .getFirstFrom().toString()));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            List result = null;
            if (query.getHqlCriteria() != null)
                result = query.getHqlCriteria().list();
            else
                result = SearchConversionUtil.getHQLSearchObject(query,
                        hbSession).list();
            return result;
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t, "find(Search query)");
            throw new CoreException(t);
        } finally {
            timmer.end(query.toString());
        }
    }

    @SuppressWarnings("unchecked")
    public <T, ID extends Serializable> T findById(Class<T> t, ID id) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("NotificationDataBroker.findById(%s, %s)", t
                .getSimpleName(), id));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            boolean tranActive = session.isTransactionActive();
            session.beginTransaction();
            Object a = (T) (hbSession.get(t, id));
            return (T) a;
        } catch (Throwable th) {
            logger.LogException(ERROR_MSG, th, "findById(Class<T> t, ID id)");
            throw new CoreException(th);
        } finally {
            timer.end();
        }
    }

    public <T> void persist(final T item) {
        DbObject dbObj = (DbObject) item;
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("NotificationDataBroker.persist(%s)", item
                .getClass().getSimpleName()));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            boolean tranActive = session.isTransactionActive();
            session.beginTransaction();
            if (item instanceof DbObject) {
                if (dbObj.isNew()) {
                    this.setUserSpecificData(dbObj, false);
                    hbSession.save(dbObj);
                } else {
                    this.setUserSpecificData(dbObj, true);
                    hbSession.merge(dbObj);
                }
            }
            if (!tranActive) {
                session.commit();
            }
        } catch (Throwable t) {
            ApplicationContext.getContext().getTxnContext().rollback();
            logger.LogException(ERROR_MSG, t, "persist(final T item)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    public void synchronize() {

        throw new DataSourceException(
                "Method NotificationDataBroker.synchronize() not implemented");
    }

    public <T> void update(T item) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("NotificationDataBroker.update(%s)", item
                .getClass().getSimpleName()));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            boolean tranActive = session.isTransactionActive();
            session.beginTransaction();
            if (item instanceof DbObject) {
                DbObject temp = (DbObject) item;
                this.setUserSpecificData(temp, true);
            }
            hbSession.merge(item);
            if (!tranActive) {
                session.commit();
            }
        } catch (Throwable t) {
            ApplicationContext.getContext().getTxnContext().rollback();
            logger.LogException(ERROR_MSG, t, "update(T item)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> List<T> findAll(Class<T> t) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("NotificationDataBroker.findAll(%s)", t
                .getSimpleName()));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            session.beginTransaction();
            List list = hbSession.createCriteria(t).list();
            return list;
        } catch (Throwable th) {
            logger.LogException(ERROR_MSG, th, "findAll(Class<T> t)");
            throw new CoreException(th);
        } finally {
            timer.end();
        }
    }

    public <T> List<T> findAll(String entityName) {

        throw new DataSourceException(
                "Method NotificationDataBroker.findAll(String) is not implemented");
    }

    public <T> void setUserSpecificData(DbObject item, boolean isModified) {
        try {
            User admin = ApplicationContext.getContext().get(
                    SessionKeyLookup.CURRENT_USER_KEY);
            String loginId = User.UNKNOWN_USER;

            // If the user is not found in the ApplicationContext then use the
            // Unknown user.
            if (admin != null)
                loginId = admin.getLoginId();

            if (!isModified)
                item.setCreation(loginId);
            else
                item.setUpdation(loginId);
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t,
                    "setUserSpecificData(DbObject item, boolean isModified)");
            throw new CoreException(t);
        }
    }

    public <ID, T extends DataObject> T findById(String entityId, ID id)
            throws DataSourceException {
        return null;
    }

    public <T> void persistAll(List<T> items) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("NotificationDataBroker.persist(%s)", items
                .get(0).getClass().getSimpleName()));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            boolean tranActive = session.isTransactionActive();
            session.beginTransaction();
            for (T item : items) {
                // escape(item);
                DbObject dbObj = (DbObject) item;
                if (item instanceof DbObject) {
                    if (dbObj.isNew()) {
                        this.setUserSpecificData(dbObj, false);
                        hbSession.save(dbObj);
                    } else {
                        this.setUserSpecificData(dbObj, true);
                        hbSession.merge(dbObj);
                    }
                } else
                    hbSession.saveOrUpdate(item);

            }
            if (!tranActive) {
                session.commit();
            }
        } catch (Throwable t) {
            ApplicationContext.getContext().getTxnContext().rollback();
            logger
                    .LogException(ERROR_MSG, t,
                            "persistAll(final List<T> items)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    @Override
    public int getCriteriaCount(Search criteria) {
        throw new DataException("Not Implemented");
    }

}
