package com.avanza.core.data.db;

public enum ProviderType {

    None(0), Oracle(1), Sql2000(2), Sql2005(3), Sybase(4), Informix(5);

    private static final String NONE = "none";
    private static final String ORACLE = "oracle";
    private static final String SQL2000 = "sql2000";
    private static final String SQL2005 = "sql2005";
    private static final String SYBASE = "sybase";
    private static final String INFORMIX = "informix";

    private int value;

    private ProviderType(int value) {

        this.value = value;
    }

    public int toInteger() {

        return this.value;
    }

    public static ProviderType parse(String typeValue) {

        if (ProviderType.NONE.compareToIgnoreCase(typeValue) == 0)
            return ProviderType.None;
        else if (ProviderType.ORACLE.compareToIgnoreCase(typeValue) == 0)
            return ProviderType.Oracle;
        else if (ProviderType.SQL2000.compareToIgnoreCase(typeValue) == 0)
            return ProviderType.Sql2000;
        else if (ProviderType.SQL2005.compareToIgnoreCase(typeValue) == 0)
            return ProviderType.Sql2005;
        else if (ProviderType.SYBASE.compareToIgnoreCase(typeValue) == 0)
            return ProviderType.Sybase;
        else if (ProviderType.INFORMIX.compareToIgnoreCase(typeValue) == 0)
            return ProviderType.Informix;

        throw new IllegalArgumentException(String.format("[%1$s] is not identified as valid ProviderType value.", typeValue));
    }

    public static ProviderType fromInt(int value) {

        ProviderType retVal;

        switch (value) {

            case 0:
                retVal = ProviderType.None;
                break;
            case 1:
                retVal = ProviderType.Oracle;
                break;
            case 2:
                retVal = ProviderType.Sql2000;
                break;
            case 3:
                retVal = ProviderType.Sql2005;
                break;
            case 4:
                retVal = ProviderType.Sybase;
                break;
            case 5:
                retVal = ProviderType.Informix;
                break;
            default:
                throw new IllegalArgumentException(String.format("[%1$d] is not identified as valid ProviderType value.", value));
        }

        return retVal;
    }
}
