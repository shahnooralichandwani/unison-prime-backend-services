package com.avanza.core.data.expression;

public class OracleQueryRepresentator extends HqlQueryRepresentator {
    public OracleQueryRepresentator() {
        this(new StringBuilder());
    }

    public OracleQueryRepresentator(StringBuilder builder) {
        super(builder);
    }
}
