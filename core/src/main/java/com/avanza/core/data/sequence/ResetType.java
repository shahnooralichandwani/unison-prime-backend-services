package com.avanza.core.data.sequence;

import com.avanza.core.CoreException;


public enum ResetType {

    Sequence("Sequence"), Daily("Daily"), Monthly("Monthly"), Yearly("Yearly");

    private String value;

    private ResetType(String val) {
        this.value = val;
    }

    public String toString() {
        return this.value;
    }

    public static ResetType fromString(String value) {

        ResetType retVal = null;

        if (value.equalsIgnoreCase("Sequence"))
            retVal = ResetType.Sequence;
        else if (value.equalsIgnoreCase("Daily"))
            retVal = ResetType.Daily;
        else if (value.equalsIgnoreCase("Monthly"))
            retVal = ResetType.Monthly;
        else if (value.equalsIgnoreCase("Yearly"))
            retVal = ResetType.Yearly;
        else
            throw new CoreException("[%1$s] is not recognized as ResetType", value);

        return retVal;
    }
}
