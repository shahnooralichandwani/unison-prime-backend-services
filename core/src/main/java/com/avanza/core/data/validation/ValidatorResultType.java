package com.avanza.core.data.validation;

import com.avanza.core.CoreException;

public enum ValidatorResultType {

    OK(0), ERROR(1);

    private int intValue;

    private ValidatorResultType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static ValidatorResultType fromString(String value) {

        ValidatorResultType retVal = null;

        if (value.equalsIgnoreCase("OK"))
            retVal = ValidatorResultType.OK;
        else if (value.equalsIgnoreCase("ERROR"))
            retVal = ValidatorResultType.ERROR;
        else
            throw new CoreException("[%1$s] is not recognized as Validator Result Type", value);

        return retVal;
    }

    public static ValidatorResultType fromInteger(int value) {

        ValidatorResultType retVal = null;

        if (value == 0)
            retVal = ValidatorResultType.OK;
        else if (value == 1)
            retVal = ValidatorResultType.ERROR;
        else
            throw new CoreException("[%1$s] is not recognized as Validator Result Type", value);

        return retVal;
    }

}
