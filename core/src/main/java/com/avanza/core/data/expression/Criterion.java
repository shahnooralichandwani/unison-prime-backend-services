package com.avanza.core.data.expression;

import java.io.Serializable;
import java.util.List;

public abstract class Criterion implements QueryRepresentable, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -6572771086298623204L;

    public static final String WHERE_CLAUSE = " WHERE ";

    protected OperatorType operator;
    private Criterion parent;

    protected Criterion(OperatorType operator) {

        this.operator = operator;
    }

    public OperatorType getOperator() {

        return this.operator;
    }

    public void setOperator(OperatorType operator) {
        this.operator = operator;
    }

    public boolean isSimple() {

        return !(this.operator.isComplex());
    }

    public boolean isComplex() {

        return this.operator.isComplex();
    }

    public void setParent(Criterion parent) {
        this.parent = parent;
    }

    public Criterion getParent() {
        return parent;
    }

    public abstract StringBuilder toString(StringBuilder builder);


    @Deprecated
    public static <T> Criterion greater(String field, T value) {
        return new SimpleCriterion<T>(OperatorType.Gt, field, "", value);
    }

    @Deprecated
    public static <T> Criterion less(String field, T value) {
        return new SimpleCriterion<T>(OperatorType.Lt, field, "", value);
    }

    @Deprecated
    public static <T> Criterion equal(String field, T value) {
        return new SimpleCriterion<T>(OperatorType.Eq, field, "", value);
    }

    @Deprecated
    public static Criterion fieldEqual(String field1, String field2) {
        return new SimpleCriterion<Column>(OperatorType.Eq, field1, "", new Column(field2));
    }

    @Deprecated
    public static <T> Criterion notEqual(String field, T value) {
        return new SimpleCriterion<T>(OperatorType.NEq, field, "", value);
    }

    @Deprecated
    public static <T> Criterion greaterOrEqual(String field, T value) {
        return new SimpleCriterion<T>(OperatorType.GtEq, field, "", value);
    }

    @Deprecated
    public static <T> Criterion lessOrEqual(String field, T value) {
        return new SimpleCriterion<T>(OperatorType.LtEq, field, "", value);
    }

    @Deprecated
    public static <T> Criterion like(String field, T value) {
        return new SimpleCriterion<T>(OperatorType.Like, field, "", value);
    }

    @Deprecated
    public static Criterion nullCriterion(String field) {
        return new SimpleCriterion<Object>(OperatorType.Null, field, "", null);
    }

    @Deprecated
    public static Criterion notNullCriterion(String field) {
        return new SimpleCriterion<Object>(OperatorType.NotNull, field, "", null);
    }

    @Deprecated
    public static <T> Criterion notIn(String field, List<T> value) {
        return new ListCriterion<T>(OperatorType.NotIn, field, value);
    }

    public static Criterion or(Criterion lhs) {
        return new ComplexCriterion(OperatorType.Or, lhs);
    }

    public static Criterion or(Criterion lhs, Criterion rhs) {

        return new ComplexCriterion(OperatorType.Or, lhs, rhs);
    }

    public static Criterion and(Criterion lhs) {
        return new ComplexCriterion(OperatorType.And, lhs);
    }

    public static Criterion and(Criterion lhs, Criterion rhs) {

        return new ComplexCriterion(OperatorType.And, lhs, rhs);
    }

    @Deprecated
    public static <T> Criterion in(String field, List<T> value) {
        return new ListCriterion<T>(OperatorType.In, field, value);
    }

    /*
     * Shahbaz: Bug removed search by arabic data not working NLike Added
     * Operator
     */

    @Deprecated
    public static <T> Criterion Nlike(String field, T value) {
        return new SimpleCriterion<T>(OperatorType.NLike, field, "", value);
    }

    public static <T> Criterion between(String field, T lowvalue, T hivalue) {

        return new BetweenCriterion<T>(OperatorType.Between, field, lowvalue, hivalue);
    }

    /*
     * public String ToString() { }
     *
     * internal String ToString(String alias, StringBuilder builder, bool
     * encloseBracket, DbProviderType providerType) { if (encloseBracket)
     * builder.Append('(');
     *
     * if (alias.Length > 0) { String strName = this._fieldName; //int index =
     * strName.LastIndexOf('.'); //if(index != -1) // strName =
     * strName.SubString(index+1);
     *
     * builder.Append(alias).Append('.').Append(strName); } else {
     * builder.Append(this._fieldName); }
     *
     * builder.Append(' ').Append(Criterion.ToString(this._op)).Append(' ');
     *
     * if ((this._op == OperatorType.NotIn) || (this._op == OperatorType.In)) {
     * builder.Append('(').Append(this._value.ToString()).Append(')'); } else if
     * (this._op == OperatorType.Or) {
     * builder.Append('(').Append(this._value.ToString()).Append(')'); } else if
     * (this._op != OperatorType.Null) {
     * builder.Append(Database.ToString(this._value,
     * Type.GetTypeCode(this._value.GetType()), providerType)); }
     *
     * if (this._op == OperatorType.Between) {
     * builder.Append(" And ").Append(Criterion.ValueString(this._highValue)); }
     *
     * if (encloseBracket) builder.Append(')');
     *
     * return builder.ToString(); }
     *
     * private static String ValueString(object value) { String strVal =
     * String.Empty; bool isString = (value is String);
     *
     * if (isString) strVal += "'";
     *
     * if (value == null) strVal += "null"; else if (value is DateTime) { strVal
     * += ((DateTime)value).ToString(Database.NetDateFormat); } else strVal +=
     * value.ToString();
     *
     * if (isString) strVal += "'";
     *
     * return strVal; }
     */
}
