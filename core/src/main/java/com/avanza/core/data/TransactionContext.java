package com.avanza.core.data;

import java.util.Hashtable;

public class TransactionContext {

    private Hashtable<String, Session> sessionList;

    TransactionContext() {

        this.sessionList = new Hashtable<String, Session>();
    }

    public void addSession(String key, Session item) {

        this.sessionList.put(key, item);
    }

    public Session getSession(String key) {

        return this.sessionList.get(key);
    }

    public void removeSession(String key) {

        this.sessionList.remove(key).release();
    }

    public boolean hasSession(String key) {

        return this.sessionList.containsKey(key);
    }

    public void release() {

        System.out.println("Releasing Sessions...  " + Thread.currentThread().getId());
        this.rollback();
        for (Session item : this.sessionList.values()) {
            try {
                item.release();
            } catch (Exception ex) {
            }
        }
        this.sessionList.clear();
    }

    public void clear() {
        release();
    }

    public void beginTransaction() {
        for (Session item : this.sessionList.values()) {
            item.beginTransaction();
        }
    }

    public void commit() {
        for (Session item : this.sessionList.values()) {
            try {
                item.commit();
            } catch (Exception ex) {
            }
        }
    }

    public void rollback() {
        for (Session item : this.sessionList.values()) {
            try {
                item.rollback();
            } catch (Exception ex) {
            }

        }
    }
}
