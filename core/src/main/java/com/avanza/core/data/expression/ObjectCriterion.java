package com.avanza.core.data.expression;

import com.avanza.core.util.StringHelper;

/**
 * joins are moved to From class.
 */
@Deprecated
public class ObjectCriterion extends Criterion {

    private String joiningAlias;
    private From from;
    private CriterionTree criterion;
    private JoinType joinType = JoinType.Natural;

    public JoinType getJoinType() {
        return joinType;
    }

    public ObjectCriterion(String joiningAlias, Class<?> clazz) {

        super(OperatorType.None);
        this.joiningAlias = joiningAlias;
        this.from = new From(clazz);
        this.criterion = new CriterionTree();
    }

    public ObjectCriterion(String joiningAlias, String name) {

        super(OperatorType.None);
        this.joiningAlias = joiningAlias;
        this.from = new From(name);
        this.criterion = new CriterionTree();
    }


    public ObjectCriterion(String joiningAlias, Class<?> clazz, String alias) {

        super(OperatorType.None);
        this.joiningAlias = joiningAlias;
        this.from = new From(clazz, alias);
        this.criterion = new CriterionTree();
    }

    public ObjectCriterion(String joiningAlias, String name, String alias) {

        super(OperatorType.None);
        this.joiningAlias = joiningAlias;
        this.from = new From(name, alias);
        this.criterion = new CriterionTree();
    }

    public ObjectCriterion(String joiningAlias, String name, String alias, JoinType joinType) {

        super(OperatorType.None);
        this.joiningAlias = joiningAlias;
        this.from = new From(name, alias);
        this.criterion = new CriterionTree();
        this.joinType = joinType;
    }

    public From getFrom() {

        return this.from;
    }

    public Class<?> getType() {

        return this.from.getType();
    }

    public Criterion getRootCriterion() {

        return this.criterion.getRoot();
    }

    public void setRootCriterion(Criterion rootCriterion) {

        this.criterion.setRoot(rootCriterion);
    }

    public void addCriterion(Criterion item) {

        this.criterion.addCriterion(item);
    }

    public void addCriterionOr(Criterion item) {

        this.criterion.addCriterionOr(item);
    }

    public StringBuilder toString(StringBuilder builder) {

//		builder.append(StringHelper.OPEN_PAREN);
//		builder.append(StringHelper.SPACE).append(this.operator.toToken()).append(StringHelper.SPACE);

        return builder;
    }

    public String toString() {

        StringBuilder builder = new StringBuilder(200);
        return this.toString(builder).toString();
    }

    public String getJoiningAlias() {
        return joiningAlias;
    }

    public void setJoiningAlias(String joiningAlias) {
        this.joiningAlias = joiningAlias;
    }

    public String getObjectName() {
        if (StringHelper.isEmpty(this.joiningAlias))
            return this.from.getName();
        else
            return this.joiningAlias + "." + this.from.getName();
    }

    public boolean represent(QueryRepresentator representator, String opener, String closer) {
        // TODO Auto-generated method stub
        return false;
    }
}
