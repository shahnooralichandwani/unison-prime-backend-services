package com.avanza.core.data.sequence;

import com.avanza.core.util.configuration.ConfigSection;

import java.util.List;

public interface SequenceGenerator {

    //sequence info has the sequence value in it.
    void load(ConfigSection section, SequenceInfo sequenceInfo);

    void dispose();

    String getNextValue(String... keyParts);

    long getNextValueAsLong();

    int getNextValueasInt();

    List<String> getNextValue(int bucketSize, String... keyParts);

    List<Long> getNextValueAsLong(int bucketSize);

    List<Integer> getNextValueasInt(int bucketSize);

    SequenceInfo getSequenceInfo();
}
