package com.avanza.core.data;

import com.avanza.core.data.expression.ExpressionException;

public interface DataFilterAttributes {
    public enum DataFilterAttributeType {
        None("None"), Aggregate("Aggregate"), SubFilter("SubFilter");

        private String value;

        DataFilterAttributeType(String name) {
            this.value = name;
        }

        public String getValue() {
            return this.value;
        }

        public static DataFilterAttributeType parseToken(String val) {
            for (DataFilterAttributeType s : values()) {
                if (s.value.trim().equalsIgnoreCase(val.trim()))
                    return s;
            }
            throw new ExpressionException("Token for %s is not defined.", val);
        }
    }

    String getAtrributeId();

    String getAttributeName();

    String getValue();

    String getOperator();

    String getJoinType();

    int getAttributePosition();

    DataFilterAttributeType getAttributeFilterType();

    DataFilter getSubFilter();

    String getAggregationType();
}
