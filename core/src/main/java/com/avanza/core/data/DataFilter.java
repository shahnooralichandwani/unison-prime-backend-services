package com.avanza.core.data;

import java.util.List;

public interface DataFilter {

    String getEntityName();

    String getId();

    List<? extends DataFilterAttributes> getAttributeList();

    String getDefaultJoinType();
}
