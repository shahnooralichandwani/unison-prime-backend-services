package com.avanza.core.data.db;

import com.avanza.core.data.Session;
import com.avanza.core.data.db.sql.SqlParameter;
import com.avanza.core.sdo.DataException;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.DataType;
import com.avanza.core.util.StringHelper;

import java.sql.*;
import java.util.Date;
import java.util.List;
import java.util.Vector;

public class DbConnection implements Session {

    private Connection dbConn;
    private DbInfo dbInfo;
    // This List keep references of all open statements on this connection
    private List<Statement> statements = new Vector<Statement>();
    private boolean isPrimarySource = true;

    DbConnection(DbInfo dbInfo, Connection con) {
        // TODO check for connections open for long time
        this.dbInfo = dbInfo;
        this.dbConn = con;
    }

    public ResultSet getResults(String cmd) {

        try {
            //Statement will remain open and so the result sets until caller calls close on DBConnection.
			/*
			 * 	Updated: Bushra
			 *  Reason: to find the number of record in Result set   
				Statement stmt = this.dbConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			*/
            Statement stmt = this.dbConn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statements.add(stmt);
            return stmt.executeQuery(cmd);
        } catch (SQLException e) {

            throw new DataException(e, "Exception occured while executing SQL: " + cmd);
        }
    }

    public CallableStatement getProcedureStatement(String procedureCmd, List<SqlParameter> sqlParameters) {
        try {
            CallableStatement pStmt = prepareStoredProcedure(procedureCmd, sqlParameters);
            return pStmt;
        } catch (SQLException e) {

            throw new DataException(e, "Exception occured while executing Stored Procedure: " + procedureCmd);
        }
    }

    public int execNonQuery(String cmd) {

        try {
            //Statement will remain open and so the result sets until caller calls close on DBConnection.
            //Here Statement is left open so caller can get generated IDs.
            Statement stmt = this.dbConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            statements.add(stmt);
            return stmt.executeUpdate(cmd, 1);
        } catch (SQLException e) {

            throw new DataException(e, "Exception occured while executing sql on database:%s SQL: " + cmd, dbInfo.getName());
        }
    }

    public ResultSet getResults(String cmd, List<Object> params) {

        try {

            PreparedStatement pStmt = this.dbConn.prepareStatement(cmd, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            statements.add(pStmt);
            int i = 0;
            for (Object obj : params) {

                i++;
                if (obj instanceof Integer) pStmt.setInt(i, ((Integer) obj).intValue());
                else if (obj instanceof String) pStmt.setString(i, ((String) obj));
                else if (obj instanceof Float) pStmt.setFloat(i, ((Float) obj).floatValue());
                else if (obj instanceof Double) pStmt.setDouble(i, ((Double) obj).doubleValue());
                else if (obj instanceof Long) pStmt.setLong(i, ((Long) obj).longValue());
                else if (obj instanceof Short) pStmt.setShort(i, ((Short) obj).shortValue());
                else if (obj instanceof Byte) pStmt.setByte(i, ((Byte) obj).byteValue());
                else if (obj instanceof Date) pStmt.setDate(i, ((new java.sql.Date(((Date) obj).getTime()))));
            }
            return pStmt.executeQuery();

        } catch (SQLException e) {

            throw new DataException(e, "Exception occured while executing sql on database:%s SQL: " + cmd, dbInfo.getName());
        }
    }

    public int execQuery(String cmd, List<Object> params) {

        try {

            PreparedStatement pStmt = this.prepareExecQuery(cmd, params);
            return pStmt.executeUpdate();

        } catch (SQLException e) {

            throw new DataException(e, "Exception occured while executing sql on database:%s SQL: " + cmd, dbInfo.getName());
        }
    }

    public PreparedStatement prepareExecQuery(String cmd, List<Object> params) {

        try {

            PreparedStatement pStmt = this.dbConn.prepareStatement(cmd, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            statements.add(pStmt);
            int i = 0;
            for (Object obj : params) {

                i++;
                if (obj instanceof Integer) pStmt.setInt(i, ((Integer) obj).intValue());
                else if (obj instanceof String) pStmt.setString(i, ((String) obj));
                else if (obj instanceof Float) pStmt.setFloat(i, ((Float) obj).floatValue());
                else if (obj instanceof Double) pStmt.setDouble(i, ((Double) obj).doubleValue());
                else if (obj instanceof Long) pStmt.setLong(i, ((Long) obj).longValue());
                else if (obj instanceof Short) pStmt.setShort(i, ((Short) obj).shortValue());
                else if (obj instanceof Byte) pStmt.setByte(i, ((Byte) obj).byteValue());
                else if (obj instanceof Date) pStmt.setTimestamp(i, ((new java.sql.Timestamp(((Date) obj).getTime()))));
                else if (obj instanceof Boolean) pStmt.setBoolean(i, ((new Boolean(((Boolean) obj)))));
            }
            return pStmt;

        } catch (SQLException e) {

            throw new DataException(e, "Exception occured while executing sql on database:%s SQL: " + cmd, dbInfo.getName());
        }
    }

    public ResultSet getProcedureResults(String procedureCmd, List<SqlParameter> sqlParameters) {
        try {
            CallableStatement pStmt = prepareStoredProcedure(procedureCmd, sqlParameters);

            return pStmt.executeQuery();

        } catch (SQLException e) {

            throw new DataException(e, "Exception occured while executing on database:%s Stored Procedure: " + procedureCmd, this.dbInfo.getName());
        }
    }

    public CallableStatement prepareStoredProcedure(String procedureCmd, List<SqlParameter> sqlParameters) throws SQLException {
        if (StringHelper.isEmpty(procedureCmd))
            throw new DataException("Exception occured while executing Stored Procedure, specify the procedure name.");

        CallableStatement pStmt = this.dbConn.prepareCall(procedureCmd, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        statements.add(pStmt);

        if (sqlParameters != null) {
            int idx = 0;
            for (SqlParameter sqlParameter : sqlParameters) {

                idx++;
                if (sqlParameter != null) {
                    if (!sqlParameter.getParamType().isOutput()) {
                        Object obj = ConvertUtil.ConvertTo(sqlParameter.getValue(), sqlParameter.getDataType().getJavaType());
                        if (obj instanceof Integer) pStmt.setInt(idx, ((Integer) obj).intValue());
                        else if (obj instanceof String) pStmt.setString(idx, ((String) obj));
                        else if (obj instanceof Float) pStmt.setFloat(idx, ((Float) obj).floatValue());
                        else if (obj instanceof Double) pStmt.setDouble(idx, ((Double) obj).doubleValue());
                        else if (obj instanceof Long) pStmt.setLong(idx, ((Long) obj).longValue());
                        else if (obj instanceof Short) pStmt.setShort(idx, ((Short) obj).shortValue());
                        else if (obj instanceof Byte) pStmt.setByte(idx, ((Byte) obj).byteValue());
                        else if (obj instanceof Date) pStmt.setDate(idx, new java.sql.Date(((Date) obj).getTime()));
                    }
                    if (!sqlParameter.getParamType().isInput()) {
                        pStmt.registerOutParameter(idx, sqlParameter.getDataType().getSqlType());
                    }
                }
                if (sqlParameter == null)
                    pStmt.setNull(idx, DataType.String.getSqlType());
            }
        }
        return pStmt;
    }

    public <T> T executeProcedure(String procedureCmd, List<SqlParameter> sqlParameters) {
        try {
            CallableStatement pStmt = prepareStoredProcedure(procedureCmd, sqlParameters);

            boolean resultType = pStmt.execute();
            if (!resultType) {
                return (T) new Integer(pStmt.getUpdateCount());
            } else
                return (T) pStmt.getResultSet();

        } catch (SQLException e) {

            throw new DataException(e, "Exception occured while executing on database:%s Stored Procedure: " + procedureCmd, this.dbInfo.getName());
        }
    }

    public void closeStatements() {

        for (Statement stmt : statements) {

            try {
                stmt.close();


            } catch (Exception e) {

                throw new DataException(e, "Exception occured while closing statement");
            }
        }
    }

    public void close() {

        for (Statement stmt : statements) {

            try {
                stmt.close();


            } catch (Exception e) {

                throw new DataException(e, "Exception occured while closing statement");
            }
        }
        try {
            this.dbConn.close();
        } catch (Exception e) {
            throw new DataException(e, "Exception occured while closing Connection");
        }
        //this.dbInfo.returnConnection(this.dbConn);
    }

    public void setAutoCommit(boolean autocommit) {

        try {
            if (this.dbConn != null)
                this.dbConn.setAutoCommit(autocommit);
        } catch (SQLException e) {
            throw new DataException(e, "Exception occoured while setting autoCommit mode.");
        }
    }

    public boolean execute(String cmd) {
        Statement stmt = null;
        try {
            //Statement will remain open and so the result sets until caller calls close on DBConnection.
            //Here Statement is left open so caller can get generated IDs.
            stmt = this.dbConn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            statements.add(stmt);
            return stmt.execute(cmd);
        } catch (SQLException e) {

            throw new DataException(e, "Exception occured while executing sql on database:%s SQL: " + cmd, dbInfo.getName());
        } finally {
            if (stmt != null)
                try {
                    stmt.close();
                } catch (SQLException e) {
                    throw new DataException(e, "Exception occured while closing Statement");
                }
        }
    }

    public Connection getConnection() {
        return this.dbConn;
    }

    public void beginTransaction() {
        // For Transaction management @ JDBC broker
        setAutoCommit(false);

    }

    public void commit() {

        try {
            if (this.dbConn != null)
                this.dbConn.commit();
        } catch (SQLException e) {
            throw new DataException(e, "Exception occoured while commit transaction.");
        }
    }

    public void release() {

        try {
            if (this.dbConn != null)
                this.dbConn.close();
        } catch (SQLException e) {
            throw new DataException(e, "Exception occoured while release transaction.");
        }
    }

    public void rollback() {

        try {
            if (this.dbConn != null)
                this.dbConn.rollback();
        } catch (SQLException e) {
            throw new DataException(e, "Exception occoured while rollback transaction.");
        }
    }

    public <T> T value() {
        return null;
    }

    public DbInfo getDbInfo() {
        return this.dbInfo;
    }

    void setPrimarySource(boolean isPrimary) {
        isPrimarySource = isPrimary;
    }

    boolean isPrimarySource() {
        return isPrimarySource;
    }

    public boolean isTransactionActive() {
        return false;
    }

    @Override
    public void notifyOnTaskFinished() {
        // TODO Auto-generated method stub

    }
}
