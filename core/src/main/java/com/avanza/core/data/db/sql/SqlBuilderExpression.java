package com.avanza.core.data.db.sql;


public class SqlBuilderExpression {

    private StringBuilder selectClause = new StringBuilder();
    private StringBuilder fromClause = new StringBuilder();
    private StringBuilder whereClause = new StringBuilder();
    private StringBuilder orderByClause = new StringBuilder();

    public <T> SqlBuilderExpression appendOrderBy(T value) {
        this.orderByClause.append(value);
        return this;
    }

    public <T> SqlBuilderExpression appendSelect(T value) {
        this.selectClause.append(value);
        return this;
    }

    public <T> SqlBuilderExpression appendFrom(T value) {
        this.fromClause.append(value);
        return this;
    }

    public <T> SqlBuilderExpression appendWhere(T value) {
        this.whereClause.append(value);
        return this;
    }

    StringBuilder getFromClause() {
        return fromClause;
    }

    StringBuilder getSelectClause() {
        return selectClause;
    }

    StringBuilder getWhereClause() {
        return whereClause;
    }

    public String getSql() {
        return new StringBuilder(this.selectClause).append(this.fromClause).append(this.whereClause).append(this.orderByClause).toString();
    }

    public StringBuilder getOrderByClause() {
        return orderByClause;
    }
}
