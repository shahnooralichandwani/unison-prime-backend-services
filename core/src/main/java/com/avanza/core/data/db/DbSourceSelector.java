package com.avanza.core.data.db;

import com.avanza.core.util.configuration.ConfigSection;

public interface DbSourceSelector {

    void init(ConfigSection conf, DbSource source);

    String getSource();

    Boolean isPrimarySource();
}
