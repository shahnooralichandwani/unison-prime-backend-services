package com.avanza.core.data.db;

import com.avanza.core.CoreException;
import com.avanza.core.util.Guard;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.Wrapper;
import com.avanza.core.util.configuration.ConfigSection;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DbSource {

    private static Logger logger = Logger.getLogger(DbSource.class);

    private String name;
    private BaseDbSelector selector;
    private SelectorType selectType;
    private Class<?> selectImpl;
    private DbInfo primaryDbInfo;
    private DbInfo secDbInfo;
    private Map<String, DbInfo> infoMap;

    public DbConnection getDbConnection() {
        DbConnection retValue = null;
        DbInfo dbInfo = getDbInfo();
        if (dbInfo != null) {
            retValue = dbInfo.getDbConnection();
            if (this.selector != null) {
                retValue.setPrimarySource(this.selector.isPrimarySource());
            }
        }
        return retValue;
    }

    public DbInfo getDbInfo() {
        DbInfo retValue = null;

        if (this.selector == null) {
            retValue = this.primaryDbInfo;
        } else {
            String Source = this.selector.getSource();
            if (StringHelper.isEmpty(Source))
                retValue = this.primaryDbInfo;
            else
                retValue = this.infoMap.get(Source);
        }
        return retValue;
    }

    public Boolean isPrimarySource() {
        return (this.selector == null) ? false : this.selector.isPrimarySource();
    }

    public DbSource(String name, String selectType, String selectImpl) {

        this.name = name;
        this.selectType = SelectorType.valueOf(selectType);
        if (this.selectType == SelectorType.Custom && StringHelper.isNotEmpty(selectImpl)) {
            try {
                this.selectImpl = Class.forName(selectImpl);
            } catch (ClassNotFoundException e) {
                logger.logError("Unable to Load the Class for the Custom DbSelectorImpl, %s", selectImpl);
            }
        }
        this.infoMap = new HashMap<String, DbInfo>();
    }

    public void init(ConfigSection conf) {

        Guard.checkNull(conf, "conf");

        if (selectType != SelectorType.None) {
            this.selector = BaseDbSelector.getDbSelector(selectType, selectImpl);
            this.selector.init(conf.getChild("select-params"), this);
        }

        List<ConfigSection> databases = conf.getChildSections("database");
        if (selectType == SelectorType.None) {
            if (databases.size() != 1)
                throw new CoreException("Unable to Load the DbSource %s, invalid databases defined for SelectType 'None'", this.name);

            DbInfo dbInfo = this.fromConfig(databases.get(0));
            this.infoMap.put(dbInfo.getName(), dbInfo);
            this.primaryDbInfo = dbInfo;
            this.secDbInfo = dbInfo;
        } else {
            for (ConfigSection db : databases) {
                DbInfo dbInfo = this.fromConfig(db);
                if (dbInfo.getName().equalsIgnoreCase(this.selector.getPrimary()))
                    this.primaryDbInfo = dbInfo;
                else if (dbInfo.getName().equalsIgnoreCase(this.selector.getSecondary()))
                    this.secDbInfo = dbInfo;
                else
                    throw new CoreException("Error in Loading the DbSource section, DbInfos defined must match the primary or secondary db-selector, %s.", this.name);
                this.infoMap.put(dbInfo.getName(), dbInfo);
            }
        }
    }

    private DbInfo fromConfig(ConfigSection dbConfig) {
        DbInfo dbInfo;
        String key = dbConfig.getTextValue(DbInfo.XML_KEY);
        Wrapper<String> wrappedUserName = new Wrapper<String>();
        Wrapper<String> wrappedPassword = new Wrapper<String>();
        StartupDatabase.getStartupDatabase().getCredentialsByKey(key, wrappedUserName, wrappedPassword);

        String name = dbConfig.getTextValue(DbInfo.XML_NAME);
        String driver = dbConfig.getTextValue(DbInfo.XML_DRIVER);
        ProviderType type = ProviderType.parse(dbConfig.getTextValue(DbInfo.XML_PROVIDER));

        String connUri = dbConfig.getTextValue(DbInfo.XML_CONN_URI);
        String propsVal = dbConfig.getValue(DbInfo.XML_POOL_PARAMS, StringHelper.EMPTY);
        boolean autoCommit = Boolean.parseBoolean(dbConfig.getValue(DbInfo.XML_AUTO_COMMIT, "true"));

        dbInfo = new DbInfo(name, key, driver, type, wrappedUserName.unwrap(), wrappedPassword.unwrap(), connUri, propsVal, autoCommit);
        return dbInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DbSourceSelector getSelector() {
        return selector;
    }

    public void setSelector(BaseDbSelector selector) {
        this.selector = selector;
    }

    public SelectorType getSelectType() {
        return selectType;
    }

    public void setSelectType(SelectorType selectType) {
        this.selectType = selectType;
    }

    public Class<?> getSelectImpl() {
        return selectImpl;
    }

    public void setSelectImpl(Class<?> selectImpl) {
        this.selectImpl = selectImpl;
    }

    public DbInfo getPrimaryDbInfo() {
        return primaryDbInfo;
    }

    public void setPrimaryDbInfo(DbInfo primaryDbInfo) {
        this.primaryDbInfo = primaryDbInfo;
    }

    public DbInfo getSecDbInfo() {
        return secDbInfo;
    }

    public void setSecDbInfo(DbInfo secDbInfo) {
        this.secDbInfo = secDbInfo;
    }
}
