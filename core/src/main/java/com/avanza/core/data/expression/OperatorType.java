package com.avanza.core.data.expression;

public enum OperatorType {

    None(""), Gt(">"), Lt("<"), Eq("="), NEq("<>"), GtEq(">="), LtEq("<="), Not("Not"), And("And"), Or("Or"), Like("Like"), Null("is null"), NotNull("is not null"), Between("Between"), NotIn("not in"), In("in"), Natural(","), StartLike("StartLike"), NLike("NLike");

    public static final String KEYWORDS = "=><";

    public static final OperatorType[] SIMPLE_OP = new OperatorType[]{Gt, Lt,
            Eq, NEq, GtEq, LtEq, Like, Null, NotNull, StartLike, NLike};
    public static final OperatorType[] BINARY_OP = new OperatorType[]{Between};
    public static final OperatorType[] COMPLEX_OP = new OperatorType[]{And,
            Or, Not};
    public static final OperatorType[] LIST_OP = new OperatorType[]{In, NotIn};
    public static final OperatorType[] JOIN_OP = new OperatorType[]{Natural};

    final String value;

    public String getValue() {
        return value.trim();
    }

    private OperatorType(String val) {
        value = val;
    }

    public static OperatorType parseToken(String val) {
        for (OperatorType s : values()) {
            if (s.value.trim().equalsIgnoreCase(val.trim()))
                return s;
        }
        throw new ExpressionException("Token for %s is not defined.", val);
    }

    public String toToken() {

        if (this.equals(NLike)) {
            return "Like  N";
        } else {
            return String.format(" %s ", value.trim());
        }
    }

    private boolean checkExist(OperatorType[] list) {

        boolean retVal = false;

        for (int idx = 0; idx < list.length; idx++) {

            if (list[idx] == this) {
                retVal = true;
                break;
            }
        }

        return retVal;
    }

    public boolean isSimple() {

        return this.checkExist(OperatorType.SIMPLE_OP);
    }

    public boolean isList() {

        return this.checkExist(OperatorType.LIST_OP);
    }

    public boolean isComplex() {

        return this.checkExist(OperatorType.COMPLEX_OP);
    }

    public boolean isBinary() {

        return this.checkExist(OperatorType.BINARY_OP);
    }

    public boolean isJoin() {

        return this.checkExist(OperatorType.JOIN_OP);
    }
}
