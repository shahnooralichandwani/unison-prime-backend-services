package com.avanza.core.data;

import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.configuration.ConfigSection;

import java.io.Serializable;
import java.util.List;

/**
 * Summary description for DataBroker
 */

public interface DataBroker {
    public enum SessionScope {
        TASK_BASED("task"), REQUEST_BASED("request");
        final String value;

        private SessionScope(String val) {
            value = val;
        }

        public static SessionScope fromValue(String val) {
            for (SessionScope s : values()) {
                if (s.value.equals(val)) return s;
            }
            return null;
        }

    }

    ProviderType getProviderType();

    <T> List<T> find(String query);

    <T> List<T> find(Search query);

    <T> List<T> findItemsByRange(Search query, int pageNo, int pageSize,
                                 String orderBy, boolean isAsc, int[] returnTotalRowCount);

    <T, ID extends Serializable> T findById(Class<T> t, ID id);

    <ID, T extends DataObject> T findById(String entityId, ID id) throws DataSourceException;

    <T> List<T> findAll(Class<T> item);

    <T> void persist(T item);

    <T> void add(T item);

    <T> void update(T item);

    <T> void delete(T item);


    int deleteBySearch(Search search);

    <T> T getSession();

    <T> void persistAll(List<T> items);

    String getDataSource();

    void synchronize();

    /**
     * To provide Initialization Hook point for the implemented Broker
     */
    void load(ConfigSection configSection);

    /**
     * To provide unloading Hook point for the implemented Broker
     */
    void dispose();

    /**
     * Get row count on provided critera basis
     *
     * @param criteria
     * @return int count
     * @author shoaib.rehman
     */
    int getCriteriaCount(Search criteria);
}
