package com.avanza.core.data.expression;

import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Column extends Expression implements QueryRepresentable {

    public enum Function {
        Sum("Sum"), Count("Count"), Max("Max"), Min("Min");
        private String value;

        Function(String name) {
            this.value = name;
        }

        public String getValue() {
            return this.value;
        }

        public static Function parseToken(String val) {
            for (Function s : values()) {
                if (s.value.trim().equalsIgnoreCase(val.trim()))
                    return s;
            }
            throw new ExpressionException("Token for %s is not defined.", val);
        }

    }

    public static final String SELECT_CLAUSE = "SELECT ";
    public static final String AS_CLAUSE = " AS ";

    private String fromName;
    private String alias;
    private Function function;
    private Object value;

    public Column(String name, String alias) {
        this.init(name, null, alias, null, null);
    }

    public Column(String name, String fromName, String alias) {
        this.init(name, fromName, alias, null, null);
    }

    public Column(String name, String fromName, String alias, Function function) {
        this.init(name, fromName, alias, function, null);
    }

    public Column(String name) {
        this.init(name, null, null, null, null);
    }

    public Column(String name, String alias, Object value) {
        this.init(name, null, alias, null, value);
    }

    public Column(String name, String fromName, String alias, Object value) {
        this.init(name, fromName, alias, null, value);
    }

    public Column(String name, String fromName, String alias, Function function, Object value) {
        this.init(name, fromName, alias, function, value);
    }

    public Column(String name, Object value) {
        this.init(name, null, null, null, value);
    }

    private void init(String name, String fromName, String alias, Function function, Object value) {

        Guard.checkNullOrEmpty(name, "field name");
        StringTokenizer tokenizer = new StringTokenizer(name, StringHelper.DOT);

        if (StringHelper.isEmpty(fromName)) {
            fromName = tokenizer.nextToken();
            if (!tokenizer.hasMoreTokens()) {
                fromName = null;
            } else {
                name = name.substring(fromName.length() + 1);
            }
        }
        this.name = name;
        this.fromName = fromName;
        this.alias = alias;
        this.function = function;
        this.value = value;
    }

    public Function getFunction() {
        return function;
    }

    public Object getValue() {
        return value;
    }

    public String getAlias() {

        return this.alias;
    }

    public void setAlias(String alias) {

        this.alias = alias;
    }

    public String getFromName() {

        return this.fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public boolean hasFromName() {

        return ((this.fromName != null) && (this.fromName.length() > 0));
    }

    public String getFullName() {
        return (hasFromName()) ? fromName + "." + name : name;
    }

    public boolean hasAlias() {

        return ((this.alias != null) && (this.alias.length() > 0));
    }

    public String toString() {
        StringBuilder builder = new StringBuilder(200);

        return this.toString(builder).toString();
    }

    public StringBuilder toString(StringBuilder builder) {
        represent(new HqlQueryRepresentator(builder), "", "");
        return builder;
    }

    public boolean represent(QueryRepresentator representator, String opener, String closer) {
        return representator.representColumn(this);
    }

    public static List<Column> ParseClause(String select) {
        Guard.checkNullOrEmpty(select, "Column.ParseClause(String)");

        if (StringHelper.startsWithIgnoreCase(select, Column.SELECT_CLAUSE))
            select = select.substring(Column.SELECT_CLAUSE.length()).trim();

        String[] columnList = select.split(StringHelper.COMMA);
        ArrayList<Column> retVal = new ArrayList<Column>(columnList.length);

        try {
            for (int index = 0; index < columnList.length; index++) {
                String orderTemp = columnList[index].trim();

                if (StringHelper.isNotEmpty(orderTemp)) {

                    int idx = StringHelper.indexOfIgnoreCase(orderTemp, Column.AS_CLAUSE);

                    if (idx != -1) {

                        String name = orderTemp.substring(0, idx).trim();
                        String alias = orderTemp.substring(idx + Column.AS_CLAUSE.length()).trim();
                        retVal.add(new Column(name, alias));
                    } else {
                        retVal.add(new Column(orderTemp.trim()));
                    }
                }
            }
            return retVal;
        } catch (RuntimeException excep) {
            throw new ExpressionException(excep,
                    "Failed to parse %s as column list for Select clause", select);
        }
    }

}
