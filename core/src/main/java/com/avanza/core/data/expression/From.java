package com.avanza.core.data.expression;

import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;

import java.util.ArrayList;
import java.util.List;

public class From extends Expression implements QueryRepresentable {

    public static final String FROM_CLAUSE = " FROM ";
    private static final String INTERN_FROM = "FROM ";


    private Class<?> type;
    private String alias;
    private List<Join> joins = new ArrayList<Join>();

    public From(Class<?> type) {

        this.init(null, null, type);
    }

    public From(String name, String alias) {

        this.init(name, alias, null);
    }

    public From(String name) {

        this.init(name, null, null);
    }

    public From(Class<?> type, String alias) {

        this.init(null, alias, type);
    }

    private void init(String name, String alias, Class<?> type) {

        if (type != null) {
            this.type = type;
            this.setName(type.getName());
        } else {
            Guard.checkNullOrEmpty(name, "From.init(String, String)");
            this.setName(name);
        }

        this.alias = alias;
    }

    public List<Join> getJoins() {
        return joins;
    }

    public Column createColumn(String colName) {
        Column col;
        if (!StringHelper.isEmpty(getAlias())) {
            col = new Column(getAlias() + "." + colName);
        } else {
            col = new Column(colName);
        }
        return col;
    }

    public Order createOrder(String colName) {
        return createOrder(colName, OrderType.ASC);
    }

    public Order createOrder(String colName, OrderType orderType) {
        Order order;
        if (!StringHelper.isEmpty(getAlias())) {
            order = new Order(getAlias() + "." + colName, orderType);
        } else {
            order = new Order(colName, orderType);
        }
        return order;
    }

    public void addJoin(JoinType joinType, String joinedTable, String alias) {
        if (hasAlias()) {
            From from = new From(getAlias() + "." + joinedTable, alias);
            joins.add(new Join(joinType, from));
        } else {
            throw new RuntimeException("alias is mandatory for joining");
        }
    }

    public Class<?> getType() {

        return this.type;
    }

    /**
     * @return Returns true if the From has its Object Type specified
     */
    public boolean hasType() {
        return (this.type != null);
    }

    /**
     * @return Returns true if the Name is specified for from clause.
     */
    public boolean hasName() {
        return (!"".equalsIgnoreCase(super.name));
    }

    public String getAlias() {

        return this.alias;
    }

    public void setAlias(String alias) {

        this.alias = alias;
    }

    public boolean hasAlias() {
        return ((this.alias != null) && (this.alias.length() > 0));
    }

    public String toString() {

        StringBuilder builder = new StringBuilder(200);
        return this.toString(builder).toString();
    }

    public StringBuilder toString(StringBuilder builder) {

        represent(new HqlQueryRepresentator(builder), "", "");
        return builder;
    }

    public static List<From> ParseClause(String fromClause) {

        if (fromClause != null)
            fromClause = fromClause.trim();

        Guard.checkNullOrEmpty(fromClause, "From.ParseClause(String)");


        if (StringHelper.startsWithIgnoreCase(fromClause, From.INTERN_FROM))
            fromClause = fromClause.substring(From.INTERN_FROM.length()).trim();

        String[] fromList = fromClause.split(StringHelper.COMMA);
        ArrayList<From> retVal = new ArrayList<From>(fromList.length);

        try {
            for (int index = 0; index < fromList.length; index++) {
                String fromTemp = fromList[index].trim();

                if (StringHelper.isNotEmpty(fromTemp)) {

                    int idx = fromTemp.indexOf(StringHelper.SPACE);

                    if (idx != -1) {

                        String name = fromTemp.substring(0, idx).trim();
                        String alias = fromTemp.substring(idx + StringHelper.SPACE.length()).trim();
                        retVal.add(new From(name, alias));
                    } else {
                        retVal.add(new From(fromTemp.trim()));
                    }
                }
            }
            return retVal;
        } catch (RuntimeException excep) {

            throw new ExpressionException(excep, "Failed to parse %s as column list for Select clause", fromClause);
        }
    }

    public boolean represent(QueryRepresentator representator, String opener, String closer) {
        return representator.representFromclause(this);
    }
}
