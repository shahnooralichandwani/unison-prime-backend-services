package com.avanza.core.data.validation;


import java.util.ArrayList;
import java.util.Date;

public class DataValidationFunctions {

    private static ArrayList<String> errorList = new ArrayList<String>(0);

    public DataValidationFunctions() {

    }

    public static ArrayList<String> getErrorList() {
        return errorList;
    }

    public static void addError(String error) {
        errorList.add(error);
    }

    public static void resetErrorList() {
        errorList.clear();
    }

    public static Boolean min(Object arg1, int arg2) {

        if (String.valueOf(arg1).length() >= arg2)
            return true;
		/*else
			addError(ResourceUtil.getInstance().getProperty("data_validation_min", String.valueOf(arg1).length(), arg2));*/
        return false;
    }

    public static Boolean isNotNull(Object arg1) {

        if (arg1 != null)
            return true;
		/*else
			addError(ResourceUtil.getInstance().getProperty("data_validation_null"));*/
        return false;
    }

    public static Boolean isAfter(Date arg1, Date arg2) {
        if (arg1.after(arg2))
            return true;
        /*else
            addError(ResourceUtil.getInstance().getProperty("data_validation_date_after"));*/
        return false;
    }

    public static Boolean isBefore(Date arg1, Date arg2) {
        if (arg1.before(arg2))
            return true;
       /* else
            addError(ResourceUtil.getInstance().getProperty("data_validation_date_before"));*/
        return false;
    }

}
