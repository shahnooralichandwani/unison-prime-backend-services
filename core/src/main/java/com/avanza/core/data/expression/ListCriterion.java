package com.avanza.core.data.expression;

import com.avanza.core.data.db.sql.SqlKeywords;
import com.avanza.core.util.EscapeUtil;
import com.avanza.core.util.StringHelper;

import java.util.Arrays;
import java.util.List;
//Extends from SimpleCriterion instead of Criteria because 'In' was not working

/**
 * User SimpleCriterion instead.
 */
@Deprecated
public class ListCriterion<T> extends SimpleCriterion<List<? extends Object>> {

    protected List<? extends Object> valueList;
    protected String fieldName;


    public String getFieldName() {
        return fieldName;
    }

    // Full nameof the Field is returned
    public String getCompleteFieldName() {
        return this.fieldName;
    }

    public ListCriterion(OperatorType operator, String fieldName, String value) {
        this(operator, fieldName, Arrays.asList(value));
    }

    public ListCriterion(OperatorType operator, String fieldName) {

        super(operator, fieldName, StringHelper.EMPTY, null);
    }

    public ListCriterion(OperatorType operator, String fieldName, List<? extends Object> valueList) {

        super(operator, fieldName, StringHelper.EMPTY, valueList);
        this.fieldName = fieldName;
        this.valueList = valueList;
    }


    public StringBuilder toString(StringBuilder builder) {

        builder.append(SqlKeywords.OpenBracket);

        builder.append(this.fieldName);
        builder.append(SqlKeywords.Space).append(this.operator.toToken()).append(SqlKeywords.Space);
        builder.append(SqlKeywords.OpenBracket);
        for (Object obj : this.valueList) {
            if (obj instanceof String)
                builder.append("'").append(obj.toString()).append("'");
            else
                builder.append(obj.toString());
            builder.append(SqlKeywords.Comma);
        }
        builder = builder.delete(builder.length() - 2, builder.length());
        builder.append(SqlKeywords.CloseBracket);
        builder.append(SqlKeywords.CloseBracket);
        return builder;
    }

    public String toString() {

        StringBuilder builder = new StringBuilder(200);
        return (this.toString(builder)).toString();
    }


    public List<? extends Object> getValueList() {
        valueList = EscapeUtil.escapeList(valueList);
        return valueList;
    }
}
