package com.avanza.core.data;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.db.DbConnection;
import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.sequence.ResetType;
import com.avanza.core.data.sequence.SequenceInfo;
import com.avanza.core.data.sequence.SequenceValue;
import com.avanza.core.sdo.DataException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SequenceDataBroker implements DataBroker {

    private static final Logger logger = Logger.getLogger(SequenceDataBroker.class);

    private String dataSource;

    public SequenceDataBroker() {
    }

    public <T> List<T> findAll(Class<T> item) {
        return null;
    }

    public <T, ID extends Serializable> T findById(Class<T> t, ID id) {
        return null;
    }

    public <ID, T extends DataObject> T findById(String entityId, ID id) throws DataSourceException {
        return null;
    }

    public <T> T getSession() {
        return null;
    }

    public <T> void delete(T item) {

    }

    public void dispose() {

    }

    public <T> List<T> find(String query) {

        return null;
    }

    public List<SequenceInfo> find(Search query) {

        return find(query.toString());
    }

    public <T> void persist(T item) {

    }

    public void synchronize() {

    }

    public String getDataSource() {
        return dataSource;
    }

    public ProviderType getProviderType() {
        return DataRepository.getDbInfo(dataSource).getType();
    }

    public void load(ConfigSection configSection) {
        this.dataSource = configSection.getTextValue("dataSource");
    }

    public <T> void add(T item) {

        executeQuery(item, false);
    }

    private String getSequenceValueQuery(SequenceValue sequenceValue, Boolean isModified) {

        String query = null;

        if (!isModified) {
            query = "INSERT INTO COUNTER_VALUE(UNIQUE_ID,COUNTER_NAME,NEXT_VALUE,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY) VALUES('" + sequenceValue.getUniqueId() + "', '" + sequenceValue.getCounterName() + "'," + sequenceValue.getNextValue() + ",'"
                    + sequenceValue.getCreatedOn().toLocaleString() + "','" + sequenceValue.getCreatedBy() + "','"
                    + sequenceValue.getUpdatedOn().toLocaleString() + "','" + sequenceValue.getUpdatedBy() + "')";
        } else {
            query = "UPDATE COUNTER_VALUE SET Next_Value=" + sequenceValue.getNextValue() + " WHERE Counter_Name='" + sequenceValue.getCounterName()
                    + "' AND Unique_Id = '" + sequenceValue.getUniqueId() + "'";
        }

        return query;
    }

    private String getSequenceInfoQuery(SequenceInfo sequenceInfo, Boolean isModified) {

        String query = null;

        /*
         * query = "INSERT INTO META_COUNTER VALUES('" + sequenceValue.getCounterName() + "','" + sequenceValue.getUniqueId() + "'," +
         * sequenceValue.getNextValue() + "," + sequenceValue.getCreatedOn() + ",'" + sequenceValue.getCreatedBy() + "'," +
         * sequenceValue.getUpdatedOn() + ",'" + sequenceValue.getUpdatedBy() + "')";
         */

        return query;


    }

    public SequenceValue findSequenceValue(String counterName,
                                           String counterUniqueId, SequenceInfo sequenceInfo) {

        SequenceValue sequenceValue = null;
        ResultSet rst = null;
        Statement stat = null;
        DbConnection dbconn = DataRepository.getDbConnection(this.dataSource);

        try {
            try {
                stat = dbconn.getConnection().createStatement();
                StringBuilder query = new StringBuilder("SELECT * FROM COUNTER_VALUE WHERE COUNTER_NAME = '")
                        .append(counterName.trim()).append("' AND UNIQUE_ID = '").append(counterUniqueId.trim()).append("'");
                logger.logInfo(String.format("Executing Query > %1$s ( SequenceDataBroker.retreiveData) ", query.toString()));
                rst = stat.executeQuery(query.toString());

                try {
                    if (rst.next()) {
                        sequenceValue = new SequenceValue();
                        sequenceValue.setUniqueId(rst.getString("UNIQUE_ID"));
                        sequenceValue.setCounterName(rst
                                .getString("COUNTER_NAME"));
                        sequenceValue.setNextValue(rst.getLong("NEXT_VALUE"));
                        sequenceValue.setCreatedOn(rst.getDate("CREATED_ON"));
                        sequenceValue.setCreatedBy(rst.getString("CREATED_BY"));
                        sequenceValue.setUpdatedOn(rst.getDate("UPDATED_ON"));
                        sequenceValue.setUpdatedBy(rst.getString("UPDATED_BY"));
                    } else {
                        // create a sequence value in db then
                        sequenceValue = new SequenceValue(counterUniqueId,
                                sequenceInfo);
                        this.add(sequenceValue);
                    }
                } catch (SQLException e) {
                    throw new DataSourceException(e, "Exception in closing ResultSet and Statement");
                } finally {
                    rst.close();
                }
            } catch (SQLException e) {
                throw new DataSourceException(e, "Exception Occured in executing query");
            } finally {
                stat.close();
            }

            return sequenceValue;

        } catch (Exception e) {
            throw new DataSourceException(e, "Exception Occured in findSequenceValue");
        } finally {
            dbconn.close();
        }
    }

    public List<SequenceInfo> findSequenceInfos(String query) {

        List<SequenceInfo> result = new ArrayList<SequenceInfo>(0);

        ResultSet rst = null;
        Statement stat = null;
        DbConnection dbconn = DataRepository.getDbConnection(this.dataSource);

        try {

            try {

                stat = dbconn.getConnection().createStatement();
                logger.logInfo(String.format("Executing Query > %1$s ( SequenceDataBroker.retreiveData) ", query));
                rst = stat.executeQuery(query);

                try {
                    while (rst.next()) {

                        SequenceInfo sequenceInfo = new SequenceInfo();
                        sequenceInfo.setCounterName(rst
                                .getString("COUNTER_NAME"));
                        sequenceInfo.setStartValue(rst.getLong("START_VALUE"));
                        sequenceInfo.setMaxValue(rst.getLong("MAX_VALUE"));
                        sequenceInfo.setPrefixValue(rst
                                .getString("PREFIX_VALUE"));
                        sequenceInfo.setIncrementedBy(rst
                                .getShort("INCREMENT_BY"));
                        sequenceInfo.setCyclic(rst.getBoolean("IS_CYCLIC"));
                        sequenceInfo.setImplClass(rst.getString("IMPL_CLASS"));
                        sequenceInfo.setFormatExpr(rst.getString("FORMAT_EXP"));

                        String resetType = rst.getString("RESET_TYPE");

                        if (resetType != null)
                            sequenceInfo.setResetType(resetType);
                        else
                            sequenceInfo.setResetType(ResetType.Sequence
                                    .toString());

                        sequenceInfo.setCreatedOn(rst.getDate("CREATED_ON"));
                        sequenceInfo.setCreatedBy(rst.getString("CREATED_BY"));
                        sequenceInfo.setUpdatedOn(rst.getDate("UPDATED_ON"));
                        sequenceInfo.setUpdatedBy(rst.getString("UPDATED_BY"));

                        sequenceInfo
                                .setSequenceValues(new HashMap<String, SequenceValue>());

                        result.add(sequenceInfo);
                    }
                } catch (SQLException sqle) {
                    throw new DataSourceException(sqle, "Exception in findSequenceInfos");
                } finally {
                    rst.close();
                }
            } catch (SQLException sqle) {
                throw new DataSourceException(sqle, "Exception in findSequenceInfos");
            } finally {
                stat.close();
            }

        } catch (Exception e) {
            throw new DataSourceException(e, "Exception in findSequenceInfos");
        } finally {
            dbconn.close();
        }

        return result;
    }

    public <T> void update(T item) {

        if (((DbObject) item).isNew())
            executeQuery(item, false);
        else
            executeQuery(item, true);
    }

    private <T> void executeQuery(T item, Boolean isModified) {
        String query = null;
        Statement stat = null;
        DbConnection dbconn = DataRepository.getDbConnection(this.dataSource);

        try {

            if (item instanceof SequenceValue) {
                SequenceValue sequenceValue = (SequenceValue) item;
                setUserSpecificData(sequenceValue, isModified);
                query = getSequenceValueQuery(sequenceValue, isModified);
            } else if (item instanceof SequenceInfo) {
                SequenceInfo sequenceInfo = (SequenceInfo) item;
                setUserSpecificData(sequenceInfo, isModified);
                query = getSequenceInfoQuery(sequenceInfo, isModified);
            }

            try {
                stat = dbconn.getConnection().createStatement();

                stat.execute(query);

            } catch (SQLException e) {
                logger.LogException("SequesceDataBroker:Exception Occured in executing query", e);
                throw new DataSourceException(e, "Exception Occured in executing query");
            } finally {
                stat.close();
            }
        } catch (Exception e) {
            throw new DataSourceException(e, "Exception in closing Statement");
        } finally {
            dbconn.close();
        }
    }

    public <T> void setUserSpecificData(DbObject item, boolean isModified) {

        User admin = ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);
        String loginId = User.UNKNOWN_USER;

        // If the user is not found in the ApplicationContext then use the Unknown user.
        if (admin != null)
            loginId = admin.getLoginId();

        if (!isModified)
            item.setCreation(loginId);
        else
            item.setUpdation(loginId);
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public <T> void persistAll(List<T> items) {
        // TODO Auto-generated method stub

    }


    @Override
    public <T> List<T> findItemsByRange(Search query, int pageNo, int pageSize,
                                        String orderBy, boolean isAsc, int[] returnTotalRowCount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getCriteriaCount(Search criteria) {
        throw new DataException("Not Implemented");
    }

    @Override
    public int deleteBySearch(Search search) {
        // TODO Auto-generated method stub
        return 0;
    }

}
