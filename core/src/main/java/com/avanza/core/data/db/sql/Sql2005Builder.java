package com.avanza.core.data.db.sql;

import com.avanza.core.data.expression.Search;
import com.avanza.core.sdo.DataException;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class Sql2005Builder extends SqlBuilder {

    @Override
    public String prepareInsert(Object obj) {
        throw new DataException("Not Implemented.");
    }

    @Override
    public String preparePaginatedSelect(Search srch) {
        throw new DataException("Not Implemented.");
    }

    @Override
    public String prepareSelect(Search srch) {
        throw new DataException("Not Implemented.");
    }

    @Override
    public PreparedStatement prepareSelectCmd() {
        throw new DataException("Not Implemented.");
    }

    @Override
    public String preparePaginatedSelect(String search, int batchNum, int batchSize) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String preparePageCountQuery(Search search) {
        // TODO Auto-generated method stub
        return null;
    }

    //prepared statement
    @Override
    public int executeInsertCmd(PreparedStatement pstmt) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ResultSet executeProceduteCallCmd(CallableStatement callStmt) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResultSet executeSelectCmd(PreparedStatement pstmt) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PreparedStatement prepareInsertCmd(String obj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String createPaginatedSearch(Search srch) {
        // TODO Auto-generated method stub
        return null;
    }
}
