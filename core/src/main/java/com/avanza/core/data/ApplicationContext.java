package com.avanza.core.data;

import com.avanza.core.web.config.BackgroundThreadContext;
import com.avanza.core.web.config.WebContext;

import java.util.Hashtable;
import java.util.Map.Entry;

public class ApplicationContext {

    private static Hashtable<Long, ApplicationContext> contextList = new Hashtable<Long, ApplicationContext>();

    private Hashtable<String, Object> serviceList;
    private TransactionContext txnContext;

    private ApplicationContext() {

        this.txnContext = new TransactionContext();
        this.serviceList = new Hashtable<String, Object>();
    }

    public static ApplicationContext getContext() {

        return ApplicationContext.getContext(Thread.currentThread().getId());
    }

    public static synchronized ApplicationContext getContext(Long threadId) {

        ApplicationContext retVal = contextList.get(threadId);

        if (retVal == null) {
            retVal = new ApplicationContext();
            contextList.put(threadId, retVal);
        }

        if (retVal.get(WebContext.class.getName()) == null) {
            retVal.add(WebContext.class.getName(), new BackgroundThreadContext());
        }

        return retVal;
    }

    public static boolean hasContext(Long threadId) {

        return contextList.containsKey(threadId);
    }

    public static boolean hasContext() {

        return ApplicationContext.hasContext(Thread.currentThread().getId());
    }

    public void release() {

        this.txnContext.release();
        this.serviceList.clear();
    }

    public TransactionContext getTxnContext() {

        return this.txnContext;
    }

    public <T> void add(T item) {

        String key = item.getClass().getName();
        this.add(key, item);
    }

    public <T> void add(String key, T item) {

        this.serviceList.put(key, item);
    }

    public <T> T get(Class<T> clazz) {

        return (T) this.get(clazz.getName());
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {

        return (T) this.serviceList.get(key);
    }

    public boolean contains(String key) {

        return this.serviceList.containsKey(key);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("The Application Context Trace:\r\n");
        for (Entry<String, Object> entry : this.serviceList.entrySet()) {
            sb.append(entry.getKey()).append(" = ").append(entry.getValue()).append("\r\n");
        }
        return sb.toString();
    }
}
