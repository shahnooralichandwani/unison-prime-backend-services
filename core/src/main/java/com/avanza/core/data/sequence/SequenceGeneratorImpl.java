package com.avanza.core.data.sequence;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TimeLogger;
import com.avanza.core.util.configuration.ConfigSection;

import java.util.ArrayList;
import java.util.Date;
import java.util.IllegalFormatException;


public class SequenceGeneratorImpl implements SequenceGenerator {

    private static Logger logger = Logger.getLogger(SequenceGeneratorImpl.class);
    private SequenceInfo sequenceInfo;
    private String defaultFormatExpr;

    public void dispose() {
    }

    public String getNextValue(String... keyParts) {
        return getNextValue(1, keyParts).get(0);
    }

    public ArrayList<String> getNextValue(int bucketSize, String... keyParts) {
        TimeLogger timmer = TimeLogger.init();
        String uniqueId = StringHelper.getCombinedKeyFrom(keyParts);
        timmer.begin("SequenceGeneratorImpl-getNextValue(keyParts)-" + uniqueId);

        String formatExpression = sequenceInfo.getFormatExpr();
        ArrayList<String> retVal = new ArrayList<String>(bucketSize);

        SequenceValue seqValue = sequenceInfo.getSequenceValue(uniqueId);

        long longValue = seqValue.NextValue(bucketSize);

        for (int i = 0; i < bucketSize; i++) {
            if (!StringHelper.isEmpty(formatExpression)) {
                //Do formatting here. %[argument_index$][flags][width][.precision]conversion
                try {
                    retVal.add(sequenceInfo.getPrefixValue() + String.format(formatExpression, uniqueId, new Date(), longValue));
                } catch (IllegalFormatException e) {
                    logger.LogException("Invalid Format String '" + formatExpression + "' using default format", e);
                    retVal.add(sequenceInfo.getPrefixValue() + String.format(this.defaultFormatExpr, uniqueId, new Date(), longValue));
                }
            } else {
                retVal.add(sequenceInfo.getPrefixValue() + String.format(this.defaultFormatExpr, uniqueId, new Date(), longValue));
            }
            longValue += sequenceInfo.getIncrementedBy();
        }
        timmer.end();
        return retVal;
    }

    public long getNextValueAsLong() {
        return getNextValueAsLong(1).get(0);
    }

    public ArrayList<Long> getNextValueAsLong(int bucketSize) {

        SequenceValue seqValue = sequenceInfo.getSequenceValue("");

        long nextValue = seqValue.NextValue();
        ArrayList<Long> retValues = new ArrayList<Long>(bucketSize);

        for (int i = 0; i < bucketSize; i++) {
            retValues.add(new Long(nextValue));
            nextValue += sequenceInfo.getIncrementedBy();
        }

        return retValues;
    }

    public ArrayList<Integer> getNextValueasInt(int bucketSize) {

        ArrayList<Integer> retVal = new ArrayList<Integer>(bucketSize);
        ArrayList<Long> retValues = getNextValueAsLong(bucketSize);
        for (Long val : retValues) {
            retVal.add(val.intValue());
        }

        return retVal;
    }

    public int getNextValueasInt() {

        return getNextValueasInt(1).get(0);
    }

    public void load(ConfigSection section, SequenceInfo sequenceInfo) {

        this.sequenceInfo = sequenceInfo;
        defaultFormatExpr = "%s_%TY-%<Tm-%<Td_%0" + (String.valueOf(sequenceInfo.getMaxValue()).length() + 1) + "d";
    }

    public SequenceInfo getSequenceInfo() {

        return this.sequenceInfo;
    }
}
