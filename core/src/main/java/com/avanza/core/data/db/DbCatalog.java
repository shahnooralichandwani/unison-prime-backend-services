package com.avanza.core.data.db;

import com.avanza.core.data.DataSourceException;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.StringMap;
import com.avanza.core.util.configuration.ConfigSection;

import java.util.List;

public class DbCatalog {

    public static final String XML_DB_CATALOG = "db-catalog";
    public static final String XML_DATABASE = "database";
    public static final String XML_DB_SOURCE = "db-source";
    public static final String XML_SELECT_PARAMS = "select-params";

    private boolean isLoaded;

    private StringMap<DbSource> dbSrcList;
    private DbSource defaultDbSrc;

    public DbCatalog() {

        this.dbSrcList = new StringMap<DbSource>();
    }

    /**
     * @param dbCatalog
     */
    public void load(ConfigSection dbCatalog) {

        if ((this.isLoaded) || (dbCatalog == null))
            return;

        List<ConfigSection> childList = dbCatalog.getChildSections(DbCatalog.XML_DB_SOURCE);

        if ((childList == null) || (childList.size() == 0)) {

            this.isLoaded = true;
            return;
        }

        for (ConfigSection dbConfig : childList) {
            DbSource dbSrc = this.fromConfig(dbConfig);
            boolean isDefault = dbConfig.getValue(DbInfo.XML_DEFAULT, false);
            this.dbSrcList.add(dbSrc.getName(), dbSrc);

            if (isDefault) {
                if (this.defaultDbSrc != null)
                    throw new DataSourceException("Can't have more than one database Sources as Deafault: Current Default: {0}",
                            this.defaultDbSrc.getName());
                else
                    this.defaultDbSrc = dbSrc;
            }
        }

        if (this.defaultDbSrc == null) {
            if (this.dbSrcList.size() == 1)
                this.defaultDbSrc = this.dbSrcList.get(0);
            else
                throw new DataSourceException("There should be one database source defined as default. {0} database found in Db Factory Catalog",
                        this.dbSrcList.size());
        }

        //this.dbList.sort();

        this.isLoaded = true;
    }

    public void dispose() {

    }

    public DbConnection getDbConnection(String key) {
        DbSource dbSrc = this.getDbInfo(key);
        if (dbSrc != null)
            return this.getDbInfo(key).getDbConnection();
        else return null;
    }

    public DbSource getDbSource(String key) {
        return this.getDbInfo(key);
    }

    public DbConnection getDbConnection() {

        if (this.defaultDbSrc != null)
            return this.defaultDbSrc.getDbConnection();
        else
            return null;
    }

    public Boolean isPrimarySource(String key) {
        DbSource dbSrc = this.getDbInfo(key);
        if (dbSrc != null)
            return this.getDbInfo(key).isPrimarySource();
        else return true;
    }

    public Boolean isPrimarySource() {

        if (this.defaultDbSrc != null)
            return this.defaultDbSrc.isPrimarySource();
        else
            return true;
    }

    private DbSource fromConfig(ConfigSection dbConfig) {

        String name = dbConfig.getTextValue("name");
        String selectType = dbConfig.getValue("select-type", "None");
        String selectImpl = dbConfig.getValue("select-impl", "");

        DbSource retVal = new DbSource(name, selectType, selectImpl);
        retVal.init(dbConfig);

        return retVal;
    }

    private DbSource getDbInfo(String key) {

        DbSource retVal = null;

        for (int idx = this.dbSrcList.size() - 1; idx > -1; idx--) {

            String temp = this.dbSrcList.get(idx).getName();
            if (StringHelper.startsWithIgnoreCase(temp, key)) {

                retVal = this.dbSrcList.get(idx);
                break;
            }
        }

        if (retVal == null)
            retVal = this.defaultDbSrc;

        return retVal;
    }
}
