package com.avanza.core.data;

import com.avanza.core.util.Guard;
import com.avanza.core.util.configuration.ConfigSection;

class BrokerInfo {

    private static final Class<DataBroker> BROKER_TYPE = DataBroker.class;

    private String key;
    private Class<?> clazz;
    private boolean isSingleton;
    private DataBroker dataBroker;
    private boolean isDynamic;
    private boolean isDefault;
    private ConfigSection brokerConfig;


    public ConfigSection getBrokerConfig() {
        return brokerConfig;
    }


    public boolean isDefault() {
        return isDefault;
    }


    public BrokerInfo(String key, Class<?> clazz, boolean isSingleton) {

        this.init(key, clazz, isSingleton, false);
    }

    public BrokerInfo(String key, Class<?> clazz, boolean isSingleton, boolean isDynamic, boolean isDefault, ConfigSection bConfig) {
        this.isDefault = isDefault;
        this.brokerConfig = bConfig;
        this.init(key, clazz, isSingleton, isDynamic);
    }

    private void init(String key, Class<?> clazz, boolean isSingleton, boolean isDynamic) {

        Guard.checkNull(clazz, "BrokerInfo.BrokerInfo(String, Class, boolean");
        Guard.checkNullOrEmpty(key, "BrokerInfo.BrokerInfo(String, Class, boolean");
        this.checkIsBroker(clazz);

        this.clazz = clazz;
        this.key = key;
        this.isSingleton = isSingleton;
        this.isDynamic = isDynamic;
    }

    public Class<?> getClazz() {

        return this.clazz;
    }

    public String getKey() {

        return key;
    }

    public boolean getIsSingleton() {

        return this.isSingleton;
    }

    public boolean getIsDynamic() {

        return this.isDynamic;
    }

    public DataBroker getDataBroker() {

        DataBroker retVal;

        if ((this.isSingleton) && (this.dataBroker != null)) {

            retVal = this.dataBroker;
        } else {

            try {

                retVal = (DataBroker) this.clazz.newInstance();
                retVal.load(this.brokerConfig);

                if (this.isSingleton) this.dataBroker = retVal;
            } catch (Exception e) {

                throw new DataSourceException(e, "Failed to instantiate instance of class [%1$s]", this.clazz.getName());
            }
        }
        return retVal;
    }

    private void checkIsBroker(Class<?> clazz) {

        boolean implementBroker = false;
        Class<?>[] impClassList = clazz.getInterfaces();

        for (int idx = 0; idx < impClassList.length; idx++) {

            if (impClassList[idx] == BrokerInfo.BROKER_TYPE) {

                implementBroker = true;
                break;
            }
        }

        if (!implementBroker)
            throw new DataSourceException("Class [%1$s] does not implement [%2$s] interface", clazz.getName(), BrokerInfo.BROKER_TYPE.getName());
    }
}
