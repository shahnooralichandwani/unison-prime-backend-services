package com.avanza.core.data;

import com.avanza.core.constants.SessionKeyLookup;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class holds the data needed while executing the process per process basis.
 *
 * @author kraza
 */
public class ExecutionContext implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3923385236071813580L;

    // Key LookUps are defined here.
    public static final String EXECUTION_CONTEXT_KEY = "ExecutionContext";

    public static final String META_ENTITY_ID = "metaEntityId";
    public static final String MODULE_ID = "moduleId";
    public static final String IS_CALLSCRIPT_CONTEXT_CHANGE = "call-script-ctx-chg";
    public static final String CURRENT_USER_KEY = SessionKeyLookup.CURRENT_USER_KEY;
    public static final String CURRENT_CUSTOMER_KEY = SessionKeyLookup.CURRENT_CUSTOMER_KEY;
    public static final String CURRENT_CHANNEL_KEY = SessionKeyLookup.CURRENT_INTERFACE_CHANNEL;

    public Map<String, Object> executionValues;

    public ExecutionContext() {
        executionValues = new HashMap<String, Object>();
    }

    public void putValue(String key, Object value) {

        this.executionValues.put(key, value);
    }

    public Object getValue(String key) {

        return this.executionValues.get(key);
    }

    public void putAll(Map<String, Object> values) {

        this.executionValues.putAll(values);
    }

    public Map<String, Object> getValues() {
        return this.executionValues;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Iterator<String> names = this.executionValues.keySet().iterator();
        while (names.hasNext()) {
            String name = names.next();
            sb.append("{").append(name).append(" = ").append(this.executionValues.get(name)).append("}, ");
        }
        return sb.toString().substring(0, sb.length() - 2);
    }
}
