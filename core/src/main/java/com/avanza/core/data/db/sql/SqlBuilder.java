package com.avanza.core.data.db.sql;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.db.DbConnection;
import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.sdo.DataException;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.Guard;
import com.avanza.core.util.Logger;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public abstract class SqlBuilder {

    protected static final Logger logger = Logger.getLogger(SqlBuilder.class);

    protected String connKey;
    protected DbConnection dbConn;

    protected SqlBuilder() {

    }

    public static SqlBuilder getInstance(ProviderType type) {
        return getInstance(type, "");
    }

    public static SqlBuilder getInstance(ProviderType type, String connKey) {

        return createNew(type, connKey);
    }

    private static SqlBuilder createNew(ProviderType type, String connKey) {

        SqlBuilder retVal = null;

        if (ProviderType.Oracle.equals(type)) {
            retVal = new OracleBuilder();
        } else if (ProviderType.Sql2000.equals(type)) {
            retVal = new Sql2000Builder();
        } else if (ProviderType.Sql2005.equals(type)) {
            retVal = new Sql2005Builder();
        } else if (ProviderType.Sybase.equals(type)) {
            retVal = new SybaseBuilder();
        } else if (ProviderType.Informix.equals(type)) {
            retVal = new InformixBuilder();
        } else
            throw new DataException("Unknown provider type found: %1$s.", type);

        retVal.connKey = connKey;

        return retVal;
    }

    public abstract String prepareSelect(Search srch);

    public abstract String preparePaginatedSelect(Search srch);

    public abstract String createPaginatedSearch(Search srch);

    public abstract String preparePaginatedSelect(String search, int batchNum, int batchSize);

    public abstract String prepareInsert(Object obj);

    public abstract String preparePageCountQuery(Search search);

    //prepared statements.
    public abstract PreparedStatement prepareSelectCmd();

    public abstract ResultSet executeSelectCmd(PreparedStatement pstmt);

    public abstract PreparedStatement prepareInsertCmd(String obj);

    public abstract int executeInsertCmd(PreparedStatement pstmt);

    public abstract ResultSet executeProceduteCallCmd(CallableStatement callStmt);

    public ResultSet executeProcedureCallCmd(String procedureCmd, List<SqlParameter> sqlParameters) {
        this.dbConn = getConnection();
        return dbConn.getProcedureResults(procedureCmd, sqlParameters);
    }

    public CallableStatement prepareProcedureCallCmd(String procedureCmd, List<SqlParameter> sqlParameters) {
        this.dbConn = getConnection();
        return dbConn.getProcedureStatement(procedureCmd, sqlParameters);
    }

    public ResultSet executeSelectCmd(String cmd, List<SqlParameter> params) {

        Guard.checkNullOrEmpty(cmd, "cmd");

        this.dbConn = getConnection();

        List<Object> objParams = new ArrayList<Object>(params.size());
        for (SqlParameter sqlParam : params) {
            if (!sqlParam.getParamType().isOutput()) {
                objParams.add(ConvertUtil.ConvertTo(sqlParam.getValue(), sqlParam.getDataType().getJavaType()));
            } else
                throw new DataException("Invalid params for query string: %s", cmd);
        }
        return dbConn.getResults(cmd, objParams);
    }

    public int executeProcedureInsertCmd(String procedureCmd, List<SqlParameter> sqlParameters) {
        this.dbConn = getConnection();
        dbConn.setAutoCommit(false);

        int retVal = 0;
        try {
            retVal = (Integer) dbConn.executeProcedure(procedureCmd, sqlParameters);
        } catch (Throwable t) {
            logger.LogException("Exception while insert Stored Proc cmd.", t);
            dbConn.rollback();
        } finally {
            dbConn.commit();
            dbConn.close();
        }
        return retVal;
    }

    public void close() {
        if (this.dbConn != null)
            this.dbConn.close();
        this.dbConn = null;
    }

    public DbConnection getConnection() {

        if (this.dbConn == null) {
            this.dbConn = DataRepository.getDbConnection(this.connKey);
        }
        return this.dbConn;
    }
}
