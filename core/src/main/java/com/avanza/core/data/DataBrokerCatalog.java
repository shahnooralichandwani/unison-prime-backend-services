/*
 ===============================================================================
 Copyright Avanza Solutions (Pvt) Ltd. All rights reserved.
 THIS CODE AND INFORMATION IS PROPERTY OF THE AVANZA SOLUTIONS AND
 CANNOT BE USED WITHOUT THE APPROVAL OF THE MANAGEMENT
 ===============================================================================
 */

package com.avanza.core.data;

import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

class DataBrokerCatalog {

    static final String XML_DATA_REPOSITORY = "Data-Repository";
    static final String XML_BROKER_CATALOG = "Broker-Catalog";
    private static final String XML_BROKER = "Broker";
    private static final String XML_KEY = "key";
    private static final String XML_CLASS = "class";
    private static final String XML_SINGLETON = "singleton";
    private static final String XML_DEFAULT = "default";
    private static final String XML_DYNAMIC = "isDynamic";
    private static final String CHILD_CONFIG = "Config";

    private Hashtable<String, BrokerInfo> catalog = new Hashtable<String, BrokerInfo>();
    private BrokerInfo defaultBroker = null;
    private boolean isLoaded;

    public void load(ConfigSection brokerSection) {

        if (this.isLoaded) return;

        List<ConfigSection> childList = brokerSection.getChildSections(DataBrokerCatalog.XML_BROKER);

        if ((childList != null) && (childList.size() > 0)) {
            for (int idx = 0; idx < childList.size(); idx++) {

                ConfigSection broker = childList.get(idx);
                String name = broker.getTextValue(DataBrokerCatalog.XML_KEY);
                String impClass = broker.getTextValue(DataBrokerCatalog.XML_CLASS);
                boolean isSingleton = broker.getValue(DataBrokerCatalog.XML_SINGLETON, true);
                boolean isDefault = broker.getValue(DataBrokerCatalog.XML_DEFAULT, false);
                boolean isDynamic = broker.getValue(DataBrokerCatalog.XML_DYNAMIC, false);
                ConfigSection brokerConfig = broker.getChild(DataBrokerCatalog.CHILD_CONFIG);
                BrokerInfo info = new BrokerInfo(name, this.getClass(impClass), isSingleton, isDynamic, isDefault, brokerConfig);
                if (isDefault) defaultBroker = info;
                name = name.toLowerCase();
                this.catalog.put(name, info);
            }
        }

        this.isLoaded = true;
    }

    public void dispose() {

        this.catalog.clear();
        this.isLoaded = false;
    }

    private Class<?> getClass(String name) {

        try {
            System.out.println("**************************" + name);
            return Class.forName(name);
        } catch (Exception e) {

            throw new DataSourceException(e, "Failed to instantiate class [%1$s].", name);
        }
    }

    public ConfigSection getBrokerConfig(String name) {

        return getBrokerInfo(name).getBrokerConfig();
    }

    public DataBroker getBroker(String name) {

        return getBrokerInfo(name).getDataBroker();
    }

    private BrokerInfo getBrokerInfo(String name) {
        Guard.checkNullOrEmpty(name, "DataBrokerCatalog.getBroker(name)");
        name = name.toLowerCase();
        BrokerInfo info = this.catalog.get(name);

        if (info == null) {
            // Then check the broker up in the hierarchy following the defined name.
            String[] keys = name.split("\\.");
            int index = keys.length - 2;

            while (info == null && index >= 0) {
                info = this.catalog.get(StringHelper.getCombinedKeyFrom(keys, index));
                index--;
            }
        }

        if (info == null) // getting the defult broker
            info = defaultBroker;

        if (info == null) throw new DataSourceException("No Data Broker found for the name [%1$s]", name);

        return info;
    }

    public void addBroker(String name, Class<? extends DataBroker> broker, boolean isSingleton) {

        BrokerInfo info = new BrokerInfo(name, broker, isSingleton);
        name = name.toLowerCase();
        this.catalog.put(name, info);
    }

    public void addBroker(String name, Class<? extends DataBroker> broker) {

        this.addBroker(name, broker, false);
    }

    public void removeBroker(String name) {

        name = name.toLowerCase();
        this.catalog.remove(name);
    }

    public List<DynamicDataBroker> getDynamicBrokers() {

        List<DynamicDataBroker> brokers = new Vector<DynamicDataBroker>(0);
        for (BrokerInfo info : catalog.values()) {

            if (info.getIsDynamic()) brokers.add((DynamicDataBroker) info.getDataBroker());
        }
        return brokers;
    }
}
