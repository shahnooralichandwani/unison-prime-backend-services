package com.avanza.core.data.db;

public enum SelectorType {

    File("File"), None("None"), Custom("Custom"), Table("Table");

    private String value;

    SelectorType(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }
}
