package com.avanza.core.data.expression;

import java.io.Serializable;

public abstract class Expression implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1763364784309840322L;
    protected String name;

    protected Expression() {

    }

    public Expression(String name) {

        this.setName(name);
    }


    public String getName() {

        return this.name;
    }

    public abstract StringBuilder toString(StringBuilder builder);

    public void setName(String name) {
        this.name = name;
    }

}
