package com.avanza.core.data.expression;

import java.util.List;

public interface QueryRepresentator {
    public enum QueryMode {
        GET_DATA, GET_COUNT, DELETE_DATA, UPDATE_DATA
    }

    <T> boolean representSimpleCriterion(SimpleCriterion<T> simpleCriterion);

    boolean representColumn(Column col);

    void clear();

    Object getRepresentation();

    boolean representFromclause(From from);

    boolean representJoinclause(Join join);

    boolean representOrderclause(Order order, boolean reverse, boolean useAlias);

    boolean representComplexCriterion(ComplexCriterion criterion, String opener, String closer);

    <T> boolean representBetweenCriterion(BetweenCriterion<T> criterion);

    <T extends QueryRepresentable> boolean representQueryRepresentableList(List<T> list, String delima);

    boolean representSearch(Search search, String opener, String closer);
}
