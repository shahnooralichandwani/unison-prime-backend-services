package com.avanza.core.data.sequence;

import com.avanza.core.function.ContextFunction;
import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;

import java.util.List;


public class DocumentSequenceGenerator implements SequenceGenerator {

    SequenceInfo sequenceInfo;

    public void dispose() {
        // TODO Auto-generated method stub

    }

    public String getNextValue(String... keyParts) {

        ContextFunction.assignTemp("Counter_Prefix", sequenceInfo.getPrefixValue());
        ContextFunction.assignTemp("Counter_Value", String.valueOf(getNextValueAsLong()));

        String formatExpression = sequenceInfo.getFormatExpr();
        String retVal = null;

        if (!StringHelper.isEmpty(formatExpression))
            retVal = (String) ExpressionHelper.getExpressionResult(formatExpression, String.class);

        return retVal;
    }

    public long getNextValueAsLong() {
        SequenceValue sequenceValue = sequenceInfo.getSequenceValue("");
        return sequenceValue.NextValue();
    }

    public int getNextValueasInt() {

        return Convert.toInteger(getNextValueAsLong());
    }

    public void load(ConfigSection section, SequenceInfo sequenceInfo) {

        this.sequenceInfo = sequenceInfo;
    }

    public SequenceInfo getSequenceInfo() {

        return this.sequenceInfo;
    }

    public List<String> getNextValue(int bucketSize, String... keyParts) {
        return null;
    }

    public List<Long> getNextValueAsLong(int bucketSize) {
        return null;
    }

    public List<Integer> getNextValueasInt(int bucketSize) {
        return null;
    }
}
