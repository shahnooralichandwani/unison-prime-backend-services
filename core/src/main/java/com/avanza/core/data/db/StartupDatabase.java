package com.avanza.core.data.db;

import com.avanza.core.util.Cryptographer;
import com.avanza.core.util.Logger;
import com.avanza.core.util.Wrapper;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.configuration.ResultSetConfigSection;
import com.sun.rowset.CachedRowSetImpl;

import javax.sql.rowset.CachedRowSet;
import java.sql.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class StartupDatabase {

    public static final String XML_STARTUP_DATABASE = "startup-database";
    public static final String XML_DB_NAME = "db-name";
    public static final String XML_CONN_URI = "conn-uri";
    public static final String XML_DRIVER = "driver";
    public static final String XML_USER = "user";
    public static final String XML_PASSWORD = "password";
    private static final Logger logger = Logger.getLogger(StartupDatabase.class.getName());

    private boolean isLoaded;

    private String name;
    private String uri;
    private String driver;
    private String user;
    private String password;
    private Cryptographer cryptographer = new Cryptographer();
    private static Map<String, StartupDatabase> startupDbMap = new HashMap<String, StartupDatabase>(
            0);

    public StartupDatabase() {
    }

    public static Collection<StartupDatabase> getStartupDatabases() {
        return startupDbMap.values();
    }

    public static StartupDatabase getStartupDatabase() {
        return startupDbMap.get("UNISON");
    }

    public void load(ConfigSection startupDatabase) {
        //System.out.println("StartupDatabase :: load ===> isLoaded : " + isLoaded);
        if ((this.isLoaded) || (startupDatabase == null))
            return;

        name = startupDatabase.getTextValue(XML_DB_NAME);
        uri = startupDatabase.getTextValue(XML_CONN_URI);
        driver = startupDatabase.getTextValue(XML_DRIVER);
        user = startupDatabase.getTextValue(XML_USER);
        password = cryptographer.decrypt(startupDatabase.getValue(XML_PASSWORD, ""));
        //System.out.println("StartupDatabase :: load ===> putting DB Map");
        startupDbMap.put(name, this);

        this.isLoaded = true;
    }

    private ResultSet executeQuery(String query) {
        //System.out.println("StartupDatabase :: executeQuery ===> START");
        Wrapper<Connection> wrappedConnection = new Wrapper<Connection>();
        try {
            //System.out.println("StartupDatabase :: executeQuery ===> Start executing query");
            ResultSet rs = executeQuery(query, wrappedConnection);
            //System.out.println("StartupDatabase :: executeQuery ===> query returened with no exception");
            //CachedRowSet rowSet = new CachedRowSetImpl();
            CachedRowSet rowSet = new CachedRowSetImpl();
            //System.out.println("StartupDatabase :: executeQuery ===> populating recordset");
            rowSet.populate(rs);
            //System.out.println("StartupDatabase :: executeQuery ===> END with rowSet");
            return rowSet;
        } catch (SQLException e) {
            System.out.println("StartupDatabase :: executeQuery ===> END with SQLException : " + e.getMessage());
            e.printStackTrace();
            logger.LogException("", e);
        } catch (Exception e) {
            System.out.println("StartupDatabase :: executeQuery ===> END with Exception : " + e.getMessage());
            e.printStackTrace();
            logger.LogException("", e);
        } finally {
            if (wrappedConnection.unwrap() != null) {
                try {
                    wrappedConnection.unwrap().close();
                } catch (SQLException e) {
                    System.out.println("StartupDatabase :: executeQuery ===> END with SQLException in finally : " + e.getMessage());
                    logger.LogException("", e);
                }
            }
        }
        //System.out.println("StartupDatabase :: executeQuery ===> END with NULL");
        return null;

    }

    private ResultSet executeQuery(String query, Wrapper<Connection> wrappedConnection) {

        try {
            //System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> START");
            try {
                //System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> driver : " + driver);
                Class.forName(driver).newInstance();
                //System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> driver instance created");
            } catch (InstantiationException e) {
                e.printStackTrace();
                System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> END with InstantiationException : " + e.getMessage());
                logger.LogException("InstantiationException ", e);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> END with IllegalAccessException : " + e.getMessage());
                logger.LogException("IllegalAccessException ", e);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> END with ClassNotFoundException : " + e.getMessage());
                logger.LogException("ClassNotFoundException ", e);
            }
            //System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> creating connection to DB");
            //System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> creating connection to DB...URI : " + uri);
            //System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> creating connection to DB...user : " + user);
            //System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> creating connection to DB...password : " + password);
            Connection conn = DriverManager.getConnection(uri, user, password);
            //System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> CONNECTION ESTABLISHED with DB");
            wrappedConnection.wrap(conn);
            //System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> creating statement");
            Statement statement = conn.createStatement();
            //System.out.println("StartupDatabase :: executeQuery(Wrapper) ===>returning after statement");
            return statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> END with SQLException : " + e.getMessage());
            logger.LogException("", e);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("StartupDatabase :: executeQuery(Wrapper) ===> END with Exception : " + e.getMessage());
            logger.LogException("", e);
            return null;
        }
    }

    public boolean testConnection() {
        return testConnection(user, password);
    }

    public boolean testConnection(String user, String password) {
        boolean success = false;
        try {
            Class.forName(driver).newInstance();
        } catch (Exception e) {
            throw new DataSourceDownException(e,
                    "Unable to retrieve Connection from dataSource: %s.", name);
        }

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(uri, user, password);
        } catch (SQLException e) {
            throw new DataSourceDownException(e,
                    "Unable to retrieve Connection from dataSource: %s.", name);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                    success = true;
                }
            } catch (Exception e) {
                throw new DataSourceDownException(e,
                        "Unable to close Connection from dataSource: %s.", name);
            }

        }
        return success;
    }

    public void getCredentialsByKey(String key, Wrapper<String> wrappedUserName,
                                    Wrapper<String> wrappedPassword) {
        String query = String.format("SELECT * FROM SYSTEM_DATASOURCE WHERE DS_KEY ='%s'", key);
        //System.out.println("StartupDatabase :: getCredentialsByKey ===> query : " + query);
        ResultSet rs = executeQuery(query);

        try {
            while (rs.next()) {
                wrappedUserName.wrap(rs.getString("DS_USER"));
                wrappedPassword.wrap(cryptographer.decrypt(rs.getString("DS_PASSWORD")));
            }
        } catch (SQLException e) {
            logger.LogException("", e);
        }
    }

    public ConfigSection getConfigSection(String tanentId, String nodeId) {
        String query = String.format("SELECT CONFIG_INST.CONFIG_INSTANCE_ID,\nCONFIG_INST.NODE_ID,\nCONFIG_INST.TANENT_ID,\nCONFIG_INST.CONFIG_PARAMS,\nCONFIG_INST.INSTANCE_NAME,\n"
                + "CONFIG.CONFIG_ID,\nCONFIG.NAME,\nCONFIG.CONFIG_XML,\nCONFIG.CONFIG_GROUP_ID,CONFIG.DEFAULT_CONFIG_PARAMS\n"
                + "FROM\n" + "CONFIGURATION_INSTANCE CONFIG_INST,\n"
                + "CONFIGURATION CONFIG\n" + "WHERE\n"
                + "CONFIG_INST.CONFIG_ID = CONFIG.CONFIG_ID AND\n"
                + "CONFIG_INST.NODE_ID = '%s' AND\n" + "CONFIG_INST.TANENT_ID = '%s'"
                + "ORDER BY CONFIG.CONFIG_GROUP_ID", nodeId, tanentId);
        //System.out.println("StartupDatabase :: getConfigSection ===> query : " + query);
        ResultSet rs = executeQuery(query);
        //System.out.println("StartupDatabase :: getConfigSection ===> query executed");
        //if(rs==null){
        //	System.out.println("StartupDatabase :: getConfigSection ===> CONFIGURATION_INSTANCE is NULL for query : " + query);
        //}
        //System.out.println("StartupDatabase :: getConfigSection ===> calling ResultSetConfigSection");
        return new ResultSetConfigSection(rs, tanentId, nodeId);

    }

    /*public LicenseLoader getLicenseLoader() throws Exception {
        return new LicenseLoader(uri, user, password);
    }*/

}
