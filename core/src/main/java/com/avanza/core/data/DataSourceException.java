package com.avanza.core.data;

import com.avanza.core.CoreException;

public class DataSourceException extends CoreException {

    private static final long serialVersionUID = 3;

    public DataSourceException(String message) {

        super(message);
    }

    public DataSourceException(String format, Object... args) {

        super(format, args);
    }

    public DataSourceException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public DataSourceException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public DataSourceException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public DataSourceException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}