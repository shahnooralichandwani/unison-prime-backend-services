package com.avanza.core.data;

public interface Session {

    //why had Generic been used over here if we don't have any parameter? (look useless)
    <T> T value();

    void release();

    void beginTransaction();

    void commit();

    void rollback();

    boolean isTransactionActive();

    //Notify that particular task has been done. Now session will decide whether to release internal resources(hibernate) or not.
    void notifyOnTaskFinished();
}
