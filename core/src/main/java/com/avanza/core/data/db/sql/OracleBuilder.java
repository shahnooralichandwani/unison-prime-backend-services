package com.avanza.core.data.db.sql;

import com.avanza.core.data.expression.OracleQueryRepresentator;
import com.avanza.core.data.expression.QueryRepresentator;
import com.avanza.core.data.expression.Search;
import com.avanza.core.sdo.DataException;
import com.avanza.core.util.StringHelper;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * This class generates the native sql based on the Oracle specific style.
 *
 * @author kraza
 */
public class OracleBuilder extends SqlBuilder {

    @Override
    public String prepareInsert(Object obj) {
        throw new DataException("Not Implemented.");
    }

    @Override
    public String preparePaginatedSelect(String sqlSearchString, int batchNum, int batchSize) {

        logger.logDebug("preparing the paginated search sql query by searchString.");

        Search sqlSearch = Search.parseSearch(sqlSearchString, false);
        sqlSearch.setBatchNum(batchNum);
        sqlSearch.setBatchNum(batchSize);

        return this.createPaginatedSearch(sqlSearch);
    }

    @Override
    public String preparePaginatedSelect(Search search) {

        logger.logDebug("preparing the paginated search sql query.");

        Search sqlSearch = MetaQueryTranslator.convertToSQLSearch(search);

        return createPaginatedSearch(sqlSearch);
    }

    public String createPaginatedSearch(Search sqlSearch) {

        StringBuilder queryBuilder = new StringBuilder();
        QueryRepresentator representator = new OracleQueryRepresentator(queryBuilder);
        queryBuilder.append(SqlKeywords.Select).append(SqlKeywords.Star).append(SqlKeywords.From);

        if (sqlSearch.isOrderBy()) {

            //Oracle related changes
            //queryBuilder.append(SqlKeywords.Select).append(SqlKeywords.TempA).append(SqlKeywords.FieldSeparator).append(SqlKeywords.Star).append(SqlKeywords.Comma).append(SqlKeywords.ROWNUM).append(SqlKeywords.As).append(SqlKeywords.ROW_NUM).append(SqlKeywords.From).append(SqlKeywords.OpenBracket);
            queryBuilder.append(SqlKeywords.OpenBracket).append(SqlKeywords.Select).append(SqlKeywords.TempA).append(SqlKeywords.FieldSeparator).append(SqlKeywords.Star).append(SqlKeywords.Comma).append(SqlKeywords.ROWNUM).append(SqlKeywords.As).append(SqlKeywords.ROW_NUM).append(SqlKeywords.From).append(SqlKeywords.OpenBracket);

            if (!sqlSearch.isSelectClause()) {
                queryBuilder.append(SqlKeywords.Select).append(sqlSearch.getFirstFrom().getAlias()).append(SqlKeywords.FieldSeparator).append(SqlKeywords.Star);
            }
            queryBuilder.append(sqlSearch.toString());

            queryBuilder.append(SqlKeywords.CloseBracket).append(SqlKeywords.TempA);
        } else {

            if (!sqlSearch.isSelectClause()) {
                queryBuilder.append(SqlKeywords.Select).append(sqlSearch.getFirstFrom().getAlias()).append(SqlKeywords.FieldSeparator).append(SqlKeywords.Star);
            } else {
                queryBuilder.append(SqlKeywords.Select);
                representator.representQueryRepresentableList(sqlSearch.getColumnList(), StringHelper.COMMA);
            }

            queryBuilder.append(SqlKeywords.Comma).append(SqlKeywords.ROWNUM).append(SqlKeywords.As).append(SqlKeywords.ROW_NUM);
            queryBuilder.append(SqlKeywords.From);
            representator.representQueryRepresentableList(sqlSearch.getFromList(), StringHelper.COMMA);

            queryBuilder.append(SqlKeywords.Where);
            if (sqlSearch.hasCriteria()) {
                sqlSearch.getRootCriterion().toString(queryBuilder);
                queryBuilder.append(SqlKeywords.And);
            }
        }

        if (sqlSearch.isOrderBy()) {
            queryBuilder.append(SqlKeywords.Where);
        } else {
            queryBuilder.append(SqlKeywords.OpenBracket);
        }

        queryBuilder.append(SqlKeywords.ROWNUM).append(SqlKeywords.LtEq).append((sqlSearch.getBatchNum() + 1) * sqlSearch.getBatchSize());


        if (sqlSearch.isOrderBy())
            queryBuilder.append(SqlKeywords.CloseBracket);

        //Oracle Related Changes
        //queryBuilder.append(SqlKeywords.CloseBracket).append(SqlKeywords.Where).append(SqlKeywords.ROW_NUM).append(SqlKeywords.Gt).append(sqlSearch.getBatchNum() * sqlSearch.getBatchSize());
        queryBuilder.append(SqlKeywords.Where).append(SqlKeywords.ROW_NUM).append(SqlKeywords.Gt).append(sqlSearch.getBatchNum() * sqlSearch.getBatchSize());

        return queryBuilder.toString();
    }

    @Override
    public PreparedStatement prepareSelectCmd() {
        throw new DataException("Not Implemented.");
    }

    @Override
    public String prepareSelect(Search search) {

        logger.logDebug("preparing the search sql query.");
        Search sqlSearch = MetaQueryTranslator.convertToSQLSearch(search);

        return sqlSearch.toString();
    }

    @Override
    public String preparePageCountQuery(Search search) {

        logger.logDebug("preparing the page count sql query.");
        Search sqlSearch = MetaQueryTranslator.convertToSQLSearch(search);
        sqlSearch.getOrderList().clear();

        StringBuilder sql = new StringBuilder();

        if (sqlSearch.getColumnList().size() == 0) {
            sql.append(SqlKeywords.Select).append(sqlSearch.getFirstFrom().getAlias()).append(SqlKeywords.FieldSeparator).append(
                    SqlKeywords.Star);
        }
        sql.append(sqlSearch.toString());


        StringBuilder pageCount = new StringBuilder();
        pageCount.append(SqlKeywords.Select).append(SqlKeywords.Count1).append("COUNT1").append(SqlKeywords.From).append(SqlKeywords.OpenBracket);
        pageCount.append(sql.toString()).append(SqlKeywords.CloseBracket).append(SqlKeywords.TempA);

        return pageCount.toString();
    }

    //prepared statement
    @Override
    public int executeInsertCmd(PreparedStatement pstmt) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ResultSet executeProceduteCallCmd(CallableStatement callStmt) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResultSet executeSelectCmd(PreparedStatement pstmt) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PreparedStatement prepareInsertCmd(String obj) {
        // TODO Auto-generated method stub
        return null;
    }
}
