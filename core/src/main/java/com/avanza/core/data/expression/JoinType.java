package com.avanza.core.data.expression;

public enum JoinType {
    Natural("inner"), LeftOuter("left outer"), RightOuter("right outer");

    private String value;

    private JoinType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }


}
