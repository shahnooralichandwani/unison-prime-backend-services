package com.avanza.core.data.db;

import com.avanza.core.util.Guard;
import com.avanza.core.util.configuration.ConfigSection;

public abstract class BaseDbSelector implements DbSourceSelector {

    private String searchUrl;
    private String emailAddress;
    private int duration; // in minutes
    private int retry; // in seconds
    private String primary;
    private String Secondary;
    protected DbSource dbSource;

    public void init(ConfigSection conf, DbSource source) {

        if (conf == null)
            return;

        searchUrl = conf.getTextValue("search-url");
        emailAddress = conf.getTextValue("email");
        duration = Integer.parseInt(conf.getValue("duaration", "10"));
        primary = conf.getTextValue("primary");
        Secondary = conf.getTextValue("secondary");
        retry = Integer.parseInt(conf.getValue("retry", "60"));
        this.dbSource = source;
    }

    protected void sendEmail() {

    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getRetry() {
        return retry;
    }

    public void setRetry(int retry) {
        this.retry = retry;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getSecondary() {
        return Secondary;
    }

    public void setSecondary(String secondary) {
        Secondary = secondary;
    }

    public static BaseDbSelector getDbSelector(SelectorType selectType, Class<?> implClass) {

        Guard.checkNull(selectType, "selectType");

        BaseDbSelector retVal = null;
        if (selectType == SelectorType.File) {
            retVal = new FileBasedDbSelector();
        } else if (selectType == SelectorType.Table) {
            retVal = new TableBasedDbSelector();
        }


        return retVal;
    }
}
