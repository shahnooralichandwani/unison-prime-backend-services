package com.avanza.core.data;

import com.avanza.core.CoreException;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.db.DbInfo;
import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.sdo.DataException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.TimeLogger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.reader.XMLDocumentReader;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.Serializable;
import java.util.List;


public class SoftphoneDataBroker implements DataBroker {

    private static final String XML_XMLPATH = "xmlpath";
    private static final String XML_KEY = "db-key";
    private static final Logger logger = Logger.getLogger(SoftphoneDataBroker.class);
    private static final String ERROR_MSG = "Exception occoured in SoftphoneDataBroker while invoking method %1$s";
    private SessionFactory sessionFactory;
    private String brokerKey;
    private String dataSource;
    private ProviderType providerType;

    public String getBrokerKey() {
        return brokerKey;
    }


    public void setBrokerKey(String brokerKey) {
        this.brokerKey = brokerKey;
    }

    public SoftphoneDataBroker() {

    }

    public void dispose() {

    }

    public final <T> void add(final T item) {
        throw new DataSourceException("Method SoftphoneDataBroker.add(String) not implemented");
    }

    public final <T> void saveOrUpdate(final T item) {
        throw new DataSourceException("Method SoftphoneDataBroker.saveOrUpdate(String) not implemented");
    }

    public final <T> void delete(final T item) {
        throw new DataSourceException("Method SoftphoneDataBroker.delete(String) not implemented");
    }

    public final <T> List<T> find(String query) {
        throw new DataSourceException("Method SoftphoneDataBroker.find(String) not implemented");
    }

    @SuppressWarnings("unchecked")
    public final <T> List<T> find(Search query) {
        throw new DataSourceException("Method SoftphoneDataBroker.find(String) not implemented");

    }

    @SuppressWarnings("unchecked")
    public <T, ID extends Serializable> T findById(Class<T> t, ID id) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("SoftphoneDataBroker.findById(%s, %s)", t.getSimpleName(), id));
        try {
            org.hibernate.Session session = SessionHibernateImpl.createSession(this.sessionFactory, this.getBrokerKey()).value();
            session.beginTransaction();
            Object a = (T) (session.get(t, id));
            session.getTransaction().commit();
            return (T) a;
        } catch (Throwable th) {
            logger.LogException(ERROR_MSG, th, "findById(Class<T> t, ID id)");
            throw new CoreException(th);
        } finally {
            ((Session) this.getSession()).disconnect();
            timer.end();
        }
    }

    private void updateHBMCofigWithDataSourceInfo(Document xmlDocument, DbInfo dbInfo) {
        NodeList list = xmlDocument.getElementsByTagName("property");
        for (int i = 0; i < list.getLength(); i++) {
            Element element = (Element) list.item(i);

            if (element.getAttribute("name").equalsIgnoreCase(
                    "connection.username")) {
                //	element.setTextContent(dbInfo.getUser());
            } else if (element.getAttribute("name")
                    .equalsIgnoreCase("connection.password")) {
                //	element.setTextContent(dbInfo.getPassword());
            } else if (element.getAttribute("name")
                    .equalsIgnoreCase("connection.url")) {
                //		element.setTextContent(dbInfo.getConnectionUri());
            } else if (element.getAttribute("name")
                    .equalsIgnoreCase("connection.driver_class")) {
                //	element.setTextContent(dbInfo.getDriver());
            }
        }

    }

    public void load(ConfigSection configSection) {

        TimeLogger timer = TimeLogger.init();
        timer.begin("SoftphoneDataBroker.load(config)");
        try {
            String baseAppPath = ApplicationLoader.BASE_APPLICATION_PATH;
            File staticHibernateCFG = new File(baseAppPath + File.separator + configSection.getTextValue(SoftphoneDataBroker.XML_XMLPATH));
            String filePath = baseAppPath + File.separator + configSection.getTextValue(SoftphoneDataBroker.XML_XMLPATH);

            this.brokerKey = configSection.getTextValue("key");
            this.dataSource = configSection.getTextValue("dataSource");

            DbInfo dbInfo = DataRepository.getDbInfo(dataSource);

            if (filePath != null && dbInfo != null) {

                // Create the SessionFactory from hibernate.cfg.xml
                XMLDocumentReader xmlDocumentReader = new XMLDocumentReader();
                Document xmlDocument = xmlDocumentReader
                        .readXMLDocument(filePath);
                updateHBMCofigWithDataSourceInfo(xmlDocument, dbInfo);
                try {
                    sessionFactory = new Configuration().configure(xmlDocument)
                            .buildSessionFactory();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else
                throw new DataSourceException("File %s not found while loading the SoftphoneDataBroker config.", configSection
                        .getTextValue(SoftphoneDataBroker.XML_XMLPATH));

            this.brokerKey = configSection.getTextValue("key");
            this.dataSource = configSection.getTextValue("dataSource");

        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t, "load(ConfigSection configSection)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    public final <T> void persist(final T item) {

    }

    public void synchronize() {
        throw new DataSourceException("Method SoftphoneDataBroker.synchronize() not implemented");
    }

    public <T> void update(T item) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("SoftphoneDataBroker.update(%s)", item.getClass().getSimpleName()));
        try {
            org.hibernate.Session session = SessionHibernateImpl.createSession(this.sessionFactory, this.getBrokerKey()).value();
            session.beginTransaction();
            if (item instanceof DbObject) {
                DbObject temp = (DbObject) item;
                this.setUserSpecificData(temp, true);
            }

            session.update(item);
            session.getTransaction().commit();
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t, "update(T item)");
            throw new CoreException(t);
        } finally {
            ((Session) this.getSession()).disconnect();
            timer.end();
        }
    }

    @SuppressWarnings("unchecked")
    public final <T> List<T> findAll(Class<T> t) {
        throw new DataSourceException("Method SoftphoneDataBroker.findAll(Class) not implemented");
    }

    public final <T> List<T> findAll(String entityName) {

        throw new DataSourceException("Method SoftphoneDataBroker.findAll(String) is not implemented");
    }

    public <T> void setUserSpecificData(DbObject item, boolean isModified) {
        try {
            User admin = ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);
            String loginId = User.UNKNOWN_USER;
            if (admin != null) loginId = admin.getLoginId();
            if (!isModified)
                item.setCreation(loginId);
            else
                item.setUpdation(loginId);
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t, "setUserSpecificData(DbObject item, boolean isModified)");
            throw new CoreException(t);
        }
    }

    public <T> T getSession() {
        return (T) SessionHibernateImpl.createSession(this.sessionFactory, this.getBrokerKey()).value();
    }


    public <ID, T extends DataObject> T findById(String entityId, ID id) throws DataSourceException {
        return null;
    }


    public String getDataSource() {
        return dataSource;
    }

    public ProviderType getProviderType() {
        return DataRepository.getDbInfo(dataSource).getType();
    }


    public <T> void persistAll(List<T> items) {
        // TODO Auto-generated method stub

    }


    @Override
    public <T> List<T> findItemsByRange(Search search, int pageNo, int pageSize,
                                        String orderBy, boolean isAsc, int[] returnTotalRowCount) {
        throw new DataSourceException("Method SoftphoneDataBroker.findItemsByRange(Search, pageNo, pageSize) not implemented");
    }

    @Override
    public int getCriteriaCount(Search criteria) {
        throw new DataException("Not Implemented");
    }


    @Override
    public int deleteBySearch(Search search) {
        // TODO Auto-generated method stub
        return 0;
    }
}
