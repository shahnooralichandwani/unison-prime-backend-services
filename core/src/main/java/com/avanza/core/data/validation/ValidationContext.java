package com.avanza.core.data.validation;

import java.util.ArrayList;
import java.util.Hashtable;

import com.avanza.core.meta.MetaAttribute;

public class ValidationContext {

    private static Hashtable<Long, ValidationContext> contextList = new Hashtable<Long, ValidationContext>();

    private Hashtable<String, ValidationInfo> validationList;

    private ValidationContext() {

        this.validationList = new Hashtable<String, ValidationInfo>();
    }

    public static ValidationContext getContext() {

        return ValidationContext.getContext(Thread.currentThread().getId());
    }

    public static synchronized ValidationContext getContext(Long threadId) {

        ValidationContext retVal = contextList.get(threadId);

        if (retVal == null) {

            retVal = new ValidationContext();
            contextList.put(threadId, retVal);
        }

        return retVal;
    }

    public <T> void add(T item) {

        String key = item.getClass().getName();
        this.add(key, item);
    }

    public <T> void add(String key, T item) {

        this.validationList.put(key, (ValidationInfo) item);
    }

    public <T> T get(Class<T> clazz) {

        return (T) this.get(clazz.getName());
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {

        return (T) this.validationList.get(key);
    }

    public boolean contains(String key) {

        return this.validationList.containsKey(key);
    }

    public Hashtable<String, ValidationInfo> getValidationList() {
        return validationList;
    }

    public <T> void addValidationInfo(String key, ArrayList<String> errorsList,
                                      String result, T item) {

        ValidationInfo validationInfo = new ValidationInfo(result,
                (MetaAttribute) item);
        for (String error : errorsList) {
            validationInfo.addError(error);
        }
        add(key, validationInfo);
    }

}
