package com.avanza.core.data.db.sql;

import com.avanza.core.data.expression.*;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.MetaRelationType;
import com.avanza.core.meta.MetaRelationship;
import com.avanza.core.sdo.DataException;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;

import java.util.List;


public class MetaQueryTranslator {

    private static Logger logger = Logger.getLogger(MetaQueryTranslator.class);

    public static Search convertToSQLSearch(Search hql) {
        logger.logDebug("preparing the paginated search sql query.");

        Search sqlSearch = new Search();

        //select clause

        if (hql.getColumnList() != null && hql.getColumnList().size() > 0) {
            throw new DataException("Columns List Not Implemented.");
        }

        List<From> fromList = hql.getFromList();
        MetaEntity metaEntity = null;

        if (fromList.size() == 0) {
            throw new DataException("From List cannot be null or empty.");
        } else if (fromList.size() > 1) {
            throw new DataException("Multiple Table in from not supported.");
        }

        From from = fromList.get(0);
        metaEntity = MetaDataRegistry.getEntityBySystemName(from.getName());
        sqlSearch.addFrom(metaEntity.getTableName(), from.getAlias());


        //where clause
        applySQLRestrictions(hql.getRootCriterion(), sqlSearch, metaEntity);

        applyOrderByForSQL(hql, metaEntity, sqlSearch, false);

        sqlSearch.setBatchNum(hql.getBatchNum());
        sqlSearch.setBatchSize(hql.getBatchSize());
        sqlSearch.setRecordSize(hql.getRecordSize());
        sqlSearch.setResultCount(hql.getResultCount());

        return sqlSearch;
    }

    private static void applySQLRestrictions(Criterion criteria, Search sqlSearch, MetaEntity metaEntity) {

        if (criteria == null) return;

        // Handling the Complex Types first.
        if (criteria.isSimple()) {

            // Applying the Simple Criteria first.
            applySimpleCriteria(criteria, sqlSearch, metaEntity, false);

        } else if (criteria.isComplex()) { // Handling the Complex types

            // Applying the Complex Criteria.
            applyComplexCriteria(criteria, sqlSearch, metaEntity, false);

        } else {
            throw new DataException("UnKnown Operator Type (other than Simple or Complex) during HQL SearchConversion.");
        }
    }

    private static void applyComplexCriteria(Criterion criteria, Search sqlSearch, MetaEntity metaEntity, boolean isOr) {
        ComplexCriterion complexCriterion = (ComplexCriterion) criteria;


        // Shahbaz : Added IsListForm
        // Campaign & Outbound

        if (complexCriterion.isListForm() || complexCriterion.isLinkedForm() || complexCriterion.isJoinForm()) {

            if (complexCriterion.getOperator() == OperatorType.And) {

                if (complexCriterion.isJoinForm()) {
                    // lhs=SimpleCriteria of the previous object, and rhs = Object Criteria and operator type is And.
                    applySimpleCriteria(complexCriterion.getLhs(), sqlSearch, metaEntity, false);
                    applySimpleCriteria(complexCriterion.getRhs(), sqlSearch, metaEntity, false);
                } else {
                    if (complexCriterion.getRhs().isComplex()) {
                        // lhs=SimpleCriteria and rhs = Complex Criteria
                        applyComplexCriteria(complexCriterion.getRhs(), sqlSearch, metaEntity, false);
                        applySimpleCriteria(complexCriterion.getLhs(), sqlSearch, metaEntity, false);

                    } else {
                        // lhs=SimpleCriteria and rhs = Simple Criteria
                        applySimpleCriteria(complexCriterion.getLhs(), sqlSearch, metaEntity, false);
                        applySimpleCriteria(complexCriterion.getRhs(), sqlSearch, metaEntity, false);
                    }
                }
            } else if (complexCriterion.getOperator() == OperatorType.Or) {

                if (complexCriterion.getRhs().isComplex()) {
                    // lhs=SimpleCriteria and rhs = Complex Criteria
                    applyComplexCriteria(complexCriterion.getRhs(), sqlSearch, metaEntity, false);
                    applySimpleCriteria(complexCriterion.getLhs(), sqlSearch, metaEntity, true);

                } else {
                    // lhs=SimpleCriteria and rhs = Simple Criteria
                    applySimpleCriteria(complexCriterion.getLhs(), sqlSearch, metaEntity, false);
                    applySimpleCriteria(complexCriterion.getRhs(), sqlSearch, metaEntity, true);
                }
            } else if (criteria.getOperator() == OperatorType.Not) {

                if (complexCriterion.getRhs().isComplex()) {
                    // We support the complex criteria in the not operation, lhs = Complex Criteria.
                    applyComplexCriteria(complexCriterion.getLhs(), sqlSearch, metaEntity, false);
                } else {
                    // We support the Simple criteria in the not operation as well, lhs = Simple Criteria.
                    applySimpleCriteria(complexCriterion.getLhs(), sqlSearch, metaEntity, false);
                }
            } else {

                throw new ExpressionException("UnKnown Operator Type (Complex Criteria) during HQL SearchConversion.");
            }
        } else
            // Not supported exception
            throw new ExpressionException(
                    "Currently we are supportin only the List Type of the Complex Criteria in the form lhs=SimpleCriteria and  rhs = Complex Criteria.");
    }

    private static void applySimpleCriteria(Criterion criteria, Search sqlSearch, MetaEntity metaEntity, boolean isOr) {

        SimpleCriterion<?> simpleCriterion = null;
        OperatorType operatorType = criteria.getOperator();

        // Save casting.
        if (criteria instanceof SimpleCriterion)
            simpleCriterion = (SimpleCriterion) criteria;

        // The between operator type is handled.
        if (operatorType == OperatorType.Between) {

            BetweenCriterion<?> betweenCriterion = (BetweenCriterion<?>) simpleCriterion;
            sqlSearch.addCriterion(SimpleCriterion.between(getColumnName(simpleCriterion, metaEntity), betweenCriterion.getValue(), betweenCriterion.getHighValue()), isOr);
        } else if (operatorType == OperatorType.Eq) {

            sqlSearch.addCriterion(SimpleCriterion.equal(getColumnName(simpleCriterion, metaEntity), simpleCriterion.getValue()), isOr);
        } else if (operatorType == OperatorType.Gt) {

            sqlSearch.addCriterion(SimpleCriterion.greater(getColumnName(simpleCriterion, metaEntity), simpleCriterion.getValue()), isOr);
        } else if (operatorType == OperatorType.GtEq) {

            sqlSearch.addCriterion(SimpleCriterion.greaterOrEqual(getColumnName(simpleCriterion, metaEntity), simpleCriterion.getValue()), isOr);
        } else if (operatorType == OperatorType.Lt) {

            sqlSearch.addCriterion(SimpleCriterion.less(getColumnName(simpleCriterion, metaEntity), simpleCriterion.getValue()), isOr);
        } else if (operatorType == OperatorType.LtEq) {

            sqlSearch.addCriterion(SimpleCriterion.lessOrEqual(getColumnName(simpleCriterion, metaEntity), simpleCriterion.getValue()), isOr);
        } else if (operatorType == OperatorType.NEq) {

            sqlSearch.addCriterion(SimpleCriterion.notEqual(getColumnName(simpleCriterion, metaEntity), simpleCriterion.getValue()), isOr);
        } else if (operatorType == OperatorType.Null) {

            sqlSearch.addCriterion(SimpleCriterion.nullCriterion(getColumnName(simpleCriterion, metaEntity)), isOr);
        } else if (operatorType == OperatorType.NotNull) {

            sqlSearch.addCriterion(SimpleCriterion.notNullCriterion(getColumnName(simpleCriterion, metaEntity)), isOr);
        } else if (operatorType == OperatorType.Like) {

            sqlSearch.addCriterion(SimpleCriterion.like(getColumnName(simpleCriterion, metaEntity), simpleCriterion.getValue()), isOr);

            /*
             * Shahbaz:
             * Bug removed search by arabic data not working
             * NLike Added Operator
             **/
        } else if (operatorType == OperatorType.NLike) {

            sqlSearch.addCriterion(SimpleCriterion.Nlike(getColumnName(simpleCriterion, metaEntity), simpleCriterion.getValue()), isOr);
        } else if (operatorType == OperatorType.In) {

            ListCriterion<?> listCriterion = (ListCriterion<?>) criteria;
            sqlSearch.addCriterion(ListCriterion.in(listCriterion.getCompleteFieldName(), listCriterion.getValueList()), isOr);
        } else if (operatorType == OperatorType.NotIn) {

            ListCriterion<?> listCriterion = (ListCriterion<?>) criteria;
            sqlSearch.addCriterion(SimpleCriterion.notIn(listCriterion.getCompleteFieldName(), listCriterion.getValueList()), isOr);
        } else if (operatorType == OperatorType.None) {

            // This If handles the Object Criteria used or join operations.
            ObjectCriterion objectCriterion = (ObjectCriterion) criteria;

            MetaRelationship metaRelationship = metaEntity.getRelationshipByName(objectCriterion.getFrom().getName());
            MetaEntity subMetaEntity = MetaDataRegistry.getMetaEntity(metaRelationship.getSubEntityId());

            String table1Alias = objectCriterion.getJoiningAlias();
            String table2Alias = objectCriterion.getFrom().getAlias();

            table2Alias = StringHelper.isEmpty(table2Alias) ? subMetaEntity.getTableName() : table2Alias;

            if (!metaRelationship.getRelationType().equalsIgnoreCase(MetaRelationType.Complex.name())) {

                // Handling the Simple join.
                sqlSearch.addFrom(subMetaEntity.getTableName(), table2Alias);

                sqlSearch.addCriterion(SimpleCriterion.fieldEqual(table1Alias + SqlKeywords.FieldSeparator + metaRelationship.getMainEntityKey(), table2Alias + SqlKeywords.FieldSeparator + metaRelationship.getSubEntityKey()), isOr);

                // Now applying the HQL restrictions to the Criteria object.
                applySQLRestrictions(objectCriterion.getRootCriterion(), sqlSearch, subMetaEntity);
            } else {

                MetaEntity relMetaEntity = MetaDataRegistry.getMetaEntity(metaRelationship.getRelatedEntityId());

                // Handling the Simple join.
                sqlSearch.addFrom(relMetaEntity.getTableName(), relMetaEntity.getTableName());
                sqlSearch.addFrom(subMetaEntity.getTableName(), table2Alias);

                sqlSearch.addCriterion(SimpleCriterion.fieldEqual(table1Alias + SqlKeywords.FieldSeparator + metaRelationship.getMainEntityKey(), relMetaEntity.getTableName() + SqlKeywords.FieldSeparator + relMetaEntity.getTableName()), isOr);
                sqlSearch.addCriterion(SimpleCriterion.fieldEqual(relMetaEntity.getTableName() + SqlKeywords.FieldSeparator + relMetaEntity.getTableName(), table2Alias + SqlKeywords.FieldSeparator + metaRelationship.getSubEntityKey()), isOr);

                // Now applying the HQL restrictions to the Criteria object.
                applySQLRestrictions(objectCriterion.getRootCriterion(), sqlSearch, subMetaEntity);
            }
        } else {

            throw new ExpressionException("UnKnown Operator Type (Simple Criteria) during HQL SearchConversion.");
        }
    }

    private void prepareListCriteria(SqlBuilderExpression exprBuilder, ListCriterion<?> listCriterion) {

        exprBuilder.appendWhere(SqlKeywords.OpenBracket);
        for (Object obj : listCriterion.getValueList()) {
            exprBuilder.appendWhere(getTypedValue(obj));
        }
        exprBuilder.appendWhere(SqlKeywords.CloseBracket);
    }

    private Object getTypedValue(Object value) {
        if (value instanceof String || value instanceof Character)
            return "'" + value + "'";
        return value;
    }

    private static String getColumnName(SimpleCriterion criteria, MetaEntity metaEntity) {
        return (StringHelper.isNotEmpty(criteria.getAlias()) ? criteria.getAlias() + SqlKeywords.FieldSeparator : StringHelper.EMPTY) + metaEntity.getAttribute(criteria.getFieldName()).getTableColumn();
    }

    /**
     * This method adds the order by clause to the sql.
     *
     * @param search
     * @param hqlCriteria
     */
    public static void applyOrderByForSQL(Search search, MetaEntity metaEntity, Search sqlSearch, boolean reverseOrder) {
        // Preparing the HQL Query OrderBy Clause.
        List<Order> orderList = search.getOrderList();

        for (Order order : orderList) {

            // ASC and DESC Types are supported
            if (reverseOrder) {
                if (order.getType() == OrderType.ASC)
                    sqlSearch.addOrder(order.getAlias(), metaEntity.getAttribute(order.getName()).getTableColumn(), OrderType.DESC);
                else if (order.getType() == OrderType.DESC)
                    sqlSearch.addOrder(order.getAlias(), metaEntity.getAttribute(order.getName()).getTableColumn(), OrderType.ASC);
                else
                    throw new ExpressionException("Order Type %s not supported.", order.getType());
            } else
                sqlSearch.addOrder(order.getAlias(), metaEntity.getAttribute(order.getName()).getTableColumn(), order.getType());

        }
    }
}
