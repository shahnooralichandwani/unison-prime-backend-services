package com.avanza.core.data.expression;

public class Join implements QueryRepresentable {
    private From joinedEntity;
    private JoinType joinType;

    public Join(JoinType joinType, From joinedEntity) {
        this.joinedEntity = joinedEntity;
        this.joinType = joinType;
    }

    public From getJoinedEntity() {
        return joinedEntity;
    }

    public JoinType getJoinType() {
        return joinType;
    }

    public boolean represent(QueryRepresentator representator, String opener, String closer) {
        return representator.representJoinclause(this);
    }

}
