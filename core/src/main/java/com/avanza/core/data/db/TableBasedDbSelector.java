package com.avanza.core.data.db;

import com.avanza.core.CoreException;
import com.avanza.core.data.db.sql.SqlBuilder;
import com.avanza.core.util.Logger;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;


public class TableBasedDbSelector extends BaseDbSelector {

    private static Logger logger = Logger.getLogger(TableBasedDbSelector.class);
    private Date lastPollTime = new Date();
    private Boolean switchPrimary = false;
//	private long switchDuration;

    public String getSource() {

        Date currentTime = new Date();
        synchronized (switchPrimary) {
            if (currentTime.getTime() >= (lastPollTime.getTime() + super.getRetry() * 1000 /*+ switchDuration*(60*1000)*/)) {
                // check the database for switch
                SqlBuilder sqlBuilder = SqlBuilder.getInstance(ProviderType.Sql2000, "UNISON_DB");
                ResultSet rset = null;
                try {
                    ArrayList<Object> params = new ArrayList<Object>();
                    params.add(this.dbSource.getName());
                    rset = sqlBuilder.getConnection().getResults("SELECT SWITCH_MODE FROM DB_SELECTOR_SWITCH WHERE DB_SOURCE = ?", params);
                    if (rset.next()) {
                        boolean isSwitchPrimary = rset.getBoolean(1);
                        if (isSwitchPrimary != switchPrimary) {
                            // now need to log it.
                            switchPrimary = isSwitchPrimary;
                            sqlBuilder.getConnection().closeStatements();
                            params = new ArrayList<Object>();
                            params.add(super.dbSource.getName());
                            params.add(super.dbSource.getPrimaryDbInfo().getName());
                            params.add(super.dbSource.getSecDbInfo().getName());
                            params.add(new Date());
                            params.add(switchPrimary);
                            params.add("System");
                            params.add("System");
                            sqlBuilder.getConnection().execQuery("INSERT INTO DB_SELECTOR_LOG (DB_SOURCE, PRIMARY_DB, SECONDARY_DB, SWITCH_TIME, SWITCH_MODE, CREATED_BY, UPDATED_BY) VALUES(?, ?, ?, ?, ?, ?, ?)", params);
                        } else {
//							switchDuration = 0;
                        }
                    } else {
                        throw new CoreException("Exception while switching the database, cannot find the DbSource %S defined in the DB_SELECTOR_SWITCH in UNISON DB.", this.dbSource.getName());
                    }
                } catch (Throwable t) {
                    logger.LogException("Exception while switching the database.", t);
                    throw new CoreException("Exception while switching the database.", t);
                } finally {
                    if (rset != null)
                        try {
                            rset.close();
                        } catch (Throwable t) {
                        }
                    sqlBuilder.close();
                }
                lastPollTime = new Date();
//				switchDuration = super.getDuration();
            }
        }

        if (switchPrimary)
            return super.getSecondary();
        else
            return super.getPrimary();
    }

    public Boolean isPrimarySource() {
        return !switchPrimary;
    }
}
