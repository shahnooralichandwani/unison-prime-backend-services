package com.avanza.core.data;

import com.avanza.core.data.db.DbConnection;
import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.meta.PicklistData;
import com.avanza.core.sdo.DataException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JDBCDataBroker implements DataBroker {

    private static final Logger logger = Logger.getLogger(JDBCDataBroker.class);

    private static final String XML_XMLPATH = "xmlpath";
    private String dataSource;

    public JDBCDataBroker() {
    }

    public <T> T getSession() {
        // TODO Auto-generated method stub
        return null;
    }

    public void load(ConfigSection configSection) {
        this.dataSource = configSection.getTextValue("dataSource");
    }

    public <T> void add(T item) {

    }

    public <T> void delete(T item) {
    }

    /* execute an insert, delete or update query */
    public int update(String query) {

        int updatedRows = 0;
        Statement stat = null;
        DbConnection dbConn = null;
        try {
            dbConn = DataRepository.getDbConnection(this.dataSource);
            try {
                stat = dbConn.getConnection().createStatement();
                logger.logInfo(String.format("Executing Query > %1$s (JDBCBroker.retreiveDate) ", query));
                updatedRows = stat.executeUpdate(query);
            } catch (SQLException e) {
                if (!dbConn.getConnection().getAutoCommit())
                    dbConn.rollback();
                throw new DataSourceException(e, e.getMessage());
            } finally {
                stat.close();
            }
        } catch (Exception e) {
            throw new DataSourceException(e, "Exception Occoured in executing query:" + e.getMessage());
        } finally {
            dbConn.close();
        }
        return updatedRows;
    }

    public void dispose() {

    }

    @SuppressWarnings(value = "unchecked")
    public List<Object[]> executeQuery(String query) {
        List<Object[]> result = new ArrayList();
        ResultSet rst = null;
        Statement stat = null;
        DbConnection dbConn = null;
        try {
            dbConn = DataRepository.getDbConnection(this.dataSource);
            try {
                stat = dbConn.getConnection().createStatement();
                logger.logInfo(String.format("Executing Query > %1$s ( JDBCBroker.retreiveData) ", query));
                rst = stat.executeQuery(query);
                try {
                    int cols = rst.getMetaData().getColumnCount();
                    Object obj[] = new Object[cols];
                    for (int i = 1; i <= cols; i++)
                        obj[i - 1] = rst.getMetaData().getColumnName(i);
                    result.add(obj);
                    while (rst.next()) {
                        obj = new Object[cols];
                        for (int i = 1; i <= cols; i++)
                            obj[i - 1] = rst.getObject(i);
                        result.add(obj);
                    }
                } catch (SQLException sqle) {
                    throw new DataSourceException(sqle,
                            "Exception in closing ResultSet and Statement");
                } finally {
                    rst.close();
                }
            } catch (SQLException sqle) {
                throw new DataSourceException(sqle, "Exception in closing ResultSet and Statement");
            } finally {

                stat.close();

            }

        } catch (Exception e) {
            throw new DataSourceException(e, "Exception in closing ResultSet and Statement");
        } finally {
            dbConn.close();

        }

        return result;
    }

    @SuppressWarnings(value = "unchecked")
    public <T> List<T> find(String query) {
        List result = new ArrayList<PicklistData>();
        ResultSet rst = null;
        Statement stat = null;
        DbConnection dbConn = null;
        try {
            dbConn = DataRepository.getDbConnection(this.dataSource);
            try {
                stat = dbConn.getConnection().createStatement();
                logger.logInfo(String.format("Executing Query > %1$s ( JDBCBroker.retreiveData) ", query));
                rst = stat.executeQuery(query);

                try {
                    while (rst.next()) {
                        String value = rst.getString(1);
                        String displayPRM = rst.getString(2);
                        String displaySEC = rst.getString(3);
                        PicklistData data = new PicklistData(displayPRM,
                                displaySEC, value);
                        result.add(data);
                    }
                } catch (SQLException sqle) {
                    throw new DataSourceException(sqle, "Exception in closing ResultSet and Statement");
                } finally {
                    rst.close();
                }

            } catch (SQLException sqle) {
                throw new DataSourceException(sqle, "Exception in closing ResultSet and Statement");
            } finally {
                stat.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new DataSourceException(e, "Exception Occured in executing query");
        } finally {

            dbConn.close();

        }
        return result;
    }

    public <T> List<T> find(Search query) {
        return null;
    }

    public <T> List<T> findAll(Class<T> item) {
        return null;
    }

    public <T, ID extends Serializable> T findById(Class<T> t, ID id) {
        return null;
    }

    public <ID, T extends DataObject> T findById(String entityId, ID id) throws DataSourceException {
        return null;
    }

    public <T> void persist(T item) {

    }

    public void synchronize() {

    }

    public <T> void update(T item) {

    }

    public String getDataSource() {
        return dataSource;
    }

    public ProviderType getProviderType() {
        return DataRepository.getDbInfo(dataSource).getType();
    }

    public <T> void persistAll(List<T> items) {
        // TODO Auto-generated method stub

    }


    @Override
    public <T> List<T> findItemsByRange(Search query, int pageNo, int pageSize,
                                        String orderBy, boolean isAsc, int[] returnTotalRowCount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getCriteriaCount(Search criteria) {
        throw new DataException("Not Implemented");
    }

    @Override
    public int deleteBySearch(Search search) {
        // TODO Auto-generated method stub
        return 0;
    }
}
