package com.avanza.core.data.expression;

import com.avanza.core.util.EscapeUtil;
import com.avanza.core.util.Guard;

public class BetweenCriterion<T> extends SimpleCriterion<T> {

    public static final String AND = "And";
    protected T highValue;

    public BetweenCriterion(OperatorType operator, String fieldName, T value, T highValue) {

        super(operator, fieldName, "", value);
        this.setHighValue(highValue);
    }

    public T getHighValue() {

        return this.highValue;
    }

    public void setHighValue(T highValue) {

        Guard.checkNull(value, "BinaryCriterion.setHighValue(T)");
        if (highValue instanceof String)
            highValue = (T) EscapeUtil.getEscapeValue(highValue);
        this.highValue = highValue;
    }

    public StringBuilder toString(StringBuilder builder) {

        represent(new HqlQueryRepresentator(builder), "", "");
        return builder;
    }

    public String toString() {

        StringBuilder builder = new StringBuilder(200);
        return (this.toString(builder)).toString();
    }

    public boolean represent(QueryRepresentator representator, String opener, String closer) {
        return representator.representBetweenCriterion(this);
    }
}
