package com.avanza.core.data;

import com.avanza.core.CoreException;
import com.avanza.core.FunctionDelegate;
import com.avanza.core.cache.Cacheable;
import com.avanza.core.cache.CustomerSessionCacheUtil;
import com.avanza.core.data.db.DbInfo;
import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.db.sql.SqlBuilder;
import com.avanza.core.data.expression.HqlQueryRepresentator;
import com.avanza.core.data.expression.QueryRepresentator.QueryMode;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SearchConversionUtil;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.hbm.HbmMetaGenerator;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.MetaException;
import com.avanza.core.sdo.DataException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TimeLogger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.reader.XMLDocumentReader;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Defines helper methods for Meta Data Broker
 *
 * @author fkazmi
 */
public class MetaDataBroker implements DataBroker, DynamicDataBroker {
    private static final String className = MetaDataBroker.class.getName();

    private static final Logger logger = Logger
            .getLogger(MetaDataBroker.className);
    private static final Logger hbmLogger = Logger.getLogger("hbmLogger");

    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock readLock = readWriteLock.readLock();
    // private static Lock writeLock = readWriteLock.writeLock();

    private static final String XML_XMLPATH = "xmlpath";
    private static final String SESSION_SCOPE_BASED_ON = "sessionScope";

    private static final String XML_KEY = "db-key";

    private SessionFactory sessionFactory;

    private Configuration cfg;

    private File dynamicHibernateCFG;

    private String brokerKey;
    private SessionScope sessionScope;

    private String dataSource;

    private Document xmlDocument;

    public MetaDataBroker() {
    }

    public <ID, T extends DataObject> T findById(String entityId, ID id)
            throws DataSourceException {

        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.findById(%s, %s)", entityId,
                id));
        Session session = getDataSession();
        try {
            MetaEntity entity = MetaDataRegistry
                    .getEntityBySystemName(entityId);
            String primaryKeyId = entity.getPrimaryKeyAttribute()
                    .getSystemName();

            Search query = new Search();
            query.addFrom(entityId); // meta entity primary key
            query.addCriterion(SimpleCriterion.equal(primaryKeyId, id));


            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();

            Map retVal = (Map) SearchConversionUtil.getHQLSearchObject(query,
                    hbSession).uniqueResult();

            if (retVal == null)
                return null;

            T object = (T) (new DataObject((HashMap) retVal));

            CustomerSessionCacheUtil.updateCache(object);

            return object;

        } catch (MetaException e) {

            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            session.notifyOnTaskFinished();
            timmer.end();
        }

        return null;
    }

    public <T, ID extends Serializable> T findById(Class<T> t, ID id,
                                                   boolean lock) throws DataSourceException {

        return null;
    }

    public <T> List<T> findAll(Class<T> item) {

        return null;
    }

    public <T> List<T> findByInstance(T exampleInstance) {

        return null;
    }

    public <T> List<T> findByProperty(Class<T> t, String propertyName,
                                      Object propertyValue, boolean useLike, String sortProperty,
                                      boolean sortDescending) {

        return null;
    }

    public <T> List<T> findByCriteria(Class<T> t, Criterion... criterion) {

        return null;
    }

    public <T> List<T> findByCriteria(Class<T> t, String sortProperty,
                                      boolean sortDescending, Criterion... criterion) {

        return null;
    }

    public <T, ID extends Serializable> T doesEntityExist(Class<T> t, ID id) {

        return null;
    }

    public <T, ID extends Serializable> void deleteById(Class<T> t, ID id)
            throws DataSourceException {

    }

    public List<DataObject> findAll(String entityName) throws MetaException {

        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.findAll(%s)", entityName));
        List<Map<String, Object>> valMaps = null;
        String TableName = "";
        List retVal = null;
        Session session = getSession();//getDataSession();
        try {
            MetaEntity entity = MetaDataRegistry.getMetaEntity(entityName);
            TableName = entity.getTableName();
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            valMaps = hbSession.createCriteria(entity.getSystemName()).list();
            retVal = MetaDataRegistry.getDataObjects(valMaps);
            CustomerSessionCacheUtil.updateCache((List<Cacheable>) retVal);

            return retVal;
        } catch (Exception e) {
            String msg = "Error occured while creating DataObject for Entity %1$s";
            throw new MetaException(e, String.format(msg,
                    new Object[]{TableName}));
        } finally {
            session.notifyOnTaskFinished();
            timmer.end();
        }
    }

    public <T> T getSession() {
        return (T) SessionHibernateImpl.createSession(this.sessionFactory,
                this.getBrokerKey()).value();
    }

    // TODO: this method will be replaced later by getSession.
    private Session getDataSession() {
        return (Session) SessionHibernateImpl.createSession(
                this.sessionFactory, this.getBrokerKey(), sessionScope);
    }

    public <T> List<T> find(String query) {
        List<T> retVal = null;
        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.find(%s)", query));
        Session session = getDataSession();
        try {

            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();

            Query q = hbSession.createQuery(query);
            List<Map<String, Object>> list = (List<Map<String, Object>>) q.list();
            retVal = (List<T>) MetaDataRegistry.getDataObjects(list);
            if (retVal != null)
                CustomerSessionCacheUtil.updateCache((List<Cacheable>) retVal);
            return retVal;
        } catch (Exception e) {
            e.printStackTrace();
            throw new DataException(e,
                    "Exception occured while fetching list: "
                            + query.toString());
        } finally {
            session.notifyOnTaskFinished();
            timmer.end(query.toString());
        }
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> findItemsByRange(Search search, int pageNo,
                                        int pageSize, String orderBy, boolean isAsc,
                                        int[] returnTotalRowCount) {
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            MetaEntity mainEntity = MetaDataRegistry
                    .getEntityBySystemName(search.getFirstFrom().getName());
            if (search.canRepresent()) {
                String queryString = search.getRepresentation(new HqlQueryRepresentator());
                Query q = hbSession.createQuery(queryString);
                q.setFirstResult(pageNo * pageSize).setMaxResults(
                        pageSize);
                List<Map<String, Object>> list = (List<Map<String, Object>>) q.list();

                List<T> returnList;
                if (search.getColumnList().size() == 0) {
                    returnList = (List<T>) MetaDataRegistry.getDataObjects(list);
                } else {
                    returnList = (List<T>) list;
                }

                return returnList;
            }

            Criteria hqlSearchObject = SearchConversionUtil.getHQLSearchObject(
                    search, hbSession);
            hqlSearchObject.setProjection(Projections.rowCount());
            returnTotalRowCount[0] = (Integer) hqlSearchObject.uniqueResult();

            hqlSearchObject.setProjection(null);

            if (StringHelper.isNotEmpty(orderBy)) {
                if (orderBy.contains(" ")) {
                    String orderByColumn = orderBy.substring(0, orderBy
                            .indexOf(' '));
                    String orderByAsc = orderBy.substring(orderBy.indexOf(' ') + 1,
                            orderBy.length());
                    if (orderByAsc.equalsIgnoreCase("desc")) {
                        isAsc = false;
                    } else {
                        isAsc = true;
                    }
                    if (isAsc) {
                        hqlSearchObject.addOrder(Order.asc(orderByColumn));
                    } else {
                        hqlSearchObject.addOrder(Order.desc(orderByColumn));
                    }
                } else {
                    if (isAsc) {
                        hqlSearchObject.addOrder(Order.asc(orderBy));
                    } else {
                        hqlSearchObject.addOrder(Order.desc(orderBy));
                    }
                }
            }

            hqlSearchObject.setFirstResult(pageNo * pageSize).setMaxResults(
                    pageSize);
            List<Map<String, Object>> list = hqlSearchObject.list();
            return (List<T>) MetaDataRegistry.getDataObjects(mainEntity, list,
                    mainEntity.getDiscriminatorCol());
        } catch (Exception ex) {
            logger.LogException("[Exception has occured]", ex);
        } finally {
            session.notifyOnTaskFinished();
        }
        return (List<T>) new ArrayList<DataObject>();
    }

    public <T> List<T> find(Search query) {
        List<T> retVal = null;
        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.find(%s)", query
                .isObjectBased() ? query.getFirstFrom().toString() : query
                .getFromList().toString()));
        Session session = getDataSession();
        try {

            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            MetaEntity mainEntity = MetaDataRegistry
                    .getEntityBySystemName(query.getFirstFrom().getName());

            if (query.canRepresent()) {
                String queryString = query.getRepresentation(new HqlQueryRepresentator(QueryMode.GET_COUNT));
                Query q = hbSession.createQuery(queryString);
                Integer recordCount = Integer.valueOf(((Long) q.uniqueResult()).intValue());
                int pageSize = (query.getBatchSize() > 0) ? query.getBatchSize() : 100;
                retVal = new LazyList<T>(recordCount, pageSize, query, new FunctionDelegate.Arg3<List<? extends Object>, Search, Integer, Integer>() {
                    public List<? extends Object> call(Search search, Integer pageNo, Integer pageSize) {
                        return findItemsByRange(search, pageNo, pageSize, "", true, null);
                    }
                });
            } else if (query.getBatchSize() <= 0 && query.getBatchNum() <= 0) {
                List<Map<String, Object>> list = SearchConversionUtil
                        .getHQLSearchObject(query, hbSession).list();
                retVal = (List<T>) MetaDataRegistry.getDataObjects(mainEntity,
                        list, mainEntity.getDiscriminatorCol());
            } else {
                SqlBuilder sqlBuilder = SqlBuilder
                        .getInstance(this.getProviderType());
                SQLQuery sqlQuery = hbSession.createSQLQuery(sqlBuilder
                        .preparePaginatedSelect(query));
                sqlQuery.addEntity(query.getFirstFrom().getName());
                List<Map<String, Object>> list = sqlQuery.list();
                retVal = (List<T>) MetaDataRegistry.getDataObjects(mainEntity,
                        list, mainEntity.getDiscriminatorCol());
            }
            if (retVal != null)
                CustomerSessionCacheUtil.updateCache((List<Cacheable>) retVal);
            return retVal;
        } catch (Exception e) {
            e.printStackTrace();
            throw new DataException(e,
                    "Exception occured while fetching list: "
                            + query.toString());
        } finally {
            session.notifyOnTaskFinished();
            timmer.end(query.toString());
        }
    }

    public <T, ID extends Serializable> T findById(Class<T> t, ID id) {
        return null;
    }

    public <T> void persist(T itm) {
        DataObject item = (DataObject) itm;
        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.persist(%s)", item
                .getMetaEntity().getSystemName()));
        Session session = getDataSession();
        boolean tranActive = session.isTransactionActive();
        try {

            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();

            session.beginTransaction();
            hbSession.saveOrUpdate(item.getMetaEntity().getSystemName(), item
                    .getValues());
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timmer.end();
        }
    }

    public <T> void add(T itm) {
        DataObject item = (DataObject) itm;
        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.add(%s)", item
                .getMetaEntity().getSystemName()));
        Session session = getDataSession();
        boolean tranActive = session.isTransactionActive();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            session.beginTransaction();
            Map vals = item.getValues();
            vals.put("$type$", item.getMetaEntity().getSystemName());
            hbSession.save(item.getMetaEntity().getSystemName(), vals);
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timmer.end();
        }
    }

    public <T> void update(T itm) {
        DataObject item = (DataObject) itm;
        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.update(%s)", item
                .getMetaEntity().getSystemName()));
        Session session = getDataSession();
        boolean tranActive = session.isTransactionActive();
        try {

            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            session.beginTransaction();
            hbSession.update(item.getMetaEntity().getSystemName(), item
                    .getValues());
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timmer.end();
        }
    }

    public <T> void delete(T itm) {
        DataObject item = (DataObject) itm;
        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.delete(%s)", item
                .getMetaEntity().getSystemName()));
        Session session = getDataSession();
        boolean tranActive = session.isTransactionActive();
        try {

            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            session.beginTransaction();
            hbSession.delete(item.getMetaEntity()
                    .getSystemName(), item.getValues());
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timmer.end();
        }
    }

    public void synchronize() {
    }

    private void updateHBMCofigWithDataSourceInfo(Document xmlDocument, DbInfo dbInfo) {
        NodeList list = xmlDocument.getElementsByTagName("property");
        for (int i = 0; i < list.getLength(); i++) {
            Element element = (Element) list.item(i);

            if (element.getAttribute("name").equalsIgnoreCase(
                    "connection.username")) {
                //	element.setTextContent(dbInfo.getUser());
            } else if (element.getAttribute("name")
                    .equalsIgnoreCase("connection.password")) {
                //	element.setTextContent(dbInfo.getPassword());
            } else if (element.getAttribute("name")
                    .equalsIgnoreCase("connection.url")) {
                //	element.setTextContent(dbInfo.getConnectionUri());
            } else if (element.getAttribute("name")
                    .equalsIgnoreCase("connection.driver_class")) {
                //	element.setTextContent(dbInfo.getDriver());
            }
        }

    }


    public void load(ConfigSection configSection) {
        TimeLogger timmer = TimeLogger.init();
        timmer.begin("MetaDataBroker-Load()");

        try {
            cfg = new Configuration();

            String baseAppPath = ApplicationLoader.BASE_APPLICATION_PATH;

            dynamicHibernateCFG = new File(baseAppPath + File.separator
                    + configSection.getTextValue(MetaDataBroker.XML_XMLPATH));

            String filePath = baseAppPath + File.separator
                    + configSection.getTextValue(MetaDataBroker.XML_XMLPATH);
            // if (dynamicHibernateCFG != null && dynamicHibernateCFG.exists()
            // && dynamicHibernateCFG.isFile()) {


            this.brokerKey = configSection.getTextValue("key");
            this.dataSource = configSection.getTextValue("dataSource");

            DbInfo dbInfo = DataRepository.getDbInfo(dataSource);

            if (filePath != null && dbInfo != null) {

                // Create the SessionFactory from hibernate.cfg.xml
                XMLDocumentReader xmlDocumentReader = new XMLDocumentReader();
                xmlDocument = xmlDocumentReader
                        .readXMLDocument(filePath);
                updateHBMCofigWithDataSourceInfo(xmlDocument, dbInfo);

            } else
                throw new DataSourceException(
                        "File %s not found while loading the MetaDataBroker config.",
                        configSection.getTextValue(MetaDataBroker.XML_XMLPATH));


            this.sessionScope = SessionScope.fromValue(configSection.getValue(
                    SESSION_SCOPE_BASED_ON, SessionScope.REQUEST_BASED.value));
        } catch (Throwable t) {
            throw new CoreException(t);
        } finally {
            timmer.end();
        }
    }

    public void dispose() {

    }

    public String getBrokerKey() {

        return this.brokerKey;
    }

    public void loadMappings() throws MetaException {

        TimeLogger timmer = TimeLogger.init();
        timmer.begin("MetaDataBroker-LoadMappings()-" + this.brokerKey);
        try {
            Hashtable<String, MetaEntity> entities = MetaDataRegistry
                    .getEntityList(this.brokerKey);

            for (MetaEntity temp : entities.values()) {
                if (temp.getTableName() != null
                        && !temp.isAbstract()
                        && (temp.getParent() == null || temp.getParent()
                        .isAbstract())) {
                    if (temp.getAttributeList(true).size() > 0) {
                        String xmlMapping = HbmMetaGenerator
                                .createMapping(temp);
                        hbmLogger.logInfo(xmlMapping);
                        try {
                            cfg.addXML(xmlMapping);
                        } catch (Exception e) {
                            logger.logInfo("debug entity" + temp.getId());
                            logger
                                    .LogException(
                                            "Exception Occoured while loading Mappings of HBM.xml, Exception: %s .",
                                            e);

                        }
                    }
                }
            }
            sessionFactory = cfg.configure(xmlDocument).buildSessionFactory();
        } catch (Throwable t) {
            throw new CoreException(t);
        } finally {
            timmer.end();
        }
    }

    public void reloadMappings() throws MetaException {
        TimeLogger timmer = TimeLogger.init();
        timmer.begin("MetaDataBroker-ReloadMappings()-" + this.brokerKey);
        try {
            // Shuwiar
            readLock.lock();
            Hashtable<String, MetaEntity> entities = MetaDataRegistry
                    .getEntityList(this.brokerKey);
            if (xmlDocument != null) {
                cfg = new Configuration();
                sessionFactory = cfg.configure(xmlDocument)
                        .buildSessionFactory();
            }
            for (MetaEntity temp : entities.values()) {
                // Shuwair
                // if (!temp.isAbstract() && (temp.getParent() == null ||
                // temp.getParent().isAbstract())) {
                if (temp.getTableName() != null
                        && !temp.isAbstract()
                        && (temp.getParent() == null || temp.getParent()
                        .isAbstract())) {
                    if (temp.getAttributeList(true).size() > 0) {
                        String xmlMapping = HbmMetaGenerator
                                .createMapping(temp);
                        hbmLogger.logInfo(xmlMapping);
                        try {
                            cfg.addXML(xmlMapping);
                        } catch (Exception e) {
                            logger
                                    .LogException(
                                            "Exception Occoured while reloading Mappings of HBM.xml, Exception: %s .",
                                            e);
                        }
                    }
                }
            }
            sessionFactory = cfg.configure(xmlDocument).buildSessionFactory();
        } catch (Throwable t) {
            throw new CoreException(t);
        } finally {
            // Shuwair
            readLock.unlock();
            timmer.end();
        }

    }

    public String getDataSource() {
        return dataSource;
    }

    public ProviderType getProviderType() {
        return DataRepository.getDbInfo(dataSource).getType();
    }

    public <T> void persistAll(List<T> items) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getCriteriaCount(Search criteria) {
        Criteria hqlCriteria = SearchConversionUtil.getHQLSearchObject(criteria, (org.hibernate.Session) this.getSession());
        hqlCriteria.setProjection(Projections.rowCount());
        int count = (Integer) hqlCriteria.uniqueResult();
        hqlCriteria.setProjection(null);
        return count;
    }


    // rollback and commit behavior can be overridden by subclass.
    protected void rollback(boolean isActiveTransaction) {
        if (!isActiveTransaction) {
            getDataSession().rollback();
        }
    }

    protected void commit(boolean isActiveTransaction) {
        if (!isActiveTransaction) {
            getDataSession().commit();
        }
    }

    public int deleteBySearch(Search search) {
        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.deleteBySearch(%s)", search.isObjectBased()
                ? search.getFirstFrom().toString()
                : search.getFromList().toString()));
        Session session = getDataSession();
        boolean tranActive = session.isTransactionActive();
        int recordCount = 0;
        try {

            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            session.beginTransaction();
            MetaEntity mainEntity = MetaDataRegistry.getEntityBySystemName(search.getFirstFrom().getName());
            if (search.canRepresent()) {
                String queryString = search.getRepresentation(new HqlQueryRepresentator(
                        QueryMode.DELETE_DATA));
                Query q = hbSession.createQuery(queryString);
                recordCount = q.executeUpdate();
            } else {
                throw new RuntimeException("canRepresent must be true in Search object");
            }
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timmer.end();
        }

        return recordCount;

    }
}
