package com.avanza.core.data.sequence;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.DbObject;
import com.avanza.core.data.db.DbConnection;
import com.avanza.core.util.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * author <B>Najeeb Danish</B>
 */
// Maps to the counter_value table do not use hibernate for it.
public class SequenceValue extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = -6655328576198001921L;
    private static final Logger logger = Logger.getLogger(SequenceValue.class);
    private static final String COUNTER_DATASOURCE = "COUNTER";

    private String counterName;
    private String uniqueId;
    private long nextValue;
    private SequenceInfo sequenceInfo; // parent info.

    // Constructor
    public SequenceValue(String uniqueId, SequenceInfo sequenceInfo) {
        this.counterName = sequenceInfo.getCounterName();
        this.uniqueId = uniqueId;
        this.nextValue = sequenceInfo.getStartValue();
        this.sequenceInfo = sequenceInfo;
    }

    public SequenceValue() {

    }

    public SequenceInfo getSequenceInfo() {
        return sequenceInfo;
    }

    // Getters and Setters
    public void setSequenceInfo(SequenceInfo sequenceInfo) {
        this.sequenceInfo = sequenceInfo;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getCounterName() {
        return counterName;
    }

    public void setCounterName(String counterName) {
        this.counterName = counterName;
    }

    public void setNextValue(long nextValue) {

        this.nextValue = nextValue;
    }

    public long getNextValue() {

        return nextValue;
    }

    // Methods
    public long NextValue(int bucketSize) {

        long retNextValue = 0;
        int counterIncrement = sequenceInfo.getIncrementedBy();
        counterIncrement = counterIncrement * bucketSize;
        DbConnection dbConn = DataRepository.getDbConnection(COUNTER_DATASOURCE);
        ResultSet rset = null;
        try {
            dbConn.getConnection().setAutoCommit(false);
            dbConn.execute("UPDATE COUNTER_VALUE SET NEXT_VALUE = NEXT_VALUE + " + counterIncrement + " WHERE COUNTER_NAME='" + this.counterName + "' AND UNIQUE_ID='" + this.uniqueId + "'");
            rset = dbConn.getResults("SELECT NEXT_VALUE - " + counterIncrement + " FROM COUNTER_VALUE WHERE COUNTER_NAME='" + this.counterName + "' AND UNIQUE_ID='" + this.uniqueId + "' AND NEXT_VALUE < " + sequenceInfo.getMaxValue());
            if (rset.next())
                retNextValue = rset.getLong(1);
            else if (sequenceInfo.isCyclic()) {
                rset.close();
                dbConn.closeStatements();
                retNextValue = this.sequenceInfo.getStartValue();
                dbConn.execute("UPDATE COUNTER_VALUE SET NEXT_VALUE = " + (retNextValue + counterIncrement) + " WHERE COUNTER_NAME='" + this.counterName + "' AND UNIQUE_ID='" + this.uniqueId + "'");
            }
            dbConn.commit();
            this.nextValue = retNextValue;
            return retNextValue;
        } catch (Throwable t) {
            logger.LogException("Exception in SequenceValue.NextValue()", t);
            dbConn.rollback();
            return 0;
        } finally {
            if (rset != null) try {
                rset.close();
            } catch (SQLException e) {
                logger.LogException("Exception in SequenceValue.NextValue() while closing resultset.", e);
            }
            dbConn.close();
        }
    }

    public long NextValue() {
        return this.NextValue(1);
    }
}
