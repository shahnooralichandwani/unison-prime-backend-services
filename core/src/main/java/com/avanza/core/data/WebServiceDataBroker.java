package com.avanza.core.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.hibernate.Session;

import com.avanza.core.CoreException;
import com.avanza.core.constants.ThreadContextLookup;
import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.sdo.DataException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.SecurityKeyInfo;
import com.avanza.core.util.Cryptographer;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TimeLogger;
import com.avanza.core.util.configuration.ConfigSection;
/*import com.avanza.core.web.config.WebContext;
import com.avanza.integrationutility.core.DetailMessageXmlFormat;
import com.avanza.integrationutility.core.IntegrationChannel;
import com.avanza.integrationutility.core.IntegrationChannelFactory;
import com.avanza.integrationutility.core.ListMessageXmlFormat;
import com.avanza.integrationutility.core.MessageResponseObject;
import com.avanza.integrationutility.exceptions.ConnectionFailedException;
import com.avanza.workflow.execution.CriticalActionFailedException;*/

/**
 * @author shahbaz.ali
 * This broker carries out tasks which is responsible for requesting the data from a WebService using XML based messages
 */
public class WebServiceDataBroker implements DataBroker {

    private HashMap<String, String> properties = new HashMap<String, String>(0);
    private static final String CHANNEL_KEY = "channelKey";
    private String brokerKey;
    private SecurityKeyInfo keyInfo;


    @Override
    public <T> void add(T item) {
        Search query = null;
        if (item instanceof DataObject) {
            DataObject obj = (DataObject) item;
            query = new Search(obj.getRefMetaEnt().getSystemName(), "abc");
            for (Entry<String, Object> entry : obj.getValues().entrySet()) {
                if (entry.getValue() != null) {
                    query.addCriterion(Criterion.equal(entry.getKey(), entry.getValue()));
                }
            }
        } else {
            query = Search.createSearchByObject(item);
        }

        find(query);


    }

    @Override
    public <T> void delete(T item) {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> List<T> find(String query) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> List<T> find(Search query) {
	/*
	DataBroker gets Meta Entity id from Search parameter.
	Instantiate the ListMessageFormat by passing Meta entity id as argument in its constructor.
	Get the associated IntegrationChannel from IntegrationChannelFactory the channel key specified in properties of the DataBroker.
	Invoke IntegrationChannel Send method, passing the ListMessageFormat object as argument and wait for response.
	Get and return the response received from IntegrationChannel.
	*/
        TimeLogger timer = TimeLogger.init();

        timer.begin(String.format("WebServiceDataBroker.find(%s)", query.isObjectBased() ? query.getFirstFrom().toString() : query.getFromList().toString()));
        List<DataObject> lstDataObject = new ArrayList<DataObject>(0);
	 
	        /*try {
	            String integrationChannelKey=this.properties.get(WebServiceDataBroker.CHANNEL_KEY);
	 
        	 if(StringHelper.isNotEmpty(integrationChannelKey)){
        	     String mainEntityId = query.getFirstFrom().getName();
        	     ListMessageXmlFormat messageFormat= new ListMessageXmlFormat(mainEntityId,keyInfo);
        	     IntegrationChannel channel= IntegrationChannelFactory.getInstance().getChannel(integrationChannelKey);
        	     
        	     //add criteria map to webcontext.
        	     WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        	     if(webContext != null) {
        	    	 webContext.setAttribute("brokers." + brokerKey + ".params",query.getCriteriaMap());
        	     } else {
        	    	 ApplicationContext.getContext().add("brokers." + brokerKey + ".params",query.getCriteriaMap());
        	     }
        			
       	     
        	     MessageResponseObject responseObject= (MessageResponseObject) channel.send(messageFormat);
        	    	 
                	 // Put into ApplicationContext to be used in presentation layer
        	     if(responseObject.getResponseCode()!=null)
                	 ApplicationContext.getContext().add(ThreadContextLookup.ExternalResponseCode.toString(),responseObject.getResponseCode());
                         
                	 lstDataObject= (List<DataObject>) responseObject.getResponseObject();
                	 return (List<T>) lstDataObject;
        	     
        	 }
        	 else
                     throw new DataSourceException("WebServiceDataBroker, Integration Channel key missing in broker configuration.");
	        }catch (ConnectionFailedException e) {
		       ApplicationContext.getContext().add(ThreadContextLookup.ExternalResponseCode.toString(),"99");
		       return (List<T>) lstDataObject;
	        }catch (Exception e) {
  	            throw new DataException(e, "Exception occured while fetching list: " + query.toString());
	        } finally {
	        	if(((Session) this.getSession()) != null)
	        		((Session) this.getSession()).disconnect();
	        	timer.end(query.toString());
	        }*/

        return (List<T>) lstDataObject;
    }

    @Override
    public <T> List<T> findAll(Class<T> item) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T, ID extends Serializable> T findById(Class<T> t, ID id) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <ID, T extends DataObject> T findById(String entityId, ID id)
            throws DataSourceException {

	/*	
		
	DataBroker will instantiate the DetailMessageFormat object by passing Meta entity id and instance id parameter as its constructor arguments.
	Get the associated IntegrationChannel from IntegrationChannelFactory of the broker using the key specified in properties of the DataBroker.
	Invoke IntegrationChannel Send method, passing the DetailMessageFormat object as argument and wait for response.
	Get and return the response received from IntegrationChannel.

	*/
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format(String.format("WebServiceDataBroker.findById(%s, %s)", entityId, id)));
        
       /* try {
    	 Object instanceId= id;
    	 String integrationChannelKey=this.properties.get(WebServiceDataBroker.CHANNEL_KEY);
    	 DetailMessageXmlFormat messageFormat= new DetailMessageXmlFormat(entityId, instanceId.toString());
    	 IntegrationChannel channel= IntegrationChannelFactory.getInstance().getChannel(integrationChannelKey);
    	 DataObject dataObject= (DataObject) channel.send(messageFormat);
    	 
    	 return  (T) dataObject;
        } catch (Exception e) {
    		throw new DataException(e, "Exception occured while fetching list, " + "EntityId="+ entityId+"|instanceId="+id);
        } finally {
        	if(((Session) this.getSession()) != null)
        		((Session) this.getSession()).disconnect();
        	timer.end(String.format("WebServiceDataBroker.findById(%s, %s)", entityId, id));
        }*/
        return null;
    }

    @Override
    public String getDataSource() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ProviderType getProviderType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T getSession() {
        // TODO Auto-generated method stub
        return null;
    }

    public void load(ConfigSection configSection) {
        TimeLogger timmer = TimeLogger.init();
        timmer.begin("WebServiceDataBroker-Load()");

        try {
            String properties = configSection.getTextValue("properties");
            this.properties = (HashMap<String, String>) StringHelper.getPropertiesMapFrom(properties, StringHelper.COMMA);
            this.brokerKey = configSection.getTextValue("key");

            //if data security and integrity required by private public key cryptography.
            keyInfo = null;
            Cryptographer cryptographer = new Cryptographer();
            if (StringHelper.isNotEmpty(configSection.getTextValue("security")) && configSection.getTextValue("security").equalsIgnoreCase("true")) {
                keyInfo = new SecurityKeyInfo(configSection.getTextValue("security.keystorePath")
                        , cryptographer.decrypt(configSection.getTextValue("security.keystorePassword"))
                        , cryptographer.decrypt(configSection.getTextValue("security.ourKeyPassword"))
                        , cryptographer.decrypt(configSection.getTextValue("security.ourKeyAlias"))
                        , cryptographer.decrypt(configSection.getTextValue("security.theirKeyAlias"))
                        , configSection.getValue("security.algorithm", (String) null)
                        , configSection.getValue("security.encoding", (String) null)
                );
            }

        } catch (Throwable t) {
            throw new CoreException(t);
        } finally {
            timmer.end();
        }
    }

    @Override
    public <T> void persist(T item) {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> void persistAll(List<T> items) {
        // TODO Auto-generated method stub

    }

    @Override
    public void synchronize() {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> void update(T item) {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> List<T> findItemsByRange(Search search, int pageNo, int pageSize,
                                        String orderBy, boolean isAsc, int[] returnTotalRowCount) {
        throw new DataSourceException("Method WebServiceDataBroker.findItemsByRange(Search, pageNo, pageSize) not implemented");
    }

    @Override
    public int getCriteriaCount(Search criteria) {
        throw new DataException("Not Implemented");
    }

    @Override
    public int deleteBySearch(Search search) {
        // TODO Auto-generated method stub
        return 0;
    }

    public String brokerKey() {
        return brokerKey;
    }

}
