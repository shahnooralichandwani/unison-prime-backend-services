package com.avanza.core.data;

import com.avanza.core.data.db.DbCatalog;
import com.avanza.core.data.db.DbConnection;
import com.avanza.core.data.db.DbInfo;
import com.avanza.core.data.db.StartupDatabase;
import com.avanza.core.util.configuration.ConfigSection;

import java.util.List;

public class DataRepository {

    public static final String XML_DATA_REPOSITORY = "Data-Repository";
    public static final String XML_CONFIGURATION = "Configuration";

    private static Boolean isLoaded = false;
    private static Boolean isStartupDatabaseLoaded = false;

    private static DataBrokerCatalog dataBrokerCatalog = new DataBrokerCatalog();
    private static DbCatalog dbCatalog = new DbCatalog();
    private static StartupDatabase startupDatabase = new StartupDatabase();

    private DataRepository() {

    }

    public static void loadStartupDatabase(ConfigSection dataSection) {

        if (DataRepository.isStartupDatabaseLoaded) return;

        if (dataSection.getName().compareTo(DataRepository.XML_DATA_REPOSITORY) != 0)
            throw new DataSourceException("Expected [%1$s] section, found [%2$s]", DataRepository.XML_DATA_REPOSITORY, dataSection.getName());

        DataRepository.startupDatabase.load(dataSection.getChild(StartupDatabase.XML_STARTUP_DATABASE));
    }


    public static void load(ConfigSection dataSection) {

        if (DataRepository.isLoaded) return;

        DataRepository.dbCatalog.load(dataSection);
        DataRepository.dataBrokerCatalog.load(dataSection);

        DataRepository.isLoaded = true;
    }

    public static void dispose() {

        DataRepository.dataBrokerCatalog.dispose();
        DataRepository.dbCatalog.dispose();
        DataRepository.isLoaded = false;
    }

    public static void synchronize() {

    }

    public static DataBroker getBroker(String key) {

        return DataRepository.dataBrokerCatalog.getBroker(key);
    }

    public static ConfigSection getBrokerConfig(String key) {

        return DataRepository.dataBrokerCatalog.getBrokerConfig(key);
    }

    public static <T extends DataBroker> void addBroker(String key, Class<T> brokerClass, boolean isSingleton) {

        DataRepository.dataBrokerCatalog.addBroker(key, brokerClass);
    }

    public static void removeBroker(String name) {

        DataRepository.dataBrokerCatalog.removeBroker(name);
    }

    public static boolean getIsLoaded() {
        return isLoaded;
    }

    public static List<DynamicDataBroker> getDynamicBrokers() {

        return DataRepository.dataBrokerCatalog.getDynamicBrokers();
    }

    public static DbConnection getDbConnection(String className) {

        return DataRepository.dbCatalog.getDbConnection(className);
    }

    public static DbInfo getDbInfo(String key) {
        return DataRepository.dbCatalog.getDbSource(key).getDbInfo();
    }

    public static Boolean isPrimarySource(String className) {

        return DataRepository.dbCatalog.isPrimarySource(className);
    }

    public static DbConnection getDbConnection(Class<? extends Object> clazz) {

        return DataRepository.dbCatalog.getDbConnection(clazz.getSimpleName());
    }

    public static Boolean isPrimarySource(Class<? extends Object> clazz) {

        return DataRepository.dbCatalog.isPrimarySource(clazz.getSimpleName());
    }

    public static DbConnection getDbConnection() {

        return DataRepository.dbCatalog.getDbConnection();
    }

    public static Boolean isPrimarySource() {

        return DataRepository.dbCatalog.isPrimarySource();
    }
}
