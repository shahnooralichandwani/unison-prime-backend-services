package com.avanza.core.data.expression;

import com.avanza.core.data.db.sql.SqlKeywords;
import com.avanza.core.util.StringHelper;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class HqlQueryRepresentator implements QueryRepresentator {

    protected QueryMode queryMode;
    protected StringBuilder builder;

    // it is to avoid unnecessary construction of builder object.
    protected boolean isNativeBuilder = true;

    public HqlQueryRepresentator() {
        this(new StringBuilder(), QueryMode.GET_DATA);
    }

    public HqlQueryRepresentator(QueryMode queryMode) {
        this(new StringBuilder(), queryMode);
    }

    public HqlQueryRepresentator(StringBuilder builder) {
        this(builder, QueryMode.GET_DATA);
    }

    public HqlQueryRepresentator(StringBuilder builder, QueryMode queryMode) {
        this.builder = builder;
        isNativeBuilder = false;
        this.queryMode = queryMode;

    }

    public boolean representJoinclause(Join join) {
        builder.append(join.getJoinType().getValue()).append(StringHelper.SPACE).append("join ");
        join.getJoinedEntity().represent(this, "", "");
        return true;
    }

    public boolean representFromclause(From from) {
        builder.append(getCompliance(from.getName()));
        if (from.hasAlias()) {
            builder.append(StringHelper.SPACE).append(getCompliance(from.getAlias()));
        }

        for (Join join : from.getJoins()) {
            builder.append(StringHelper.NEW_LINE);
            join.represent(this, "", "");
        }
        return true;
    }

    public boolean representComplexCriterion(ComplexCriterion criterion, String opener,
                                             String closer) {
        builder.append(opener);

        Criterion first = criterion.getLhs();
        Criterion second = criterion.getRhs();
        if (criterion.getRhs() instanceof SimpleCriterion<?>) {
            first = criterion.getRhs();
            second = criterion.getLhs();
        }

        first.represent(this, StringHelper.OPEN_PAREN, StringHelper.CLOSED_PAREN);
        builder.append(StringHelper.SPACE).append(criterion.getOperator().toToken()).append(
                StringHelper.SPACE);
        second.represent(this, StringHelper.OPEN_PAREN, StringHelper.CLOSED_PAREN);
        builder.append(closer);
        return true;
    }

    private <T> void representDate(Date date) {
        representString(new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aaa").format(date));
    }

    private <T> void representString(String str) {
        builder.append(SqlKeywords.Quote).append(str).append(SqlKeywords.Quote);
    }

    public <T> boolean representBetweenCriterion(BetweenCriterion<T> criterion) {
        criterion.getField().represent(this, StringHelper.OPEN_PAREN, StringHelper.CLOSED_PAREN);
        builder.append(StringHelper.SPACE).append(criterion.getOperator().toToken()).append(
                StringHelper.SPACE);
        if (criterion.getValue() instanceof QueryRepresentable) {
            ((QueryRepresentable) criterion.getValue()).represent(this, StringHelper.OPEN_PAREN,
                    StringHelper.CLOSED_PAREN);
        } else if (criterion.getValue() instanceof Date) {
            representDate((Date) criterion.getValue());
            builder.append(StringHelper.SPACE).append(BetweenCriterion.AND).append(
                    StringHelper.SPACE);
            representDate((Date) criterion.getHighValue());
        } else {
            builder.append(criterion.getValue());
            builder.append(StringHelper.SPACE).append(BetweenCriterion.AND).append(
                    StringHelper.SPACE);
            builder.append(criterion.getHighValue().toString());
        }

        return true;
    }

    public <T> boolean representSimpleCriterion(SimpleCriterion<T> criterion) {
        QueryRepresentable field = criterion.getField();
        Object value = criterion.getValue();
        OperatorType operator = criterion.getOperator();

        // validation of value
        if (operator != OperatorType.Null && operator != OperatorType.NotNull && value == null) {
            // no need to represent.
            builder.append("1=1");
            return true;
        }

        // field
        field.represent(this, StringHelper.OPEN_PAREN, StringHelper.CLOSED_PAREN);

        // operator
        builder.append(StringHelper.SPACE).append(operator.toToken());

        // value
        if (value instanceof QueryRepresentable) {
            ((QueryRepresentable) value).represent(this, StringHelper.OPEN_PAREN,
                    StringHelper.CLOSED_PAREN);
        } else if (operator == OperatorType.In) {
            builder.append(StringHelper.OPEN_PAREN);
            builder.append(StringHelper.join((Collection<Object>) value, StringHelper.COMMA, "'%s'"));
            builder.append(StringHelper.CLOSED_PAREN);
        } else if (operator != OperatorType.Null && operator != OperatorType.NotNull) {
            if ((value instanceof String || value instanceof Character)
                    && !"?".equalsIgnoreCase((String) value)) {
                representString(value.toString());
            } else if (value instanceof Date) {
                representDate((Date) value);
            } else {
                builder.append(value);
            }
        }

        return true;

    }

    public <T extends QueryRepresentable> boolean representQueryRepresentableList(List<T> list,
                                                                                  String delima) {
        String currDelima = "";
        for (T item : list) {
            builder.append(currDelima);
            item.represent(this, "", "");
            currDelima = delima;
        }

        return true;
    }

    public boolean representColumn(Column col) {
        StringBuilder sb = new StringBuilder();

        // field
        if (col.hasFromName()) {
            sb.append(getCompliance(col.getFromName())).append(StringHelper.DOT);
        }
        sb.append(col.getName());

        // function
        if (col.getFunction() != null) {
            builder.append(col.getFunction().getValue()).append(StringHelper.OPEN_PAREN).append(sb).append(
                    StringHelper.CLOSED_PAREN);
        } else {
            builder.append(sb);
        }

        // alias
        if (queryMode == QueryMode.UPDATE_DATA) {
            Object value = col.getValue();
            builder.append("=");
            if ((value instanceof String || value instanceof Character)
                    && !"?".equalsIgnoreCase((String) value)) {
                representString(value.toString());
            } else if (value instanceof Date) {
                representDate((Date) value);
            } else if (value == null) {
                builder.append("null");
            } else {
                builder.append(value);
            }
        } else if (col.hasAlias()) {
            builder.append(Column.AS_CLAUSE).append(col.getAlias());
        }
        return true;
    }

    public boolean representSearch(Search search, String opener, String closer) {
        builder.append(opener);
        QueryMode queryMode = this.queryMode;
        if (StringHelper.isNotEmpty(opener)) {
            //inner queries should not be Mode depended.
            queryMode = QueryMode.GET_DATA;
        }

        // select & delete
        if (queryMode == QueryMode.DELETE_DATA) {
            builder.append(" Delete ");
        } else if (queryMode == QueryMode.GET_COUNT || search.getColumnList().size() > 0) {
            builder.append(Column.SELECT_CLAUSE);
        }

        // column list
        if (queryMode == QueryMode.GET_COUNT) {
            builder.append(" Count(*) ");
        } else if (search.getColumnList().size() > 0) {
            representQueryRepresentableList(search.getColumnList(), StringHelper.COMMA);
        }

        // from clause
        builder.append(StringHelper.SPACE);
        builder.append(From.FROM_CLAUSE);
        representQueryRepresentableList(search.getFromList(), StringHelper.COMMA);

        // filters
        if (search.getRootCriterion() != null) {
            builder.append(Criterion.WHERE_CLAUSE);
            search.getRootCriterion().represent(this, "", "");
        }

        // order by
        if (queryMode == QueryMode.GET_DATA && search.getOrderList().size() > 0) {
            builder.append(SqlKeywords.Space).append(Order.ORDER_CLAUSE).append(SqlKeywords.Space);
            representQueryRepresentableList(search.getOrderList(), StringHelper.COMMA);
        }
        builder.append(closer);
        return true;
    }

    public boolean representOrderclause(Order order, boolean reverse, boolean useAlias) {
        if (useAlias && StringHelper.isNotEmpty(order.getAlias())) {
            builder.append(order.getAlias()).append(SqlKeywords.FieldSeparator);
        }

        builder.append(order.getName());

        OrderType type = order.getType();
        if (reverse) {
            if (type == OrderType.ASC) {
                type = OrderType.DESC;
            } else {
                type = OrderType.ASC;
            }
        }
        builder.append(SqlKeywords.Space).append(type);

        return true;
    }

    /**
     * Warnning: Object constructing parameter [StringBuilder] will be
     * deattached.
     */
    public void clear() {
        if (isNativeBuilder) {
            builder.delete(0, builder.length());
        } else {
            builder = new StringBuilder();
            isNativeBuilder = true;
        }
    }

    public Object getRepresentation() {
        return this.builder;
    }

    private String getCompliance(String str) {
        // 1.b.3 -> _1.b._3
        return str.replaceAll("(\\.|^)(\\d)", "$1_$2");
    }

}
