package com.avanza.core.data.expression;

import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CriteriaParser {


    private enum ExpType {
        None, Simple, Binary, List, Complex
    }
	
	/*
	private class ParserFrame {

		public ExpType lastExpType;
		public int parenCount;
		public Criterion root;
		public Criterion last;
		public int recurLevel;
		
		public ParserFrame(int recurLevel) {
			this.lastExpType = ExpType.None;
			this.parenCount = 0;
			this.recurLevel = recurLevel;
		}
	}
	*/

    private static final String WHERE = Criterion.WHERE_CLAUSE.substring(1);
    private static final String keywords = "([\\(\\)" + OperatorType.KEYWORDS + "])|(like)|(and)|(>=)|(<=)|(<>)|(is null)|(between)|(not in)|(or)|(in)|(and)";

    private String whereClause;
    private Matcher matchList;
    private int lastMatchEnd;
    private boolean found;
    public ExpType lastExpType;
    public int parenCount;
    public Criterion root;
    public Criterion last;
    public int lastRecLevel;

    private CriteriaParser(String whereClause) {

        this.whereClause = whereClause;
        Pattern pattern = Pattern.compile(CriteriaParser.keywords);
        this.matchList = pattern.matcher(whereClause.toLowerCase());
        this.lastExpType = ExpType.None;
    }

    public static Criterion parseClause(String whereClause) {

        if (whereClause != null)
            whereClause = whereClause.trim();

        Guard.checkNullOrEmpty(whereClause, "CriteriaParser.parseClause(String)");

        if (StringHelper.startsWithIgnoreCase(whereClause, CriteriaParser.WHERE))
            whereClause = whereClause.substring(CriteriaParser.WHERE.length()).trim();

        CriteriaParser parser = new CriteriaParser(whereClause);
        return parser.parse();
    }

    private boolean hasMore() {

        return this.found;
    }

    private void findFirst() {

        this.found = this.matchList.find();
    }

    private void findNext() {

        this.lastMatchEnd = this.matchList.end();
        this.findFirst();
    }

    private String getString() {

        String retVal;

        if (this.found)
            retVal = this.whereClause.substring(this.lastMatchEnd, this.matchList.start());
        else
            retVal = this.whereClause.substring(this.lastMatchEnd);

        return retVal.trim();
    }


    private Criterion parse() {

        this.clearState();
        this.findFirst();
        this.innerParse(0);

        if (this.parenCount > 0)
            this.throwDataException(String.format("Incorrect no. of paraenthesis found."));

        if (this.root == null) {
            if (this.last != null)
                this.root = this.last;
            else
                throw new ExpressionException("Invalid format. Failed to parse exception %s.", this.whereClause);
        }

        return this.root;
    }

    private void clearState() {

        this.found = false;
        this.matchList.reset();
        this.lastMatchEnd = 0;
        this.lastExpType = ExpType.None;
        this.parenCount = 0;
        this.root = null;
        this.last = null;
        this.lastRecLevel = 0;
    }

    private Criterion innerParse(int recLevel) {

        Criterion retVal = null;

        while (this.hasMore()) {

            String temp = this.matchList.group();
            if (temp.equals(StringHelper.OPEN_PAREN)) {

                this.parenCount++;
                this.findNext();
            } else if (temp.equals(StringHelper.CLOSED_PAREN)) {

                if (this.parenCount == 0)
                    this.throwDataException(String.format("args)%s must be paired with %s", StringHelper.CLOSED_PAREN, StringHelper.OPEN_PAREN));

                this.parenCount--;
                this.findNext();

                // if true, return recursive call back to parseComplex
                if ((this.lastRecLevel > 0) && (this.parenCount == this.lastRecLevel))
                    break;
            } else {

                OperatorType operator = OperatorType.parseToken(temp);

                if (operator.isComplex())
                    retVal = this.parseComplex(operator);
                else if (operator.isList())
                    retVal = this.parseList(operator);
                else if (operator.isBinary())
                    retVal = this.parseBinary(operator);
                else if (operator.isSimple())
                    retVal = this.parseSimple(operator);
                else
                    this.throwDataException(String.format("%s is not classified in any operatory category", temp));
            }
        }

        return retVal;
    }

    private void setLast(Criterion last, ExpType type) {

        if (root == null) {
            this.root = last;
        }

        if (type == ExpType.Complex) {

            if (last != this.root) {
                ComplexCriterion temp = (ComplexCriterion) last;
                if ((temp.getLhs() == this.root) || (temp.getRhs() == this.root))
                    this.root = last;
            }
        }

        this.last = last;
        this.lastExpType = type;
    }

    private Criterion parseComplex(OperatorType type) {

        if (this.last == null)
            this.throwDataException(String.format("Expected expression on the left hands side of %s operator.", type.toString()));

        ComplexCriterion retVal = null;

        // The statement is required, if there is a hierarchy of complex expressions
        // along with a list of complex expressions at certain level
        if (this.parenCount < this.lastRecLevel)
            this.last = this.root;

        if ((this.lastRecLevel == this.parenCount) && (this.last instanceof ComplexCriterion)) {

            ComplexCriterion temp = (ComplexCriterion) this.last;
            retVal = new ComplexCriterion(type, temp.getRhs());
            temp.setRhs(retVal);
        } else {

            retVal = new ComplexCriterion(type, this.last);
        }


        this.lastRecLevel = this.parenCount;
        this.setLast(retVal, ExpType.Complex);

        this.findNext();
        Criterion rhs = this.innerParse(this.parenCount);

        if (rhs == null)
            this.throwDataException(String.format("Expectected subexpression on the right hands side of %s operator", type.toString()));

        retVal.setRhs(rhs);
        this.lastRecLevel = this.parenCount;
        this.setLast(retVal, ExpType.Complex);

        return retVal;
    }

    private void throwDataException(String message) {

        throw new ExpressionException("Error at position %d. %s. Expression[%s]",
                this.matchList.start(), message, this.whereClause);
    }

    private Criterion parseSimple(OperatorType type) {

        if ((this.last != null) && (this.lastExpType != ExpType.Complex))
            this.throwDataException("Only compound criteria can be used to relate to simple critera");

        Criterion retVal = null;
        String name = this.getString();

        this.findNext();

        String value = null;

        if (type != OperatorType.Null)
            value = this.getString();

        retVal = new SimpleCriterion<String>(type, name, "", value);

        this.setLast(retVal, ExpType.Simple);

        return retVal;
		/*
		switch(type) {
		
		case Gt:
			retVal = Criterion.greater(name, value);
			break;
		case Lt:
			retVal = Criterion.less(name, value);
			break;
		case Eq:
			retVal = Criterion.equal(name, value);
			break;
		case NEq:
			retVal = Criterion.notEqual(name, value);
			break;
		case GtEq:
			retVal = Criterion.greaterOrEqual(name, value);
			break;
		case LtEq:
			retVal = Criterion.lessOrEqual(name, value);
			break;
		case Like:
			retVal = Criterion.like(name, value);
			break;
		case null:
			retVal = Criterion.nullCriterion(field);
			break;
		}
		*/
    }

    private Criterion parseList(OperatorType type) {

        return null;
    }

    private Criterion parseBinary(OperatorType type) {

        return null;
    }
}
