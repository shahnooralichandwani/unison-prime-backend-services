package com.avanza.core.data.db.sql;

import com.avanza.core.util.DataType;
import com.avanza.integration.dbmapper.meta.DbMetaParamType;

import java.io.Serializable;


public class SqlParameter implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1408446880762436768L;

    private DataType dataType;
    private String name;
    private Object value;
    private DbMetaParamType paramType;

    public <T> SqlParameter(String name, T value) {
        this((value != null ? DataType.fromJavaType(value.getClass()) : DataType.None), name, value, DbMetaParamType.Input);
    }

    public <T> SqlParameter(DataType dataType, String name, T value) {
        this(dataType, name, value, DbMetaParamType.InOut);
    }

    public <T> SqlParameter(DataType dataType, String name) {
        this(dataType, name, null, DbMetaParamType.Output);
    }

    public <T> SqlParameter(DataType dataType, String name, T value, DbMetaParamType paramType) {
        this.dataType = dataType;
        this.name = name;
        this.value = value;
        this.paramType = paramType;
    }

    public DbMetaParamType getParamType() {
        return paramType;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public void setParamType(DbMetaParamType paramType) {
        this.paramType = paramType;
    }

    @Override
    public String toString() {
        return String.format("DataType: %s, Name: %s, Value: %s, ParameterType: %s", this.dataType, this.name, this.value, this.paramType);
    }
}
