package com.avanza.core.data.sequence;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.SequenceDataBroker;
import com.avanza.core.data.expression.Search;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Singleton class
public class SequenceManager {

    private static SequenceManager _instance = new SequenceManager();
    private boolean isLoaded = false;
    private Map<String, SequenceGenerator> sequenceGenerators;

    protected SequenceManager() {
        sequenceGenerators = new HashMap<String, SequenceGenerator>();
    }

    public static synchronized SequenceManager getInstance() {

        return _instance;
    }

    public void load() {

        if (isLoaded)
            return;

        Search search = new Search();
        search.addFrom("Meta_Counter");
        search.addColumn("*");
        SequenceDataBroker broker = (SequenceDataBroker) DataRepository.getBroker("SequenceDataBroker");
        List<SequenceInfo> sequenceInfoList = broker.findSequenceInfos(search.toString());

        for (SequenceInfo sequenceinfo : sequenceInfoList) {

            sequenceGenerators.put(sequenceinfo.getCounterName(), sequenceinfo.getSequenceGenerator());
        }

        isLoaded = true;
    }

    public void dispose() {

    }

    public String getNextValue(String sequenceName, String... keyParts) {

        return sequenceGenerators.get(sequenceName).getNextValue(keyParts);
    }

    public long getNextValueAsLong(String sequenceName) {

        return sequenceGenerators.get(sequenceName).getNextValueAsLong();
    }

    public int getNextValueasInt(String sequenceName) {

        return sequenceGenerators.get(sequenceName).getNextValueasInt();
    }

    public List<String> getNextValue(String sequenceName, int bucketSize, String... keyParts) {

        return sequenceGenerators.get(sequenceName).getNextValue(bucketSize, keyParts);
    }

    public List<Long> getNextValueAsLong(String sequenceName, int bucketSize) {

        return sequenceGenerators.get(sequenceName).getNextValueAsLong(bucketSize);
    }

    public List<Integer> getNextValueasInt(String sequenceName, int bucketSize) {

        return sequenceGenerators.get(sequenceName).getNextValueasInt(bucketSize);
    }

    public SequenceInfo getSequenceInfo(String counterName) {

        return sequenceGenerators.get(counterName).getSequenceInfo();
    }

}
