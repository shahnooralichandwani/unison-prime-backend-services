package com.avanza.core.data.expression;

import com.avanza.core.util.Guard;
import com.avanza.core.util.MapItem;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.StringMap;

import java.util.Iterator;
import java.util.List;

@Deprecated
public class ExpressionList<T extends Expression> {

    private StringMap<T> expList;

    public ExpressionList() {

        this.expList = new StringMap<T>();
    }

    public ExpressionList(int capacity) {

        this.expList = new StringMap<T>(capacity);
    }

    public void add(T item) {

        Guard.checkNull(item, "ExpressionList.add(T)");
        this.expList.add(item.getName(), item);
    }

    public void add(T[] clauses) {

        Guard.checkNull(clauses, "ExpressionList.add(T[])");
        for (int idx = 0; idx < clauses.length; idx++) {
            T temp = clauses[idx];
            this.add(temp);
        }
    }

    public void add(List<T> clauseList) {

        Guard.checkNull(clauseList, "ExpressionList.add(List<T>)");
        for (int idx = 0; idx < clauseList.size(); idx++) {
            T temp = clauseList.get(idx);
            this.add(temp);
        }
    }

    public T get(String name) {

        return this.expList.get(name);
    }

    public T get(int index) {

        return this.expList.get(index);
    }

    public T remove(String name) {

        return this.expList.remove(name);
    }

    public void clear() {

        this.expList.clear();
    }

    public int size() {

        return this.expList.size();
    }

    public Iterator<MapItem<T>> getExpressionIterator() {
        return expList.iterator();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        int idx = 0;
        expList.get(idx++).toString(builder);

        for (; idx < expList.size(); idx++) {

            builder.append(StringHelper.COMMA);
            expList.get(idx).toString(builder);
        }
        return builder.toString();
    }
}
