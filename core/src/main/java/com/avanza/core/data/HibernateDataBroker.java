package com.avanza.core.data;

import com.avanza.core.CoreException;
import com.avanza.core.FunctionDelegate;
import com.avanza.core.cache.Cacheable;
import com.avanza.core.cache.CustomerSessionCacheUtil;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.db.DbInfo;
import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.expression.HqlQueryRepresentator;
import com.avanza.core.data.expression.QueryRepresentator.QueryMode;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SearchConversionUtil;
import com.avanza.core.interceptor.AuditLogInterceptor;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.sdo.DataException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.TimeLogger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.reader.XMLDocumentReader;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Majid Mehmood Defines helper methods for Hibernate Broker Must get a
 * valid Session from Persistence Context
 */

public class HibernateDataBroker implements DataBroker {

    private static final String XML_XMLPATH = "xmlpath";
    private static final String SESSION_SCOPE_BASED_ON = "sessionScope";
    private static final String XML_KEY = "db-key";
    private static final Logger logger = Logger.getLogger(HibernateDataBroker.class);
    private static final String ERROR_MSG = "Exception occoured in HibernateDataBroker while invoking method %1$s";
    protected SessionFactory sessionFactory;
    private String brokerKey;
    private String dataSource;
    private SessionScope sessionScope;

    public String getBrokerKey() {
        return brokerKey;
    }

    protected SessionScope getSessionScope() {
        return sessionScope;
    }

    public HibernateDataBroker() {

    }

    public void dispose() {

    }

    public void enableFilter(final String filterName) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("HibernateDataBroker.enableFilter(%s)",
                filterName.getClass().getSimpleName()));
        try {
            org.hibernate.Session session = getSession();
            if (filterName != null && !"".equalsIgnoreCase(filterName))
                session.enableFilter(filterName);
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t, "enableFilter(final filterName)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    public void disableFilter(final String filterName) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("HibernateDataBroker.disableFilter(%s)",
                filterName.getClass().getSimpleName()));
        try {
            org.hibernate.Session session = getSession();
            if (filterName != null && !"".equalsIgnoreCase(filterName))
                session.disableFilter(filterName);
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t, "disableFilter(final filterName)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    public <T> void add(final T item) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("HibernateDataBroker.add(%s)", item.getClass().getSimpleName()));
        Session session = getDataSession();
        boolean tranActive = false;
        try {

            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            tranActive = session.isTransactionActive();
            if (item instanceof DbObject) {
                DbObject temp = (DbObject) item;
                this.setUserSpecificData(temp, false);
            }
            session.beginTransaction();
            hbSession.save(item);
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            logger.LogException(ERROR_MSG, t, "add(final T item)");
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timer.end();
        }
    }

    public <T> void saveOrUpdate(final T item) {

        Session session = getDataSession();
        boolean tranActive = false;
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            tranActive = session.isTransactionActive();
            session.beginTransaction();
            if (item instanceof DbObject) {
                DbObject temp = (DbObject) item;
                this.setUserSpecificData(temp, false);
            }
            hbSession.saveOrUpdate(item);
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            logger.LogException(ERROR_MSG, t, "saveOrUpdate(final T item)");
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
        }
    }

    public <T> void delete(final T item) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("HibernateDataBroker.delete(%s)", item.getClass().getSimpleName()));
        Session session = getDataSession();
        boolean tranActive = false;
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            tranActive = session.isTransactionActive();
            session.beginTransaction();
            if (item instanceof DbObject) {
                DbObject temp = (DbObject) item;
                this.setUserSpecificData(temp, true);
            }

            hbSession.delete(item);
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            logger.LogException(ERROR_MSG, t, "delete(final T item)");
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timer.end();
        }
    }

    public <T> List<T> find(String query) {
        List<T> retVal = null;
        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.find(%s)", query));
        Session session = getDataSession();
        try {

            org.hibernate.Session hbSession = (org.hibernate.Session) session
                    .value();
            hbSession.clear();
            Query q = hbSession.createQuery(query);
            List<Map<String, Object>> list = (List<Map<String, Object>>) q.list();
            retVal = (List<T>) MetaDataRegistry.getDataObjects(list);
            if (retVal != null)
                CustomerSessionCacheUtil.updateCache((List<Cacheable>) retVal);
            return retVal;
        } catch (Exception e) {
            e.printStackTrace();
            throw new DataException(e,
                    "Exception occured while fetching list: "
                            + query.toString());
        } finally {
            session.notifyOnTaskFinished();
            timmer.end(query.toString());
        }
    }


    @SuppressWarnings("unchecked")
    public <T> List<T> find(Search query) {
        TimeLogger timmer = TimeLogger.init();
        if (query.getHqlCriteria() != null)
            timmer.begin(String.format("HibernateDataBroker.find(%s)",
                    query.getHqlCriteria().toString()));
        else
            timmer.begin(String.format("HibernateDataBroker.find(%s)",
                    query.getFirstFrom().toString()));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            List result = null;
            if (query.canRepresent()) {
                String queryString = query.getRepresentation(new HqlQueryRepresentator(QueryMode.GET_COUNT));
                Query q = hbSession.createQuery(queryString);
                //Gibran: uniqueResult() returns null or single record therefore null check implemented
                Object queryResult = q.uniqueResult();
                Integer recordCount = 0;
                if (queryResult != null)
                    recordCount = Integer.valueOf(((Long) queryResult).intValue());
                int pageSize = (query.getBatchSize() > 0) ? query.getBatchSize() : 100;
                result = new LazyList<T>(recordCount, pageSize, query, new FunctionDelegate.Arg3<List<? extends Object>, Search, Integer, Integer>() {
                    public List<? extends Object> call(Search search, Integer pageNo, Integer pageSize) {
                        return findItemsByRange(search, pageNo, pageSize, "", true, null);
                    }
                });
            } else if (query.getHqlCriteria() != null) {
                result = query.getHqlCriteria().list();
            } else {
                result = SearchConversionUtil.getHQLSearchObject(query, hbSession).list();
            }
            return result;
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t, "find(Search query)");
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timmer.end((query.getHqlCriteria() != null)
                    ? query.getHqlCriteria().toString()
                    : query.toString());
        }
    }

    @SuppressWarnings("unchecked")
    public <T, ID extends Serializable> T findById(Class<T> t, ID id) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("HibernateDataBroker.findById(%s, %s)", t.getSimpleName(), id));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            Object a = (T) (hbSession.get(t, id));
            return (T) a;
        } catch (Throwable th) {
            logger.LogException(ERROR_MSG, th, "findById(Class<T> t, ID id)");
            throw new CoreException(th);
        } finally {
            session.notifyOnTaskFinished();
            timer.end();
        }
    }

    private void updateHBMCofigWithDataSourceInfo(Document xmlDocument, DbInfo dbInfo) {
        NodeList list = xmlDocument.getElementsByTagName("property");
        for (int i = 0; i < list.getLength(); i++) {
            Element element = (Element) list.item(i);

            if (element.getAttribute("name").equalsIgnoreCase("connection.username")) {
                //element.setTextContent(dbInfo.getUser());
            } else if (element.getAttribute("name").equalsIgnoreCase("connection.password")) {
                //	element.setTextContent(dbInfo.getPassword());
            } else if (element.getAttribute("name").equalsIgnoreCase("connection.url")) {
                //	element.setTextContent(dbInfo.getConnectionUri());
            } else if (element.getAttribute("name").equalsIgnoreCase("connection.driver_class")) {
                //	element.setTextContent(dbInfo.getDriver());
            }
        }

    }

    public void load(ConfigSection configSection) {

        TimeLogger timer = TimeLogger.init();
        timer.begin("HibernateDataBroker.load(config)");

        try {
            String baseAppPath = ApplicationLoader.BASE_APPLICATION_PATH;
            String filePath = baseAppPath + File.separator
                    + configSection.getTextValue(HibernateDataBroker.XML_XMLPATH);

            this.brokerKey = configSection.getTextValue("key");
            this.dataSource = configSection.getTextValue("dataSource");
            DbInfo dbInfo = DataRepository.getDbInfo(dataSource);


            if (filePath != null && dbInfo != null) {


                // Create the SessionFactory from hibernate.cfg.xml
                XMLDocumentReader xmlDocumentReader = new XMLDocumentReader();
                Document xmlDocument = xmlDocumentReader.readXMLDocument(filePath);
                updateHBMCofigWithDataSourceInfo(xmlDocument, dbInfo);
                try {
                   /* Configuration configuration =  new Configuration().setInterceptor(new AuditLogInterceptor()).configure(xmlDocument);
                    configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServer2012Dialect");
                    configuration.setProperty("hibernate.connection.driver_class", "net.sourceforge.jtds.jdbc.Driver");
                    configuration.setProperty("hibernate.connection.url", "jdbc:jtds:sqlserver://localhost:1433/UNISON_PD;charset=CP1256;socketTimeout=5;");
                    configuration.setProperty("hibernate.connection.username", "sa");
                    configuration.setProperty("hibernate.connection.password", "abcd@1234");
                    configuration.setProperty("show_sql", "true");
                    configuration.setProperty("format_sql", "true");
                    configuration.setProperty("use_sql_comments", "true");


                    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                            configuration.getProperties()).build();
                    sessionFactory = configuration.setInterceptor(new AuditLogInterceptor()).buildSessionFactory(serviceRegistry);*/
                    sessionFactory = new Configuration().setInterceptor(new AuditLogInterceptor()).configure(xmlDocument).buildSessionFactory();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else
                throw new DataSourceException(
                        "File %s not found while loading the HibernateDataBroker config.",
                        configSection.getTextValue(HibernateDataBroker.XML_XMLPATH));

            this.sessionScope = SessionScope.fromValue(configSection.getValue(
                    SESSION_SCOPE_BASED_ON, SessionScope.REQUEST_BASED.value));
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t, "load(ConfigSection configSection)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    public <T> void persist(final T item) {
        // escape(item);
        DbObject dbObj = (DbObject) item;
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("HibernateDataBroker.persist(%s)",
                item.getClass().getSimpleName()));
        Session session = getDataSession();
        boolean tranActive = false;
        try {
            tranActive = session.isTransactionActive();
            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            session.beginTransaction();
            if (item instanceof DbObject) {

                if (dbObj.isNew()) {

                    this.setUserSpecificData(dbObj, false);
                    hbSession.save(dbObj);
                } else {
                    this.setUserSpecificData(dbObj, true);
                    hbSession.update(dbObj);
                }
            } else
                hbSession.saveOrUpdate(item);
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            logger.LogException(ERROR_MSG, t, "persist(final T item)");
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timer.end();
        }
    }

    public void synchronize() {

        throw new DataSourceException("Method HibernateDataBroker.synchronize() not implemented");
    }

    public <T> void update(T item) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("HibernateDataBroker.update(%s)", item.getClass().getSimpleName()));
        Session session = getDataSession();
        boolean tranActive = false;
        try {
            tranActive = session.isTransactionActive();
            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            session.beginTransaction();
            if (item instanceof DbObject) {
                // escape(item);
                DbObject temp = (DbObject) item;
                this.setUserSpecificData(temp, true);
            }

            hbSession.update(item);
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            logger.LogException(ERROR_MSG, t, "update(T item)");
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timer.end();
        }
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> findAll(Class<T> t) {
        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("HibernateDataBroker.findAll(%s)", t.getSimpleName()));
        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            List list = hbSession.createCriteria(t).list();
            return list;
        } catch (Throwable th) {
            logger.LogException(ERROR_MSG, th, "findAll(Class<T> t)");
            throw new CoreException(th);
        } finally {
            session.notifyOnTaskFinished();
            timer.end();
        }
    }

    public <T> List<T> findAll(String entityName) {

        throw new DataSourceException(
                "Method HibernateDataBroker.findAll(String) is not implemented");
    }

    public <T> void setUserSpecificData(DbObject item, boolean isModified) {
        try {
            User admin = ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);
            String loginId = User.UNKNOWN_USER;

            // If the user is not found in the ApplicationContext then use the
            // Unknown user.
            if (admin != null)
                loginId = admin.getLoginId();

            if (!isModified)
                item.setCreation(loginId);
            else
                item.setUpdation(loginId);
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t,
                    "setUserSpecificData(DbObject item, boolean isModified)");
            throw new CoreException(t);
        }
    }

    // TODO: This method should return data.Session. So transaction can be
    // handled through the broker.
    // Hibernate's implementer class should be independent for the broker.
    // So Broker session should be created somewhere else.
    // It is bad approach to used generic this way.
    public <T> T getSession() {
        return (T) getDataSession().value();
    }

    // TODO: this method will be replaced later, by getSession.
    protected Session getDataSession() {
        return (Session) SessionHibernateImpl.createSession(this.sessionFactory,
                this.getBrokerKey(), sessionScope);
    }

    // rollback and commit behavior can be overridden by subclass.
    protected void rollback(boolean isActiveTransaction) {
        if (!isActiveTransaction) {
            getDataSession().rollback();
        }
    }

    protected void commit(boolean isActiveTransaction) {
        if (!isActiveTransaction) {
            getDataSession().commit();
        }
    }

    public <ID, T extends DataObject> T findById(String entityId, ID id) throws DataSourceException {
        return null;
    }

    public String getDataSource() {
        return dataSource;
    }

    public ProviderType getProviderType() {
        return DataRepository.getDbInfo(dataSource).getType();
    }

    /*
     * Shahbaz: Campaign&Outbound. To insert in batch(non-Javadoc)
     *
     * @see com.avanza.core.data.DataBroker#persistAll(java.util.List)
     */
    public <T> void persistAll(List<T> items) {

        TimeLogger timer = TimeLogger.init();
        timer.begin(String.format("HibernateDataBroker.persist(%s)",
                items.get(0).getClass().getSimpleName()));
        Session session = getDataSession();
        boolean tranActive = false;
        try {
            tranActive = session.isTransactionActive();
            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            session.beginTransaction();
            int count = 1;
            for (T item : items) {
                // escape(item);
                DbObject dbObj = (DbObject) item;

                if (item instanceof DbObject) {

                    if (dbObj.isNew()) {

                        this.setUserSpecificData(dbObj, false);
                        hbSession.save(dbObj);
                    } else {
                        this.setUserSpecificData(dbObj, true);
                        hbSession.update(dbObj);
                    }
                } else
                    hbSession.saveOrUpdate(item);

                if (count == 50) {
                    hbSession.flush();
                    hbSession.clear();
                    count = 0;
                }

                count++;

            }

            commit(tranActive);

        } catch (Throwable t) {
            rollback(tranActive);
            logger.LogException(ERROR_MSG, t, "persistAll(final List<T> items)");
            throw new CoreException(t);
        } finally {
            timer.end();
        }
    }

    /*
     * This function is used to escape javascript and html to save String data
     * type field, this function can be used later
     */
    /*
     * public <T> void escape(T item){ Field fields[]=
     * item.getClass().getDeclaredFields(); for(Field field : fields){
     * if(field.getType() == String.class &&
     * !Modifier.isFinal(field.getModifiers()) &&
     * !Modifier.isStatic(field.getModifiers())) try {
     * field.setAccessible(true); if(field.get(item) != null){ String value =
     * StringEscapeUtils.escapeHtml(field.get(item).toString()); value =
     * StringEscapeUtils.escapeJavaScript(value); field.set(item, value); } }
     * catch (IllegalArgumentException t) { logger.LogException(ERROR_MSG, t,
     * "persist(<T> item)"); throw new CoreException(t); } catch
     * (IllegalAccessException t) { logger.LogException(ERROR_MSG, t,
     * "persist(<T> item)"); throw new CoreException(t); } } }
     */

    public static void main(String[] args) {

    }

    @Override
    public <T> List<T> findItemsByRange(Search search, int pageNo, int pageSize, String orderBy,
                                        boolean isAsc, int[] returnTotalRowCount) {

        Session session = getDataSession();
        try {
            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();

            if (search.canRepresent()) {
                String queryString = search.getRepresentation(new HqlQueryRepresentator());
                Query q = hbSession.createQuery(queryString);
                q.setFirstResult(pageNo * pageSize).setMaxResults(pageSize);
                List<T> list = q.list();
                return list;
            } else {
                throw new RuntimeException("Search.canRepresent() must be true");
            }
        } catch (Exception ex) {
            logger.LogException("[Exception has occured]", ex);
        } finally {
        }
        return null;

    }

    public org.hibernate.Session getTempSession() {
        return sessionFactory.openSession();
    }


    @Override
    public int getCriteriaCount(Search criteria) {
        Criteria hqlCriteria = criteria.getHqlCriteria();
        hqlCriteria.setProjection(Projections.rowCount());
        int count = (Integer) hqlCriteria.uniqueResult();
        hqlCriteria.setProjection(null);
        return count;
    }

    public int deleteBySearch(Search search) {
        TimeLogger timmer = TimeLogger.init();
        timmer.begin(String.format("MetaDataBroker.deleteBySearch(%s)", search.isObjectBased()
                ? search.getFirstFrom().toString()
                : search.getFromList().toString()));
        Session session = getDataSession();
        boolean tranActive = session.isTransactionActive();
        int recordCount = 0;
        try {

            org.hibernate.Session hbSession = (org.hibernate.Session) session.value();
            session.beginTransaction();
            if (search.canRepresent()) {
                String queryString = search.getRepresentation(new HqlQueryRepresentator(
                        QueryMode.DELETE_DATA));
                Query q = hbSession.createQuery(queryString);
                recordCount = q.executeUpdate();
            } else {
                throw new RuntimeException("canRepresent must be true in Search object");
            }
            commit(tranActive);
        } catch (Throwable t) {
            rollback(tranActive);
            throw new CoreException(t);
        } finally {
            session.notifyOnTaskFinished();
            timmer.end();
        }

        return recordCount;

    }

}
