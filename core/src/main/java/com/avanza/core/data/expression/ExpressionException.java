package com.avanza.core.data.expression;

public class ExpressionException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ExpressionException(String message) {

        super(message);
    }

    public ExpressionException(String format, Object... args) {

        super(String.format(format, args));
    }

    public ExpressionException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public ExpressionException(RuntimeException innerExcep, String format, Object... args) {

        super(String.format(format, args, innerExcep));
    }
}
