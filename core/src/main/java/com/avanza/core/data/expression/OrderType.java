package com.avanza.core.data.expression;

public enum OrderType {

    ASC, DESC;

    public static OrderType fromString(String strVal) {
        if (strVal.equalsIgnoreCase(OrderType.ASC.name()))
            return OrderType.ASC;
        if (strVal.equalsIgnoreCase(OrderType.DESC.name()))
            return OrderType.DESC;
        return null;
    }
}
