package com.avanza.core.data.sequence;

import com.avanza.core.CoreException;

public class SequenceException extends CoreException {

    private static final long serialVersionUID = -606122954338075010L;

    public SequenceException(String message) {

        super(message);
    }

    public SequenceException(String format, Object... args) {

        super(format, args);
    }

    public SequenceException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public SequenceException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public SequenceException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public SequenceException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}
