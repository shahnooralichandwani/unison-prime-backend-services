package com.avanza.core.data;

import com.avanza.core.FunctionDelegate;
import com.avanza.core.data.expression.Search;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * It is used to avoid the system getting overloaded. It queries more data iteratively only if required.
 *
 * @param <T>
 */
public class LazyList<T extends Object> extends AbstractList<T> implements Iterator<T> {

    int size;
    int pageSize = 4;
    int currentPage = 0;
    int currentPageCursor = 0;
    private Search search;
    private FunctionDelegate.Arg3<List<? extends Object>, Search, Integer, Integer> dataFetcher;

    private List<T> currentPageData;
    private FunctionDelegate.Arg1<T, Object> transformer;

    //it is very common that developers get value of first index if only single recored being assumed in the list.
    //so we will not discard first record in the list.
    //by using this approach, we will avoid extra request to db which is only for getting first record.
    private T firstItem;

    private FunctionDelegate.Arg1<T, Integer> indexBasedGetter = new FunctionDelegate.Arg1<T, Integer>() {

        public T call(Integer index) {
            System.out.println(index);
            //return firstItem if already fetched earlier.
            if (index == 0 && firstItem != null) {
                return firstItem;
            }

            //zero based index
            index = index + 1;

            //page number.
            int page = (int) Math.ceil((double) index / (double) pageSize);

            //page cursor
            int pageCursor = index % pageSize;
            if (pageCursor == 0) {
                pageCursor = pageSize;
            }

            //page data
            List<T> pageData = currentPageData;
            if (currentPage != page) {
                //no need to request for data
                pageData = retrievePage(page);
            }

            T obj = pageData.get(pageCursor - 1);
            currentPage = page;
            currentPageCursor = pageCursor;
            currentPageData = pageData;

            //initialize firstItem if it is first record
            if (index == 1) {
                firstItem = obj;
            }

            return obj;

        }
    };

    private FunctionDelegate.Arg1<T, Integer> iteratorBasedGetter = new FunctionDelegate.Arg1<T, Integer>() {

        public T call(Integer index) {
            return currentPageData.get(currentPageCursor - 1);
        }
    };

    private FunctionDelegate.Arg1<T, Integer> getter;

    /**
     * @param size
     * @param dataFetcher Return call(Search search,Integer pageNumber,Integer pageSize);
     */
    public LazyList(int size, int pageSize, Search search, FunctionDelegate.Arg3<List<? extends Object>, Search, Integer, Integer> dataFetcher) {
        this.size = size;
        this.pageSize = pageSize;
        this.dataFetcher = dataFetcher;
        this.search = search;

        //default getter.
        getter = indexBasedGetter;
    }

    private int currentPosition() {
        int pos = ((currentPage - 1) * pageSize) + currentPageCursor;
        return (pos < 0) ? 0 : pos;
    }

    private int remainingRecords() {
        return size() - (pageSize * currentPage);
    }

    public T get(int index) {
        return getter.call(index);
    }

    public int size() {
        return size;
    }


    public boolean hasNext() {
        return size() > currentPosition();
    }

    public T next() {
        if (hasNext()) {
            currentPageCursor++; // advance the pointer.
            if (currentPage == 0 || currentPageCursor > pageSize) { // more data need to be fetched
                currentPageCursor = 1; // advance to first position
                currentPage++;
                currentPageData = transformData(dataFetcher.call(search, currentPage - 1, pageSize)); //zero based index
            }
            return currentPageData.get(currentPageCursor - 1); // zero based index.
        } else {
            getter = indexBasedGetter;
        }

        return null;
    }

    private int startIndexOfCurrentPage() {
        int pos = ((currentPage - 1) * pageSize) + 1;
        return pos;
    }

    private int lastIndexOfCurrentPage() {
        int pos = (currentPage * pageSize);
        return pos;
    }

    @SuppressWarnings("unchecked")
    private List<T> transformData(List<? extends Object> list) {
        List<T> data = null;
        if (transformer == null) {
            data = (List<T>) list;
        } else if (list != null) {
            data = new ArrayList<T>(0);
            for (Object obj : list) {
                data.add(transformer.call(obj));
            }
        }

        return data;

    }

    public int pageCount() {
        return (int) Math.ceil(((double) size) / ((double) pageSize));
    }


    /**
     * @param pageNumber = 1 for first page. (Not zero based index)
     */
    public List<T> retrievePage(int pageNumber) {
        if (pageNumber > 0 && pageNumber <= pageCount()) {
            return transformData(dataFetcher.call(search, pageNumber - 1, pageSize)); //zero based index
        } else {
            throw new RuntimeException("invalid page number");
        }
    }

    public Iterator<T> iterator() {
        currentPage = 0;
        currentPageCursor = 0;
        getter = iteratorBasedGetter;
        return this;
    }

    public void remove() {

    }

    public void setTransformer(FunctionDelegate.Arg1<T, Object> transformer) {
        this.transformer = transformer;
    }

    public FunctionDelegate.Arg1<T, Object> getTransformer() {
        return transformer;
    }

}
