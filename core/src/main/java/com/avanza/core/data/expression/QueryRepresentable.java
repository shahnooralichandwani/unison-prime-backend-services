package com.avanza.core.data.expression;

public interface QueryRepresentable {
    boolean represent(QueryRepresentator representator, String opener, String closer);
}
