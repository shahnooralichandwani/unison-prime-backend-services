package com.avanza.core.data.expression;

import com.avanza.core.FunctionDelegate;
import com.avanza.core.data.DataFilter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.util.DateUtil;
import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;
import com.google.common.base.Defaults;
import org.hibernate.Criteria;

import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.*;

public class Search implements QueryRepresentable, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -4740006515853743274L;

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface SearchExcluded {
    }

    private static final int NONE = -1;

    private List<Column> columnList;
    private List<Order> orderList;
    private CriterionTree criterionTree;
    private HashMap<String, From> fromMap;
    private boolean objectBased;
    private QueryRepresentator representator;

    private int batchSize;
    private int batchNum;
    private long recordSize;
    private long resultCount;

    private boolean canRepresent;

    private Criteria hqlCriteria;

    public Search(Criteria hqlCriteria) {
        this.hqlCriteria = hqlCriteria;
    }

    public Search() {

        this.init(Search.NONE, Search.NONE);
    }

    public Search(Class<?> clazz) {
        this(clazz, null);
    }

    public Search(Class<?> clazz, String alias) {

        this.init(Search.NONE, Search.NONE);
        this.addFrom(clazz, alias);
    }

    public Search(String firtFrom, String alias) {

        this.init(Search.NONE, Search.NONE);
        this.addFrom(firtFrom, alias);
    }

    public Search(int batchNum, int batchSize) {

        this.init(batchNum, batchSize);
    }

    private void init(int batchNum, int batchSize) {

        this.columnList = new ArrayList<Column>();
        this.orderList = new ArrayList<Order>(4);
        this.fromMap = new HashMap<String, From>();
        this.batchNum = batchNum;
        this.batchSize = batchSize;
        this.criterionTree = new CriterionTree();
        this.objectBased = false;
    }

    /**
     * currently we have same functionality implemented earlier. which cannot be
     * depricated before testing new implementation. so this flag will decide
     * wheither to use new implementation or not.
     *
     * @return
     */
    public boolean canRepresent() {
        return canRepresent;
    }

    public void setCanRepresent(boolean canRepresent) {
        this.canRepresent = canRepresent;
    }

    public QueryRepresentator getRepresentator() {
        return representator;
    }

    public void setRepresentator(QueryRepresentator representator) {
        this.representator = representator;
    }

    public int getBatchNum() {

        return this.batchNum;
    }

    public void setBatchNum(int batchNum) {

        this.batchNum = batchNum;
    }

    public int getBatchSize() {

        return batchSize;
    }

    public void setBatchSize(int batchSize) {

        this.batchSize = batchSize;
    }

    public void setColumnClause(String columnClause) {

        List<Column> columns = Column.ParseClause(columnClause);
        this.columnList.clear();
        this.columnList.addAll(columns);
    }

    public List<Column> getColumnList() {

        return this.columnList;
    }

    public From findFromForColumn(Column column) {
        return fromMap.get(column.getAlias());
    }

    public void addColumn(Column item) {

        Guard.checkNull(item, "Search.addColumn(Column)");
        this.columnList.add(item);
    }

    public void addColumn(String name) {

        this.columnList.add(new Column(name));
    }

    public void addColumn(String name, String alias) {

        this.columnList.add(new Column(name, alias));
    }

    public void setOrderClause(String orderClause) {

        List<Order> orders = Order.ParseClause(orderClause);
        this.orderList.clear();
        this.orderList.addAll(orders);
    }

    public List<Order> getOrderList() {
        return this.orderList;
    }

    public void addOrder(Order item) {

        Guard.checkNull(item, "Search.addOrder(Order)");
        this.orderList.add(item);
    }

    public void addOrder(String name) {

        this.orderList.add(new Order(name));
    }

    public void addOrder(String fullName, OrderType type) {

        this.orderList.add(new Order(fullName, type));
    }

    public void addOrder(String alias, String name, OrderType type) {

        this.orderList.add(new Order(alias, name, type));
    }

    public void setFrom(From... froms) {
        fromMap.clear();
        for (From from : froms) {
            String key = (StringHelper.isNotEmpty(from.getAlias()))
                    ? from.getAlias()
                    : from.getName();
            fromMap.put(key, from);
        }
    }

    public void setFromClause(String fromClause, boolean objectBased) {

        List<From> froms = From.ParseClause(fromClause);

        if (objectBased) {

            if (froms.size() != 1)
                throw new ExpressionException(
                        "Object based search didn't support multiple classes in FROM list");

            this.objectBased = true;
        }

        this.fromMap.clear();
        setFrom((From[]) froms.toArray());
    }

    public List<From> getFromList() {
        return new ArrayList<From>(this.fromMap.values());
    }

    public From getFirstFrom() {

        if (this.fromMap.size() == 0)
            throw new ExpressionException("Can't access empty FROM list search");
        return this.fromMap.values().iterator().next();
    }

    public void addFrom(Class<?> clazz) {

        this.addFrom(clazz, null);
    }

    public void addFrom(Class<?> clazz, String alias) {

        if (this.objectBased) {
            this.fromMap.clear();
        } else if (this.fromMap.size() > 0)
            throw new ExpressionException(
                    "Object based search didn't support multiple classes in FROM list");
        else
            this.objectBased = true;

        if (StringHelper.isEmpty(alias))
            setFrom(new From(clazz));
        else
            setFrom(new From(clazz, alias));
    }

    public void addFrom(String name) {

        this.addFrom(name, null);
    }

    public void addFrom(String name, String alias) {

        if (this.objectBased)
            throw new ExpressionException(
                    "Object based search didn't support multiple classes in FROM list");

        if (StringHelper.isEmpty(alias))
            setFrom(new From(name));
        else
            setFrom(new From(name, alias));
    }

    public void setRootCriterion(Criterion item) {

        this.criterionTree.setRoot(item);
    }

    public void addCriterion(Criterion item, boolean isOr) {

        if (isOr)
            this.criterionTree.addCriterionOr(item);
        else
            this.criterionTree.addCriterion(item);
    }

    public void addCriterion(Criterion item) {

        this.criterionTree.addCriterion(item);
    }

    public void addCriterionOr(Criterion item) {

        this.criterionTree.addCriterionOr(item);
    }

    public void clear() {

        this.fromMap.clear();
        this.columnList.clear();
        this.orderList.clear();
        this.criterionTree.clear();
        this.batchNum = Search.NONE;
        this.batchSize = Search.NONE;
        this.objectBased = false;
    }

    public Criterion getRootCriterion() {

        return this.criterionTree.getRoot();
    }

    public void setWhereClause(String whereClause) {

        this.criterionTree.setWhereClause(whereClause);
    }

    public Map<String, Object> getCriteriaMap() {
        Criterion criterion = criterionTree.getRoot();
        Map<String, Object> map = new HashMap<String, Object>();
        getCriteriaMapRecursive(map, criterion);
        return map;
    }

    private void getCriteriaMapRecursive(Map<String, Object> map, Criterion criterion) {
        if (criterion == null) {
            return;
        } else if (criterion instanceof ComplexCriterion) {
            getCriteriaMapRecursive(map, ((ComplexCriterion) criterion).lhs);
            getCriteriaMapRecursive(map, ((ComplexCriterion) criterion).rhs);
        } else if (criterion instanceof SimpleCriterion<?> && ((SimpleCriterion<?>) criterion).getField() instanceof Column) {
            try {
                map.put(((Column) ((SimpleCriterion<?>) criterion).getField()).getName()
                        , ((SimpleCriterion<?>) criterion).getValue());
            } catch (RuntimeException ex) {
                ex.printStackTrace();
                throw ex;
            }
        }

    }

    public static Search parseSearch(String searchStmt, boolean objectBased) {

        Guard.checkNullOrEmpty(searchStmt, "Search.parseSearch(String)");
        String tempSearch = searchStmt.toUpperCase();
        Search retVal = new Search();

        int idxSelect = tempSearch.indexOf(Column.SELECT_CLAUSE);
        int idxFrom = tempSearch.indexOf(From.FROM_CLAUSE);
        int idxWhere = tempSearch.indexOf(Criterion.WHERE_CLAUSE);
        int idxOrder = tempSearch.indexOf(Order.ORDER_CLAUSE);

        if (idxFrom == -1)
            throw new ExpressionException("Invalid Expression.");
        else {

            if (idxWhere == -1)
                retVal.setFromClause(searchStmt.substring(idxFrom), objectBased);
            else
                retVal.setFromClause(searchStmt.substring(idxFrom, idxWhere), objectBased);
        }

        // parse select clause only
        if (idxSelect != -1)
            retVal.setColumnClause(searchStmt.substring(idxSelect, idxFrom));

        if (idxOrder != -1)
            retVal.setOrderClause(searchStmt.substring(idxOrder));

        if (idxWhere != -1) {
            if (idxOrder == -1)
                retVal.setWhereClause(searchStmt.substring(idxWhere));
            else
                retVal.setWhereClause(searchStmt.substring(idxWhere, idxOrder));
        }

        return retVal;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder(1024);
        represent(new HqlQueryRepresentator(builder), "", "");
        return builder.toString();
    }

    public boolean isObjectBased() {
        return objectBased;
    }

    public boolean isOrderBy() {
        return this.orderList.size() > 0;
    }

    public boolean isSelectClause() {
        return this.columnList.size() > 0;
    }

    public boolean hasCriteria() {
        return this.criterionTree.getRoot() != null;
    }

    public long getRecordSize() {
        if (recordSize == 0) {
            Integer tempPageSize = ((this.batchNum + 1) * this.batchSize);
            if (tempPageSize > this.resultCount)
                this.recordSize = this.resultCount - (this.batchNum * this.batchSize);
            else
                return this.batchSize;
        }
        return this.recordSize;
    }

    public void setRecordSize(long recordSize) {
        this.recordSize = recordSize;
    }

    public long getResultCount() {
        return resultCount;
    }

    public void setResultCount(long resultCount) {
        this.resultCount = resultCount;
    }

    public Criteria getHqlCriteria() {
        return hqlCriteria;
    }

    public static Search createFromDataFilter(DataFilter filter) {
        MetaEntity entity = MetaDataRegistry.getMetaEntity(filter.getEntityName());
        Search search = new Search();
        String alias = entity.getId();
        search.addFrom(entity.getSystemName(), alias);
        search.addCriterion(CriterionTree.createFromDataFilter(filter).getRoot());
        search.setCanRepresent(true);
        return search;
    }

    /*public static void main(String[] argc) {

        String str = "test";
        String str1 = "test";
        String str2 = str1;
        String str3 = str1;
        String str4 = str1;

        Number num = 1;
        Number num1 = 1;
        Number num2 = num1;
        Number num3 = num1;
        Number num4 = num1;

        Object obj = new Object();
        Object obj1 = new Object();
        Object obj2 = obj1;
        Object obj3 = obj1;
        Object obj4 = obj1;

        Hashtable<String, String> ht = new Hashtable<String, String>();
        ht.put("name", "abdul wahid");
        ht.put("name", "abdul basit");
        ht.put("name", "abdul wajid");
        ht.put("name", "ammar");
        String name = ht.get("name");

        MetaAttributeImpl attrib = new MetaAttributeImpl();
        attrib.setDisplayName("XXXX");
        attrib.setViewMenu(new MetaViewMenu());
        attrib.getViewMenu().setCreatedBy("User");
        Search userSearch = Search.createSearchByObject(attrib);
        userSearch.toString();
        // campain is optional.
        attrib.setDisplayName(null);
        userSearch.markCriteriaOptionalByObject(attrib);

        String alias = userSearch.getFirstFrom().getAlias();
        String property = (StringHelper.isEmpty(alias)) ? "displayName" : alias
                + ".displayName";
        SimpleCriterion<String> criterion = (SimpleCriterion<String>) userSearch.getCriterionByFieldName(property);
        criterion.setValue("YYYYYY");

    }*/

    public boolean represent(QueryRepresentator representator, String opener, String closer) {
        return representator.representSearch(this, opener, closer);
    }

    /**
     * auto clear is true
     *
     * @return
     */
    public String getRepresentation() {
        return getRepresentation(true);
    }

    public String getRepresentation(QueryRepresentator representator) {
        return getRepresentation(false, representator);
    }

    public String getRepresentation(boolean autoClear) {
        return getRepresentation(autoClear, this.representator);
    }

    private String getRepresentation(boolean autoClear, QueryRepresentator representator) {
        if (representator != null) {
            represent(representator, "", "");
            String str = representator.getRepresentation().toString();
            if (autoClear) {
                representator.clear();
            }
            return str;
        }
        return null;
    }

    public void merge(Search search) {
        merge(search, false);
    }

    public void merge(Search search, boolean isOr) {

        // merge entity (from) list
        From[] fromArray = new From[search.getFromList().size()];
        setFrom(search.getFromList().toArray(fromArray));

        // merge criteria
        addCriterion(search.getRootCriterion(), isOr);

    }

    /**
     * It is used to add columns by from a POJO. Only dirty properties will be
     * considered.
     *
     * @param <T>
     * @param object
     */
    public <T> void addColumnsByObject(T object) {
        addColumnsByObject(object, null);
    }

    public <T> void addColumnsByObject(T object, String parentFrom) {
        processRequiredProperties(object, null, this, parentFrom,
                new FunctionDelegate.Arg4<Void, Search, String, Object, Object>() {
                    // add columns.
                    public Void call(Search search, String property, Object value, Object flagValue) {
                        if (value != null) {
                            search.addColumn(property);
                        }
                        return null;
                    }
                });

    }

    /**
     * It is used to mark some criteria optional by a related POJO. Only dirty
     * properties will be considered.
     *
     * @param <T>
     * @param object
     */
    public <T> void markCriteriaOptionalByObject(T object) {
        markCriteriaOptionalByObject(object, this.getFirstFrom().getAlias());
    }

    public <T> void markCriteriaOptionalByObject(T object, String parentFrom) {
        processRequiredProperties(object, null, this, parentFrom,
                new FunctionDelegate.Arg4<Void, Search, String, Object, Object>() {
                    // mark criteria optional for properties.
                    public Void call(Search search, String property, Object value, Object flagValue) {
                        if (value != null) {
                            Criterion crit = criterionTree.getCriterionByFieldName(property);
                            if (crit != null) {
                                CriterionTree.addCriterion(crit, OperatorType.Or,
                                        SimpleCriterion.getSimpleCriterion(OperatorType.Null,
                                                new Column(property), null));
                            }
                        }
                        return null;
                    }
                });

    }

    /**
     * It is used to create Search object from a POJO.
     *
     * @param <T>
     * @param object
     * @return
     */
    public static <T> Search createSearchByObject(T object) {
        return createSearchByObject(object, null);
    }

    /**
     * It is used to create Search object from a POJO.
     *
     * @param <T>
     * @param object
     * @param inclusionFlagsObject we are only considering dirty properties. E-g Empty strings,
     *                             false values, 0 values will be ignored by the system, unless
     *                             you specifically tell the system by providing this parameter
     *                             with dirty values. <br/>
     *                             It is also used to provide ranges. from -> object and to ->
     *                             inclusionFlagsObject For applying ranges "from" must be
     *                             greater than or equal to "to", otherwise "equal" operator will
     *                             be used.
     * @return
     */

    public static <T> Search createSearchByObject(T object, boolean isOr) {
        return createSearchByObject(object, isOr, null);
    }

    public static <T> Search createSearchByObject(T object, String parentFrom, boolean isOr) {
        return createSearchByObject(object, parentFrom, isOr, null);
    }

    public static <T> Search createSearchByObject(T object, T inclusionFlagsObject) {
        return createSearchByObject(object, null, false, inclusionFlagsObject);
    }

    public static <T> Search createSearchByObject(T object, String parentFrom,
                                                  T inclusionFlagsObject) {
        return createSearchByObject(object, parentFrom, false, inclusionFlagsObject);
    }

    public static <T> Search createSearchByObject(T object, final boolean isOr,
                                                  T inclusionFlagsObject) {
        return createSearchByObject(object, null, false, inclusionFlagsObject);
    }

    public static <T> Search createSearchByObject(T object, String parentFrom, final boolean isOr,
                                                  T inclusionFlagsObject) {
        return processRequiredProperties(object, inclusionFlagsObject, null, parentFrom,
                new FunctionDelegate.Arg4<Void, Search, String, Object, Object>() {

                    // make criteria.
                    public Void call(Search search, String property, Object value, Object flagValue) {
                        Criterion lhs = new SimpleCriterion<Object>(OperatorType.Eq, property,
                                StringHelper.EMPTY, value);

                        // will be used only in case of range.
                        Criterion rhs = new SimpleCriterion<Object>(OperatorType.LtEq, property,
                                StringHelper.EMPTY, flagValue);

                        boolean isRange = false;

                        if (value == null) {
                            lhs.setOperator(OperatorType.Null);
                        } else if (value instanceof CharSequence) {
                            lhs.setOperator(OperatorType.Like);
                        } else if (value instanceof Number
                                && flagValue != null
                                && ((Number) value).doubleValue() < ((Number) flagValue).doubleValue()) {
                            lhs.setOperator(OperatorType.GtEq);
                            isRange = true;
                            double maxVal = 0;
                            double minVal = 0;
                            try {
                                maxVal = value.getClass().getField("MAX_VALUE").getDouble(null);
                                minVal = value.getClass().getField("MIN_VALUE").getDouble(null);
                            } catch (Exception e) {
                            }

                            if (((Number) value).doubleValue() == minVal) {
                                lhs = null;
                            } else if (((Number) flagValue).doubleValue() == maxVal) {
                                rhs = null;
                            }

                        } else if (value instanceof Date && flagValue != null
                                && ((Date) value).before((Date) flagValue)) {
                            lhs.setOperator(OperatorType.GtEq);
                            isRange = true;
                            Date maxVal = DateUtil.getMaxDate();
                            Date minVal = DateUtil.getMinDate();
                            if (value.equals(minVal)) {
                                lhs = null;
                            } else if (flagValue.equals(maxVal)) {
                                rhs = null;
                            }

                        }

                        // add criterion
                        if (lhs != null && rhs != null && isRange) {
                            search.addCriterion(new ComplexCriterion(OperatorType.And, lhs, rhs),
                                    isOr);
                        } else if (rhs != null && isRange) {
                            search.addCriterion(rhs, isOr);
                        } else {
                            search.addCriterion(lhs, isOr);
                        }

                        return null;
                    }
                });
    }

    private static <T> Search processRequiredProperties(T object, T inclusionFlagsObject,
                                                        Search search, String parentProperty,
                                                        FunctionDelegate.Arg4<Void, Search, String, Object, Object> propertyProcessor) {
        Class<? extends Object> clazz = object.getClass();
        if (search == null) {
            search = new Search(clazz);
            search.setCanRepresent(true);
            search.getFirstFrom().setAlias("_" + clazz.getSimpleName());
            if (StringHelper.isEmpty(parentProperty)) {
                parentProperty = search.getFirstFrom().getAlias();
            } else {
                parentProperty += "." + search.getFirstFrom().getAlias();
            }
        }
        for (Method method : clazz.getMethods()) {
            Class<? extends Object> returnType = method.getReturnType();
            String methodName = method.getName();

            // ignore if it is not a getter.
            if (method.getName().indexOf("get") != 0) {
                // only getters will be considered.
                continue;
            }

            // ignore, if getter with parameters
            else if (method.getParameterTypes().length > 0) {
                continue;
            }

            // ignore, if explicitly excluded
            else if (method.getAnnotation(SearchExcluded.class) != null) {
                continue;
            }

            // ignore excluded types
            else if (returnType == null || returnType.equals(Void.TYPE)
                    || Iterable.class.isAssignableFrom(returnType)
                    || Map.class.isAssignableFrom(returnType) || returnType.isArray()) {
                continue;
            }

            // ignore, if setter not found.
            String setter = "set" + methodName.substring(3);
            try {
                clazz.getMethod(setter, returnType);
            } catch (SecurityException e) {
                continue;
            } catch (NoSuchMethodException e) {
                continue;
            }

            // get field and flag values.
            Object value = null;
            Object flagValue = null;
            try {
                value = method.invoke(object, (Object[]) null);
                if (inclusionFlagsObject != null) {
                    flagValue = method.invoke(inclusionFlagsObject, (Object[]) null);
                }
            } catch (Exception e) {
            }

            // make property
            String property = methodName.substring(3, 4).toLowerCase() + methodName.substring(4);
            if (StringHelper.isNotEmpty(parentProperty)) {
                property = parentProperty + "." + property;
            }

            // recursion for nesting properties.
            if (value != null && !(value instanceof Number) && !(value instanceof CharSequence)
                    && !(value instanceof Boolean) && !(value instanceof Date)
                    && !returnType.isPrimitive()) {
                processRequiredProperties(value, flagValue, search, property, propertyProcessor);
                continue;
            }

            // ignore if value and flagValue have default value.
            if ((value == null || value.equals(Defaults.defaultValue(returnType)))
                    && (flagValue == null || flagValue.equals(Defaults.defaultValue(returnType)))) {
                continue;
            } else if ((value != null && flagValue != null) && flagValue.equals(value)) {
                // ignore same value
                continue;
            } else if (value instanceof String && StringHelper.isEmpty((String) value)) {
                // ignore empty string
                continue;
            } else if (value instanceof Number && ((Number) value).floatValue() == 0 && (flagValue == null || !value.equals(flagValue))) {
                // ignore number with zero
                continue;
            } else if (value instanceof Boolean && ((Boolean) value).booleanValue() == false && (flagValue == null || !value.equals(flagValue))) {
                // ignore boolean with false
                continue;
            }

            propertyProcessor.call(search, property, value, flagValue);

        }
        return search;
    }

    public Criterion getCriterionByFieldName(String fieldName) {
        return criterionTree.getCriterionByFieldName(fieldName);
    }
}
