package com.avanza.core.data.sequence;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.DbObject;
import com.avanza.core.data.SequenceDataBroker;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

//Maps to the Meta_counter table do not use hibernate for it. 
public class SequenceInfo extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 4356357271089363451L;
    private static Logger logger = Logger.getLogger(SequenceInfo.class);

    private String counterName;
    private Long startValue;
    private Long maxValue;
    private String prefixValue;
    private short incrementedBy;
    private boolean cyclic;
    private String implClass;
    private String formatExpr;
    private ResetType resetType;

    private Map<String, SequenceValue> sequenceValues;

    private Class<?> clazz;
    private SequenceGenerator sequenceGenerator;

    public Map<String, SequenceValue> getSequenceValues() {
        return sequenceValues;
    }

    public synchronized SequenceValue getSequenceValue(String uniqueId) {
        // get the sequence value from the DB if not exists then create it based on the reset Type.
        String counterUniqueId = generateCounterUniqueId(uniqueId);
        SequenceValue seqValue = sequenceValues.get(counterUniqueId);

        if (seqValue == null) {
            SequenceDataBroker broker = (SequenceDataBroker) DataRepository.getBroker("SequenceDataBroker");
            seqValue = broker.findSequenceValue(this.counterName, counterUniqueId, this);
            seqValue.setSequenceInfo(this);
            sequenceValues.put(seqValue.getUniqueId(), seqValue);
        }

        return seqValue;
    }

    private String generateCounterUniqueId(String uniqueId) {

        String retVal = uniqueId;

        if (ResetType.Sequence.equals(resetType)) {
            return retVal;
        } else {
            Date currentDate = new Date();
            if (ResetType.Daily.equals(resetType)) {
                String formattedDate = new SimpleDateFormat(".yyyy-MM-dd").format(currentDate);
                retVal = retVal + formattedDate;
            } else if (ResetType.Monthly.equals(resetType)) {
                String formattedDate = new SimpleDateFormat(".yyyy-MM").format(currentDate);
                retVal = retVal + formattedDate;
            } else if (ResetType.Yearly.equals(resetType)) {
                String formattedDate = new SimpleDateFormat(".yyyy").format(currentDate);
                retVal = retVal + formattedDate;
            }
        }
        return retVal;
    }

    public void setSequenceValues(Map<String, SequenceValue> sequenceValues) {
        this.sequenceValues = sequenceValues;
    }

    public boolean isCyclic() {
        return cyclic;
    }

    public void setCyclic(boolean cyclic) {
        this.cyclic = cyclic;
    }

    public String getCounterName() {
        return counterName;
    }

    public void setCounterName(String counterName) {
        this.counterName = counterName;
    }

    public String getImplClass() {
        return implClass;
    }

    public void setImplClass(String implClass) {
        this.implClass = implClass;
    }

    public Long getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Long maxValue) {
        this.maxValue = maxValue;
    }

    public String getPrefixValue() {
        return prefixValue == null ? "" : this.prefixValue;
    }

    public void setPrefixValue(String prefixValue) {
        this.prefixValue = prefixValue;
    }

    public void setResetType(String resetType) {
        this.resetType = ResetType.fromString(resetType);
    }

    public short getIncrementedBy() {
        return incrementedBy;
    }


    public void setIncrementedBy(short incrementedBy) {
        this.incrementedBy = incrementedBy;
    }

    public String getFormatExpr() {
        return formatExpr;
    }

    public void setFormatExpr(String formatExpr) {
        this.formatExpr = formatExpr;
    }

    public Class<?> getClazz() {

        if (clazz == null) {
            try {
                if (StringHelper.isEmpty(this.implClass)) {
                    this.clazz = SequenceGeneratorImpl.class;
                } else {
                    this.clazz = Class.forName(this.implClass);
                }
            } catch (ClassNotFoundException e) {
                logger.LogException(String.format("Unable to Load the class %1$s in sequenceInfo.", this.implClass), e);
            }
        }
        return clazz;
    }

    public SequenceGenerator getSequenceGenerator() {

        if (this.sequenceGenerator == null) {
            try {
                this.sequenceGenerator = (SequenceGenerator) this.getClazz().newInstance();
                this.sequenceGenerator.load(null, this);
            } catch (InstantiationException e) {
                logger.LogException(String.format("Unable to create instance of the class %1$s in sequenceInfo.", this.implClass), e);
            } catch (IllegalAccessException e) {
                logger.LogException(String.format("Unable to create instance of the class %1$s in sequenceInfo.", this.implClass), e);
            }
        }
        return sequenceGenerator;
    }

    public ResetType getResetType() {
        return resetType;
    }

    public Long getStartValue() {
        return startValue;
    }

    public void setStartValue(Long startValue) {
        this.startValue = startValue;
    }
}
