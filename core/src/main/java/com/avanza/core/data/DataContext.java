package com.avanza.core.data;

import java.util.Hashtable;
import java.util.Map;

public class DataContext {

    private static Map<String, Object> threadSession = new Hashtable<String, Object>(0);

    private DataContext() {

    }

    public static Object get(String threadId) {
        return threadSession.get(threadId);
    }

    public static void add(String threadId, Object item) {
        threadSession.put(threadId, item);
    }

    public static void releaseContext() {

        // check thread locale for any Session object
        // and call release method on it
    }

    public static void release(String key) {
        threadSession.remove(key);

    }

}