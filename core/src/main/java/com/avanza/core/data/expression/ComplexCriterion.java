package com.avanza.core.data.expression;

import com.avanza.core.util.Guard;

public class ComplexCriterion extends Criterion {

    protected Criterion lhs;
    protected Criterion rhs;

    public ComplexCriterion(OperatorType operator, Criterion lhs) {

        super(operator);
        this.setLhs(lhs);
    }

    public ComplexCriterion(OperatorType operator, Criterion lhs, Criterion rhs) {

        super(operator);
        this.setLhs(lhs);
        this.setRhs(rhs);
    }

    /**
     * This method returns true for the case of lhs=SimpleCriteria and  rhs = Complex Criteria
     *
     * @return
     */
    public boolean isLinkedForm() {
        if (lhs != null && rhs != null && (lhs instanceof SimpleCriterion<?>) && (rhs instanceof ComplexCriterion || rhs instanceof SimpleCriterion))
            return true;
        return false;
    }

    /**
     * This method returns true for the case of lhs=SimpleCriteria and  rhs = Object Criteria
     *
     * @return
     */
    public boolean isJoinForm() {
        if (lhs != null && rhs != null && (lhs instanceof SimpleCriterion<?>) && rhs instanceof ObjectCriterion)
            return true;
        return false;
    }

    public Criterion getLhs() {

        return this.lhs;
    }

    public void setLhs(Criterion lhs) {

        Guard.checkNull(lhs, "ComplexCriterion.setLhsCriterion");
        this.lhs = lhs;
        lhs.setParent(this);
    }

    public Criterion getRhs() {

        return this.rhs;
    }

    public void setRhs(Criterion rhs) {
        Guard.checkNull(lhs, "ComplexCriterion.setRhsCriterion");
        this.rhs = rhs;
        rhs.setParent(this);
    }

    public StringBuilder toString(StringBuilder builder) {


        QueryRepresentator representator = new HqlQueryRepresentator(builder);
        represent(representator, "", "");
        return builder;
    }

    public String toString() {

        StringBuilder builder = new StringBuilder(200);
        return this.toString(builder).toString();
    }

    // Shahbaz: To handle Complex criteria/Simple Criteria with ListCriteria
    // Needed in Campaign & Outbound.

    public boolean isListForm() {

        if (lhs != null && rhs != null && (lhs instanceof ListCriterion<?> || lhs instanceof SimpleCriterion<?>) && (rhs instanceof ComplexCriterion || rhs instanceof SimpleCriterion<?> || rhs instanceof ListCriterion<?>))
            return true;

        return false;

    }

    public boolean represent(QueryRepresentator representator, String opener, String closer) {
        return representator.representComplexCriterion(this, opener, closer);
    }
}
