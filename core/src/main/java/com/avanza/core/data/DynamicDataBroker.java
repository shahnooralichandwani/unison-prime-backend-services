package com.avanza.core.data;

import com.avanza.core.meta.MetaException;


/**
 * @author Haris Mirza
 * <p>
 * This interface is meant to provide generalization for Brokers which deals with Meta Entities
 */

public interface DynamicDataBroker extends DataBroker {


    public void loadMappings() throws MetaException;

    public void reloadMappings() throws MetaException;
}
