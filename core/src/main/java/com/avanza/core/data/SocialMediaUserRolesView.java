package com.avanza.core.data;

public class SocialMediaUserRolesView {

    private String smUserRoleID;
    private String roleId;
    private String pageId;
    private String loginId;
    private String orgPrmRoleName;
    private String orgSecRoleName;
    private String orgUnitName;
    private String orgFullUnitName;
    private String smPageName;
    private String pageType;


    public String getSmUserRoleID() {
        return smUserRoleID;
    }

    public void setSmUserRoleID(String smUserRoleID) {
        this.smUserRoleID = smUserRoleID;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getOrgPrmRoleName() {
        return orgPrmRoleName;
    }

    public void setOrgPrmRoleName(String orgPrmRoleName) {
        this.orgPrmRoleName = orgPrmRoleName;
    }

    public String getOrgSecRoleName() {
        return orgSecRoleName;
    }

    public void setOrgSecRoleName(String orgSecRoleName) {
        this.orgSecRoleName = orgSecRoleName;
    }

    public String getOrgUnitName() {
        return orgUnitName;
    }

    public void setOrgUnitName(String orgUnitName) {
        this.orgUnitName = orgUnitName;
    }

    public String getOrgFullUnitName() {
        return orgFullUnitName;
    }

    public void setOrgFullUnitName(String orgFullUnitName) {
        this.orgFullUnitName = orgFullUnitName;
    }

    public String getSmPageName() {
        return smPageName;
    }

    public void setSmPageName(String smPageName) {
        this.smPageName = smPageName;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }


}
