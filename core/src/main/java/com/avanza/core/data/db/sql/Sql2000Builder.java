package com.avanza.core.data.db.sql;

import com.avanza.core.data.expression.From;
import com.avanza.core.data.expression.Order;
import com.avanza.core.data.expression.Search;
import com.avanza.core.sdo.DataException;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 * This class generates the native sql based on the sql server 2000 specific style.
 *
 * @author kraza
 */
public class Sql2000Builder extends SqlBuilder {

    @Override
    public String prepareInsert(Object obj) {
        throw new DataException("Not Implemented.");
    }

    @Override
    public String preparePaginatedSelect(String sqlSearchString, int batchNum, int batchSize) {

        logger.logDebug("preparing the paginated search sql query by searchString.");

        Search sqlSearch = Search.parseSearch(sqlSearchString, false);
        sqlSearch.setBatchNum(batchNum);
        sqlSearch.setBatchNum(batchSize);

        return this.createPaginatedSearch(sqlSearch);
    }

    @Override
    public String preparePaginatedSelect(Search search) {

        logger.logDebug("preparing the paginated search sql query.");

        Search sqlSearch = MetaQueryTranslator.convertToSQLSearch(search);

        return createPaginatedSearch(sqlSearch);
    }

    public String createPaginatedSearch(Search sqlSearch) {

        StringBuilder sql = new StringBuilder();

        if (sqlSearch.getColumnList().size() == 0) {
            sql.append(SqlKeywords.Select).append(sqlSearch.getFirstFrom().getAlias()).append(SqlKeywords.FieldSeparator).append(SqlKeywords.Star);
        }
        sql.append(sqlSearch.toString());

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(SqlKeywords.Select).append(SqlKeywords.Star).append(SqlKeywords.From)
                .append(SqlKeywords.OpenBracket)
                .append(SqlKeywords.Select).append(SqlKeywords.Top).append(sqlSearch.getRecordSize()).append(SqlKeywords.Star).append(SqlKeywords.From)
                .append(SqlKeywords.OpenBracket);

        queryBuilder.append(SqlKeywords.Select);
        if (sqlSearch.getRecordSize() == sqlSearch.getBatchSize())
            queryBuilder.append(SqlKeywords.Top).append((sqlSearch.getBatchNum() + 1) * sqlSearch.getBatchSize());
        else
            queryBuilder.append(SqlKeywords.Top).append(sqlSearch.getResultCount());
        queryBuilder.append(SqlKeywords.Space).append(sql.substring(SqlKeywords.Select.value.length()));

        queryBuilder.append(SqlKeywords.CloseBracket).append(SqlKeywords.TempA);

        createOrderBySql(sqlSearch, queryBuilder, true, false);

        queryBuilder.append(SqlKeywords.CloseBracket).append(SqlKeywords.TempB);
        createOrderBySql(sqlSearch, queryBuilder, false, false);

        return queryBuilder.toString();
    }

    private void createOrderBySql(Search sqlSearch, StringBuilder queryBuilder, boolean reverseOrder, boolean useAlias) {
        List<Order> orderList = sqlSearch.getOrderList();
        if (orderList.size() > 0)
            queryBuilder.append(SqlKeywords.OrderBy);

        for (Order order : orderList) {
            order.toString(queryBuilder, reverseOrder, useAlias);
        }
    }

    @Override
    public PreparedStatement prepareSelectCmd() {
        throw new DataException("Not Implemented.");
    }

    @Override
    public String prepareSelect(Search search) {

        logger.logDebug("preparing the search sql query.");
        Search sqlSearch = MetaQueryTranslator.convertToSQLSearch(search);

        return sqlSearch.toString();
    }

    @Override
    public String preparePageCountQuery(Search search) {

        logger.logDebug("preparing the page count sql query.");
        Search sqlSearch = MetaQueryTranslator.convertToSQLSearch(search);
        sqlSearch.getOrderList().clear();

        StringBuilder sql = new StringBuilder();

        if (sqlSearch.getColumnList().size() == 0) {
            From from = sqlSearch.getFirstFrom();
            String alias = from.getAlias();
            alias = (alias == null) ? from.getName() : alias;
            sql.append(SqlKeywords.Select).append(alias).append(SqlKeywords.FieldSeparator).append(SqlKeywords.Star);
        }
        sql.append(sqlSearch.toString());


        StringBuilder pageCount = new StringBuilder();
        pageCount.append(SqlKeywords.Select).append(SqlKeywords.Count1).append("COUNT1").append(SqlKeywords.From).append(SqlKeywords.OpenBracket);
        pageCount.append(sql.toString()).append(SqlKeywords.CloseBracket).append(SqlKeywords.TempA);

        return pageCount.toString();
    }

    @Override
    public PreparedStatement prepareInsertCmd(String cmd) {
        throw new DataException("Not Implemented.");
    }

    //prepared statement
    @Override
    public int executeInsertCmd(PreparedStatement pstmt) {
        throw new DataException("Not Implemented.");
    }

    @Override
    public ResultSet executeProceduteCallCmd(CallableStatement callStmt) {
        throw new DataException("Not Implemented.");
    }

    @Override
    public ResultSet executeSelectCmd(PreparedStatement pstmt) {
        throw new DataException("Not Implemented.");
    }
}
