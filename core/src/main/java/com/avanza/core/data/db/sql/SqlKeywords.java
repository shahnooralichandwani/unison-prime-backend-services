package com.avanza.core.data.db.sql;

/**
 * Standard Sql keywords are defined here.
 *
 * @author kraza
 */
public enum SqlKeywords {
    Select("SELECT "), From(" FROM "), Where(" WHERE "), OrderBy(" ORDER BY "), Star("*"), Comma(", "), As(" AS "), Asc(" ASC"), Desc(" DESC"), Count1(" Count(1) "), ROWNUM(" ROWNUM "),
    Top("TOP "), OpenBracket("("), CloseBracket(")"), Space(" "), FieldSeparator("."), Eq("="), Gt(">"), GtEq(">="), Lt("<"),
    LtEq("<="), Nqe("!="), Null(" IS NULL"), NotNull(" IS NOT NULL"), Like(" LIKE "), In(" IN"), NotIn(" NOT IN"), Between(" BETWEEN "),
    And(" AND "), Or(" OR "), TempA(" TEMPA "), TempB(" TEMPB "), ROW_NUM(" ROW_NUM "),
    Quote("'"), First(" FIRST "), Skip(" SKIP ");

    String value;

    SqlKeywords(final String val) {
        this.value = val;
    }

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return this.value;
    }

}
