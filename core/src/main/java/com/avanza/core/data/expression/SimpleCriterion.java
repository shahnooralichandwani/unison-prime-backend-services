package com.avanza.core.data.expression;

import com.avanza.core.util.EscapeUtil;

import java.util.Arrays;

public class SimpleCriterion<T extends Object> extends Criterion implements QueryRepresentable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    protected T value;
    protected QueryRepresentable field;

    public SimpleCriterion(OperatorType operator, String fieldName, String alias, T value) {
        this(operator, new Column(fieldName, alias, ""), value);
    }

    public SimpleCriterion(OperatorType operator, QueryRepresentable field, T value) {
        super(operator);
        this.field = field;
        this.setValue(value);
    }

    /**
     * use getField instead.
     *
     * @return
     */
    @Deprecated
    public String getFieldName() {
        return ((Column) field).getName();
    }

    public QueryRepresentable getField() {
        return this.field;
    }

    // Full name of the Field is returned
    public String getCompleteFieldName() {
        return field.toString();
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(T value) {
        if (value instanceof String)
            value = (T) EscapeUtil.getEscapeValue(value);
        this.value = value;
    }

    /**
     * use getField instead.
     *
     * @return
     */
    @Deprecated
    public String getAlias() {
        return ((Column) field).getAlias();
    }

    /**
     * use getField instead.
     *
     * @return
     */
    @Deprecated
    public boolean hasAlias() {
        return ((((Column) field).getAlias() != null) && (((Column) field).getAlias().length() > 0));
    }

    public boolean represent(QueryRepresentator representator, String opener, String closer) {
        return representator.representSimpleCriterion(this);
    }

    public StringBuilder toString(StringBuilder builder) {

        QueryRepresentator representator = new HqlQueryRepresentator(builder);
        represent(representator, "", "");
        return builder;
    }

    public String toString() {

        StringBuilder builder = new StringBuilder(200);
        return this.toString(builder).toString();
    }

    public static SimpleCriterion getSimpleCriterion(String operator, String attributeName,
                                                     String alias, Object attributeValue) {
        return getSimpleCriterion(OperatorType.parseToken(operator), new Column(attributeName,
                alias, ""), attributeValue);
    }

    public static SimpleCriterion getSimpleCriterion(OperatorType operatorType,
                                                     String attributeName, String alias, Object attributeValue) {
        return getSimpleCriterion(operatorType, new Column(attributeName, alias, ""),
                attributeValue);
    }

    public static SimpleCriterion getSimpleCriterion(String operator, QueryRepresentable field,
                                                     Object attributeValue) {
        return getSimpleCriterion(OperatorType.parseToken(operator), field, attributeValue);
    }

    public static SimpleCriterion getSimpleCriterion(OperatorType operatorType,
                                                     QueryRepresentable field, Object attributeValue) {
        if (operatorType.equals(OperatorType.In) && attributeValue instanceof CharSequence) {
            attributeValue = Arrays.asList((String[]) ((String) attributeValue).split(","));
        }
        return new SimpleCriterion(operatorType, field, attributeValue);
    }
}
