package com.avanza.core.data;

import com.avanza.core.campaign.CampaignCatalog;
import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.configuration.ConfigSection;

import java.io.Serializable;
import java.util.List;

public class CustomerDataBroker implements DataBroker {
    private String defaultBrokerKey;

    private DataBroker getCustomerBroker() {
        return DataRepository.getBroker(defaultBrokerKey);
    }

    private DataBroker getNonCustomerBroker() {
        MetaEntity prospectEntity = MetaDataRegistry.getMetaEntity(CampaignCatalog.getInstance().getNonCustomerEnitityId());
        return DataRepository.getBroker(prospectEntity.getSystemName());
    }

    public <T> void add(T item) {
        getCustomerBroker().add(item);
    }

    public <T> void delete(T item) {
        getCustomerBroker().delete(item);
    }

    public int deleteBySearch(Search search) {
        return getCustomerBroker().deleteBySearch(search);
    }

    public void dispose() {
        getCustomerBroker().dispose();
    }

    public <T> List<T> find(String query) {
        return getCustomerBroker().find(query);
    }

    public <T> List<T> find(Search query) {
        return getCustomerBroker().find(query);
    }

    public <T> List<T> findAll(Class<T> item) {
        return getCustomerBroker().findAll(item);
    }

    public <T, ID extends Serializable> T findById(Class<T> t, ID id) {
        return getCustomerBroker().findById(t, id);
    }

    public <ID, T extends DataObject> T findById(String entityId, ID id) throws DataSourceException {
        // first try to get for main customer source
        MetaEntity prospectEntity = MetaDataRegistry.getMetaEntity(CampaignCatalog.getInstance().getCustomerEnitityId());
        DataBroker customerBroker = getCustomerBroker();
        DataObject prospect = customerBroker.findById(prospectEntity.getSystemName(), id);

        if (prospect == null) {
            // now try to get prospect from non customer list.
            prospectEntity = MetaDataRegistry.getMetaEntity(CampaignCatalog.getInstance().getNonCustomerEnitityId());
            customerBroker = getNonCustomerBroker();
            prospect = customerBroker.findById(prospectEntity.getSystemName(), id);
        }

        return (T) prospect;
    }

    public <T> List<T> findItemsByRange(Search query, int pageNo, int pageSize, String orderBy,
                                        boolean isAsc, int[] returnTotalRowCount) {
        return getCustomerBroker().findItemsByRange(query, pageNo, pageSize, orderBy, isAsc, returnTotalRowCount);
    }

    public int getCriteriaCount(Search criteria) {
        return getCustomerBroker().getCriteriaCount(criteria);
    }

    public String getDataSource() {
        return getCustomerBroker().getDataSource();
    }

    public ProviderType getProviderType() {
        return getCustomerBroker().getProviderType();
    }

    public <T> T getSession() {
        return getCustomerBroker().getSession();
    }

    public void load(ConfigSection configSection) {
        defaultBrokerKey = configSection.getTextValue("defaultBroker");
    }

    public <T> void persist(T item) {
        getCustomerBroker().persist(item);
    }

    public <T> void persistAll(List<T> items) {
        getCustomerBroker().persistAll(items);
    }

    public void synchronize() {
        getCustomerBroker().synchronize();
    }

    public <T> void update(T item) {
        getCustomerBroker().update(item);
    }

    public String getDefaultBrokerKey() {
        return defaultBrokerKey;
    }

}
