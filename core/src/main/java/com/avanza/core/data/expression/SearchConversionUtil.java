package com.avanza.core.data.expression;

import com.avanza.core.util.StringHelper;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * This class is used for the conversionof the Search Object into their
 * respective Search objects to be used by the approprate dataBroker
 *
 * @author kraza
 */
public class SearchConversionUtil {

    /**
     * This class is used as a wrapper class to the actual Criteria class so
     * that we keep the reference to that class safe.
     *
     * @author kraza
     */
    private static class HQLCriteriaWrapper {

        public Criteria hqlCriteria;

        public HQLCriteriaWrapper(Criteria hqlcriteria) {
            hqlCriteria = hqlcriteria;
        }
    }

    /**
     * This method takes the search object as an input and generates its
     * respective HQL Query object.
     *
     * @param search
     * @return HQL Query object
     */
    public static Criteria getHQLSearchObject(Search search, Session session) {

        // Creating the Criterion Object i.e. preparing the HQL Query from
        // Clause.
        Criteria hqlCriteria = applyFromForHQL(search, session);

        // We done adding the resrictions now we will add other sql clauses.
        applyOrderByForHQL(search, hqlCriteria);

        return hqlCriteria;
    }


    /**
     * This method apply the From clause for HQL.
     *
     * @param search
     * @param session
     * @return
     */
    private static Criteria applyFromForHQL(Search search, Session session) {
        Criteria hqlCriteria = null;
        HQLCriteriaWrapper hql = null;
        List<From> fromList = search.getFromList();

        // Only one iteration because the size of the Map is always one most of
        // the time.
        for (From from : fromList) {

            // Creating the Criteria Object for HQL
            if (from.hasType())
                hqlCriteria = (hqlCriteria == null)
                        ? session.createCriteria(from.getType())
                        : hqlCriteria.createCriteria(from.getType().getSimpleName());
            else if (from.hasName()) {
                if (from.hasAlias())
                    hqlCriteria = (hqlCriteria == null) ? session.createCriteria(
                            from.getName(), from.getAlias()) : hqlCriteria.createCriteria(
                            from.getName(), from.getAlias());
                else
                    hqlCriteria = (hqlCriteria == null)
                            ? session.createCriteria(from.getName())
                            : hqlCriteria.createCriteria(from.getName());
            } else
                throw new ExpressionException("Invalid From Object in Expression.");

            // Next step is to add the restrictions for that criteria.
            Criterion criteriaRoot = search.getRootCriterion();

            // Now applying the HQL restrictions to the Criteria object.
            hql = new HQLCriteriaWrapper(hqlCriteria);
            applyHQLRestrictions(criteriaRoot, hql);
        }
        return hql.hqlCriteria;
    }

    /**
     * This method adds the order by clause to the HQL Criteria.
     *
     * @param search
     * @param hqlCriteria
     */
    private static Criteria applyOrderByForHQL(Search search, Criteria hqlCriteria) {
        // Preparing the HQL Query OrderBy Clause.
        List<Order> orderList = search.getOrderList();

        for (Order order : orderList) {

            // ASC and DESC Types are supported
            if (order.getType() == OrderType.ASC) {

                hqlCriteria.addOrder(org.hibernate.criterion.Order.asc(order.getFullName()));
            } else if (order.getType() == OrderType.DESC) {

                hqlCriteria.addOrder(org.hibernate.criterion.Order.desc(order.getFullName()));
            } else
                throw new ExpressionException("Order Type %s not supported.", order.getType());

        }

        return hqlCriteria;
    }

    /**
     * This method apply the HQL restrictions to the HQL Criteria param input
     * from the criteriaRoot of the search object.
     *
     * @param criteriaRoot The search object criteria.
     * @param hqlCriteria  The applied HQL criteria.
     * @return The Hql criteria object.
     */
    private static void applyHQLRestrictions(Criterion criteria, HQLCriteriaWrapper hqlCriteria) {

        if (criteria == null)
            return;

        org.hibernate.criterion.Criterion hqlCriterion = null;

        // Handling the Complex Types first.
        if (criteria.isSimple()) {

            // Applying the Simple Criteria first.
            hqlCriterion = applySimpleCriteria(criteria, hqlCriteria);

        } else if (criteria.isComplex()) { // Handling the Complex types

            // Applying the Complex Criteria.
            hqlCriterion = applyComplexCriteria(criteria, hqlCriteria);

        } else {
            throw new ExpressionException(
                    "UnKnown Operator Type (other than Simple or Complex) during HQL SearchConversion.");
        }

        if (hqlCriterion != null)
            hqlCriteria.hqlCriteria.add(hqlCriterion);

    }

    /**
     * This method applies the complex HQL restrictions to our supplied search
     * criterion
     *
     * @param criteria
     * @return
     */
    private static org.hibernate.criterion.Criterion applyComplexCriteria(Criterion criteria,
                                                                          HQLCriteriaWrapper hqlCriteria) {
        org.hibernate.criterion.Criterion hqlCriterion = null;
        ComplexCriterion complexCriterion = (ComplexCriterion) criteria;

        if (complexCriterion.isLinkedForm() || complexCriterion.isJoinForm()) {

            if (complexCriterion.getOperator() == OperatorType.And) {

                if (complexCriterion.isJoinForm()) {
                    // lhs=SimpleCriteria of the previous object, and rhs =
                    // Object Criteria and operator type is And.
                    hqlCriteria.hqlCriteria.add(applySimpleCriteria(complexCriterion.getLhs(),
                            hqlCriteria));
                    hqlCriterion = applySimpleCriteria(complexCriterion.getRhs(), hqlCriteria);
                } else {
                    if (complexCriterion.getRhs().isComplex()) {
                        // lhs=SimpleCriteria and rhs = Complex Criteria
                        org.hibernate.criterion.Criterion criteriaOne = applySimpleCriteria(
                                complexCriterion.getLhs(), hqlCriteria);
                        org.hibernate.criterion.Criterion criteriaTwo = applyComplexCriteria(
                                complexCriterion.getRhs(), hqlCriteria);

                        if (criteriaTwo != null) {
                            hqlCriterion = Restrictions.and(criteriaOne, criteriaTwo);
                        } else {
                            // criteriaTwo== null means that object criteria is
                            // applied to it.
                            hqlCriterion = criteriaOne;
                        }
                    } else {
                        // lhs=SimpleCriteria and rhs = Simple Criteria
                        org.hibernate.criterion.Criterion criteriaOne = applySimpleCriteria(
                                complexCriterion.getLhs(), hqlCriteria);
                        org.hibernate.criterion.Criterion criteriaTwo = applySimpleCriteria(
                                complexCriterion.getRhs(), hqlCriteria);
                        hqlCriterion = Restrictions.and(criteriaOne, criteriaTwo);
                    }
                }
            } else if (complexCriterion.getOperator() == OperatorType.Or) {

                if (complexCriterion.getRhs().isComplex()) {
                    // lhs=SimpleCriteria and rhs = Complex Criteria
                    hqlCriterion = Restrictions.or(applySimpleCriteria(complexCriterion.getLhs(),
                            hqlCriteria), applyComplexCriteria(complexCriterion.getRhs(),
                            hqlCriteria));
                } else {
                    // lhs=SimpleCriteria and rhs = Simple Criteria
                    hqlCriterion = Restrictions.or(applySimpleCriteria(complexCriterion.getLhs(),
                            hqlCriteria), applySimpleCriteria(complexCriterion.getRhs(),
                            hqlCriteria));
                }
            } else if (criteria.getOperator() == OperatorType.Not) {

                if (complexCriterion.getRhs().isComplex()) {
                    // We support the complex criteria in the not operation, lhs
                    // = Complex Criteria.
                    hqlCriterion = Restrictions.not(applyComplexCriteria(complexCriterion.getLhs(),
                            hqlCriteria));
                } else {
                    // We support the Simple criteria in the not operation as
                    // well, lhs = Simple Criteria.
                    hqlCriterion = Restrictions.not(applySimpleCriteria(complexCriterion.getLhs(),
                            hqlCriteria));
                }
            } else {

                throw new ExpressionException(
                        "UnKnown Operator Type (Complex Criteria) during HQL SearchConversion.");
            }
        } else {
            // This check is Added for resolution for Campaign Search Exception
            // By Kazim... May be any side Effects are there..
            if (complexCriterion.getLhs().isComplex() && complexCriterion.getRhs().isComplex()) {
                org.hibernate.criterion.Criterion criteriaOne = applyComplexCriteria(
                        complexCriterion.getLhs(), hqlCriteria);
                org.hibernate.criterion.Criterion criteriaTwo = applyComplexCriteria(
                        complexCriterion.getRhs(), hqlCriteria);
                if (criteriaOne != null) {
                    hqlCriterion = Restrictions.and(criteriaOne, criteriaTwo);
                } else {
                    hqlCriterion = criteriaTwo;
                }

            } else if (complexCriterion.getLhs().isComplex()) {
                org.hibernate.criterion.Criterion criteriaOne = applyComplexCriteria(
                        complexCriterion.getLhs(), hqlCriteria);
                org.hibernate.criterion.Criterion criteriaTwo = applySimpleCriteria(
                        complexCriterion.getRhs(), hqlCriteria);
                if (criteriaOne != null) {
                    hqlCriterion = Restrictions.and(criteriaOne, criteriaTwo);
                } else {
                    hqlCriterion = criteriaTwo;
                }

            } else if (complexCriterion.getRhs().isComplex()) {
                org.hibernate.criterion.Criterion criteriaOne = applyComplexCriteria(
                        complexCriterion.getRhs(), hqlCriteria);
                org.hibernate.criterion.Criterion criteriaTwo = applySimpleCriteria(
                        complexCriterion.getLhs(), hqlCriteria);
                if (criteriaOne != null) {
                    hqlCriterion = Restrictions.and(criteriaTwo, criteriaOne);
                } else {
                    hqlCriterion = criteriaTwo;
                }

            } else {
                org.hibernate.criterion.Criterion criteriaOne = applySimpleCriteria(
                        complexCriterion.getLhs(), hqlCriteria);
                org.hibernate.criterion.Criterion criteriaTwo = applySimpleCriteria(
                        complexCriterion.getRhs(), hqlCriteria);
                hqlCriterion = null;
            }

        }
        return hqlCriterion;
    }

    /**
     * This method applys the simple HQL restrictionsto our supplied search
     * criterion
     *
     * @param criteria
     * @return
     */
    private static org.hibernate.criterion.Criterion applySimpleCriteria(Criterion criteria,
                                                                         HQLCriteriaWrapper hqlCriteria) {
        org.hibernate.criterion.Criterion hqlCriterion = null;
        SimpleCriterion<?> simpleCriterion = null;
        OperatorType operatorType = criteria.getOperator();

        // Save casting.
        if (criteria instanceof SimpleCriterion)
            simpleCriterion = (SimpleCriterion) criteria;

        // The between operator type is handled.
        if (operatorType == OperatorType.Between) {

            BetweenCriterion<?> betweenCriterion = (BetweenCriterion<?>) simpleCriterion;
            hqlCriterion = Restrictions.between(betweenCriterion.getCompleteFieldName(),
                    betweenCriterion.value, betweenCriterion.highValue);
        } else if (operatorType == OperatorType.Eq) {

            hqlCriterion = Restrictions.eq(simpleCriterion.getCompleteFieldName(),
                    simpleCriterion.getValue());
        } else if (operatorType == OperatorType.Gt) {

            hqlCriterion = Restrictions.gt(simpleCriterion.getCompleteFieldName(),
                    simpleCriterion.getValue());
        } else if (operatorType == OperatorType.GtEq) {

            hqlCriterion = Restrictions.ge(simpleCriterion.getCompleteFieldName(),
                    simpleCriterion.getValue());
        } else if (operatorType == OperatorType.Lt) {

            hqlCriterion = Restrictions.lt(simpleCriterion.getCompleteFieldName(),
                    simpleCriterion.getValue());
        } else if (operatorType == OperatorType.LtEq) {

            hqlCriterion = Restrictions.le(simpleCriterion.getCompleteFieldName(),
                    simpleCriterion.getValue());
        } else if (operatorType == OperatorType.NEq) {

            hqlCriterion = Restrictions.ne(simpleCriterion.getCompleteFieldName(),
                    simpleCriterion.getValue());
        } else if (operatorType == OperatorType.Null) {

            hqlCriterion = Restrictions.isNull(simpleCriterion.getCompleteFieldName());
        } else if (operatorType == OperatorType.NotNull) {

            hqlCriterion = Restrictions.isNotNull(simpleCriterion.getCompleteFieldName());
        } else if (operatorType == OperatorType.Like) {

            hqlCriterion = Restrictions.like(simpleCriterion.getCompleteFieldName(),
                    simpleCriterion.getValue());
        } else if (operatorType == OperatorType.NLike) {

            hqlCriterion = Restrictions.like(simpleCriterion.getCompleteFieldName(),
                    simpleCriterion.getValue());
        } else if (operatorType == OperatorType.In) {

            ListCriterion<?> listCriterion = (ListCriterion<?>) criteria;
            hqlCriterion = Restrictions.in(listCriterion.getCompleteFieldName(),
                    listCriterion.getValueList());
        } else if (operatorType == OperatorType.NotIn) {

            ListCriterion<?> listCriterion = (ListCriterion<?>) criteria;
            hqlCriterion = Restrictions.not(Restrictions.in(listCriterion.getCompleteFieldName(),
                    listCriterion.getValueList()));
        } else if (operatorType == OperatorType.None) {

            // This If handles the Object Criteria used or join operations.
            ObjectCriterion objectCriterion = (ObjectCriterion) criteria;

            // checking if we want a left join
            if (objectCriterion.getJoinType() == JoinType.Natural) {
                // Creating the new HQL criteria.
                if (StringHelper.isEmpty(objectCriterion.getFrom().getAlias()))
                    hqlCriteria.hqlCriteria.createCriteria(objectCriterion.getObjectName());
                else
                    hqlCriteria.hqlCriteria.createCriteria(objectCriterion.getObjectName(),
                            objectCriterion.getFrom().getAlias());
            } else if (objectCriterion.getJoinType() == JoinType.LeftOuter) {
                // Creating the new HQL criteria.
                if (StringHelper.isEmpty(objectCriterion.getFrom().getAlias()))
                    hqlCriteria.hqlCriteria.createCriteria(objectCriterion.getObjectName(),
                            Criteria.LEFT_JOIN);
                else
                    hqlCriteria.hqlCriteria.createCriteria(objectCriterion.getObjectName(),
                            objectCriterion.getFrom().getAlias(), Criteria.LEFT_JOIN);
            }
            // Now applying the HQL restrictions to the Criteria object.

            applyHQLRestrictions(objectCriterion.getRootCriterion(), hqlCriteria);
        } else {

            throw new ExpressionException(
                    "UnKnown Operator Type (Simple Criteria) during HQL SearchConversion.");
        }

        return hqlCriterion;
    }
}
