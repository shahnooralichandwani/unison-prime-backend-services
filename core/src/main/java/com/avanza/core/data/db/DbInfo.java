package com.avanza.core.data.db;

import com.avanza.core.data.DataSourceException;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.configuration.XmlConfigReader;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

//TODO This class should be enhanced for Application Server Data Pool
public class DbInfo {

    private static Logger logger = Logger.getLogger(DbInfo.class);

    public static final String XML_NAME = "name";
    static final String XML_KEY = "db-key";
    static final String XML_DRIVER = "driver";
    static final String XML_PROVIDER = "provider";
    static final String XML_DB_USER = "db-user";
    static final String XML_PASSWORD = "password";
    static final String XML_CONN_URI = "conn-uri";
    static final String XML_POOL_PARAMS = "pool-params";
    static final String XML_DEFAULT = "default";
    static final String XML_AUTO_COMMIT = "auto-commit";

    private String name;
    private String driver;
    private ProviderType type;
    private String user;
    private String password;
    private String connectionUri;
    private Properties poolParams;
    private DataSource dataSource;
    private int queryTimeOut;
    private String key;

    public DbInfo(String name, String key, String driver, ProviderType type, String user, String password, String conUri, String propsVal, boolean autoCommit) {

        this.name = name;
        this.setKey(key);
        this.setDriver(driver);
        this.setType(type);
        this.setUser(user);
        this.setPassword(password);
        this.setConnectionUri(conUri);
        this.setPoolParams(propsVal);
        ComboPooledDataSource cpds = new ComboPooledDataSource();
        try {
            cpds.setDriverClass(driver);
            //Class.forName(driver).newInstance();

            /*
             * cpds.setDriverClass will auto instantiate new driver instance but the commented code snippet
             * would also do fine. For the sake of completion have used the C3P0 mechanism.
             *
             */
        } catch (Exception e) {
            throw new DataSourceException(e, "Unable to create instance of driver: " + driver);
        }
        cpds.setJdbcUrl(conUri);
        cpds.setUser(user);
        cpds.setPassword(password);
        setPoolSettings(cpds);
        this.dataSource = cpds;
		/*GenericObjectPool gPool = new GenericObjectPool();		
		setPoolSettings(gPool);
		ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(this.getConnectionUri(), 
				this.getUser(), this.getPassword());
		PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, 
				gPool, null, this.poolParams.getProperty("validationQuery"), true, autoCommit);
		
		this.connectionPool = poolableConnectionFactory.getPool();
				
		this.dataSource = new PoolingDataSource(gPool);
		*/
    }

    private void setPoolSettings(ComboPooledDataSource source) {
        if (this.poolParams.getProperty("maxPoolSize") != null)
            source.setMaxPoolSize(Integer.parseInt(this.poolParams.getProperty("maxPoolSize")));

        if (this.poolParams.getProperty("minPoolSize") != null)
            source.setMinPoolSize(Integer.parseInt(this.poolParams.getProperty("minPoolSize")));

        if (this.poolParams.getProperty("maxIdleTime") != null)
            source.setMaxIdleTime(Integer.parseInt(this.poolParams.getProperty("maxIdleTime")));

        //Alternative for DBCP "maxWait" parameter
        if (this.poolParams.getProperty("checkoutTimeout") != null)
            source.setCheckoutTimeout(Integer.parseInt(this.poolParams.getProperty("checkoutTimeout")));

        //No. of connections C3P0 acquires after pool is exhausted
        if (this.poolParams.getProperty("acquireIncrement") != null)
            source.setAcquireIncrement(Integer.parseInt(this.poolParams.getProperty("acquireIncrement")));

        //Alternative for DBCP testOnBorrow
        if (this.poolParams.getProperty("testOnCheckin") != null)
            source.setTestConnectionOnCheckin(Boolean.parseBoolean(this.poolParams.getProperty("testOnCheckin")));

        //Alternative for DBCP testOnReturn
        if (this.poolParams.getProperty("testOnCheckout") != null)
            source.setTestConnectionOnCheckout(Boolean.parseBoolean(this.poolParams.getProperty("testOnCheckout")));

        //Additional parameter for C3P0
        if (this.poolParams.getProperty("idleTestPeriod") != null)
            source.setIdleConnectionTestPeriod(Integer.parseInt(this.poolParams.getProperty("idleTestPeriod")));

        //Alternate for DBCP validationQuery
        if (this.poolParams.getProperty("validationQuery") != null)
            source.setPreferredTestQuery(this.poolParams.getProperty("validationQuery"));

        if (this.poolParams.getProperty("queryTimeOut") != null)
            this.setQueryTimeOut(Integer.parseInt(this.poolParams.getProperty("queryTimeOut")));

        //Property added to support initial pool size setting. Currently not added in the config file.
        if (this.poolParams.getProperty("initialPoolSize") != null)
            source.setInitialPoolSize(Integer.parseInt(this.poolParams.getProperty("initialPoolSize")));

        //This parameter could be used to turn on preparedStatement pooling.
        if (this.poolParams.getProperty("maxStatements") != null)
            source.setMaxStatements(Integer.parseInt(this.poolParams.getProperty("maxStatements")));

        if (this.poolParams.getProperty("maxConnectionAge") != null)
            source.setMaxConnectionAge(Integer.parseInt(this.poolParams.getProperty("maxConnectionAge")));
        /*
         * One of the parameters of DBCP "timeBetweenEvictionRunsMillis" is not used for C3P0
         */

    }

    public int getQueryTimeOut() {
        return queryTimeOut;
    }

    public void setQueryTimeOut(int queryTimeOut) {
        this.queryTimeOut = queryTimeOut;
    }

    public String getName() {

        return this.name;
    }

    public String getDriver() {

        return this.driver;
    }

    public void setDriver(String driver) {

        this.driver = driver;
    }

    public ProviderType getType() {

        return this.type;
    }

    public void setType(ProviderType type) {

        this.type = type;
    }

    public String getUser() {

        return this.user;
    }

    public void setUser(String user) {

        this.user = user;
    }

    public String getPassword() {

        // TODO: Need to decrypt password before returning
        return this.password;
    }

    public void setPassword(String password) {

        // TODO: Need to encrypt password before setting
        this.password = password;
    }

    public Properties getPoolParams() {

        return this.poolParams;
    }

    public void setPoolParams(Properties poolParams) {

        this.poolParams = poolParams;
    }

    public String getConnectionUri() {

        return this.connectionUri;
    }

    public void setConnectionUri(String connectionUri) {

        this.connectionUri = connectionUri;
    }

    public void setPoolParams(String propsVal) {

        if ((propsVal != null) && (propsVal.length() > 0)) {

            String[] valList = propsVal.split(";");
            Properties temp = new Properties();
            for (int cnt = 0; cnt < valList.length; cnt++) {

                int idx = valList[cnt].indexOf('=');
                if (idx > -1) {

                    String key = valList[cnt].substring(0, idx).trim();
                    String value = valList[cnt].substring(idx + 1).trim();
                    temp.setProperty(key, value);
                }

                if (temp.size() > 0)
                    this.poolParams = temp;
            }
        } else {
            this.poolParams = new Properties();
        }
    }

    DbConnection getDbConnection() {

        try {
            int activeConnections = ((ComboPooledDataSource) this.dataSource).getNumBusyConnections();
            int idleConnections = ((ComboPooledDataSource) this.dataSource).getNumIdleConnections();
            //logger.logInfo("Database: %s, Connection Active: %s, Connection Idle: %s.", this.name, this.connectionPool.getNumActive(), this.connectionPool.getNumIdle());
            logger.logInfo("Database: %s, Connection Active: %s, Connection Idle: %s.", this.name, activeConnections, idleConnections);
            return (new DbConnection(this, this.dataSource.getConnection()));

        } catch (SQLException e) {

            throw new DataSourceDownException(e, "Unable to retrieve Connection from dataSource: %s.", this.name);
        }
    }

    void returnConnection(Connection con) {

        try {
            //(new PoolableConnection(con, this.connectionPool)).close();
            con.close();
        } catch (SQLException e) {

            throw new DataSourceException(e, "Exception occured while returning a Connection back to pool");
        }
    }

    //	// Test Fucntion
    /*public static void main(String[] args) throws SQLException, UnsupportedEncodingException {

        //ApplicationLoader.load("C:\\\\Documents and Settings\\\\shahnoor.mansoor\\\\Workspaces\\\\MyEclipse 8.5\\\\Unison\\\\resources\\\\unison-conf.xml");

        //DataBroker broker = DataRepository.getBroker("JDBC.Unison");
		
		*//*broker.update("UPDATE META_ENTITY SET " +
				"");
	*//*


        logger.logInfo("[Registering the ShutDown Hook.]");
        Runtime.getRuntime().addShutdownHook(new ApplicationShutdownHook());
        String configFilePath = "C:\\\\Documents and Settings\\\\shahnoor.mansoor\\\\Workspaces\\\\MyEclipse 8.5\\\\.metadata\\\\.me_tcat\\\\webapps\\\\Unison\\\\WEB-INF\\\\classes\\\\unison-conf.xml";
        logger.logInfo("[Starting up the Application from the config file: '%1$s']", configFilePath);


        // Need to add the base path to the Unison web app to the session.
        String baseAppPath = configFilePath.substring(0, configFilePath.lastIndexOf(File.separator));
		*//*BASE_APPLICATION_PATH = baseAppPath;

		confFilePath = configFilePath;*//*
        // Reading and parsing the xml config file.

        ConfigSection configSection = null;//ApplicationLoader.loadConfigFile(configFilePath);

        try {
            logger.logInfo("[Parsing the config file]");
            XmlConfigReader xmlConfigReader = new XmlConfigReader();
            configSection = xmlConfigReader.load(configFilePath);
            Guard.checkNull(configSection, String.format("ApplicationLoader.load('%s')", configFilePath));
        } catch (Exception e) {
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        *//* Shahbaz
     * Dated 20-Sept-2010
     * During Campaign issues fixes
     * Holds Configuration Cache for future use
     * start
     *//*
        ConfigurationCache.getInstance().setConfigurations(configSection);
        // end
        try {
            logger.logInfo("[Loading the Data Repositories]");
            ConfigSection dataRepository = configSection.getChild(ApplicationLoader.XML_DATA_REPOSITORY_SECTION);
            DataRepository.load(dataRepository);
        } catch (Exception e) {
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            e.printStackTrace();
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }

        //DataRepository.getDbConnection();

        DataBroker broker = DataRepository.getBroker("JDBC.Unison");
        String canonicalName = broker.getClass().getCanonicalName();

        broker.update("update META_ENTITY set TEMPLATE_ENABLED=0 where META_ENT_ID=0000000001");
		
		
		
		
        *//*XmlConfigReader xmlConfigReader = new XmlConfigReader();
        ConfigSection configSection = xmlConfigReader.load("C:\\\\Documents and Settings\\\\shahnoor.mansoor\\\\Workspaces\\\\MyEclipse 8.5\\\\Unison\\\\resources\\\\unison-conf.xml");

        DbCatalog dbCatalog = new DbCatalog();
        dbCatalog.load(configSection.getChild("Data-Repository").getChild("db-catalog"));
		
		for (int i=0;i<50;i++) {
		    DbConnection con =  dbCatalog.getDbConnection("UNISON");
    
    		ResultSet rs = con.getResults("select top 5 full_name from customer");
    		while(rs.next()){
    		    System.out.println(rs.getString(1));
    		}
    		con.close();
		}*//*
    }*/

    public static String testConn() throws SQLException {

        XmlConfigReader xmlConfigReader = new XmlConfigReader();
        ConfigSection configSection = xmlConfigReader.load("..\\webapps\\Unison\\WEB-INF\\classes\\unison-conf.xml");

        DbCatalog dbCatalog = new DbCatalog();
        dbCatalog.load(configSection.getChild("Data-Repository").getChild("db-catalog"));
        DbConnection conn = dbCatalog.getDbConnection("CBDBASE");
        conn.getConnection().close();
        System.out.println("CBDBASE connected.");

        conn = dbCatalog.getDbConnection("PrimeDB");
        conn.getConnection().close();
        System.out.println("PrimeDB connected.");

        conn = dbCatalog.getDbConnection("TRADWIND");
        conn.getConnection().close();
        System.out.println("TRADWIND connected.");

        conn = dbCatalog.getDbConnection("PHOENIX");
        conn.getConnection().close();
        System.out.println("PHOENIX connected.");

        conn = dbCatalog.getDbConnection("CBDFS");
        conn.getConnection().close();
        System.out.println("CBDFS connected.");

        conn = dbCatalog.getDbConnection("ChequeImageCb");
        conn.getConnection().close();
        System.out.println("ChequeImageCb connected.");

        conn = dbCatalog.getDbConnection("ODS-DB");
        conn.getConnection().close();
        System.out.println("ODS-DB connected.");

        conn = dbCatalog.getDbConnection("MIS-SQL");
        conn.getConnection().close();
        System.out.println("MIS-SQL connected.");

        conn = dbCatalog.getDbConnection("SMS");
        conn.getConnection().close();
        System.out.println("SMS connected.");

        conn = dbCatalog.getDbConnection("RIGHTFAX");
        conn.getConnection().close();
        System.out.println("RIGHTFAX connected.");
        return "";
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setAutoCommit(boolean autoCommit) {

    }

}
