package com.avanza.core.data.expression;

import com.avanza.core.data.DataFilter;
import com.avanza.core.data.DataFilterAttributes;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.MetaRelationship;
import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.Wrapper;
import com.jamonapi.utils.Logger;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CriterionTree implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4851965742645358933L;
    private Criterion root;
    private Map<String, Criterion> fieldCriterionMap = new HashMap<String, Criterion>();

    public CriterionTree() {

    }

    public void setRoot(Criterion item) {

        Guard.checkNull(item, "CriterionTree.setRoot(Criterion)");
        this.root = item;
    }

    public Criterion getRoot() {

        return this.root;
    }

    public void addCriterion(Criterion item) {

        if (item == null) {
            Logger.logInfo("CriterionTree.addCriterion,Criterion was null");
        } else {
            this.addCriterion(OperatorType.And, item);
        }
    }

    public void addCriterionOr(Criterion item) {

        Guard.checkNull(item, "CriterionTree.OrCriterion");
        this.addCriterion(OperatorType.Or, item);
    }

    public void setWhereClause(String whereClause) {

        this.root = CriteriaParser.parseClause(whereClause);
    }

    public void clear() {

        this.root = null;
    }

    void toString(StringBuilder builder) {

        if (this.root != null) {

            builder.append(Criterion.WHERE_CLAUSE);
            this.root.toString(builder);
        }
    }

    public Criterion getCriterionByFieldName(String fieldName) {
        return fieldCriterionMap.get(fieldName);
    }

    private void addCriterion(OperatorType type, Criterion item) {
        Wrapper<Criterion> rootWrapper = new Wrapper<Criterion>(root);
        addCriterion(rootWrapper, type, item);
        root = rootWrapper.unwrap();

        // add into map for easy access of criterion.
        SimpleCriterion<?> simpleCriterion = null;
        if (item instanceof SimpleCriterion<?>) {
            simpleCriterion = (SimpleCriterion<?>) item;
        } else if (item instanceof ComplexCriterion) {
            if (((ComplexCriterion) item).lhs instanceof SimpleCriterion<?>) {
                simpleCriterion = (SimpleCriterion<?>) ((ComplexCriterion) item).lhs;
            } else if (((ComplexCriterion) item).rhs instanceof SimpleCriterion<?>) {
                simpleCriterion = (SimpleCriterion<?>) ((ComplexCriterion) item).rhs;
            }
        }
        if (simpleCriterion != null) {
            QueryRepresentable field = ((SimpleCriterion<?>) simpleCriterion).getField();
            if (field instanceof Column) {
                fieldCriterionMap.put(((Column) field).getFullName(), item);
            }
        }
    }

    public static void addCriterion(Criterion parent, OperatorType type, Criterion item) {
        addCriterion(new Wrapper<Criterion>(parent), type, item);
    }

    private static void addCriterion(Wrapper<Criterion> parentWrapper, OperatorType type, Criterion item) {

        Criterion parent = parentWrapper.unwrap();
        Criterion grandParent = null;
        Criterion lastParent = parent;
        if (parent != null) {
            grandParent = parent.getParent();
        }

        if (parent == null) {
            parent = item;
        } else if (parent.isSimple()) {
            parent = new ComplexCriterion(type, item, parent);
        } else {

            Criterion rhs = parent;
            Criterion lhs = item;
            parent = new ComplexCriterion(type, lhs, rhs);
        }

        //wrapper with new parent.
        parentWrapper.wrap(parent);

        // adjust parent and child relations.
        if (grandParent != null) {

            // join ancestor to new parent.
            parent.setParent(grandParent);
            if (grandParent instanceof ComplexCriterion) {
                if (((ComplexCriterion) grandParent).getLhs() == lastParent) {
                    ((ComplexCriterion) grandParent).setLhs(parent);
                } else {
                    ((ComplexCriterion) grandParent).setRhs(parent);
                }
            }
        }
    }

    /**
     * <b>Assumptions</b><br/>
     * -single direct relation exsists between tables.<br/>
     * -alias must be defined<br/>
     *
     * @param filter
     * @return
     */
    public static CriterionTree createFromDataFilter(DataFilter filter) {
        MetaEntity entity = MetaDataRegistry.getMetaEntity(filter.getEntityName());
        String alias = entity.getId();
        List<? extends DataFilterAttributes> attributeList = filter.getAttributeList();
        CriterionTree tree = new CriterionTree();

        // default operator
        OperatorType defaultOperatorType = null;
        if (StringHelper.isNotEmpty(filter.getDefaultJoinType())) {
            defaultOperatorType = OperatorType.parseToken(filter.getDefaultJoinType());
        }

        for (DataFilterAttributes attrib : attributeList) {

            // join operator default is And if not defined at filter level
            OperatorType operatorType = (defaultOperatorType != null)
                    ? defaultOperatorType
                    : OperatorType.And;

            String attributeName = attrib.getAttributeName();
            Criterion criterion = null;
            switch (attrib.getAttributeFilterType()) {
                case None:
                    criterion = SimpleCriterion.getSimpleCriterion(attrib.getOperator(), attributeName,
                            alias, attrib.getValue());
                    break;

                case SubFilter:
                    criterion = createFromDataFilter(attrib.getSubFilter()).getRoot();
                    break;

                case Aggregate:
                    MetaRelationship relationShip = entity.getRelationShipBySubEntity(
                            attrib.getSubFilter().getEntityName()).get(0);
                    Search search = Search.createFromDataFilter(attrib.getSubFilter());

                    String subFilterAlias = attrib.getSubFilter().getEntityName();

                    // change from clause of search e.g customer.12344 12345.
                    search.setFrom(new From(alias + "." + relationShip.getRelationshipId(),
                            subFilterAlias));

                    // add agregate function to search
                    search.addColumn(new Column(attributeName, subFilterAlias, "",
                            Column.Function.parseToken(attrib.getAggregationType())));

                    criterion = new SimpleCriterion(OperatorType.parseToken(attrib.getOperator()),
                            search, attrib.getValue());
            }

            if (StringHelper.isNotEmpty(attrib.getJoinType())
                    && attrib.getJoinType().equalsIgnoreCase(OperatorType.Or.getValue())) {
                operatorType = OperatorType.Or;
            }
            if (criterion != null)
                tree.addCriterion(operatorType, criterion);

        }
        return tree;
    }
}
