package com.avanza.core.data.expression;

import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;

import java.util.ArrayList;
import java.util.List;

public class Order extends Expression implements QueryRepresentable {
    public static final String ORDER_CLAUSE = "ORDER BY";
    private static final Class<OrderType> ENUM_TYPE = OrderType.class;

    private String alias;
    private OrderType type;

    public Order(String alias, String name, OrderType type) {

        this.setName(name);
        this.setAlias(alias);
        this.setType(type);
    }

    public Order(String fullName, OrderType type) {

        this.init(fullName, type);
    }

    public Order(String fullName) {

        this.init(fullName, OrderType.ASC);
    }

    private void init(String fullName, OrderType type) {
        int idx = fullName.indexOf(".");
        if (idx != -1) {
            String alias = fullName.substring(0, idx);
            String fieldName = fullName.substring(idx + 1);
            this.setName(fieldName);
            this.setAlias(alias);
        } else
            this.setName(fullName);
        this.setType(type);
    }

    public OrderType getType() {

        return this.type;
    }

    public void setType(OrderType type) {

        this.type = type;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder(200);

        return this.toString(builder).toString();
    }

    public StringBuilder toString(StringBuilder builder) {
        represent(new HqlQueryRepresentator(builder), "", "");
        return builder;
    }

    public StringBuilder toString(StringBuilder builder, boolean reverse, boolean useAlias) {
        represent(new HqlQueryRepresentator(builder), reverse, useAlias);
        return builder;
    }

    public static List<Order> ParseClause(String orderBy) {
        Guard.checkNullOrEmpty(orderBy, "Order.ParseClause(String)");

        if (StringHelper.startsWithIgnoreCase(orderBy, Order.ORDER_CLAUSE))
            orderBy = orderBy.substring(Order.ORDER_CLAUSE.length()).trim();

        String[] orderList = orderBy.split(StringHelper.COMMA);
        ArrayList<Order> retVal = new ArrayList<Order>(orderList.length);

        try {
            for (int index = 0; index < orderList.length; index++) {


                String orderTemp = orderList[index].trim();
                if (StringHelper.isNotEmpty(orderTemp)) {

                    int idx = orderTemp.indexOf(StringHelper.SPACE);

                    if (idx != -1) {
                        String name = orderTemp.substring(0, idx);
                        OrderType type = OrderType.fromString(orderTemp.substring(idx + 1).trim());
                        retVal.add(new Order(name, type));
                    } else {
                        retVal.add(new Order(orderTemp));
                    }
                }
            }

            return retVal;
        } catch (RuntimeException excep) {
            throw new ExpressionException(excep, "Failed to parse %s as order list for Order clause", orderBy);
        }
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getFullName() {

        if (StringHelper.isNotEmpty(this.alias))
            return this.alias + "." + this.name;
        else
            return this.name;
    }

    public boolean represent(QueryRepresentator representator, boolean reverse, boolean useAlias) {
        return representator.representOrderclause(this, reverse, useAlias);
    }

    public boolean represent(QueryRepresentator representator, String opener, String closer) {
        return represent(representator, false, true);
    }
}
