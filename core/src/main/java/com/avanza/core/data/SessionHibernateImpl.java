package com.avanza.core.data;

import com.avanza.core.data.DataBroker.SessionScope;

public class SessionHibernateImpl implements Session {

    private org.hibernate.Session hibernateSession;
    private org.hibernate.Transaction hibernateTxn;
    private org.hibernate.SessionFactory factory;

    // long live objects of this class need to close the session on commit and
    // rollback.
    private SessionScope sessionScope;

    // As we are closing hibernate session inside this class. So hibernate
    // session's life should be limited to the life of this object.
    private SessionHibernateImpl(org.hibernate.SessionFactory factory,
                                 SessionScope sessionScope) {
        this.factory = factory;
        this.sessionScope = sessionScope;
    }

    public void beginTransaction() {
        //we need to clear the session in order to avoid the exception
        //****org.hibernate.NonUniqueObjectException: a different object with the same identifier value was already associated with the session******

        if (this.hibernateTxn == null) {
            org.hibernate.Session hbSession = (org.hibernate.Session) value();
            hbSession.clear();
            this.hibernateTxn = hbSession.beginTransaction();
        } else if (this.hibernateTxn.wasCommitted()/*this.hibernateTxn.getStatus().isOneOf(TransactionStatus.COMMITTED)*/) {
            // UNISONPRIME: //(this.hibernateTxn.wasCommitted()) {
            hibernateSession.clear();
            this.hibernateTxn.begin();
        }
    }

    public boolean isTransactionActive() {
        // UNISONPRIME:
        return (this.hibernateTxn != null && /*this.hibernateTxn.getStatus().isOneOf(TransactionStatus.ACTIVE) */  this.hibernateTxn.isActive());
    }

    public void commit() {
        // UNISONPRIME:
        if (this.hibernateTxn != null &&/* this.hibernateTxn.getStatus().isOneOf(TransactionStatus.ACTIVE)*/ this.hibernateTxn.isActive()) {
            this.hibernateTxn.commit();
            this.hibernateTxn = null;

        }

    }


    public void notifyOnTaskFinished() {
        if (sessionScope == SessionScope.TASK_BASED && this.hibernateSession != null && !isTransactionActive()) {
            release();
        }

    }

    public void commitAndBegin() {

        commit();
        beginTransaction();
    }

    public void release() {
        if (this.hibernateSession != null) {
            // we are assuming that someone may forgot to rollback the
            // transaction.
            // so we will first rollback pending changes and then close the
            // session.
            if (this.hibernateTxn != null) {
                rollback();
            }

            this.hibernateSession.close();
            System.out.println("Session Close "
                    + Thread.currentThread().getId());
            this.hibernateSession = null;
        }
    }

    public void rollback() {
        // UNISONPRIME:
        if (this.hibernateTxn != null && /*this.hibernateTxn.getStatus().isOneOf(TransactionStatus.ACTIVE)*/this.hibernateTxn.isActive()) {
            this.hibernateTxn.rollback();
            this.hibernateTxn = null;
        }
    }

    public <T> T value() {
        if (hibernateSession == null || !hibernateSession.isOpen()) {
            hibernateSession = factory.openSession();
            System.out
                    .println("Session Open " + Thread.currentThread().getId());
        }
        return (T) hibernateSession;
    }

    public static Session createSession(org.hibernate.SessionFactory factory,
                                        String key) {
        return createSession(factory, key, SessionScope.REQUEST_BASED);
    }

    public static Session createSession(org.hibernate.SessionFactory factory,
                                        String key, SessionScope sessionScope) {

        TransactionContext txnContext = ApplicationContext.getContext()
                .getTxnContext();

        Session session = txnContext.getSession(key);
        if (session == null) {
            session = new SessionHibernateImpl(factory, sessionScope);
            txnContext.addSession(key, session);
        }

        return session;
    }
}
