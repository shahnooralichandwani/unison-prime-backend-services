package com.avanza.core.data.validation;

import java.util.ArrayList;

import com.avanza.core.meta.MetaAttribute;

public class ValidationInfo {

    ArrayList<String> errorList = new ArrayList<String>();
    String result;
    MetaAttribute metaAttribute;

    public ValidationInfo(String result, MetaAttribute metaAttribute) {
        this.result = result;
        this.metaAttribute = metaAttribute;
    }

    public ValidationInfo() {

    }

    public ArrayList<String> getErrorList() {
        return errorList;
    }

    public void setErrorList(ArrayList<String> errorList) {
        this.errorList = errorList;
    }

    public MetaAttribute getMetaAttribute() {
        return metaAttribute;
    }

    public void setMetaAttribute(MetaAttribute metaAttribute) {
        this.metaAttribute = metaAttribute;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void addError(String error) {
        errorList.add(error);
    }
}
