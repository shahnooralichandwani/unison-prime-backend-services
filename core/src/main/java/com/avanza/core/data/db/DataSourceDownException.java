package com.avanza.core.data.db;

import com.avanza.core.CoreException;

public class DataSourceDownException extends CoreException {

    private static final long serialVersionUID = 3;

    public DataSourceDownException(String message) {

        super(message);
    }

    public DataSourceDownException(String format, Object... args) {

        super(format, args);
    }

    public DataSourceDownException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public DataSourceDownException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public DataSourceDownException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public DataSourceDownException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}