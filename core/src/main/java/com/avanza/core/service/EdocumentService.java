/**
 *
 */
package com.avanza.core.service;

import com.avanza.core.CoreException;
import com.avanza.core.sdo.DataObject;

/**
 * @author shahbaz.ali
 *
 */
public class EdocumentService extends MetaDataService {

    @Override
    public String save(String metEntId, DataObject obj) throws CoreException, Exception {

        // Just calling parent's save method

        return super.save(metEntId, obj);
    }

}
