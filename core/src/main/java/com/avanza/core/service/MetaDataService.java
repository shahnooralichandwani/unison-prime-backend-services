/**
 *
 */
package com.avanza.core.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.avanza.core.CoreException;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.validation.ValidationInfo;
import com.avanza.core.meta.AttributeType;
import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.MetaException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.DataState;
import com.avanza.core.security.User;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.workflow.configuration.AssignmentType;
import com.avanza.workflow.configuration.Process;
import com.avanza.workflow.configuration.ProcessCatalog;
import com.avanza.workflow.configuration.StateParticipant;
import com.avanza.workflow.configuration.Transition;
import com.avanza.workflow.configuration.TransitionParticipant;
import com.avanza.workflow.configuration.filter.DefaultTransitionParticipantFilter;
import com.avanza.workflow.configuration.filter.TransitionParticipantFilter;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.Role;
import com.avanza.workflow.execution.ProcessContext;
import com.avanza.workflow.execution.ProcessContextImpl;
import com.avanza.workflow.execution.ProcessInstance;
import com.avanza.workflow.execution.ProcessManager;
import com.avanza.workflow.execution.TaskContext;
import com.avanza.workflow.execution.TaskLoadManager;

/**
 * @author Shahbaz.ali
 *         This class encapsulates the functionality of persisting
 *         meta entity and executing the associated process if any and return
 *         the new instance ID
 *         Also helps in executing workflow activity for an instance of an
 *         entity type
 */

public class MetaDataService {

    private final Logger logger = Logger.getLogger(MetaDataService.class);

    public String save(String metEntId, DataObject obj) throws CoreException, Exception {
        logger.logInfo("[Performing save ( ServiceAdapter.save() )]");
        Object objId = null;
        try {

            if (StringHelper.isEmpty(obj.getIdValue())) {
                MetaEntity entity = MetaDataRegistry.getMetaEntity(metEntId);
                MetaAttribute idMetaAttrib = entity.getIdAttribute();
                if (obj.getState() == DataState.New) {

                    if (idMetaAttrib.getType().equals(AttributeType.String)) {
                        objId = MetaDataRegistry.getNextCounterValue(idMetaAttrib.getCounterName());
                        obj.setValue(idMetaAttrib.getId(), objId);
                    } else {
                        objId = MetaDataRegistry.getNextCounterValueAsLong(idMetaAttrib.getCounterName());
                        obj.setValue(idMetaAttrib.getId(), objId);
                    }
                }
            } else {
                objId = obj.getIdValue();
            }

            String loginId = "";
            User user = ((UserDbImpl) ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY));

            if (user == null)
                loginId = "System";

            else
                loginId = user.getLoginId();

            obj.setValue("UPDATED_ON", Calendar.getInstance().getTime());
            obj.setValue("UPDATED_BY", loginId);
            obj.setValue("CREATED_ON", Calendar.getInstance().getTime());
            obj.setValue("CREATED_BY", loginId);

            Iterator<ValidationInfo> validationInfoIter = obj.validate().values().iterator();
            while (validationInfoIter.hasNext()) {
                ValidationInfo validationInfo = validationInfoIter.next();
                ArrayList<String> errorList = validationInfo.getErrorList();
                if (errorList.size() > 0) {
                    throw new CoreException("Data validation error (MetaService.Save()) ");
                }
            }

            boolean processFound = ProcessCatalog.isAnyAssociatedProcess(metEntId);

            if (!processFound) {
                obj.save();
            } else {

                ProcessContext ctx = new ProcessContextImpl(obj);

                Object productEntity = ctx.getProductEntity();
                if (productEntity.toString().equalsIgnoreCase("n/a"))
                    ctx.setProductEntity(null);

                Process process = ProcessCatalog.getProcessByDocumentId(metEntId, ctx);

                if (process != null) {
                    int TAT = (int) process.getAssociatedBinding(metEntId).getTurnAroundTime();
                    Calendar currentDateTime = Calendar.getInstance();
                    currentDateTime.setTime(Calendar.getInstance().getTime());
                    currentDateTime.add(Calendar.MINUTE, TAT);
                    Date followUpTime = currentDateTime.getTime();
                    ctx.setAttribute("FOLLOWUP_TIME", followUpTime);

                    TaskContext taskctx = new TaskContext("System", ctx.getInstanceId(), "", null);
                    ProcessInstance inst = ProcessManager.getInstance().createInstance(process.getProcessCode(), ctx, process.getAssociatedBinding(metEntId), taskctx);
                    if (inst == null) {
                        throw new CoreException("Exception initializing Process context (MetaService.Save()) ");
                    }
                } else {
                    throw new CoreException("Process not found while initializing Process context (MetaService.Save()) ");
                }

            }

        } catch (MetaException e) {
            logger.LogException("MetaException while saving DataObject (MetaService.Save()) ", e);
            throw e;
        } catch (Exception e) {
            logger.LogException("Exception while saving DataObject (MetaService.Save()) ", e);
            throw e;
        }

        return objId.toString();
    }

    public void doWorkflowActivity(String entityIdOrSysName, String objectInstanceId, String userId, String nextStatecode, List<String> usersToAssigned, String activityNotes) {

        MetaEntity entity = MetaDataRegistry.getMetaEntity(entityIdOrSysName);

        if (entity == null)
            entity = MetaDataRegistry.getEntityBySystemName(entityIdOrSysName);

        if (entity == null) {
            throw new CoreException(String.format("Entity not found for '%s'", entityIdOrSysName));
        }

        DataBroker broker = DataRepository.getBroker(entity.getSystemName());
        DataObject obj = broker.findById(entity.getSystemName(), objectInstanceId);

        if (obj == null) {
            throw new CoreException(String.format("Entity instance not found for '%s' of entity '%s'", objectInstanceId, entity.getSystemName()));
        }

        String processCode = obj.getProcessCode();
        ProcessInstance procInstance = ProcessManager.getInstance().populateInstance(processCode, new ProcessContextImpl(obj));

        boolean isRoleExist = false;

        try {
            DefaultTransitionParticipantFilter defaultFilter = new DefaultTransitionParticipantFilter();
            List<Role> roles = OrganizationManager.getAllRoles(userId);
            isRoleExist = defaultFilter.isRoleInTransitionParticipant(roles);
        } catch (Exception e) {
            logger.LogException("Filter Class Not Found", e);
        }

        Set<Transition> transitionList = procInstance.nextTasks();
        String taskCode = null;

        if (isRoleExist) {
            Set<StateParticipant> stateParticipant = procInstance.getStateParticipants(procInstance.getContext().getDocumentId(), procInstance.getCurrentState().getStateCode());

            for (Transition transition : transitionList) {
                TransitionParticipantFilter filter = null;
                for (TransitionParticipant tp : transition.getTransitionParticipantList()) {
                    try {
                        filter = (TransitionParticipantFilter) Class.forName(tp.getActivityUserFilter()).newInstance();
                    } catch (Exception e) {
                        logger.LogException("Filter Class Not Found", e);
                        continue;
                    }
                }
                if (filter == null) {
                    try {
                        filter = (TransitionParticipantFilter) Class.forName("com.avanza.workflow.configuration.filter.DefaultTransitionParticipantFilter").newInstance();
                    } catch (Exception e) {
                        logger.LogException("Filter Class Not Found", e);
                        continue;
                    }
                }
                if (transition.getTransitionParticipantList().size() > 0 && stateParticipant.size() > 0) {
                    boolean transParticipantExist = filter.getActivities(transition, stateParticipant, userId);
                    if (transParticipantExist) {
                        for (Iterator iterator = transitionList.iterator(); iterator.hasNext(); ) {
                            Transition nextTransition = (Transition) iterator.next();

                            if (nextTransition.getNextState().getStateCode().compareTo(nextStatecode) == 0) {
                                taskCode = nextTransition.getTransitionCode();
                            }
                        }

                        if (taskCode == null) {
                            throw new CoreException(String.format("Transition not found from current state to provided state i.e. '%s'", nextStatecode));
                        }

                        if (!procInstance.getCurrentActor().contains(userId) || !procInstance.getCalculatedActor().contains(userId))
                            logger.logWarning(String.format("User '%s' is not in current actors for the instance '%s' of entity '%s'", userId, objectInstanceId, entity.getSystemName()));

                        if (nextStatecode != null && nextStatecode.length() > 0) {
                            List<String> users = (usersToAssigned == null ? new ArrayList<String>() : usersToAssigned);

                            if (users.isEmpty())
                                users = procInstance.getCurrentActor();

                            procInstance.setCurrentActor(users);
                            procInstance.setCalculatedActor(users);

                            if (procInstance.getCurrentActor().size() == 1)
                                TaskLoadManager.incrementUserWorkLoad(procInstance.getCurrentActor().get(0), MetaDataRegistry.getNonAbstractParentEntity(MetaDataRegistry.getMetaEntity(procInstance.getContext().getDocumentId())).getId());

                            TaskContext ctx = new TaskContext(userId, taskCode, procInstance.getInstanceId(), activityNotes, nextStatecode);
                            boolean isSuccess = procInstance.moveToState(nextStatecode, ctx);
                            if (isSuccess) {
                                break;
                            } else {

                            }
                        } else {
                            Transition transitionbyId = procInstance.getCurrentState().getTransitionbyId(taskCode);
                            boolean isMannual = false;
                            if (AssignmentType.fromName(transitionbyId.getAssignmentType()) == AssignmentType.Mannual) {
                                List<String> users = usersToAssigned;
                                procInstance.setCurrentActor(users);
                                procInstance.setCalculatedActor(users);
                                if (procInstance.getCurrentActor().size() == 1)
                                    TaskLoadManager.incrementUserWorkLoad(procInstance.getCurrentActor().get(0), MetaDataRegistry.getNonAbstractParentEntity(MetaDataRegistry.getMetaEntity(procInstance.getContext().getDocumentId())).getId());
                                isMannual = true;
                            }
                            TaskContext ctx = new TaskContext(userId, taskCode, procInstance.getInstanceId(), activityNotes, null);
                            boolean isSuccess = procInstance.moveToNextState(ctx, isMannual);
                            if (!isSuccess) {

                            }
                        }

                    } else {
                        throw new CoreException(String.format("User '%s' is not in transition participant or state participant list of the entity '%s'", userId, entity.getSystemName()));
                    }

                } else {
                    throw new CoreException(String.format("Transition participants or state participants are not defined for this entity instance '%s'", objectInstanceId));
                }
            }

        } else {
            for (Iterator iterator = transitionList.iterator(); iterator.hasNext(); ) {
                Transition nextTransition = (Transition) iterator.next();

                if (nextTransition.getNextState().getStateCode().compareTo(nextStatecode) == 0) {
                    taskCode = nextTransition.getTransitionCode();
                }
            }

            if (taskCode == null) {
                throw new CoreException(String.format("Transition not found from current state to provided state i.e. '%s'", nextStatecode));
            }

            if (!procInstance.getCurrentActor().contains(userId) || !procInstance.getCalculatedActor().contains(userId))
                throw new CoreException(String.format("User '%s' is not in current actors for the instance '%s' of entity '%s'", userId, objectInstanceId, entity.getSystemName()));

            if (nextStatecode != null && nextStatecode.length() > 0) {
                List<String> users = (usersToAssigned == null ? new ArrayList<String>() : usersToAssigned);

                if (users.isEmpty())
                    users = procInstance.getCurrentActor();

                procInstance.setCurrentActor(users);
                procInstance.setCalculatedActor(users);

                if (procInstance.getCurrentActor().size() == 1)
                    TaskLoadManager.incrementUserWorkLoad(procInstance.getCurrentActor().get(0), MetaDataRegistry.getNonAbstractParentEntity(MetaDataRegistry.getMetaEntity(procInstance.getContext().getDocumentId())).getId());

                TaskContext ctx = new TaskContext(userId, taskCode, procInstance.getInstanceId(), activityNotes, nextStatecode);
                boolean isSuccess = procInstance.moveToState(nextStatecode, ctx);
                if (!isSuccess) {

                }
            } else {
                Transition transitionbyId = procInstance.getCurrentState().getTransitionbyId(taskCode);
                boolean isMannual = false;
                if (AssignmentType.fromName(transitionbyId.getAssignmentType()) == AssignmentType.Mannual) {
                    List<String> users = usersToAssigned;
                    procInstance.setCurrentActor(users);
                    procInstance.setCalculatedActor(users);
                    if (procInstance.getCurrentActor().size() == 1)
                        TaskLoadManager.incrementUserWorkLoad(procInstance.getCurrentActor().get(0), MetaDataRegistry.getNonAbstractParentEntity(MetaDataRegistry.getMetaEntity(procInstance.getContext().getDocumentId())).getId());
                    isMannual = true;
                }
                TaskContext ctx = new TaskContext(userId, taskCode, procInstance.getInstanceId(), activityNotes, null);
                boolean isSuccess = procInstance.moveToNextState(ctx, isMannual);
                if (!isSuccess) {

                }
            }

        }

    }

}
