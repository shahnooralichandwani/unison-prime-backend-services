/**
 *
 */
package com.avanza.core.document;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author shahbaz.ali
 *
 */
public class ERepositoryDocument extends Document {


    private String fromAddress;
    private String toAddress;
    private Date sentDate;
    private Date receivedDate;
    private Map<List, InputStream> attachments = new HashMap<List, InputStream>();


    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }


    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Map<List, InputStream> getAttachments() {
        return attachments;
    }

    public void setAttachments(Map<List, InputStream> attachments) {
        this.attachments = attachments;
    }

}
