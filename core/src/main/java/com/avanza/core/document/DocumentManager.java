package com.avanza.core.document;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import com.avanza.core.CoreException;
import com.avanza.core.main.ApplicationLoader;
import com.avanza.core.main.UnisonStartupLog;
import com.avanza.core.main.UnisonStartupLogDetail;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;

public class DocumentManager {

    private static RepositoryCatalog catalog = new RepositoryCatalog();
    private static final Logger logger = Logger.getLogger(DocumentManager.class);

    public static void load(ConfigSection section) {

        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_DOCUMENT_REPOSITORY);

        List<ConfigSection> repos = section
                .getChildSections("Repository");
        for (ConfigSection child : repos) {
            String id = child.getTextValue("id");
            String implClass = child.getTextValue("implClass");
            try {
                Constructor cons = Class.forName(implClass).getConstructor(ConfigSection.class);
                Repository repository = (Repository) cons.newInstance(child);
                catalog.addRepository(id, repository);
            } catch (Throwable e) {
                detail.setExceptionMessage(e.getMessage());
                detail.setStatus(UnisonStartupLog.FAIL);
                logger.logInfo("[Loading Failed Exception occoured]");
                logger.LogException("Exception Occoured: %1$s", e);
                continue;
            }
        }
        ApplicationLoader.log.addDetail(detail);
    }

    public static String saveDocument(Document doc) {
        return catalog.getDefaultRepository().saveDocument(doc);
    }

    public static String saveDocument(Document doc, String repoId) {
        String docId = "";
        try {
            docId = catalog.getRepository(repoId).saveDocument(doc);
        } catch (Exception e) {
            throw new CoreException(e, "No Such Repository Exists : " + repoId);
        }
        return docId;
    }

    public static Document retrieveDocument(String docId) {
        return catalog.getDefaultRepository().retrieveDocument(docId);
    }

    public static Document retrieveDocument(String docId, String repoId) {
        Document doc = null;
        try {
            doc = catalog.getRepository(repoId).retrieveDocument(docId);
        } catch (Exception e) {
            throw new CoreException(e, "No Such Repository : " + repoId);
        }
        return doc;
    }

    public static List<Document> retrieveDocument(String metaId,
                                                  String instanceId, String repoId) {
        List<Document> retList = new ArrayList<Document>(0);
        try {
            retList = catalog.getRepository(repoId).retrieveDocuments(
                    instanceId, metaId);
        } catch (Exception e) {
            throw new CoreException(e, "No Such Repository : " + repoId);
        }
        return retList;
    }

    public static RepositoryCatalog getCatalog() {
        return catalog;
    }

    public static String removeDocument(Document document, String repoId) {
        String docId = StringHelper.EMPTY;
        try {
            catalog.getRepository(repoId).removeDocument(document);
            if (StringHelper.isNotEmpty(document.getRepoId()))
                catalog.getRepository(document.getRepoId()).removeDocument(
                        document);
            docId = document.getDocumentId();
        } catch (Exception e) {
            throw new CoreException(e, "No Such Repository Exists : " + repoId);
        }
        return docId;
    }


    public static int getCountDocument(String repoId, String associatedId) {

        try {
            return catalog.getRepository(repoId).getCountDocuments(associatedId);

        } catch (Exception e) {
            throw new CoreException(e, "No Such Repository Exists : " + repoId);
        }

    }

    public static void shutdownRepositories() {
        catalog.shutdownRepositories();
    }

    public static List<Document> retrieveDocument(String[] ids, String repoId) {
        List<Document> retList = new ArrayList<Document>(0);
        try {
            retList = catalog.getRepository(repoId).retrieveDocuments(ids);
        } catch (Exception e) {
            throw new CoreException(e, "No Such Repository : " + repoId);
        }
        return retList;
    }

}
