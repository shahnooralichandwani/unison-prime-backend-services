package com.avanza.core.document;

import java.util.Date;
import java.util.List;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.ConvertUtil;


public class Document extends DbObject {

    private String documentId;
    private String absolutePath;
    private String repoId;
    private String documentTitle;
    private String documentFileName;
    private String documentType;
    private long documentSize;
    private String metaEntId;
    private String associatedObjId;
    private String documentpath;
    private byte[] doc;
    private String desc;

    private List<String> channelIdList;
    private boolean isFolder;
    private Document parent;
    private List<Document> childList;
    private String docGroup;
    private String eformType;
    private boolean submit;

    public boolean isSubmit() {
        return submit;
    }

    public void setSubmit(boolean isSubmit) {
        this.submit = isSubmit;
    }

    public Document() {

    }

    public Document(byte[] doc, String documentPath, String documentFileName, String documentId, long documentSize, String documentTitle, String documentType,
                    String repoId, String createdBy, Date createdOn, String updatedBy, Date updatedOn) {
        this.doc = doc;
        this.documentFileName = documentFileName;
        this.documentId = documentId;
        this.documentSize = documentSize;
        this.documentTitle = documentTitle;
        this.documentType = documentType;
        this.repoId = repoId;
        this.absolutePath = documentPath;
        this.documentpath = documentPath;
        this.setCreatedOn(createdOn);
        this.setCreatedBy(createdBy);
        this.setUpdatedOn(updatedOn);
        this.setUpdatedBy(updatedBy);
    }

    public Document(byte[] doc, String documentPath, String documentFileName, String documentId, long documentSize, String documentTitle, String documentType,
                    String repoId, String createdBy, Date createdOn, String updatedBy, Date updatedOn, boolean isSubmit) {
        this.doc = doc;
        this.documentFileName = documentFileName;
        this.documentId = documentId;
        this.documentSize = documentSize;
        this.documentTitle = documentTitle;
        this.documentType = documentType;
        this.repoId = repoId;
        this.absolutePath = documentPath;
        this.documentpath = documentPath;
        this.setCreatedOn(createdOn);
        this.setCreatedBy(createdBy);
        this.setUpdatedOn(updatedOn);
        this.setUpdatedBy(updatedBy);
        this.setSubmit(isSubmit);
    }

    public Document(byte[] doc, String documentPath, String documentFileName, String documentId, long documentSize, String documentTitle, String documentType,
                    String repoId, String createdBy, Date createdOn, String updatedBy, Date updatedOn, String desc) {
        this.doc = doc;
        this.documentFileName = documentFileName;
        this.documentId = documentId;
        this.documentSize = documentSize;
        this.documentTitle = documentTitle;
        this.documentType = documentType;
        this.repoId = repoId;
        this.absolutePath = documentPath;
        this.setCreatedOn(createdOn);
        this.setCreatedBy(createdBy);
        this.setUpdatedOn(updatedOn);
        this.setUpdatedBy(updatedBy);
        this.setDesc(desc);
    }

    public Document(byte[] doc, String documentPath, String documentFileName, String documentId, long documentSize, String documentTitle, String documentType,
                    String repoId, String createdBy, Date createdOn, String updatedBy, Date updatedOn, String desc, boolean isSubmit) {
        this.doc = doc;
        this.documentFileName = documentFileName;
        this.documentId = documentId;
        this.documentSize = documentSize;
        this.documentTitle = documentTitle;
        this.documentType = documentType;
        this.repoId = repoId;
        this.absolutePath = documentPath;
        this.setCreatedOn(createdOn);
        this.setCreatedBy(createdBy);
        this.setUpdatedOn(updatedOn);
        this.setUpdatedBy(updatedBy);
        this.setDesc(desc);
        this.setSubmit(isSubmit);
    }

    public Document(byte[] doc, String documentFileName, String documentId, long documentSize, String documentTitle, String documentType, String repoId) {
        this.doc = doc;
        this.documentFileName = documentFileName;
        this.documentId = documentId;
        this.documentSize = documentSize;
        this.documentTitle = documentTitle;
        this.documentType = documentType;
        this.repoId = repoId;
        this.setCreatedOn(new Date());
        this.setCreatedBy("System");
        this.setUpdatedOn(new Date());
        this.setUpdatedBy("System");
    }

    public Document(byte[] doc, String documentFileName, String documentPath, String documentId, long documentSize, String documentTitle, String documentType, String repoId) {
        this.doc = doc;
        this.documentFileName = documentFileName;
        this.documentpath = documentPath;
        this.documentId = documentId;
        this.documentSize = documentSize;
        this.documentTitle = documentTitle;
        this.documentType = documentType;
        this.repoId = repoId;
        this.setCreatedOn(new Date());
        this.setCreatedBy("System");
        this.setUpdatedOn(new Date());
        this.setUpdatedBy("System");
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public long getDocumentSize() {
        return documentSize;
    }

    public void setDocumentSize(long documentSize) {
        this.documentSize = documentSize;
    }

    public byte[] getDoc() {
        return doc;
    }

    public void setDoc(byte[] doc) {
        this.doc = doc;
    }


    public String getDocumentTitle() {
        return documentTitle;
    }


    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }


    public String getDocumentFileName() {
        return documentFileName;
    }


    public void setDocumentFileName(String documentFileName) {
        this.documentFileName = documentFileName;
    }


    public String getDocumentType() {
        return documentType;
    }


    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }


    public String getRepoId() {
        return repoId;
    }


    public void setRepoId(String repoId) {
        this.repoId = repoId;
    }


    public boolean isFolder() {
        return isFolder;
    }


    public void setFolder(boolean isFolder) {
        this.isFolder = isFolder;
    }


    public Document getParent() {
        return parent;
    }


    public void setParent(Document parent) {
        this.parent = parent;
    }


    public List<Document> getChildList() {
        return childList;
    }


    public void setChildList(List<Document> childList) {
        this.childList = childList;
    }


    public List<String> getChannelIdList() {
        return channelIdList;
    }


    public void setChannelIdList(List<String> channelIdList) {
        this.channelIdList = channelIdList;
    }


    public String getAbsolutePath() {
        return absolutePath;
    }


    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public String getDocumentpath() {
        return documentpath;
    }

    public void setDocumentpath(String documentpath) {
        this.documentpath = documentpath;
    }

    public String getMetaEntId() {
        return metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

    public String getAssociatedObjId() {
        return associatedObjId;
    }

    public void setAssociatedObjId(String associatedObjId) {
        this.associatedObjId = associatedObjId;
    }

    public String getCreatedDate() {
        return (this.getCreatedOn() == null) ? "" : ConvertUtil.toString(this.getCreatedOn());
    }

    public String getDesc() {
        return desc;
    }

    public String getShortDesc() {
        if (desc.length() > 20)
            return desc.substring(0, 20);

        else
            return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRespositoryPath() {

        FileSystemRepository tempRepo = ((FileSystemRepository) DocumentManager.getCatalog().getRepository(getRepoId()));
        return tempRepo.getConfPath() + tempRepo.getRootFolder();

    }

    public String getDocGroup() {
        return docGroup;
    }

    public void setDocGroup(String docGroup) {
        this.docGroup = docGroup;
    }

    public String getEformType() {
        return eformType;
    }

    public void setEformType(String eformType) {
        this.eformType = eformType;
    }

}
