package com.avanza.core.document.knowledgebase;

import java.util.Date;

import com.avanza.core.data.DbObject;
import com.avanza.core.meta.MetaEntity;


/**
 * Reference Case
 *
 * @author Muhammad Ali
 */

public class ReferenceCase extends DbObject {

    private static final long serialVersionUID = 3855295440191216390L;

    private String caseId;
    private MetaEntity metaEntity;
    private String metaEntityId;
    private String caseNotes;
    private String caseTitle;
    private String relatedDocId;
    private Date createdOn;
    private String createdBy;
    private Date updatedOn;
    private String updatedBy;

    public ReferenceCase() {
    }

    public ReferenceCase(String caseId, Date createdOn, String createdBy, Date updatedOn, String updatedBy) {
        this.caseId = caseId;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
    }

    public ReferenceCase(String caseId, MetaEntity metaEntity, String caseNotes, String caseTitle, String relatedDocId, Date createdOn,
                         String createdBy, Date updatedOn, String updatedBy) {
        this.caseId = caseId;
        this.metaEntity = metaEntity;
        this.caseNotes = caseNotes;
        this.caseTitle = caseTitle;
        this.relatedDocId = relatedDocId;
        this.createdOn = createdOn;
        this.createdBy = createdBy;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
    }


    public String getCaseId() {
        return this.caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public MetaEntity getMetaEntity() {
        return this.metaEntity;
    }

    public void setMetaEntity(MetaEntity metaEntity) {
        this.metaEntity = metaEntity;
    }

    public String getCaseNotes() {
        return this.caseNotes;
    }

    public void setCaseNotes(String caseNotes) {
        this.caseNotes = caseNotes;
    }

    public String getCaseTitle() {
        return this.caseTitle;
    }

    public void setCaseTitle(String caseTitle) {
        this.caseTitle = caseTitle;
    }

    public String getRelatedDocId() {
        return this.relatedDocId;
    }

    public void setRelatedDocId(String relatedDocId) {
        this.relatedDocId = relatedDocId;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUpdatedBy() {
        return this.updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

    public String getShortCaseNotes() {

        if (this.caseNotes.length() > 50)
            return this.caseNotes.substring(0, 49);

        else
            return this.caseNotes;
    }
}