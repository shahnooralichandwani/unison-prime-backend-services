package com.avanza.core.document;

import java.util.List;

public class AttachmentsHelper {

    public static void saveDocument(Document doc) {
        String filePath = DocumentManager.saveDocument(doc, doc.getRepoId());
        FileSystemRepository repository = (FileSystemRepository) DocumentManager.getCatalog().getRepository(
                doc.getRepoId());
        filePath = filePath.substring(repository.getConfPath().length());
        doc.setDoc(null);
        doc.setDocumentpath(filePath);
        DocumentManager.saveDocument(doc, "unisonDb");
    }

    public static List<Document> retrieveDocument(String metaEntityId, String instanceId) {
        return DocumentManager.retrieveDocument(metaEntityId, instanceId, "unisonDb");
    }

    public static String removeDocument(Document document) {
        return DocumentManager.removeDocument(document, "unisonDb");
    }
}
