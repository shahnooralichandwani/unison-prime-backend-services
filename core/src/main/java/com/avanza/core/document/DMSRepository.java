package com.avanza.core.document;

//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//
//import javax.faces.context.FacesContext;
//import javax.xml.ws.Holder;
//
//import com.avanza.core.constants.SessionKeyLookup;
//import com.avanza.core.security.db.UserDbImpl;
//import com.avanza.core.util.configuration.ConfigSection;
//import com.logicaldoc.webservice.document.DocumentService;
//import com.logicaldoc.webservice.document.WsDocument;
//import com.logicaldoc.webservice.folder.FolderService;
//import com.logicaldoc.webservice.folder.WsFolder;
//import com.org.dms.webserviceclient.WebServiceClientManager;
//
public class DMSRepository {
//	private static String sid;
//	WebServiceClientManager clientManager = new WebServiceClientManager(); 
//	
//	public DMSRepository(ConfigSection configSection) 
//	{
//	}
//	
//	private String getSid()
//	{
//		try
//		{
//			if(sid==null)
//			{
//				 sid = WebServiceClientManager.getLoginService().login("admin", "admin");
//			}
//			else
//			{
//				if(!WebServiceClientManager.getLoginService().valid(sid))
//					sid = WebServiceClientManager.getLoginService().login("admin", "admin");
//			}
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//			return null;
//		}
//		return sid;
//	}
//	
//	@Override
//	public void archiveDocument(String docId) 
//	{
//		
//	}
//
//	@Override
//	public int getCountDocuments(String associatedObjectId)
//	{
//		return 0;
//	}
//
//	@Override
//	public boolean getDefault()
//	{
//		return false;
//	}
//
//	@Override
//	public Document getDocumentHierarchy()
//	{
//		return null;
//	}
//
//	@Override
//	public String getRepoId()
//	{
//		return null;
//	}
//
//	@Override
//	public void removeDocument(Document doc)
//	{
//		try {
//			if (getSid() != null) 
//			{
//				System.out.println("Inilizing logicaldocs document delete service");
//				DocumentService documentService = clientManager.getDocumentService();
//				documentService.delete(sid, Long.parseLong(doc.getDocumentId()));
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@Override
//	public Document retrieveDocument(String docId)
//	{
//		Document unisonDoc = new Document();
//		try
//		{
//			DocumentService documentService = clientManager.getDocumentService();
//			WsDocument doc = documentService.getDocumentByCustomId(sid, docId);
//			byte[] content = documentService.getContent(sid, doc.getId());
//	
//			
//			unisonDoc.setDoc(content);
//			unisonDoc.setDocumentFileName(doc.getFileName());
//			unisonDoc.setDocumentId(doc.getCustomId());
//			return unisonDoc;
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	@Override
//	public List<Document> retrieveDocumentFromFileSystem(String folderPath)
//	{
//		return null;
//	}
//
//	@Override
//	public List<Document> retrieveDocuments(String instanceId, String metaId)
//	{
//		return null;
//	}
//
//	@Override
//	public List<Document> retrieveDocuments(Document document)
//	{
//		return null;
//	}
//
//	@Override
//	public String saveDocument(Document doc)
//	{
//		try
//		{
//			if(getSid()!=null)
//			{
//				System.out.println("Inilizing logicaldocs document creation service");
//				DocumentService documentService = clientManager.getDocumentService();
//				
//				WsDocument document = createDocument(sid, doc.getDocumentFileName(),doc.getDocGroup(),doc.getDesc(),doc.getDocumentId(),doc.getAssociatedObjId(),doc.getCreatedBy(),doc.getEformType());
//		        Holder holder=new Holder<WsDocument>();
//		        holder.value= document;
//		        documentService.create(sid, holder, doc.getDoc());
//				return doc.getDocumentId();
//			}
//			else
//			return null;
//		}
//		catch (Exception e) 
//		{
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public void shutdown()
//	{
//		
//	}
//	
//	private WsDocument createDocument(String sid,String fileName,String docType,String comments,String docId,String associatedObjId,String createdBy,String eformType) throws com.logicaldoc.webservice.folder.Exception_Exception{
//		Calendar cal = Calendar.getInstance();
//		int month = cal.get(Calendar.MONTH)+1;
//		int year = cal.get(Calendar.YEAR);
//		String folderName = month + "-" + year;
//		//String folderPathAndName = "/Unison/eforms/" + eformType + "/"
//		//		+ folderName;
//		
//		String folderPathAndName = "/Unison/eforms/" 
//		+ folderName;
//		
//		System.out.println("Adding to folder :"+folderPathAndName);
//		FolderService service = clientManager.getFolderService();
//		WsFolder folder;
//		WsDocument document = new WsDocument();
//		try {
//			UserDbImpl user = (UserDbImpl) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SessionKeyLookup.CURRENT_USER_KEY);
//			folder = service.findByPath(sid, folderPathAndName);
//			if (folder == null) {
//				System.out.println("Creating new folder");
//				folder = new WsFolder();
//				folder.setName(folderName);
//				Date date = new Date();
//				folder.setCreation(date.toString());
//				folder.setCreator(user==null?createdBy:user.getLoginId());
//				folder.setDescription(eformType + " Folder " + folderName);
//				WsFolder parent=service.findByPath(sid, "/Unison/eforms/");
//				folder.setParentId(parent.getId());
//				folder.setType(0);
//				Holder<WsFolder> holder = new Holder();
//				holder.value = folder;				
//				service.create(sid, holder);
//				folder = service.findByPath(sid, folderPathAndName);
//			}
//			document.setComment(comments);
//			document.setCreator(user==null?createdBy:user.getLoginId());
//			document.setDate(new Date().toString());
//			document.setFolderId(folder.getId());
//			document.setFileName(fileName);
//			document.setCustomId(docId);
//			document.setSourceId(associatedObjId);
//			document.getTags().add(docType);
//			document.getTags().add(eformType);
//			System.out.println("Folder and document created successfully on logicaldocs");
//		} catch (com.logicaldoc.webservice.folder.Exception_Exception e) {
//			e.printStackTrace();
//		}
//
//		return document;
//	}
//	
//	public List<Document> retrieveDocuments(String[] docIds) {
//
//		List<Document> unisonDocList = new ArrayList<Document>();
//		try {
//			if (getSid() != null) 
//			{
//				for (String Id : docIds) 
//				{
//					if(Id!=null)
//					{
//						DocumentService documentService = clientManager.getDocumentService();
//						WsDocument doc = documentService.getDocumentByCustomId(sid, Id);
//						byte[] content = documentService.getContent(sid, doc.getId());
//	
//						Document unisonDoc = new Document();
//						unisonDoc.setDoc(content);
//						unisonDoc.setDocumentFileName(doc.getFileName());
//						unisonDoc.setDocumentId(doc.getCustomId());
//						unisonDocList.add(unisonDoc);
//					}
//				}
//			}
//			return unisonDocList;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return unisonDocList;
//		}
//	}
//	
//
}
