package com.avanza.core.document;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.avanza.core.CoreException;
import com.avanza.core.constants.TemplateType;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;

public class FileSystemRepository implements Repository {

    private Logger logger = Logger.getLogger(FileSystemRepository.class);
    private String repoId;
    private String confPath;
    private String rootFolder;
    private boolean isDefault;
    private Document catalogue;
    private long maxSize;
    private String fileTypes;
    private int maxFileCount;

    public FileSystemRepository(String repoId, String confPath, String rootFolder, boolean isdefault) {
        this.repoId = repoId;
        this.confPath = confPath;
        this.isDefault = isdefault;
        this.rootFolder = rootFolder;
    }

    public FileSystemRepository(String repoId, String confPath, String rootFolder, boolean isdefault, long maxSize, String fileTypes, int maxFileCount) {

        this.repoId = repoId;
        this.confPath = confPath;
        this.isDefault = isdefault;
        this.rootFolder = rootFolder;
        this.maxSize = maxSize;
        this.fileTypes = fileTypes;
        this.maxFileCount = maxFileCount;

    }

    public FileSystemRepository(ConfigSection section) {
        this(section.getTextValue("id")
                , section.getTextValue("path")
                , section.getTextValue("rootFolder")
                , section.getBooleanValue("isdefault")
                , section.getLongValue("maxSizeinKb")
                , section.getTextValue("excludeFileTypes")
                , section.getIntValue("maxFileCount"));
    }

    public String getRepoId() {
        return repoId;
    }

    public boolean getDefault() {
        return false;
    }

    public Document retrieveDocument(String docId) {

        if (this.catalogue != null) {
            if (!this.catalogue.isFolder()) {
                if (this.catalogue.getDocumentId().equalsIgnoreCase(docId))
                    return this.catalogue;
            } else
                return getDocumentFromCatalogue(docId, this.catalogue.getChildList());

        }
        return null;
    }

    public List<Document> retrieveDocumentFromFileSystem(String folderPath) {
        List<Document> retList = new ArrayList<Document>(0);
        File folder = new File(confPath + rootFolder + File.separator + folderPath);
        if (folder.exists()) {
            File[] files = folder.listFiles();
            for (File file : files) {
                Document document = new Document();
                document.setDocumentSize(file.length());
                document.setDocumentTitle(file.getName());
                document.setDocumentFileName(file.getName());
                document.setDocumentSize(file.length());
                StringTokenizer tokenizer = new StringTokenizer(file.getName(), ".");
                String documenttype = null;
                while (tokenizer.hasMoreTokens()) {
                    documenttype = tokenizer.nextToken();
                }
                document.setDocumentType("." + documenttype);
                folderPath = folderPath.replaceAll("/", "//");
                document.setAbsolutePath("../../" + rootFolder + "//" + folderPath + "//" + file.getName());

                retList.add(document);
            }
        }
        return retList;
    }

    private Document getDocumentFromCatalogue(String docId, List<Document> docList) {
        Document retVal = null;
        for (Document document : docList) {
            if (!document.isFolder()) {
                if (document.getDocumentId().equalsIgnoreCase(docId))
                    return document;
            } else {
                retVal = getDocumentFromCatalogue(docId, document.getChildList());
                if (retVal != null)
                    return retVal;
            }

        }
        return null;
    }

    public String saveDocument(Document doc) {
        String path = confPath + rootFolder + "/";
        if (doc.getDocumentFileName().lastIndexOf("/") != -1)
            path += doc.getDocumentFileName().substring(0, doc.getDocumentFileName().lastIndexOf("/"));
        File folder = new File(path);
        folder.mkdirs();
        String filePath = path;
        if (doc.getDocumentFileName().lastIndexOf("/") != -1)
            filePath += doc.getDocumentFileName().substring(doc.getDocumentFileName().lastIndexOf("/"));
        else
            filePath += doc.getDocumentFileName();
        //filePath += doc.getAbsolutePath();
        File file = new File(filePath);
        FileOutputStream writer = null;
        try {
            file.createNewFile();
            writer = new FileOutputStream(file);
            writer.write(doc.getDoc());
            writer.close();
        } catch (IOException e) {
            throw new CoreException(e);
        }
        if (file.getAbsolutePath() != null) return doc.getAbsolutePath();
        return file.getAbsolutePath();
    }

    public String saveDocument(Document doc, String repoId) {
        return null;
    }

    public Document getDocumentHierarchy() {
        try {
            String path = confPath + rootFolder;
            File file = new File(path);
            if (!file.exists())
                file.mkdir();
            String[] folderList = file.list();
            /*
             * long fileSize=0; fileSize=this.getFolderSize(file,fileSize);
             *
             * if(this.catalogue!=null &&
             * this.catalogue.getDocumentSize()==fileSize) return
             * this.catalogue;
             */

            Document document = new Document();
            document.setDocumentId("Root");
            document.setDocumentSize(file.length());
            document.setFolder(true);
            document.setParent(null);
            document.setDocumentTitle(file.getName());
            document.setRepoId(repoId);
            List<Document> childList = new ArrayList<Document>();
            for (String folderName : folderList) {
                childList.add(getChild(document, path, path, folderName));
            }
            document.setChildList(childList);
            catalogue = document;
            // file.setReadable(true);
            // file.setWritable(true);
            return document;
        } catch (Throwable t) {
            throw new CoreException(t);
        }

    }

    /*
     * private long getFolderSize(File file,long fileSize){
     *
     * File[] filesList=file.listFiles();
     *
     * for(File currentFile:filesList){ if(currentFile.isFile())
     * fileSize=fileSize+currentFile.length(); else
     * fileSize=getFolderSize(currentFile, fileSize); }
     *
     * return fileSize; }
     */

    private Document getChild(Document parent, String confPath, String actualPath, String fileName) {

        File file = new File(actualPath + "\\" + fileName);
        // file.setReadable(true);
        // file.setWritable(true);
        Document document = new Document();
        if (file.isDirectory()) {
            File[] filesList = file.listFiles();
            Document parentDocument = new Document();
            parentDocument.setDocumentId(file.getAbsolutePath().substring(confPath.length(),
                    file.getAbsolutePath().length()));
            parentDocument.setDocumentSize(file.length());
            parentDocument.setParent(parent);
            parentDocument.setFolder(true);
            parentDocument.setDocumentTitle(file.getName());
            parentDocument.setRepoId(repoId);
            List<Document> childList = new ArrayList<Document>();

            for (File currentFile : filesList) {
                childList.add(getChild(parentDocument, confPath, file.getAbsolutePath(), currentFile.getName()));
            }
            parentDocument.setChildList(childList);
            return parentDocument;
        } else {
            List<String> channelIdlist = new ArrayList<String>();
            String documentTitle = "";
            document.setDocumentFileName(actualPath + "\\" + fileName);
            document
                    .setDocumentId(file.getAbsolutePath().substring(confPath.length(), file.getAbsolutePath().length()));
            document.setDocumentSize(file.length());
            document.setParent(parent);
            document.setFolder(false);

            document.setRepoId(repoId);
            setDocumentByteArray(document, file);

            StringTokenizer tokenizer = new StringTokenizer(file.getName(), ".");

            String documenttype = null;
            while (tokenizer.hasMoreTokens()) {
                documenttype = tokenizer.nextToken();
            }
            document.setDocumentType("." + documenttype);
            documentTitle = file.getName().replaceAll("." + documenttype, "");

            tokenizer = new StringTokenizer(documentTitle, "_");

            String channelId = null;
            while (tokenizer.hasMoreTokens()) {
                channelId = tokenizer.nextToken();
                if (channelId.equalsIgnoreCase(TemplateType.EMail.getValue())) {
                    channelIdlist.add(TemplateType.EMail.getValue());
                    documentTitle = documentTitle.replaceAll("_" + TemplateType.EMail.getValue(), "");
                }
                if (channelId.equalsIgnoreCase(TemplateType.Fax.getValue())) {
                    channelIdlist.add(TemplateType.Fax.getValue());
                    documentTitle = documentTitle.replaceAll("_" + TemplateType.Fax.getValue(), "");
                }
                if (channelId.equalsIgnoreCase(TemplateType.SMS.getValue())) {
                    channelIdlist.add(TemplateType.SMS.getValue());
                    documentTitle = documentTitle.replaceAll("_" + TemplateType.SMS.getValue(), "");
                }
            }
            //	if (channelIdlist.size() <= 0)
            //		channelIdlist.add(TemplateType.All.getValue());
            document.setChannelIdList(channelIdlist);
            document.setDocumentTitle(documentTitle);
            document.setAbsolutePath(File.separator + rootFolder + File.separator + file.getName());

        }
        return document;
    }

    private void setDocumentByteArray(Document document, File file) {

        try {
            InputStream is = new FileInputStream(file);
            long length = file.length();

            if (length > Integer.MAX_VALUE) {
                System.out.println("File is too large to process");
            }

            byte[] bytes = new byte[(int) length];

            int offset = 0;
            int numRead = 0;
            while ((offset < bytes.length) && ((numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)) {

                offset += numRead;

            }

            if (offset < bytes.length) {
                throw new IOException("Could not completely read file " + file.getName());
            }

            // document.setDoc(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(String rootFolder) {
        this.rootFolder = rootFolder;
    }

    public List<Document> retrieveDocuments(String instanceId, String metaId) {
        // TODO Auto-generated method stub
        return null;
    }

    public String getConfPath() {
        return confPath;
    }

    public void setConfPath(String confPath) {
        this.confPath = confPath;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public int getCountDocuments(String associatedObjectId) {

        return 0;
    }

    public void removeDocument(Document doc) {

        String path = StringHelper.EMPTY;
        String filePath = StringHelper.EMPTY;
        int index;
        if (doc.getAbsolutePath().startsWith("\\\\"))
            filePath = doc.getAbsolutePath();
        else {
            index = doc.getDocumentFileName().lastIndexOf("/");
            if (index > 0)
                path = confPath + File.separator + rootFolder + File.separator + doc.getDocumentFileName().substring(0, doc.getDocumentFileName().lastIndexOf("/"));
            else
                path = confPath + File.separator + rootFolder;

            if (index > 0)
                filePath = path + File.separator + doc.getDocumentFileName().substring(doc.getDocumentFileName().lastIndexOf("/"));
            else
                filePath = path + File.separator + doc.getDocumentFileName();

        }

        File file = new File(filePath);

        if (file.exists())
            file.delete();
    }

    public long getMaxSize() {
        return maxSize;
    }

    public String getExcludeFileTypes() {
        return fileTypes;
    }

    public void setMaxSize(long maxSize) {
        this.maxSize = maxSize;
    }

    public void setExcludeFileTypes(String fileTypes) {
        this.fileTypes = fileTypes;
    }

    public int getMaxFileCount() {
        return maxFileCount;
    }

    public void setMaxFileCount(int maxFileCount) {
        this.maxFileCount = maxFileCount;
    }

    public List<Document> retrieveDocuments(Document document) {
        // TODO Auto-generated method stub
        return null;
    }

    public void archiveDocument(String docId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void shutdown() {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Document> retrieveDocuments(String[] docIds) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Document updateDocument(Document document) {
        return document;

    }

    @Override
    public Document openDocument(String relativePath) {
        Document unisonDoc = new Document();
        try {
            File file = new File(relativePath);
            if (!file.exists()) {

                String filePath = relativePath.replace('/', '\\');
                file = new File(this.getConfPath() + this.getRootFolder() + "\\" + filePath);
                Path path = Paths.get(this.getConfPath() + this.getRootFolder() + "\\" + filePath);
                unisonDoc.setDoc(Files.readAllBytes(path));
                unisonDoc.setDocumentSize(file.length());
                unisonDoc.setDocumentFileName(file.getName());
                unisonDoc.setDocumentId(file.getParent());
                return unisonDoc;

            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

}
