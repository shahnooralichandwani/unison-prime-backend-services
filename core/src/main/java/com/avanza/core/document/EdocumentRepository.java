package com.avanza.core.document;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

//import javax.jcr.Credentials;
//import javax.jcr.ItemExistsException;
//import javax.jcr.LoginException;
//import javax.jcr.Node;
//import javax.jcr.NodeIterator;
//import javax.jcr.PathNotFoundException;
//import javax.jcr.Property;
//import javax.jcr.PropertyIterator;
//import javax.jcr.PropertyType;
//import javax.jcr.Repository;
//import javax.jcr.RepositoryException;
//import javax.jcr.Session;
//import javax.jcr.SimpleCredentials;
//import javax.jcr.Value;
//import javax.jcr.ValueFormatException;
//import javax.jcr.lock.LockException;
//import javax.jcr.nodetype.ConstraintViolationException;
//import javax.jcr.nodetype.NoSuchNodeTypeException;
//import javax.jcr.query.Query;
//import javax.jcr.query.QueryManager;
//import javax.jcr.query.QueryResult;
//import javax.jcr.version.VersionException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;

//import org.apache.jackrabbit.api.JackrabbitRepository;
//import org.apache.jackrabbit.core.RepositoryImpl;
//import org.apache.jackrabbit.core.config.RepositoryConfig;
//import org.apache.jackrabbit.core.value.DateValue;
//import org.apache.jackrabbit.core.value.StringValue;
import org.xml.sax.InputSource;

import com.avanza.core.CoreException;
import com.avanza.core.util.Guard;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;

public class EdocumentRepository {//[unisonprime] implements com.avanza.core.document.Repository {

/*[unisonprime]
	private static final Logger logger = Logger.getLogger(EdocumentRepository.class);
	private Map<String, String> properties= new HashMap<String, String>();
	  
	private String repoId;

	private Repository repository;
	private InitialContext jndiContext;
	 
	private String repoHome;

	// The repository's JNDI name
	private String repoName;

	private  String MESSAGE_UID = "UID";
	private  String MESSAGE_SUBJECT = "Subject";
	private  String MESSAGE_BODY = "MessageBody";
	private  String MESSAGE_TO = "Recipients";
	private  String MESSAGE_FROM = "Sender";
	private  String SENT_DATE = "SentOn";
	private  String RECEIVED_DATE = "ReceivedOn";

	// User ID and password to log into the repository
	private static final String USERID = "userid";
	private static final char[] PASSWORD = "".toCharArray();

	private String archiveRepositoryId;
	private String type;
	static Properties jndiEnv = new Properties();
	*/
/**
 * @param repoId
 *
 *//*

	
	static {
	   
	    jndiEnv.put(Context.PROVIDER_URL, "http://www.apache.org/jackrabbit");
	    jndiEnv.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.jackrabbit.core.jndi.provider.DummyInitialContextFactory");
	
	}
	public EdocumentRepository(
			String repoId,
			String type,
			String repoHome,
			String repoName,
			String archiveRepositoryId,
			Map<String, String> properties) {
		
		Guard.checkNullOrEmpty(repoId, "Repository Id");
		Guard.checkNullOrEmpty(type, "Repository Type");
		Guard.checkNullOrEmpty(repoHome, "Repository Home");
		Guard.checkNullOrEmpty(repoName, "Repository Name");
		Guard.checkNull(properties, "Repository Properties");
		
		
		this.type= type;
		this.repoHome = repoHome;
		this.repoName = repoName;
		this.archiveRepositoryId = archiveRepositoryId;
		this.repoId = repoId;
		this.properties= properties;
		
		// Shahbaz: initialize later when need to use
		initialize();
	}
	
	public EdocumentRepository(ConfigSection section) {
		
		this(section.getTextValue("id"),
				section.getTextValue("type"),
				section.getTextValue("path"),
				section.getTextValue("name"),
				section.getValue("archiveRepositoryId",""),
				StringHelper.getPropertiesMapFrom(section.getValue("properties","")));
	}

	private void initialize() {
		logger.logInfo("[Initializing Edocument Repository ( EdocumentRepository.initialize() )]");
		try {
		    startup();
		} catch (Exception e) {
		    logger.LogException("Exception initializing EdocumentRepository", e);
		}
		

	}

	public int getCountDocuments(String associatedObjectId) {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean getDefault() {
		// TODO Auto-generated method stub
		return false;
	}

	public Document getDocumentHierarchy() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getRepoId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void removeDocument(Document doc) {
		// TODO Auto-generated method stub

	}

	public Document retrieveDocument(String docId) {
		logger.logInfo("[Retreiving Edocument from Repository ( EdocumentRepository.retrieveDocument(String docId) ) ]");
		Session session = null;

		
		try {
		    	
			Repository repository = getRepositoryByJNDI();

			Credentials credentials = new SimpleCredentials(USERID,PASSWORD);
			session = repository.login(credentials);

			QueryManager qm = session.getWorkspace().getQueryManager();
			StringBuffer qstr = new StringBuffer("//"+type+"[@");
			qstr.append(MESSAGE_UID);
			qstr.append("='");
			qstr.append(docId);
			qstr.append("'");
			qstr.append("]");

			Query q = qm.createQuery(qstr.toString(), Query.XPATH);
			QueryResult result = q.execute();
			NodeIterator it = result.getNodes();

			if (it.hasNext()) {
				Node emailNode = it.nextNode();
				ERepositoryDocument document = new ERepositoryDocument();
				document.setDocumentId(docId);

				document.setDesc(emailNode.getProperty(MESSAGE_BODY).getString());
				document.setDocumentTitle(emailNode.getProperty(MESSAGE_SUBJECT).getString());
				document.setFromAddress(emailNode.getProperty(MESSAGE_FROM).getString());
				document.setToAddress(emailNode.getProperty(MESSAGE_TO).getString());

				if (emailNode.hasProperty(SENT_DATE))
					document.setSentDate(emailNode.getProperty(SENT_DATE).getDate().getTime());

				if (emailNode.hasProperty(RECEIVED_DATE))
					document.setReceivedDate(emailNode.getProperty(RECEIVED_DATE).getDate().getTime());

				NodeIterator iterator = emailNode.getNodes("Attachment*");

				while (iterator.hasNext()) {
					Node node = iterator.nextNode();
					InputStream iStream = node.getNode("jcr:content").getProperty("jcr:data").getStream();
					String attachName = node.getName().substring(("Attachment-").length());
					String contentType = node.getNode("jcr:content").getProperty("jcr:mimeType").getString();

					List<String> nametype = new ArrayList<String>(2);
					nametype.add(attachName);
					nametype.add(contentType);

					document.getAttachments().put(nametype, iStream);
				}

				return document;
			}

		} catch (LoginException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (ItemExistsException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (PathNotFoundException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (VersionException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (ConstraintViolationException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (LockException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (NoSuchNodeTypeException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (NamingException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (RepositoryException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (Exception e) {
		    logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		}

		finally {
			if (session != null) {
			    
				session.logout();
			}

		}
		return null;

	}

	public List<Document> retrieveDocumentFromFileSystem(String folderPath) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Document> retrieveDocuments(String instanceId, String metaId) {
		// TODO Auto-generated method stub
		return null;
	}

	public String saveDocument(Document doc) {

		logger.logInfo("[Saving Edocument in Repository ( EdocumentRepository.saveDocument() ) ]");

		if (!(doc instanceof ERepositoryDocument)) {
			throw new CoreException("[Object is not an instance of EDocument, ( EdocumentRepository.saveDocument) ]");
		}

		ERepositoryDocument edoc = (ERepositoryDocument) doc;

		try {
		    	
		    	Repository repository = getRepositoryByJNDI();
			saveinRepository(repository, edoc);
			// printAllEmails(session);
			
		} catch (LoginException e) {
			logger.LogException("Exception adding Edocument to the Repository", e);
		} catch (ItemExistsException e) {
			logger.LogException("Exception adding Edocument to the Repository", e);
		} catch (PathNotFoundException e) {
			logger.LogException("Exception adding Edocument to the Repository", e);
		} catch (VersionException e) {
			logger.LogException("Exception adding Edocument to the Repository", e);
		} catch (ConstraintViolationException e) {
			logger.LogException("Exception adding Edocument to the Repository", e);
		} catch (LockException e) {
			logger.LogException("Exception adding Edocument to the Repository", e);
		} catch (NoSuchNodeTypeException e) {
			logger.LogException("Exception adding Edocument to the Repository", e);
		} catch (NamingException e) {
			logger.LogException("Exception adding Edocument to the Repository", e);
		} catch (RepositoryException e) {
			logger.LogException("Exception adding Edocument to the Repository", e);
		} catch (Exception e) {
		    logger.LogException("Exception adding Edocument to the Repository", e);		}

		finally {
			 
		}
		return null;
	}

	private void saveinRepository(Repository repository,
			ERepositoryDocument edoc) throws LoginException,
			RepositoryException {
	    	
		Credentials credentials = new SimpleCredentials(USERID,PASSWORD);
		Session session = repository.login(credentials);

		Node rootNode = session.getRootNode();

		Node emailNode = rootNode.addNode(type);
		emailNode.setProperty(MESSAGE_UID, new StringValue(edoc.getDocumentId()));
		emailNode.setProperty(MESSAGE_SUBJECT, new StringValue(edoc.getDocumentTitle()));
		emailNode.setProperty(MESSAGE_BODY, new StringValue(edoc.getDesc()));
		emailNode.setProperty(MESSAGE_FROM, new StringValue(edoc.getFromAddress()));
		emailNode.setProperty(MESSAGE_TO, new StringValue(edoc.getToAddress()));

		if (edoc.getSentDate() != null) {
			Calendar senton = Calendar.getInstance();
			senton.setTime(edoc.getSentDate());
			emailNode.setProperty(SENT_DATE, new DateValue(senton));
		}

		if (edoc.getReceivedDate() != null) {
			Calendar senton = Calendar.getInstance();
			senton.setTime(edoc.getReceivedDate());
			emailNode.setProperty(RECEIVED_DATE, new DateValue(senton));
		}

		for (Map.Entry<List, InputStream> attachmentEntry : edoc.getAttachments().entrySet()) {

			List<String> nametype = attachmentEntry.getKey();
			InputStream stream = attachmentEntry.getValue();

			*/
    /*
     * Shahbaz
     * Jackrabbit issue: does not allow index boundries like [] hence replacing with () if any
     *//*

			String name = nametype.get(0).replace('[', '(').replace(']', ')');

			String type = nametype.get(1);
			Node attachment = emailNode.addNode("Attachment-" + name, "nt:file").addNode("jcr:content", "nt:resource");
			attachment.setProperty("jcr:data", stream);
			attachment.setProperty("jcr:mimeType", type);
			Calendar lastModified = Calendar.getInstance();
			attachment.setProperty("jcr:lastModified", lastModified);

		}
		if (session.hasPendingChanges())
			session.save();

		session.logout();

	}

	private void printAllEmails() {

		try {

			Session session = null;

			Repository repository = getRepositoryByJNDI();

			Credentials credentials = new SimpleCredentials(USERID,PASSWORD);
			session = repository.login(credentials);

			NodeIterator iterator = session.getRootNode().getNodes();

			while (iterator.hasNext()) {
				Node n = iterator.nextNode();
				PropertyIterator properties = n.getProperties();

				while (properties.hasNext()) {
					Property property = properties.nextProperty();
					if (property.getDefinition().isMultiple()) {
						// A multi-valued property, print all values
						Value[] values = property.getValues();
						for (int i = 0; i < values.length; i++) {
							System.out.println(property.getPath() + " = " + values[i].getString());
						}
					}
					else {
						// A single-valued property
						if (property.getType() == PropertyType.DATE) {
							System.out.println(property.getName() + " = " + property.getString());
						}
						else if (property.getType() == PropertyType.STRING) {
							System.out.println(property.getName() + " = " + (property.getLength() == -1 ? "(blank)" : property.getString()));
						}
					}

				}
			}

		} catch (ValueFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}

	}

	public List<Document> retrieveDocuments(Document document) {

		logger.logInfo("[Retreiving Emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ]");

		if (!(document instanceof ERepositoryDocument)) {
			throw new CoreException("[Object is not an instance of EDocument, ( EdocumentRepository.retrieveDocuments(Document document) ) ]");
		}

		ERepositoryDocument edoc = (ERepositoryDocument) document;
		Session session = null;
		List<Document> searchedEdocs = new ArrayList<Document>(0);

		try {
		    
			Repository repository = getRepositoryByJNDI();

			Credentials credentials = new SimpleCredentials(USERID,PASSWORD);
			session = repository.login(credentials);

			QueryManager qm = session.getWorkspace().getQueryManager();
			StringBuffer qstr = new StringBuffer("/jcr:root/"+type+"[");

			if (StringHelper.isNotEmpty(edoc.getDocumentTitle()))
				qstr.append("jcr:like(@" + MESSAGE_SUBJECT + ", '%" + edoc.getDocumentTitle() + "%')");

			if (StringHelper.isNotEmpty(edoc.getDesc())) {
				qstr.append(" and ");
				qstr.append("jcr:like(@" + MESSAGE_BODY + ", '%" + edoc.getDesc() + "%')");
			}

			if (StringHelper.isNotEmpty(edoc.getFromAddress())) {
				qstr.append(" and ");
				qstr.append("jcr:like(@" + MESSAGE_FROM + ", '%" + edoc.getFromAddress() + "%')");
			}

			if (StringHelper.isNotEmpty(edoc.getToAddress())) {
				qstr.append(" and ");
				qstr.append("jcr:like(@" + MESSAGE_TO + ", '%" + edoc.getToAddress() + "%')");
			}

			qstr.append("]");
			Query q = qm.createQuery(qstr.toString(), Query.XPATH);
			QueryResult result = q.execute();
			NodeIterator it = result.getNodes();

			while (it.hasNext()) {

				Node emailNode = it.nextNode();
				ERepositoryDocument temp = new ERepositoryDocument();
				temp.setDocumentId(emailNode.getProperty(MESSAGE_UID).getString());
				temp.setDesc(emailNode.getProperty(MESSAGE_BODY).getString());
				temp.setDocumentTitle(emailNode.getProperty(MESSAGE_SUBJECT).getString());
				temp.setFromAddress(emailNode.getProperty(MESSAGE_FROM).getString());
				temp.setToAddress(emailNode.getProperty(MESSAGE_TO).getString());

				if (emailNode.getProperties(SENT_DATE) != null)
					temp.setSentDate(emailNode.getProperty(SENT_DATE).getDate().getTime());

				if (emailNode.getProperties(RECEIVED_DATE) != null)
					temp.setReceivedDate(emailNode.getProperty(RECEIVED_DATE).getDate().getTime());

				NodeIterator iterator = emailNode.getNodes("Attachment*");

				while (iterator.hasNext()) {
					Node node = iterator.nextNode();
					InputStream iStream = node.getNode("jcr:content").getProperty("jcr:data").getStream();
					String attachName = node.getName().substring(("Attachment-").length());
					String contentType = node.getNode("jcr:content").getProperty("jcr:mimeType").toString();

					List<String> nametype = new ArrayList<String>(2);
					nametype.add(attachName);
					nametype.add(contentType);

					temp.getAttachments().put(nametype, iStream);
				}

				searchedEdocs.add(temp);
			}

		} catch (LoginException e) {
			logger.LogException("Exception searching emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ", e);
		} catch (ItemExistsException e) {
			logger.LogException("Exception searching emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ", e);
		} catch (PathNotFoundException e) {
			logger.LogException("Exception searching emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ", e);
		} catch (VersionException e) {
			logger.LogException("Exception searching emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ", e);
		} catch (ConstraintViolationException e) {
			logger.LogException("Exception searching emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ", e);
		} catch (LockException e) {
			logger.LogException("Exception searching emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ", e);
		} catch (NoSuchNodeTypeException e) {
			logger.LogException("Exception searching emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ", e);
		} catch (NamingException e) {
			logger.LogException("Exception searching emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ", e);
		} catch (RepositoryException e) {
			logger.LogException("Exception searching emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ", e);
		} catch (Exception e) {
		    logger.LogException("Exception searching emails from Repository ( EdocumentRepository.retrieveDocuments(Document document) ) ", e);		}

		finally {
			if (session != null) {
				session.logout();
			}
	 
		}

		logger.logInfo("[Returning " + searchedEdocs.size() + " Edocuments ( EdocumentRepository.retrieveDocuments(Document document) ) ]");
		return searchedEdocs;
	}

	public void archiveDocument(String docId) {

		Guard.checkNullOrEmpty(archiveRepositoryId, "Archive Repository Id");

		logger.logInfo("[Archiving Edocument from Repository ( EdocumentRepository.archiveDocument(String docId) ) ]");

		Session session = null; 

		try {
		   
			Repository repository = getRepositoryByJNDI();

			Credentials credentials = new SimpleCredentials(USERID,PASSWORD);
			session = repository.login(credentials);

			QueryManager qm = session.getWorkspace().getQueryManager();
			StringBuffer qstr = new StringBuffer("//"+type+"[@");
			qstr.append(MESSAGE_UID);
			qstr.append("='");
			qstr.append(docId);
			qstr.append("'");
			qstr.append("]");

			Query q = qm.createQuery(qstr.toString(), Query.XPATH);
			QueryResult result = q.execute();
			NodeIterator it = result.getNodes();

			if (it.hasNext()) {
				Node emailNode = it.nextNode();

				ERepositoryDocument document = new ERepositoryDocument();
				document.setDocumentId(docId);

				document.setDesc(emailNode.getProperty(MESSAGE_BODY).getString());
				document.setDocumentTitle(emailNode.getProperty(MESSAGE_SUBJECT).getString());
				document.setFromAddress(emailNode.getProperty(MESSAGE_FROM).getString());
				document.setToAddress(emailNode.getProperty(MESSAGE_TO).getString());

				if (emailNode.hasProperty(SENT_DATE))
					document.setSentDate(emailNode.getProperty(SENT_DATE).getDate().getTime());

				if (emailNode.hasProperty(RECEIVED_DATE))
					document.setReceivedDate(emailNode.getProperty(RECEIVED_DATE).getDate().getTime());

				NodeIterator iterator = emailNode.getNodes("Attachment*");

				while (iterator.hasNext()) {
					Node node = iterator.nextNode();
					InputStream iStream = node.getNode("jcr:content").getProperty("jcr:data").getStream();
					String attachName = node.getName().substring(("Attachment-").length());
					String contentType = node.getNode("jcr:content").getProperty("jcr:mimeType").getString();

					List<String> nametype = new ArrayList<String>(2);
					nametype.add(attachName);
					nametype.add(contentType);

					document.getAttachments().put(nametype, iStream);
				}

				saveinArchiveRepository(document);
				Node parent = emailNode.getParent();
				emailNode.remove();
				parent.save();

			}
			if (session.hasPendingChanges())
				session.save();

		} catch (LoginException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (ItemExistsException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (PathNotFoundException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (VersionException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (ConstraintViolationException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (LockException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (NoSuchNodeTypeException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (NamingException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (RepositoryException e) {
			logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		} catch (Exception e) {
		    logger.LogException("Exception retreiving Edocument from Edocument Repository", e);
		}

		finally {
			if (session != null) {
			    	
				session.logout();

			}
			 
		}

	}

	private void saveinArchiveRepository(Document document) {

		// Just saving the document in archive repository defined
		DocumentManager.saveDocument(document, this.archiveRepositoryId);
	}
	
	 
	 private Repository getRepositoryByJNDI() throws Exception {
	        
	       
	        // acquire via JNDI
	        String repositoryName = repoName;
	        InitialContext ctx = getInitialContext();
	        if (ctx == null) {
	            return null;
	        }
	        try {
	            Repository r = (Repository) ctx.lookup(repositoryName);
	            logger.logInfo("Acquired repository via JNDI.");
	            return r;
	        } catch (NamingException e) {
	            logger.logError("Error while retrieving repository using JNDI (name={})", repositoryName, e);
	            return null;
	        }
	    }
	 
	 private InitialContext getInitialContext() {
	       
	            // retrieve JNDI Context environment
	            try {
	                jndiContext = new InitialContext(jndiEnv);
	            } catch (NamingException e) {
	                logger.logError("Create initial context: " + e.toString());
	            }
	         
	        return jndiContext;
	    }

	 
	 private void initRepository() throws Exception {
	        // get repository config
	        File repHome;
	        try {
	            repHome = new File(repoHome).getCanonicalFile();
	        } catch (IOException e) {
	            throw new Exception(
	                    "Repository configuration failure: " +repoHome, e);
	        }
	        InputStream in = EdocumentRepository.class.getResourceAsStream(properties.get("configFilePath"));

	        try {
	            repository = createRepository(new InputSource(in), repHome);
	        } catch (RepositoryException e) {
	            throw new Exception("Error while creating repository", e);
	        }
	    }
	 
	 protected Repository createRepository(InputSource is, File homedir)  throws RepositoryException {
             RepositoryConfig config = RepositoryConfig.create(is, homedir.getAbsolutePath());
             return RepositoryImpl.create(config);
         }
	 
	
	public void startup() throws Exception {
	        if (repository != null) {
	            logger.logError("Startup: Repository already running.");
	            throw new ServletException("Repository already running.");
	        }
	        logger.logInfo("Repository initializing...");
	        try {
	            	URL jaasFile = EdocumentRepository.class.getResource("/jaas.config");
			System.setProperty("java.security.auth.login.config", jaasFile.getPath());
			
	                initRepository();
	                registerJNDI();
	            logger.logInfo("Repository initialized.");
	        } catch (Exception e) {
	            // shutdown repository
	            shutdownRepository();
	            logger.logError("Repository initializing failed: " + e, e);
	        }
	    }
	
	
	private void shutdownRepository() {
	      if(repository instanceof JackrabbitRepository)
	            ((JackrabbitRepository) repository).shutdown();
	      
	      else if(repository instanceof RepositoryImpl)
		  ((RepositoryImpl) repository).shutdown();

	      repository = null;
	    }

	private void registerJNDI() throws Exception {
	    
	    	try {
		    jndiContext = new InitialContext(jndiEnv);
		    jndiContext.bind(repoName, repository);
		    
		    logger.logInfo("Repository bound to JNDI with name: " + repoName);
		} catch (NamingException e) {
	                throw new Exception("Unable to bind repository using JNDI: " + repoName, e);
	            }
	}
	
	
	public void shutdown() {
	        if (repository == null) {
	            logger.logInfo("Shutdown: Repository "+repoId+" already stopped.");
	        } else {
	            logger.logInfo("Repository: "+repoId+" shutting down...");
	            shutdownRepository();
	            unregisterJNDI();
	            logger.logInfo("Repository: "+repoId+" shut down successfully.");
	        }
	    }
	
	private void unregisterJNDI() {
	        if (jndiContext != null) {
	            try {
	                jndiContext.unbind(repoName);
	            } catch (NamingException e) {
	                logger.logError("Error while unbinding repository from JNDI: " + e);
	            }
	        }
	    }
	
	public void restart() throws Exception {
	        if (repository != null) {
	            shutdown();
	        }
	        startup();
	    }
	
	
	 public void destroy() {
	        shutdown();
	    }

	@Override
	public List<Document> retrieveDocuments(String[] docIds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Document updateDocument(Document document) {
		// TODO Auto-generated method stub
		return document;
	}

	@Override
	public String getExcludeFileTypes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getMaxSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Document openDocument(String docId) {
		return retrieveDocument(docId);
	}
*/

}
