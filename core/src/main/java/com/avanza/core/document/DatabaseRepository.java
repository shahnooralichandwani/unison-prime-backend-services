package com.avanza.core.document;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.avanza.core.CoreException;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.db.DbConnection;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;

public class DatabaseRepository implements Repository {

    private static Logger logger = Logger.getLogger(DatabaseRepository.class);

    private static String INSERT_STATEMENT = "insert into DOCUMENT(DOCUMENT_ID,DOCUMENT,DOCUMENT_TITLE,DOCUMENT_FILENAME,DOCUMENT_TYPE,REPOSITORY_ID,SIZE_IN_BYTES,DOCUMENT_PATH,META_ENT_ID,ASSOCIATED_OBJECT_ID,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY, DESCRIPTION, IS_SUBMIT) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static String SELECT_STATEMENT = "select * from DOCUMENT where DOCUMENT_ID=?";
    private static String SELECT_STATEMENT1 = "select * from DOCUMENT where META_ENT_ID = ? and ASSOCIATED_OBJECT_ID = ?";
    private static String SELECT_STATEMENT2 = "select * from DOCUMENT where ASSOCIATED_OBJECT_ID = ?";
    private static String SELECT_STATEMENT3 = "select * from DOCUMENT where META_ENT_ID = ? and ASSOCIATED_OBJECT_ID IS NULL";
    private static String DELETE_STATEMENT = "DELETE FROM DOCUMENT WHERE DOCUMENT_PATH = ? AND META_ENT_ID = ? AND ASSOCIATED_OBJECT_ID = ?";
    private static String DELETE_STATEMENT1 = "DELETE FROM DOCUMENT WHERE DOCUMENT_ID=?";
    private static String UPDATE_STATEMENT = "UPDATE DOCUMENT SET IS_SUBMIT = 1 WHERE DOCUMENT_PATH = ? AND META_ENT_ID = ? AND ASSOCIATED_OBJECT_ID = ?";
    private static String UPDATE_STATEMENT1 = "UPDATE DOCUMENT SET IS_SUBMIT = 1 WHERE DOCUMENT_ID=?";
    private String repoId;
    private boolean isDefault;
    private String datasource;

    public DatabaseRepository(String datasource, boolean isdefault) {
        this.datasource = datasource;
        this.isDefault = isdefault;
        initRepository();
    }

    public DatabaseRepository(ConfigSection section) {
        this(section.getTextValue("datasource")
                , section.getBooleanValue("isdefault"));
    }

    private void initRepository() {
    }

    public String getRepoId() {

        return repoId;
    }

    public boolean getDefault() {
        return isDefault;
    }

    public String getDatasource() {
        return datasource;
    }

    public void setDatasource(String datasource) {
        this.datasource = datasource;
    }

    public void setDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public void setRepoId(String repoId) {
        this.repoId = repoId;
    }

    public Document retrieveDocument(String docId) {
        ResultSet resultSet = null;
        Document document = null;
        DbConnection dbconn = DataRepository.getDbConnection(this.datasource);
        try {
            PreparedStatement statement = dbconn.getConnection().prepareStatement(SELECT_STATEMENT);
            statement.setString(1, docId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String documentId = resultSet.getString("DOCUMENT_ID");
                byte[] doc = resultSet.getBytes("DOCUMENT");
                String documentTitle = resultSet.getString("DOCUMENT_TITLE");
                String documentFileName = resultSet.getString("DOCUMENT_FILENAME");
                String documentType = resultSet.getString("DOCUMENT_TYPE");
                String repoId = resultSet.getString("REPOSITORY_ID");
                String documentPath = resultSet.getString("DOCUMENT_PATH");
                String metEntId = resultSet.getString("META_ENT_ID");
                String ObjId = resultSet.getString("ASSOCIATED_OBJECT_ID");
                int documentSize = resultSet.getInt("SIZE_IN_BYTES");
                Date createdOn = resultSet.getDate("CREATED_ON");
                String createdBy = resultSet.getString("CREATED_BY");
                Date updatedOn = resultSet.getDate("UPDATED_ON");
                String updatedBy = resultSet.getString("UPDATED_BY");
                boolean isSubmit = resultSet.getBoolean("IS_SUBMIT");
                document = new Document(doc, documentPath, documentFileName, documentId, documentSize, documentTitle, documentType, repoId, createdBy, createdOn, updatedBy, updatedOn, isSubmit);
                document.setMetaEntId(metEntId);
                document.setAssociatedObjId(ObjId);
            }
        } catch (SQLException e) {
            logger.LogException("DatabaseRepository:Unable To Insert Document", e);
            throw new CoreException(e, "Unable To Insert Document");
        } finally {
            try {
                resultSet.close();
            } catch (SQLException se) {
            }
            dbconn.close();
        }
        return document;
    }

    public String saveDocument(Document doc) {
        DbConnection dbconn = DataRepository.getDbConnection(this.datasource);
        try {
            PreparedStatement statement = dbconn.getConnection().prepareStatement(INSERT_STATEMENT);
            statement.setString(1, doc.getDocumentId());
            statement.setBytes(2, doc.getDoc());
            statement.setString(3, doc.getDocumentTitle());
            statement.setString(4, doc.getDocumentFileName());
            statement.setString(5, doc.getDocumentType());
            statement.setString(6, doc.getRepoId());
            statement.setLong(7, doc.getDocumentSize());
            statement.setString(8, doc.getDocumentpath());
            statement.setString(9, doc.getMetaEntId());
            statement.setString(10, doc.getAssociatedObjId());
            statement.setDate(11, new Date(new java.util.Date().getTime()));
            statement.setString(12, "System");
            statement.setDate(13, new Date(new java.util.Date().getTime()));
            statement.setString(14, "System");
            statement.setString(15, doc.getDesc());
            statement.setBoolean(16, doc.isSubmit());
            statement.execute();
        } catch (SQLException e) {
            logger.LogException("DatabaseRepository:Unable To Insert Document", e);
            throw new CoreException(e, "Unable To Insert Document");
        } finally {
            dbconn.close();
        }
        return doc.getDocumentId();
    }

    public Document getDocumentHierarchy() {

        throw new CoreException("Document Hierarchy is not supported in Database Repository yet");
    }

    public List<Document> retrieveDocumentFromFileSystem(String folderPath) {
        return null;
    }

    public List<Document> retrieveDocuments(String instanceId, String metaId) {
        ResultSet resultSet = null;
        List<Document> documentList = new ArrayList<Document>(0);
        DbConnection dbconn = DataRepository.getDbConnection(this.datasource);
        try {
            PreparedStatement statement;
            if (StringHelper.isNotEmpty(instanceId)) {
                statement = dbconn.getConnection().prepareStatement(SELECT_STATEMENT1);
                statement.setString(1, metaId);
                statement.setString(2, instanceId);
            } else {
                statement = dbconn.getConnection().prepareStatement(SELECT_STATEMENT3);
                statement.setString(1, metaId);
            }
            resultSet = statement.executeQuery();
            while (resultSet.next()) {

                String documentId = resultSet.getString("DOCUMENT_ID");
                byte[] doc = resultSet.getBytes("DOCUMENT");
                String documentTitle = resultSet.getString("DOCUMENT_TITLE");
                String documentFileName = resultSet.getString("DOCUMENT_FILENAME");
                documentFileName = documentFileName.substring(documentFileName.lastIndexOf("/") + 1);
                String documentType = resultSet.getString("DOCUMENT_TYPE");
                String repoId = resultSet.getString("REPOSITORY_ID");
                String documentPath = resultSet.getString("DOCUMENT_PATH");
                String metEntId = resultSet.getString("META_ENT_ID");
                String ObjId = resultSet.getString("ASSOCIATED_OBJECT_ID");
                int documentSize = resultSet.getInt("SIZE_IN_BYTES");
                Date createdOn = resultSet.getDate("CREATED_ON");
                String createdBy = resultSet.getString("CREATED_BY");
                Date updatedOn = resultSet.getDate("UPDATED_ON");
                String updatedBy = resultSet.getString("UPDATED_BY");
                String desc = resultSet.getString("DESCRIPTION");
                boolean isSubmit = resultSet.getBoolean("IS_SUBMIT");
                Document docTemp = new Document(doc, documentPath, documentFileName, documentId, documentSize, documentTitle, documentType, repoId, createdBy, createdOn, updatedBy, updatedOn, desc, isSubmit);
                docTemp.setMetaEntId(metEntId);
                docTemp.setAssociatedObjId(ObjId);

                documentList.add(docTemp);
            }
        } catch (SQLException e) {
            logger.LogException("DatabaseRepository:Unable To Insert Document", e);
            throw new CoreException(e, "Unable To Insert Document");
        } finally {
            try {
                resultSet.close();
            } catch (SQLException se) {
            }
            dbconn.close();
        }
        return documentList;
    }

    public void removeDocument(Document doc) {

        PreparedStatement statement = null;
        int recordCount = 0;
        DbConnection dbconn = DataRepository.getDbConnection(this.datasource);
        try {
            if (StringHelper.isNotEmpty(doc.getAssociatedObjId())) {
                statement = dbconn.getConnection().prepareStatement(DELETE_STATEMENT);
                statement.setString(1, doc.getAbsolutePath());
                statement.setString(2, doc.getMetaEntId());
                statement.setString(3, doc.getAssociatedObjId());
            } else {
                statement = dbconn.getConnection().prepareStatement(DELETE_STATEMENT1);
                statement.setString(1, doc.getDocumentId());
            }
            recordCount = statement.executeUpdate();
        } catch (SQLException e) {
            logger.LogException("DatabaseRepository:Unable To Delete Document", e);
            throw new CoreException(e, "Unable To Delete Document");
        } finally {
            dbconn.close();
        }
    }

    public Document updateDocument(Document doc) {

        PreparedStatement statement = null;
        int recordCount = 0;
        DbConnection dbconn = DataRepository.getDbConnection(this.datasource);
        try {
            if (StringHelper.isNotEmpty(doc.getAssociatedObjId())) {
                statement = dbconn.getConnection().prepareStatement(UPDATE_STATEMENT);
                statement.setString(1, doc.getAbsolutePath());
                statement.setString(2, doc.getMetaEntId());
                statement.setString(3, doc.getAssociatedObjId());
            } else {
                statement = dbconn.getConnection().prepareStatement(UPDATE_STATEMENT1);
                statement.setString(1, doc.getDocumentId());
            }
            recordCount = statement.executeUpdate();
        } catch (SQLException e) {
            logger.LogException("DatabaseRepository:Unable To Update Document", e);
            throw new CoreException(e, "Unable To Update Document");
        } finally {
            dbconn.close();
        }
        return doc;
    }

    public int getCountDocuments(String associatedObjectId) {

        DbConnection dbconn = DataRepository.getDbConnection(this.datasource);
        int count = 0;
        try {
            PreparedStatement statement = dbconn.getConnection().prepareStatement(SELECT_STATEMENT2);
            statement.setString(1, associatedObjectId);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                count++;
            }


            return count;


        } catch (SQLException e) {
            logger.LogException("Unable to count Documents", e);
            throw new CoreException(e, "Unable to count Documents");
        } finally {

            dbconn.close();

        }

    }

    public List<Document> retrieveDocuments(Document document) {
        // TODO Auto-generated method stub
        return null;
    }

    public void archiveDocument(String docId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void shutdown() {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Document> retrieveDocuments(String[] docIds) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getExcludeFileTypes() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getMaxSize() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Document openDocument(String docId) {
        return retrieveDocument(docId);
    }
}
