package com.avanza.core.document;

import java.util.HashMap;


public class RepositoryCatalog {

    private static HashMap<String, Repository> repos = new HashMap<String, Repository>(0);

    public static void addRepository(String repoId, Repository repo) {
        repos.put(repoId, repo);
    }

    public Repository getRepository(String repoId) {
        if (repos.containsKey(repoId)) return repos.get(repoId);
        return getDefaultRepository();
    }

    public Repository getDefaultRepository() {
        for (Repository rpo : repos.values()) {
            if (rpo.getDefault()) return rpo;
        }
        return null;
    }

    public void shutdownRepositories() {
        for (Repository rpo : repos.values()) {
            rpo.shutdown();
        }
        repos.clear();
    }

    public Repository getRepositoryByKey(String repoId) {
        if (repos.containsKey(repoId)) return repos.get(repoId);
        return null;
    }
}
