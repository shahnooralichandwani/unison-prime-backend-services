/**
 *
 */
package com.avanza.core.document;

import java.util.ArrayList;
import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.unison.admin.template.DocumentTemplateRelation;
import com.avanza.unison.admin.template.NsTemplate;

/**
 * @author rehan.ahmed
 *
 */
public class DocumentTemplateManager {

    private DataBroker broker;


    public DocumentTemplateManager() {
        broker = DataRepository.getBroker(DocumentTemplateRelation.class.getName());
    }
//	public void getAllDocumentTemplates(){
//		try{
//			
//		}catch (Exception e) {
//			// TODO: handle exception
//			
//		}
//	}


    public List<NsTemplate> getTemplatesByDocumentType(String metaEntId) {
        try {
            List<DocumentTemplateRelation> listDocTemplates = null;
            List<NsTemplate> templateList = new ArrayList<NsTemplate>();
            if (!metaEntId.isEmpty()) {
                Search search = new Search(DocumentTemplateRelation.class);
                search.addCriterion(Criterion.equal("metaEntId", metaEntId));

                listDocTemplates = broker.find(search);

                for (DocumentTemplateRelation rel : listDocTemplates) {
                    search = new Search(NsTemplate.class);
                    search.addCriterion(Criterion.equal("templateId", rel.getTemplateId()));
                    NsTemplate sTemplate = (NsTemplate) broker.find(search).get(0);
                    sTemplate.setDefault(rel.getIsDefault());
                    templateList.add(sTemplate);
                }
                return templateList;
            }

        } catch (Exception e) {
            // TODO: handle exception;
            System.out.print("Exception: " + e.getMessage());
        }
        return null;
    }

    public NsTemplate getSelectedTemplate(String templateId) {
        try {
            Search search = new Search(NsTemplate.class);
            if (!templateId.isEmpty()) {
                search.addCriterion(Criterion.equal("templateId", templateId));
                List<NsTemplate> template = broker.find(search);
                if (template != null && template.size() > 0)
                    return template.get(0);
            }

        } catch (Exception e) {
            // TODO: handle exception
            System.out.print("Exception :" + e.getMessage());
        }
        return null;
    }


}
