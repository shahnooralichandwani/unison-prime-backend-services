package com.avanza.core.document;

import java.util.List;


public interface Repository {

    public String getRepoId();

    public boolean getDefault();

    public String saveDocument(Document doc);

    public Document retrieveDocument(String docId);

    public List<Document> retrieveDocuments(String instanceId, String metaId);

    public List<Document> retrieveDocumentFromFileSystem(String folderPath);

    public Document getDocumentHierarchy();

    public void removeDocument(Document doc);

    public int getCountDocuments(String associatedObjectId);

    public List<Document> retrieveDocuments(Document document);

    public void archiveDocument(String docId);

    public void shutdown();

    public List<Document> retrieveDocuments(String[] docIds);

    public Document updateDocument(Document document);

    public String getExcludeFileTypes();

    public long getMaxSize();

    public Document openDocument(String docId);
}
