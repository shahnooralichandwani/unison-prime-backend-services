package com.avanza.core.leaddetail;

import java.util.LinkedHashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.product.ProductType;

public class LeadDetail extends DbObject implements Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -8550951534847723086L;

    protected String leadDetailId;
    protected String leadTicketNum;
    protected String flag;
    protected String notesFlag;
    protected String notes;
    protected String loginUser;
    protected LeadDetail leadDetail;
    protected Set<LeadDetail> leadDetails = new LinkedHashSet<LeadDetail>(0);
    //private ValueTreeNode valueTreeNode;


    public LeadDetail() {
    }

    public LeadDetail getLeadDetail() {
        return this.leadDetail;
    }

    public void setLeadDetail(LeadDetail leadDetail) {
        this.leadDetail = leadDetail;
    }


    public String getLeadDetailId() {
        return this.leadDetailId;
    }

    public void setLeadDetailId(String leadDetailId) {
        this.leadDetailId = leadDetailId;
    }

    public String getLeadTicketNum() {
        return this.leadTicketNum;
    }

    public void setLeadTicketNum(String leadTicketNum) {
        this.leadTicketNum = leadTicketNum;
    }

    public String getLoginUser() {
        return this.loginUser;
    }

    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    public String getFlag() {
        return this.flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getNotesFlag() {
        return this.notesFlag;
    }

    public void setNotesFlag(String notesFlag) {
        this.notesFlag = notesFlag;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Set<LeadDetail> getLeadDetails() {
        return this.leadDetails;
    }

    public void setLeadDetails(Set<LeadDetail> leadDetails) {
        this.leadDetails = leadDetails;
    }



    /*
     * @Override protected Object clone() throws CloneNotSupportedException {
     * ProductType ptype = new ProductType(); ptype.setProductCode(productCode);
     * ptype.setDescription(description); ptype.setDisplayOrder(displayOrder);
     * ptype.setLevelId(levelId); ptype.setParentCode(parentCode);
     * ptype.setProduct(product); ptype.setProductNamePrm(productNamePrm);
     * ptype.setProductNameSec(productNameSec); ptype.setRootNodeId(rootNodeId);
     * ptype.setSystemName(systemName); return ptype; }
     */
}
