package com.avanza.core.util;

import com.avanza.core.CoreException;

import java.sql.Types;
import java.util.Date;


public enum DataType {

    None(0), Boolean(1), Integer(2), Long(3), Float(4), Double(5), Date(6), DateTime(7), String(8),
    Object(9), Char(10), Cursor(11);

    private int intValue;

    private DataType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static DataType fromString(String value) {

        DataType retVal = null;

        if (value.equalsIgnoreCase("None"))
            retVal = DataType.None;
        else if (value.equalsIgnoreCase("String"))
            retVal = DataType.String;
        else if (value.equalsIgnoreCase("Integer"))
            retVal = DataType.Integer;
        else if (value.equalsIgnoreCase("Date"))
            retVal = DataType.Date;
        else if (value.equalsIgnoreCase("DateTime"))
            retVal = DataType.DateTime;
        else if (value.equalsIgnoreCase("Double"))
            retVal = DataType.Double;
        else if (value.equalsIgnoreCase("Float"))
            retVal = DataType.Float;
        else if (value.equalsIgnoreCase("Long"))
            retVal = DataType.Long;
        else if (value.equalsIgnoreCase("Object"))
            retVal = DataType.Object;
        else if (value.equalsIgnoreCase("Boolean"))
            retVal = DataType.Boolean;
        else if (value.equalsIgnoreCase("Char"))
            retVal = DataType.Char;
        else if (value.equalsIgnoreCase("Char"))
            retVal = DataType.Char;
        else if (value.equalsIgnoreCase("Cursor"))
            retVal = DataType.Cursor;

        else
            throw new CoreException("[%1$s] is not recognized as DataType", value);

        return retVal;
    }

    public static DataType fromInteger(int value) {

        DataType retVal = null;

        if (value == 0)
            retVal = DataType.None;
        else if (value == 1)
            retVal = DataType.Boolean;
        else if (value == 2)
            retVal = DataType.Integer;
        else if (value == 3)
            retVal = DataType.Long;
        else if (value == 4)
            retVal = DataType.Float;
        else if (value == 5)
            retVal = DataType.Double;
        else if (value == 6)
            retVal = DataType.Date;
        else if (value == 7)
            retVal = DataType.DateTime;
        else if (value == 8)
            retVal = DataType.String;
        else if (value == 9)
            retVal = DataType.Object;
        else if (value == 10)
            retVal = DataType.Char;
        else if (value == 11)
            retVal = DataType.Cursor;

        else
            throw new CoreException("[%1$s] is not recognized as DataType", value);

        return retVal;
    }

    public static DataType fromJavaType(Class<?> clazz) {

        DataType retVal = null;

        if (clazz == boolean.class)
            return DataType.Boolean;
        else if (clazz == int.class)
            return DataType.Integer;
        else if (clazz == Integer.class)
            return DataType.Integer;
        else if (clazz == long.class)
            return DataType.Long;
        else if (clazz == float.class)
            return DataType.Float;
        else if (clazz == double.class)
            return DataType.Double;
        else if (clazz == Date.class)
            return DataType.DateTime;
        else if (clazz == String.class)
            return DataType.String;
        else if (clazz == char.class)
            return DataType.Char;
        else
            throw new CoreException("Java Type [%1$s] don't have equivalent DataType type", clazz.getName());
    }

    public boolean isSimple() {

        return (this == DataType.Boolean) || (this == DataType.Integer) || (this == DataType.Long) ||
                (this == DataType.Float) || (this == DataType.Double) || (this == DataType.Date) ||
                (this == DataType.DateTime) || (this == DataType.String) || (this == DataType.Char);
    }

    public boolean isComplex() {

        return !this.isSimple();
    }

    public Class<?> getJavaType() {

        Class<?> retVal = null;

        switch (this) {

            case Boolean:
                retVal = boolean.class;
                break;

            case Integer:
                retVal = int.class;
                break;

            case Long:
                retVal = long.class;
                break;

            case Float:
                retVal = float.class;
                break;

            case Double:
                retVal = double.class;
                break;

            case Date:
                retVal = Date.class;
                break;

            case DateTime:
                retVal = Date.class;
                break;

            case String:
                retVal = String.class;
                break;

            case Char:
                retVal = char.class;
                break;

            default:
                throw new CoreException("DataType [%1$s] don't have equivalent Java data type", this.toString());
        }

        return retVal;
    }

    public int getSqlType() {

        int retVal = 0;

        switch (this) {

            case Boolean:
                retVal = Types.BIT;
                break;

            case Integer:
                retVal = Types.INTEGER;
                break;

            case Long:
                retVal = Types.NUMERIC;
                break;

            case Float:
                retVal = Types.FLOAT;
                break;

            case Double:
                retVal = Types.DOUBLE;
                break;

            case Date:
                retVal = Types.DATE;
                break;

            case DateTime:
                retVal = Types.TIMESTAMP;
                break;

            case String:
                retVal = Types.VARCHAR;
                break;

            case Char:
                retVal = Types.CHAR;
                break;

            //TODO: UNISONPRIME
          /*  case Cursor:
                retVal = oracle.jdbc.driver.OracleTypes.CURSOR;
                break;*/

            default:
                throw new CoreException("DataType [%1$s] don't have equivalent Java data type", this.toString());
        }

        return retVal;
    }
}
