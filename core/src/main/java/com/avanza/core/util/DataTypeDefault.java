package com.avanza.core.util;

import java.util.Date;

public class DataTypeDefault {

    public static final char CHAR = '\0';
    public static final byte BYTE = 0;
    public static final short SHORT = 0;
    public static final int INTEGER = 0;
    public static final long LONG = 0;
    public static final float FLOAT = (float) 0.0;
    public static final double DOUBLE = 0.0;
    public static final String STRING = StringHelper.EMPTY;
    public static final Date DATE = new Date(0);
    public static final Object OBJECT = null;

}
