package com.avanza.core.util.reader;


public class CsvReader extends StreamReader {

    //private static final String DOUBLE_QUOTE = "\"\"";
    //private static final String SINGLE_QUOTE = "\"";
    public static final char ESC_CHAR = '"';
    public static final char END_REC_CHAR = '|';

    private char delimitChar;
    private char escChar;
    private char endRecChar;

    public CsvReader(char delimitChar) {

        super(false);
        this.init(delimitChar, CsvReader.ESC_CHAR, CsvReader.END_REC_CHAR);
    }

    public CsvReader(char delimitChar, boolean isTrim) {

        super(isTrim);
        this.init(delimitChar, CsvReader.ESC_CHAR, CsvReader.END_REC_CHAR);
    }

    private void init(char delimitChar, char escChar, char endRecChar) {

        this.delimitChar = delimitChar;
        this.escChar = escChar;
        this.endRecChar = endRecChar;
    }

    public char getDelimitChar() {

        return this.delimitChar;
    }

    public String readField() {

        boolean isQuoteStart = false;
        boolean isLastQuote = false;
        StringBuilder builder = new StringBuilder(1024);

        try {

            int tempChar = this.reader.read();

            while (tempChar != StreamReader.END) {

                char charRead = (char) tempChar;

                if (charRead == this.delimitChar) {

                    if (isQuoteStart) {

                        if (isLastQuote) {
                            isLastQuote = false;
                            isQuoteStart = false;
                            break;
                        }
                    } else
                        break;
                } else if (charRead == this.escChar) {

                    if (isQuoteStart) {

                        if (isLastQuote) {

                            builder.append(this.escChar);
                            isLastQuote = false;
                        } else
                            isLastQuote = true;
                    } else
                        isQuoteStart = true;
                } else if ((charRead == this.endRecChar) && (!isQuoteStart)) {

                    this.isEndRecord = true;
                    this.lineNumber++;
                    break;
                } else {
                    builder.append(charRead);
                }

                tempChar = this.reader.read();
            }

            this.currentField = builder.toString();
            if (this.isTrim)
                this.currentField = this.currentField.trim();

            this.recordData.add(this.currentField);

            return this.currentField;
        } catch (Exception e) {

            throw new StreamReaderException(e, "I/O Error occured while reading from stream. Err Msg: [%1$s]. Check exception stack for further details",
                    e.getMessage());
        }
    }
}
