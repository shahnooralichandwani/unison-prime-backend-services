package com.avanza.core.util.reader;

public enum ReadState {

    None(0), Initial(1), RecordStart(2), Field(3), Record(4), EndOfFile(5),
    Closed(6), Error(7);

    private int intValue;

    private ReadState(int intValue) {

        this.intValue = intValue;
    }

    public int getIntValue() {

        return this.intValue;
    }

    public static ReadState fromString(String value) {

        ReadState retVal;

        if (value.equalsIgnoreCase("None"))
            retVal = ReadState.None;
        else if (value.equalsIgnoreCase("Initial"))
            retVal = ReadState.Initial;
        else if (value.equalsIgnoreCase("RecordStart"))
            retVal = ReadState.RecordStart;
        else if (value.equalsIgnoreCase("Field"))
            retVal = ReadState.Field;
        else if (value.equalsIgnoreCase("Record"))
            retVal = ReadState.Record;
        else if (value.equalsIgnoreCase("EndOfFile"))
            retVal = ReadState.EndOfFile;
        else if (value.equalsIgnoreCase("Closed"))
            retVal = ReadState.Closed;
        else if (value.equalsIgnoreCase("Error"))
            retVal = ReadState.Error;
        else
            throw new StreamReaderException("[%1$s] is not recognized as valid Write State", value);

        return retVal;
    }

    public static ReadState fromInteger(int value) {

        ReadState retVal;

        switch (value) {

            case 0:
                retVal = ReadState.None;
                break;

            case 1:
                retVal = ReadState.Initial;
                break;

            case 2:
                retVal = ReadState.RecordStart;
                break;

            case 3:
                retVal = ReadState.Field;
                break;

            case 4:
                retVal = ReadState.Record;
                break;

            case 5:
                retVal = ReadState.EndOfFile;
                break;

            case 6:
                retVal = ReadState.Closed;
                break;

            case 7:
                retVal = ReadState.Error;
                break;

            default:
                throw new StreamReaderException("[%1$d] is not recognized as valid Write State", value);
        }

        return retVal;
    }
}
