package com.avanza.core.util.configuration;

import com.avanza.core.CoreException;


public class ConfigurationException extends CoreException {

    private static final long serialVersionUID = 2L;

    public ConfigurationException(String message) {

        super(message);
    }

    public ConfigurationException(String format, Object... args) {

        super(format, args);
    }

    public ConfigurationException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public ConfigurationException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public ConfigurationException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public ConfigurationException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}
