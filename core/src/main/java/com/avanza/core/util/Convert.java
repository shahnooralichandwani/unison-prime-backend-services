package com.avanza.core.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Convert {

    private static final String NOT_SUPPORTED_MSG = "Conversion of [%1$s] to %2$s is not supported. Data: %3$s";
    private static final String OUT_OF_RANGE_MSG = "Value [%1.3f] out of range for %2s. Must be between %3d & %4d";
    private static final String PARSE_ERR_MSG = "Unable to parse %1$s as %2s";
    private static final String EMPTY = "null";

    private Convert() {

    }

    public static List<String> toList(String commaSeperatedString) {

        List<String> result = new ArrayList<String>(0);
        StringTokenizer strTok = new StringTokenizer(commaSeperatedString, ",");
        while (strTok.hasMoreTokens()) {
            result.add(strTok.nextToken());
        }
        return result;
    }

    public static String toCSV(Collection<String> list) {

        StringBuilder builder = new StringBuilder(200);

        Iterator<String> itr = list.iterator();

        if (itr.hasNext()) builder.append(itr.next());

        while (itr.hasNext()) {
            builder.append(StringHelper.COMMA).append(itr.next());
        }

        return builder.toString();
    }

    public static boolean toBoolean(Object value) {

        if (value == null) return false;

        if (value instanceof Number) return Convert.toBoolean(((Number) value).longValue());

        if (value instanceof Boolean) return ((Boolean) value).booleanValue();

        if (value instanceof String) return Convert.toBoolean((String) value);

        if (value instanceof Character) return Convert.toBoolean(value.toString());

        throw new ConvertException(Convert.NOT_SUPPORTED_MSG, value.getClass().getName(), boolean.class.getSimpleName(), value.toString());
    }

    public static boolean toBoolean(char value) {

        return Convert.toBoolean(new Character(value).toString());
    }

    public static boolean toBoolean(String value) {

        boolean retVal;

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY))
            retVal = false;
        else if ((value.compareTo("1") == 0) || (value.compareToIgnoreCase("true") == 0) || (value.compareToIgnoreCase("y") == 0)
                || (value.compareToIgnoreCase("yes") == 0) || (value.compareToIgnoreCase("t") == 0)) {

            retVal = true;
        } else if ((value.compareTo("0") == 0) || (value.compareToIgnoreCase("false") == 0) || (value.compareToIgnoreCase("n") == 0)
                || (value.compareToIgnoreCase("no") == 0) || (value.compareToIgnoreCase("f") == 0)) {

            retVal = false;
        } else
            throw new ConvertException(Convert.PARSE_ERR_MSG, value, boolean.class.getSimpleName());

        return retVal;
    }

    public static boolean toBoolean(byte value) {

        if (value == 0) return false;

        return true;
    }

    public static boolean toBoolean(short value) {

        if (value == 0) return false;

        return true;
    }

    public static boolean toBoolean(int value) {

        if (value == 0) return false;

        return true;
    }

    public static boolean toBoolean(long value) {

        if (value == 0) return false;

        return true;
    }

    public static boolean toBoolean(float value) {

        if (value == 0.0) return false;

        return true;

    }

    public static boolean toBoolean(double value) {

        if (value == 0.0) return false;

        return true;
    }

    public static byte toByte(Object value) {

        if (value instanceof String) return Convert.toByte(((String) value));

        if (value instanceof Byte) return ((Byte) value).byteValue();

        if (value instanceof Number) {
            Number temp = (Number) value;
            return Convert.toByte(temp.doubleValue());
        }

        throw new ConvertException(Convert.NOT_SUPPORTED_MSG, value.getClass().getName(), byte.class.getSimpleName(), value.toString());
    }

    public static byte toByte(boolean value) {

        if (value) return 1;

        return 0;
    }

    public static byte toByte(char value) {

        return Convert.toByte(new Character(value).toString());
    }

    public static byte toByte(String value) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return DataTypeDefault.BYTE;

        return Convert.toByte(value, 10);
    }

    public static byte toByte(String value, int base) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return DataTypeDefault.BYTE;

        try {
            return Byte.parseByte(value, base);
        } catch (Exception ex) {
            throw new ConvertException(ex, Convert.PARSE_ERR_MSG, value, byte.class.getSimpleName());
        }

    }

    private static void checkByteRange(double value) {

        if ((value > Byte.MAX_VALUE) || (value < Byte.MIN_VALUE))
            throw new ConvertException(Convert.OUT_OF_RANGE_MSG, value, byte.class.getSimpleName(), Byte.MIN_VALUE, Byte.MAX_VALUE);
    }

    public static byte toByte(short value) {

        Convert.checkByteRange(value);
        return (byte) value;
    }

    public static byte toByte(int value) {

        Convert.checkByteRange(value);
        return (byte) value;
    }

    public static byte toByte(long value) {

        Convert.checkByteRange(value);
        return (byte) value;
    }

    public static byte toByte(float value) {

        Convert.checkByteRange(value);
        return (byte) value;
    }

    public static byte toByte(double value) {

        Convert.checkByteRange(value);
        return (byte) value;
    }

    public static short toShort(Object value) {

        if (value instanceof String) return Convert.toShort(((String) value));

        if (value instanceof Character) return Convert.toShort(value.toString());

        if (value instanceof Short) return ((Short) value).shortValue();

        if (value instanceof Number) {
            Number temp = (Number) value;
            return Convert.toShort(temp.doubleValue());
        }

        throw new ConvertException(Convert.NOT_SUPPORTED_MSG, value.getClass().getName(), short.class.getSimpleName(), value.toString());
    }

    public static short toShort(boolean value) {

        if (value) return 1;

        return 0;
    }

    public static short toShort(char value) {

        return Convert.toShort((new Character(value)).toString());
    }

    public static short toShort(String value) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return DataTypeDefault.SHORT;
        return Convert.toShort(value, 10);
    }

    public static short toShort(String value, int base) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return DataTypeDefault.SHORT;

        try {
            return Short.parseShort(value, base);
        } catch (Exception ex) {
            throw new ConvertException(ex, Convert.PARSE_ERR_MSG, value, short.class.getSimpleName());
        }
    }

    private static void checkShortRange(double value) {

        if ((value > Short.MAX_VALUE) || (value < Short.MIN_VALUE))
            throw new ConvertException(Convert.OUT_OF_RANGE_MSG, value, short.class.getSimpleName(), Short.MIN_VALUE, Short.MAX_VALUE);
    }

    public static short toShort(int value) {

        Convert.checkShortRange(value);
        return (short) value;
    }

    public static short toShort(long value) {

        Convert.checkShortRange(value);
        return (short) value;
    }

    public static short toShort(float value) {

        Convert.checkShortRange(value);
        return (short) value;
    }

    public static short toShort(double value) {

        Convert.checkShortRange(value);
        return (short) value;
    }

    public static int toInteger(Object value) {

        if (value == null) return 0;

        if (value instanceof String) return Convert.toInteger(((String) value));

        if (value instanceof Character) return Convert.toInteger(value.toString());

        if (value instanceof Integer) return ((Integer) value).intValue();

        if (value instanceof Number) {
            Number temp = (Number) value;
            return Convert.toInteger(temp.doubleValue());
        }

        throw new ConvertException(Convert.NOT_SUPPORTED_MSG, value.getClass().getName(), int.class.getSimpleName(), value.toString());
    }

    public static int toInteger(boolean value) {

        if (value) return 1;

        return 0;
    }

    public static int toInteger(char value) {
        return Convert.toInteger((new Character(value)).toString());
    }

    public static int toInteger(String value) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return DataTypeDefault.INTEGER;
        return Convert.toInteger(value, 10);
    }

    public static int toInteger(String value, int base) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return DataTypeDefault.INTEGER;

        try {
            return Integer.parseInt(value, base);
        } catch (Exception ex) {
            throw new ConvertException(ex, Convert.PARSE_ERR_MSG, value, int.class.getSimpleName());
        }
    }

    private static void checkIntegerRange(double value) {

        if ((value > Integer.MAX_VALUE) || (value < Integer.MIN_VALUE))
            throw new ConvertException(Convert.OUT_OF_RANGE_MSG, value, int.class.getSimpleName(), Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public static int toInteger(long value) {

        Convert.checkIntegerRange(value);
        return (int) value;
    }

    public static int toInteger(float value) {

        Convert.checkIntegerRange(value);
        return (int) value;
    }

    public static int toInteger(double value) {

        Convert.checkIntegerRange(value);
        return (int) value;
    }

    public static long toLong(Object value) {

        if (value instanceof String) return Convert.toLong(((String) value));

        if (value instanceof Character) return Convert.toLong(value.toString());

        if (value instanceof Date) return Convert.toLong(((Date) value));

        if (value instanceof Long) return ((Long) value).longValue();

        if (value instanceof Number) {
            Number temp = (Number) value;
            return Convert.toLong(temp.doubleValue());
        }

        throw new ConvertException(Convert.NOT_SUPPORTED_MSG, value.getClass().getName(), long.class.getSimpleName(), value.toString());
    }

    public static long toLong(char value) {

        return Convert.toLong((new Character(value).toString()));
    }

    public static long toLong(boolean value) {

        if (value) return 1;

        return 0;
    }

    public static long toLong(Date value) {
        return value.getTime();
    }

    public static long toLong(String value) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return DataTypeDefault.LONG;
        return Convert.toLong(value, 10);
    }

    public static long toLong(String value, int base) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return DataTypeDefault.LONG;

        try {
            return Long.parseLong(value, base);
        } catch (Exception ex) {
            throw new ConvertException(ex, Convert.PARSE_ERR_MSG, value, long.class.getSimpleName());
        }
    }

    private static void checkLongRange(double value) {

        if ((value > Long.MAX_VALUE) || (value < Long.MIN_VALUE))
            throw new ConvertException(Convert.OUT_OF_RANGE_MSG, value, long.class.getSimpleName(), Long.MIN_VALUE, Long.MAX_VALUE);
    }

    public static long toLong(float value) {

        Convert.checkLongRange(value);
        return (long) value;
    }

    public static long toLong(double value) {

        Convert.checkLongRange(value);
        return (long) value;
    }

    public static float toFloat(Object value) {

        if (value instanceof String) return Convert.toFloat(((String) value));

        if (value instanceof Character) return Convert.toFloat(value.toString());

        if (value instanceof Float) return ((Float) value).floatValue();

        if (value instanceof Number) {
            Number temp = (Number) value;
            return Convert.toFloat(temp.doubleValue());
        }

        throw new ConvertException(Convert.NOT_SUPPORTED_MSG, value.getClass().getName(), float.class.getSimpleName(), value.toString());
    }

    public static float toFloat(boolean value) {

        if (value) return 1;

        return 0;
    }

    public static float toFloat(char value) {

        return Convert.toFloat((new Character(value)).toString());
    }

    public static float toFloat(String value) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return DataTypeDefault.FLOAT;

        try {
            return Float.parseFloat(value);
        } catch (Exception e) {
            throw new ConvertException(e, Convert.PARSE_ERR_MSG, value, float.class.getSimpleName());
        }
    }

    private static void checkFloatRange(double value) {

        if ((value > Float.MAX_VALUE) || (value < Float.MIN_VALUE))
            throw new ConvertException(Convert.OUT_OF_RANGE_MSG, value, float.class.getSimpleName(), Float.MIN_VALUE, Float.MAX_VALUE);
    }

    public static float toFloat(double value) {

        Convert.checkFloatRange(value);
        return (float) value;
    }

    public static double toDouble(Object value) {

        if (value instanceof String) return Convert.toDouble(((String) value));

        if (value instanceof Character) return Convert.toDouble(value.toString());

        if (value instanceof Number) return ((Number) value).doubleValue();

        if (value instanceof Date) return Convert.toDouble((Date) value);

        throw new ConvertException(Convert.NOT_SUPPORTED_MSG, value.getClass().getName(), double.class.getSimpleName(), value.toString());

    }

    public static double toDouble(boolean value) {

        if (value) return 1.0;

        return 0.0;
    }

    public static double toDouble(Date value) {
        return Convert.toLong(value);
    }

    public static double toDouble(char value) {
        return Convert.toDouble((new Character(value)).toString());
    }

    public static double toDouble(String value) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return DataTypeDefault.DOUBLE;

        try {
            return new Double(value).doubleValue();

        } catch (Exception e) {
            throw new ConvertException(e, Convert.PARSE_ERR_MSG, value, double.class.getSimpleName());
        }
    }

    public static Date toDate(String value) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return null;

        DateFormat formater = new SimpleDateFormat(ConvertUtil.DATE_FORMAT);
        try {
            return formater.parse(value);
        } catch (Exception ex) {
            throw new ConvertException(ex, Convert.PARSE_ERR_MSG, value, Date.class.getSimpleName());
        }
    }

    public static Date toDate(String value, String format) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) return null;

        DateFormat formater = new SimpleDateFormat(format);

        try {
            return formater.parse(value);
        } catch (ParseException e) {
            throw new ConvertException(e, "Unable to parse %1$s as date. Format Used: %2$s", value, format);
        }
    }

    public static Date toDate(long value) {

        try {
            return new Date(value);
        } catch (Exception ex) {
            throw new ConvertException(ex, "Unable to convert Long value [%1$s] to date", value);
        }

    }

    public static Date toDate(double value) {
        return Convert.toDate(Convert.toLong(value));
    }

    public static Date toDate(Object value) {

        if (value == null)
            return null;

        if (value instanceof String) return Convert.toDate((String) value);

        if (value instanceof Date) return (Date) value;

        if (value instanceof Calendar) return ((Calendar) value).getTime();

        if (value instanceof Long) return Convert.toDate(((Long) value).longValue());

        if (value instanceof Double) return Convert.toDate(((Double) value).doubleValue());

        throw new ConvertException(Convert.NOT_SUPPORTED_MSG, value.getClass().getName(), Date.class.getSimpleName(), value.toString());
    }


    public static char toChar(String value) {

        if (StringHelper.isEmpty(value) || value.equalsIgnoreCase(EMPTY)) {
            Character temp = null;
            return temp;
        }

        return value.charAt(0);
    }

    public static char toChar(short value) {

        return (char) value;
    }

    public static char toChar(Object value) {

        if (value instanceof String) return Convert.toChar(((String) value));

        if (value instanceof Character) return (((Character) value).charValue());

        if (value instanceof Number) {
            short shortVal = Convert.toShort(value);
            return Convert.toChar(shortVal);
        }

        throw new ConvertException(Convert.NOT_SUPPORTED_MSG, value.getClass().getName(), char.class.getSimpleName(), value.toString());
    }

    public static <T> Object toType(String value, Class<T> type) {

        Object retVal;

        if (type == int.class || type == Integer.class)
            retVal = Convert.toInteger(value);
        else if (type == long.class || type == Long.class)
            retVal = Convert.toLong(value);
        else if (type == double.class || type == Double.class)
            retVal = Convert.toDouble(value);
        else if (type == float.class)
            retVal = Convert.toFloat(value);
        else if (type == Date.class)
            retVal = Convert.toDate(value);
        else if (type == boolean.class)
            retVal = Convert.toBoolean(value);
        else if (type == String.class)
            retVal = value;
        else
            throw new ConvertException("Conversion of [%1$s] is not supported. data: [%2$s]", type.getName(), value);

        return retVal;
    }

    public static String toDateString(Date date, String format) {
        DateFormat formater = new SimpleDateFormat(format);
        String retValue = "";
        try {
            retValue = formater.format(date);
        } catch (Exception e) {
            throw new ConvertException(Convert.PARSE_ERR_MSG);
        }
        return retValue;

    }

    static Object getNull(Class<? extends Object> type) {

        if (type == boolean.class) {
            Boolean retVal = null;
            return retVal;
        } else if (type == char.class) {
            Character retVal = null;
            return retVal;
        } else if (type == String.class) {
            String retVal = null;
            return retVal;
        } else if (type == Date.class) {
            Date retVal = null;
            return retVal;
        } else if (type == byte.class) {
            Byte retVal = null;
            return retVal;
        } else if (type == short.class) {
            Short retVal = null;
            return retVal;
        } else if (type == int.class) {
            Integer retVal = null;
            return retVal;
        } else if (type == long.class) {
            Long retVal = null;
            return retVal;
        } else if (type == float.class) {
            Float retVal = null;
            return retVal;
        } else if (type == double.class) {
            Double retVal = null;
            return retVal;
        } else if (type == Map.class) {
            Map retVal = null;
            return retVal;
        } else if (type == List.class) {
            List retVal = null;
            return retVal;
        } else if (type == Object.class) {
            Object retVal = null;
            return retVal;
        } else return null;
    }
}
