package com.avanza.core.util;

/**
 * it is the alternative to pass values by reference.
 *
 * @param <T>
 */
public class Wrapper<T> {

    T wrappedObject;

    public T unwrap() {
        return wrappedObject;
    }

    public void wrap(T wrappedObject) {
        this.wrappedObject = wrappedObject;
    }

    public Wrapper(T wrappedObject) {

        this.wrappedObject = wrappedObject;
    }

    public Wrapper() {
    }


}
