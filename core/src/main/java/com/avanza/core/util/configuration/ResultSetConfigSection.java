package com.avanza.core.util.configuration;

import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class ResultSetConfigSection implements ConfigSection {
    private static final Logger logger = Logger.getLogger(ResultSetConfigSection.class.getName());

    private Map<String, List<ConfigSection>> groupedSectionsMap = new HashMap<String, List<ConfigSection>>();
    private List<ConfigSection> sections = new ArrayList<ConfigSection>();
    private String tanentId;
    private String nodeId;

    /**
     * RowSet should be sorted by CONFIG_GROUP_ID.
     *
     * @param resultSet
     */
    public ResultSetConfigSection(ResultSet resultSet, String tanentId, String nodeId) {
        //System.out.println("ResultSetConfigSection :: ResultSetConfigSection ===> START");
        //System.out.println("ResultSetConfigSection :: ResultSetConfigSection ===> tanentId : " + tanentId + ", nodeId : " + nodeId);
        populateGroupwiseSections(resultSet);
        //System.out.println("ResultSetConfigSection :: ResultSetConfigSection ===> END");
        this.nodeId = nodeId;
        this.tanentId = tanentId;
    }

    public String getTanentId() {
        return tanentId;
    }

    public String getNodeId() {
        return nodeId;
    }

    private void populateGroupwiseSections(ResultSet resultSet) {
        try {
            //System.out.println("ResultSetConfigSection :: populateGroupwiseSections ===> START");
            String currentConfigGroup = "";
            List<ConfigSection> groupedSections = null;
            while (resultSet.next()) {
                String configGroup = resultSet.getString("CONFIG_GROUP_ID");
                //System.out.println("ResultSetConfigSection :: populateGroupwiseSections ===> configGroup : " + configGroup);
                if (!configGroup.equalsIgnoreCase(currentConfigGroup)) {
                    currentConfigGroup = configGroup;
                    groupedSections = new ArrayList<ConfigSection>();
                    if (groupedSectionsMap.containsKey(currentConfigGroup)) {
                        //System.out.println("ResultSetConfigSection :: populateGroupwiseSections ===> Throwing  RuntimeException...");
                        throw new RuntimeException("RowSet is not sorted by CONFIG_GROUP_ID");
                    } else {
                        //System.out.println("ResultSetConfigSection :: populateGroupwiseSections ===> putting currentConfigGroup : " + currentConfigGroup);
                        groupedSectionsMap.put(currentConfigGroup, groupedSections);
                    }
                }
                //System.out.println("ResultSetConfigSection :: populateGroupwiseSections ===> parsing XML");
                ConfigSection section = new XmlConfigSection(parseXml(resultSet));
                //System.out.println("ResultSetConfigSection :: populateGroupwiseSections ===> adding section : " + section.getValue());
                groupedSections.add(section);
                sections.add(section);
                //System.out.println("ResultSetConfigSection :: populateGroupwiseSections ===> Section Added : " + section.getValue());
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("ResultSetConfigSection :: populateGroupwiseSections ===> END with SQLException : " + e.getMessage());
            logger.LogException("", e);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ResultSetConfigSection :: populateGroupwiseSections ===> END with Exception : " + e.getMessage());
            logger.LogException("", e);
        }
    }

    @Override
    public ConfigSection getChild(String name) {
        // result set config is flat in nature.
        // we will ignore any nested calling, but list of sections.
        return this;
    }

    @Override
    public List<ConfigSection> getChildSections() {
        return sections;
    }

    @Override
    public boolean hasChild(String name) {

        return groupedSectionsMap.containsKey(name);
    }

    @Override
    public boolean hasChildSections() {
        return sections.size() > 0;
    }

    @Override
    public List<ConfigSection> getChildSections(String name) {
        if (groupedSectionsMap.containsKey(name)) {
            return groupedSectionsMap.get(name);
        }
        return new ArrayListWithNull<ConfigSection>();
    }

    private Element parseXml(ResultSet resultSet) {
        String instName = "";
        try {

            String xml = resultSet.getString("CONFIG_XML");
            instName = resultSet.getString("INSTANCE_NAME");

            if (instName.equals("timeLogger"))
                System.out.print("");
            logger.logInfo("Configuration Parsing for Instance %s", instName);

            // add params to xml
            String params = resultSet.getString("CONFIG_PARAMS");
            Map<String, String> paramsMap = StringHelper.getPropertiesMapFrom(params);
            for (String param : paramsMap.keySet()) {
                xml = xml.replace(String.format("#{%s}", param), paramsMap.get(param));
            }

            // add default params to xml
            params = resultSet.getString("DEFAULT_CONFIG_PARAMS");
            paramsMap = StringHelper.getPropertiesMapFrom(params);
            for (String param : paramsMap.keySet()) {
                xml = xml.replace(String.format("#{%s}", param), paramsMap.get(param));
            }

            // replace name place holder.
            xml = xml.replace("#{instName}", resultSet.getString("INSTANCE_NAME"));
            logger.logInfo(xml);
            DocumentBuilderFactory fctr = DocumentBuilderFactory.newInstance();
            DocumentBuilder bldr = fctr.newDocumentBuilder();
            InputStream inputStream = new ByteArrayInputStream(xml.getBytes());
            Document doc = bldr.parse(inputStream);
            return doc.getDocumentElement();
        } catch (Exception e) {
            logger.LogException("error while parsing xml", e);
        }

        return null;
    }

    @Override
    public boolean getBooleanValue(String name) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public char getCharValue(String name) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public Date getDateValue(String name) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public double getDoubleValue(String name) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public <T extends Enum<T>> T getEnumValue(String name, T defVal, Class<T> classtype) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public <T extends Enum<T>> T getEnumValue(String name, Class<T> classtype) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public float getFloatValue(String name) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public int getIntValue(String name) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public long getLongValue(String name) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String getName() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public short getShortValue(String name) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String getTextValue(String name) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String getValue() {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String getValue(String name, String defVal) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public short getValue(String name, short defVal) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public int getValue(String name, int defVal) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public long getValue(String name, long defVal) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public boolean getValue(String name, boolean defVal) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public double getValue(String name, double defVal) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public float getValue(String name, float defVal) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public char getValue(String name, char defVal) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public Date getValue(String name, Date defVal) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public boolean hasAttribute(String name) {
        throw new RuntimeException("Not implemented");
    }

    private class ArrayListWithNull<T> extends ArrayList<T> {
        private static final long serialVersionUID = 1L;

        public ArrayListWithNull() {
            super(0);
        }

        public T get(int i) {
            if (size() > i) {
                return get(1);
            }
            return null;
        }
    }

}
