package com.avanza.core.util;


import com.avanza.core.CoreException;
import com.avanza.core.FunctionDelegate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author Haris Mirza
 */

public class CollectionHelper {

    private CollectionHelper() {
    }

    /**
     * This method will create a map using
     *
     * @param propertyName as the key.
     * @param propertyName
     * @param set
     * @return map
     */
    public static Map convertToMap(Collection collection, String propertyName) {

        try {

            if (propertyName != null && !propertyName.equals("") && collection != null && !collection.isEmpty()) {
                // Get the getter property NAme
                propertyName = propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1, propertyName.length());

                Map map = new Hashtable();

                for (Object obj : collection) {

                    Method getProperty = obj.getClass().getMethod(("get" + propertyName), (new Class[0]));
                    Object returnParam = getProperty.invoke(obj);
                    map.put(returnParam, obj);
                }

                return map;
            } else {
                throw new ConvertException("Invalid Arguments Exception");
            }
        } catch (ConvertException ex) {
            throw new ConvertException("Exception occured in SetHelper while converting Set to Map");
        } catch (Exception ex) {
            throw new ConvertException("Exception occured in SetHelper while converting Set to Map");
        }
    }

    /**
     * This method will return an Object from Collection which has
     *
     * @param value         of
     * @param propertyName.
     * @param propertyName
     * @param set
     * @return Object
     */
    public static Object getObject(Collection collection, String propertyName, Object value) {

        try {

            if (propertyName != null && !propertyName.equals("") && collection != null && !collection.isEmpty()) {
                // Get the getter property NAme
                propertyName = propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1, propertyName.length());

                for (Object obj : collection) {

                    if (checkObject(obj, propertyName, value)) return obj;
                }

                return null;
            } else {
                throw new ConvertException("Invalid Arguments Exception");
            }
        } catch (ConvertException ex) {
            throw new ConvertException("Exception occured in SetHelper while converting Set to Map");
        } catch (Exception ex) {
            throw new ConvertException("Exception occured in SetHelper while converting Set to Map");
        }
    }

    /**
     * This method will return boolean depending upon whether an is present in Set which has
     *
     * @param value         of
     * @param propertyName.
     * @param propertyName
     * @param set
     * @return boolean
     */
    public static boolean contains(Collection collection, String propertyName, Object value) {
        if (getObject(collection, propertyName, value) != null) return true;

        return false;
    }

    public static HashSet getNativeHashSet(Set persistentSet) {
        HashSet result = new HashSet();
        Iterator itr = persistentSet.iterator();
        while (itr.hasNext()) {
            result.add(itr.next());
        }
        return result;
    }

    /**
     * This Function can be used for converting Data from one type of collection to another type of Collection
     *
     * @param <T          extends Collection>
     * @param collection
     * @param targetClazz
     * @return
     */
    public static <T extends Collection<T>> Collection<T> getCollection(Collection<T> collection, Class<T> targetClazz) {

        try {
            if (collection == null) return null;

            Collection<T> newCollection = targetClazz.newInstance();

            for (T obj : collection) {

                newCollection.add(obj);
            }

            return newCollection;

        } catch (InstantiationException e) {

            throw new ConvertException(e, "Exception occured in CollectionHelper while instantiating: " + targetClazz);

        } catch (IllegalAccessException e) {

            throw new ConvertException("Exception occured in CollectionHelper while instantiating: " + targetClazz);
        }
    }


    /**
     * This Functions helps to filter the collection according to the provided Map of Property Names and Values
     *
     * @param <T>
     * @param collection
     * @param criteria
     * @return
     */
    public static <T> Collection<T> filterCollection(Collection<T> collection, Map<String, Object> criteria) {

        if (criteria == null) return collection;
        if (criteria.isEmpty()) return collection;
        if (collection == null) return null;

        try {
            Collection<T> filteredColec = (Collection<T>) collection.getClass().newInstance();

            for (T item : collection) {
                if (checkObject(item, criteria))
                    filteredColec.add(item);
            }

            return filteredColec;
        } catch (CoreException e) {

            throw new CoreException("Exception occured in CollectionHelper while filtering collection: " + collection);
        } catch (Exception e) {

            throw new CoreException("Exception occured in CollectionHelper while filtering collection: " + collection);
        }
    }

    private static boolean checkObject(Object item, String propertyName, Object value)
            throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        if (propertyName == null || propertyName.equals("") || value == null)
            throw new CoreException("Invalid Filter Argument Exception");

        if (item instanceof Map) {

            return value.equals(((Map) item).get(propertyName));
        }
        propertyName = propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1, propertyName.length());
        Method getProperty = item.getClass().getMethod(("get" + propertyName), (new Class[0]));
        Object returnParam = getProperty.invoke(item);
        return returnParam.equals(value);
    }

    private static boolean checkObject(Object item, Map<String, Object> criteria)
            throws SecurityException, NoSuchMethodException,
            IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {

        if (criteria == null) return true;
        if (criteria.isEmpty()) return true;
        boolean flag = true;

        for (String propertyName : criteria.keySet()) {

            if (propertyName == null || propertyName.equals("") || criteria.get(propertyName) == null)
                throw new CoreException(
                        "Invalid Filter Argument Exception: [%1$s]",
                        propertyName);

            flag = checkObject(item, propertyName, criteria.get(propertyName));
            if (!flag) break;
        }
        return flag;
    }

    public static void setProperty(Object obj, String propertyName, Object value) {

        if (propertyName == null || propertyName.equals("") || obj == null)
            throw new CoreException("Invalid Filter Argument Exception");
        try {

            propertyName = propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1, propertyName.length());
            Method setProperty = obj.getClass().getMethod(("set" + propertyName), value.getClass());
            setProperty.invoke(obj, value);
        } catch (Exception e) {

            throw new CoreException(e, "Exception occured while calling setter [" + propertyName + "] of [" + obj.getClass()
                    + "] for value [" + value + "]");
        }
    }


    /**
     * @param <T>
     * @param list
     * @return
     */
    public static <T> Set<T> TrimDuplicate(List<T> list) {

        Set<T> userSet = new HashSet<T>();
        for (T obj : list) {
            userSet.add(obj);
        }

        return userSet;
    }

    public static <K, V> void mergeMap(Map<K, V> mergedMap, Map<K, V> map) {
        for (K key : map.keySet()) {
            mergedMap.put(key, map.get(key));
        }
    }

    public static <V> void mergeListToMap(Map<String, V> mergedMap, List<V> list) {
        for (V value : list) {
            mergedMap.put(value.toString(), value);
        }
    }


    /**
     * Return true if Any item of collection "findForCol" found in the collection "findInCol".
     *
     * @param findForCol
     * @param findInCol
     * @param ignoreCase
     * @return
     */
    public static boolean findAny(Collection<String> findForCol, Collection<String> findInCol, boolean ignoreCase) {
        for (String strFor : findForCol) {
            for (String strIn : findInCol) {
                boolean found;
                if (ignoreCase) {
                    found = strIn.equalsIgnoreCase(strFor);
                } else {
                    found = strIn.equals(strFor);
                }

                if (found) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return true if All items of collection "findForCol" found in the collection "findInCol".
     *
     * @param findForCol
     * @param findInCol
     * @param ignoreCase
     * @return
     */
    public static boolean findAll(Collection<String> findForCol, Collection<String> findInCol, boolean ignoreCase) {
        boolean found = true;
        for (String strFor : findForCol) {
            found = false;
            for (String strIn : findInCol) {
                if (ignoreCase) {
                    found = strIn.equalsIgnoreCase(strFor);
                } else {
                    found = strIn.equals(strFor);
                }

                if (found) {
                    break;
                }
            }

            if (!found) {
                return false;
            }
        }
        return found;
    }

    public static Collection<String> getSameItems(Collection<String> findForCol, Collection<String> findInCol, boolean ignoreCase) {
        boolean found = true;
        Collection<String> sameItems = new ArrayList<String>();
        for (String strFor : findForCol) {
            found = false;
            for (String strIn : findInCol) {
                if (ignoreCase) {
                    found = strIn.equalsIgnoreCase(strFor);
                } else {
                    found = strIn.equals(strFor);
                }

                if (found) {
                    break;
                }
            }

            if (found) {
                sameItems.add(strFor);
            }
        }
        return sameItems;
    }


    public static Collection<String> getMissingItems(Collection<String> findForCol, Collection<String> findInCol, boolean ignoreCase) {
        boolean found = true;
        Collection<String> missingItems = new ArrayList<String>();
        for (String strFor : findForCol) {
            found = false;
            for (String strIn : findInCol) {
                if (ignoreCase) {
                    found = strIn.equalsIgnoreCase(strFor);
                } else {
                    found = strIn.equals(strFor);
                }

                if (found) {
                    break;
                }
            }

            if (!found) {
                missingItems.add(strFor);
            }
        }
        return missingItems;
    }

    public static String getFirstMissingItem(Collection<String> findForCol, Collection<String> findInCol, boolean ignoreCase) {
        boolean found = true;
        Collection<String> missingItems = new ArrayList<String>();
        for (String strFor : findForCol) {
            found = false;
            for (String strIn : findInCol) {
                if (ignoreCase) {
                    found = strIn.equalsIgnoreCase(strFor);
                } else {
                    found = strIn.equals(strFor);
                }

                if (found) {
                    break;
                }
            }

            if (!found) {
                return strFor;
            }
        }
        return null;
    }

    /**
     * Return true if item  "findFor" found in the collection findInCol.
     *
     * @param findFor
     * @param findInCol
     * @param ignoreCase
     * @return
     */
    public static boolean find(String findFor, Collection<String> findInCol, boolean ignoreCase) {
        return findAny(Arrays.asList(findFor), findInCol, ignoreCase);

    }

    /**
     * It is used to transform the list items.
     * e-g to get the list customer Ids from the list of customers.
     */
    public static <RT, T> List<RT> transformList(Collection<T> list, FunctionDelegate.Arg1<RT, T> transformer) {
        List<RT> returnList = new ArrayList<RT>();
        for (T item : list) {
            RT returnValue = transformer.call(item);
            if (returnValue != null) {
                returnList.add(returnValue);
            }
        }
        return returnList;
    }

    public static <T> Collection<T> mergeList(Collection<T> mList, Collection<T> list) {
        for (T item : list) {
            mList.add(item);
        }
        return mList;
    }

    public static void main(String[] args) {
        List<String> lst1 = Arrays.asList("a", "y", "x");
        List<String> lst2 = Arrays.asList("t", "b", "d");
        Map<String, String> map = new HashMap<String, String>();
        map.put("t", "t");
        CollectionHelper.mergeListToMap(map, lst2);
        CollectionHelper.mergeListToMap(map, lst1);
        System.out.println(map);
    }


}
