package com.avanza.core.util.reader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class PositionMetaCollection implements java.util.Collection<PositionMetaInfo> {

    private static final String INVALID_CONF_MSG = "Invalid Position Configuration. Field %1$s overlaps with %2$s";

    private ArrayList<PositionMetaInfo> metaList;

    public PositionMetaCollection() {

        this.metaList = new ArrayList<PositionMetaInfo>(20);
    }

    public void add(int startPos, int length) {

        this.add(new PositionMetaInfo(startPos, length));
    }

    public boolean add(PositionMetaInfo info) {

        int startPos = info.getStartPos();
        int index = this.findIndex(startPos);

        if (index > 0) {

            PositionMetaInfo previous = this.metaList.get(index - 1);

            if ((previous.getStartPos() + previous.getLength()) > startPos)
                throw new StreamReaderException(PositionMetaCollection.INVALID_CONF_MSG,
                        info.toString(), previous.toString());
        }

        if (index < this.metaList.size()) {

            PositionMetaInfo next = this.metaList.get(index);

            if ((startPos + info.getLength()) > next.getStartPos())
                throw new StreamReaderException(PositionMetaCollection.INVALID_CONF_MSG,
                        info.toString(), next.toString());
        }

        this.metaList.add(index, info);
        return true;
    }

    public PositionMetaInfo get(int index) {

        return this.metaList.get(index);
    }

    public int size() {

        return this.metaList.size();
    }

    public PositionMetaInfo[] toArray() {

        return (PositionMetaInfo[]) this.metaList.toArray();
    }

    public boolean addAll(Collection<? extends PositionMetaInfo> c) {

        for (PositionMetaInfo item : c) {
            this.add(item);
        }

        return true;
    }

    public void clear() {

        this.clear();
    }

    public boolean contains(Object o) {

        return this.contains(o);
    }

    public boolean containsAll(Collection<?> c) {

        return this.metaList.containsAll(c);
    }

    public boolean isEmpty() {

        return this.metaList.isEmpty();
    }

    public Iterator<PositionMetaInfo> iterator() {

        return this.metaList.iterator();
    }

    public boolean remove(Object o) {

        return this.metaList.remove(o);
    }

    public boolean removeAll(Collection<?> c) {

        return this.metaList.removeAll(c);
    }

    public boolean retainAll(Collection<?> c) {

        throw new StreamReaderException("PositionMetaCollection.retainAll(Collection) not implemented");
    }

    public <T> T[] toArray(T[] a) {

        return this.metaList.toArray(a);
    }

    private int findIndex(int startPos) {

        int retVal = 0;

        for (; retVal < this.metaList.size(); retVal++) {

            if (this.metaList.get(retVal).getStartPos() >= startPos)
                break;
        }

        return retVal;
    }
}
