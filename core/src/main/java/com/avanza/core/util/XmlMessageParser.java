/**
 *
 */
package com.avanza.core.util;

import com.avanza.core.meta.messaging.MessageAttribute;
import com.avanza.core.meta.messaging.MessageAttributeType;
import com.avanza.core.meta.messaging.enums.MessageAttributeTypeKeys;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahbaz.ali
 *
 */
public class XmlMessageParser {

    NodeList nodes = null;
    Document document;

    private static DocumentBuilder documentBuilder = null;

    private DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
        synchronized (this) {
            if (documentBuilder == null)
                documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        }
        return documentBuilder;
    }

    private static XPath xPath = null;

    private XPath getXPath() {
        synchronized (this) {
            if (xPath == null)
                xPath = XPathFactory.newInstance().newXPath();
        }
        return xPath;
    }

    public XmlMessageParser(String message) throws SAXException, IOException, ParserConfigurationException {
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(message));
        document = getDocumentBuilder().parse(is);
    }

    public int getTagCount(String tagName) {
        return document.getElementsByTagName(tagName).getLength();
    }

    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
        StringBuffer sb = new StringBuffer();
        sb.append("<MIDWARECALL><HEADER><FUNCTIONID>0302</FUNCTIONID><EXTERNALHOSTID>IBM</EXTERNALHOSTID><XREF>WWW123123121</XREF></HEADER>")
                .append("<BODY><FCCACC><REPLY_ACCOUNT><XREF>2311080358092523</XREF><TXNFLAG>S</TXNFLAG><COUNT>3</COUNT>")
                .append("<ACCOUNTDETLS id=\"1\"><BRN>001</BRN><ACC>001520XXXXXXX01</ACC><CCY>AED</CCY><ADESC>MOHAMED1 NOURELDIN ABDALLA AND EETIMAD MAHMOUD ABBAS</ADESC></ACCOUNTDETLS>")
                .append("<ACCOUNTDETLS><BRN>002</BRN><ACC>001520XXXXXXX02</ACC><CCY>AED</CCY><ADESC>MOHAMED2 NOURELDIN ABDALLA AND EETIMAD MAHMOUD ABBAS</ADESC></ACCOUNTDETLS>")
                .append("<ACCOUNTDETLS><BRN>003</BRN><ACC>001520XXXXXXX03</ACC><CCY>AED</CCY><ADESC>MOHAMED3 NOURELDIN ABDALLA AND EETIMAD MAHMOUD ABBAS</ADESC></ACCOUNTDETLS>")
                .append("<SHASHKEY><HAIN><THEY>SHASHKEY</THEY></HAIN></SHASHKEY>")
                .append("</REPLY_ACCOUNT></FCCACC></BODY><ACCOUNTDETLS></ACCOUNTDETLS></MIDWARECALL>");
        XmlMessageParser xmlMessageParser = new XmlMessageParser(sb.toString());
        MessageAttribute mvc = new MessageAttribute();
        mvc.setAttributeName("MIDWARECALL");
        MessageAttribute body = new MessageAttribute();
        body.setAttributeName("BODY");
        body.setParent(mvc);
        MessageAttribute header = new MessageAttribute();
        header.setAttributeName("FCCACC");
        header.setParent(body);
        MessageAttribute funcId = new MessageAttribute();
        funcId.setAttributeName("REPLY_ACCOUNT");
        funcId.setParent(header);
        MessageAttribute acct = new MessageAttribute();
        MessageAttributeType messageAttributeType = new MessageAttributeType();
        messageAttributeType.setSystemName(MessageAttributeTypeKeys.WS_ATTRIBUTE.name());
        acct.setMessageAttributeType(messageAttributeType);
        acct.setAttributeName("ACCOUNTDETLS");
        acct.setParent(funcId);
        acct.setXMLAttributeName("id");
        MessageAttribute brn = new MessageAttribute();
        brn.setAttributeName("BRN");
        brn.setParent(acct);
        try {
            System.out.println(xmlMessageParser.getTagCount(acct));
            int count = xmlMessageParser.getTagCount(acct);
            for (int i = 0; i < count; i++)
                System.out.println(xmlMessageParser.getAttributeValue(brn, i));
        } catch (XPathExpressionException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
//		xmlMessageParser.getNode(mvc);
//		xmlMessageParser.getNode(header);
//		for(int i = 0; i < 3; i++){
//			Node n = xmlMessageParser.getNode(acct, i);
//			System.out.println(n == null ? "NULL" : n.getChildNodes().item(0).getTextContent());
//		}
        System.out.println(xmlMessageParser.generateXPath(brn));
        try {
            System.out.println(xmlMessageParser.getAttributeValue(acct));
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
    }

    public Node getNode(MessageAttribute messageAttribute, int position) {
        //if no parent is defined; then return root node
        if (messageAttribute.getParent() == null)
            return document.getElementsByTagName(messageAttribute.getAttributeName()).item(0);
        //call method recursively to get required node
        Node node = getNode(messageAttribute.getParent(), position);
        //if node returned is null, reutn null
        if (node == null)
            return null;
        //get nodes based on tag name
        NodeList childNodes = document.getElementsByTagName(messageAttribute.getAttributeName());
        if (messageAttribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.WS_REPEAT_NODE.name())) {
            //if its greater than 1, it means its a repeating block
            //create a temp list for those childNodes where parent is node
            List<Node> list = new ArrayList<Node>();
            for (int i = 0; i < childNodes.getLength(); i++) {
                if (childNodes.item(i).getParentNode() == node) {
                    list.add(childNodes.item(i));
                }
            }
            if (!list.isEmpty())
                return list.get(position);
        } else {
            for (int i = 0; i < childNodes.getLength(); i++) {
                if (childNodes.item(0).getParentNode() == node) {
                    return childNodes.item(0);
                }
            }
        }
        return null;
    }

    public Node getNode(MessageAttribute messageAttribute) {
        return getNode(messageAttribute, 0);
    }

    public String getNodeValue(MessageAttribute messageAttribute) {
        Node node = getNode(messageAttribute, 0);
        return null;
    }

    public String getNodeValue(MessageAttribute messageAttribute, int position) {
        Node node = getNode(messageAttribute, position);
        return null;
    }

    public int getTagCount(MessageAttribute messageAttribute) throws XPathExpressionException {
        String path = generateXPath(messageAttribute);
        return ((NodeList) getXPath().compile(path).evaluate(document, XPathConstants.NODESET)).getLength();
    }

    public String generateXPath(MessageAttribute messageAttribute) {
        if (messageAttribute.getParent() == null)
            return messageAttribute.getAttributeName();
        return generateXPath(messageAttribute.getParent()) + "/" + messageAttribute.getAttributeName();
    }

    public String getAttributeValue(MessageAttribute attribute, int position) throws XPathExpressionException {
        String path = generateXPath(attribute);
        NodeList list = (NodeList) getXPath().compile(path).evaluate(document, XPathConstants.NODESET);
        if (list.getLength() > position)
            //return list.item(position).getTextContent();
            return null;
        return path;
    }

    public String getAttributeValue(MessageAttribute attribute) throws XPathExpressionException {
        //get complete xpath based on tags i.e. attributes ignored
        String path = generateXPath(attribute);
        //add xml attribute of the attribute passed to the method
        if (attribute.getMessageAttributeType() != null &&
                attribute.getMessageAttributeType().getSystemName().equalsIgnoreCase(MessageAttributeTypeKeys.WS_ATTRIBUTE.name()))
            path += "/@" + attribute.getXMLAttributeName();
        return getXPath().compile(path).evaluate(document);
    }
}
