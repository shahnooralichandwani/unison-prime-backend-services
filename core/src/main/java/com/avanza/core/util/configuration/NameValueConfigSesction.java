package com.avanza.core.util.configuration;

import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;
import org.w3c.dom.NodeList;

import java.text.SimpleDateFormat;
import java.util.*;

public class NameValueConfigSesction implements ConfigSection {

    private HashMap<String, String> properties;
    private HashMap<String, HashMap> childList;
    private static final String PARSE_ERR_MSG = "Failed To Parse [%1$s] as [%2$s]. Section: [%3$s]";
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public NameValueConfigSesction() {
        properties = new HashMap<String, String>();
        childList = new HashMap<String, HashMap>();
    }


    private NodeList getNodeList(String name, boolean canThrow) {

        NodeList retVal = null;
        return retVal;
    }

    private String getAttribute(String name, boolean canThrow) {

        String retVal = this.properties.get(name);

        if (StringHelper.isEmpty(retVal)) {

            if (canThrow)
                throw new ConfigurationException("Failed to find [%1$s] in configuration section [%2$s]",
                        name, this.getName());
            else
                retVal = null;
        }

        return retVal;
    }

    public <T extends Enum<T>> T getEnumValue(String name, T defVal, Class<T> classType) {

        String attrib = this.getAttribute(name, false);

        try {

            if (attrib != null)
                defVal = Enum.valueOf(classType, attrib);
        } catch (Exception e) {
        }

        return defVal;
    }

    public <T extends Enum<T>> T getEnumValue(String name, Class<T> classType) {

        String attrib = this.getAttribute(name, true);

        try {

            return (T) Enum.valueOf(classType, attrib);
        } catch (RuntimeException e) {

            throw new ConfigurationException(e, NameValueConfigSesction.PARSE_ERR_MSG, name, classType.getName(), this.getName());
        }
    }


    public boolean getBooleanValue(String name) {

        String attrib = this.getAttribute(name, true);

        try {

            return Convert.toBoolean(attrib);
        } catch (RuntimeException e) {

            throw new ConfigurationException(e, NameValueConfigSesction.PARSE_ERR_MSG, name, "boolean", this.getName());
        }
    }

    public char getCharValue(String name) {

        String attrib = this.getAttribute(name, true);

        try {

            return attrib.charAt(0);
        } catch (Exception e) {

            throw new ConfigurationException(e, NameValueConfigSesction.PARSE_ERR_MSG, name, "Char", this.getName());
        }
    }

    public ConfigSection getChild(String name) {
        return null;
    }

    public List<ConfigSection> getChildSections() {
        ArrayList<ConfigSection> ElemsList = new ArrayList<ConfigSection>();
        return ElemsList;
    }

    private List<ConfigSection> getSectionList(NodeList list) {

        ArrayList<ConfigSection> ElemsList = new ArrayList<ConfigSection>();
        return ElemsList;
    }

    public List<ConfigSection> getChildSections(String name) {

        NodeList list = this.getNodeList(name, true);
        return this.getSectionList(list);
    }

    public Date getDateValue(String name) {

        String attrib = this.getAttribute(name, true);
        try {

            return dateFormatter.parse(attrib);
        } catch (Exception e) {

            throw new ConfigurationException(e, NameValueConfigSesction.PARSE_ERR_MSG, name, "Date", this.getName());
        }
    }

    public double getDoubleValue(String name) {

        String attrib = this.getAttribute(name, true);
        try {

            return Double.parseDouble(attrib);
        } catch (RuntimeException e) {

            throw new ConfigurationException(e, NameValueConfigSesction.PARSE_ERR_MSG, name, "Double", this.getName());
        }
    }

    public float getFloatValue(String name) {

        String attrib = this.getAttribute(name, true);
        try {

            return Float.parseFloat(attrib);
        } catch (RuntimeException e) {

            throw new ConfigurationException(e, NameValueConfigSesction.PARSE_ERR_MSG, name, "Float", this.getName());
        }
    }

    public int getIntValue(String name) {

        String attrib = this.getAttribute(name, true);
        try {

            return Integer.parseInt(attrib);
        } catch (RuntimeException e) {

            throw new ConfigurationException(e, NameValueConfigSesction.PARSE_ERR_MSG, name, "Integer", this.getName());
        }
    }

    public long getLongValue(String name) {

        String attrib = this.getAttribute(name, true);
        try {

            return Long.parseLong(attrib);
        } catch (RuntimeException e) {

            throw new ConfigurationException(e, NameValueConfigSesction.PARSE_ERR_MSG, name, "Long", this.getName());
        }
    }

    public String getName() {

        return "";
    }

    public short getShortValue(String name) throws ConfigurationException {

        String attrib = this.getAttribute(name, true);
        try {

            return Short.parseShort(attrib);
        } catch (RuntimeException e) {

            throw new ConfigurationException(e, NameValueConfigSesction.PARSE_ERR_MSG, name, "Short", this.getName());
        }
    }

    public String getTextValue(String name) {

        return this.getAttribute(name, true);
    }

    public String getValue() {

        return "";
    }

    public String getValue(String name, String defVal) {

        String attrib = this.getAttribute(name, false);

        if (attrib != null)
            defVal = attrib;

        return defVal;
    }

    public short getValue(String name, short defVal) {

        String attrib = this.getAttribute(name, false);
        try {

            if (attrib != null)
                defVal = Short.parseShort(attrib);
        } catch (Exception e) {
        }

        return defVal;
    }

    public int getValue(String name, int defVal) {

        String attrib = this.getAttribute(name, false);
        try {
            if (attrib != null)
                defVal = Integer.parseInt(attrib);
        } catch (Exception e) {
        }

        return defVal;
    }

    public long getValue(String name, long defVal) {

        String attrib = this.getAttribute(name, false);
        try {

            if (attrib != null)
                defVal = Long.parseLong(attrib);
        } catch (Exception e) {
        }
        return defVal;
    }

    public boolean getValue(String name, boolean defVal) {

        String attrib = this.getAttribute(name, false);
        try {

            if (attrib != null)
                defVal = Convert.toBoolean(attrib);
        } catch (RuntimeException e) {
        }

        return defVal;
    }

    public double getValue(String name, double defVal) {

        String attrib = this.getAttribute(name, false);
        try {

            if (attrib != null)
                defVal = Double.parseDouble(attrib);
        } catch (Exception e) {
        }
        return defVal;
    }

    public float getValue(String name, float defVal) {

        String attrib = this.getAttribute(name, false);
        try {

            if (attrib != null)
                defVal = Float.parseFloat(attrib);
        } catch (Exception e) {
        }

        return defVal;
    }

    public char getValue(String name, char defVal) {

        String attrib = this.getAttribute(name, false);
        try {

            if ((attrib != null) && attrib.length() > 0)
                defVal = attrib.charAt(0);
        } catch (Exception e) {
        }

        return defVal;
    }

    public Date getValue(String name, Date defVal) {

        String attrib = this.getAttribute(name, false);
        try {

            if (attrib != null)
                defVal = dateFormatter.parse(attrib);
        } catch (Exception e) {
        }

        return defVal;
    }

    public boolean hasAttribute(String name) {

        return this.properties.containsKey(name);
    }

    public boolean hasChild(String name) {

        return this.childList.containsKey(name);
    }

    public boolean hasChildSections() {

        return false;
    }

    public void put(String key, String value) {
        this.properties.put(key, value);
    }

    public void put(String key, HashMap value) {
        this.childList.put(key, value);
    }

    public String getValuesStr() {
        String retString = "";
        Iterator keyIterator = properties.keySet().iterator();
        Iterator valueIterator = properties.values().iterator();
        while (keyIterator.hasNext()) {
            retString += (String) keyIterator.next() + "=" + (String) valueIterator.next();
        }
        return retString;
    }

}
