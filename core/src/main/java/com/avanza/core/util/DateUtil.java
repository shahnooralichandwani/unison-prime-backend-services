/**
 *
 */
package com.avanza.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateUtil {

    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String SQL_DATE_FORMAT = "dd-MM-yyyy";
    public static final String DATETIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String SQL_DATETIME_FORMAT = "dd-MM-yy HH:mm:ss";
    public static final String SQL_TIME_FORMAT = "HH:mm:ss";

    /**
     * Returns a string representing the given date in the format DATE_FORMAT
     *
     * @param dtDate A {@link java.util.Date} object. The date to format
     * @return A string representing the input date in the format specified by DATE_FORMAT
     * @version 0.1
     */
    public static String dateToString(Date dtDate) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DateUtil.DATE_FORMAT);
        String sDate = dateFormatter.format(dtDate);

        return sDate;
    }

    public static String timeToString(Date dtDate) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DateUtil.SQL_TIME_FORMAT);
        String sDate = dateFormatter.format(dtDate);


        return sDate;
    }

    /**
     * Overloaded dateToString funciton which takes date and date format as param
     */

    public static String dateToString(Date dtDate, String dateFormat) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
        String sDate = dateFormatter.format(dtDate);

        return sDate;
    }

    /**
     * Returns a string representing the given date and time in the format DATETIME_FORMAT
     *
     * @param dtDate A {@link java.util.Date} object. The date to format
     * @return A string representing the input date in the format specified by DATE_FORMAT
     * @version 0.1
     */
    public static String datetimeToString(Date dtDate) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DateUtil.DATETIME_FORMAT);
        String sDate = dateFormatter.format(dtDate);

        return sDate;
    }

    /**
     * Parses a string formatted as specified by DATE_FORMAT and returns the corresponding
     * {@link java.util.Date} object.
     *
     * @param sDate A string in the format specified by DATE_FORMAT
     * @return A {@link java.util.Date} object. The date represented by the given string.
     * Returns null if the string is not in the correct format
     * @version 0.1
     */
    public static Date stringToDate(String sDate) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);

        Date dtDate = null;

        try {
            dtDate = dateFormatter.parse(sDate);
        } catch (ParseException e) {
            return null;
        }

        return dtDate;
    }

    /**
     * Parses a string formatted as specified by DATETIME_FORMAT and returns the corresponding
     * {@link java.util.Date} object.
     *
     * @param sDate A string in the format specified by DATETIME_FORMAT
     * @return A {@link java.util.Date} object. The date represented by the given string.
     * Returns null if the string is not in the correct format
     * @version 0.1
     */
    public static Date stringToDatetime(String sDate) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATETIME_FORMAT);

        Date dtDate = null;

        try {
            dtDate = dateFormatter.parse(sDate);
        } catch (ParseException e) {
            return null;
        }

        return dtDate;
    }

    /**
     * Overloaded function which takes Dateformat as Param
     */
    public static Date stringToDatetime(String sDate, String dateFormat) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
        Date dtDate = null;

        try {
            dtDate = dateFormatter.parse(sDate);
        } catch (Exception e) {
            return null;
        }

        return dtDate;
    }

    /**
     * Returns the next date to the given one
     *
     * @param sDate The {@link java.util.Date} to increment one day.
     * @return A {@link java.util.Date} object. The date incremented by one day
     * @version 0.1
     */
    public static Date getNextDate(Date dtDate) {
        Calendar calDate = Calendar.getInstance();

        calDate.setTime(dtDate);

        calDate.add(Calendar.DATE, 1);

        return calDate.getTime();
    }

    /**
     * Returns the previous date to the given one
     *
     * @param sDate The {@link java.util.Date} to decrement one day.
     * @return A {@link java.util.Date} object. The date decremented by one day
     * @version 0.1
     */
    public static Date getPreviousDate(Date dtDate) {
        Calendar calDate = Calendar.getInstance();

        calDate.setTime(dtDate);

        calDate.add(Calendar.DATE, -1);

        return calDate.getTime();
    }

    /**
     * Evaluates if the first given date is before the second one
     *
     * @param dtFirst A {@link java.util.Date} object.
     * @param dtSecond A {@link java.util.Date} object.
     * @return True if the first date is before second one. Otherwise false.
     * @version 0.1
     * @since 12/06/2001
     */
    public static boolean isBefore(Date dtFirst, Date dtSecond) {
        Calendar calFirst = Calendar.getInstance();
        Calendar calSecond = Calendar.getInstance();

        calFirst.setTime(dtFirst);
        calSecond.setTime(dtSecond);

        return calFirst.before(calSecond);
    }

    /**
     * Evaluates if the first given date is after the second one
     *
     * @param dtFirst A {@link java.util.Date} object.
     * @param dtSecond A {@link java.util.Date} object.
     * @return True if the first date is after second one. Otherwise false.
     * @version 0.1
     * @since 12/06/2001
     */
    public static boolean isAfter(Date dtFirst, Date dtSecond) {
        Calendar calFirst = Calendar.getInstance();
        Calendar calSecond = Calendar.getInstance();

        calFirst.setTime(dtFirst);
        calSecond.setTime(dtSecond);

        return calFirst.after(calSecond);
    }

    public static boolean isEqual(Date dtFirst, Date dtSecond) {
        Calendar calFirst = Calendar.getInstance();
        Calendar calSecond = Calendar.getInstance();
        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(stringToDate(getCurrentDate()));

        calFirst.setTime(dtFirst);
        calSecond.setTime(dtSecond);
        if (calFirst.equals(currentDate) && calSecond.equals(currentDate))
            return true;

        return false;
    }

    public static boolean isEqualDates(Date dtFirst, Date dtSecond) {
        Calendar calFirst = Calendar.getInstance();
        Calendar calSecond = Calendar.getInstance();

        calFirst.setTime(dtFirst);
        calFirst.set(Calendar.HOUR_OF_DAY, 0);
        calFirst.set(Calendar.MINUTE, 0);
        calFirst.set(Calendar.SECOND, 0);

        calSecond.setTime(dtSecond);
        calSecond.set(Calendar.HOUR_OF_DAY, 0);
        calSecond.set(Calendar.MINUTE, 0);
        calSecond.set(Calendar.SECOND, 0);

        if (calFirst.equals(calSecond))
            return true;

        return false;
    }

    /**
     * Returns the current time
     */
    public static String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        String timeValue = calendar.get(calendar.HOUR_OF_DAY) + ":" + calendar.get(calendar.MINUTE) + ":" + calendar.get(calendar.SECOND);
        return timeValue;

    }

    public static StringBuffer getCurrentIVRTransactionTime() {
        Calendar calendar = Calendar.getInstance();
        String timeValue = calendar.get(calendar.HOUR_OF_DAY) + ":" + calendar.get(calendar.MINUTE) + ":" + calendar.get(calendar.SECOND);
        String DattimeArray[] = timeValue.split(":");
        StringBuffer str = new StringBuffer();
        str.append(DattimeArray[0].length() < 2 ? "0" + DattimeArray[0] : DattimeArray[0]);
        str.append(DattimeArray[1].length() < 2 ? "0" + DattimeArray[1] : DattimeArray[1]);
        str.append(DattimeArray[2].length() < 2 ? "0" + DattimeArray[2] : DattimeArray[2]);
        System.out.println(str);

        return str;

    }

    /**
     * Returns the current Date
     */
    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        String dateValue = calendar.get(calendar.DAY_OF_MONTH) + "/" + (calendar.get(calendar.MONTH) + 1) + "/" + calendar.get(calendar.YEAR);
        return dateValue;
    }

    public static int getAddedTimeStamp() {
        Calendar calendar = Calendar.getInstance();
        int dateValue = calendar.get(calendar.YEAR) + (calendar.get(calendar.MONTH) + 1) + calendar.get(calendar.DAY_OF_MONTH);
        dateValue += calendar.get(calendar.HOUR) + calendar.get(calendar.MINUTE) + calendar.get(calendar.SECOND);
        return dateValue;
    }

    public static int getAddedTimeStampforThread() {
        Calendar calendar = Calendar.getInstance();
        int dateValue = calendar.get(calendar.DAY_OF_YEAR) * 1000;
        dateValue = dateValue + calendar.get(calendar.HOUR_OF_DAY * 100) + calendar.get(calendar.MINUTE * 10) + calendar.get(calendar.SECOND);
        return dateValue;
    }

/*
public static String getCurrentDateForATMBatch()
{
    Calendar calendar = Calendar.getInstance();
    String dateValue = calendar.get(calendar.YEAR)+""+Constants.formatRecordNumber((calendar.get(calendar.MONTH)+1),2)+""+Constants.formatRecordNumber(calendar.get(calendar.DAY_OF_MONTH),2);
    return dateValue;
}
*/

    public static String getFileNameStamp() {
        Calendar calendar = Calendar.getInstance();
        String dateValue = "" + calendar.get(calendar.DAY_OF_MONTH) + (calendar.get(calendar.MONTH) + 1) + calendar.get(calendar.YEAR);
        String timeValue = "" + calendar.get(calendar.HOUR) + calendar.get(calendar.MINUTE) + calendar.get(calendar.SECOND);
        return dateValue + timeValue;
    }

    public static String getSQLFormattedDate(Date date) {
        String strDate = "";
        strDate = "CONVERT(datetime,'" + datetimeToString(date) + "',103)";
        return strDate;
    }

    public static String getSQLFormattedDate(String sDate) {
        String strDate = "";
        strDate = "CONVERT(datetime,'" + sDate + "',103)";
        return strDate;
    }

    public static String getSQLFormattedDateTime(String sDateTime) {
        String strDate = "";
        strDate = "CONVERT(datetime,'" + sDateTime + "',131)";
        return strDate;
    }

    /**
     * Evaluates if the current date is between toDate and fromDate
     *
     * @param dtTo A {@link java.util.Date} object.
     * @param dtFrom A {@link java.util.Date} object.
     * @return True if the current date is between toDate and fromDate. Otherwise false.
     * @version 0.1
     * @since 14/10/2002
     */
    public static boolean isBetween(Date dtTo, Date dtFrom) {
        Date currentDate;
        boolean isAfter;
        boolean isBefore;
        boolean isEqual;

        currentDate = stringToDate(getCurrentDate());

        if (!(isEqual(currentDate, dtFrom))) {
            isBefore = isBefore(currentDate, dtFrom);
        } else {
            isBefore = true;
        }

        if (!(isEqual(currentDate, dtTo))) {
            isAfter = isAfter(currentDate, dtTo);
        } else {
            isAfter = true;
        }

        if ((isAfter) && (isBefore))
            return true;

        if (!isAfter && !isBefore)
            return true;

        return false;
    }

    public static boolean isBetweenTime(Date dtFirst, Date dtSecond) {
        boolean isTrue = true;
        Calendar today = Calendar.getInstance();
        Calendar calFirst = Calendar.getInstance();
        Calendar calSecond = Calendar.getInstance();

        calFirst.setTime(dtFirst);
        calSecond.setTime(dtSecond);
        // reseting Dates
        calFirst.set(calFirst.DAY_OF_MONTH, today.get(today.DAY_OF_MONTH));
        calFirst.set(calFirst.MONTH, today.get(today.MONTH));
        calFirst.set(calFirst.YEAR, today.get(today.YEAR));

        calSecond.set(calSecond.DAY_OF_MONTH, today.get(today.DAY_OF_MONTH));
        calSecond.set(calSecond.MONTH, today.get(today.MONTH));
        calSecond.set(calSecond.YEAR, today.get(today.YEAR));
        isTrue = isBetween(calFirst.getTime(), calSecond.getTime());

        ////System.out.println("The value of time is "+isTrue);
        return isTrue;

    }

    public static String checkDateBefore(Date date) {
        if (date == null)
            return "";

        else if (date.before(DateUtil.stringToDate("31/12/1900")))
            return "";

        return DateUtil.dateToString(date, DateUtil.DATE_FORMAT);
    }

    public static String checkDateBefore(String date) {
        if (date == null)
            return "";

        else if (stringToDate(date).before(DateUtil.stringToDate("31/12/1900")))
            return "";

        return DateUtil.dateToString(stringToDate(date), DateUtil.DATE_FORMAT);
    }

/*
public static String getCurrentDateTimeForPHX()
{
   Calendar calendar = Calendar.getInstance();
   String dateValue = calendar.get(calendar.YEAR)+""+Constants.formatRecordNumber((calendar.get(calendar.MONTH)+1),2)+""+Constants.formatRecordNumber(calendar.get(calendar.DAY_OF_MONTH),2);
   String timeValue = ""+Constants.formatRecordNumber(calendar.get(calendar.HOUR_OF_DAY),2)+Constants.formatRecordNumber(calendar.get(calendar.MINUTE),2)+Constants.formatRecordNumber(calendar.get(calendar.SECOND),2);
   return dateValue+timeValue;
}

public static String getCurrentDateForPHX()
{
   Calendar calendar = Calendar.getInstance();
   String dateValue = calendar.get(calendar.YEAR)+""+Constants.formatRecordNumber((calendar.get(calendar.MONTH)+1),2)+""+Constants.formatRecordNumber(calendar.get(calendar.DAY_OF_MONTH),2);
   return dateValue;
}

public static String getCurrentTimeForPHX()
{
   Calendar calendar = Calendar.getInstance();
   String timeValue = ""+Constants.formatRecordNumber(calendar.get(calendar.HOUR_OF_DAY),2)+Constants.formatRecordNumber(calendar.get(calendar.MINUTE),2)+Constants.formatRecordNumber(calendar.get(calendar.SECOND),2);
   return timeValue;
}
public static String getCurrentDateTimeForRDZ()
{
	  Calendar calendar = Calendar.getInstance();
	  String dateValue = calendar.get(calendar.YEAR)+""+Constants.formatRecordNumber((calendar.get(calendar.MONTH)+1),2)+""+Constants.formatRecordNumber(calendar.get(calendar.DAY_OF_MONTH),2);
	  String timeValue = ""+Constants.formatRecordNumber(calendar.get(calendar.HOUR_OF_DAY),2)+Constants.formatRecordNumber(calendar.get(calendar.MINUTE),2)+Constants.formatRecordNumber(calendar.get(calendar.SECOND),2);
	  return dateValue+timeValue;
}


public static String getCurrentDateForRDZ()
{
	  Calendar calendar = Calendar.getInstance();
	  String dateValue = calendar.get(calendar.YEAR)+""+Constants.formatRecordNumber((calendar.get(calendar.MONTH)+1),2)+""+Constants.formatRecordNumber(calendar.get(calendar.DAY_OF_MONTH),2);
	  return dateValue;
}

public static String getCurrentTimeForRDZ()
{
	  Calendar calendar = Calendar.getInstance();
	  String timeValue = ""+Constants.formatRecordNumber(calendar.get(calendar.HOUR_OF_DAY),2)+Constants.formatRecordNumber(calendar.get(calendar.MINUTE),2)+Constants.formatRecordNumber(calendar.get(calendar.SECOND),2);
	  return timeValue;
}
*/

    public static int convertTimetoSeconds(Date dt) {
        int TotalSeconds = 0;
        int iHour = new Integer(dt.toString().substring(0, 2)).intValue();
        int iMinutes = new Integer(dt.toString().substring(3, 5)).intValue();
        int iSeconds = new Integer(dt.toString().substring(6, 8)).intValue();

        if (iHour != 00)
            iHour = iHour * 60 * 60;
        else
            iHour = 60 * 60;

        if (iMinutes != 00)
            iMinutes = iMinutes * 60;
        else
            iMinutes = 60;

        if (iSeconds != 00)
            iSeconds = iSeconds;
        else
            iSeconds = 0;

        TotalSeconds = iHour + iMinutes + iSeconds;

        //Calendar calendar = Calendar.getInstance();
        //String timeValue = ""+Constants.formatRecordNumber(calendar.get(calendar.HOUR_OF_DAY),2)+Constants.formatRecordNumber(calendar.get(calendar.MINUTE),2)+Constants.formatRecordNumber(calendar.get(calendar.SECOND),2);
        return TotalSeconds;
    }

    public static boolean checkTodayDateforTP(String sDate) {

        boolean check = false;

        String sDay = "";
        String sMonth = "";
        String sYear = "";

        String sParsedDate = "";

        sDay = sDate.substring(0, 2);
        sMonth = sDate.substring(3, 6);
        sYear = "20" + sDate.substring(sDate.length() - 2, sDate.length());

        sParsedDate = sDay;

        if (sMonth.equals("Jan")) {
            sParsedDate += "/1/";
        } else if (sMonth.equals("Feb")) {
            sParsedDate += "/2/";
        } else if (sMonth.equals("Mar")) {
            sParsedDate += "/3/";
        } else if (sMonth.equals("Apr")) {
            sParsedDate += "/4/";
        } else if (sMonth.equals("May")) {
            sParsedDate += "/5/";
        } else if (sMonth.equals("Jun")) {
            sParsedDate += "/6/";
        } else if (sMonth.equals("Jul")) {
            sParsedDate += "/7/";
        } else if (sMonth.equals("Aug")) {
            sParsedDate += "/8/";
        } else if (sMonth.equals("Sep")) {
            sParsedDate += "/9/";
        } else if (sMonth.equals("Oct")) {
            sParsedDate += "/10/";
        } else if (sMonth.equals("Nov")) {
            sParsedDate += "/11/";
        } else if (sMonth.equals("Dec")) {
            sParsedDate += "/12/";
        }

        sParsedDate += sYear;

        if (sParsedDate.equals(DateUtil.getCurrentDate())) {
            check = true;
        }
        return check;
    }

    public static long dateDiff(String Date1, String Date2) {
        if (stringToDate(Date1) == stringToDate(Date2)) {
            return 1;
        } else {
            long days = (stringToDate(Date2).getTime() - stringToDate(Date1).getTime()) / (1000 * 60 * 60 * 24);
            return days;
        }
    }

    public static boolean lastDayOfMonth(int day, int month) {
        if (month == 1 && day == 31) //January
            return true;
        else if (month == 2 && day == 28)
            return true;
        else if (month == 3 && day == 31)
            return true;
        else if (month == 4 && day == 30)
            return true;
        else if (month == 5 && day == 31)
            return true;
        else if (month == 6 && day == 30)
            return true;
        else if (month == 7 && day == 31)
            return true;
        else if (month == 8 && day == 31)
            return true;
        else if (month == 9 && day == 30)
            return true;
        else if (month == 10 && day == 31)
            return true;
        else if (month == 11 && day == 30)
            return true;
        else if (month == 12 && day == 31)
            return true;
        else
            return false;
    }

    public static boolean lastDayOfWeek(int day) {
        if (day == 6)
            return true;
        else
            return false;
    }

    public static String convertMonthFromPhenoix(String sMonth) {

        String sConvertedMonth = "";

        if (sMonth.equals("Jan")) {
            sConvertedMonth = "1";
        } else if (sMonth.equals("Feb")) {
            sConvertedMonth = "2";
        } else if (sMonth.equals("Mar")) {
            sConvertedMonth = "3";
        } else if (sMonth.equals("Apr")) {
            sConvertedMonth = "4";
        } else if (sMonth.equals("May")) {
            sConvertedMonth = "5";
        } else if (sMonth.equals("Jun")) {
            sConvertedMonth = "6";
        } else if (sMonth.equals("Jul")) {
            sConvertedMonth = "7";
        } else if (sMonth.equals("Aug")) {
            sConvertedMonth = "8";
        } else if (sMonth.equals("Sep")) {
            sConvertedMonth = "9";
        } else if (sMonth.equals("Oct")) {
            sConvertedMonth = "10";
        } else if (sMonth.equals("Nov")) {
            sConvertedMonth = "11";
        } else if (sMonth.equals("Dec")) {
            sConvertedMonth = "12";
        }

        return sConvertedMonth;
    }

    public static Date getFirstDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_WEEK);
        Date firstDayOfWeek = null;

        switch (day) {
            case 1:
                firstDayOfWeek = cal.getTime();
                break;
            case 2:
                cal.add(Calendar.DAY_OF_WEEK, -1);
                firstDayOfWeek = cal.getTime();
                break;
            case 3:
                cal.add(Calendar.DAY_OF_WEEK, -2);
                firstDayOfWeek = cal.getTime();
                break;
            case 4:
                cal.add(Calendar.DAY_OF_WEEK, -3);
                firstDayOfWeek = cal.getTime();
                break;
            case 5:
                cal.add(Calendar.DAY_OF_WEEK, -4);
                firstDayOfWeek = cal.getTime();
                break;
            case 6:
                cal.add(Calendar.DAY_OF_WEEK, -5);
                firstDayOfWeek = cal.getTime();
                break;
            default:
                cal.add(Calendar.DAY_OF_WEEK, -6);
                firstDayOfWeek = cal.getTime();

        }

        return firstDayOfWeek;
    }

    public static Date getLastDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_WEEK);
        Date firstDayOfWeek = null;

        switch (day) {
            case 1:
                cal.add(Calendar.DAY_OF_WEEK, 6);
                firstDayOfWeek = cal.getTime();
                break;
            case 2:
                cal.add(Calendar.DAY_OF_WEEK, 5);
                firstDayOfWeek = cal.getTime();
                break;
            case 3:
                cal.add(Calendar.DAY_OF_WEEK, 4);
                firstDayOfWeek = cal.getTime();
                break;
            case 4:
                cal.add(Calendar.DAY_OF_WEEK, 3);
                firstDayOfWeek = cal.getTime();
                break;
            case 5:
                cal.add(Calendar.DAY_OF_WEEK, 2);
                firstDayOfWeek = cal.getTime();
                break;
            case 6:
                cal.add(Calendar.DAY_OF_WEEK, 1);
                firstDayOfWeek = cal.getTime();
                break;
            default:
                firstDayOfWeek = cal.getTime();

        }

        return firstDayOfWeek;
    }

    public static int calculateDifferenceInDays(Date a, Date b) {
        a = new java.sql.Date(a.getTime());
        int tempDifference = 0;
        int difference = 0;
        Calendar earlier = Calendar.getInstance();
        Calendar later = Calendar.getInstance();

        if (a.compareTo(b) < 0) {
            earlier.setTime(a);
            later.setTime(b);
        } else {
            earlier.setTime(b);
            later.setTime(a);
        }

        while (earlier.get(Calendar.YEAR) != later.get(Calendar.YEAR)) {
            tempDifference = 365 * (later.get(Calendar.YEAR) - earlier.get(Calendar.YEAR));
            difference += tempDifference;

            earlier.add(Calendar.DAY_OF_YEAR, tempDifference);
        }

        if (earlier.get(Calendar.DAY_OF_YEAR) != later.get(Calendar.DAY_OF_YEAR)) {
            tempDifference = later.get(Calendar.DAY_OF_YEAR) - earlier.get(Calendar.DAY_OF_YEAR);
            difference += tempDifference;

            earlier.add(Calendar.DAY_OF_YEAR, tempDifference);
        }

        return difference;
    }

    public static int calculateDifferenceInMinutes(Date a, Date b) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        // Set the date for both of the calendar instance
        cal1.setTime(a);
        cal2.setTime(b);
        // Get the represented date in milliseconds
        long milis1 = cal1.getTimeInMillis();
        long milis2 = cal2.getTimeInMillis();
        long diff = milis2 - milis1;
        long diffMinutes = diff / (60 * 1000);
        return (int) diffMinutes;
    }

    public static Date getMinDate() {
        return new Date(Long.MIN_VALUE);
    }

    public static Date getMaxDate() {
        return new Date(Long.MAX_VALUE);
    }

    public static Date getNow() {
        return new Date();
    }

    public static Date getToday() {
        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        return today.getTime();

    }

    public static Date addMinutes(Date date, long minutes) {
        return addSeconds(date, minutes * 60);
    }

    public static Date addSeconds(Date date, long seconds) {
        return new Date(date.getTime() + (seconds * 1000));
    }

    public static Date addHours(Date date, long hours) {
        return addSeconds(date, hours * 60 * 60);
    }

    public static Date addDays(Date date, long days) {
        return addSeconds(date, days * 24 * 60 * 60);
    }


}
