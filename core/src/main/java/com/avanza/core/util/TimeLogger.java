package com.avanza.core.util;

import com.avanza.core.CoreException;
import com.avanza.core.CoreInterface;
import com.avanza.core.util.configuration.ConfigSection;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;


public class TimeLogger implements CoreInterface {

    private static final Logger logger = Logger.getLogger(TimeLogger.class);
    private static boolean enableLogging = false;
    private static LogLevel logLevel = LogLevel.Off;
    private static boolean isLoaded = false;

    private Monitor monitor;

    protected TimeLogger() {
    }

    public static TimeLogger init() {
        return new TimeLogger();
    }

    public static void init(ConfigSection section) {

        if (isLoaded) return;

        logger.logInfo("[Creating the TimeLogger Instance]");
        new TimeLogger().load(section);
        isLoaded = true;
    }

    public void begin(Class<?> clazz, String methodName, LogLevel logLevel) {

        if (!enableLogging || !logger.isLoggable(logLevel))
            return;
        monitor = MonitorFactory.start(clazz.getName() + "." + methodName);
    }

    public void begin(String key, LogLevel logLevel) {

        if (!enableLogging || !logger.isLoggable(logLevel))
            return;
        monitor = MonitorFactory.start(key);
    }

    public void begin(String key) {

        if (!enableLogging || !logger.isLoggable(logLevel))
            return;
        monitor = MonitorFactory.start("(" + Thread.currentThread().getId() + ")" + key);
    }

    public void begin(Class<?> clazz, String methodName) {

        if (!enableLogging || !logger.isLoggable(logLevel))
            return;
        monitor = MonitorFactory.start(clazz.getName() + "." + methodName);
    }

    public void end(String logMessage) {

        if (!enableLogging || !logger.isLoggable(logLevel))
            return;
        if (this.monitor == null)
            throw new CoreException("TimeLogger cannot end Log without calling to begin logging.");
        this.endNow(logLevel, logMessage);
    }

    public void end(LogLevel logLevel, String logMessage) {

        if (!enableLogging || !logger.isLoggable(logLevel))
            return;
        if (this.monitor == null)
            throw new CoreException("TimeLogger cannot end Log without calling to begin logging.");
        Guard.checkNull(logLevel, "TimeLogger.end(logLevel, logMessage)");
        this.endNow(logLevel, logMessage);
    }

    public void end() {

        if (!enableLogging || !logger.isLoggable(logLevel))
            return;
        if (this.monitor == null)
            throw new CoreException("TimeLogger cannot end Log without calling to begin logging.");
        endNow(logLevel, "");
    }

    private void endNow(LogLevel logLevel, String logMessage) {
        this.monitor.stop();
        logger.log(logLevel, this.monitor.toString() + logMessage);
    }

    public void end(LogLevel logLevel) {

        if (!enableLogging || !logger.isLoggable(logLevel))
            return;
        if (this.monitor == null)
            throw new CoreException("TimeLogger cannot end Log without calling to begin logging.");
        Guard.checkNull(logLevel, "TimeLogger.end(logLevel)");
        endNow(logLevel, "");
    }

    public void load(ConfigSection section) {

        Guard.checkNull(section, "TimeLogger.load(cfgSection)");
        enableLogging = section.getBooleanValue("enableLogging");
        logLevel = LogLevel.valueOf(section.getTextValue("logLevel"));
    }

    public void dispose() {

    }
}
