package com.avanza.core.util;

import com.avanza.core.CoreException;

public class ConvertException extends CoreException {

    private static final long serialVersionUID = 3L;

    public ConvertException(String message) {

        super(message);
    }

    public ConvertException(String format, Object... args) {

        super(format, args);
    }

    public ConvertException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public ConvertException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public ConvertException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public ConvertException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}
