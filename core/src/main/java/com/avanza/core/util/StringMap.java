package com.avanza.core.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class StringMap<T> {

    public static final int end = -1;

    private ArrayList<MapItem<T>> mapList;

    public StringMap() {

        this.mapList = new ArrayList<MapItem<T>>(20);
    }

    public StringMap(int capacity) {

        this.mapList = new ArrayList<MapItem<T>>(capacity);
    }

    public void clear() {

        this.mapList.clear();
    }

    public boolean containsKey(String key) {

        return (this.indexOf(key) != StringMap.end);
    }

    private int indexOf(String key) {

        int retVal = StringMap.end;

        for (int index = 0; index < this.mapList.size(); index++) {

            if (this.mapList.get(index).key.compareToIgnoreCase(key) == 0) {

                retVal = index;
                break;
            }
        }

        return retVal;
    }

    public boolean containsValue(T value) {

        boolean retVal = false;

        for (int index = 0; index < this.mapList.size(); index++) {

            if (this.mapList.get(index).value.equals(value)) {
                retVal = true;
                break;
            }
        }

        return retVal;
    }

    public T get(String key) {

        T retVal = null;
        int idx = this.indexOf(key);

        if (idx != StringMap.end)
            retVal = this.mapList.get(idx).value;

        return retVal;
    }

    public T get(int index) {

        return this.mapList.get(index).value;
    }

    public boolean isEmpty() {

        return this.mapList.isEmpty();
    }

    public T add(String key, T value) {

        int index = this.indexOf(key);
        if (index == StringMap.end)
            this.mapList.add(new MapItem<T>(key, value));
        else
            this.mapList.set(index, new MapItem<T>(key, value));

        return value;
    }

    public void addAll(Map<String, T> values) {

        for (Entry<String, T> value : values.entrySet()) {
            this.add(value.getKey(), value.getValue());
        }
    }

    public T remove(String key) {

        int index = this.indexOf(key);

        if (index == StringMap.end)
            return this.mapList.remove(index).value;

        return null;
    }

    public int size() {

        return this.mapList.size();
    }

    /**
     * This method returns the iterator of the StringMap of Type MapItem<ValueType>
     *
     * @return
     */
    public Iterator<MapItem<T>> iterator() {
        return mapList.iterator();
    }

    public void sort() {

        if (this.mapList.size() > 1)
            java.util.Collections.sort(this.mapList);
    }
}
