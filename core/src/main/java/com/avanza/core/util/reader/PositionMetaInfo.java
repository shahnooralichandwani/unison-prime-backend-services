package com.avanza.core.util.reader;

public class PositionMetaInfo {
    private int startPos;
    private int length;

    public PositionMetaInfo(int startPos, int length) {
        this.startPos = startPos;
        this.length = length;
    }

    public int getStartPos() {

        return this.startPos;
    }

    public void setStartPos(int startPos) {

        this.startPos = startPos;
    }

    public int getLength() {

        return this.length;
    }

    public void setLength(int length) {

        this.length = length;
    }

    public String toString() {
        return String.format("[%1$d,%2$d]", this.startPos, this.length);
    }
}
