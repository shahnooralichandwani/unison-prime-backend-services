package com.avanza.core.util.reader;

import com.avanza.core.util.configuration.ConfigurationException;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;

public class XMLDocumentReader {

    private DocumentBuilderFactory factory = null;
    private DocumentBuilder builder = null;
    private Document document = null;

    public Document readXMLDocument(String file) {
        factory = DocumentBuilderFactory.newInstance();
        try {
            File xmlFile = new File(file);
            FileInputStream inputStream = new FileInputStream(xmlFile);
            builder = factory.newDocumentBuilder();
            document = builder.parse(inputStream);
        } catch (Exception e) {
            throw new ConfigurationException(e, "Error parsing configuration");
        }

        return document;
    }

}
