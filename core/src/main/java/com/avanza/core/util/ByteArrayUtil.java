package com.avanza.core.util;

public class ByteArrayUtil {

    private static final String hexEmpty = "0x";
    private static final byte highMask = (byte) 0xf0;
    private static final byte lowMask = (byte) 0x0f;

    public static boolean compare(byte[] arg1, byte[] arg2) {
        if (arg1 == null || arg2 == null)
            return false;

        if (arg1.length != arg2.length)
            return false;

        boolean retVal = true;
        for (int index = 0; index < arg1.length; index++) {

            if (arg1[index] != arg2[index]) {
                retVal = false;
                break;
            }
        }

        return retVal;
    }

    // TO DO: Need to check whether it is working fine or not
    public static byte[] fromHexString(String hexValue) {

        if (StringHelper.isEmpty(hexValue))
            return null;

        int length = hexValue.length();
        if ((length % 2) > 0)
            throw new ConvertException("Invalid input string. Has to be multiple of 2");

        byte[] retVal = new byte[length / 2];

        int cnt = 0;
        int index = 0;
        while (index < length) {

            String strTemp = hexValue.substring(index, 2);
            retVal[cnt] = Byte.parseByte(strTemp, 16);
            index = index + 2;
            cnt++;
        }

        return retVal;
    }

    public static String toHexString(byte[] arg) {
        if ((arg == null) || (arg.length == 0))
            return hexEmpty;


        StringBuilder builder = new StringBuilder((arg.length * 2) + hexEmpty.length());

        builder.append(hexEmpty);
        for (int index = 0; index < arg.length; index++) {

            byte temp = ByteArrayUtil.getHighNibble(arg[index]);
            builder.append(ByteArrayUtil.toHexChar(temp));

            temp = ByteArrayUtil.getLowNibble(arg[index]);
            builder.append(ByteArrayUtil.toHexChar(temp));
        }

        return builder.toString();
    }


    public static String toHexLiteral(byte[] input) {

        String hexString = "";

        for (int i = 0; i < input.length; i++) {
            String temp = Integer.toHexString(input[i] & 0xFF);
            hexString += (temp.length() < 2) ? ("0" + temp) : temp;
        }
        return hexString;
    }

    public static byte[] fromHEXLiteral(String input) {

        input.toLowerCase();
        byte[] bts = new byte[input.length() / 2];

        for (int i = 0; i < bts.length; i++) {

            bts[i] = (byte) Integer.parseInt(input.substring(2 * i, 2 * i + 2), 16);
        }
        return bts;
    }

    public static byte getLowNibble(byte arg) {
        return (byte) (arg & lowMask);
    }

    public static byte getHighNibble(byte arg) {
        return (byte) ((arg & highMask) >> 4);
    }

    private static char toHexChar(byte nibble) {
        char retVal;

        if (nibble < 10)
            retVal = (char) ('0' + nibble);
        else
            retVal = (char) ('a' + (nibble - 10));

        return retVal;
    }

}
