package com.avanza.core.util;

/*
 * Utility Class that can be used for basic argument checking
 */

public final class Guard {

    private Guard() {
    }

    /*
     * Checks an argument to ensure it isn't null
     *
     * @param name="argValue">The argument value to check
     * @param name="argName">The name of the argument
     */
    public static <T> void checkNull(T argVal, String argName) {

        if (argVal == null)
            throw new IllegalArgumentException(String.format("Argument[%s] can't be null", argName));
    }

    /*
     * Checks a string argument to ensure it isn't null or empty
     * </summary>
     * <param name="argValue">The argument value to check.</param>
     * <param name="argName">The name of the argument.</param>
     *
     */
    public static String checkNullOrEmpty(String argVal, String argName) {
        Guard.checkNull(argVal, argName);
        Guard.checkEmpty(argVal, argName);

        return argVal;
    }

    public static String checkEmpty(String argVal, String argName) {
        if (argVal.length() == 0)
            throw new IllegalArgumentException(String.format("Argument[%s] can't be empty", argName));

        return argVal;
    }

    public static <T> T getDefaultIfNull(T argVal, T defaultVal) {
        return (argVal != null) ? argVal : defaultVal;
    }

    /*
    public static <T> void CheckEnumValue(Class<T> enumType, Object value, String argName){
    	
        if (Enum.(enumType, value) == false)
            throw new IllegalArgumentException(String.format("Argument[%s] contain invalid value for Enum[%s]",argName, enumType.ToString()));
    }
    */
}
