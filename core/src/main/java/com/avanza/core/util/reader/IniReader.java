package com.avanza.core.util.reader;

import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.configuration.NameValueConfigSesction;

public class IniReader extends CsvReader {

    private static char ASSIGNMENT_OPERATOR = '=';
    private static char DELIMITED_CHAR = ';';

    public IniReader() {
        super(ASSIGNMENT_OPERATOR);
        isTrim = true;
    }

    public IniReader(char delimitChar) {
        super(delimitChar);
        isTrim = true;
    }

    public String readField() {

        StringBuilder builder = new StringBuilder(1024);
        try {

            int tempChar = this.reader.read();
            while (tempChar != StreamReader.END) {
                char charRead = (char) tempChar;

                if (charRead == this.ASSIGNMENT_OPERATOR)
                    break;

                else if (charRead == this.DELIMITED_CHAR) {

                    this.lineNumber++;
                    break;
                } else {
                    builder.append(charRead);
                }

                tempChar = this.reader.read();
            }

            this.currentField = builder.toString();
            if (this.isTrim)
                this.currentField = this.currentField.trim();

            this.recordData.add(this.currentField);

            return this.currentField;
        } catch (Exception e) {

            throw new StreamReaderException(e, "I/O Error occured while reading from stream. Err Msg: [%1$s]. Check exception stack for further details",
                    e.getMessage());
        }
    }


    public ConfigSection getConfigFromFile(String fileName) {
        super.load(fileName);
        NameValueConfigSesction retSection = new NameValueConfigSesction();
        try {
            while (!eof()) {
                retSection.put(readField(), readField());
            }
        } catch (Exception e) {
        }
        return retSection;
    }


    public ConfigSection getConfigFromData(String msgData) {
        NameValueConfigSesction retSection = new NameValueConfigSesction();
        if (StringHelper.isEmpty(msgData))
            return retSection;

        super.loadData(msgData);
        try {
            while (!eof()) {
                String name = readField();
                if (StringHelper.isEmpty(name))
                    break;
                String value = readField();
                retSection.put(name, value);
            }
        } catch (Exception e) {
        }
        return retSection;
    }


}
