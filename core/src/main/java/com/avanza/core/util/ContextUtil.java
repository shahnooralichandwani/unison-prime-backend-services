package com.avanza.core.util;

import com.avanza.core.CoreException;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.ExecutionContext;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.web.config.WebContext;

import java.lang.reflect.Method;
import java.util.Map;

public class ContextUtil {

    public static void addToExecutionContext(String metaEntityId, String moduleId) {

        ExecutionContext executionContext = getExecutionContext();
        executionContext.putValue(ExecutionContext.META_ENTITY_ID, metaEntityId);
        executionContext.putValue(ExecutionContext.MODULE_ID, moduleId);
        executionContext.putValue(ExecutionContext.IS_CALLSCRIPT_CONTEXT_CHANGE, true);
    }

    public static void addToExecutionContext(String metaEntityId) {

        ExecutionContext executionContext = getExecutionContext();
        executionContext.putValue(ExecutionContext.META_ENTITY_ID, metaEntityId);
        executionContext.putValue(ExecutionContext.IS_CALLSCRIPT_CONTEXT_CHANGE, true);
    }

    public static void addToExecutionContext(String name, Object value) {

        ExecutionContext executionContext = getExecutionContext();
        executionContext.putValue(name, value);
    }

    public static void addToExecutionContext(Map<String, Object> values) {

        ExecutionContext executionContext = getExecutionContext();
        executionContext.putAll(values);
    }

    private static ExecutionContext getExecutionContext() {

        WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
        ExecutionContext executionContext = ctx.getAttribute(ExecutionContext.EXECUTION_CONTEXT_KEY);

        if (executionContext == null) {
            executionContext = new ExecutionContext();
            ctx.setAttribute(ExecutionContext.EXECUTION_CONTEXT_KEY, executionContext);
        }

        return executionContext;
    }

    /**
     * Used for nested properties.
     *
     * @param propertyName
     * @return
     */
    public static Object getPropertyValue(String propertyName) {

        propertyName = propertyName.substring(1, propertyName.length() - 1);
        Object propertyValue = getPropertyValueFromThread(propertyName);
        if (propertyValue == null)
            propertyValue = getPropertyValueFromSession(propertyName);

        return propertyValue;
    }

    private static Object getPropertyValueFromThread(String propertyName) {

        Object propertyType = null;
        String ObjectId = propertyName.substring(0, propertyName.lastIndexOf("."));
        String property = propertyName.substring(propertyName.lastIndexOf(".") + 1, propertyName.length());

        //If its a nested property then!!!
        if (ObjectId.lastIndexOf(".") > 0) {
            propertyType = getPropertyValue(ObjectId);
        } else {
            propertyType = ApplicationContext.getContext().get(ObjectId);
        }

        if (propertyType == null)
            return null;

        if (propertyType instanceof DataObject)
            return ((DataObject) propertyType).getValue(property);
        else if (propertyType instanceof ExecutionContext)
            return ((ExecutionContext) propertyType).getValue(property);
        else
            return getValue(propertyType, property);
    }

    private static Object getPropertyValueFromSession(String propertyName) {

        Object propertyType = null;
        String ObjectId = propertyName.substring(0, propertyName.lastIndexOf("."));
        String property = propertyName.substring(propertyName.lastIndexOf(".") + 1, propertyName.length());

        //If its a nested property then!!!
        if (ObjectId.lastIndexOf(".") > 0) {
            propertyType = getPropertyValue(ObjectId);
        } else {
            WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
            propertyType = ctx.getAttribute(ObjectId);
        }

        if (propertyType == null)
            return null;

        if (propertyType instanceof DataObject)
            return ((DataObject) propertyType).getValue(property);
        else if (propertyType instanceof ExecutionContext)
            return ((ExecutionContext) propertyType).getValue(property);
        else
            return getValue(propertyType, property);
    }

    private static Object getValue(Object ctxObj, String property) {

        String getterName = String.format("get%1$s", property);
        try {
            Class classObj = Class.forName(ctxObj.getClass().getName());
            Method[] methods = classObj.getMethods();
            Method getter = null;
            for (Method meth : methods) {
                if (meth.getName().equalsIgnoreCase(getterName)) {
                    Class[] params = meth.getParameterTypes();
                    if (params == null || params.length == 0) {
                        getter = meth;
                        break;
                    }
                }
            }
            if (getter == null) throw new CoreException(String.format("Property %1$2 Not Found", property));
            Object Value = getter.invoke(ctxObj, new Object[0]);
            return Value;
        } catch (Exception e) {
            throw new CoreException(e, "Exception Occured");
        }
    }

    @Deprecated
    public static Object getContextValue(String expression, WebContext context) {
        String ObjectId = expression.substring(0, expression.lastIndexOf("."));
        String property = expression.substring(expression.lastIndexOf(".") + 1, expression.length());
        Object ctxObj = context.getAttribute(ObjectId);
        if (ctxObj instanceof DataObject)
            return String.format("%1$s%2$s%3$s", "'", ((DataObject) ctxObj).getValue(property), "'");
        else
            return getValue(ctxObj, property);

    }

    public static Object[] getContextValues(String expression) {
        WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
        String ObjectId = expression.substring(0, expression.lastIndexOf("."));
        String property = expression.substring(expression.lastIndexOf(".") + 1, expression.length());
        Object ctxObj = ctx.getAttribute(ObjectId);
        // if (ctxObj instanceof DataObject)
        // return ((DataObject) ctxObj).getAsString(AttribId);
        // else
        // return getValue(ctxObj);
        return null;
    }

    public static String getApplicationContextTrace() {
        return ApplicationContext.getContext().toString();
    }
}
