package com.avanza.core.util.reader;

import com.avanza.core.util.Guard;

public class PbvReader extends StreamReader {

    private PositionMetaCollection metaList;

    public PbvReader(PositionMetaCollection metaList) {

        super(false);
        Guard.checkNull(metaList, "PbvReader(PositionMetaCollection)");
        this.metaList = metaList;
    }

    public PbvReader(PositionMetaCollection metaList, boolean isTrim) {

        super(isTrim);
        Guard.checkNull(metaList, "PbvReader(PositionMetaCollection, bool)");
        this.metaList = metaList;
    }

    public PositionMetaCollection getMetaList() {

        return this.metaList;
    }

    //TODO need to implement
    protected String readField() {

        throw new StreamReaderException("PbvReader.readField is not implemented");
    }

    protected void parse(String toParse) {
        this.recordData.clear();

        // Assuming that meta list contains fields in ascending order based on position
        int idx = this.metaList.size() - 1;
        PositionMetaInfo info = this.metaList.get(idx);
        int totalLen = info.getStartPos() + info.getLength() - 1;

        if (totalLen > toParse.length())
            throw new StreamReaderException("Line Number %1$d contains data less then expected. Expected Length = {1}, Data Length = {2}",
                    this.getLineNumber(), totalLen, toParse.length());

        for (idx = 0; idx < this.metaList.size(); idx++) {

            info = this.metaList.get(idx);
            String strVal = toParse.substring(info.getStartPos() - 1, info.getLength());

            if (this.isTrim)
                strVal = strVal.trim();

            this.recordData.add(strVal);
        }
    }
}
