//===============================================================================
// Copyright Avanza Solutions (Pvt) Ltd.  All rights reserved.
// THIS CODE AND INFORMATION IS PROPERTY OF THE AVANZA SOLUTIONS AND 
// CANNOT BE USED WITHOUT THE APPROVAL OF THE MANAGEMENT
//===============================================================================

package com.avanza.core.util.reader;

import com.avanza.core.util.Convert;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;

public abstract class StreamReader {

    protected static final int END = -1;
    private static final String ERR_MSG = "Failed to parse value [%1$s] at index [%2$d] as %3$s";

    protected String currentLine;
    protected ArrayList<String> recordData;
    protected java.io.Reader reader;
    protected boolean isTrim;
    protected String currentField;
    protected int lineNumber;
    protected boolean isEndRecord;

    protected StreamReader() {

        this.init(false);
    }

    public StreamReader(boolean isTrim) {

        this.init(isTrim);
    }

    private void init(boolean isTrim) {

        this.recordData = new ArrayList<String>(20);
        this.isTrim = isTrim;
        this.currentLine = StringHelper.EMPTY;
        this.currentField = StringHelper.EMPTY;
        this.lineNumber = StreamReader.END;
    }

    protected abstract String readField();

    public boolean isTrim() {

        return this.isTrim;
    }

    public int getFieldCount() {

        Guard.checkNull(this.recordData, "Reader state is invalid.");
        return this.recordData.size();
    }

    public String getRawData() {

        return this.currentLine;
    }

    public int getLineNumber() {

        return this.lineNumber;
    }

    public String[] getRecordData() {

        String[] retVal = null;

        if (this.currentLine != null)
            retVal = (String[]) this.recordData.toArray();

        return retVal;
    }

    public boolean eof() {
        Guard.checkNull(this.reader, "Internal Stream is either null or in invalid state");

        try {
            return !this.reader.ready();
        } catch (Exception ex) {

            throw new StreamReaderException(ex, "System Error occured while tyring to access the stream");
        }
    }

    public void load(String fileUrl, Charset cs) {

        Guard.checkNull(fileUrl, "StreamReader.load(String, Charset)");

        InputStream stream = null;

        try {

            stream = new FileInputStream(fileUrl);
        } catch (Exception e) {

            throw new StreamReaderException(e, "System Error occured while tyring to open file %1$s. Check Excepiton details",
                    fileUrl);
        }

        this.load(stream, cs);
    }

    public void load(String fileUrl) {

        this.load(fileUrl, Charset.defaultCharset());
    }

    public void load(InputStream stream, Charset cs) {

        Guard.checkNull(stream, "StreamReader.load(InputStream, Charset)");

        try {

            this.reader = new BufferedReader(new InputStreamReader(stream, cs));
        } catch (Exception ex) {

            throw new StreamReaderException(ex, "System Error occured while tyring to initialize the stream");
        }
    }

    public void load(Reader reader) {

        Guard.checkNull(reader, "StreamReader.load(java.io.Reader)");

        this.reader = reader;

    }

    public void loadData(String msgData) {

        Guard.checkNull(msgData, "StreamReader.loadData(String)");

        try {

            this.reader = new StringReader(msgData);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, "System Error occured while tyring to initialize reader on string");
        }
    }

    public void load(InputStream stream) {

        this.load(stream, Charset.defaultCharset());
    }

    public void readRecord() {

        this.isEndRecord = false;
        this.recordData.clear();

        while (!this.isEndRecord) {
            this.readField();
        }
    }

    public char getChar(int index) {

        String value = this.getValue(index);

        try {

            return Convert.toChar(value);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, StreamReader.ERR_MSG,
                    value, index, char.class.getSimpleName());
        }
    }

    public String getString(int index) {

        return this.getValue(index);
    }

    public byte getByte(int index) {

        String value = this.getValue(index);

        try {

            return Convert.toByte(value);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, StreamReader.ERR_MSG,
                    value, index, byte.class.getSimpleName());
        }
    }

    public short getShort(int index) {

        String value = this.getValue(index);

        try {

            return Convert.toShort(value);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, StreamReader.ERR_MSG,
                    value, index, short.class.getSimpleName());
        }
    }

    public int getInteger(int index) {

        String value = this.getValue(index);

        try {

            return Convert.toInteger(value);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, StreamReader.ERR_MSG,
                    value, index, int.class.getSimpleName());
        }
    }

    public long getLong(int index) {

        String value = this.getValue(index);

        try {

            return Convert.toLong(value);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, StreamReader.ERR_MSG,
                    value, index, long.class.getSimpleName());
        }
    }

    public float getFloat(int index) {

        String value = this.getValue(index);

        try {

            return Convert.toFloat(value);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, StreamReader.ERR_MSG,
                    value, index, float.class.getSimpleName());
        }
    }

    public double GetDouble(int index) {
        String value = this.getValue(index);

        try {

            return Convert.toDouble(value);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, StreamReader.ERR_MSG,
                    value, index, double.class.getSimpleName());
        }
    }

    public boolean getBoolean(int index) {

        String value = this.getValue(index);

        try {

            return Convert.toBoolean(value);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, StreamReader.ERR_MSG,
                    value, index, boolean.class.getSimpleName());
        }
    }

    public Date getDate(int index) {

        String value = this.getValue(index);

        try {
            return Convert.toDate(value);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, StreamReader.ERR_MSG,
                    value, index, Date.class.getSimpleName());
        }
    }

    public Date getDate(int index, String format) {

        String value = this.getValue(index);

        try {
            return Convert.toDate(value, format);
        } catch (Exception ex) {

            throw new StreamReaderException(ex, "Failed to parse value [%1$s] at index [%2$d] as %3$s. Parsing format %4$d",
                    value, index, Date.class.getSimpleName(), format);
        }
    }

    public byte[] getBytes(int index, Charset encoder) {

        String value = this.getValue(index);

        try {
            ByteBuffer buffer = encoder.encode(value);
            return buffer.array();
        } catch (Exception ex) {

            throw new StreamReaderException(ex, "Failed to parse value [%1$s] at index [%2$d] as %3$s. Parsing format",
                    value, index, byte[].class.getSimpleName());
        }
    }

    public byte[] getBytes(int index) {

        return this.getBytes(index, Charset.defaultCharset());
    }

    public Object getValue(int index, Class<? extends Object> type) {

        String value = this.getValue(index);

        return ConvertUtil.parse(value, type);
    }

    public boolean isEmpty(int index) {

        return StringHelper.isEmpty(this.getValue(index));
    }

    public void reset() {

        try {

            if (this.reader != null)
                this.reader.reset();
        } catch (Exception ex) {

            throw new StreamReaderException(ex, "Error %1$s occured on reset of the stream",
                    ex.getMessage());
        }
    }

    public void close() {

        try {

            if (this.reader != null)
                this.reader.close();
        } catch (Exception ex) {

            throw new StreamReaderException(ex, "Error %1$s occured on reset of the stream",
                    ex.getMessage());
        }
    }

    private String getValue(int index) {

        Guard.checkNull(this.recordData, "Reader state is invalid.");

        if (index >= this.recordData.size())
            throw new StreamReaderException("Invalid index [%1$d] specified. Record Line contains [%2$d] elements.",
                    index, this.recordData.size());

        return this.recordData.get(index);
    }

    public Reader getReader() {
        return this.reader;
    }
}
