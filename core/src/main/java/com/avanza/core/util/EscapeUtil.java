package com.avanza.core.util;

import org.apache.commons.lang.StringEscapeUtils;

import java.util.List;

public class EscapeUtil {

    public static <T> List<T> escapeList(List<T> list) {
        List<T> escapeList = list;
        for (int i = 0; i < list.size(); i++) {
            T item = list.get(i);
            if (item instanceof String) {
                T escapedItem = (T) getEscapeValue(item);
                escapeList.remove(item);
                escapeList.add(escapedItem);
            }
        }
        return escapeList;
    }

    public static Object getEscapeValue(Object value) {
        Object val = value;
        val = StringEscapeUtils.escapeSql(val.toString());
        return val;
    }

}
