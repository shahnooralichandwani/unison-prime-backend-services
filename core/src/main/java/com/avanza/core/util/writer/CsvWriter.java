package com.avanza.core.util.writer;

import com.avanza.core.util.StringHelper;

public class CsvWriter extends StreamWriter {

    public static final char ESC_CHAR = '"';
    public static final char END_REC_CHAR = '|';

    private char delimitChar;
    private char escChar;
    private char endRecChar;

    public CsvWriter(char delimitChar) {

        super(false);
        this.init(delimitChar, CsvWriter.ESC_CHAR, CsvWriter.END_REC_CHAR);
    }

    public CsvWriter(char delimitChar, boolean isTrim) {

        super(isTrim);
        this.init(delimitChar, CsvWriter.ESC_CHAR, CsvWriter.END_REC_CHAR);
    }

    private void init(char delimitChar, char escChar, char endRecChar) {

        this.delimitChar = delimitChar;
        this.escChar = escChar;
        this.endRecChar = endRecChar;
    }

    public char getDelimitChar() {

        return this.delimitChar;
    }

    @Override
    public void writeEndRecord() {

        if (this.hasEndRecord()) {

            try {
                this.writer.write(this.endRecChar);
            } catch (Exception e) {

                throw new StreamWriterException(e, StreamWriter.IO_ERR_MSG, e.getMessage());
            }
            this.state = WriteState.Record;
        }
    }

    @Override
    public void writeField(String value) {

        if (this.hasField()) {

            this.innerWriteField(value);
            this.state = WriteState.Field;
        }
    }

    private void innerWriteField(String value) {

        if (value == null)
            value = StringHelper.EMPTY;

        try {

            if (this.state == WriteState.Field)
                this.writer.write(this.delimitChar);

            boolean hasDelimit = (value.indexOf(this.delimitChar) > -1);
            boolean hasEscape = (value.indexOf(this.escChar) > -1);

            if (hasDelimit || hasEscape)
                this.writer.write(this.escChar);

            for (int idx = 0; idx < value.length(); idx++) {

                char temp = value.charAt(idx);
                if (hasEscape && (temp == this.escChar))
                    this.writer.write(this.escChar);

                this.writer.write(temp);
            }

            if (hasDelimit || hasEscape)
                this.writer.write(this.escChar);
        } catch (Exception e) {

            throw new StreamWriterException(e, StreamWriter.IO_ERR_MSG, e.getMessage());
        }
    }

    @Override
    public void writeStartRecord() {

        try {

            if (this.state == WriteState.Field)
                this.writer.write(this.endRecChar);
        } catch (Exception e) {

            throw new StreamWriterException(e, StreamWriter.IO_ERR_MSG, e.getMessage());
        }

        if (this.isOpen())
            this.state = WriteState.RecordStart;

    }
}