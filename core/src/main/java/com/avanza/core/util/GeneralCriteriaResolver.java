package com.avanza.core.util;

import com.avanza.core.sdo.expression.Expression;
import com.avanza.core.sdo.expression.ExpressionParser;
import com.avanza.core.sdo.expression.ExpressionType;
import com.avanza.core.security.User;
import com.avanza.workflow.configuration.org.OrganizationManager;

import java.util.List;

/**
 * Resolves view on select critera
 */
public class GeneralCriteriaResolver {

    private static final Logger logger = Logger.getLogger(GeneralCriteriaResolver.class);

    public static boolean resolveCriteria(String selectCritera) {
        Expression expression = ExpressionParser.parse(selectCritera);
        boolean multiExpressionResult[] = new boolean[expression.size()];
        if (expression.hasChildExpressions()) {
            for (int i = 0; i < expression.size(); i++) {
                Expression exp = expression.get(i);
                if (exp.getType() == ExpressionType.Function) {
                    multiExpressionResult[i] = evaluate(exp);
                }
            }
            if (expression.getText().toLowerCase().equals("and")) {
                for (boolean bool : multiExpressionResult) {
                    if (bool == false)
                        return false;
                }
            }
            if (expression.getText().toLowerCase().equals("or")) {
                for (boolean bool : multiExpressionResult) {
                    if (bool == true)
                        return true;
                }
                return false;
            }

        } else {
            return evaluate(expression);
        }
        return true;
    }

    private static boolean evaluate(Expression exp) {
        String vals[] = getValues(exp);
        if (exp.getText().toLowerCase().equals("equal")) {

            if (vals[0].equals(vals[1]))
                return true;
        }
        if (exp.getText().toLowerCase().equals("notequal")) {

            if (!(vals[0].equals(vals[1])))
                return true;
        }
        if (exp.getText().toLowerCase().equals("sql")) {

        }
        if (exp.getText().toLowerCase().equals("inrole")) {
            return userInRole(vals[0], vals[1]);
        }

        return false;
    }

    private static String[] getValues(Expression exp) {
        String values[] = new String[exp.size()];

        for (int i = 0; i < exp.size(); i++) {
            Expression expparam = exp.get(i);
            if (expparam.getType() == ExpressionType.Constant) {
                if (expparam.getText().indexOf("&") != -1) {
                    values[i] = (String) ContextUtil.getPropertyValue(expparam.getText());
                } else {
                    values[i] = expparam.getText();
                }
            }
        }
        return values;
    }

    private static boolean userInRole(String userid, String roleid) {
        List<User> users = OrganizationManager.getAllUsers(roleid);
        for (User usr : users) {
            if (usr.getLoginId().equals(userid))
                return true;
        }
        return false;
    }
}
