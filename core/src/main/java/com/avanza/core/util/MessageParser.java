package com.avanza.core.util;

public class MessageParser {

    private String message;
    private int currentIndex;

    public MessageParser(String message) {
        this.message = message;
    }

    public String getNext(String delimiter) {
        String res = "";
        int index = message.indexOf(delimiter, currentIndex);
        if (index != -1) {
            res = message.substring(currentIndex, index);
            currentIndex = index + delimiter.length();
        }
        return res;
    }

    public String getNext(int length) {
        String temp = message.substring(currentIndex, currentIndex += length);
        return temp;
    }

    public void ignore(int length) {
        currentIndex += length;
    }

}
