package com.avanza.core.util;

import com.avanza.core.data.sequence.SequenceManager;
import com.avanza.core.meta.MetaCounter;

import java.text.SimpleDateFormat;


/**
 * This class is used for generating the Id's for the
 * UI for JSF controls.
 *
 * @author kraza
 */
public class CounterUtils {
    private static final String ID_PREFIX = "ui_";
    private static final String ACTIVITY_SESSION_KEY_PREFIX = "ACT_";
    private static long _counter = 1;
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");

    public synchronized static String getNextId() {
        return ID_PREFIX + _counter++;
    }

    public synchronized static String getNextTxnUniqueId() {
        String id = String.valueOf(SequenceManager.getInstance().getNextValueasInt(MetaCounter.STAN_ID_COUNTER));
        return StringHelper.getLeadingZeros(8 - id.length()).append(id).toString();
    }

    public static String getNextActivitySessionKey() {
        String id = SequenceManager.getInstance().getNextValue(MetaCounter.ACTIVITY_SESSION_KEY_COUNTER);
        return id;
    }

    public static String getNextNotificationId() {
        String id = SequenceManager.getInstance().getNextValue(MetaCounter.NOTIFICATION_COUNTER);
        return id;
    }

    public static String getNextAttachmentId() {
        String id = SequenceManager.getInstance().getNextValue(MetaCounter.ATTACHMENT_ID);
        return id;
    }

    public static String getNextWorkflowLogId() {
        String id = SequenceManager.getInstance().getNextValue(MetaCounter.WORKFLOW_LOG_COUNTER);
        return id;
    }

    public static String getNextActionLogId() {
        String id = SequenceManager.getInstance().getNextValue(MetaCounter.ACTION_LOG_COUNTER);
        return id;
    }

    public static String getNextMetaPickListCounter() {
        String id = SequenceManager.getInstance().getNextValue(MetaCounter.META_PICK_LIST);
        return id;
    }

    public synchronized static String getNextPd4mlId() {
        return "_attachment_" + _counter++;
    }

    public static int getNextAuditTableKey() {
        int id = Integer.parseInt(SequenceManager.getInstance().getNextValue(MetaCounter.ACTIVITY_SESSION_KEY_COUNTER));
        return id;
    }
}
