package com.avanza.core.util.writer;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.List;

import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.reader.StreamReaderException;

public abstract class StreamWriter {

    protected static final int END = -1;
    protected static final String IO_ERR_MSG = "I/O Error occured while writing to the stream. Err Msg: [%1$s]. Check exception stack for further details";

    protected String currentLine;
    protected Writer writer;
    protected boolean isTrim;
    protected String currentField;
    protected int lineNumber;
    protected WriteState state;

    protected StreamWriter() {

        this.init(false);
    }

    public StreamWriter(boolean isTrim) {

        this.init(isTrim);
    }

    private void init(boolean isTrim) {

        this.isTrim = isTrim;
        this.currentLine = StringHelper.EMPTY;
        this.currentField = StringHelper.EMPTY;
        this.lineNumber = StreamWriter.END;
        this.state = WriteState.None;
    }

    public boolean isTrim() {

        return this.isTrim;
    }

    public String getRawData() {

        return this.currentLine;
    }

    public int getLineNumber() {

        return this.lineNumber;
    }

    public void open(String fileUrl, Charset cs) {

        Guard.checkNull(fileUrl, "StreamReader.load(String, Charset)");

        OutputStream stream = null;

        try {

            stream = new FileOutputStream(fileUrl);
        } catch (Exception e) {

            throw new StreamWriterException(e, "System Error occured while tyring to open file %1$s. Error Msg: %2$s. Check Excepiton stack for further details",
                    fileUrl);
        }

        this.open(stream, cs);
    }

    public void open(String fileUrl) {

        this.open(fileUrl, Charset.defaultCharset());
    }

    public void open(OutputStream stream, Charset cs) {

        if (this.isOpen())
            return;

        Guard.checkNull(stream, "StreamWriter.open(OutputStream, Charset)");

        try {

            this.writer = new BufferedWriter(new OutputStreamWriter(stream, cs));
            this.state = WriteState.Initial;
        } catch (Exception ex) {

            throw new StreamWriterException(ex, "System Error occured while tyring to initialize the stream");
        }
    }

    public void open(StringWriter msgData) {

        if (this.isOpen())
            return;

        Guard.checkNull(msgData, "StreamWriter.open(msgData)");

        try {

            this.writer = msgData;
            this.state = WriteState.Initial;
        } catch (Exception ex) {

            throw new StreamReaderException(ex, "System Error occured while tyring to initialize writer on StringWriter");
        }
    }

    public void open(OutputStream stream) {

        this.open(stream, Charset.defaultCharset());
    }

    //TODO Need to implement
    public void close() {

        try {

            if (this.isOpen()) {

                this.writer.close();
                this.state = WriteState.Closed;
            }
        } catch (Exception ex) {

            throw new StreamWriterException(ex, "System Error occured while tyring to close the stream");
        }

    }

    public abstract void writeStartRecord();

    public abstract void writeEndRecord();

    public abstract void writeField(String value);

    public boolean hasEndRecord() {

        return (this.state == WriteState.RecordStart) || (this.state == WriteState.Field);
    }

    public boolean hasField() {

        return (this.state != WriteState.None) && (this.state != WriteState.Closed) &&
                (this.state != WriteState.Error);
    }

    public boolean isNotOpen() {

        return (this.state == WriteState.None) || (this.state == WriteState.Closed) ||
                (this.state == WriteState.Error);
    }

    public boolean isOpen() {

        return (this.state != WriteState.None) && (this.state != WriteState.Closed) &&
                (this.state != WriteState.Error);
    }

    public void writeRecord(List<String> valueList) {

        for (String value : valueList) {

            this.writeField(value);
        }
    }

    public void writeField(int value) {

        this.writeField(Integer.toString(value));
    }

    //TODO Need to implement
    public void writeField(Object value) {

        this.writeField(value.toString());
    }

    //TODO Need to implement
    public void writeField(double value) {

    }

    //TODO Need to implement writeField function for all basic data type

}
