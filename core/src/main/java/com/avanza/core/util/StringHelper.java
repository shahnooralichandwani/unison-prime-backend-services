package com.avanza.core.util;

import com.avanza.core.FunctionDelegate;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.Expression;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;

import java.util.*;
import java.util.Map.Entry;

public final class StringHelper {
    private StringHelper() {
    }

    public static final String WhiteSpace = " \n\r\f\t";
    public static final String NEW_LINE = "\n";

    public static final Locale INAVARIANT_LOCALE = Locale.US;

    public static final String DOT = ".";

    public static final String SEMI_COLON = ";";

    public static final String EQUAL = "=";

    public static final String SPACE = " ";

    public static final String UNDER_SCORE = "_";

    public static final String COMMA_SPACE = ", ";

    public static final String COMMA = ",";

    public static final String OPEN_PAREN = "(";

    public static final String CLOSED_PAREN = ")";

    public static final String SINGLE_QUOTE = "'";

    public static final String EMPTY = "";

    public static final String ZERO = "0";

    public static boolean isEmpty(String str) {

        return (str == null) || (str.length() == 0);
    }

    public static boolean isNotEmpty(String str) {

        return (str != null) && (str.length() > 0);
    }

    public static String nullSafeToString(Object obj) {

        return obj == null ? "(null)" : obj.toString();
    }

    public static String nullSafeString(String str, String defaultVal) {

        return (str == null || "".equalsIgnoreCase(str)) ? defaultVal : str;
    }

    public static int indexOfIgnoreCase(String value, String find) {

        String valueTemp = value.toUpperCase(Locale.US);
        String findTemp = find.toUpperCase(Locale.US);

        return valueTemp.indexOf(findTemp);
    }

    public static boolean startsWithIgnoreCase(String value, String find) {

        String valueTemp = value.toUpperCase(Locale.US);
        String findTemp = find.toUpperCase(Locale.US);

        return valueTemp.startsWith(findTemp);
    }

    public static String getCombinedKeyFrom(String[] keys, int size) {
        if (keys == null || keys.length <= 0)
            return "";
        StringBuffer sb = new StringBuffer("");
        for (int i = 0; i <= size; i++) {
            String key = keys[i];
            sb.append(key);
            if (i + 1 <= size)
                sb.append(DOT);
        }
        return sb.toString();
    }

    public static String getCombinedKeyFrom(String[] keys) {

        return getCombinedKeyFrom(keys, keys.length - 1);
    }

    public static String serilizePropertiesMap(Map<String, ? extends Object> map) {
        return serilizePropertiesMap(map, SEMI_COLON, '[', ']');
    }

    @SuppressWarnings("unchecked")
    public static String serilizePropertiesMap(Map<String, ? extends Object> map,
                                               final String delimiter, final Character opener, final Character closer) {
        return join((Collection) map.entrySet(), delimiter,
                new FunctionDelegate.Arg1<String, Entry<String, ? extends Object>>() {
                    public String call(Entry<String, ? extends Object> entry) {
                        if (StringHelper.isEmpty(entry.getKey())) {
                            return null;
                        }

                        String value = "";
                        if (entry.getValue() instanceof Map<?, ?>) {
                            value = serilizePropertiesMap(
                                    (Map<String, ? extends Object>) entry.getValue(), delimiter,
                                    opener, closer);
                            if (StringHelper.isNotEmpty(value)) {
                                value = opener + value + closer;
                            }
                        } else if (entry.getValue() != null) {
                            value = entry.getValue().toString();
                            if (value.contains(delimiter) || value.contains(EQUAL)) {
                                value = opener + value + closer;
                            }
                        }
                        return entry.getKey() + EQUAL + value;
                    }
                });
    }

    public static Map<String, String> getPropertiesMapFrom(String paramString) {
        return getPropertiesMapFrom(paramString, SEMI_COLON);
    }

    public static Collection<String> SplitEnclosedString(String str, String delimiterRegex,
                                                         Character opener, Character closer) {
        if (opener == null || closer == null) {
            return Arrays.asList(str.split(delimiterRegex));
        }

        StringTokenizer tokenizer = new StringTokenizer(str, delimiterRegex);
        Collection<String> parts = new ArrayList<String>();
        int notClosedOpeners = 0;

        StringBuilder currentPart = new StringBuilder();
        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (currentPart.length() > 0) {
                currentPart.append(delimiterRegex);
            }
            currentPart.append(token);
            notClosedOpeners += countNotClosedOpeners(token, opener, closer);
            if (notClosedOpeners < 0) {
                throw new RuntimeException("Enclosers were not properly closed");
            } else if (notClosedOpeners == 0) {
                parts.add(trimEnclosers(currentPart.toString(), opener, closer));
                currentPart.setLength(0);
            }
        }

        if (notClosedOpeners > 0) {
            throw new RuntimeException("Enclosers were not properly closed");
        }

        return parts;
    }

    private static String trimEnclosers(String str, char opener, char closer) {
        // it is used to trim useless/extra enclosers ((TEXT)(TEXT)) =>
        // (TEXT)(TEXT)
        StringBuilder sb = new StringBuilder(str.trim());
        int notClosedOpeners = 0;
        int extraEnclosers = 0;

        // first calculate extra enclosers.
        int stringEndingClosers = 0;
        boolean textStarted = false;
        for (int i = 0; i < sb.length(); i++) {
            if (sb.charAt(i) == opener) {
                notClosedOpeners++;
                stringEndingClosers = 0;
                if (!textStarted) {
                    extraEnclosers++;
                }
            } else if (sb.charAt(i) == closer) {
                textStarted = true;
                notClosedOpeners--;
                if (notClosedOpeners < 0) {
                    throw new RuntimeException("Enclosers were not properly closed");
                } else if (notClosedOpeners < extraEnclosers) {
                    // it may not be closing of extra opener. it depends on up
                    // coming chars.
                    stringEndingClosers++;
                    extraEnclosers--;
                }
            } else if (!Character.isWhitespace(sb.charAt(i))) {
                textStarted = true;
                stringEndingClosers = 0;
            }
        }

        // not a valid string.
        if (notClosedOpeners != 0) {
            throw new RuntimeException("Enclosers were not properly closed");
        }

        // total calculated extra enclosers.
        extraEnclosers += stringEndingClosers;

        // remove extra openers
        int extras = extraEnclosers;
        while (sb.length() > 0 && extras > 0) {
            if (sb.charAt(0) == opener) {
                extras--;
            }
            sb.deleteCharAt(0);
        }

        // remove extra closers
        extras = extraEnclosers;
        while (sb.length() > 0 && extras > 0) {
            if (sb.charAt(sb.length() - 1) == closer) {
                extras--;
            }
            sb.deleteCharAt(sb.length() - 1);
        }

        return sb.toString().trim();

    }

    private static int countNotClosedOpeners(String str, char opener, char closer) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == opener) {
                count++;
            } else if (str.charAt(i) == closer) {
                count--;
            }
        }
        return count;
    }

    public static Map<String, String> getPropertiesMapFrom(String paramString, String delimiterRegex) {
        return getPropertiesMapFrom(paramString, delimiterRegex, '[', ']');
    }

    public static Map<String, String> getPropertiesMapFrom(String paramString,
                                                           String delimiterRegex, Character opener, Character closer) {

        Map<String, String> propsMap = new HashMap<String, String>();

        if (StringHelper.isEmpty(paramString)) {
            return propsMap;
        }

        Collection<String> paramTokens = SplitEnclosedString(paramString, delimiterRegex, opener,
                closer);

        for (String param : paramTokens) {
            Collection<String> col = SplitEnclosedString(param, EQUAL, opener, closer);
            String[] keyValuePair = new String[col.size()];
            col.toArray(keyValuePair);
            propsMap.put(keyValuePair[0], (keyValuePair.length > 1) ? keyValuePair[1] : ""); // "key=",
        }

        return propsMap;
    }

    public static String getPropertiesString(Map<String, String> propsMap) {

        StringBuilder propsStr = new StringBuilder();
        Iterator<Entry<String, String>> iter = propsMap.entrySet().iterator();
        while (iter.hasNext()) {
            Entry<String, String> prop = iter.next();
            propsStr.append(prop.getKey()).append(EQUAL).append(prop.getValue());
            if (iter.hasNext())
                propsStr.append(SEMI_COLON);
        }

        return propsStr.toString();
    }

    public static String getPropertiesString(Map<String, String> propsMap, String delimiter) {

        StringBuilder propsStr = new StringBuilder();
        Iterator<Entry<String, String>> iter = propsMap.entrySet().iterator();
        while (iter.hasNext()) {
            Entry<String, String> prop = iter.next();
            propsStr.append(prop.getKey()).append(EQUAL).append(prop.getValue());
            if (iter.hasNext())
                propsStr.append(delimiter);
        }

        return propsStr.toString();
    }

    public static StringBuilder getLeadingZeros(int count) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count; i++) {
            sb.append("0");
        }
        return sb;
    }

    public static String padLeft(String value, char pad, int size) {

        return StringHelper.innerPad(value, pad, size, true);
    }

    public static String padRight(String value, char pad, int size) {

        return StringHelper.innerPad(value, pad, size, false);
    }

    public static String rTrim(String value, char trimVal) {

        if (StringHelper.isEmpty(value))
            return value;

        int idx = value.length() - 1;

        for (; idx >= 0; idx--) {

            if (value.charAt(idx) != trimVal)
                break;
        }

        if (idx < 0)
            return StringHelper.EMPTY;

        if (idx >= 0)
            value = value.substring(0, idx + 1);

        return value;

    }

    public static String lTrim(String value, char trimVal) {

        if (StringHelper.isEmpty(value))
            return value;

        int idx = 0;
        for (; idx < value.length(); idx++) {

            if (value.charAt(idx) != trimVal)
                break;
        }

        if (idx >= value.length())
            return StringHelper.EMPTY;

        if (idx > 0)
            value = value.substring(idx);

        return value;
    }

    private static String innerPad(String value, char pad, int size, boolean padLeft) {

        if (value == null)
            value = StringHelper.EMPTY;

        int strLen = value.length();

        if (strLen >= size)
            return value;

        int padLen = size - strLen;

        StringBuilder builder = new StringBuilder(size);

        if (!padLeft)
            builder.append(value);

        for (int cnt = 0; cnt < padLen; cnt++) {

            builder.append(pad);
        }

        if (padLeft)
            builder.append(value);

        return builder.toString();
    }

    public static String getFullMetaId(String stringToAppendZero) {

        int zerolength = 10 - stringToAppendZero.length();

        return getLeadingZeros(zerolength).append(stringToAppendZero).toString();

    }

    public static String removeChar(String s, char c) {
        String r = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != c)
                r += s.charAt(i);
        }
        return r;
    }

    public static String removeFormatting(String s) {

        if (StringHelper.isNotEmpty(s)) {

            if (s.contains("$"))
                s = StringHelper.removeChar(s, '$');

            if (s.contains("%"))
                s = StringHelper.removeChar(s, '%');

            if (s.contains(","))
                s = StringHelper.removeChar(s, ',');
        }

        return s;
    }

    public static String getTruncatedText(String sourceStr, int length) {

        if (sourceStr.length() <= length)
            return sourceStr;

        else {
            String newStr = sourceStr.substring(0, length - 3);
            newStr += "...";
            return newStr;
        }
    }

    public static String getSubString(String sourceStr, String startIdentifier, String endIdentifier) {

        if (sourceStr.indexOf(startIdentifier) == -1 || sourceStr.indexOf(endIdentifier) == -1)
            return sourceStr;

        else {
            String newStr = sourceStr.substring(sourceStr.indexOf(startIdentifier) + 1,
                    sourceStr.indexOf(endIdentifier));
            return newStr;
        }
    }

    public static String trimSpaceReplace(String source, char replaceChar) {

        if (StringHelper.isEmpty(source) || Character.valueOf(replaceChar) == null)
            return source;

        String finalStr = source.trim();

        return finalStr.replaceAll(StringHelper.SPACE, String.valueOf(replaceChar));

    }

    public static Object maskString(Object source, String maskChar, int startIndex, int length) {

        String sourceString = source.toString().trim();
        StringBuilder maskStringBuilder = new StringBuilder();
        int masked = 0;

        if (length > sourceString.length() || length == 0)
            length = sourceString.length();

        for (int i = 0; i < sourceString.length(); i++) {

            if ((i + 1) >= startIndex && masked < length) {
                masked++;
                maskStringBuilder.append(maskChar);
            } else
                maskStringBuilder.append(sourceString.charAt(i));

        }

        return maskStringBuilder.toString();

    }

    public static String encrypt(String src) {
        Cryptographer crypto = new Cryptographer();
        return crypto.encrypt(src);
    }

    public static String decrypt(String src) {
        Cryptographer crypto = new Cryptographer();
        return crypto.decrypt(src);
    }

    public static void main(String[] args) {
        String str = StringHelper.join(Arrays.asList("a", "b", "c", "d", "e", "f"),
                StringHelper.COMMA, "'%s'");
        getPropertiesMapFrom("name=abdul wahid;properties=[relegion=islam;accopation=[employer=avanza;job=SE];status=maried];");
        System.out.println(StringHelper.maskString("123456789", "*", 3, 4));
    }

    /**
     * It is used to join List items into a concatenated string.
     *
     * @param <T>
     * @param list
     * @param delima
     * @param formatter it can be used to provide more specific format even exclude
     *                  some items.
     * @return
     */
    public static <T> String join(Collection<T> list, String delima,
                                  FunctionDelegate.Arg1<String, T> formatter) {
        String currDelima = "";
        StringBuilder builder = new StringBuilder();
        for (T item : list) {
            String str = formatter.call(item);
            if (str != null) {
                builder.append(currDelima);
                builder.append(str);
                currDelima = delima;
            }
        }
        return builder.toString();
    }

    /**
     * It is used to join List items into a concatenated string.
     *
     * @param <T>
     * @param list
     * @param delima
     * @param format : it can be used to keep the list items enclosed in quotes
     *               like �name� or �abc�
     * @return
     */
    public static <T> String join(Collection<T> list, String delima, String format) {
        final String finalFormat = format;
        return join(list, delima, new FunctionDelegate.Arg1<String, T>() {
            public String call(T arg1) {
                return String.format(finalFormat, arg1);
            }
        });
    }

    public static String[] parseListBoxString(String listboxStr) {
        listboxStr = listboxStr.replace("[", "");
        listboxStr = listboxStr.replace("]", "");
        String[] tempArr = listboxStr.split(",");

        for (int i = 0; i < tempArr.length; i++)
            tempArr[i] = tempArr[i].trim();

        return tempArr;
    }

    public static Object evaluateFEL(String felExpression) {
        //TODO: UNISONPRIME
        ExpressionParser expParser = new ExpressionParser(felExpression, Object.class, FunctionCatalogManager.getInstance().getFunctionCatalog(ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction/*, ExpressionClass.WorkFlowFunctions, ExpressionClass.SocialMediaFunctions*/));
        Expression retVal = expParser.parse();
        ExpressionEvaluator eval = ExpressionEvaluator.getInstance();
        return eval.evaluate(retVal);

    }


}
