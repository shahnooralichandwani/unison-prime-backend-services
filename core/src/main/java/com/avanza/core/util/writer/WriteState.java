package com.avanza.core.util.writer;

public enum WriteState {

    None(0), Initial(1), RecordStart(2), Field(3), Record(4), Closed(5),
    Error(6);

    private int intValue;

    private WriteState(int intValue) {

        this.intValue = intValue;
    }

    public int getIntValue() {

        return this.intValue;
    }

    public static WriteState fromString(String value) {

        WriteState retVal;

        if (value.equalsIgnoreCase("None"))
            retVal = WriteState.None;
        else if (value.equalsIgnoreCase("Initial"))
            retVal = WriteState.Initial;
        else if (value.equalsIgnoreCase("Closed"))
            retVal = WriteState.Closed;
        else if (value.equalsIgnoreCase("Record"))
            retVal = WriteState.Record;
        else if (value.equalsIgnoreCase("RecordStart"))
            retVal = WriteState.RecordStart;
        else if (value.equalsIgnoreCase("Field"))
            retVal = WriteState.Field;
        else if (value.equalsIgnoreCase("Error"))
            retVal = WriteState.Error;
        else
            throw new StreamWriterException("[%1$s] is not recognized as valid Write State", value);

        return retVal;
    }

    public static WriteState fromInteger(int value) {

        WriteState retVal;

        switch (value) {

            case 0:
                retVal = WriteState.None;
                break;

            case 1:
                retVal = WriteState.Initial;
                break;

            case 2:
                retVal = WriteState.RecordStart;
                break;

            case 3:
                retVal = WriteState.Field;
                break;

            case 4:
                retVal = WriteState.Record;
                break;

            case 5:
                retVal = WriteState.Closed;
                break;

            case 6:
                retVal = WriteState.Error;
                break;

            default:
                throw new StreamWriterException("[%1$d] is not recognized as valid Write State", value);
        }

        return retVal;
    }
}