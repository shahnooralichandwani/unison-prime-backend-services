package com.avanza.core.util.configuration;

import java.util.List;

/**
 * @author shahbaz.ali This class holds configuration from database
 * This class can be used directly to get a section from the
 * configuration file.
 */

public class ConfigurationCache {

    private static ConfigurationCache _self;
    private ConfigSection configuration;

    private ConfigurationCache() {

    }

    public static ConfigurationCache getInstance() {
        if (_self == null)
            _self = new ConfigurationCache();

        return _self;
    }

    private ConfigSection getConfiguratons() {
        return configuration;

    }

    public void setConfigurations(ConfigSection configuration) {
        this.configuration = configuration;
    }

    public List<ConfigSection> getConfigSection(String name) {

        if (configuration != null) {

            return getConfiguratons().getChildSections(name);
        } else {
            throw new ConfigurationException("Configuration is null, ConfigurationCache.getConfigSection ");
        }
    }

    public List<ConfigSection> getConfigSectionChild(String parentSection, String section) {

        if (configuration != null) {

            List<ConfigSection> pSections = getConfiguratons().getChildSections(parentSection);

            if (pSections.size() > 1)
                throw new ConfigurationException("There exists more then one parent Section in the configuration");

            else {
                return pSections.get(0).getChildSections(section);
            }
        } else {
            throw new ConfigurationException("Configuration is null, ConfigurationCache.getConfigSection ");
        }
    }
}
