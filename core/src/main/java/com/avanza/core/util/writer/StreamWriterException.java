package com.avanza.core.util.writer;

import com.avanza.core.CoreException;

public class StreamWriterException extends CoreException {

    private static final long serialVersionUID = 1195634124800347111L;

    public StreamWriterException(String message) {

        super(message);
    }

    public StreamWriterException(String format, Object... args) {

        super(format, args);
    }

    public StreamWriterException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public StreamWriterException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public StreamWriterException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public StreamWriterException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}