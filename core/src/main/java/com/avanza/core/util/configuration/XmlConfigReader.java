package com.avanza.core.util.configuration;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class XmlConfigReader implements ConfigReader {

    private DocumentBuilderFactory factory = null;
    private Document doc = null;
    private ConfigSection section = null;
    private DocumentBuilder builder = null;

    public ConfigSection load(String fileName) {

        factory = DocumentBuilderFactory.newInstance();

        try {

            builder = factory.newDocumentBuilder();
            doc = builder.parse(new File(fileName));
            Element root = doc.getDocumentElement();
            section = new XmlConfigSection(root);
        } catch (IOException e) {

            throw new ConfigurationException(e, "Failed to Load file [%1$s]", fileName);
        } catch (Exception e) {

            throw new ConfigurationException(e, "Error parsing configuration");
        }

        return section;
    }

    public ConfigSection loadData(InputStream streamData) {

        try {

            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            doc = builder.parse(streamData);
            Element root = doc.getDocumentElement();
            section = new XmlConfigSection(root);
        } catch (Exception e) {

            throw new ConfigurationException(e, "Error parsing configuration");
        }

        return section;
    }

    public void reload() {

    }
}
