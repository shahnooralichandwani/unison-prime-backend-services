package com.avanza.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

//import org.apache.myfaces.custom.fileupload.UploadedFile;

public final class ConvertUtil {

    public static final String DATE_FORMAT = "dd-MMM-yyyy";

    private ConvertUtil() {

    }

    public static Class<? extends Object> toType(String value) {

        Class<? extends Object> retVal = null;

        if (value.compareToIgnoreCase("byte") == 0)
            retVal = byte.class;
        else if (value.compareToIgnoreCase("short") == 0)
            retVal = short.class;
        else if (value.compareToIgnoreCase("integer") == 0)
            retVal = int.class;
        else if (value.compareToIgnoreCase("long") == 0)
            retVal = long.class;
        else if (value.compareToIgnoreCase("boolean") == 0)
            retVal = boolean.class;
        else if (value.compareToIgnoreCase("date") == 0)
            retVal = Date.class;
        else if (value.compareToIgnoreCase("string") == 0)
            retVal = String.class;
        else if (value.compareToIgnoreCase("char") == 0)
            retVal = char.class;
        else if (value.compareToIgnoreCase("float") == 0)
            retVal = float.class;
        else if (value.compareToIgnoreCase("double") == 0)
            retVal = Double.class;
        else
            throw new ConvertException("[%1$s] is not recognized as valid type", value);

        return retVal;
    }

    public static Object parse(String value, Class<? extends Object> type) {

        Object retVal = null;

        if (type == String.class) {
            if (value != null)
                retVal = new String(value);
        } else if (type == char.class)
            retVal = Convert.toChar(value);
        else if (type == boolean.class || type == Boolean.class)
            retVal = Convert.toBoolean(value);
        else if (type == Date.class)
            retVal = Convert.toDate(value, ConvertUtil.DATE_FORMAT);
        else if (type == Byte.class || type == byte.class)
            retVal = Convert.toByte(value);
        else if (type == Short.class || type == short.class)
            retVal = Convert.toShort(value);
        else if (type == Integer.class || type == int.class)
            retVal = Convert.toInteger(value);
        else if (type == Long.class || type == long.class)
            retVal = Convert.toLong(value);
        else if (type == Float.class || type == float.class)
            retVal = Convert.toDouble(value);
        else if (type == Double.class || type == double.class)
            retVal = Convert.toDouble(value);
        else if (type == Object.class)
            retVal = (Object) value;
            /*
             * else if (type == UploadedFile.class) retVal = (Object) value;
             */
        else
            throw new ConvertException("Conversion for Type [%1$s] is not supported. Data: [%2$s]", type.getName(),
                    value);

        return retVal;
    }

    public static String toString(Object value) {
        String retVal = StringHelper.EMPTY;

        if (value instanceof Date) {
            SimpleDateFormat format = new SimpleDateFormat(ConvertUtil.DATE_FORMAT);
            retVal = format.format(value);
        } else if (value != null)
            retVal = value.toString();
        else
            retVal = "";

        return retVal;
    }

    public static Object ConvertTo(Object value, Class<? extends Object> type) {

        if (type == null)
            return value;

        if (value == null)
            return Convert.getNull(type);

        if (value.getClass() == type)
            return value;

        if (value instanceof String)
            return ConvertUtil.parse(((String) value).trim(), type);

        Object retVal;

        if (type == boolean.class)
            retVal = Convert.toBoolean(value);
        else if (type == char.class)
            retVal = Convert.toChar(value);
        else if (type == String.class)
            retVal = value.toString();
        else if (type == Date.class)
            retVal = Convert.toDate(value);
        else if (type == byte.class)
            retVal = Convert.toByte(value);
        else if (type == short.class)
            retVal = Convert.toShort(value);
        else if (type == int.class)
            retVal = Convert.toInteger(value);
        else if (type == long.class)
            retVal = Convert.toLong(value);
        else if (type == float.class || type == Float.class)
            retVal = Convert.toFloat(value);
        else if (type == double.class || type == Double.class)
            retVal = Convert.toDouble(value);
        else if (type == Map.class)
            retVal = type.cast(value);
        else if (type == List.class)
            retVal = type.cast(value);
        else if (type == Object.class)
            retVal = type.cast(value);
        else {
            throw new ConvertException("Conversion for Type [%1$s] is not supported. Data: [%2$s]", type.getName(),
                    value.toString());
        }

        return retVal;
    }
}
