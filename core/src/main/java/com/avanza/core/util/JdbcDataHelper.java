package com.avanza.core.util;

import java.util.HashMap;

public class JdbcDataHelper {

    public static String parseCritera(String criteraStr) {

        if (StringHelper.isEmpty(criteraStr))
            return null;

        // criteraStr = "RELATIONSHIP_NUM=&ODS.Customer.RELATIONSHIP_NUM& and RELATIONSHIP_NUM=&ODS.Customer.NUM&";
        HashMap<String, String> replaceValMap = new HashMap<String, String>();
        int startIndex = criteraStr.indexOf("&");
        while (startIndex != -1) {
            int endIndex = criteraStr.indexOf("&", startIndex + 1);
            String subString = criteraStr.substring(startIndex, endIndex + 1);
            startIndex = criteraStr.indexOf("&", criteraStr.indexOf("&", endIndex) + 1);

            String value = (String) ContextUtil.getPropertyValue(subString);
            replaceValMap.put(subString, "'" + value + "'");
        }
        for (String key : replaceValMap.keySet()) {
            String value = replaceValMap.get(key);
            criteraStr = criteraStr.replaceAll(key, value);
        }

        return criteraStr;
    }
}
