package com.avanza.core.util.reader;

import com.avanza.core.CoreException;

public class StreamReaderException extends CoreException {

    private static final long serialVersionUID = 7648220057612950419L;

    public StreamReaderException(String message) {

        super(message);
    }

    public StreamReaderException(String format, Object... args) {

        super(format, args);
    }

    public StreamReaderException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public StreamReaderException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public StreamReaderException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public StreamReaderException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}
