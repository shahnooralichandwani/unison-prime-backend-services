package com.avanza.core.util;

import com.avanza.core.CoreException;

import javax.crypto.Cipher;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;

public class Cryptographer {
    private static final String IsoAnsiLatinI = "ISO-8859-1"; // ISO ANSI LATIN I
    private static final byte[] BASE_KEY = {0x6a, 0x68, 0x6b, 0x6b, 0x73, 0x6f, 0x61, 0x6b, 0x66, 0x73, 0x61, 0x7a, 0x61, 0x2d, 0x39, 0x32, 0x6a, 0x68, 0x6b, 0x6b, 0x73, 0x6f, 0x61, 0x6b, 0x66, 0x73, 0x61, 0x7a, 0x61, 0x2d, 0x39, 0x32};
    //private static final String CipherMode = "DESede/ECB/NoPadding";
    private static final String CipherMode = "DESede/ECB/PKCS5Padding";

    private byte[] key;
    private String cipher;

    public String getCipher() {
        return cipher;
    }

    public Cryptographer() {

        this.cipher = Cryptographer.CipherMode;
        this.key = Cryptographer.BASE_KEY;
    }

    public Cryptographer(byte[] key) {

        this.setKey(key);
    }

    public byte[] getKey() {
        return this.key;
    }

    public void setKey(byte[] key) {

        Guard.checkNull(key, "Cryptographer.setKey(byte[])");
        this.key = key;
    }

    public String encrypt(String input) {

        String retVal;

        try {

            Cipher algorithm = Cipher.getInstance(CipherMode);
            algorithm.init(Cipher.ENCRYPT_MODE, new SecretKeySpec((new DESedeKeySpec(this.key)).getKey(), "DESede"));
            byte[] tempArr = input.getBytes(IsoAnsiLatinI);
            //retVal =  new String (algorithm.doFinal(tempArr, 0, tempArr.length), IsoAnsiLatinI);
            retVal = ByteArrayUtil.toHexLiteral((algorithm.doFinal(tempArr, 0, tempArr.length)));
        } catch (Exception e) {

            throw new CoreException(e, "Exception Occured while Encrypting the Input Data");
        }

        return retVal;
    }


    public String decrypt(String input) {

        String retVal;
        try {

            Cipher algorithm = Cipher.getInstance(CipherMode);
            algorithm.init(Cipher.DECRYPT_MODE, new SecretKeySpec((new DESedeKeySpec(this.key)).getKey(), "DESede"));
            retVal = new String(algorithm.doFinal(ByteArrayUtil.fromHEXLiteral(input)), IsoAnsiLatinI);

        } catch (Exception e) {

            throw new CoreException(e, "Exception Occured while Decrypting the Input Data");
        }

        return retVal;
    }

    public static byte[] generateSignature(byte[] data, String algorithm, PrivateKey key) throws Exception {
        Signature sig = Signature.getInstance(algorithm);
        sig.initSign(key);
        sig.update(data);
        return sig.sign();
    }

    public static boolean verifyData(byte[] data, byte[] signature, String algorithm, PublicKey key) throws Exception {
        Signature sig = Signature.getInstance(algorithm);
        sig.initVerify(key);
        sig.update(data);
        return sig.verify(signature);
    }

    // This main function is added for testing purpose. It will be removed later
    public static void main(String[] arr) {
        StringBuilder sb = new StringBuilder();
        sb.append("A\r\n");
        sb.append("B\r\n");
        sb.append("C\r\n");
        sb.append("D\r\n");
        sb.append("E\r\n");
        sb.append("F\r\n");
        sb.append("G\r\n");
        String[] strs = sb.toString().split("\n");

        Cryptographer c = new Cryptographer();
        //String dec = c.decrypt("e0f430be0c3f5f705ca8edcde53557b8");
        String enc = c.encrypt("difficultpass1*");
        //	String dec = c.decrypt("a1884774a75ea1a66ff1549325fee03c");
        //  String enc = c.encrypt("Avanza123");
        System.out.println(enc);
        //dec = c.decrypt(enc);
        //System.out.print(dec);
    }
}




