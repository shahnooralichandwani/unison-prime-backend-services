package com.avanza.core.util.configuration;

import java.io.InputStream;

public interface ConfigReader {

    ConfigSection load(String fileName) throws ConfigurationException;

    ConfigSection loadData(InputStream configData) throws ConfigurationException;

    void reload() throws ConfigurationException;
}
