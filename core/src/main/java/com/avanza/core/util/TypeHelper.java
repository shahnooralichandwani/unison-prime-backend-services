package com.avanza.core.util;

import com.avanza.core.CoreException;

public class TypeHelper {

    private TypeHelper() {

    }

    public static <T> T createInstance(String name, Class<T> clazz, boolean isInterface) {

        Class<?> temp = TypeHelper.getClass(name);
		
		/*
		if(isInterface) {
			
			if(!TypeHelper.implementInterface(temp, clazz))
	    		throw new CoreException("Class [%1$s] does not implement [%2$s] interface",
	                                     name, clazz.getName()); 
		}
		*/

        try {

            return clazz.cast(temp.newInstance());
        } catch (Exception e) {

            throw new CoreException("Class [%1$s] does not implement [%2$s] interface",
                    name, clazz.getName());
        }
    }

    public static <T> boolean implementInterface(Class<?> clazz, Class<T> intface) {

        boolean retVal = false;

        Class<?>[] impClassList = clazz.getInterfaces();

        for (int idx = 0; idx < impClassList.length; idx++) {

            if (impClassList[idx] == intface) {

                retVal = true;
                break;
            }
        }

        return retVal;
    }

    public static Class<?> getClass(String name) {

        try {

            return Class.forName(name);
        } catch (Exception e) {

            throw new CoreException(e, "Failed to instantiate class [%1$s].", name);
        }
    }
}
