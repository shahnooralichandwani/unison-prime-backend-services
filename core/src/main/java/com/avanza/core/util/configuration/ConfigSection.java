package com.avanza.core.util.configuration;

import java.util.Date;
import java.util.List;

public interface ConfigSection {

    String getName();

    String getValue();

    boolean hasChildSections();

    boolean hasChild(String name);

    boolean hasAttribute(String name);

    ConfigSection getChild(String name);

    boolean getBooleanValue(String name);

    String getTextValue(String name);

    short getShortValue(String name);

    int getIntValue(String name);

    long getLongValue(String name);

    double getDoubleValue(String name);

    float getFloatValue(String name);

    <T extends Enum<T>> T getEnumValue(String name, T defVal, Class<T> classtype);

    <T extends Enum<T>> T getEnumValue(String name, Class<T> classtype);

    char getCharValue(String name);

    Date getDateValue(String name);

    String getValue(String name, String defVal);

    short getValue(String name, short defVal);

    int getValue(String name, int defVal);

    long getValue(String name, long defVal);

    boolean getValue(String name, boolean defVal);

    double getValue(String name, double defVal);

    float getValue(String name, float defVal);

    char getValue(String name, char defVal);

    Date getValue(String name, Date defVal);

    List<ConfigSection> getChildSections();

    List<ConfigSection> getChildSections(String name);
}