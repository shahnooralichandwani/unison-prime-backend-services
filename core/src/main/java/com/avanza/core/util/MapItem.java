package com.avanza.core.util;


public class MapItem<E> implements java.lang.Comparable<MapItem<E>> {
    public String key;
    public E value;

    public MapItem(String key, E value) {

        this.key = key;
        this.value = value;
    }

    public int compareTo(MapItem<E> rhs) {

        return this.key.compareTo(rhs.key);
    }
}
