package com.avanza.core.util;

import freemarker.cache.FileTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.StringTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * This class loads up the free Marker configuration object.
 *
 * @author Kazim
 */
public class FreeMarkerConfigurator {

    private static final Logger logger = Logger.getLogger(FreeMarkerConfigurator.class.getName());

    private static FreeMarkerConfigurator _instance;

    public boolean isLoaded = false;
    private Configuration cfg = null;
    private StringTemplateLoader stringTemplateLoader;
    private FileTemplateLoader fileTemplateLoader;

    private FreeMarkerConfigurator() {
        cfg = new Configuration();
    }

    public static synchronized FreeMarkerConfigurator getInstance() {
        if (_instance == null) {
            _instance = new FreeMarkerConfigurator();
            _instance.load("");
        }
        return _instance;
    }

    public void load(String baseTemplateDirectoryPath) {

        if (isLoaded) return;

        try {
            // To Loading here.
            File templateFile = new File(baseTemplateDirectoryPath);
            if (templateFile.exists() == false) logger.logInfo("Unable to locate the base template directory.");

            stringTemplateLoader = new StringTemplateLoader();
//            fileTemplateLoader = new FileTemplateLoader(templateFile);

            TemplateLoader[] templateLoaders = new TemplateLoader[]{stringTemplateLoader};
            MultiTemplateLoader mutliTemplateLoader = new MultiTemplateLoader(templateLoaders);
            cfg.setTemplateLoader(mutliTemplateLoader);
        } catch (Throwable t) {
            logger.LogException("Excepion while FreeMarkerConfigurator.load(String)", t);
        }

        isLoaded = true;
    }

    public void createTemplate(String templateName, String templateString) {

        if (!isLoaded)
            throw new RuntimeException("FreeMarker Configuration not Loaded, first load it.");

        stringTemplateLoader.putTemplate(templateName, templateString);
    }

    public String processTemplate(String templateName, Map<Object, Object> dataModel) {
        return processTemplateByObject(templateName, dataModel);
    }

    public String processTemplateByObject(String templateName, Object dataModel) {

        if (!isLoaded)
            throw new RuntimeException("FreeMarker Configuration not Loaded, first load it.");

        try {
            StringWriter strWriter = new StringWriter();
            Template template = this.cfg.getTemplate(templateName);
            if (template == null)
                return "";
            template.process(dataModel, strWriter);
            return strWriter.toString();
        } catch (Throwable t) {
            logger.LogException("Exception while getting Template.", t);
        }

        return null;
    }

    public static void main(String[] args) {
        FreeMarkerConfigurator fm = FreeMarkerConfigurator.getInstance();
        fm.load("C:/");
        fm.createTemplate("testTemplate", "Hello ${name}.");
        Map m = new HashMap();
        m.put("name", "'Kazim Raza'");
        System.out.println(fm.processTemplate("testTemplate", m));
    }
}

