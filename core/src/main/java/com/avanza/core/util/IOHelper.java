package com.avanza.core.util;

import com.avanza.core.data.DataSourceException;

import java.io.File;

/**
 * Helper methods related to IO operations.
 *
 * @author kraza
 */
public class IOHelper {

    private static Logger logger = Logger.getLogger(IOHelper.class);

    public static File openOrCreateFolder() {
        return null;
    }

    public static void existFile(File file) {
        Guard.checkNull(file, "addDocumentAttachment(file)");
        if (!file.exists()) {
            logger.logDebug("The file for document attachment $1%s has invalid file path.", file.getAbsolutePath());
            throw new DataSourceException("The file for document attachment has invalid file path.", file.getAbsolutePath());
        }
    }

    public static void moveTo(File srcFile, String realPath, String fileName) {

        existFile(srcFile);
        createIfNotExists(realPath);
        srcFile.renameTo(new File(realPath, fileName));
    }

    public static void createIfNotExists(String directoryPath) {

        File directory = new File(directoryPath);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }
}
