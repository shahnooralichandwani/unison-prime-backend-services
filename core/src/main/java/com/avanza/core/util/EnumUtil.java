package com.avanza.core.util;

import com.avanza.core.IEnum;

import java.lang.reflect.Method;

public class EnumUtil {
    public static <ReturnType extends IEnum<Value>, Value> ReturnType getEnum(Class<ReturnType> clazz, Value value) {
        return getEnum(clazz, value, true);
    }

    public static <ReturnType extends IEnum<Value>, Value> ReturnType getEnum(Class<ReturnType> clazz, Value value, boolean ignoreCase) {
        if (!clazz.isEnum()) {
            throw new RuntimeException("Only enum types are supported");
        }
        try {
            Method method = clazz.getMethod("values", (Class<?>[]) null);
            IEnum<Value>[] values = (IEnum<Value>[]) method.invoke(null, (Object[]) null);
            for (IEnum<Value> ienum : values) {
                boolean found = false;
                if (ignoreCase && value instanceof String) {
                    found = ((String) ienum.getValue()).equalsIgnoreCase((String) value);
                } else {
                    found = ienum.getValue().equals(value);
                }

                if (found) {
                    return (ReturnType) ienum;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
