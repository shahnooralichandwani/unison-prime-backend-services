package com.avanza.core.util;

import com.avanza.core.CoreException;
import com.avanza.core.data.DbObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class TreeObject extends DbObject {

    protected static final char NAME_SEPARATOR = '.';

    protected String id;
    private String key;
    protected List<TreeObject> childList;

    public TreeObject() {

        this.childList = new ArrayList<TreeObject>();
    }

    protected TreeObject(String id) {

        this.init(id, -1, null);
    }

    protected TreeObject(String id, List<TreeObject> childList) {

        Guard.checkNull(childList, "TreeObject.TreeObject(String, List<TreeObject");
        this.init(id, -1, childList);
    }

    protected TreeObject(String id, int size) {

        this.init(id, size, null);
    }

    // Methods
    public String getId() {

        return this.id;
    }

    public abstract String getPrimaryName();

    public abstract String getValue();

    public abstract String getSecondaryName();

    public String getKey() {

        if (this.key == null) {

            // Changed to last index of from indexOf
            int idx = this.id.lastIndexOf(this.getSeparator());
            if (idx == -1)
                this.key = id;
            else
                this.key = id.substring(idx + 1);
        }

        return this.key;
    }

    public Iterator<TreeObject> iterator() {

        return this.childList.iterator();
    }

    public int getChildCount() {

        return this.childList.size();
    }

    @SuppressWarnings("unchecked")
    public <T extends TreeObject> List<T> getChildList() {

        ArrayList<T> retList = new ArrayList<T>(this.childList.size());

        for (TreeObject item : this.childList) {
            retList.add((T) item);
        }

        return retList;
    }

    @Override
    public abstract TreeObject clone();

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public abstract boolean equals(Object obj);

    protected abstract TreeObject createObject(String id);

    char getSeparator() {

        return TreeObject.NAME_SEPARATOR;
    }

    @SuppressWarnings("unchecked")
    public <T extends TreeObject> T getChild(String childId) {

        Guard.checkNull(id, "TreeObject.getChild(String)");

        // childId = this.trimId(childId);

        T retVal = null;
        int idx = childId.indexOf(this.getSeparator());

        if (idx == -1) {
            retVal = (T) this.findChild(childId);
        } else {
            String key = childId.substring(0, idx);
            String subName = childId.substring(idx + 1);
            TreeObject parent = this.findChild(key);

            if (parent != null) retVal = (T) parent.getChild(subName);
        }

        return retVal;
    }

    public <T extends TreeObject> void add(T item) {

        String newId = item.getId();

        int idx = newId.indexOf(this.id);

        char separator = this.getSeparator();

        if (idx > -1 && this.id.length() > 0) {

            if ((newId.length() == this.id.length()) && (newId.charAt(this.id.length()) == separator))
                throw new CoreException("Incorrect name [%1$s] for adding as a child of [%2$s]", newId, this.id);

            newId = newId.substring(this.id.length() + 1);
        }

        idx = newId.indexOf(separator);

        if (idx == -1) {

            this.innerAdd(newId, item);
        } else {

            String key = newId.substring(0, idx);
            String parentKey = (this.id.length() == 0) ? key : /* this.id + separator + This is for hierchy view in EForm Add */key;

            TreeObject temp = this.getChild(parentKey);

            if (temp == null) {
                temp = this.createObject(parentKey);
                this.add(temp);
            }

            temp.add(item);
        }
    }

    public boolean remove(String id) {

        int idx = id.indexOf(this.id);

        char separator = this.getSeparator();

        if (idx > -1 && this.id.length() > 0) {

            if ((id.length() == this.id.length()) && (id.charAt(this.id.length()) == separator))
                throw new CoreException("Incorrect name [%1$s] for adding as a child of [%2$s]", id, this.id);

            id = id.substring(this.id.length() + 1);
        }

        idx = id.lastIndexOf(separator);

        if (idx == -1) return this.innerRemove(id);

        // ELSE case

        String key = id.substring(idx + 1);
        String parentKey = (this.id.length() == 0) ? key : this.id + separator + key;

        TreeObject temp = this.getChild(parentKey);

        if (temp != null) return temp.innerRemove(key);

        return false;
    }

    private void init(String id, int size, List<TreeObject> childList) {

        Guard.checkNullOrEmpty(id, "TreeObject.init(String)");

        this.id = id;

        if (childList != null)
            this.childList = childList;
        else if (size > -1)
            this.childList = new ArrayList<TreeObject>(size);
        else
            this.childList = new ArrayList<TreeObject>();
    }

    private String trimId(String childId) {

        int idx = childId.indexOf(this.id);
        int length = this.id.length();

        if (idx >= 0 && length > 0) childId = childId.substring(length + 1);

        return childId;
    }

    private TreeObject findChild(String key) {

        TreeObject retVal = null;

        for (TreeObject item : this.childList) {

            if (item.getKey().compareTo(key) == 0) {
                retVal = item;
                break;
            }
        }

        return retVal;
    }

    protected <T extends TreeObject> void innerAdd(String newId, T item) {

        TreeObject temp = this.getChild(newId);

        if (temp != null)
            temp.copyValues(item);
        else
            this.childList.add(item);
    }

    protected boolean innerRemove(String key) {

        TreeObject temp = this.getChild(key);

        if (temp != null) return this.childList.remove(temp);

        return false;
    }

    public boolean removeChild(String key) {

        TreeObject temp = getChildFromList(key);

        if (temp != null) return this.childList.remove(temp);

        return false;
    }

    private TreeObject getChildFromList(String key) {
        for (TreeObject obj : childList) {
            if (obj.getId().equalsIgnoreCase(key)) return obj;
        }
        return null;
    }


}
