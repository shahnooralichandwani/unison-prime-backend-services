package com.avanza.core.json;

import java.util.HashMap;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.avanza.core.serialized.Parser;
import com.avanza.core.serialized.SerializedAttribute;
import com.google.gson.Gson;

public class JsonParser implements Parser {
    public String getColumnName() {
        return JsonKeyLookup.JSON_COLUMN;
    }

    public String serialize(HashMap<String, SerializedAttribute> serializedAttribMap) {
        return new Gson().toJson(serializedAttribMap);
    }


    public HashMap<String, SerializedAttribute> deSerialize(String jsonString) {

        ObjectMapper mapper = new ObjectMapper();
        HashMap<String, SerializedAttribute> userFromJSON = null;

        try {
            userFromJSON =
                    mapper.readValue(jsonString, new TypeReference<HashMap<String, SerializedAttribute>>() {
                    });


        } catch (Exception e) {
            e.printStackTrace();
        }

        return userFromJSON;

//		Type type = new TypeToken<HashMap<String, SerializedAttribute>>(){}.getType();
//		return (HashMap<String, SerializedAttribute>)new Gson().fromJson(jsonString,type);
    }


}
