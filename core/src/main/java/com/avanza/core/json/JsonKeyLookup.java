package com.avanza.core.json;

public final class JsonKeyLookup {
    public static String JSON_ATTRIB = "JSONData";
    public static final String JSON_SERIALIZATION = "JSON";
    public static String JSON_COLUMN = "JSON_Data";
}
