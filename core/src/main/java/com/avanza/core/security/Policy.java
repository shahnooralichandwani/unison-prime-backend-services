package com.avanza.core.security;

import java.util.List;

import com.avanza.core.data.DbObject;
import com.avanza.core.security.db.UserDbImpl;

public abstract class Policy extends DbObject {

    // Fields

    private String policyId;
    private String implClass;
    private String policyNamePrm;
    private String policyNameSec;
    private Integer passwordExpiryDuration;
    private String passwordExpression;
    private Integer failureCount;
    private Integer idleDuration;
    private boolean multiLogin;
    private boolean defaultPolicy;
    private Integer passwordReuseCount;
    private Integer passwordLength;
    private String invalidWords;
    private boolean changeDefaultPassword;
    private String passwordFailMsgKeys;
    private boolean selected;


    // Constructors

    /**
     * default constructor
     */
    protected Policy() {

    }

    // Property accessors


    public String getPolicyId() {
        return policyId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getImplClass() {
        return implClass;
    }

    public void setImplClass(String implClass) {
        this.implClass = implClass;
    }

    public String getPolicyNamePrm() {
        return policyNamePrm;
    }

    public void setPolicyNamePrm(String policyNamePrm) {
        this.policyNamePrm = policyNamePrm;
    }

    public String getPolicyNameSec() {
        return policyNameSec;
    }

    public void setPolicyNameSec(String policyNameSec) {
        this.policyNameSec = policyNameSec;
    }

    public Integer getPasswordExpiryDuration() {
        return passwordExpiryDuration;
    }

    public void setPasswordExpiryDuration(Integer passwordExpiryDuration) {
        this.passwordExpiryDuration = passwordExpiryDuration;
    }

    public String getPasswordExpression() {
        return passwordExpression;
    }

    public void setPasswordExpression(String passwordExpression) {
        this.passwordExpression = passwordExpression;
    }

    public Integer getFailureCount() {
        return failureCount;
    }

    public void setFailureCount(Integer failureCount) {
        this.failureCount = failureCount;
    }

    public Integer getIdleDuration() {
        return idleDuration;
    }

    public void setIdleDuration(Integer idleDuration) {
        this.idleDuration = idleDuration;
    }

    public boolean isMultiLogin() {
        return multiLogin;
    }

    public void setMultiLogin(boolean isMultiLogin) {
        this.multiLogin = isMultiLogin;
    }

    public boolean isDefaultPolicy() {
        return defaultPolicy;
    }

    public void setDefaultPolicy(boolean defaultPolicy) {
        this.defaultPolicy = defaultPolicy;
    }

    public Integer getPasswordReuseCount() {
        return passwordReuseCount;
    }

    public void setPasswordReuseCount(Integer passwordReuseCount) {
        this.passwordReuseCount = passwordReuseCount;
    }

    public Integer getPasswordLength() {
        return passwordLength;
    }

    public void setPasswordLength(Integer passwordLength) {
        this.passwordLength = passwordLength;
    }

    public String getInvalidWords() {
        return invalidWords;
    }

    public void setInvalidWords(String invalidWords) {
        this.invalidWords = invalidWords;
    }

    public boolean isChangeDefaultPassword() {
        return changeDefaultPassword;
    }

    public void setChangeDefaultPassword(boolean changeDefaultPassword) {
        this.changeDefaultPassword = changeDefaultPassword;
    }

    // abstract methods
    public abstract List<UserDbImpl> getUsers();

    public abstract void addUser(UserDbImpl user);

    public abstract void removeUser(UserDbImpl user);

    public String getPasswordFailMsgKeys() {
        return passwordFailMsgKeys;
    }

    public void setPasswordFailMsgKeys(String passwordFailMsgKeys) {
        this.passwordFailMsgKeys = passwordFailMsgKeys;
    }
}