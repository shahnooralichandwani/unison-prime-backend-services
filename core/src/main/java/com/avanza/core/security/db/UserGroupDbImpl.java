package com.avanza.core.security.db;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.security.PermissionBinding;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.util.StringHelper;

/**
 * This class is the DB Specific Implementation of the UserGroup Class. This class is mapped to the Hibernate mapping file.
 *
 * @author kraza
 */
public class UserGroupDbImpl extends UserGroup {

    public static final String PACKAGE_NAME = UserDbImpl.class.getPackage().getName();

    private Set<GroupUserBinding> userBindings;
    private Set<GroupRelationBinding> parentGroupBindings;
    private Set<GroupRelationBinding> childGroupBindings;
    // private Set<User> users = new HashSet<User>();

    private Set<GroupPermissionBinding> groupPermissions;

    public UserGroupDbImpl() {

        super();
    }

    public UserGroupDbImpl(String groupId) {

        super();
        this.setUserGroupId(groupId);
    }


    public Set<GroupRelationBinding> getChildGroupBindings() {
        return childGroupBindings;
    }

    public void setChildGroupBindings(Set<GroupRelationBinding> childGroupBindings) {
        this.childGroupBindings = childGroupBindings;
    }

    public Set<GroupPermissionBinding> getGroupPermissionBindings() {
        return this.groupPermissions;
    }

    public void setGroupPermissionBindings(Set<GroupPermissionBinding> groupPermissions) {
        this.groupPermissions = groupPermissions;
    }

    /**
     * returns collection of GroupRelationBinding where this group is defined as a child..
     *
     * @return
     */
    public Set<GroupRelationBinding> getParentGroupBindings() {
        return this.parentGroupBindings;
    }

    public void setParentGroupBindings(Set<GroupRelationBinding> parentGroupBindings) {
        this.parentGroupBindings = parentGroupBindings;
    }

    public Set<GroupUserBinding> getUserBindings() {
        return this.userBindings;
    }

    public void setUserBindings(Set<GroupUserBinding> userBindings) {
        this.userBindings = userBindings;
    }


    @Override
    protected void fillPermissions() {

        if (this.permissionTree != null)
            return;

        this.permissionTree = new PermissionBinding(StringHelper.EMPTY);

        for (GroupPermissionBinding item : this.groupPermissions)
            this.permissionTree.add(item.getBinding());
    }

    /**
     * This method need to be fixed so that it will pick the users from the cache.
     */
    @Override
    public Set<User> getUsers() {


        Set<User> grpUsers = new HashSet<User>();

        DataBroker dataBroker = DataRepository.getBroker(UserGroupDbImpl.PACKAGE_NAME);

        Search search = new Search(GroupUserBinding.class);
        search.addCriterion(SimpleCriterion.equal("userGroupId", getUserGroupId()));
        List<GroupUserBinding> groupBindings = dataBroker.find(search);

        for (GroupUserBinding grpBinding : groupBindings) {
            try {
                User usr = dataBroker.findById(UserDbImpl.class, grpBinding.getUserId());
                grpUsers.add(usr);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return grpUsers;

        // return this.users;
    }

    @Override
    public void removeUser(String loginId) {

        for (GroupUserBinding gUserBind : this.getUserBindings()) {

            if (gUserBind.getUserId().equals(loginId)) {
                this.getUserBindings().remove(gUserBind);
                break;
            }
        }
    }

    /**
     * This method gets the UserGroup from the DB This is the DB specfic Impl TODO: Remove Replication of code.
     */
    @Override
    public UserGroup getUserGroup(String userGroupId) {

        UserGroup userGroups = DataRepository.getBroker(UserDbImpl.PACKAGE_NAME).findById(UserGroupDbImpl.class, userGroupId);
        return userGroups;
    }

    /**
     * This method fills the UserGroup from the DB This is the DB specfic Impl
     */
    @Override
    protected HashSet<UserGroup> fillGroups() {

        DataBroker dataBroker = DataRepository.getBroker(UserDbImpl.PACKAGE_NAME);

        HashSet<UserGroup> userGroupsList = new HashSet<UserGroup>();

        Search search = new Search(GroupRelationBinding.class);
        search.addCriterion(SimpleCriterion.equal("parentGroup", getUserGroupId()));
        List<GroupRelationBinding> groupBindings = dataBroker.find(search);

        for (GroupRelationBinding grpBinding : groupBindings) {
            try {
                UserGroup usrGrp = dataBroker.findById(UserGroupDbImpl.class, grpBinding.getChildGroup());
                userGroupsList.add(usrGrp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return userGroupsList;
    }

    public void Persist(DataBroker broker) {
        broker.persist(this);


        // persist New User groups added to the user
        if (this.addedGroups != null) {

            for (UserGroup userGroup : this.addedGroups) {

                GroupRelationBinding grpBinding = new GroupRelationBinding(this.getUserGroupId(), userGroup.getUserGroupId());
                if (this.childGroupBindings == null)
                    this.childGroupBindings = new HashSet<GroupRelationBinding>();
                this.childGroupBindings.add(grpBinding);
                broker.add(grpBinding);
            }
        }

        // delete user group association that are removed
        if (this.removedGroups != null) {

            for (UserGroup userGroup : this.removedGroups) {

                GroupRelationBinding grpBinding = null;
                String relGroupId = userGroup.getUserGroupId();

                for (GroupRelationBinding gItem : this.childGroupBindings) {
                    if (gItem.getChildGroup().compareTo(relGroupId) == 0) {
                        grpBinding = gItem;
                        break;
                    }
                }

                this.childGroupBindings.remove(grpBinding);
                broker.delete(grpBinding);
            }
        }

        // Persist the permissions for GROUP PERMISSION BINDINGS in the userGroup.
        if (this.addedPermissions != null) {

            for (PermissionBinding permission : this.addedPermissions.values()) {
                GroupPermissionBinding grpPermBinding = null;

                grpPermBinding = broker.findById(GroupPermissionBinding.class, new GroupPermissionId(permission.getPermissionId(), permission
                        .getAppId(), this.userGroupId));

                if (grpPermBinding != null) {
                    grpPermBinding.setCanCreate(permission.getCanCreate());
                    grpPermBinding.setCanUpdate(permission.getCanUpdate());
                    grpPermBinding.setCanDelete(permission.getCanDelete());
                    broker.update(grpPermBinding);
                } else {
                    grpPermBinding = new GroupPermissionBinding(this.userGroupId, permission);
                    if (this.getGroupPermissionBindings() == null)
                        this.setGroupPermissionBindings((new HashSet()));
                    this.getGroupPermissionBindings().add(grpPermBinding);
                    broker.add(grpPermBinding);
                }
            }
        }

        // delete user group association that are removed
        if (this.removedPermissions != null) {

            for (PermissionBinding permission : this.removedPermissions.values()) {

                GroupPermissionBinding grpPermBinding = null;
                String relId = permission.getPermissionId();

                for (GroupPermissionBinding pItem : this.groupPermissions) {
                    if (pItem.getPermissionId().compareTo(relId) == 0) {
                        grpPermBinding = pItem;
                        break;
                    }
                }

                if (grpPermBinding != null) {
                    //this.groupPermissions.remove(grpPermBinding);
                    broker.delete(grpPermBinding);
                }


            }
        }

        this.synchronizeGroups();
        this.syncPermissions();
    }

    private void syncPermissions() {

        if (this.addedPermissions != null) {
            if (this.permissionTree == null)
                this.permissionTree = new PermissionBinding(0);
            for (PermissionBinding item : this.addedPermissions.values()) {

                this.permissionTree.add(item);
            }

            this.addedPermissions.clear();
        }


        if (this.removedPermissions != null) {

            for (PermissionBinding item : this.addedPermissions.values()) {

                this.permissionTree.remove(item.getPermissionId());
            }

            this.removedPermissions.clear();
        }
    }

    /*
     * public void setUsers(Set<User> users) { this.users = users; }
     */

    /*
     * @Override protected void innerSyncAdd(UserGroup group) {
     *
     * GroupRelationBinding grpBinding = new GroupRelationBinding(this.getUserGroupId(), group.getUserGroupId());
     *
     * this.childGroupBindings.add(grpBinding); }
     *
     *
     * @Override protected void innerSyncRemove(UserGroup group) {
     *
     * GroupRelationBinding grpBinding = null; String relGroupId = group.getUserGroupId();
     *
     * for(GroupRelationBinding gItem: this.childGroupBindings) { if(gItem.getChildGroup().compareTo(relGroupId) == 0) { grpBinding = gItem; break; } }
     *
     * this.childGroupBindings.remove(grpBinding); }
     */
}