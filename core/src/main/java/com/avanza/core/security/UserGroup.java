package com.avanza.core.security;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public abstract class UserGroup extends GroupHelper implements Comparable {

    // Fields
    protected String userGroupId;

    protected String primaryName;

    protected String secondaryName;

    protected PermissionBinding permissionTree;


    protected HashMap<String, PermissionBinding> addedPermissions;

    protected HashMap<String, PermissionBinding> removedPermissions;

    // Constructors


    public HashMap<String, PermissionBinding> getAddedPermissions() {
        return addedPermissions;
    }


    public HashMap<String, PermissionBinding> getRemovedPermissions() {
        return removedPermissions;
    }

    /**
     * default constructor
     */
    protected UserGroup() {

        super();
        this.addedPermissions = new HashMap<String, PermissionBinding>();
        this.removedPermissions = new HashMap<String, PermissionBinding>();
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getSecondaryName() {
        return secondaryName;
    }

    public void setSecondaryName(String secondaryName) {
        this.secondaryName = secondaryName;
    }

    public String getUserGroupId() {
        return this.userGroupId;
    }

    protected abstract void fillPermissions();

    public abstract Set<User> getUsers();

    public abstract void removeUser(String loginId);

    public List<PermissionBinding> getPermissions() {

        this.fillPermissions();
        return this.permissionTree.getPermissions();
    }

    /**
     * This method add the permission to the UserGroup to its temporary location
     * if not originally exist else copy the values to the contained binding
     */
    public void addPermission(PermissionBinding permission) {

        String id = permission.getPermissionId();

        this.removedPermissions.remove(id);
        if (this.permissionTree != null) {
            PermissionBinding temp = this.permissionTree.getPermission(id);

            if (temp != null) {
                temp.copyValues(permission);
                //return;
            }
        }

        this.addedPermissions.put(id, permission);
    }

    /**
     * This method removes the permission from the temporary storage.
     */
    public void removePermission(PermissionBinding permission) {

        String id = permission.getPermissionId();

        if (this.addedPermissions.remove(id) != null)
            return;
        try {
            PermissionBinding temp = this.permissionTree.getPermission(id);


            if (temp != null)
                this.removedPermissions.put(id, permission);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addPermission(List<PermissionBinding> permissionList) {

        if (permissionList == null)
            return;

        for (PermissionBinding item : permissionList) {
            this.addPermission(item);
        }
    }

    public void selectBindings(PermissionBinding toSelect) {

        this.fillPermissions();

        this.permissionTree.selectBindings(toSelect);
    }

    @Override
    public int hashCode() {

        return this.userGroupId.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (this.getClass() != obj.getClass())
            return false;

        final UserGroup other = (UserGroup) obj;

        return (this.userGroupId.compareTo(other.userGroupId) == 0);
    }

    protected void setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
    }

    public int compareTo(Object arg0) {

        return this.getPrimaryName().compareToIgnoreCase(((UserGroup) arg0).getPrimaryName());

    }

}