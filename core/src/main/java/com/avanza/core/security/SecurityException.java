package com.avanza.core.security;

import com.avanza.core.CoreException;

public class SecurityException extends CoreException {

    private static final long serialVersionUID = -8114301646092330303L;

    public SecurityException(String message) {

        super(message);
    }

    public SecurityException(String format, Object... args) {

        super(format, args);
    }

    public SecurityException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public SecurityException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public SecurityException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public SecurityException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}
