package com.avanza.core.security.auditTables.logger;

import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.security.audit.ActivityMessagesCatalog;
import com.avanza.core.util.Logger;

/**
 * Singleton logger for saving audit tables
 *
 * @author arsalan.ahmed
 */
public class SimpleAuditableLogger implements AuditableLogger {

    private static final Logger logger = Logger.getLogger(SimpleAuditableLogger.class);

    private static SimpleAuditableLogger simpleLogger = null;

    private SimpleAuditableLogger() {

    }

    public static SimpleAuditableLogger getLogger() {
        if (simpleLogger == null)
            simpleLogger = new SimpleAuditableLogger();

        return simpleLogger;
    }


    @Override
    public void logEntries(List<Object> entries) {
        try {
            for (Object auditTable : entries) {
                DataBroker broker = DataRepository.getBroker(auditTable.getClass().getName());

                logger.logDebug("Saving audit table: " + auditTable.getClass().getName());
                broker.add(auditTable);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.logError("Error occur while saving audit table", e);
        }

    }
}
