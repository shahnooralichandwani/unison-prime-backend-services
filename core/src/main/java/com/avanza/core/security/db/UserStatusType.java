package com.avanza.core.security.db;

import com.avanza.core.CoreException;


public enum UserStatusType {

    Active("0"), InActive("1"), All("2");

    private String value;

    private UserStatusType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static UserStatusType fromString(String value) {

        UserStatusType retVal = null;

        if (value.equalsIgnoreCase("0"))
            retVal = UserStatusType.Active;
        else if (value.equalsIgnoreCase("1"))
            retVal = UserStatusType.InActive;
        else if (value.equalsIgnoreCase("2"))
            retVal = UserStatusType.All;
        else
            throw new CoreException("[%1$s] is not recognized as UserStatusType", value);

        return retVal;
    }

}
