package com.avanza.core.security;

//76: InvalidRole and InvalidGroup  added Osama
public enum AuthenticationStatus {

    Success, InActiveUser, InvalidPassword, InvalidUser, PasswordNearExpiry, InvalidRole, InvalidGroup,
    PasswordExpired, InvalidPasswordFormat, ProviderError
}