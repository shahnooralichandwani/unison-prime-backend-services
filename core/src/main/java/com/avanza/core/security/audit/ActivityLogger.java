package com.avanza.core.security.audit;

import java.util.Date;

import com.avanza.core.CoreException;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.constants.ThreadContextLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.ExecutionContext;
import com.avanza.core.function.ContextFunction;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.security.User;
import com.avanza.core.util.CounterUtils;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.config.WebContext;

/**
 * @author kraza
 */
public class ActivityLogger implements ActivityLogging {

    private static final Logger logger = Logger.getLogger(ActivityLogger.class);
    private static ActivityLogger _instance = new ActivityLogger();
    private static final Logger sysLogger = Logger.getLogger("sysLogger");

    protected ActivityLogger() {
    }

    static ActivityLogger getInstance() {

        return _instance;
    }

    public void beginActivityLogging() {

        //Shuwair for dublicate  id issue
        try {
            WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
            User user = webContext.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
            String activitySessionKey = CounterUtils.getNextActivitySessionKey();
            ActivityLog activityLog = new ActivityLog();
            activityLog.setActivitySessionKey(activitySessionKey);
            activityLog.setLoginId(user.getLoginId());
            activityLog.setLoginTime(new Date());
            activityLog.setCreation(user.getLoginId());

            DBActivityLogger.getInstance().addActivityLog(activityLog);

            webContext.setAttribute(SessionKeyLookup.ACTIVITY_LOG_KEY, activityLog);
        } catch (Exception e) {
            logger.LogException("Exception occoured while beginActivityLogging().", e);
        }
    }

    public void endActivityLogging() {

        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        User user = webContext.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
        ActivityLog activityLog = webContext.getAttribute(SessionKeyLookup.ACTIVITY_LOG_KEY);

        if (checkValidity(user, activityLog, "Ending the ActivityLog Session")) {
            activityLog.setLogoutTime(new Date());
            activityLog.setUpdation(user.getLoginId());
            DBActivityLogger.getInstance().updateActivityLog(activityLog);
        }

        webContext.removeAttribute(SessionKeyLookup.ACTIVITY_LOG_KEY);
    }

    private Boolean checkValidity(User user, ActivityLog activityLog, String operation) {

        if (activityLog == null)
            return false;
        //throw new CoreException("Exception while " + operation + ", Null ActivityLog found first begin ActivityLogging.");


        if (!activityLog.getLoginId().equalsIgnoreCase(user.getLoginId()))
            return false;
            /*throw new CoreException("Exception while " + operation + ", Login Id mismatched found %1$s while expected %2$s.", activityLog
                    .getLoginId(), user.getLoginId());*/

        return true;
    }

    @Deprecated
    public void logDetail() {

        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        if (webContext == null)
            return;
        User user = webContext.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
        ActivityLog activityLog = webContext.getAttribute(SessionKeyLookup.ACTIVITY_LOG_KEY);
        ActivityOperationType operationType = ApplicationContext.getContext().get(ThreadContextLookup.ActivityOperationType.getValue());
        String moduleId = ApplicationContext.getContext().get(ThreadContextLookup.ModuleId.getValue());
        String metaEntityId = ContextFunction.executionContextValue(ExecutionContext.META_ENTITY_ID);
        MetaEntity metaEnt = MetaDataRegistry.getEntityBySystemName(metaEntityId);
        if (metaEnt != null)
            metaEntityId = metaEnt.getId();
        String description = ApplicationContext.getContext().get(ThreadContextLookup.ActivityDescription.getValue());

        /* Nasir changes for activity logging*/
        String indicator = ApplicationContext.getContext().get(ThreadContextLookup.Indicator.getValue());
        /*if (StringHelper.isEmpty(moduleId) || operationType == null)
            return;
*/
        if (operationType == null)
            return;
        /* Nasir changes end here*/

        checkValidity(user, activityLog, "Logging Activity Detail");

        ActivityLogDetail activityLogDetail = new ActivityLogDetail();
        activityLogDetail.setActivityLog(activityLog);
        activityLogDetail.setActivitySessionKey(activityLog.getActivitySessionKey());
        activityLogDetail.setMetaEntityId(metaEntityId);
        activityLogDetail.setModuleId(moduleId);
        activityLogDetail.setOperationType(operationType.toString());
        activityLogDetail.setDescription(description);
        activityLogDetail.setActivityTime(new Date());
        activityLogDetail.setCreation(user.getLoginId());
        activityLogDetail.setIndicator(indicator);

        //DBActivityLogger.getInstance().addActivityLogDetail(activityLogDetail);
        sysLogger.logInfo(user.getFullName() + "," + activityLogDetail.getOperationType() + "," +
                activityLogDetail.getActivityTime() + "," + activityLogDetail.getIndicator() + "," +
                activityLogDetail.getDescription());

        webContext.setAttribute(SessionKeyLookup.ACTIVITY_LOG_DETAIL_KEY, activityLogDetail);
    }

    public void prepareLogDetail(ActivityOperationType activityOperationType, String description, String moduleId) {
        ApplicationContext.getContext().add(ThreadContextLookup.ActivityOperationType.getValue(), activityOperationType);
        ApplicationContext.getContext().add(ThreadContextLookup.ActivityDescription.getValue(), description);
        ApplicationContext.getContext().add(ThreadContextLookup.ModuleId.getValue(), moduleId);
    }

    public void prepareLogDetail(ActivityOperationType activityOperationType, String description) {
        ApplicationContext.getContext().add(ThreadContextLookup.ActivityOperationType.getValue(), activityOperationType);
        ApplicationContext.getContext().add(ThreadContextLookup.ActivityDescription.getValue(), description);
    }

    public void prepareLogDetail(String moduleId) {
        ApplicationContext.getContext().add(ThreadContextLookup.ModuleId.getValue(), moduleId);
    }
}
