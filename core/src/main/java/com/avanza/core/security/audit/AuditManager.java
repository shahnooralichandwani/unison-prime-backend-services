/**
 *
 */
package com.avanza.core.security.audit;

import com.avanza.core.util.Logger;

/**
 * @author shahbaz.ali
 * This Class provides audit funciotnality while doing activity on workflow process 
 */


public class AuditManager {

    private static final Logger logger = Logger.getLogger(AuditManager.class);

    //TODO: UNISONPRIME
   /* public static void saveChangeLog(WorkflowLog wlog, ProcessContext processContext) {

	    logger.logInfo(String.format("[Saving change logs..., AuditManager.saveChangeLog ]"));
	    
		try{
		 	    WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
    		    User user = ctx.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
            	    Map<MetaViewAttribute,String> oldValueMap=ctx.getAttribute(SessionKeyLookup.EDITABLE_CONTROLS);
            	    	
            	    	if(oldValueMap!=null){
            	    	    
                    		for (Map.Entry<MetaViewAttribute, String> entry: oldValueMap.entrySet()) {
                            		 String oldValue=  entry.getValue();
                            		 String newValue=  (String) processContext.getAttribute(entry.getKey().getMetaAttribSystemName());
                            		 
                            		 String pickListId= entry.getKey().getMetaAttribute().getPickListId();
                            		 
                            		 if(StringHelper.isNotEmpty(pickListId)){
                            		     newValue= PickListManager.getPickListValue(newValue, pickListId)[0];
                            		 }
                    			
                            		 AuditLog log= new AuditLog();
                            		 log.setAuditLogId((String) MetaDataRegistry.getNextCounterValue("AuditLogCounter").toString());
                            		 log.setWorkflowLog(wlog.getLogId());
                            		 log.setMetaViewAttribId(entry.getKey().getId());
                            		 log.setLoginId(user.getLoginId());
                            		 log.setOldValue(oldValue);
                            		 log.setNewValue(newValue);
                            		 DataBroker broker= DataRepository.getBroker(AuditLog.class.getName());
                            		 broker.add(log);
            			}
            	    	}
       		
		}catch (Exception e) {
		    logger.LogException("Exception occured, cannot log changes while persisting the document, AuditManager.saveChangeLog", e);
		}
		
		
	    
	}*/

}
