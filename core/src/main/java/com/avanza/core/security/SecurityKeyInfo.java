package com.avanza.core.security;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import com.avanza.core.util.StringHelper;

public class SecurityKeyInfo {
    private String algorithm;
    private Charset encoding;
    private KeyPair ourKeyPair;
    private PublicKey theirPublicKey;

    public SecurityKeyInfo(String keystorePath, String keystorePassword,
                           String ourKeyPassword, String ourKeyAlias, String theirKeyAlias,
                           String algorithm, String encoding) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException {

        this.algorithm = algorithm;
        this.encoding = (StringHelper.isEmpty(encoding)) ? Charset
                .defaultCharset() : Charset.forName(encoding);

        KeyStore keyStore = loadStore(keystorePath, keystorePassword);

        //our key pair
        PrivateKey privateKey = (PrivateKey) keyStore.getKey(ourKeyAlias, ourKeyPassword.toCharArray());
        Certificate cert = keyStore.getCertificate(ourKeyAlias);
        ourKeyPair = new KeyPair(cert.getPublicKey(), privateKey);

        //their public key
        theirPublicKey = keyStore.getCertificate(ourKeyAlias).getPublicKey();

    }

    public String getAlgorithm() {
        return algorithm;
    }

    public Charset getEncoding() {
        return encoding;
    }

    public KeyPair getOurKeyPair() {
        return ourKeyPair;
    }

    public PublicKey getPublicKey() {
        return theirPublicKey;
    }

    private static KeyStore loadStore(String keystorePath,
                                      String keystorePassword) throws NoSuchAlgorithmException, CertificateException, FileNotFoundException, IOException, KeyStoreException {

        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(new FileInputStream(keystorePath), keystorePassword
                .toCharArray());
        return keyStore;

    }

    public static void main(String[] args) {

        try {
            SecurityKeyInfo info;
            info = new SecurityKeyInfo("E:/unisondibkeys/awkeystore.jks", "password", "password", "awahid", "middleware", null, null);
            PrivateKey key = info.getOurKeyPair().getPrivate();
        } catch (UnrecoverableKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (CertificateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
}
