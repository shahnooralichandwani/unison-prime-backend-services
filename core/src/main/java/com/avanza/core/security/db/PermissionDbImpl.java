package com.avanza.core.security.db;

import java.io.Serializable;
import java.util.Set;

import com.avanza.core.security.Permission;
import com.avanza.core.security.PermissionType;

public class PermissionDbImpl extends Permission implements Serializable {

    private PermissionId id;
    private String parentPermissionId;
    private String key;
    private Permission parent;
    private Set<PermissionDbImpl> childPermissions;
    private String permissionUrl;
    private Boolean isMenu;
    private Boolean isWelcomeMenu;
    private String smallIconURL;
    private String largeIconURL;
    private String menuURL;
    private String welcomeMeunURL;
    private String menuType;
    private Integer position;
    private String urlParameterExp;

    public PermissionDbImpl() {
    }

    protected PermissionDbImpl(PermissionId id) {

        this.id = id.clone();
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public Boolean getIsMenu() {
        return (isMenu == null) ? false : isMenu;
    }

    public void setIsMenu(Boolean isMenu) {
        this.isMenu = isMenu;
    }

    public Boolean getIsWelcomeMenu() {
        return (isWelcomeMenu == null) ? false : isWelcomeMenu;
    }

    public void setIsWelcomeMenu(Boolean isWelcomeMenu) {
        this.isWelcomeMenu = isWelcomeMenu;
    }

    public String getSmallIconURL() {
        return smallIconURL;
    }

    public void setSmallIconURL(String smallIconURL) {
        this.smallIconURL = smallIconURL;
    }

    public String getLargeIconURL() {
        return largeIconURL;
    }

    public void setLargeIconURL(String largeIconURL) {
        this.largeIconURL = largeIconURL;
    }

    public String getMenuURL() {
        return menuURL;
    }

    public void setMenuURL(String menuURL) {
        this.menuURL = menuURL;
    }

    public String getWelcomeMeunURL() {
        return welcomeMeunURL;
    }

    public void setWelcomeMeunURL(String welcomeMeunURL) {
        this.welcomeMeunURL = welcomeMeunURL;
    }

    public Permission clone() {

        PermissionDbImpl retVal = new PermissionDbImpl(this.id);

        // Setting properties for base class
        retVal.canCreate = this.canCreate;
        retVal.canDelete = this.canDelete;
        retVal.canUpdate = this.canUpdate;
        retVal.key = this.key;

        retVal.type = this.type;
        retVal.primaryName = this.primaryName;
        retVal.secondaryName = this.secondaryName;

        retVal.parentPermissionId = this.parentPermissionId;
        retVal.parent = this.parent;
        retVal.permissionUrl = this.permissionUrl;

        if (this.childList != null) {
            retVal.childList = this.childList.clone();
        }

        return retVal;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public void setSecondaryName(String secondaryName) {
        this.secondaryName = secondaryName;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public boolean equals(Object other) {

        if ((this == other))
            return true;

        if ((other == null))
            return false;

        if (!(other instanceof PermissionDbImpl))
            return false;

        PermissionDbImpl castOther = (PermissionDbImpl) other;
        return this.id.equals(castOther.id);
    }

    @Override
    public String getAppId() {
        return this.id.getAppId();
    }

    @Override
    public String getPermissionId() {
        return this.id.getPermissionId();
    }

    public void setId(PermissionId id) {
        this.id = id;
    }

    public PermissionId getId() {
        return this.id;
    }

    public void setChildPermissions(Set<PermissionDbImpl> childPermissions) {

        this.childPermissions = childPermissions;
    }

    public void setPermissionType(String permissionType) {

        if (PermissionType.ID_MODULE.compareToIgnoreCase(permissionType) == 0)
            this.type = PermissionType.Module;
        else if (PermissionType.ID_TRANSACTION.compareToIgnoreCase(permissionType) == 0)
            this.type = PermissionType.Transaction;
        else if (PermissionType.ID_ACTION.compareToIgnoreCase(permissionType) == 0)
            this.type = PermissionType.Action;
        else
            this.type = PermissionType.None;
    }

    public String getPermissionUrl() {
        return permissionUrl;
    }

    public void setPermissionUrl(String permissionUrl) {
        this.permissionUrl = permissionUrl;
    }

    public String getUrlParameterExp() {
        return urlParameterExp;
    }

    public void setUrlParameterExp(String urlParameterExp) {
        this.urlParameterExp = urlParameterExp;
    }


}