package com.avanza.core.security.db;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Search;
import com.avanza.core.security.AuthenticationProvider;
import com.avanza.core.security.AuthenticationStatus;
import com.avanza.core.security.ProviderFactory;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.web.config.WebContext;


public class LDAPSecurityProvider implements AuthenticationProvider {

    private static final Logger logger = Logger.getLogger(LDAPSecurityProvider.class);

    private static final String LDAP_CONTEXT_FACTORY = "contextFactory";
    private static final String LDAP_PROVIDER_URL = "providerURL";
    private static final String LDAP_SECURITY_PRINCIPAL = "userName";
    private static final String LDAP_SECURITY_CREDENTIALS = "password";
    private static final String SSO_USER_BASE_PATH = "searchBase";
    private static final String OBJECT_CLASS = "objectClass";
    private static final String LOGIN_FILTER_NAME = "loginFilter";

    private String initialContextFactory;
    private String providerURL;
    private String securityPrincipal;
    private String securityCredentials;
    private String baseSearch;
    private String objectClass;
    private String loginFilterName;

    private Hashtable env;
    private Map<String, String> attributeMap = new HashMap<String, String>(0);

    public void load(ConfigSection section) {
        if (section == null) {
            // TODO : Throw exception
            throw new SecurityException("LDAP Configuration not found");
        } else {
            initialContextFactory = section.getValue(LDAP_CONTEXT_FACTORY, StringHelper.EMPTY);
            providerURL = section.getValue(LDAP_PROVIDER_URL, StringHelper.EMPTY);
            securityPrincipal = section.getValue(LDAP_SECURITY_PRINCIPAL, StringHelper.EMPTY);
            securityCredentials = section.getValue(LDAP_SECURITY_CREDENTIALS, StringHelper.EMPTY);
            baseSearch = section.getValue(SSO_USER_BASE_PATH, StringHelper.EMPTY);
            objectClass = section.getValue(OBJECT_CLASS, StringHelper.EMPTY);
            loginFilterName = section.getValue(LOGIN_FILTER_NAME, StringHelper.EMPTY);
            env = null;

            //preparing the attriibute map.
            List<ConfigSection> childs = section.getChildSections("attribute");
            for (ConfigSection cfgSection : childs) {
                attributeMap.put(cfgSection.getTextValue("ldap-attribute"), cfgSection.getTextValue("user-attribute"));
            }

            try {
                getDefaultDirContext();
            } catch (NamingException ex) {
                throw new com.avanza.core.security.SecurityException(ex, ex.getMessage());
            }
        }
    }

    public void dispose() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean userExists(String loginId) {
        return false;
    }

    public AuthenticationStatus authenticate(String loginId, String password) {

        AuthenticationStatus status = AuthenticationStatus.InvalidPassword;

        loginId = loginId.trim();

        //Is EOD processing Started?
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class.getName());
        if (webContext != null)
            webContext.setAttribute("eodProcessingStarted", !DataRepository.isPrimarySource("CBDBASE"));

        // Whether that user exist in Unison or not by checking login Id in our SEC_USER table
        UserDbImpl usr = (UserDbImpl) SecurityManager.getUser(loginId);

        if (usr == null)
            return AuthenticationStatus.InvalidUser;
        else if (!usr.isActive())
            return AuthenticationStatus.InActiveUser;

        if (usr.isSystem()) {
            return ProviderFactory.getLocalAuthenProvider().authenticate(loginId, password);
        }

        LdapContext ctx = null;
        try {

            ctx = new InitialLdapContext(env, null);

            String filter = "(&(objectClass={0})({1}={2}))";
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            searchControls.setReturningAttributes(new String[0]);
            searchControls.setReturningObjFlag(true);
            NamingEnumeration namingEnumeration = ctx.search(baseSearch, filter, new String[]{this.objectClass, loginFilterName, loginId}, searchControls);

            String dn = null;
            if (namingEnumeration.hasMore()) {
                SearchResult result = (SearchResult) namingEnumeration.next();
                dn = result.getNameInNamespace();

                logger.logDebug("dn: " + dn);
            }

            if (dn == null) {
                // uid not found or not unique
                return AuthenticationStatus.InvalidUser;
            }

            try {
                // Authenticate with Directory
                logger.logDebug("Authenticating user: " + dn);
                DirContext dirContext = this.getDirectoryContext(dn, password);

            } catch (AuthenticationException authEx) {

                throw new AuthenticationException(" Invalid Password ");
            }

            logger.logDebug("Authentication successful. User:" + loginId);
            status = AuthenticationStatus.Success;

        } catch (AuthenticationException authex) {
            logger.logError("Invalid user id or password", authex);
            status = AuthenticationStatus.InvalidPassword;
        } catch (NameAlreadyBoundException nabe) {
            logger.logError("Username is already in use. Please choose another.", nabe);
            status = AuthenticationStatus.ProviderError;
        } catch (NamingException ne) {
            logger.logError("NamingException: ", ne);
            ne.printStackTrace();
            status = AuthenticationStatus.ProviderError;
        } catch (Exception e) {
            logger.logError("User account was not created.", e);
            status = AuthenticationStatus.ProviderError;
        } finally {
            try {
                ctx.close();
            } catch (Exception e) {
            }
        }
        return status;
    }

    public AuthenticationStatus changePassword(User user, String oldPwd, String newPwd) {
        // TODO Auto-generated method stub
        return null;
    }

    public List<User> getUsers(Search query) {
        List<User> users = new ArrayList<User>();
        InitialDirContext ctx = null;
        try {

            ctx = new InitialDirContext(env);
            // get all users where object class is person
            String filter = "(&(objectClass={0}))";
            String returnedAtts[] = {"*"};
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            //searchControls.setReturningAttributes(returnedAtts);
            searchControls.setReturningObjFlag(true);
            NamingEnumeration namingEnumeration = ctx.search(baseSearch, filter, new String[]{this.objectClass}, searchControls);

            String dn = null;
            Attributes userAttributes = null;
            Attribute tempAttribute = null;
            UserDbImpl user = null;
            while (namingEnumeration.hasMore()) {
                SearchResult result = (SearchResult) namingEnumeration.next();
                userAttributes = result.getAttributes();

                if (userAttributes != null) {
                    NamingEnumeration<Attribute> attrNames = (NamingEnumeration<Attribute>) userAttributes.getAll();

                    String loginId = userAttributes.get(this.loginFilterName).get(0).toString();
                    user = (UserDbImpl) SecurityManager.getUser(loginId);

                    if (user != null)
                        continue;

                    user = new UserDbImpl();

                    while (attrNames.hasMore()) {
                        Attribute attrib = attrNames.next();
                        String propertyName = this.attributeMap.get(attrib.getID());
                        if (StringHelper.isNotEmpty(propertyName)) {
                            Method method = null;
                            try {
                                method = user.getClass().getMethod("get" + propertyName);
                            } catch (Throwable t) {
                                method = null;
                            }
                            String ret = StringHelper.EMPTY;
                            if (method != null)
                                ret = (String) method.invoke(user);
                            if (StringHelper.isNotEmpty(ret))
                                continue;

                            if (attrib.get(0) != null && StringHelper.isNotEmpty(attrib.get(0).toString())) {
                                try {
                                    method = user.getClass().getMethod("set" + propertyName, String.class);
                                } catch (Throwable t) {
                                    method = null;
                                }
                                if (method != null)
                                    method.invoke(user, attrib.get(0).toString());
                            }
                        }
                    }
                }

                dn = result.getNameInNamespace();

                logger.logDebug("dn: " + dn);
                users.add(user);
            }
        } catch (RuntimeException ex) {
            logger.LogException("Exception occured in getUsers(Search query) method", ex);
        } catch (Exception ex) {
            logger.LogException("Exception occured in getUsers(Search query) method", ex);
        }
        return users;
    }

    /**
     * Initializes a Directory Context with the specified credentials and return it. If the password is blank(null), it binds as anonymous user and
     * returns the context.
     *
     * @param username Directory user name
     * @param password Directory user password
     * @return valid directory context, if credentials are valid
     * @throws AuthenticationException if credentails are invalid
     * @throws NamingException         if directory operation fails
     */
    private DirContext getDirectoryContext(String username, String password) throws AuthenticationException, NamingException {
        DirContext dCtx = null;
        try {

            Hashtable env = new Hashtable();
            env.put(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
            env.put(Context.PROVIDER_URL, providerURL);

            // if password is specified, set the credentials
            if (password != null) {
                env.put(Context.SECURITY_AUTHENTICATION, "simple");
                env.put(Context.SECURITY_PRINCIPAL, username);
                env.put(Context.SECURITY_CREDENTIALS, password);
            }

            // Bind and initialize the Directory context
            dCtx = new InitialDirContext(env);

            return dCtx;
        } finally {
            try {
                dCtx.close();
            } catch (Exception e) {
            }
        }
    }

    private void getDefaultDirContext() throws NamingException {
        env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
        env.put(Context.PROVIDER_URL, providerURL);
        env.put(Context.SECURITY_PRINCIPAL, securityPrincipal);
        env.put(Context.SECURITY_CREDENTIALS, securityCredentials);
    }

    // main method to test this class
    public static void main(String args[]) {
        LDAPSecurityProvider oImpl = new LDAPSecurityProvider();
        oImpl.load(null);
        List list = oImpl.getUsers(null);
        oImpl.authenticate("kraza", "Aaaaaaaaaaa_");
        UserDbImpl user = new UserDbImpl();
        user.setLoginId("kraza");
        user.setFirstName("kazim");
        user.setLastName("raza");
        user.setEmailUrl("kraza@avanzasolutions.com");

        oImpl.changePassword(user, "Aaaaaaaaaaa_", "Bbbbbbbbbbb_");
        //  aAbB_5555555
    }
}
