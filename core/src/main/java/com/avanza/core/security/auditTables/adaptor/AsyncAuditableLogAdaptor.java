package com.avanza.core.security.auditTables.adaptor;

import com.avanza.core.security.auditTables.AuditableDTO;
import com.avanza.core.util.Logger;
import com.avanza.ui.util.ResourceUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Singleton adaptor for logging table audit logs
 *
 * @author arsalan.ahmed
 */
public final class AsyncAuditableLogAdaptor implements AuditableLogAdaptor {

    private static final Logger logger = Logger.getLogger(AsyncAuditableLogAdaptor.class);
    private static AsyncAuditableLogAdaptor asyncAdaptor = null;
    private ExecutorService executor = null;
    private static final String threadPoolSizeKey = "unison_audit_table_logger_thread_pool_size";


    /**
     * Private constructor for creating Singleton object
     */
    private AsyncAuditableLogAdaptor() {
        int poolSize = getThreadPoolSize();
        this.clear();
        this.executor = Executors.newFixedThreadPool(poolSize);
    }

    private static int getThreadPoolSize() {
        int poolSize = 50;//Absolute default
        try {
            poolSize = Integer.parseInt(ResourceUtil.getInstance().getProperty(threadPoolSizeKey));
        } catch (Exception e) {
            logger.LogException("Exception occur in getThreadPoolSize, sesstion absolute default vaule of poolSize: " + poolSize, e);
        }

        return poolSize;
    }


    /**
     * Get singleton instance of this class
     *
     * @return
     */
    public static AsyncAuditableLogAdaptor getAdaptor() {
        if (asyncAdaptor == null) {
            asyncAdaptor = new AsyncAuditableLogAdaptor();
        }

        return asyncAdaptor;
    }


    @Override
    public void addTableAuditLog(AuditableDTO auditTableDTO) {
        try {
            start(auditTableDTO);
        } catch (Exception e) {
            logger.logError("Exxcpr occur:", e);
        }

    }

    /**
     * This method will be responsible for stating new thread
     *
     * @param entities
     * @throws Exception
     */
    private void start(AuditableDTO auditTableDTO) throws Exception {
        try {
            AuditableLogRunable thread = new AuditableLogRunable(auditTableDTO);
            executor.execute(thread);

        } catch (Exception e) {
            throw e;
        }

    }


    @Override
    public void clear() {
        try {
            if (asyncAdaptor != null) {
                logger.logDebug("[Clearing activity logging backlog ...]");
                int count = 0;
                executor.shutdown();

                while (!asyncAdaptor.executor.isTerminated()) {
                    count++;
                }
                logger.logDebug("asyncAdaptor.executor.isTerminated() :" + asyncAdaptor.executor.isTerminated());

            }
        } catch (Exception ex) {
            logger.logError("Exception occur", ex);
        }

    }


}
