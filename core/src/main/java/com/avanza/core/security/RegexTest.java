package com.avanza.core.security;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class RegexTest {

    public static void main(String[] args) {
        String regex;
        String input;
        Scanner in = new Scanner(System.in);


        while (true) {

            regex = in.nextLine();
            input = in.nextLine();


            Pattern pattern =
                    Pattern.compile(regex);

            Matcher matcher =
                    pattern.matcher(input);

            boolean found = false;

            while (matcher.find()) {
                System.out.format("I found the text \"%s\" starting at " +
                                "index %d and ending at index %d.%n",
                        matcher.group(), matcher.start(), matcher.end());
                found = true;
            }

//            if(matcher.matches()){
//            	 System.out.println("Match found");
//            	 found = true;
//            }
            if (!found) {
                System.out.println("No match found");
            }


        }
    }
}
