/**
 *
 */
package com.avanza.core.security;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shahbaz.ali
 *
 */
public class SecurityCredentialResponse {

    ArrayList<String> messages = new ArrayList<String>(0);
    CredentialNavigationOutcome credentialNavigationCode;

    public List<String> getResponseMessages() {
        return messages;
    }

    public void addResponseMessage(String message) {
        this.messages.add(message);
    }

    public CredentialNavigationOutcome getCredentialNavigationCode() {
        return credentialNavigationCode;
    }

    public void setCredentialNavigationCode(
            CredentialNavigationOutcome navigationCode) {
        this.credentialNavigationCode = navigationCode;
    }

}
