package com.avanza.core.security;

/**
 * @author shahbaz.ali
 */
public enum AuthenticationNavigationOutcome {

    // null means to stay in the page

    OK("OK"), AccountSuspended(null), ChangeDefaultPassword("ToChangePassword"), PasswordExpired("ToChangePassword");

    AuthenticationNavigationOutcome(String value) {
        this.value = value;
    }

    private String value;

    public String toString() {
        return this.value;
    }

}
