package com.avanza.core.security.db;

import java.io.Serializable;

import com.avanza.core.data.DbObject;

/**
 * it is to hold parent child relation of groups.
 *
 * @author
 */
public class GroupRelationBinding extends DbObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2107434231608485266L;
    private String parentGroup;
    private String childGroup;
    private UserGroupDbImpl parentGrp;

    public GroupRelationBinding() {

    }

    public GroupRelationBinding(String parentGrp, String childGrp) {
        this.parentGroup = parentGrp;
        this.childGroup = childGrp;
    }

    public String getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(String parentGroup) {
        this.parentGroup = parentGroup;
    }

    public String getChildGroup() {
        return childGroup;
    }

    public void setChildGroup(String childGroup) {
        this.childGroup = childGroup;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;

        result = prime * result + ((childGroup == null) ? 0 : childGroup.hashCode());
        result = prime * result + ((parentGroup == null) ? 0 : parentGroup.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (this.getClass() != obj.getClass())
            return false;

        final GroupRelationBinding other = (GroupRelationBinding) obj;

        if (this.childGroup == null) {
            if (other.childGroup != null)
                return false;
        } else if (this.childGroup.compareTo(other.childGroup) != 0)
            return false;

        if (parentGroup == null) {
            if (other.parentGroup != null)
                return false;
        } else if (this.parentGroup.compareTo(other.parentGroup) != 0)
            return false;

        return true;
    }


    public UserGroupDbImpl getParentGrp() {
        return parentGrp;
    }


    public void setParentGrp(UserGroupDbImpl parentGrp) {
        this.parentGrp = parentGrp;
    }
}