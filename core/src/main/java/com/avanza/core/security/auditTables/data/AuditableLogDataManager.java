package com.avanza.core.security.auditTables.data;

/**
 * Template for audit table data management
 *
 * @author arsalan.ahmed
 */
public interface AuditableLogDataManager {

    /**
     * This method will be responsible for managing auditing of table
     */
    public void manageTableAudit();

    /**
     * Clear back log when application is data manager is getting shut down
     */
    public void clearBackLog();
}
