package com.avanza.core.security.db;

import java.security.Key;

import com.avanza.core.data.DbObject;

/**
 * SystemDataSource entity.
 *
 * @author MyEclipse Persistence Tools
 */

public class SystemDataSource extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 4118219327710258849L;
    private String dsKey;
    private String dsUser;
    private String dsPassword;

    private boolean selected;

    public String getDsKey() {
        return this.dsKey;
    }

    public void setDsKey(String dsKey) {
        this.dsKey = dsKey;
    }

    public String getDsUser() {
        return this.dsUser;
    }

    public void setDsUser(String dsUser) {
        this.dsUser = dsUser;
    }

    public String getDsPassword() {
        return this.dsPassword;
    }

    public void setDsPassword(String dsPassword) {
        this.dsPassword = dsPassword;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}