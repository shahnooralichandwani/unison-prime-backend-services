package com.avanza.core.security.db;

import java.util.List;

import com.avanza.core.security.Policy;

public class PolicyDbImpl extends Policy implements Cloneable {

    private List<UserDbImpl> users;

    @Override
    public void addUser(UserDbImpl user) {

    }

    @Override
    public List<UserDbImpl> getUsers() {
        return users;
    }

    public void setUsers(List<UserDbImpl> users) {
        this.users = users;
    }

    @Override
    public void removeUser(UserDbImpl user) {

    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        PolicyDbImpl policy = new PolicyDbImpl();
        policy.setPolicyId(super.getPolicyId());
        policy.setPolicyNamePrm(super.getPolicyNamePrm());
        policy.setPolicyNameSec(super.getPolicyNameSec());
        policy.setImplClass(super.getImplClass());
        policy.setDefaultPolicy(super.isDefaultPolicy());
        policy.setFailureCount(super.getFailureCount());
        policy.setIdleDuration(super.getIdleDuration());
        policy.setInvalidWords(super.getInvalidWords());
        policy.setPasswordFailMsgKeys(super.getPasswordFailMsgKeys());
        policy.setPasswordExpiryDuration(super.getPasswordExpiryDuration());
        policy.setPasswordExpression(super.getPasswordExpression());
        policy.setPasswordLength(super.getPasswordLength());
        policy.setPasswordReuseCount(super.getPasswordReuseCount());
        policy.setChangeDefaultPassword(super.isChangeDefaultPassword());
        policy.setCreatedBy(super.getCreatedBy());
        policy.setCreatedOn(super.getCreatedOn());
        policy.setUpdatedBy(super.getUpdatedBy());
        policy.setUpdatedOn(super.getUpdatedOn());
        return policy;
    }
}