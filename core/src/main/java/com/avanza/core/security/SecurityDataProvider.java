package com.avanza.core.security;

import java.util.List;

import com.avanza.core.CoreInterface;
import com.avanza.core.data.expression.Search;
import com.avanza.core.sessionhistory.ValidateScript;

public interface SecurityDataProvider extends CoreInterface {

    User getUser(String loginId);

    User createUser(String loginId, String password);

    void persistUser(User user);

    void deleteUser(User user);

    List<User> findUsers(Search criteria);

    UserGroup createUserGroup(String groupId);

    void persistGroup(UserGroup group);

    List<UserGroup> findGroups(Search criteria);

    List<Permission> findPermissions(Search criteria);

    void persistPolicy(Policy policy);

    List<Policy> findPolicies(Search criteria);

    UserGroup getGroup(String groupId);

    ValidateScript getValidateScript(String validateScriptId);

    void persistValidateScript(ValidateScript selectedScript);

    public List<User> findUserByPolicy(Search search);
}