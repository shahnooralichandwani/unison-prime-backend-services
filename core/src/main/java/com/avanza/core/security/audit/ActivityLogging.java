package com.avanza.core.security.audit;

/**
 * @author kraza
 */
public interface ActivityLogging {

    void beginActivityLogging();

    void endActivityLogging();

    void logDetail();

    void prepareLogDetail(ActivityOperationType activityOperationType, String description);

    void prepareLogDetail(String moduleId);
}
