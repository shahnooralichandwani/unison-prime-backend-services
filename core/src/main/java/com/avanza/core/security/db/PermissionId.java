package com.avanza.core.security.db;

import java.io.Serializable;


public class PermissionId implements Serializable {

    private static final long serialVersionUID = 2883328543906549918L;
    private String permissionId;
    private String appId;

    public PermissionId() {
    }

    public PermissionId(String permissionId, String appId) {
        this.permissionId = permissionId;
        this.appId = appId;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPermissionId() {
        return this.permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public boolean equals(Object other) {

        if ((this == other))
            return true;

        if ((other == null))
            return false;

        if (!(other instanceof PermissionId))
            return false;

        PermissionId castOther = (PermissionId) other;

        return ((this.permissionId.compareTo(castOther.permissionId) == 0) &&
                (this.appId.compareTo(castOther.appId) == 0));
    }

    @Override
    public int hashCode() {

        int result = 17;

        result = 37 * result + this.getPermissionId().hashCode();
        result = 37 * result + this.getAppId().hashCode();
        return result;
    }

    @Override
    public PermissionId clone() {

        return new PermissionId(this.permissionId, this.appId);
    }
}