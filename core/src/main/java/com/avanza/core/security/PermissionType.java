package com.avanza.core.security;

public enum PermissionType {

    None(0), Module(1), Transaction(2), Action(3);

    public static final String ID_MODULE = "M";
    public static final String ID_TRANSACTION = "T";
    public static final String ID_ACTION = "A";
    public static final String ID_NONE = "N";

    private int intVal;

    private PermissionType(int intVal) {
        this.intVal = intVal;
    }

    public int toInt() {
        return this.intVal;
    }
}