package com.avanza.core.security.audit;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;

/**
 * @author kraza
 */
public class ActivityLog extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 5798825471126881856L;

    private String loginId;
    private String activitySessionKey;
    private Date loginTime;
    private Date logoutTime;

    public ActivityLog() {
    }

    private Set<ActivityLogDetail> activityLogDetails = new HashSet<ActivityLogDetail>(0);

    public String getActivitySessionKey() {
        return this.activitySessionKey;
    }

    public void setActivitySessionKey(String activitySessionKey) {
        this.activitySessionKey = activitySessionKey;
    }

    public Date getLoginTime() {
        return this.loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Date getLogoutTime() {
        return this.logoutTime;
    }

    public void setLogoutTime(Date logoutTime) {
        this.logoutTime = logoutTime;
    }

    public Set<ActivityLogDetail> getActivityLogDetails() {
        return this.activityLogDetails;
    }

    public void setActivityLogDetails(
            Set<ActivityLogDetail> activityLogDetails) {
        this.activityLogDetails = activityLogDetails;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
}
