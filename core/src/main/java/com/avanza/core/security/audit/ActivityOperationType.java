package com.avanza.core.security.audit;

import com.avanza.core.CoreException;


public enum ActivityOperationType {

    Add(0), Update(1), Delete(2), Search(3), Opened(4);

    private int intValue;

    private ActivityOperationType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static ActivityOperationType fromString(String value) {

        ActivityOperationType retVal;

        if (value.equalsIgnoreCase("Add"))
            retVal = ActivityOperationType.Add;
        else if (value.equalsIgnoreCase("Update"))
            retVal = ActivityOperationType.Update;
        else if (value.equalsIgnoreCase("Delete"))
            retVal = ActivityOperationType.Delete;
        else if (value.equalsIgnoreCase("Search"))
            retVal = ActivityOperationType.Search;
        else if (value.equalsIgnoreCase("Opened"))
            retVal = ActivityOperationType.Opened;
        else
            throw new CoreException("[%1$s] is not recognized as valid ActivityOperationType", value);

        return retVal;
    }
}
