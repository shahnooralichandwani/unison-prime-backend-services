package com.avanza.core.security;

import com.avanza.core.util.StringHelper;

import java.io.Serializable;
import java.util.List;

public class PermissionBinding extends PermissionBase implements Serializable {

    protected Permission linkPermission;
    protected boolean isSelected;
    protected PermissionCollection<PermissionBinding> childList;
    protected String permissionId;

    public PermissionBinding(String permissionId) {

        this.permissionId = permissionId;
        this.childList = new PermissionCollection<PermissionBinding>(0);
    }

    public PermissionBinding(int size) {

        this.permissionId = StringHelper.EMPTY;
        this.childList = new PermissionCollection<PermissionBinding>(size);
    }

    private PermissionBinding(Permission linkPermission, boolean create, boolean update,
                              boolean delete, boolean isSelected) {

        super(create, update, delete);
        this.isSelected = isSelected;
        this.linkPermission = linkPermission;
        if (this.linkPermission != null)
            this.permissionId = this.linkPermission.getPermissionId();
        this.childList = new PermissionCollection<PermissionBinding>(0);
    }

    public PermissionBinding(Permission linkPermission, boolean create, boolean update,
                             boolean delete) {

        super(create, update, delete);
        this.linkPermission = linkPermission;
        this.permissionId = this.linkPermission.getPermissionId();
        this.childList = new PermissionCollection<PermissionBinding>(0);
    }

    public PermissionBinding(Permission linkPermission) {

        this.linkPermission = linkPermission;
        this.permissionId = this.linkPermission.getPermissionId();
        this.childList = new PermissionCollection<PermissionBinding>(0);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public void changeCreate(boolean create) {

        if (!this.linkPermission.getCanCreate() && create)
            throw new SecurityException("Creation of %s permission is not allowed",
                    this.linkPermission.getPermissionId());

        if (this.canCreate != create) {
            this.canCreate = create;
        }
    }

    public void changeUpdate(boolean update) {

        if (!this.linkPermission.getCanUpdate() && update)
            throw new SecurityException("Updation of %s permission is not allowed",
                    this.linkPermission.getPermissionId());

        if (this.canUpdate != update) {
            this.canUpdate = update;
        }
    }

    public void changeDelete(boolean delete) {

        if (!this.linkPermission.getCanDelete() && delete)
            throw new SecurityException("Updation of %s permission is not allowed",
                    this.linkPermission.getPermissionId());

        if (this.canDelete != delete) {
            this.canDelete = delete;
        }
    }

    public PermissionType getType() {

        if (this.linkPermission == null)
            return PermissionType.None;

        return this.linkPermission.getType();
    }

    public String getPermissionId() {

        return this.permissionId;
    }

    public Permission getLinkPermission() {
        return this.linkPermission;
    }

    @Override
    public PermissionBase clone() {

        PermissionBinding clone = new PermissionBinding(this.linkPermission, this.canCreate,
                this.canUpdate, this.canDelete, this.isSelected);
        clone.key = this.key;
        clone.childList = this.childList.clone();

        return clone;
    }

    @Override
    // TODO need to implement
    public boolean equals(Object obj) {

        PermissionBinding permissionBinding = (PermissionBinding) obj;

        if (this.getPermissionId().equals(permissionBinding.getPermissionId()))
            return true;

        return false;
    }

    @Override
    public String getAppId() {

        if (this.linkPermission == null)
            return StringHelper.EMPTY;

        return this.linkPermission.getAppId();
    }

    @Override
    @SuppressWarnings("unchecked")
    PermissionBinding getPermission(String name, PermissionType enumValue) {

        if (this.getType() == PermissionType.Action)
            return null;

        return this.childList.getPermission(name, enumValue);
    }

    @Override
    public int hashCode() {

        if (this.linkPermission == null)
            return this.permissionId.hashCode();

        return this.linkPermission.hashCode();
    }

    @Override
    void copyValues(PermissionBase copy) {

        super.copyValues(copy);
        PermissionBinding temp = (PermissionBinding) copy;
        this.setSelected(temp.isSelected());
        this.linkPermission = temp.linkPermission;
        this.permissionId = temp.permissionId;
    }

    void mergeValues(PermissionBase mergeFrom) {
        super.mergeValues(mergeFrom);
        PermissionBinding temp = (PermissionBinding) mergeFrom;
        this.setSelected(this.isSelected || temp.isSelected());

    }

    public void selectBindings(PermissionBinding toSelect) {
        selectBindings(toSelect, false);
    }

    public void mergeBindingsTo(PermissionBinding toSelect) {
        selectBindings(toSelect, true);
    }

    private void selectBindings(PermissionBinding toSelect, boolean createMissingBindings) {

        if (toSelect == null)
            return;

        PermissionBinding temp = toSelect.getPermission(this.permissionId);

        if (temp != null) {

            temp.mergeValues(this);
            for (PermissionBinding item : this.childList) {
                item.selectBindings(temp, createMissingBindings);
            }
        } else if (createMissingBindings) {
            toSelect.add((PermissionBinding) this.clone());
        }
    }

    public boolean isDummy() {

        return StringHelper.isEmpty(this.permissionId);
    }

    // Collection Methods

    public PermissionBinding getPermission(String id) {

        if (this.permissionId.compareTo(id) == 0)
            return this;

        if (this.getType() == PermissionType.Action)
            return null;

        return this.childList.getPermission(this.getPermissionId(), id);
    }

    public PermissionBinding getModule(String name) {

        if (this.permissionId.compareTo(name) == 0)
            return this;

        if (this.getType() == PermissionType.Action)
            return null;

        return this.childList.getModule(this.getPermissionId(), name);
    }

    public PermissionBinding getUserTxn(String name) {

        if (this.permissionId.compareTo(name) == 0)
            return this;

        if (this.getType() == PermissionType.Action)
            return null;

        return this.childList.getUserTxn(this.getPermissionId(), name);
    }

    public PermissionBinding getAction(String name) {

        if (this.permissionId.compareTo(name) == 0)
            return this;

        if (this.getType() == PermissionType.Action)
            return null;

        return this.childList.getAction(this.getPermissionId(), name);
    }

    public List<PermissionBinding> getPermissions() {

        if (this.getType() == PermissionType.Action)
            return null;

        return this.childList.getPermissions();
    }

    public List<PermissionBinding> getModules() {

        if (this.getType() == PermissionType.Action)
            return null;

        return this.childList.getPermissions(PermissionType.Module);
    }

    public List<PermissionBinding> getUserTxns() {

        if (this.getType() == PermissionType.Action)
            return null;

        return this.childList.getPermissions(PermissionType.Transaction);
    }

    public List<PermissionBinding> getActions() {

        if (this.getType() == PermissionType.Action)
            return null;

        return this.childList.getPermissions(PermissionType.Action);
    }

    public List<PermissionBinding> getPermissions(PermissionType enumValue) {

        if (this.getType() == PermissionType.Action)
            return null;

        return this.childList.getPermissions(enumValue);
    }

    /**
     * Add items in tree structure.
     *
     * @param item
     */
    public void add(PermissionBinding item) {

        String newId = item.getPermissionId();
        String containPermission = this.getPermissionId();

        int idx = newId.indexOf(containPermission);

        if (idx == 0 && StringHelper.isNotEmpty(containPermission)) {
            // e.g. newId = A.B, containPermission = A
            if (newId.length() == containPermission.length()) {
                // e.g. newId = A, containPermission = A
                throw new SecurityException(
                        "Incorrect name [%1$s] for adding as a child of [%2$s]", newId,
                        containPermission);
            }

            // e.g. newId = B
            newId = newId.substring(containPermission.length() + 1);
        }

        idx = newId.indexOf(PermissionBase.NAME_SEPARATOR);
        if (idx == -1) {
            this.childList.add(newId, item);
        } else {
            // if sub-child. e.g B.C
            String key = newId.substring(0, idx);
            String parentKey = (containPermission.length() == 0) ? key : containPermission
                    + PermissionBase.NAME_SEPARATOR + key;
            PermissionBinding temp = this.getPermission(parentKey);
            if (temp == null) {
                temp = new PermissionBinding(SecurityManager.getPermission(parentKey));
                this.add(temp);
            }

            temp.add(item);
        }
    }

    public boolean remove(String id) {

        String containPermission = this.getPermissionId();

        int idx = id.indexOf(containPermission);

        if (idx > -1 && containPermission.length() > 0) {

            if ((id.length() == containPermission.length())
                    && (id.charAt(containPermission.length()) == PermissionBase.NAME_SEPARATOR))
                throw new SecurityException(
                        "Incorrect name [%1$s] for adding as a child of [%2$s]", id,
                        containPermission);

            id = id.substring(containPermission.length() + 1);
        }

        idx = id.lastIndexOf(PermissionBase.NAME_SEPARATOR);

        if (idx == -1)
            return this.childList.remove(id);

        // ELSE case

        String key = id.substring(idx + 1);
        String parentKey = (containPermission.length() == 0) ? key : containPermission
                + PermissionBase.NAME_SEPARATOR + key;
        PermissionBinding temp = this.getPermission(parentKey);

        if (temp != null)
            return temp.childList.remove(key);

        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getPermissionId());
        sb.append("(");
        for (PermissionBinding child : childList.getPermissions()) {
            sb.append(child.toString() + ",");
        }
        sb.append(")");

        return sb.toString();
    }
}
