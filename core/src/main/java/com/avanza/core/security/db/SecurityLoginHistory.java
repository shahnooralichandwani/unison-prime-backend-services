package com.avanza.core.security.db;

import java.util.Date;

import com.avanza.core.data.DbObject;


public class SecurityLoginHistory extends DbObject {


    // Fields    

    private String loginHistoryId;
    private String authenticationStatus;
    private Date loginTime;
    private Date logoutTime;
    private String loginId;
    private String remoteIp;


    // Constructors

    /**
     * default constructor
     */
    public SecurityLoginHistory() {
    }


    /**
     * full constructor
     */
    public SecurityLoginHistory(String loginHistoryId, String authenticationStatus, Date loginTime, String loginId, String remoteIp, Date logoutTime) {
        this.loginHistoryId = loginHistoryId;
        this.authenticationStatus = authenticationStatus;
        this.loginTime = loginTime;
        this.loginId = loginId;
        this.remoteIp = remoteIp;
        this.logoutTime = logoutTime;

    }


    // Property accessors

    public String getLoginHistoryId() {
        return this.loginHistoryId;
    }

    public void setLoginHistoryId(String loginHistoryId) {
        this.loginHistoryId = loginHistoryId;
    }

    public String getAuthenticationStatus() {
        return this.authenticationStatus;
    }

    public void setAuthenticationStatus(String authenticationStatus) {
        this.authenticationStatus = authenticationStatus;
    }

    public Date getLoginTime() {
        return this.loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getLoginId() {
        return this.loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getRemoteIp() {
        return this.remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }


    public Date getLogoutTime() {
        return logoutTime;
    }


    public void setLogoutTime(Date logoutTime) {
        this.logoutTime = logoutTime;
    }


}