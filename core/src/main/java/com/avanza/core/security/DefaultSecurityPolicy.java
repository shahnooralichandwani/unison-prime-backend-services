/**
 *
 */
package com.avanza.core.security;

import com.avanza.core.security.db.PasswordHistoryDbImpl;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.security.db.UserStatusType;
import com.avanza.core.util.Cryptographer;
import com.avanza.core.util.DateUtil;
import com.avanza.core.util.StringHelper;
import com.avanza.ui.util.ResourceUtil;

/*import javax.faces.context.FacesContext;*/
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author shahbaz.ali
 *
 */
public class DefaultSecurityPolicy implements SecurityPolicy {

    private Policy policy;
    private int failedAttempts;
    Date lastLogin;
    boolean invalidUser;
    private final String regexSpecialCharacters = "$,^,.,*,+,?,[,]";

    public DefaultSecurityPolicy(Policy policy) {
        this.policy = policy;
    }

    public SecurityAuthenticationResponse onAuthenticate(String loginId) {

        clearFailedAttempts(loginId);
        SecurityAuthenticationResponse response = new SecurityAuthenticationResponse();

        // Check for default password
        if (getPolicy().isChangeDefaultPassword()) {

            Date lastTimeChanged = SecurityManager.getPasswordChangeTime(loginId);
            String msgFirstChangePassword = ResourceUtil.getInstance().getProperty("unison", "firstChangePassword_msg");

            // If user is valid and logging in for the first time
            if (!invalidUser && lastLogin == null) {

                response.addResponseMessage(msgFirstChangePassword);
                response.setAuthenticationNavigationCode(AuthenticationNavigationOutcome.ChangeDefaultPassword);
                return response;
            }

            // if user is valid and has logged in but never changed his password
            else if (!invalidUser && lastTimeChanged == null) {
                response.addResponseMessage(msgFirstChangePassword);
                response.setAuthenticationNavigationCode(AuthenticationNavigationOutcome.ChangeDefaultPassword);
                return response;
            }

        }

        // Check for password Expiry
        if (getPolicy().getPasswordExpiryDuration() != null && getPolicy().getPasswordExpiryDuration() != 0) {

            Date lastTimeChanged = SecurityManager.getPasswordChangeTime(loginId);
            String msgPasswordExpired = ResourceUtil.getInstance().getProperty("unison", "passwordExpired_msg");

            if (lastTimeChanged != null) {

                Date currentDate = Calendar.getInstance().getTime();
                int interval = DateUtil.calculateDifferenceInDays(lastTimeChanged, currentDate);

                if (interval > getPolicy().getPasswordExpiryDuration()) {
                    response.addResponseMessage(msgPasswordExpired);
                    response.setAuthenticationNavigationCode(AuthenticationNavigationOutcome.PasswordExpired);
                    return response;

                }
            }
        }

        // Check for suspended logins
        if (getPolicy().getIdleDuration() != null && getPolicy().getIdleDuration() != 0) {

            String msgAccountSuspended = ResourceUtil.getInstance().getProperty("unison", "accountSuspended_msg");

            if (!invalidUser && lastLogin != null) {

                Date currentDate = Calendar.getInstance().getTime();
                int interval = DateUtil.calculateDifferenceInDays(lastLogin, currentDate);

                if (interval > getPolicy().getIdleDuration()) {

                    UserDbImpl user = (UserDbImpl) SecurityManager.getUser(loginId);
                    user.setStatusId(UserStatusType.InActive.getValue());
                    SecurityManager.persistUser(user);

                    response.addResponseMessage(msgAccountSuspended);
                    response.setAuthenticationNavigationCode(AuthenticationNavigationOutcome.AccountSuspended);
                    return response;

                }
            }
        }

        response.setAuthenticationNavigationCode(AuthenticationNavigationOutcome.OK);
        return response;

    }

    private Policy getPolicy() {
        return policy;
    }

    public SecurityAuthenticationResponse onAuthenticationFailure(String loginId) {

        SecurityAuthenticationResponse response = new SecurityAuthenticationResponse();
        raiseFailedAttempts(loginId);

        if (getPolicy().getFailureCount() != null && getPolicy().getFailureCount() != 0) {

            if (getFailedAttempts(loginId) >= getPolicy().getFailureCount()) {

                UserDbImpl user = (UserDbImpl) SecurityManager.getUser(loginId);
                user.setStatusId(UserStatusType.InActive.getValue());
                SecurityManager.persistUser(user);

                response.setAuthenticationNavigationCode(AuthenticationNavigationOutcome.AccountSuspended);

                return response;
            }

        }

        response.setAuthenticationNavigationCode(AuthenticationNavigationOutcome.OK);
        return response;
    }

    public SecurityCredentialResponse onPasswordUpdate(String loginId,
                                                       String newPassword) {

        SecurityCredentialResponse response = new SecurityCredentialResponse();

        // Check for password length
        if (getPolicy().getPasswordLength() != null && getPolicy().getPasswordLength() != 0) {

            int passLength = getPolicy().getPasswordLength();

            //TODO: UNISONPRIME
            // FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PASSWORD_LENGTH", passLength);

            if (newPassword.trim().length() < passLength) {
                String msgKeys = getPolicy().getPasswordFailMsgKeys();

                if (StringHelper.isNotEmpty(msgKeys)) {
                    String[] keys = msgKeys.split(",");

                    for (String msgkey : keys) {
                        String message = ResourceUtil.getInstance().getProperty("unison", msgkey);
                        response.addResponseMessage(message);
                    }
                }
                response.setCredentialNavigationCode(CredentialNavigationOutcome.InvalidPasswordLength);
                return response;
            }

        }

        // Check for invalid words and characters
        if (StringHelper.isNotEmpty(getPolicy().getInvalidWords())) {

            String invalidWords = getPolicy().getInvalidWords();

            //TODO: UNISONPRIME
            // FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("INVALID_WORDS", invalidWords);


            String newLine = System.getProperty("line.separator");
            ArrayList<String> invalidWordsList = new ArrayList<String>();

            int foundNewLines = invalidWords.indexOf(newLine);

            if (foundNewLines >= 0) {
                String[] lines = invalidWords.split(newLine);

                if (lines.length > 0) {
                    for (int i = 0; i < lines.length; i++) {
                        String[] lineWords = lines[i].split(" ");
                        for (int k = 0; k < lineWords.length; k++) {
                            invalidWordsList.add(lineWords[k]);
                        }

                    }

                }
            } else {
                String[] lineWords = invalidWords.split(" ");
                for (int k = 0; k < lineWords.length; k++) {
                    invalidWordsList.add(lineWords[k]);
                }
            }

            StringBuffer regex = new StringBuffer();

            for (String aWord : invalidWordsList) {
                if (regexSpecialCharacters.indexOf(aWord) >= 0) {
                    regex.append("\\");
                    regex.append(aWord.toLowerCase());
                } else
                    regex.append(aWord.toLowerCase());

                regex.append("|");
            }
            String regexFinal = regex.toString();
            regexFinal = regexFinal.substring(0, regexFinal.length() - 1);

            Pattern pattern = Pattern.compile(regexFinal);

            Matcher matcher = pattern.matcher(newPassword.toLowerCase());
            boolean invalidPhraseFound = false;

            if (matcher.matches()) {
                invalidPhraseFound = true;
            }

            if (invalidPhraseFound) {
                String msgKeys = getPolicy().getPasswordFailMsgKeys();

                if (StringHelper.isNotEmpty(msgKeys)) {
                    String[] keys = msgKeys.split(",");

                    for (String msgkey : keys) {
                        String message = ResourceUtil.getInstance().getProperty("unison", msgkey);
                        response.addResponseMessage(message);
                    }
                }

                response.setCredentialNavigationCode(CredentialNavigationOutcome.InvalidPasswordFormat);
                return response;
            }
        }

        // applying expression if available
        if (StringHelper.isNotEmpty(getPolicy().getPasswordExpression())) {

            String expression = getPolicy().getPasswordExpression();

            Pattern pattern = Pattern.compile(expression);

            Matcher matcher = pattern.matcher(newPassword.toLowerCase());
            boolean invalidPhraseFound = false;

            if (matcher.matches()) {
                invalidPhraseFound = true;
            }

            if (invalidPhraseFound) {
                String msgKeys = getPolicy().getPasswordFailMsgKeys();

                if (StringHelper.isNotEmpty(msgKeys)) {
                    String[] keys = msgKeys.split(",");

                    for (String msgkey : keys) {
                        String message = ResourceUtil.getInstance().getProperty("unison", msgkey);
                        response.addResponseMessage(message);
                    }
                }

                response.setCredentialNavigationCode(CredentialNavigationOutcome.InvalidPasswordFormat);
                return response;
            }
        }

        // veriying if the password is not reusing passwords
        if (getPolicy().getPasswordReuseCount() != null && getPolicy().getPasswordReuseCount() != 0) {

            Cryptographer crypto = UserDbImpl.getCryptographer();
            List<PasswordHistoryDbImpl> passwordHistory = SecurityManager.getPasswordsHistory(loginId);
            int canUse = getPolicy().getPasswordReuseCount();

            for (int i = canUse; i < passwordHistory.size(); i++) {

                String oldPassword = crypto.decrypt(passwordHistory.get(i).getPassword());

                if (newPassword.indexOf(oldPassword) >= 0) {
                    String msgKeys = getPolicy().getPasswordFailMsgKeys();

                    if (StringHelper.isNotEmpty(msgKeys)) {
                        String[] keys = msgKeys.split(",");

                        for (String msgkey : keys) {
                            String message = ResourceUtil.getInstance().getProperty("unison", msgkey);
                            response.addResponseMessage(message);
                        }
                    }

                    response.setCredentialNavigationCode(CredentialNavigationOutcome.InvalidPasswordFormat);
                    return response;
                }

            }

        }

        response.setCredentialNavigationCode(CredentialNavigationOutcome.OK);

        return response;
    }

    public int getFailedAttempts(String loginId) {
        return failedAttempts;
    }

    public void raiseFailedAttempts(String loginId) {
        this.failedAttempts += 1;
    }

    public void clearFailedAttempts(String loginId) {

        failedAttempts = 0;
    }

    public final void onBeforeAuthenticate(String loginId) {
        try {
            lastLogin = SecurityManager.getLastLoginTime(loginId);
        } catch (SecurityException e) {
            // user not found
            invalidUser = true;
        }

    }

}
