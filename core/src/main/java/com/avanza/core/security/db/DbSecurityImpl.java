package com.avanza.core.security.db;

import java.util.Date;
import java.util.List;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.DataSourceException;
import com.avanza.core.data.expression.Search;
import com.avanza.core.meta.MetaCounter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.security.AuthenticationProvider;
import com.avanza.core.security.AuthenticationStatus;
import com.avanza.core.security.Permission;
import com.avanza.core.security.Policy;
import com.avanza.core.security.SecurityDataProvider;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.sessionhistory.ValidateScript;
import com.avanza.core.util.Cryptographer;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.web.config.WebContext;

public class DbSecurityImpl implements AuthenticationProvider, SecurityDataProvider {

    private static final String XML_BROKER = "broker";
    private static final String DEF_BROKER_NAME = "com.avanza.core.security";

    private DataBroker broker;

    public DbSecurityImpl() {
    }

    // Implementation of CoreInterface methods
    public void load(ConfigSection section) {

        String brokerName = DbSecurityImpl.DEF_BROKER_NAME;

        if (section != null)
            brokerName = section.getValue(DbSecurityImpl.XML_BROKER, DbSecurityImpl.DEF_BROKER_NAME);

        this.broker = DataRepository.getBroker(brokerName);
    }

    public void dispose() {

    }

    // End Implementation of CoreInterface methods

    // Implementation of AuthenticationProvider methods
    public AuthenticationStatus authenticate(String loginId, String password) {

        loginId = loginId.toLowerCase();

        //Is EOD processing Started?
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class.getName());
        if (webContext != null)
            webContext.setAttribute("eodProcessingStarted", !DataRepository.isPrimarySource("CBDBASE"));

        UserDbImpl user = this.broker.findById(UserDbImpl.class, loginId);

        //76:osama start
        if (user.getOrgUserRole().size() == 0 || user.getOrgUserRole().equals("")) {
            return AuthenticationStatus.InvalidRole;
        }
        if (user.getGroupBindings().size() == 0 || user.getOrgUserRole().equals("")) {
            return AuthenticationStatus.InvalidGroup;
        }
        //76:osama end

        if (user == null || !user.getLoginId().equals(loginId))
            return AuthenticationStatus.InvalidUser;

        // return proper error
        if (!user.isActive())
            return AuthenticationStatus.InActiveUser;

        String pwd = UserDbImpl.getCryptographer().decrypt(user.getPassword());

        if (!pwd.equals(password))
            return AuthenticationStatus.InvalidPassword;

        // TO DO: check other validation
        // check whether password is near expiry if policy is enabled
        // check whether password is expired if policy is enabled

        // Update login date of the user, change IS_ACTIVE from NEW to ACTIVE and persist
        user.setLastLogin(new Date());
        if (user.isLoggedIn())
            user.setIsActive(User.ACTIVE);

        this.broker.update(user);
        // add and entry in SEC_SESSION_HISTORY table after generating a session cookie

        return AuthenticationStatus.Success;
    }

    public AuthenticationStatus changePassword(User user, String oldPwd, String newPwd) {

        UserDbImpl castUser = (UserDbImpl) user;
        Cryptographer crypto = UserDbImpl.getCryptographer();
        String currentPwd = crypto.decrypt(castUser.getPassword());

        if (oldPwd.compareTo(currentPwd) != 0)
            return AuthenticationStatus.InvalidPassword;

        newPwd = crypto.encrypt(newPwd);
        castUser.setPassword(newPwd);
        this.broker.update(user);
        PasswordHistoryDbImpl pwdHistory = new PasswordHistoryDbImpl(user.getLoginId(), newPwd);
        this.broker.add(pwdHistory);

        return AuthenticationStatus.Success;
    }

    public List<User> getUsers(Search query) {
        throw new DataSourceException("AuthenticationProvider.getUsers(Search) Not Implemented");
    }

    // End Implementation of AuthenticationProvider methods

    // Implementation of SecurityDataProvidere methods
    public User getUser(String loginId) {

        return this.broker.findById(UserDbImpl.class, loginId);
    }


    public UserGroup getGroup(String groupId) {

        return this.broker.findById(UserGroupDbImpl.class, groupId);
    }


    public List<User> findUsers(Search criteria) {

        return this.broker.find(criteria);
    }

    public void persistUser(User user) {

        UserDbImpl temp = (UserDbImpl) user;
        temp.persist(this.broker);
        // TO DO: Also log an entry in SECURITY_LOG;
    }

    public void deleteUser(User user) {

        if (user.isLoggedIn()) {

            this.broker.delete(user);
        } else {

            user.setWhenDeleted(new Date());
            this.broker.update(user);
        }

        // TO DO: Also log an entry in SECURITY_LOG;
    }

    public <T> void deleteBinding(T grpBinding) {

        this.broker.delete(grpBinding);

        // TO DO: Also log an entry in SECURITY_LOG;
    }

    public List<Permission> findPermissions(Search criteria) {

        if (criteria == null)
            criteria = new Search(PermissionDbImpl.class);
        else
            criteria.addFrom(PermissionDbImpl.class);

        return this.broker.find(criteria);
    }

    public List<UserGroup> findGroups(Search criteria) {

        return this.broker.find(criteria);
    }

    public void persistGroup(UserGroup group) {

        UserGroupDbImpl temp = (UserGroupDbImpl) group;
        temp.Persist(this.broker);
    }

    public List<Policy> findPolicies(Search criteria) {

        return this.broker.find(criteria);
    }

    public void persistPolicy(Policy policy) {

        broker.persist(policy);

    }

    // End Implementation of AuthenticationProvider methods

    public boolean userExists(String loginId) {

        return (this.broker.findById(UserDbImpl.class, loginId) != null);
    }

    public User createUser(String loginId, String password) {

        UserDbImpl user = new UserDbImpl();
        user.setLoginId(loginId);
        user.resetPassword(UserDbImpl.getCryptographer().encrypt(password));
        return user;
    }

    public <T> void persistBindings(T grpBinding) {
        this.broker.persist(grpBinding);
    }

    public UserGroup createUserGroup(String groupId) {

        return new UserGroupDbImpl(groupId);
    }

    public DataBroker GetBroker() {

        return this.broker;

    }

    public ValidateScript getValidateScript(String validateScriptId) {
        return this.broker.findById(ValidateScript.class, validateScriptId);
    }

    public void persistValidateScript(ValidateScript selectedScript) {


        this.broker.persist(selectedScript);

    }

    public List<User> findUserByPolicy(Search search) {
        return broker.find(search);

    }


}