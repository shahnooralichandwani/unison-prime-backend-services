package com.avanza.core.security.audit;


import com.avanza.core.data.DbObject;


/**
 * SecActivityMessages entity. @author MyEclipse Persistence Tools
 */

public class SecActivityMessages extends DbObject implements java.io.Serializable {


    // Fields    
    private static final long serialVersionUID = 6800128360286285604L;
    private Integer id;
    private String moduleName;
    private String activityKey;
    private String activityKeyVlaue;
    private boolean IsActive;


    // Constructors

    /**
     * default constructor
     */
    public SecActivityMessages() {
    }

    /**
     * minimal constructor
     */
    public SecActivityMessages(Integer id, String moduleName, String activityKey, String activityKeyVlaue) {
        this.id = id;
        this.moduleName = moduleName;
        this.activityKey = activityKey;
        this.activityKeyVlaue = activityKeyVlaue;
    }


    // Property accessors

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModuleName() {
        return this.moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getActivityKey() {
        return this.activityKey;
    }

    public void setActivityKey(String activityKey) {
        this.activityKey = activityKey;
    }

    public String getActivityKeyVlaue() {
        return this.activityKeyVlaue;
    }

    public void setActivityKeyVlaue(String activityKeyVlaue) {
        this.activityKeyVlaue = activityKeyVlaue;
    }

    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean isActive) {
        IsActive = isActive;
    }


}