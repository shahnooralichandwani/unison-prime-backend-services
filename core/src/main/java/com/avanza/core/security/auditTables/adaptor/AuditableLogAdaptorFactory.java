package com.avanza.core.security.auditTables.adaptor;

public class AuditableLogAdaptorFactory {

    private AuditableLogAdaptorFactory() {

    }

    public static AuditableLogAdaptor getAdaptor() {
        //String configValue="";//TODO set value from config file
        AuditableLogAdaptor adaptor = null;

        //Default adaptor
        if (adaptor == null)
            adaptor = AsyncAuditableLogAdaptor.getAdaptor();

        return adaptor;
    }

}
