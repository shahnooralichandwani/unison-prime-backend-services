package com.avanza.core.security.auditTables;

import java.util.HashSet;
import java.util.Set;

import com.avanza.core.interceptor.Auditable;

public class AuditableDTO {
    private Set<Auditable> inserts = new HashSet<Auditable>();
    private Set<Auditable> updates = new HashSet<Auditable>();
    private Set<Auditable> deletes = new HashSet<Auditable>();

    public AuditableDTO(Set inserts, Set updates, Set deletes) {
        super();
        this.inserts.addAll(inserts);
        this.updates.addAll(updates);
        this.deletes.addAll(deletes);
    }

    public Set<Auditable> getInserts() {
        return inserts;
    }

    public Set<Auditable> getUpdates() {
        return updates;
    }

    public Set<Auditable> getDeletes() {
        return deletes;
    }


}
