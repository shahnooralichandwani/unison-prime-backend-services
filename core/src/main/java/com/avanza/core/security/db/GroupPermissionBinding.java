package com.avanza.core.security.db;

import java.io.Serializable;

import com.avanza.core.data.DbObject;
import com.avanza.core.security.Permission;
import com.avanza.core.security.PermissionBinding;
import com.avanza.core.security.SecurityManager;

public class GroupPermissionBinding extends DbObject implements Serializable {

    private static final long serialVersionUID = 5990058996951723378L;

    // Feilds
    private boolean canCreate;
    private boolean canUpdate;
    private boolean canDelete;
    private GroupPermissionId groupPermissionId;

    public GroupPermissionId getGroupPermissionId() {
        return groupPermissionId;
    }

    public void setGroupPermissionId(GroupPermissionId groupPermissionId) {
        this.groupPermissionId = groupPermissionId;
    }

    public GroupPermissionBinding() {
    }

    public GroupPermissionBinding(String userGroupId, PermissionBinding binding) {

        this.canCreate = binding.getCanCreate();
        this.canUpdate = binding.getCanUpdate();
        this.canDelete = binding.getCanDelete();
        groupPermissionId = new GroupPermissionId(binding.getPermissionId(), binding.getAppId(), userGroupId);
    }

    // getters & setters methods
    public void setCanCreate(boolean canCreate) {
        this.canCreate = canCreate;
    }

    public boolean getCanCreate() {
        return this.canCreate;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public boolean getCanDelete() {
        return this.canDelete;
    }

    public void setCanUpdate(boolean canUpdate) {
        this.canUpdate = canUpdate;
    }

    public boolean getCanUpdate() {
        return this.canUpdate;
    }

    public String getUserGroupId() {
        return this.groupPermissionId.getUserGroupId();
    }

    public void setUserGroupId(String userGroupId) {
        this.groupPermissionId.setUserGroupId(userGroupId);
    }

    public String getPermissionId() {
        return this.groupPermissionId.getPermissionId();
    }

    public void setPermissionId(String permissionId) {
        this.groupPermissionId.setPermissionId(permissionId);
    }

    public String getAppId() {
        return this.groupPermissionId.getAppId();
    }

    public void setAppId(String appId) {
        this.groupPermissionId.setAppId(appId);
    }

    public PermissionBinding getBinding() {

        Permission temp = SecurityManager.getPermission(this.groupPermissionId.getPermissionId());

        PermissionBinding permissionBinding = new PermissionBinding(temp, this.canCreate, this.canUpdate, this.canDelete);
        permissionBinding.setSelected(true);
        return permissionBinding;
    }
}