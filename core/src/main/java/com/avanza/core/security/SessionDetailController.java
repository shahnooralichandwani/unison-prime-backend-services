package com.avanza.core.security;

/*import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.OrderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.license.CustomerLicenseManager;
import com.avanza.core.meta.MetaCounter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.sessionhistory.SessionDetail;
import com.avanza.core.util.Logger;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;*/

import java.io.Serializable;

public class SessionDetailController implements Serializable {

    /**
     * @author shafique.rehman
     *//*
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger
			.getLogger(SessionDetailController.class);

	private List<SessionDetail> sessionDetList = new ArrayList<SessionDetail>();
	private String userId = null;
	private String sessionId = null;
	

	
	*//**
     * invalidate Session when Force Log out Button clicked.
     *//*
	
	public void invalidateSession() {

		
       logger.logInfo("[Searching SessionDetail instance from database...]");

       SessionDetail detail=new SessionDetail();
       
       Search search=new Search();
	   search.addFrom(SessionDetail.class);
	   search.addCriterion(SimpleCriterion.equal("sessionId", sessionId));
		
		DataBroker broker = DataRepository
				.getBroker(CustomerLicenseManager.class.getName());
	
		List<SessionDetail> sessionDetailList=broker.find(search);
		if(sessionDetailList.size()>0){
			detail=sessionDetailList.get(0);
		}
		
		if(detail!=null && detail.getUrl()!=null){
			try {
				URL url=new URL(detail.getUrl()+"/sessionDestroyServlet?userId="+detail.getUserId()+"&sessionId="+detail.getSessionId());
				try {
					
					logger.logInfo("[Calling SessionDestroyServlet...]");
					
					URLConnection connection = url.openConnection();
					connection.setConnectTimeout(5000);
					connection.getInputStream();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		   
		
		
		*//*if (getUserId() != null) {

			CustomerLicenseManager.invalidateSession(getUserId());

		}*//*

	}
	
	
	*//**
     * Add session in db for force log out features.
     *
     * @param userId
     * @param session
     *//*
	
	@SuppressWarnings("unused")
	public static void addSessionInDB(String userId, HttpSession session){
		logger.logInfo("[Persisting SessionDetail instance to database...]");
		
		String remoteIp=((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();

		String path=((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getScheme()+"://"+
					((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getLocalAddr()+":"+
					((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getLocalPort()+
					((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getContextPath();
		
		SessionDetail sessionDetail = new SessionDetail();

		sessionDetail.setSessDetId(String.valueOf(MetaDataRegistry
				.getNextCounterValue(MetaCounter.SESSION_HISTORY_COUNTER)));
		sessionDetail.setSessionId(session.getId());
		sessionDetail.setUserId(userId);
		sessionDetail.setNodeIp(((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getLocalAddr());
		sessionDetail.setRemoteIp(remoteIp);
		sessionDetail.setUrl(path);

		DataBroker broker = DataRepository
				.getBroker(CustomerLicenseManager.class.getName());
		broker.add(sessionDetail);

	}
	
	
	*//**
     * Remove session from DB by passing session Id.
     *
     * @param sessionId
     *
     *//*

	public static void removeSessionFromDB(String sessionId) {

		logger.logInfo("[Deleting SessionDetail instance from database...]");

		
		Search search=new Search();
		search.addFrom(SessionDetail.class);
		search.addCriterion(SimpleCriterion.equal("sessionId", sessionId));
		
		DataBroker broker = DataRepository
				.getBroker(CustomerLicenseManager.class.getName());
	
		List<SessionDetail> sessionDetailList=broker.find(search);
		if(sessionDetailList.size()>0){
			broker.delete(sessionDetailList.get(0));
		}

	}


	public List<SessionDetail> getSessionDetList() {

		Search search = new Search();
		search.addFrom(SessionDetail.class);
		search.addOrder("createdOn", OrderType.DESC);

		DataBroker broker = DataRepository
				.getBroker(SessionDetailController.class.getName());

		List<SessionDetail> sessionDetailList = broker.find(search);
		if (sessionDetailList.size() > 0)
			sessionDetList = sessionDetailList;
		else 
			sessionDetList.clear();
		return sessionDetList;
	}

	public void setSessionDetList(List<SessionDetail> sessionDetList) {
		this.sessionDetList = sessionDetList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getSessionId() {
		return sessionId;
	}


	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
*/
}
