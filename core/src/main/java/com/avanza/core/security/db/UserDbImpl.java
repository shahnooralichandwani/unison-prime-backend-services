package com.avanza.core.security.db;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.interceptor.Auditable;
import com.avanza.core.meta.audit.SecUserAudit;
import com.avanza.core.meta.audit.SecUserAuditId;
import com.avanza.core.security.User;
import com.avanza.core.security.UserGroup;
import com.avanza.core.util.Cryptographer;
import com.avanza.workflow.configuration.org.RoleUserBinding;
import org.apache.commons.beanutils.BeanUtils;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * This is the DB specific Impl of the User class
 * This class is mapped to the Hibernate mapping file.
 *
 * @author kraza
 */
public class UserDbImpl extends User implements Auditable {

    public static final String PACKAGE_NAME = UserDbImpl.class.getPackage().getName();

    private static final long serialVersionUID = -4193246588342416072L;

    static Cryptographer cryptographer;

    public String password;
    private String policyId;
    private UserStatusType statusId;
    private Set<GroupUserBinding> groupBindings;
    private Set<RoleUserBinding> orgUserRole;
    private String defaultOrgUnit;

    public String getDefaultOrgUnit() {
        return defaultOrgUnit;
    }

    public void setDefaultOrgUnit(String defaultOrgUnit) {
        this.defaultOrgUnit = defaultOrgUnit;
    }

    public UserDbImpl() {
    }

    public Set<GroupUserBinding> getGroupBindings() {
        return groupBindings;
    }


    public void setGroupBindings(Set<GroupUserBinding> groupBindings) {
        this.groupBindings = groupBindings;
    }

    /**
     * This method gets the UserGroup from the DB This is the DB specfic Impl
     * TODO: Remove Replication of code.
     */
    @Override
    public UserGroup getUserGroup(String userGroupId) {

        UserGroup userGroups = DataRepository.getBroker(UserDbImpl.PACKAGE_NAME)
                .findById(UserGroupDbImpl.class, userGroupId);
        return userGroups;
    }

    @Override
    public void resetPassword(String password) {

        this.password = password;
    }

    /**
     * This method fills the UserGroup from the DB This is the DB specfic Impl
     */
    @Override
    protected HashSet<UserGroup> fillGroups() {

        DataBroker dataBroker = DataRepository.getBroker(UserGroupDbImpl.class.getName());

        HashSet<UserGroup> userGroupsList = new HashSet<UserGroup>();

        for (GroupUserBinding grpBinding : this.getGroupBindings()) {
            UserGroup usrGrp = dataBroker.findById(UserGroupDbImpl.class, grpBinding.getUserGroupId());
            userGroupsList.add(usrGrp);
            /*Iterator<GroupRelationBinding> grp = ((UserGroupDbImpl)usrGrp).getChildGroupBindings().iterator();
            while(grp.hasNext()){
                GroupRelationBinding child = grp.next();
                userGroupsList.add(dataBroker.findById(UserGroupDbImpl.class, child.getChildGroup())); 
            }*/
        }

        return userGroupsList;
    }

    /**
     * This method returns the encrypted password to Hibernate for persistence.
     *
     * @return
     */
    String getPassword() {

        return this.password;
    }


    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPolicyId() {

        return this.policyId;
    }

    public String getStatusId() {
        if (this.statusId == null)
            return UserStatusType.Active.getValue();
        return this.statusId.getValue();
    }

    //This method is called by hibernate
    public void setPassword(String password) {
        this.password = password;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public void setStatusId(String statusId) {

        this.statusId = UserStatusType.fromString(statusId);
        if (this.statusId == UserStatusType.Active)
            this.isActive = User.ACTIVE;
        else if (this.statusId == UserStatusType.InActive)
            this.isActive = User.INACTIVE;
    }

    public static Cryptographer getCryptographer() {

        if (cryptographer == null)
            cryptographer = new Cryptographer();

        return cryptographer;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getIsActive() {
        return this.isActive;
    }

    void persist(DataBroker broker) {

        broker.persist(this);

        // persist New User groups added to the user
        if (this.addedGroups != null && this.addedGroups.size() > 0) {

            for (UserGroup userGroup : this.addedGroups) {

                GroupUserBinding grpBinding = new GroupUserBinding(this.loginId, userGroup.getUserGroupId());
                if (!this.groupBindings.contains(grpBinding)) {
                    this.groupBindings.add(grpBinding);
                    broker.add(grpBinding);
                }
            }
        }

        // delete user group association that are removed
        if (this.removedGroups != null && this.removedGroups.size() > 0) {

            for (UserGroup userGroup : this.removedGroups) {

                GroupUserBinding grpBinding = null;
                String relId = userGroup.getUserGroupId();

                for (GroupUserBinding gItem : this.groupBindings) {

                    if (gItem.getUserGroupId().compareTo(relId) == 0) {
                        grpBinding = gItem;
                        break;
                    }
                }

                this.groupBindings.remove(grpBinding);
                broker.delete(grpBinding);
            }
        }

        this.synchronizeGroups();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{LoginId=").append(this.loginId).append(", fullName=").append(this.fullName).append("}");
        return sb.toString();
    }

    public Set<RoleUserBinding> getOrgUserRole() {
        return orgUserRole;
    }

    public void setOrgUserRole(Set<RoleUserBinding> orgUserRole) {
        this.orgUserRole = orgUserRole;
    }

    public String getDecPass() {
        return getCryptographer().decrypt(password);
    }

    @Override
    public Object getAuditEntry(Short revtype) {
        SecUserAudit secUserV = null;
        try {


            Random rand = new Random();

            secUserV = new SecUserAudit();
            secUserV.setRevtype(revtype);

            BeanUtils.copyProperties(secUserV, this);
            secUserV.setId(new SecUserAuditId(this.loginId, rand.nextInt()));

            //secUserV.setId(new SecUserVId(this.loginId, CounterUtils.getNextAuditTableKey()));
        } catch (Exception e) {
            secUserV = null;
        }
        return secUserV;
    }
}
