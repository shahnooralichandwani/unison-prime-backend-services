package com.avanza.core.security.db;

import java.io.Serializable;

import com.avanza.core.data.DbObject;

public class GroupUserBinding extends DbObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6595279811157252036L;
    private String userGroupId;
    private String userId;

    // Constructors

    /**
     * default constructor
     */
    public GroupUserBinding() {

    }

    public GroupUserBinding(String userId, String userGroupId) {

        this.setUserId(userId);
        this.setUserGroupId(userGroupId);
    }

    public String getUserId() {

        return this.userId;
    }

    public void setUserId(String userId) {

        this.userId = userId;
    }

    public String getUserGroupId() {

        return this.userGroupId;
    }

    public void setUserGroupId(String userGroupId) {

        this.userGroupId = userGroupId;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;

        result = prime * result + ((this.userGroupId == null) ? 0 : this.userGroupId.hashCode());
        result = prime * result + ((this.userId == null) ? 0 : this.userId.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (this.getClass() != obj.getClass())
            return false;

        final GroupUserBinding other = (GroupUserBinding) obj;

        if (this.userGroupId == null) {

            if (other.userGroupId != null)
                return false;
        } else if (this.userGroupId.compareTo(other.userGroupId) != 0)
            return false;

        if (this.userId == null) {

            if (other.userId != null)
                return false;
        } else if (this.userId.compareTo(other.userId) != 0)
            return false;

        return true;
    }
}