package com.avanza.core.security;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.avanza.core.CoreInterface;
import com.avanza.core.security.db.DbSecurityImpl;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TypeHelper;
import com.avanza.core.util.configuration.ConfigSection;

public class ProviderFactory {

    static final String XML_PROVIDER = "SecurityProvider";
    static final String XML_PROVIDER_ASSIGNMENT = "SecurityProviderAssignment";
    static final String XML_NAME = "name";
    static final String XML_CLASS = "class";
    static final String XML_CONFIG = "Config";
    static final String XML_AUTHEN = "authentication";
    static final String XML_DATA = "data";
    static final String XML_LOCAL_AUTHEN = "local-authen";

    private static AuthenticationProvider defaultAuthenProvider;
    private static AuthenticationProvider defaultLocalAuthenProvider;
    private static SecurityDataProvider defaultDataProvider;
    private static String authenProviderName;
    private static String localAuthenProviderName;
    private static String dataProviderName;
    private static boolean isLoaded;
    private static Map<String, CoreInterface> providers = new HashMap<String, CoreInterface>();

    private ProviderFactory() {

    }

    public static void load(ConfigSection section) {

        if (ProviderFactory.isLoaded)
            return;

        // first load all security providers.
        List<ConfigSection> providerConfs = section.getChildSections(ProviderFactory.XML_PROVIDER);
        for (ConfigSection providerConf : providerConfs) {
            try {
                String className = providerConf.getTextValue(ProviderFactory.XML_CLASS);
                String providerName = providerConf.getTextValue(ProviderFactory.XML_NAME);
                CoreInterface obj = TypeHelper.createInstance(className, CoreInterface.class, true);
                obj.load(providerConf);
                providers.put(providerName, obj);
            } catch (Exception ex) {
                //Logger.logException(ex);
            }
        }

        // now assign providers to related variables.
        ConfigSection providerAssignmentConf = section.getChildSections(ProviderFactory.XML_PROVIDER_ASSIGNMENT).get(0);
        if (providerAssignmentConf != null) {
            authenProviderName = providerAssignmentConf.getValue(XML_AUTHEN, StringHelper.EMPTY);
            dataProviderName = providerAssignmentConf.getValue(XML_DATA, StringHelper.EMPTY);
            localAuthenProviderName = providerAssignmentConf.getValue(XML_LOCAL_AUTHEN,
                    StringHelper.EMPTY);
        }

        // initialize default providers
        DbSecurityImpl impl = new DbSecurityImpl();
        ProviderFactory.defaultAuthenProvider = impl;
        ProviderFactory.defaultDataProvider = impl;
        ProviderFactory.defaultLocalAuthenProvider = impl;

        ProviderFactory.isLoaded = true;

    }

    public static void dispose() {

        if (!ProviderFactory.isLoaded)
            return;

        ProviderFactory.defaultAuthenProvider.dispose();
        ProviderFactory.defaultDataProvider.dispose();
        ProviderFactory.defaultLocalAuthenProvider.dispose();

        ProviderFactory.defaultAuthenProvider = null;
        ProviderFactory.defaultDataProvider = null;
        ProviderFactory.defaultLocalAuthenProvider = null;

        for (String key : providers.keySet()) {
            providers.get(key).dispose();
        }
        providers.clear();

        ProviderFactory.isLoaded = false;
    }

    public static AuthenticationProvider getAuthenticationProvider() {
        AuthenticationProvider provider = getProvider(authenProviderName,
                AuthenticationProvider.class);
        return (provider != null) ? provider : defaultAuthenProvider;
    }

    public static SecurityDataProvider getDataProvider() {
        SecurityDataProvider provider = getProvider(dataProviderName,
                SecurityDataProvider.class);
        return (provider != null) ? provider : defaultDataProvider;
    }

    public static AuthenticationProvider getLocalAuthenProvider() {
        AuthenticationProvider provider = getProvider(localAuthenProviderName,
                AuthenticationProvider.class);
        return (provider != null) ? provider : defaultLocalAuthenProvider;
    }

    @SuppressWarnings("unchecked")
    private static <T extends CoreInterface> T getProvider(String providerName, Class<T> type) {
        if (StringHelper.isNotEmpty(providerName)) {
            CoreInterface provider = providers.get(providerName);
            if (provider == null) {
                throw new RuntimeException(String.format(
                        "Unable to find Security Provider for key %s"
                        , providerName));
            } else if (!type.isAssignableFrom(provider.getClass())) {
                throw new RuntimeException(String.format(
                        "Unable to Cast Security Provider for key %s to type %s",
                        providerName
                        , type.getName()));
            } else {
                return (T) provider;
            }
        }
        return null;

    }
}