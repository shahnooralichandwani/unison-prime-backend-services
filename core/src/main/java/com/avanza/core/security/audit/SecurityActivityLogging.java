package com.avanza.core.security.audit;

import java.util.Date;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.User;
import com.avanza.core.util.CounterUtils;
import com.avanza.core.util.Logger;
import com.avanza.core.web.config.WebContext;

public class SecurityActivityLogging {
    public static final String USER_MODULE_ID = "Unison.Admin.User";
    public static final String USER_GROUP_MODULE_ID = "Unison.Admin.UserGroup";
    public static final String TEMPLATE_MODULE_ID = "Unison.Admin.Template";
    public static final String MESSAGE_CENTRE_MODULE_ID = "Unison.Admin.MessageCentre";
    public static final String CALL_SCRIPT_MODULE_ID = "Unison.Admin.CallScript";
    public static final String ACTIVITY_TYPE_MODULE_ID = "Unison.Admin.ActivityType";
    public static final String ORG_ROLE_MODULE_ID = "Unison.Admin.OrgRole";
    public static final String ORG_UNIT_MODULE_ID = "Unison.Admin.OrgUnit";
    public static final String PICK_LIST_MODULE_ID = "Unison.Admin.PickList";
    public static final String CALENDAR_MODULE_ID = "Unison.Admin.Calendar";
    public static final String SECURITY_POLICY_MODULE_ID = "Unison.Admin.SecurityPolicy";
    public static final String VALIDATION_SCRIPT_MODULE_ID = "Unison.Admin.ValidationScript";
    public static final String SYSTEM_DATASOURCE_MODULE_ID = "Unison.Admin.SystemDataSource";
    public static final String VALUE_TREE_MODULE_ID = "Unison.Admin.ValueTree";
    public static final String DESIGNER_MODULE_ID = "Unison.Admin.Designer";
    public static final String RELOAD_MODULE_ID = "Unison.Admin.ReloadData";
    public static final String CONTACT_MODULE_ID = "Unison.Main.OtherService.Contact";

    private static final Logger sysLogger = Logger.getLogger("sysLogger");

    @Deprecated
    public static void logSecutityLogging(String moduleId, String metaEntityId, String description, ActivityOperationType operationType, String indicator) {
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        if (webContext == null)
            return;

        User user = webContext.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
        ActivityLog activityLog = webContext.getAttribute(SessionKeyLookup.ACTIVITY_LOG_KEY);

        ActivityLogDetail activityLogDetail = new ActivityLogDetail();
        activityLogDetail.setActivityLog(activityLog);
        activityLogDetail.setActivitySessionKey(activityLog.getActivitySessionKey());
        activityLogDetail.setMetaEntityId(null);
        activityLogDetail.setModuleId(moduleId);
        activityLogDetail.setOperationType(operationType.toString());
        activityLogDetail.setDescription(description);
        activityLogDetail.setActivityTime(new Date());
        activityLogDetail.setCreation(user.getLoginId());
        activityLogDetail.setIndicator(indicator);

        //DBActivityLogger.getInstance().addActivityLogDetail(activityLogDetail);	

        sysLogger.logInfo(user.getFullName() + "," + activityLogDetail.getOperationType() + "," +
                activityLogDetail.getActivityTime() + "," + activityLogDetail.getIndicator() + "," +
                activityLogDetail.getDescription());

    }

    //Shuwair temp
    public static void logSecutityLogging(String moduleId, String metaEntityId, String description, ActivityOperationType operationType) {
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        if (webContext == null)
            return;

        User user = webContext.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
        ActivityLog activityLog = webContext.getAttribute(SessionKeyLookup.ACTIVITY_LOG_KEY);

        ActivityLogDetail activityLogDetail = new ActivityLogDetail();
        activityLogDetail.setActivityLog(activityLog);
        activityLogDetail.setActivitySessionKey(activityLog.getActivitySessionKey());
        activityLogDetail.setMetaEntityId(null);
        activityLogDetail.setModuleId(moduleId);
        activityLogDetail.setOperationType(operationType.toString());
        activityLogDetail.setDescription(description);
        activityLogDetail.setActivityTime(new Date());
        activityLogDetail.setCreation(user.getLoginId());
        //activityLogDetail.setIndicator(indicator);

        //DBActivityLogger.getInstance().addActivityLogDetail(activityLogDetail);	

        sysLogger.logInfo(user.getFullName() + "," + activityLogDetail.getOperationType() + "," +
                activityLogDetail.getActivityTime() + "," + activityLogDetail.getIndicator() + "," +
                activityLogDetail.getDescription());

    }
}
