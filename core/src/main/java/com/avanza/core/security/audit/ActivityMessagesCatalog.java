package com.avanza.core.security.audit;

import com.avanza.core.CoreException;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;

import java.util.HashMap;
import java.util.List;

/**
 * This class is written for managing DB operation related to SecActivityMessages
 *
 * @author arsalan.ahmed
 */
public class ActivityMessagesCatalog {

    private static final Logger logger = Logger.getLogger(ActivityMessagesCatalog.class);


    private static HashMap<String, SecActivityMessages> activityMessages = new HashMap<String, SecActivityMessages>();
    private static HashMap<String, String> configProperties = new HashMap<String, String>();

    private static String XMLproperties = "Properties";
    private static String XMLproperty = "Activity_Log_Provider";
    private static String XMLkey = "key";
    private static String XMLvalue = "value";


    /**
     * This method load all the activity messages exist in SecActivityMessages table and put them into activityMessages HashMap.
     */
    public static void load(ConfigSection configSection) {
        DataBroker broker = DataRepository.getBroker(ActivityMessagesCatalog.class.getName());
        List<SecActivityMessages> listSecActivityMessages = null;
        try {
            //Load common all messages
            listSecActivityMessages = broker.findAll(SecActivityMessages.class);
            if (listSecActivityMessages != null && !listSecActivityMessages.isEmpty()) {
                for (SecActivityMessages activityMsg : listSecActivityMessages) {
                    activityMessages.put(activityMsg.getActivityKey(), activityMsg);
                }
            }

            loadConfigProperties(configSection);
        } catch (Exception ex) {
            throw new CoreException(ex, "Ubable to load SecActivityMessages");
        }

    }

    /**
     * This method will be responsible for setting activity logging configuration
     *
     * @param configSection
     */
    private static void loadConfigProperties(ConfigSection configSection) {
        try {
            if (configSection != null && configSection.getChildSections().size() > 0) {
                ConfigSection propSection = configSection.getChild(XMLproperties);
                if (propSection != null) {
                    List<ConfigSection> childSections = propSection.getChildSections(XMLproperty);
                    for (ConfigSection sec : childSections) {
                        String key = sec.getTextValue(XMLkey);
                        String value = sec.getTextValue(XMLvalue);
                        configProperties.put(key, value);
                    }
                }
            }
        } catch (Exception e) {
            logger.LogException("Exception while loading Designer Config", e);
            throw new CoreException(e, "Exception while loading Designer Config");
        }
    }

    /**
     * This method will return configuration value against the key, if key value not found return empty string
     *
     * @param configKey
     * @return
     */
    public static String getConfiguration(String configKey) {

        String configValue = configProperties.get(configKey) != null ? configProperties.get(configKey) : "";
        return configValue;
    }


    /**
     * This method return SecActivityMessages object if its found in activityMessages HashMap otherwise return null
     *
     * @param activityKey
     * @return
     */
    public static SecActivityMessages getSecActivityMessages(String activityKey) {

        SecActivityMessages secActivityMessages = null;
        if (activityMessages != null) {
            secActivityMessages = activityMessages.get(activityKey);
        }

        return secActivityMessages;

    }

    public void persistActivityLogDetails(List<ActivityLogDetail> activityLogDetails) throws Exception {
        try {
            DataBroker broker = DataRepository.getBroker(ActivityMessagesCatalog.class.getName());
            broker.persistAll(activityLogDetails);
        } catch (Exception ex) {
            logger.LogException("Exceeption", ex);
            throw ex;
        }

    }


}
