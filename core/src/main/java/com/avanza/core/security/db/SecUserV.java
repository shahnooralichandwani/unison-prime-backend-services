package com.avanza.core.security.db;

import java.sql.Timestamp;
import java.util.Date;

import com.avanza.core.data.DbObject;
import com.avanza.core.security.User;

/**
 * SecUserV entity. @author MyEclipse Persistence Tools
 */

public class SecUserV extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = -8160610838051168776L;
    // Fields

    private SecUserVId id;
    private Short revtype;
    private String password;
    private String fullName;
    private String firstName;
    private String lastName;
    private String middleName;
    private String emailUrl;
    private Date whenDeleted;
    private String preferedCulture;
    private String policyId;
    private Date lastLogin;
    private String statusId;
    private String otherId;
    private boolean system;
    private String mobilePhone;
    private String defaultOrgUnit;
    private String defaultLocale;


    // Constructors

    /**
     * default constructor
     */
    public SecUserV() {
    }


    // Property accessors

    public SecUserVId getId() {
        return this.id;
    }

    public void setId(SecUserVId id) {
        this.id = id;
    }

    public Short getRevtype() {
        return this.revtype;
    }

    public void setRevtype(Short revtype) {
        this.revtype = revtype;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getEmailUrl() {
        return this.emailUrl;
    }

    public void setEmailUrl(String emailUrl) {
        this.emailUrl = emailUrl;
    }

    public Date getWhenDeleted() {
        return this.whenDeleted;
    }

    public void setWhenDeleted(Date whenDeleted) {
        this.whenDeleted = whenDeleted;
    }

    public String getPreferedCulture() {
        return this.preferedCulture;
    }

    public void setPreferedCulture(String preferedCulture) {
        this.preferedCulture = preferedCulture;
    }

    public String getPolicyId() {
        return this.policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public Date getLastLogin() {
        return this.lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }


    public String getStatusId() {
        return this.statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getOtherId() {
        return this.otherId;
    }

    public void setOtherId(String otherId) {
        this.otherId = otherId;
    }

    public boolean getSystem() {
        return this.system;
    }

    public void setSystem(boolean system) {
        this.system = system;
    }

    public boolean isSystem() {
        return system;
    }

    public String getMobilePhone() {
        return this.mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getDefaultOrgUnit() {
        return this.defaultOrgUnit;
    }

    public void setDefaultOrgUnit(String defaultOrgUnit) {
        this.defaultOrgUnit = defaultOrgUnit;
    }

    public String getDefaultLocale() {
        return this.defaultLocale;
    }

    public void setDefaultLocale(String defaultLocale) {
        this.defaultLocale = defaultLocale;
    }


}