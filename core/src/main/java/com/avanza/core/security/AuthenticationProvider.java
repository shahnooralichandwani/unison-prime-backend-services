package com.avanza.core.security;

import java.util.List;

import com.avanza.core.CoreInterface;
import com.avanza.core.data.expression.Search;
import com.avanza.core.util.configuration.ConfigSection;

public interface AuthenticationProvider extends CoreInterface {

    void load(ConfigSection section);

    void dispose();

    AuthenticationStatus authenticate(String loginId, String password);

    AuthenticationStatus changePassword(User user, String oldPwd, String newPwd);

    List<User> getUsers(Search query);

    boolean userExists(String loginId);
}