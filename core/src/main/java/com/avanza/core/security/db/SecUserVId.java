package com.avanza.core.security.db;

/**
 * SecUserVId entity. @author MyEclipse Persistence Tools
 */

public class SecUserVId implements java.io.Serializable {

    // Fields

    private String loginId;
    private Integer rev;

    // Constructors

    /**
     * default constructor
     */
    public SecUserVId() {
    }

    /**
     * full constructor
     */
    public SecUserVId(String loginId, Integer rev) {
        this.loginId = loginId;
        this.rev = rev;
    }

    // Property accessors

    public String getLoginId() {
        return this.loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public Integer getRev() {
        return this.rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof SecUserVId))
            return false;
        SecUserVId castOther = (SecUserVId) other;

        return ((this.getLoginId() == castOther.getLoginId()) || (this
                .getLoginId() != null
                && castOther.getLoginId() != null && this.getLoginId().equals(
                castOther.getLoginId())))
                && ((this.getRev() == castOther.getRev()) || (this.getRev() != null
                && castOther.getRev() != null && this.getRev().equals(
                castOther.getRev())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result
                + (getLoginId() == null ? 0 : this.getLoginId().hashCode());
        result = 37 * result
                + (getRev() == null ? 0 : this.getRev().hashCode());
        return result;
    }

}