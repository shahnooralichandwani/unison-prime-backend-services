package com.avanza.core.security.audit;


import com.avanza.core.data.DbObject;

/**
 * Shahbaz Ali
 * For Meta View Editing Logging purpose
 */

public class AuditLog extends DbObject implements java.io.Serializable {

    // Fields

    /**
     *
     */
    private static final long serialVersionUID = 8839054992705832058L;
    private String auditLogId;
    private String metaViewAttribId;
    private String loginId;
    private String oldValue;
    private String newValue;
    private String workflowLog;

    // Constructors

    /**
     * default constructor
     */
    public AuditLog() {
    }

    /**
     * full constructor
     */
    public AuditLog(String auditLogId, String metaViewAttrib,
                    String secUser, String oldValue, String newValue) {
        this.auditLogId = auditLogId;
        this.metaViewAttribId = metaViewAttrib;
        this.loginId = secUser;
        this.oldValue = oldValue;
        this.newValue = newValue;

    }

    // Property accessors

    public String getAuditLogId() {
        return this.auditLogId;
    }

    public void setAuditLogId(String auditLogId) {
        this.auditLogId = auditLogId;
    }


    public String getOldValue() {
        return this.oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return this.newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getMetaViewAttribId() {
        return metaViewAttribId;
    }

    public void setMetaViewAttribId(String metaViewAttribId) {
        this.metaViewAttribId = metaViewAttribId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getWorkflowLog() {
        return workflowLog;
    }

    public void setWorkflowLog(String workflowLog) {
        this.workflowLog = workflowLog;
    }

}