package com.avanza.core.security.auditTables.adaptor;

import java.util.Iterator;

import com.avanza.core.security.auditTables.AuditableDTO;

/**
 * Template for audit table adaptor
 *
 * @author arsalan.ahmed
 */
public interface AuditableLogAdaptor {

    /**
     * This method will be responsible for managing table audit logging
     *
     * @param entities
     */
    public void addTableAuditLog(AuditableDTO auditTableDTO);

    /**
     * This method clear the backlog
     */
    public void clear();

}
