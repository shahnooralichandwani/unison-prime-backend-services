/**
 *
 */
package com.avanza.core.security;

/**
 * @author shahbaz.ali
 *
 */
public interface SecurityPolicy {

    public abstract void onBeforeAuthenticate(String loginId);

    public abstract SecurityAuthenticationResponse onAuthenticate(String loginId);

    public abstract SecurityCredentialResponse onPasswordUpdate(String loginId,
                                                                String newPassword);

    public abstract SecurityAuthenticationResponse onAuthenticationFailure(
            String loginId);

}
