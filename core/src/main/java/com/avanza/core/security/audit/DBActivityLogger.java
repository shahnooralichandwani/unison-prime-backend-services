package com.avanza.core.security.audit;

import java.sql.Date;
import java.util.Calendar;

import com.avanza.core.CoreException;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.security.db.SecurityLoginHistory;
import com.avanza.core.util.Logger;

/**
 * @author kraza
 */
public class DBActivityLogger {

    private static final Logger logger = Logger.getLogger(DBActivityLogger.class);
    private DataBroker broker;
    private static DBActivityLogger _instance = new DBActivityLogger();

    protected DBActivityLogger() {
        broker = DataRepository.getBroker(DBActivityLogger.class.getName());
    }

    public static DBActivityLogger getInstance() {

        return _instance;
    }

    public void addActivityLog(ActivityLog activityLog) {
        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        this.broker.getSession();
        txContext.beginTransaction();

        try {
            broker.add(activityLog);
            txContext.commit();
        } catch (Exception e) {
            logger.LogException("Exception occoured while DBActivityLogger.add(ActivityLog).", e);
            txContext.rollback();
            throw new CoreException("Exception occoured while DBActivityLogger.add(ActivityLog).", e);
        }
    }

    public void updateActivityLog(ActivityLog activityLog) {
        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        this.broker.getSession();
        txContext.beginTransaction();

        try {
            broker.update(activityLog);
            txContext.commit();
        } catch (Exception e) {
            logger.LogException("Exception occoured while DBActivityLogger.update(ActivityLog).", e);
            txContext.rollback();
            throw new CoreException("Exception occoured while DBActivityLogger.update(ActivityLog).", e);
        }
    }

    public void addActivityLogDetail(ActivityLogDetail activityLog) {
        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        this.broker.getSession();
        txContext.beginTransaction();

        try {
            broker.add(activityLog);
            txContext.commit();
        } catch (Exception e) {
            logger.LogException("Exception occoured while DBActivityLogger.add(ActivityLogDetail).", e);
            txContext.rollback();
            throw new CoreException("Exception occoured while DBActivityLogger.add(ActivityLogDetail).", e);
        }
    }

    public void updateActivityLogDetail(ActivityLogDetail activityLog) {
        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();
        this.broker.getSession();
        txContext.beginTransaction();

        try {
            broker.update(activityLog);
            txContext.commit();
        } catch (Exception e) {
            logger.LogException("Exception occoured while DBActivityLogger.update(ActivityLogDetail).", e);
            txContext.rollback();
            throw new CoreException("Exception occoured while DBActivityLogger.update(ActivityLogDetail).", e);
        }
    }

    public void addLoginActivity(String loginId, String remoteIp, String status) {
        SecurityLoginHistory securityLoginHistory = new SecurityLoginHistory();
        securityLoginHistory.setAuthenticationStatus(status);
        securityLoginHistory.setLoginId(loginId);
        securityLoginHistory.setRemoteIp(remoteIp);
        securityLoginHistory.setLoginTime(Calendar.getInstance().getTime());
        broker.persist(securityLoginHistory);

    }
}
