package com.avanza.core.security.auditTables.logger;

import java.util.List;

public interface AuditableLogger {

    public void logEntries(List<Object> entries);
}
