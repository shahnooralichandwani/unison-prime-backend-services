package com.avanza.core.security.db;

import java.io.Serializable;


public class GroupPermissionId implements Serializable {

    private static final long serialVersionUID = 2883328543906549918L;
    private String permissionId;
    private String appId;
    private String userGroupId;

    public String getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(String userGroupId) {
        this.userGroupId = userGroupId;
    }

    public GroupPermissionId() {
    }

    public GroupPermissionId(String permissionId, String appId, String userGroupId) {
        this.permissionId = permissionId;
        this.appId = appId;
        this.userGroupId = userGroupId;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPermissionId() {
        return this.permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + ((appId == null) ? 0 : appId.hashCode());
        result = PRIME * result + ((permissionId == null) ? 0 : permissionId.hashCode());
        result = PRIME * result + ((userGroupId == null) ? 0 : userGroupId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final GroupPermissionId other = (GroupPermissionId) obj;
        if (appId == null) {
            if (other.appId != null)
                return false;
        } else if (!appId.equals(other.appId))
            return false;
        if (permissionId == null) {
            if (other.permissionId != null)
                return false;
        } else if (!permissionId.equals(other.permissionId))
            return false;
        if (userGroupId == null) {
            if (other.userGroupId != null)
                return false;
        } else if (!userGroupId.equals(other.userGroupId))
            return false;
        return true;
    }
}