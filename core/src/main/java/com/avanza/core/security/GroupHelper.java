package com.avanza.core.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.StringHelper;

public abstract class GroupHelper extends DbObject {

    protected HashSet<UserGroup> userGroups;
    protected HashSet<UserGroup> addedGroups;
    protected HashSet<UserGroup> removedGroups;

    protected GroupHelper() {
        this.addedGroups = new HashSet<UserGroup>();
        this.removedGroups = new HashSet<UserGroup>();
    }

    public abstract UserGroup getUserGroup(String userGroupId);

    protected abstract HashSet<UserGroup> fillGroups();

    public void addGroup(String userGroupId) {

        if (StringHelper.isEmpty(userGroupId))
            return;

        UserGroup userGroup = this.getUserGroup(userGroupId);

        addGroup(userGroup);
    }

    public void removeGroup(String userGroupId) {

        UserGroup userGrp = getUserGroup(userGroupId);
        removeGroup(userGrp);
    }

    public void addGroup(UserGroup group) {

        this.innerGetGroups();

        if (this.removedGroups.remove(group)) {
            return;
        }

        if (!this.userGroups.contains(group)) {
            this.addedGroups.add(group);
        }
    }

    public void removeGroup(UserGroup group) {

        this.innerGetGroups();

        if (this.addedGroups.remove(group)) {
            return;
        }

        if (this.userGroups.contains(group))
            this.removedGroups.add(group);
    }

    protected void innerGetGroups() {

        if (this.userGroups == null)
            this.userGroups = this.fillGroups();
    }

    public void addGroup(List<UserGroup> groupList) {

        if (groupList == null)
            return;

        for (UserGroup group : groupList) {

            this.addGroup(group);
        }
    }

    public List<UserGroup> getUserGroups() {

        innerGetGroups();

        List<UserGroup> retVal = new ArrayList<UserGroup>();

        for (UserGroup uGroup : this.userGroups) {
            retVal.add(uGroup);
        }

        return retVal;
    }

    public void addGroupId(List<String> groupIdList) {

        if (groupIdList == null)
            return;

        for (String groupId : groupIdList) {
            addGroup(groupId);
        }
    }

    public HashSet<UserGroup> getAddedGroups() {
        return addedGroups;
    }

    public HashSet<UserGroup> getRemovedGroups() {
        return removedGroups;
    }

    /**
     * This method synchronizes the userGroup, This method is called after persistance by DBSecurityImpl.
     */
    public void synchronizeGroups() {

        if (this.userGroups != null) {
            for (UserGroup userGroup : this.addedGroups) {
                this.userGroups.add(userGroup);
                //this.innerSyncAdd(userGroup);
            }

            this.addedGroups.clear();


            for (UserGroup userGroup : this.removedGroups) {
                this.userGroups.remove(userGroup);
                //this.innerSyncRemove(userGroup);
            }
        }

        this.removedGroups.clear();
    }

//    protected abstract void innerSyncAdd(UserGroup group);

//    protected abstract void innerSyncRemove(UserGroup group);
}