package com.avanza.core.security;

/**
 * @author shahbaz.ali
 */
public enum CredentialNavigationOutcome {

    // null means to stay in the page
    OK("OK"), InvalidPasswordFormat(null), InvalidPasswordLength(null);

    CredentialNavigationOutcome(String value) {
        this.value = value;
    }

    private String value;

    public String toString() {
        return this.value;
    }

}
