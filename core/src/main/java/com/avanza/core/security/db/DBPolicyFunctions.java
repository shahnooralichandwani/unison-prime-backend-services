/**
 *
 */
package com.avanza.core.security.db;

import java.util.Date;
import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.OrderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.security.SecurityException;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.User;

/**
 * @author shahbaz.ali
 *
 */
public class DBPolicyFunctions {

    private DataBroker broker;
    private final String DEF_BROKER_NAME = "com.avanza.core.security";

    public DBPolicyFunctions() {
        this.broker = DataRepository.getBroker(DEF_BROKER_NAME);
    }

    public User getUser(String loginId) {
        return SecurityManager.getUser(loginId);
    }

    public Date getLastLogin(String loginId) {

        User user = this.getUser(loginId);
        if (user != null)
            return user.getLastLogin();

        else
            throw new SecurityException("User not found with login id " + loginId);
    }

    public Date getLastPasswordChangeTime(String loginId) {

        Search query = new Search(PasswordHistoryDbImpl.class);
        query.addCriterion(SimpleCriterion.equal("loginId", loginId));
        query.addOrder("createdOn", OrderType.DESC);

        List<PasswordHistoryDbImpl> passwordHistory = broker.find(query);

        if (passwordHistory.size() > 0) {
            return passwordHistory.get(0).getCreatedOn();
        } else
            return null;

    }


    public List<PasswordHistoryDbImpl> getPasswordsFromHistory(String loginId) {

        Search query = new Search(PasswordHistoryDbImpl.class);
        query.addCriterion(SimpleCriterion.equal("loginId", loginId));
        query.addOrder("createdOn", OrderType.DESC);

        return broker.find(query);


    }


}
