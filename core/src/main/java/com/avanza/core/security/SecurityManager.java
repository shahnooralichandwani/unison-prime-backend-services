package com.avanza.core.security;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.security.audit.DBActivityLogger;
import com.avanza.core.security.db.*;
import com.avanza.core.sessionhistory.ValidateScript;
import com.avanza.core.util.Guard;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecurityManager {

    private static final Logger logger = Logger.getLogger(SecurityManager.class);
    public static final String XML_SECURITY_MANAGER = "SecurityManager";
    static final String URL_AUTHORIZATION = "urlAuthorization";
    private static boolean urlAuthorization;
    static HashMap<String, Permission> permissionList;
    static Map<String, SecurityPolicy> policies = new HashMap<String, SecurityPolicy>(0);

    private SecurityManager() {

    }

    public static void load(ConfigSection section) {
        ConfigSection managerConf = section.getChildSections(XML_SECURITY_MANAGER).get(0);
        if (managerConf != null) {
            urlAuthorization = managerConf.getBooleanValue(URL_AUTHORIZATION);
        }


        List<Permission> result = SecurityManager.findPermission(null);
        permissionList = new HashMap<String, Permission>(result.size());
        for (Permission item : result) {
            SecurityManager.permissionList.put(item.getPermissionId(), item);
        }
    }

    public static Permission getPermission(String permissionId,
                                           boolean throwExcep) {

        Permission retVal = null;
        if (SecurityManager.permissionList != null)
            retVal = SecurityManager.permissionList.get(permissionId);

        if ((retVal == null) && throwExcep)
            throw new SecurityException("Permission [%1$s] is not defined in the database", permissionId);

        return retVal;
    }

    public static Permission getPermission(String permissionId) {

        return SecurityManager.getPermission(permissionId, true);
    }

    public static PermissionBinding prepareBindings() {

        PermissionBinding retVal = new PermissionBinding(SecurityManager.permissionList.size());

        for (Permission item : SecurityManager.permissionList.values()) {
            retVal.add(new PermissionBinding(item));
        }

        return retVal;
    }

    public static void dispose() {

        ProviderFactory.dispose();
    }

    public static AuthenticateResponseObj authenticateUser(String loginId, String password) {
        AuthenticateResponseObj responseObj = new AuthenticateResponseObj();

        AuthenticationProvider authenTemp = ProviderFactory.getAuthenticationProvider();
        if (authenTemp == null)
            return responseObj;

        AuthenticationStatus authenticationStatus = authenTemp.authenticate(loginId, password);

        String remoteIp = null;
		
		/*// Get remote IP of user for exception logging
		if (FacesContext.getCurrentInstance() != null) {
			WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
			remoteIp= ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
			ctx.setAttribute("remoteIp", remoteIp);
		}
		
		addLoginActivity(loginId, remoteIp, authenticationStatus.toString());
		
		// if successfull
		if (authenticationStatus == AuthenticationStatus.Success) {
			SecurityDataProvider dataTemp = ProviderFactory.getDataProvider();
			User retVal = dataTemp.getUser(loginId.toLowerCase());
			// retVal.resetPassword(password);
			responseObj.setUser(retVal);
			responseObj.setStatus(authenticationStatus);
			return responseObj;
		}
		else if(authenticationStatus == AuthenticationStatus.InvalidRole){
			String message = ResourceUtil.getInstance().getProperty("unison","login_invalidlogingidroleerror");
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,message,""));
			
		}else if(authenticationStatus == AuthenticationStatus.InvalidGroup){
			String message = ResourceUtil.getInstance().getProperty("unison","login_invalidlogingidgrouperror");
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,message,""));
			
		}
		
		else if (authenticationStatus == AuthenticationStatus.InvalidPassword){
			String message= ResourceUtil.getInstance().getProperty("unison", "login_invalidlogingidpass_msg");
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,message,""));
		}
		else if (authenticationStatus == AuthenticationStatus.InvalidUser){
			String message= ResourceUtil.getInstance().getProperty("unison", "login_invalidlogingidpass_msg");
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,message,""));
		}
		else if (authenticationStatus == AuthenticationStatus.InActiveUser){
			String message= ResourceUtil.getInstance().getProperty("unison", "login_accountSuspended_msg");
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,message,""));
		}*/

        responseObj.setUser(null);
        responseObj.setStatus(authenticationStatus);
        return responseObj;

    }

    public static AuthenticationStatus changePassword(User user,
                                                      String oldPassword, String newPassword) {

        return ProviderFactory.getAuthenticationProvider().changePassword(user, oldPassword, newPassword);
    }

    public static void persistUser(User user) {

        TransactionContext txnContext = ApplicationContext.getContext().getTxnContext();
        txnContext.beginTransaction();

        try {

            ProviderFactory.getDataProvider().persistUser(user);
            txnContext.commit();
        } catch (Exception e) {
            logger.logError("Exception occoured at Security manager while persistUser", e);
            txnContext.rollback();
            throw new SecurityException(e, "Error occured while persisting %s. Check Exception details", user.getLoginId());
        } finally {

            txnContext.release();
        }
    }

    public static void deleteUser(User user) {

        ProviderFactory.getDataProvider().deleteUser(user);
    }

    public static <T> void deleteBinding(T grpBinding) {
        TransactionContext txnContext = ApplicationContext.getContext().getTxnContext();
        txnContext.beginTransaction();

        try {

            ((DbSecurityImpl) ProviderFactory.getDataProvider()).deleteBinding(grpBinding);
            txnContext.commit();
        } catch (Exception e) {
            e.printStackTrace();
            logger.logError("Exception occoured at Security manager while persistGroup", e);
            txnContext.rollback();

        } finally {

            txnContext.release();
        }

    }

    public static List<User> findUser(Search criteria) {

        return ProviderFactory.getDataProvider().findUsers(criteria);
    }

    public static User getUser(String loginId) {

        return ProviderFactory.getDataProvider().getUser(loginId);
    }

    public static void persistGroup(UserGroup group) {

        TransactionContext txnContext = ApplicationContext.getContext().getTxnContext();
        txnContext.beginTransaction();

        try {

            ProviderFactory.getDataProvider().persistGroup(group);
            txnContext.commit();
        } catch (Exception e) {
            e.printStackTrace();
            logger.logError("Exception occoured at Security manager while persistGroup", e);
            txnContext.rollback();
            throw new SecurityException(e, "Error occured while persisting UserGroup %s. Check Exception details", group.getUserGroupId());
        } finally {

            txnContext.release();
        }
    }

    public static List<UserGroup> findGroup(Search criteria) {

        return ProviderFactory.getDataProvider().findGroups(criteria);
    }

    public static UserGroup getGroup(String groupId) {

        return ProviderFactory.getDataProvider().getGroup(groupId);
    }

    public static ValidateScript getValidateScript(String validateScriptId) {

        return ProviderFactory.getDataProvider().getValidateScript(validateScriptId);
    }

    public static List<Permission> findPermission(Search criteria) {

        return ProviderFactory.getDataProvider().findPermissions(criteria);
    }

    public static void persistPolicy(Policy policy) {

        /* Shahbaz:
         *  Reset all policies ,already loaded in the session
         *  Note: this call is not mandatory can be hide if explicit server reset is required
         */
        resetPolicies();

        ProviderFactory.getDataProvider().persistPolicy(policy);
    }

    public static <T> void persistBinding(T grpBinding) {
        TransactionContext txnContext = ApplicationContext.getContext().getTxnContext();
        txnContext.beginTransaction();

        try {

            ((DbSecurityImpl) ProviderFactory.getDataProvider()).persistBindings(grpBinding);
            txnContext.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            txnContext.release();
        }

    }

    public static List<Policy> findPolicy(Search criteria) {

        return ProviderFactory.getDataProvider().findPolicies(criteria);
    }

    public static User createUser(String loginId, String password) {

        if (ProviderFactory.getAuthenticationProvider().userExists(loginId))
            throw new SecurityException("The user with loginId: %s already exsts.", loginId);

        User user = ProviderFactory.getDataProvider().createUser(loginId, password);

        return user;
    }

    public static UserGroup createUserGroup(String groupId) {

        Guard.checkNullOrEmpty(groupId, "SecurityManager.createUserGroup(String)");

        return ProviderFactory.getDataProvider().createUserGroup(groupId);
    }

    public static DataBroker GetBroker() {
        return ((DbSecurityImpl) (ProviderFactory.getDataProvider())).GetBroker();
    }

    public static void createOrUpdateValidationScript(
            ValidateScript selectedScript) {

        ProviderFactory.getDataProvider().persistValidateScript(selectedScript);

    }

    public static SecurityPolicy getUserPolicyImpl(String loginId) {

        try {

            if (SecurityManager.policies.containsKey(loginId))
                return SecurityManager.policies.get(loginId);

            SecurityDataProvider dataTemp = ProviderFactory.getDataProvider();
            User user = dataTemp.getUser(loginId.toLowerCase());

            if (user != null) {

                String policyId = ((UserDbImpl) user).getPolicyId();

                if (policyId != null) {
                    Search search = new Search(PolicyDbImpl.class);
                    search.addCriterion(SimpleCriterion.equal("policyId", policyId));
                    SecurityPolicy secPolicy = null;

                    List<Policy> policies = ProviderFactory.getDataProvider().findPolicies(search);

                    if (policies.size() > 0) {
                        Policy userPolicy = policies.get(0);
                        String implClass = userPolicy.getImplClass();
                        Constructor constructor = Class.forName(implClass).getConstructor(Policy.class);
                        secPolicy = (SecurityPolicy) constructor.newInstance(userPolicy);
                        SecurityManager.policies.put(user.getLoginId(), secPolicy);

                        return secPolicy;
                    }
                } else {
                    return getDefaultSecurityImpl();
                }

            }

        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public static boolean isUrlAuthorization() {
        return urlAuthorization;
    }

    public static SecurityPolicy getDefaultSecurityImpl() {
        try {
            Search search = new Search(Policy.class);
            search.addCriterion(SimpleCriterion.equal("defaultPolicy", true));
            SecurityPolicy secPolicy = null;

            List<Policy> policies = ProviderFactory.getDataProvider().findPolicies(search);

            if (policies.size() > 0) {
                Policy userPolicy = policies.get(0);
                String implClass = userPolicy.getImplClass();
                Constructor constructor = Class.forName(implClass).getConstructor(Policy.class);
                secPolicy = (SecurityPolicy) constructor.newInstance(userPolicy);
                return secPolicy;
            }


        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    private static void addLoginActivity(String loginId, String remoteIp, String authenticationStatus) {
        DBActivityLogger.getInstance().addLoginActivity(loginId, remoteIp, authenticationStatus);
    }


    public static Date getLastLoginTime(String loginId) throws SecurityException {

        User user = getUser(loginId);

        if (user != null) {
            return user.getLastLogin();
        } else
            throw new SecurityException("User not found with login id " + loginId);
    }

    public static Date getPasswordChangeTime(String loginId) {
        DBPolicyFunctions policyFunction = new DBPolicyFunctions();
        return policyFunction.getLastPasswordChangeTime(loginId);
    }

    public static List<PasswordHistoryDbImpl> getPasswordsHistory(String loginId) {
        DBPolicyFunctions policyFunction = new DBPolicyFunctions();
        return policyFunction.getPasswordsFromHistory(loginId);
    }

    public static List<Policy> getDefaultPolicy() {
        Search search = new Search(Policy.class);
        search.addCriterion(SimpleCriterion.equal("defaultPolicy", true));
        SecurityPolicy secPolicy = null;

        return ProviderFactory.getDataProvider().findPolicies(search);

    }

    public static List<User> findUserByPolicy(Search search) {
        return ProviderFactory.getDataProvider().findUserByPolicy(search);
    }

    public static void resetPolicies() {
        policies.clear();
    }


}
