package com.avanza.core.security;


public abstract class Permission extends PermissionBase {

    // Fields
    protected PermissionType type;
    protected String primaryName;
    protected String secondaryName;

    protected PermissionCollection<Permission> childList;

    // Constructors

    /**
     * default constructor
     */
    protected Permission() {
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public String getSecondaryName() {
        return secondaryName;
    }

    public String getPermissionType() {

        String retVal = PermissionType.ID_NONE;

        switch (this.type) {

            case Module:
                retVal = PermissionType.ID_MODULE;
                break;

            case Transaction:
                retVal = PermissionType.ID_TRANSACTION;
                break;

            case Action:
                retVal = PermissionType.ID_ACTION;
                break;
        }

        return retVal;
    }

    public PermissionType getType() {
        return this.type;
    }

}