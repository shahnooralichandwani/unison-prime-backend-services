package com.avanza.core.security.audit;


import java.text.SimpleDateFormat;
import java.util.Date;

import com.avanza.core.data.DbObject;

/**
 * @author kraza
 */
public class ActivityLogDetail extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 211956900333904502L;

    private String activitySessionKey;
    private String metaEntityId;
    private ActivityLog activityLog;
    private String moduleId;
    private String operationType;
    private Date activityTime;
    private String description;
    private String indicator;
    private String actorId;
    private String recordId;
    private String relationshipNum;


    //Changes added for current Activity Tab in in-bound moudule:Anam Siddiqi


    private Boolean deleted;
    private String serNo;

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ActivityLogDetail() {
    }

    public String getActivitySessionKey() {
        return this.activitySessionKey;
    }

    public void setActivitySessionKey(String activitySessionKey) {
        this.activitySessionKey = activitySessionKey;
    }

    public ActivityLog getActivityLog() {
        return this.activityLog;
    }

    public void setActivityLog(ActivityLog activityLog) {
        this.activityLog = activityLog;
    }

    public String getModuleId() {
        return this.moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getOperationType() {
        return this.operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Date getActivityTime() {
        return this.activityTime;
    }

    public void setActivityTime(Date activityTime) {
        this.activityTime = activityTime;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public String getActorId() {
        return actorId;
    }

    public void setActorId(String actorId) {
        this.actorId = actorId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getRelationshipNum() {
        return relationshipNum;
    }

    public void setRelationshipNum(String relationshipNum) {
        this.relationshipNum = relationshipNum;
    }


    //Added for Current Activity Tab:Anam Siddiqi
    public String getTimeString() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
        if (df.format(new Date()).equalsIgnoreCase(df.format(this.activityTime))) {
            df = new SimpleDateFormat("hh:mm:ss");
            return df.format(this.activityTime);
        } else {
            df = new SimpleDateFormat("hh:mm:ss");
            return df.format(this.activityTime);
        }
    }

}
