package com.avanza.core.security;


import com.avanza.core.data.DbObject;
import com.avanza.core.util.StringHelper;

public abstract class PermissionBase extends DbObject implements Cloneable {

    public static final char NAME_SEPARATOR = '.';

    protected boolean canCreate;
    protected boolean canUpdate;
    protected boolean canDelete;
    protected String key;

    protected PermissionBase(boolean create, boolean update, boolean delete) {

        this.canCreate = create;
        this.canUpdate = update;
        this.canDelete = delete;
    }

    protected PermissionBase() {

    }

    // Methods
    @Override
    public abstract PermissionBase clone();

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object obj);

    public abstract PermissionType getType();

    public abstract String getAppId();

    public abstract String getPermissionId();

    public boolean getCanCreate() {
        return this.canCreate;
    }

    public boolean getCanUpdate() {
        return this.canUpdate;
    }

    public boolean getCanDelete() {
        return this.canDelete;
    }

    public boolean isReadOnly() {
        return !(this.canCreate || this.canDelete || this.canUpdate);
    }

    // setters used for internal purpose
    void setCanCreate(boolean canCreate) {
        this.canCreate = canCreate;
    }

    void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    void setCanUpdate(boolean canUpdate) {
        this.canUpdate = canUpdate;
    }

    void copyValues(PermissionBase copy) {

        this.canCreate = copy.canCreate;
        this.canUpdate = copy.canUpdate;
        this.canDelete = copy.canDelete;
    }

    void mergeValues(PermissionBase mergeFrom) {

        this.canCreate |= mergeFrom.canCreate;
        this.canUpdate |= mergeFrom.canUpdate;
        this.canDelete |= mergeFrom.canDelete;
    }

    protected String getKey() {

        if (StringHelper.isEmpty(this.key)) {

            String permId = this.getPermissionId();

            // Changed to last index of from indexOf
            int idx = permId.lastIndexOf(PermissionBase.NAME_SEPARATOR);
            if (idx == -1)
                this.key = permId;
            else
                this.key = permId.substring(idx + 1);
        }

        return this.key;
    }

    <T extends PermissionBase> T getPermission(String name, PermissionType enumValue) {
        return null;
    }
}