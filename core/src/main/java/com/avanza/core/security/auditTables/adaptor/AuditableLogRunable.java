package com.avanza.core.security.auditTables.adaptor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.avanza.core.interceptor.Auditable;
import com.avanza.core.security.auditTables.AuditableDTO;
import com.avanza.core.security.auditTables.logger.AuditableLoggerFactory;
import com.avanza.core.util.Logger;

/**
 * @author arsalan.ahmed
 */
public class AuditableLogRunable implements Runnable {

    private static final Logger logger = Logger.getLogger(AuditableLogRunable.class);
    private AuditableDTO auditTableDTO;


    public AuditableLogRunable(AuditableDTO auditTableDTO) {
        this.auditTableDTO = auditTableDTO;
    }


    @Override
    public void run() {
        List<Object> auditEntries = new ArrayList<Object>();
        try {
            //Populating audit entries for created objects
            for (Auditable aud : auditTableDTO.getInserts()) {
                Object obj = aud.getAuditEntry(Auditable.create);
                if (obj != null)
                    auditEntries.add(obj);
            }

            //Populating audit entries for updated objects
            for (Auditable aud : auditTableDTO.getUpdates()) {
                Object obj = aud.getAuditEntry(Auditable.update);
                if (obj != null)
                    auditEntries.add(obj);
            }

            //Populating audit entries for deleted objects
            for (Auditable aud : auditTableDTO.getDeletes()) {
                Object obj = aud.getAuditEntry(Auditable.delete);
                if (obj != null)
                    auditEntries.add(obj);
            }

            AuditableLoggerFactory.getLogger().logEntries(auditEntries);

        } catch (Exception e) {
            logger.logError("Exception occur in thread", e);
        }

    }
}
