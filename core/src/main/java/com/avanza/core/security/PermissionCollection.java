package com.avanza.core.security;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;

public class PermissionCollection<T extends PermissionBase> implements java.io.Serializable, Cloneable,
        java.lang.Iterable<T> {

    private static final long serialVersionUID = -1613999576049114675L;

    private List<T> childList;

    public PermissionCollection(List<T> childList) {

        this.childList = childList;
    }

    public PermissionCollection(int size) {
        this.childList = new ArrayList<T>(size);
    }

    public int size() {

        return this.childList.size();
    }
	
	/*
	private List<T> getChildList() {
		return this.childList;
	}
	
	private void setChildList(List<T> childList) {
		
		this.childList = childList;
	}
	*/

    private String trimId(String containerId, String id) {

        int idx = id.indexOf(containerId);
        int length = containerId.length();
        if (idx >= 0 && length > 0)
            id = id.substring(length + 1);

        return id;
    }

    public T getPermission(String containerId, String id) {

        Guard.checkNull(id, "PermissionCollection.getPermission(String, String)");

        id = this.trimId(containerId, id);
        return this.getPermission(id, PermissionType.None);
    }

    public T getModule(String containerId, String id) {

        Guard.checkNull(id, "PermissionCollection.getModule(String, String)");

        id = this.trimId(containerId, id);
        return this.getPermission(id, PermissionType.Module);
    }

    public T getUserTxn(String containerId, String id) {

        Guard.checkNull(id, "PermissionCollection.getUserTxn(String, String)");

        id = this.trimId(containerId, id);
        return this.getPermission(id, PermissionType.Transaction);
		
		/*
		if(retVal == null)
			throw new SecurityException("User Transaction [%1$s] is not found in the [%2$s] hierarchy", name, permissionId);
		*/
    }

    public T getAction(String containerId, String id) {

        Guard.checkNull(id, "PermissionCollection.getAction(String, String)");

        id = this.trimId(containerId, id);
        return this.getPermission(id, PermissionType.Action);
    }

    public List<T> getPermissions() {

        ArrayList<T> list = new ArrayList<T>(4);

        if (this.childList != null) {
            for (T item : this.childList) {
                list.add(item);
            }
        }

        return list;
    }

    public List<T> getModules() {

        return this.getPermissions(PermissionType.Module);
    }

    public List<T> getUserTxns() {

        return this.getPermissions(PermissionType.Transaction);
    }

    public List<T> getActions() {

        return this.getPermissions(PermissionType.Action);
    }

    public List<T> getPermissions(PermissionType enumValue) {

        ArrayList<T> list = new ArrayList<T>(4);

        if (this.childList != null) {
            for (T item : this.childList) {
                if (item.getType() == enumValue)
                    list.add(item);
            }
        }

        return list;
    }

    T getPermission(String name, PermissionType enumValue) {

        T retVal = null;

        int idx = name.indexOf(PermissionBase.NAME_SEPARATOR);

        if (idx == -1) {
            retVal = this.findPermission(name, enumValue);
        } else {
            String key = name.substring(0, idx);
            String subName = name.substring(idx + 1);
            T parentVal = this.findPermission(key, PermissionType.None);

            if (parentVal != null) {
                retVal = (T) parentVal.getPermission(subName, enumValue);
            }
        }

        return retVal;

    }

    private T findPermission(String name, PermissionType enumValue) {

        T retVal = null;

        if (this.childList != null) {

            for (T item : this.childList) {

                if ((enumValue != PermissionType.None) &&
                        (item.getType() != enumValue))
                    continue;

                if (item.getKey().compareToIgnoreCase(name) == 0) {
                    retVal = item;
                    break;
                }
            }
        }

        return retVal;
    }

    void add(String newId, T item) {

        T temp = this.getPermission(StringHelper.EMPTY, newId);

        if (temp != null)
            temp.copyValues(item);
        else
            this.childList.add(item);
    }

    @SuppressWarnings("unchecked")
    public PermissionCollection<T> clone() {

        PermissionCollection<T> clone = new PermissionCollection<T>(this.childList.size());

        for (T item : this.childList) {
            clone.childList.add((T) item.clone());
        }

        return clone;
    }

    public boolean remove(String key) {

        T temp = this.getPermission(StringHelper.EMPTY, key);

        if (temp != null)
            return this.childList.remove(temp);

        return false;
    }

    public Iterator<T> iterator() {
        return this.childList.iterator();
    }
}