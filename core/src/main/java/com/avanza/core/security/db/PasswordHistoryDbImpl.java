package com.avanza.core.security.db;

import java.io.Serializable;

import com.avanza.core.data.DbObject;

public class PasswordHistoryDbImpl extends DbObject implements Serializable {

    private static final long serialVersionUID = -8321400301448692435L;

    // Fields    
    protected String loginId;
    protected String password;

    // constructors
    public PasswordHistoryDbImpl() {

    }

    public PasswordHistoryDbImpl(String loginId, String password) {

        this.setLoginId(loginId);
        this.setPassword(password);
    }

    // Properties
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginId() {
        return this.loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
}