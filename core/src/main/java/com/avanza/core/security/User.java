package com.avanza.core.security;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.db.GroupRelationBinding;
import com.avanza.core.security.db.UserGroupDbImpl;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;

public abstract class User extends GroupHelper implements Comparable<User> {

    public static final String UNKNOWN_USER = "System";

    public static final int NEW = 0;
    public static final int ACTIVE = 1;
    public static final int INACTIVE = 2;

    // Fields
    protected String loginId;
    protected String fullName;
    protected String firstName;
    protected String lastName;
    protected String middleName;
    protected int isActive;
    protected String emailUrl;
    protected Date whenDeleted;
    protected String preferedCulture;
    protected Date lastLogin;
    protected boolean system;
    protected String mobilePhone;
    protected String defaultLocale;

    protected PermissionBinding permissionTree;

    // Constructors

    /**
     * default constructor
     */
    protected User() {

    }

    // Property accessors
    //public abstract Policy getPolicy();

    //public abstract void setPolicy(Policy policy);

    // This method is used to reset the user password.
    public abstract void resetPassword(String password);

    public String getLoginId() {

        return this.loginId;
    }

    public String getFullName() {

        return this.fullName;
    }

    public void setFullName(String fullName) {

        this.fullName = fullName;
    }

    public String getFirstName() {

        return this.firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    public String getLastName() {

        return this.lastName;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    public String getMiddleName() {

        return this.middleName;
    }

    public void setMiddleName(String middleName) {

        this.middleName = middleName;
    }

    public String getEmailUrl() {

        return this.emailUrl;
    }

    public void setEmailUrl(String emailUrl) {

        this.emailUrl = emailUrl;
    }

    public String getPreferedCulture() {

        return this.preferedCulture;
    }

    public void setPreferedCulture(String preferedCulture) {

        this.preferedCulture = preferedCulture;
    }

    public Date getLastLogin() {

        return this.lastLogin;
    }

    public String getFormatedLastLogin() {
        if (this.lastLogin == null)
            return StringHelper.EMPTY;
        //Date Time should be in Arabic
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class.getName());
        LocaleInfo localeInfo = (LocaleInfo) webContext.getAttribute(SessionKeyLookup.CurrentLocale);
        return new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a", localeInfo.getLocale()).format(this.lastLogin);
    }

    public String getFormatedLastLoginDifference() {
        if (this.lastLogin == null)
            return StringHelper.EMPTY;
        Date currentDate = new Date();
        Date date1 = this.lastLogin;
        Date date2 = currentDate;
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(date1);
        calendar2.setTime(date2);
        long milliseconds1 = calendar1.getTimeInMillis();
        long milliseconds2 = calendar2.getTimeInMillis();
        long diff = milliseconds2 - milliseconds1;
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000);
        long diffHours = diff / (60 * 60 * 1000);
        long diffDays = diff / (24 * 60 * 60 * 1000);

        return (diffHours + ":" + diffMinutes + ":" + diffSeconds);
    }

    public PermissionBinding getPermission(String permissionId) {

        this.fillPermissions();
        return this.permissionTree.getPermission(permissionId);
    }

    public PermissionBinding getModule(String permissionId) {

        this.fillPermissions();
        return this.permissionTree.getModule(permissionId);
    }

    public PermissionBinding getUserTxn(String permissionId) {

        this.fillPermissions();
        return this.permissionTree.getUserTxn(permissionId);
    }

    public PermissionBinding getAction(String permissionId) {

        this.fillPermissions();
        return this.permissionTree.getAction(permissionId);
    }

    public List<PermissionBinding> getModules() {

        this.fillPermissions();
        return this.permissionTree.getModules();
    }

    public boolean hasPermission(String permissionId) {

        this.fillPermissions();
        return this.permissionTree.getPermission(permissionId) != null;
    }

    private void fillPermissions() {

        if (this.permissionTree != null)
            return;

        this.permissionTree = new PermissionBinding(StringHelper.EMPTY);

        for (UserGroup item : this.getUserGroups()) {

            List<PermissionBinding> permList = item.getPermissions();
            fillParentPermissions((UserGroupDbImpl) item);
            if (permList.size() > 0) {
                permList.get(0).mergeBindingsTo(permissionTree);
            }
        }
    }

    private void fillParentPermissions(UserGroupDbImpl userGroup) {
        //iterate to parent groups.
        Iterator<GroupRelationBinding> iter = userGroup.getParentGroupBindings().iterator();

        while (iter.hasNext()) {
            GroupRelationBinding groupRelationBinding = (GroupRelationBinding) iter.next();
            UserGroupDbImpl parentGroup = groupRelationBinding.getParentGrp();
            if (parentGroup != null) {
                List<PermissionBinding> parentGroupPermissions = parentGroup.getPermissions();
                if (parentGroupPermissions.size() > 0) {
                    parentGroupPermissions.get(0).mergeBindingsTo(permissionTree);
                }
            }
        }
    }

    public boolean hasUserGroup(String groupId) {

        boolean retVal = false;
        this.innerGetGroups();

        for (UserGroup group : this.getUserGroups()) {

            if (group.getUserGroupId().compareTo(groupId) == 0) {

                retVal = true;
                break;
            }
        }

        return retVal;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public boolean isActive() {

        return (this.isActive == User.ACTIVE) || (this.isActive == User.NEW);
    }

    public boolean isLoggedIn() {

        return (this.isActive == User.NEW);
    }

    public Date getWhenDeleted() {
        return whenDeleted;
    }

    public void setWhenDeleted(Date whenDeleted) {
        this.whenDeleted = whenDeleted;
    }

    public int compareTo(User arg0) {
        return this.getFullName().compareToIgnoreCase(((User) arg0).getFullName());
    }

    public boolean isSystem() {
        return system;
    }

    public void setSystem(boolean system) {
        this.system = system;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Override
    protected HashSet<UserGroup> fillGroups() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public UserGroup getUserGroup(String userGroupId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User)
            return this.loginId.equals(((User) obj).getLoginId());
        return super.equals(obj);
    }

    public String getDefaultLocale() {
        return defaultLocale;
    }

    public void setDefaultLocale(String defaultLocale) {
        this.defaultLocale = defaultLocale;
    }
}