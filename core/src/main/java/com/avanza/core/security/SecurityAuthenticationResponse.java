/**
 *
 */
package com.avanza.core.security;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shahbaz.ali
 *
 */
public class SecurityAuthenticationResponse {

    ArrayList<String> messages = new ArrayList<String>(0);
    AuthenticationNavigationOutcome navigationCode;

    public List<String> getResponseMessages() {
        return messages;
    }

    public void addResponseMessage(String message) {
        this.messages.add(message);
    }

    public AuthenticationNavigationOutcome getAuthenticationNavigationCode() {
        return navigationCode;
    }

    public void setAuthenticationNavigationCode(
            AuthenticationNavigationOutcome navigationCode) {
        this.navigationCode = navigationCode;
    }

}
