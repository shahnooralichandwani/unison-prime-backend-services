package com.avanza.core.interceptor;

/**
 * Marker interface for making entity auditable
 *
 * @author arsalan.ahmed
 */
public interface Auditable {

    public static final Short create = 0;
    public static final Short update = 1;
    public static final Short delete = 2;

    public Object getAuditEntry(Short revtype);

}
