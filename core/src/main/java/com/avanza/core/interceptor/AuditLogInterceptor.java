package com.avanza.core.interceptor;

import com.avanza.core.security.auditTables.AuditableDTO;
import com.avanza.core.security.auditTables.adaptor.AuditableLogAdaptorFactory;
import com.avanza.core.util.Logger;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author arsalan.ahmed
 */
public class AuditLogInterceptor extends EmptyInterceptor {

    private static final Logger logger = Logger.getLogger(AuditLogInterceptor.class);

    private Set<Auditable> insertsAudit = new HashSet<Auditable>();
    private Set<Auditable> updatesAudit = new HashSet<Auditable>();
    private Set<Auditable> deletesAudit = new HashSet<Auditable>();


    //This method is called when object gets deleted.
    public void onDelete(Object entity,
                         Serializable id,
                         Object[] state,
                         String[] propertyNames,
                         Type[] types) {

        if (entity instanceof Auditable) {
            deletesAudit.add((Auditable) entity);
        }

    }


    // This method is called when object gets updated.
    public boolean onFlushDirty(Object entity,
                                Serializable id,
                                Object[] currentState,
                                Object[] previousState,
                                String[] propertyNames,
                                Type[] types) {

        if (entity instanceof Auditable) {
            updatesAudit.add((Auditable) entity);
            return true;
        }
        return false;
    }


    // This method is called when object gets created.
    public boolean onSave(Object entity,
                          Serializable id,
                          Object[] state,
                          String[] propertyNames,
                          Type[] types) {

        if (entity instanceof Auditable) {
            insertsAudit.add((Auditable) entity);
            return true;
        }

        return false;
    }

    //called before commit into database
    public void preFlush(Iterator iterator) {
        System.out.println("preFlush");
    }


    //called after committed into database
    public void postFlush(Iterator iterator) {

        try {

            //If any auditing exist
            if (!insertsAudit.isEmpty() || !updatesAudit.isEmpty() || !deletesAudit.isEmpty()) {
                AuditableDTO auditTableDTO = new AuditableDTO(insertsAudit, updatesAudit, deletesAudit);
                AuditableLogAdaptorFactory.getAdaptor().addTableAuditLog(auditTableDTO);
            }

        } catch (Exception e) {
            logger.logError("Exception occur", e);
        } finally {
            insertsAudit.clear();
            updatesAudit.clear();
            deletesAudit.clear();
        }
    }


}
