package com.avanza.core.callscript;

import java.util.Date;

import com.avanza.core.data.DbObject;
import com.avanza.core.sessionhistory.SessionHistory;

/**
 * @author kraza
 */
public class CallLog extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 6310742407594514843L;

    private String callLogId;
    private Date startTime;
    private Date endTime;
    private String operatorId;
    private String stationCode;
    private String custRelationNum;
    private SessionHistory sessionHistory;

    public CallLog() {
    }

    public String getCallLogId() {
        return this.callLogId;
    }

    public void setCallLogId(String callLogId) {
        this.callLogId = callLogId;
    }

    public SessionHistory getSessionHistory() {
        return this.sessionHistory;
    }

    public void setSessionHistory(SessionHistory sessionHistory) {
        this.sessionHistory = sessionHistory;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getOperatorId() {
        return this.operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getStationCode() {
        return this.stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getCustRelationNum() {
        return this.custRelationNum;
    }

    public void setCustRelationNum(String custRelationNum) {
        this.custRelationNum = custRelationNum;
    }
}
