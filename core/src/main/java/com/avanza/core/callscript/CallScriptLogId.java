package com.avanza.core.callscript;

import java.io.Serializable;
import java.util.Date;


public class CallScriptLogId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3061072156399038203L;

    private String scriptNodeId;
    private String sessionId;

    public CallScriptLogId() {
    }

    public CallScriptLogId(String scriptNodeId, String sessionId) {
        this.scriptNodeId = scriptNodeId;
        this.sessionId = sessionId;
    }

    public String getScriptNodeId() {
        return scriptNodeId;
    }

    public void setScriptNodeId(String scriptNodeId) {
        this.scriptNodeId = scriptNodeId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public int hashCode() {
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + ((scriptNodeId == null) ? 0 : scriptNodeId.hashCode());
        result = PRIME * result + ((sessionId == null) ? 0 : sessionId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final CallScriptLogId other = (CallScriptLogId) obj;
        if (scriptNodeId == null) {
            if (other.scriptNodeId != null)
                return false;
        } else if (!scriptNodeId.equals(other.scriptNodeId))
            return false;
        if (sessionId == null) {
            if (other.sessionId != null)
                return false;
        } else if (!sessionId.equals(other.sessionId))
            return false;
        return true;
    }
}
