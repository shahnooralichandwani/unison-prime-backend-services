package com.avanza.core.callscript;

import com.avanza.core.data.DbObject;
import com.avanza.core.sessionhistory.InterfaceChannel;
import com.avanza.core.sessionhistory.SessionHistory;
import com.avanza.core.sessionhistory.SessionHistoryDetail;

/**
 * We need to log the call script that are active during the session. so we are loging it to DB.
 *
 * @author kraza
 */
public class CallScriptLog extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = -6962561297311627520L;
    private String Id;
    private String scriptNodeId;
    private String sessionId;
    private Long actionSeqNum;
    private String activityTypeId;
    private String custRelationNum;
    private String refDocNum;
    private String docTypeId;
    private String rootNodeId;
    private String channelId;
    private boolean selected;

    private InterfaceChannel interfaceChannel;
    private SessionHistory sessionHistory;
    private SessionHistoryDetail sessionHistoryDetail;

    public CallScriptLog() {
    }

    public String getCustRelationNum() {
        return this.custRelationNum;
    }

    public void setCustRelationNum(String custRelationNum) {
        this.custRelationNum = custRelationNum;
    }

    public String getRefDocNum() {
        return this.refDocNum;
    }

    public void setRefDocNum(String refDocNum) {
        this.refDocNum = refDocNum;
    }

    public String getDocTypeId() {
        return this.docTypeId;
    }

    public void setDocTypeId(String docTypeId) {
        this.docTypeId = docTypeId;
    }

    public String getRootNodeId() {
        return this.rootNodeId;
    }

    public void setRootNodeId(String rootNodeId) {
        this.rootNodeId = rootNodeId;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getScriptNodeId() {
        return scriptNodeId;
    }

    public void setScriptNodeId(String scriptNodeId) {
        this.scriptNodeId = scriptNodeId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public InterfaceChannel getInterfaceChannel() {
        return this.interfaceChannel;
    }

    public void setInterfaceChannel(InterfaceChannel interfaceChannel) {
        this.interfaceChannel = interfaceChannel;
    }

    public SessionHistory getSessionHistory() {
        return this.sessionHistory;
    }

    public void setSessionHistory(SessionHistory sessionHistory) {
        this.sessionHistory = sessionHistory;
    }

    public String getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public SessionHistoryDetail getSessionHistoryDetail() {
        return sessionHistoryDetail;
    }

    public void setSessionHistoryDetail(SessionHistoryDetail sessionHistoryDetail) {
        this.sessionHistoryDetail = sessionHistoryDetail;
    }

    public Long getActionSeqNum() {
        return actionSeqNum;
    }


    public void setActionSeqNum(Long actionSeqNum) {
        this.actionSeqNum = actionSeqNum;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }
}
