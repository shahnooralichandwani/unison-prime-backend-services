package com.avanza.core.callscript;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TreeObject;

/**
 * @author kraza
 */
public class ScriptNode extends TreeObject implements Comparable<ScriptNode> {

    private static final long serialVersionUID = -8226664330833330034L;

    private String scriptNodeId;
    private String parentNodeId;
    private String rootNodeId;
    private String scriptTextPrm;
    private String scriptTextSec;
    private String selectCriteria;
    private ScriptNode parentNode;
    private int displayOrder;
    private String value;
    private boolean select; // Whether to show the script tree as selected or not the parent flag takes precedence to its childs.
    private boolean active;


    private Set<ScriptNode> scriptNodes = new HashSet<ScriptNode>(0);
    private Set<CallScriptLog> callScriptLogs = new HashSet<CallScriptLog>(0);
    private Set<ScriptNode> scriptRootNodes = new HashSet<ScriptNode>(0);


    public ScriptNode(ScriptNode node) {
        this.scriptNodeId = node.scriptNodeId;
        this.displayOrder = node.displayOrder;
        this.parentNode = node.parentNode;
        this.parentNodeId = node.parentNodeId;
        this.rootNodeId = node.rootNodeId;
        this.scriptNodes = node.scriptNodes;
        this.scriptRootNodes = node.scriptRootNodes;
        this.scriptTextPrm = node.scriptTextPrm;
        this.scriptTextSec = node.scriptTextSec;
        this.select = node.select;
        this.selectCriteria = node.selectCriteria;
        this.value = node.value;

    }

    public ScriptNode() {
        super();
        scriptNodeId = "";
        id = "";
    }

    public ScriptNode(String id) {
        super(id);
    }

    public String getParentNodeId() {
        return parentNodeId;
    }

    public void setParentNodeId(String parentNodeId) {
        this.parentNodeId = parentNodeId;
    }

    public String getScriptNodeId() {
        return scriptNodeId;
    }

    public void setScriptNodeId(String scriptNodeId) {
        this.scriptNodeId = scriptNodeId;
        this.id = scriptNodeId;
    }

    public String getScriptTextPrm() {
        return scriptTextPrm;
    }

    public void setScriptTextPrm(String scriptTextPrm) {
        this.scriptTextPrm = scriptTextPrm;
    }

    public String getScriptTextSec() {
        return scriptTextSec;
    }

    public void setScriptTextSec(String scriptTextSec) {
        this.scriptTextSec = scriptTextSec;
    }

    public String getSelectCriteria() {
        return selectCriteria;
    }

    public void setSelectCriteria(String selectCriteria) {
        this.selectCriteria = selectCriteria;
    }

    public boolean getSelect() {
        return select;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public Set<CallScriptLog> getCallScriptLogs() {
        return callScriptLogs;
    }

    public void setCallScriptLogs(Set<CallScriptLog> callScriptLogs) {
        this.callScriptLogs = callScriptLogs;
    }

    public Set<ScriptNode> getScriptNodes() {
        return scriptNodes;
    }

    public List<ScriptNode> getOrderedScriptNodes() {
        List<ScriptNode> orderedScriptNodes = new ArrayList<ScriptNode>();
        orderedScriptNodes.addAll(scriptNodes);
        Collections.sort(orderedScriptNodes);
        return orderedScriptNodes;
    }

    public void setScriptNodes(Set<ScriptNode> scriptNodes) {
        this.scriptNodes = scriptNodes;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public int compareTo(ScriptNode scriptNode) {

        if (this.getDisplayOrder() > scriptNode.getDisplayOrder())
            return 1;
        else if (this.getDisplayOrder() < scriptNode.getDisplayOrder())
            return -1;

        return 0;
    }

    public String getRootNodeId() {
        return rootNodeId;
    }

    public void setRootNodeId(String rootNodeId) {
        this.rootNodeId = rootNodeId;
    }

    public Set<ScriptNode> getScriptRootNodes() {
        return scriptRootNodes;
    }

    public void setScriptRootNodes(Set<ScriptNode> scriptRootNodes) {
        this.scriptRootNodes = scriptRootNodes;
    }

    public void setRoot(boolean root) {
        if (root) {
            parentNodeId = null;
            rootNodeId = null;
        }
    }

    public boolean getRoot() {
        return isRoot();
    }

    public boolean isRoot() {
        return (parentNodeId == null && rootNodeId == null);
    }

    @Override
    public String getPrimaryName() {
        return scriptTextPrm;
    }

    public void setPrimaryName(String scriptTextPrm) {
        this.scriptTextPrm = scriptTextPrm;
    }

    @Override
    public String getSecondaryName() {
        return scriptTextSec;
    }

    public void setSecondaryName(String scriptTextSec) {
        this.scriptTextSec = scriptTextSec;
    }

    @Override
    public TreeObject clone() {

        ScriptNode scriptNode = new ScriptNode(this.id);
        scriptNode.copyValues(this);

        //Now need to copy their childs also.
        for (TreeObject tobj : this.childList) {
            TreeObject cloned = ((ScriptNode) tobj).clone();
            ((ScriptNode) cloned).parentNode = scriptNode;
            ((ScriptNode) cloned).parentNodeId = scriptNode.id;
            scriptNode.childList.add(cloned);
        }

        return scriptNode;
    }


    public ScriptNode getParentNode() {
        return parentNode;
    }


    public void setParentNode(ScriptNode parentNode) {
        this.parentNode = parentNode;
    }

    @Override
    public void copyValues(DbObject copyFrom) {

        ScriptNode scriptNode = (ScriptNode) copyFrom;

        super.copyValues(copyFrom);
        this.scriptTextPrm = scriptNode.scriptTextPrm;
        this.scriptTextSec = scriptNode.scriptTextSec;
        this.parentNodeId = scriptNode.parentNodeId;
        this.parentNode = scriptNode.parentNode;
    }

    @Override
    protected TreeObject createObject(String id) {

        return new ScriptNode(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        /*if (!super.equals(obj)) return false;*/
        if (getClass() != obj.getClass()) return false;
        final ScriptNode other = (ScriptNode) obj;
        if (displayOrder != other.displayOrder) return false;
        if (parentNode == null) {
            if (other.parentNode != null) return false;
        } else if (!parentNode.equals(other.parentNode)) return false;
        if (parentNodeId == null) {
            if (other.parentNodeId != null) return false;
        } else if (!parentNodeId.equals(other.parentNodeId)) return false;
        if (rootNodeId == null) {
            if (other.rootNodeId != null) return false;
        } else if (!rootNodeId.equals(other.rootNodeId)) return false;
        if (scriptNodeId == null) {
            if (other.scriptNodeId != null) return false;
        } else if (!scriptNodeId.equals(other.scriptNodeId)) return false;
        if (scriptTextPrm == null) {
            if (other.scriptTextPrm != null) return false;
        } else if (!scriptTextPrm.equals(other.scriptTextPrm)) return false;
        if (scriptTextSec == null) {
            if (other.scriptTextSec != null) return false;
        } else if (!scriptTextSec.equals(other.scriptTextSec)) return false;
        if (select != other.select) return false;
        if (selectCriteria == null) {
            if (other.selectCriteria != null) return false;
        } else if (!selectCriteria.equals(other.selectCriteria)) return false;
        if (value == null) {
            if (other.value != null) return false;
        } else if (!value.equals(other.value)) return false;
        return true;
    }

    @Override
    public <T extends TreeObject> void add(T item) {
        super.add(item);
        ((ScriptNode) item).setParentNodeId(this.scriptNodeId);
        ((ScriptNode) item).parentNode = this;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + displayOrder;
        result = prime * result + ((parentNode == null) ? 0 : parentNode.hashCode());
        result = prime * result + ((parentNodeId == null) ? 0 : parentNodeId.hashCode());
        result = prime * result + ((rootNodeId == null) ? 0 : rootNodeId.hashCode());
        result = prime * result + ((scriptNodeId == null) ? 0 : scriptNodeId.hashCode());
        result = prime * result + ((scriptTextPrm == null) ? 0 : scriptTextPrm.hashCode());
        result = prime * result + ((scriptTextSec == null) ? 0 : scriptTextSec.hashCode());
        result = prime * result + (select ? 1231 : 1237);
        result = prime * result + ((selectCriteria == null) ? 0 : selectCriteria.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public String getValue() {
        value = getShortTextPrm();
        return value;
    }

    public void setValue(String value) {

        this.value = value;
    }

    public String getShortTextPrm() {
        if (StringHelper.isEmpty(scriptTextPrm))
            return StringHelper.EMPTY;

        return (scriptTextPrm.length() > 30) ? scriptTextPrm.substring(0, 30) + "..." : scriptTextPrm;
    }

    public void removeScriptNode(ScriptNode node) {
        Set<ScriptNode> tempScSet = new HashSet<ScriptNode>(0);
        for (ScriptNode scriptnode : scriptNodes) {
            if (!node.scriptNodeId.equalsIgnoreCase(scriptnode.scriptNodeId))
                tempScSet.add(scriptnode);
        }
        scriptNodes = tempScSet;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean getActive() {
        return active;
    }

}
