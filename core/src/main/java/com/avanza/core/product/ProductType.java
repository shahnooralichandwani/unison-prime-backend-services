package com.avanza.core.product;

import java.util.LinkedHashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.meta.ValueTreeNode;


public class ProductType extends DbObject implements Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -8550951534847723086L;

    protected String productCode;
    protected String parentCode;
    protected ProductType product;
    protected String rootNodeId;
    protected String systemName;
    protected String productNamePrm;
    protected String productNameSec;
    protected short displayOrder;
    protected String description;
    protected short levelId;
    protected Set<ProductType> products = new LinkedHashSet<ProductType>(0);
    private ValueTreeNode valueTreeNode;

    private boolean selected;

    public ProductType() {
    }

    public ValueTreeNode getValueTreeNode() {
        return valueTreeNode;
    }

    public String getProductCode() {
        return this.productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public ProductType getProduct() {
        return this.product;
    }

    public void setProduct(ProductType product) {
        this.product = product;
    }

    public String getRootNodeId() {
        return this.rootNodeId;
    }

    public void setRootNodeId(String rootNodeId) {
        this.rootNodeId = rootNodeId;
    }

    public String getSystemName() {
        return this.systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getProductNamePrm() {
        return this.productNamePrm;
    }

    public void setProductNamePrm(String productNamePrm) {
        this.productNamePrm = productNamePrm;
    }

    public String getProductNameSec() {
        return this.productNameSec;
    }

    public void setProductNameSec(String productNameSec) {
        this.productNameSec = productNameSec;
    }

    public short getDisplayOrder() {
        return this.displayOrder;
    }

    public void setDisplayOrder(short displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getLevelId() {
        return this.levelId;
    }

    public void setLevelId(short levelId) {
        this.levelId = levelId;
    }

    public Set<ProductType> getProducts() {
        return this.products;
    }

    public void setProducts(Set<ProductType> products) {
        this.products = products;
    }


    public String getParentCode() {
        return parentCode;
    }


    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public void setValueTreeNode(ValueTreeNode valueTreeNode) {
        this.valueTreeNode = valueTreeNode;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        ProductType ptype = new ProductType();
        ptype.setProductCode(productCode);
        ptype.setDescription(description);
        ptype.setDisplayOrder(displayOrder);
        ptype.setLevelId(levelId);
        ptype.setParentCode(parentCode);
        ptype.setProduct(product);
        ptype.setProductNamePrm(productNamePrm);
        ptype.setProductNameSec(productNameSec);
        ptype.setRootNodeId(rootNodeId);
        ptype.setSystemName(systemName);
        return ptype;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
