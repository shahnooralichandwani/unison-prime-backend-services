package com.avanza.core.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.core.CoreInterface;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.meta.MetaCounter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.ValueTreeNode;
import com.avanza.core.sdo.ValueTreeManager;
import com.avanza.core.util.Guard;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TreeObject;
import com.avanza.core.util.configuration.ConfigSection;

public class ProductCatalog implements CoreInterface {

    private static String rootNodeId = "0000000079";
    private static String parentCode = "0000000001";

    public static String getValueTreeNodeRootId() {
        return rootNodeId;
    }

    public static String getParentProductCode() {
        return parentCode;
    }

    private static final Logger logger = Logger.getLogger(ProductCatalog.class);
    private static ProductCatalog _instance = new ProductCatalog();
    private static Map<String, ProductType> productTypes;
    private static Map<String, ValueTreeNode> categoryTree;
    private static List<ProductType> productTypesList;

    private ProductCatalog() {
        categoryTree = new HashMap<String, ValueTreeNode>();
    }

    public static ProductCatalog getInstance() {
        return _instance;
    }

    public ProductType getProducts(String productName) {

        Guard.checkNullOrEmpty(productName, "productName");
        DataBroker broker = DataRepository.getBroker(ProductType.class.getName());

        if (productTypes == null) {
            productTypes = new HashMap<String, ProductType>();
            Search search = new Search(ProductType.class);
            search.addCriterion(Criterion.nullCriterion("parentCode"));
            List<ProductType> productTypeList = broker.find(search);
            for (ProductType ptype : productTypeList) {
                productTypes.put(ptype.getSystemName(), ptype);
            }
        }

        return productTypes.get(productName);
    }

    public static List<ProductType> getProducts() {
        if (productTypesList == null) {
            productTypesList = new ArrayList<ProductType>();
            DataBroker broker = DataRepository.getBroker(ProductType.class.getName());
            Search search = new Search(ProductType.class);
            // search.addCriterion(Criterion.nullCriterion("parentCode"));

            productTypesList = broker.find(search);
        }
        return productTypesList;
    }

    private ValueTreeNode getProductCategoryTree(ProductType productTypes, ValueTreeNode mergedTree) {

        for (ProductType productType : productTypes.getProducts()) {
            ValueTreeNode item = new ValueTreeNode(productType.getRootNodeId() + "."
                    + productType.getProductCode(), "");
            item.setPrimaryName(productType.getProductNamePrm());
            item.setSecondaryName(productType.getProductNameSec());
            item.setValue(productType.getProductCode());
            mergedTree.add(item);

            if (!productType.getProducts().isEmpty()) {
                mergedTree = getProductCategoryTree(productType, mergedTree);
            }
        }

        return mergedTree;
    }

    private String getCombinedProdCode(ProductType productType) {
        if (productType.getParentCode() != null)
            return getCombinedProdCode(productType.getProduct()) + "."
                    + productType.getProductCode();
        return productType.getProductCode();
    }

    public ValueTreeNode getProductCategoryTree(String productName) {

        Guard.checkNullOrEmpty(productName, "productName");

        ValueTreeNode retTree = categoryTree.get(productName);

        if (retTree == null) {
            ProductType ptype = this.getProducts(productName);
            retTree = getProductCategoryTree(ptype, ValueTreeManager.getTree(ptype.getRootNodeId()));
            categoryTree.put(productName, retTree);
        }

        return retTree;
    }

    public String getSelectedTreeValue(String productName, String productCode, boolean isPrimary) {

        ValueTreeNode treeNode = this.getProductCategoryTree(productName);

        return getSelectedValue(productCode, isPrimary, treeNode, true);
    }

    private String getSelectedValue(String productCode, boolean isPrimary, ValueTreeNode treeNode,
                                    boolean returnValue) {

        if (treeNode.getChildCount() == 0 && treeNode.getKey().equalsIgnoreCase(productCode)) {
            if (returnValue)
                return isPrimary ? treeNode.getPrimaryName() : treeNode.getSecondaryName();
            else
                return treeNode.getId();
        }

        for (TreeObject tobj : treeNode.getChildList()) {
            String retVal = getSelectedValue(productCode, isPrimary, (ValueTreeNode) tobj,
                    returnValue);
            if (StringHelper.isNotEmpty(retVal)) {
                return isPrimary
                        ? treeNode.getPrimaryName() + " - " + retVal
                        : treeNode.getSecondaryName() + " - " + retVal;
            }
        }

        return StringHelper.EMPTY;
    }

    public String getSelectedTreePath(String productName, String productCode, boolean isPrimary) {

        ValueTreeNode treeNode = this.getProductCategoryTree(productName);

        return getSelectedValue(productCode, isPrimary, treeNode, false);
    }

    public void persist(ProductType productType) {

        DataBroker broker = DataRepository.getBroker(ProductType.class.getName());
        if (StringHelper.isEmpty(productType.getProductCode())) {
            productType.setProductCode((String) MetaDataRegistry.getNextCounterValue(MetaCounter.PRODUCT_COUNTER));
            ValueTreeNode vtn = broker.findById(ValueTreeNode.class, productType.getRootNodeId());
            productType.setSystemName(vtn.getPrimaryName().replaceAll(" ", "") + "."
                    + productType.getProductNamePrm().replaceAll(" ", ""));
            broker.add(productType);
        } else {
            ProductType pt = broker.findById(ProductType.class, productType.getProductCode());
            pt.setDisplayOrder(productType.getDisplayOrder());
            pt.setProductNamePrm(productType.getProductNamePrm());
            pt.setProductNameSec(productType.getProductNameSec());
            pt.setDescription(productType.getDescription());
            pt.setRootNodeId(productType.getRootNodeId());

            ValueTreeNode vtn = broker.findById(ValueTreeNode.class, productType.getRootNodeId());
            productType.setSystemName(vtn.getPrimaryName().replaceAll(" ", "") + "."
                    + productType.getProductNamePrm().replaceAll(" ", ""));

            broker.update(pt);
        }
        this.productTypesList = null;
        this.categoryTree = new HashMap<String, ValueTreeNode>();
        this.productTypes = null;
    }

    public void delete(ProductType ptype) {
        DataBroker broker = DataRepository.getBroker(ProductType.class.getName());
        ProductType pt = broker.findById(ProductType.class, ptype.getParentCode());
        broker.delete(pt);

        for (ProductType ptitem : productTypesList) {
            if (pt.getProductCode().equalsIgnoreCase(ptitem.getProductCode()))
                productTypesList.remove(ptitem);
        }
    }

    public void load(ConfigSection section) {
        if (section != null) {
            section = section.getChildSections("Product-Catalog").get(0);
            if (section != null) {
                rootNodeId = section.getValue("root-node-id", rootNodeId);
                parentCode = section.getValue("parent-code", parentCode);
            }
        }
    }

    public void dispose() {
    }

    public ProductType getProductByCode(String productCode) {

        Guard.checkNullOrEmpty(productCode, "productCode");
        DataBroker broker = DataRepository.getBroker(ProductType.class.getName());
        Search search = new Search(ProductType.class);
        search.addCriterion(Criterion.equal("productCode", productCode));
        List<ProductType> productTypeList = broker.find(search);
        if (productTypeList != null && productTypeList.size() > 0) {
            return productTypeList.get(0);
        }
        return null;
    }
}
