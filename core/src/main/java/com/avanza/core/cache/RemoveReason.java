package com.avanza.core.cache;


public enum RemoveReason {

    Removed("Removed"), Expired("Expired"), Underused("Underused");

    private String value;

    RemoveReason(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }
}
