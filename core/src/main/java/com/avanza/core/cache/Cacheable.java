package com.avanza.core.cache;

import java.util.Date;


public interface Cacheable {

    long CACHE_TIME_MULTIPLIER = 1000 * 60; // minutes

    String getKey();

    boolean isExpired();

    long getCacheTime();

    Date getCacheAbsoluteTime();

    void onCachedItemRemoved(String key, Object value, RemoveReason reason);

    boolean isCacheable();
}
