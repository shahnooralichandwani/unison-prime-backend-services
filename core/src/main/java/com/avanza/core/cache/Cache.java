package com.avanza.core.cache;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Cache implements Serializable, Iterable<Cacheable> {

    /**
     *
     */
    private static final long serialVersionUID = -9211690652539714807L;

    private Map<String, Cacheable> cacheMap;

    public Cache() {
        cacheMap = new HashMap<String, Cacheable>();
    }

    @SuppressWarnings("unchecked")
    public Cache(Map cacheMap) {
        this.cacheMap = cacheMap;
    }

    public Cache(int size) {
        cacheMap = new HashMap<String, Cacheable>(size);
    }

    public int getCount() {
        return this.cacheMap.size();
    }

    public void add(String key, Cacheable cacheObject) {
        this.cacheMap.put(key, cacheObject);
    }

    public Object remove(String key) {
        return this.cacheMap.remove(key);
    }

    public Object get(String key) {
        return this.cacheMap.get(key);
    }

    public Iterator<Cacheable> iterator() {
        return this.cacheMap.values().iterator();
    }

    //TODO Decide whether to iterate and call the event of removal.
    public void clear() {
        this.cacheMap.clear();
    }
}
