package com.avanza.core.cache;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.constants.ThreadContextLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TimeLogger;
import com.avanza.core.web.config.WebContext;

import java.util.List;


public class CustomerSessionCacheUtil {

    private static Logger logger = Logger.getLogger(CustomerSessionCacheUtil.class);

    public static void updateCache(List<Cacheable> objects) {
        TimeLogger timer = TimeLogger.init();

        String dbErrorMessage = ApplicationContext.getContext().get(ThreadContextLookup.DbErrorMesssage.toString());
        if (StringHelper.isNotEmpty(dbErrorMessage))
            return;

        timer.begin("CustomerSessionCache.updateCache()");
        try {
            WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
            if (webContext != null) {
                Cache customerCache = webContext.getAttribute(SessionKeyLookup.UserCustomerCache);
                if (customerCache == null) {
                    logger.logDebug("Customer Cache is found null while updateCache(List).");
                    return;
                }
                for (Object obj : objects) {
                    if (obj instanceof Cacheable) {
                        Cacheable cacheable = (Cacheable) obj;
                        if (cacheable.isCacheable()) {
                            customerCache.add(cacheable.getKey(), cacheable);
                        }
                    } else {
                        //assuming all items are of same type.
                        return;
                    }
                }
            }
        } catch (Throwable t) {
            logger.LogException("Error while updateCache(List)", t);
        } finally {
            timer.end();
        }
    }

    public static void updateCache(Cacheable object) {
        TimeLogger timer = TimeLogger.init();
        timer.begin("CustomerSessionCache.updateCache()");

        String dbErrorMessage = ApplicationContext.getContext().get(ThreadContextLookup.DbErrorMesssage.toString());
        if (StringHelper.isNotEmpty(dbErrorMessage))
            return;

        try {
            WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
            if (webContext == null)
                return;
            Cache customerCache = webContext.getAttribute(SessionKeyLookup.UserCustomerCache);
            if (customerCache == null) {
                logger.logDebug("Customer Cache is found null while updateCache(Cacheable).");
                return;
            }
            if (object.isCacheable()) {
                customerCache.add(object.getKey(), object);
            }
        } catch (Throwable t) {
            logger.LogException("Error while updateCache(Cacheable)", t);
        } finally {
            timer.end();
        }
    }

    public static DataObject cacheLookUp(String key) {

        TimeLogger timer = TimeLogger.init();
        timer.begin("CustomerSessionCache.cacheLookUp(key)");
        try {
            WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
            Cache customerCache = webContext.getAttribute(SessionKeyLookup.UserCustomerCache);
            if (customerCache == null) {
                logger.logDebug("Customer Cache is found null while cacheLookUp().");
                return null;
            }

            DataObject cachedObj = (DataObject) customerCache.get(key);
            if (cachedObj != null && !cachedObj.isExpired()) {
                return cachedObj;
            } else {
                if (cachedObj != null) {
                    customerCache.remove(key);
                    cachedObj.onCachedItemRemoved(key, customerCache, RemoveReason.Expired);
                }
            }
        } catch (Throwable t) {
            logger.LogException("Error while cacheLookUp(key)", t);
        } finally {
            timer.end();
        }
        return null;
    }

    public static void createCacheSession() {

        clearCacheSession();

        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        webContext.setAttribute(SessionKeyLookup.UserCustomerCache, new Cache());
    }

    public static void clearCacheSession() {

        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        Cache cacheSession = webContext.removeAttribute(SessionKeyLookup.UserCustomerCache);
        if (cacheSession != null)
            cacheSession.clear();
    }
}
