package com.avanza.core.hbm;

import com.avanza.core.meta.MetaAttribute;

public class HbmIdElement extends HbmMetaFactory {

    public static String getId(MetaAttribute attrib) {
        String id = String.format("<id name=\"%1$s\" type=\"%2$s\" column=\"%3$s\"/>", attrib.getSystemName(),
                HbmMetaFactory.getHibernateType(attrib.getType()), attrib.getTableColumn());
        return id;
    }
}