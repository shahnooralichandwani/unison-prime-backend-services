package com.avanza.core.hbm;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.MetaException;
import com.avanza.core.meta.MetaRelationship;

public class HbmMetaGenerator {

    public static String createMapping(MetaEntity entity) {
        HbmMetaContext context = null;
        try {

            Map<String, MetaAttribute> Attributes = entity.getAttributeList();
            Iterator<String> itr = Attributes.keySet().iterator();
            context = new HbmMetaContext(entity);
            String idElem = new String();
            String discrimElem = new String();
            boolean hasInternalKey = entity.hasInternalPrimary();
            while (itr.hasNext()) {
                MetaAttribute attrib = (MetaAttribute) Attributes.get(itr.next());

                if (attrib.isInternalPrimary())
                    idElem = HbmMetaFactory.getId(attrib);
                else if (attrib.getIsPrimary() && !hasInternalKey)
                    idElem = HbmMetaFactory.getId(attrib);
                else {
                    if (attrib.getTableColumn() != null && attrib.getTableColumn().length() > 0)
                        HbmMetaFactory.writeElement(attrib, context);
                }
            }
            // }
            for (MetaRelationship rel : entity.getRelationList().values()) {

                HbmMetaFactory.writeAssociationMapping(rel, entity, context);
            }

            for (MetaRelationship rel : entity.getAssociationRelations().values()) {

                HbmMetaFactory.writeAssociationMapping(rel, entity, context);
            }

            for (MetaRelationship rel : entity.getSubEntityRelations().values()) {

                HbmMetaFactory.writeAssociationMapping(rel, entity, context);
            }

            Set<MetaEntity> subEntities = entity.getChildList();
            for (MetaEntity ent : subEntities) {
                HbmMetaFactory.writeSubClass(ent, context);
            }
            if (subEntities.size() > 0) {
                discrimElem = HbmMetaFactory.getDiscriminatorElement(entity.getDiscriminatorCol());
            }

            context.finalize(idElem, discrimElem);

        } catch (Exception e) {

            throw new MetaException(e, "Error occured while generating hbm mapping");
        }

        return context.getXMLString();
    }
}
