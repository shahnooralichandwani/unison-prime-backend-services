package com.avanza.core.hbm;

import java.io.StringWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.MetaException;

public class HbmMetaContext {

    private MetaEntity entity = null;

    private static XMLOutputFactory xmlout = null;

    private static StringWriter stringWriter = null;

    private static XMLStreamWriter xmlwriter = null;

    private static Result result = null;

    private static String XMLHeader = "<?xml version=\"1.0\"?>";

    private static String HibernateDOCtype = "<!DOCTYPE hibernate-mapping PUBLIC \"-//Hibernate/Hibernate Mapping DTD 3.0//EN\" \"http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd\">";

    private String xmlString = "";

    public HbmMetaContext(MetaEntity entity) throws MetaException {
        this.entity = entity;
        xmlout = XMLOutputFactory.newInstance();
        try {
            stringWriter = new StringWriter();
            result = new StreamResult(stringWriter);
            xmlwriter = xmlout.createXMLStreamWriter(result);
        } catch (XMLStreamException e) {
            e.printStackTrace();
            throw new MetaException(e, "Error creating xml stream for hbm mapping");
        }
    }

    public static XMLStreamWriter getXmlwriter() {

        return xmlwriter;
    }

    public static StringWriter getStringWriter() {
        return stringWriter;
    }

    public static void setStringWriter(StringWriter stringWriter) {
        HbmMetaContext.stringWriter = stringWriter;
    }

    public void finalize(String id, String discrimElem) throws MetaException {
        try {
            xmlwriter.close();
            xmlString = stringWriter.getBuffer().toString();
            xmlString = XMLHeader + HibernateDOCtype + getRootStart() + getClassStart(entity.getSystemName(), entity.getTableName()) + id + discrimElem
                    + xmlString + getClassEnd() + getRootEnd();
        } catch (XMLStreamException e) {
            throw new MetaException(e, "Error finalizing xml stream for hbm mapping");
        }

    }

    public String getXMLString() {

        return xmlString;
    }

    private String getRootStart() {
        return "<hibernate-mapping>";
    }

    private String getRootEnd() {
        return "</hibernate-mapping>";
    }

    private String getClassStart(String name, String table) {
        return String.format("<class entity-name=\"%1$s\" table=\"%2$s\">", new Object[]{name, table});
    }

    private String getClassEnd() {
        return "</class>";
    }
}
