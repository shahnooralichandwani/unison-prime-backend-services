package com.avanza.core.hbm;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaException;

public class HbmPropertyElement extends HbmMetaFactory {

    public static void writeElement(MetaAttribute attrib, HbmMetaContext context) {

        try {
            XMLStreamWriter write = context.getXmlwriter();
            write.writeStartElement("property");
            write.writeAttribute("name", attrib.getSystemName());
            write.writeAttribute("column", attrib.getTableColumn());
            write.writeAttribute("type", HbmMetaFactory.getHibernateType(attrib.getType()));
            write.writeEndElement();
        } catch (XMLStreamException e) {
            throw new MetaException("Error occured while writing property element for hmbm mapping");
        }
    }
}
