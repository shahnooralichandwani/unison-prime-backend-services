package com.avanza.core.hbm;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.MetaException;
import com.avanza.core.meta.MetaRelationship;

public class HbmSubclassElement extends HbmMetaFactory {

    public static void writeElement(MetaEntity entity, HbmMetaContext context) {

        try {
            XMLStreamWriter write = context.getXmlwriter();
            write.writeStartElement("subclass");
            write.writeAttribute("entity-name", entity.getSystemName());
            write.writeAttribute("discriminator-value", entity.getDiscriminatorValue());
            write.writeAttribute("node", entity.getSystemName());
            write.writeAttribute("lazy", "false");
            write.writeAttribute("extends", entity.getParent().getSystemName());
            write.writeAttribute("dynamic-insert", "true");
            write.writeAttribute("dynamic-update", "true");
            for (MetaAttribute attrib : entity.getAttributeList(false).values()) {
                if (attrib.getTableColumn() != null && attrib.getTableColumn().length() > 0)
                    HbmMetaFactory.writeElement(attrib, context);
            }
            for (MetaRelationship rel : entity.getRelationList().values()) {

                HbmMetaFactory.writeAssociationMapping(rel, entity, context);
            }

            for (MetaRelationship rel : entity.getAssociationRelations().values()) {

                HbmMetaFactory.writeAssociationMapping(rel, entity, context);
            }

            for (MetaRelationship rel : entity.getSubEntityRelations().values()) {

                HbmMetaFactory.writeAssociationMapping(rel, entity, context);
            }
            write.writeEndElement();
        } catch (XMLStreamException e) {
            throw new MetaException("Error occured while writing property element for hmbm mapping");
        }
    }
}
