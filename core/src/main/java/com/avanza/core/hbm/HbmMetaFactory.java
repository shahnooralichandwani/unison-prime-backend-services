package com.avanza.core.hbm;

import com.avanza.core.meta.AttributeType;
import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.MetaException;
import com.avanza.core.meta.MetaRelationship;

public abstract class HbmMetaFactory {

    public static void writeElement(MetaAttribute attrib, HbmMetaContext context) {

        HbmPropertyElement.writeElement(attrib, context);
    }

    public static void writeSubClass(MetaEntity entity, HbmMetaContext context) {
        HbmSubclassElement.writeElement(entity, context);
    }

    public static void writeAssociationMapping(MetaRelationship rel, MetaEntity mainEntity, HbmMetaContext context) {
        HbmAssociationElement.writeElement(rel, mainEntity, context);
    }

    public static String getHibernateType(AttributeType type) {

        String retVal = null;

        switch (type) {

            case Boolean:
                retVal = "boolean";
                break;

            case Integer:
                retVal = "integer";
                break;

            case Long:
                retVal = "long";
                break;

            case Float:
                retVal = "float";
                break;

            case Double:
                retVal = "double";
                break;

            case Date:
            case DateTime:
                retVal = "timestamp";
                break;

            case String:
            case LongString:
            case PickList:
            case Sequence:
                retVal = "string";
                break;
            case Attachment:
                retVal = "string";
                break;

            default:
                throw new MetaException("No Hibernate data type defined for [%1$s].", type.toString());
        }

        return retVal;
    }

    public static String getId(MetaAttribute attrib) {
        return HbmIdElement.getId(attrib);
    }

    public static String getDiscriminatorElement(String column) {
        return HbmDiscriminatorElement.getElement(column);
    }


}