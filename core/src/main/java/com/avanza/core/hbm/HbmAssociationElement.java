package com.avanza.core.hbm;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.MetaException;
import com.avanza.core.meta.MetaRelationType;
import com.avanza.core.meta.MetaRelationship;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;

public class HbmAssociationElement extends HbmMetaFactory {

    private static final Logger logger = Logger.getLogger(HbmAssociationElement.class);

    public static void writeElement(MetaRelationship rel, MetaEntity mainEntity, HbmMetaContext context) {

        try {
            XMLStreamWriter write = context.getXmlwriter();

            MetaEntity relMainEntity = MetaDataRegistry.getMetaEntity(rel.getMainEntityId());
            MetaEntity relSubEntity = MetaDataRegistry.getMetaEntity(rel.getSubEntityId());
            DataBroker dataBrokerMain = DataRepository.getBroker(relMainEntity.getSystemName());
            DataBroker dataBrokerSub = DataRepository.getBroker(relSubEntity.getSystemName());

            if (rel.getRelationType().equalsIgnoreCase(MetaRelationType.Direct.name())) {

                if (!dataBrokerMain.getDataSource().equals(dataBrokerSub.getDataSource())) {
                    logger.logDebug("No-Mapping created for Direct Relationship %1$s b/w MainEntity: %2$s, SubEntity: %3$s", rel.getRelationshipId(), rel.getMainEntityId(), rel.getSubEntityId());
                    return;
                }

                logger.logDebug("Mapping created for Direct Relationship %1$s b/w MainEntity: %2$s, SubEntity: %3$s", rel.getRelationshipId(), rel.getMainEntityId(), rel.getSubEntityId());

                //Handling the relations from the Parent table to the Child table [Like customer to credit-cards]. 
                if (mainEntity.getId().equals(rel.getMainEntityId())) {
                    write.writeStartElement("bag");
                    write.writeAttribute("name", "_" + rel.getRelationshipId());
                    write.writeAttribute("lazy", "true");
                    write.writeAttribute("inverse", "true");

                    write.writeStartElement("key");
                    write.writeAttribute("column", rel.getSubEntityKey());
                    write.writeEndElement();

                    write.writeStartElement("one-to-many");
                    write.writeAttribute("entity-name", MetaDataRegistry.getMetaEntity(rel.getSubEntityId()).getSystemName());
                    write.writeEndElement();

                    write.writeEndElement();
                }

            } else if (rel.getRelationType().equalsIgnoreCase(MetaRelationType.Indirect.name())) {

                if (!dataBrokerMain.getDataSource().equals(dataBrokerSub.getDataSource())) {
                    logger.logDebug("No-Mapping created for InDirect Relationship %1$s b/w MainEntity: %2$s, SubEntity: %3$s", rel.getRelationshipId(), rel.getMainEntityId(), rel.getSubEntityId());
                    return;
                }

                logger.logDebug("Mapping created for Indirect Relationship %1$s b/w MainEntity: %2$s, SubEntity: %3$s", rel.getRelationshipId(), rel.getMainEntityId(), rel.getSubEntityId());

                //Handling the relations from the Child table to the Parent table [Like customer to branch]. 
                if (mainEntity.getId().equals(rel.getMainEntityId())) {
                    write.writeStartElement("many-to-one");
                    write.writeAttribute("name", rel.getRelationshipId());
                    write.writeAttribute("entity-name", MetaDataRegistry.getMetaEntity(rel.getSubEntityId()).getSystemName());
                    write.writeAttribute("insert", "false");
                    write.writeAttribute("update", "false");
                    write.writeAttribute("column", rel.getMainEntityKey());
                    write.writeAttribute("not-null", "true");
                    write.writeEndElement();
                }
            } else if (rel.getRelationType().equalsIgnoreCase(MetaRelationType.Complex.name())) {

                if (dataBrokerMain.getDataSource().equals(dataBrokerSub.getDataSource())) {
                    logger.logDebug("Mapping created for MultiComplex Relationship %1$s b/w MainEntity: %2$s, SubEntity: %3$s", rel.getRelationshipId(), rel.getMainEntityId(), rel.getSubEntityId());

                    //Handling the relations from the Parent table to the Association to Child table [Like customer to Customer_Accounts to Accounts]. 
                    if (mainEntity.getId().equals(rel.getMainEntityId())) {
                        write.writeStartElement("bag");
                        write.writeAttribute("name", rel.getRelationshipId());
                        write.writeAttribute("table", MetaDataRegistry.getMetaEntity(rel.getRelatedEntityId()).getTableName());
                        write.writeAttribute("lazy", "true");
                        if (mainEntity.getId().equalsIgnoreCase(rel.getMainEntityId()))
                            write.writeAttribute("inverse", "true");

                        write.writeStartElement("key");
                        write.writeAttribute("column", rel.getMainEntityKey());
                        write.writeEndElement();

                        write.writeStartElement("many-to-many");
                        write.writeAttribute("entity-name", MetaDataRegistry.getMetaEntity(rel.getSubEntityId()).getSystemName());
                        write.writeAttribute("column", rel.getSubEntityKey());
                        write.writeEndElement();

                        write.writeEndElement();
                    } else if (mainEntity.getId().equals(rel.getSubEntityId())) {
                        write.writeStartElement("bag");
                        write.writeAttribute("name", rel.getRelationshipId());
                        write.writeAttribute("table", MetaDataRegistry.getMetaEntity(rel.getRelatedEntityId()).getTableName());
                        write.writeAttribute("lazy", "true");

                        write.writeStartElement("key");
                        write.writeAttribute("column", rel.getSubEntityKey());
                        write.writeEndElement();

                        write.writeStartElement("many-to-many");
                        write.writeAttribute("entity-name", MetaDataRegistry.getMetaEntity(rel.getMainEntityId()).getSystemName());
                        write.writeAttribute("column", rel.getMainEntityKey());
                        write.writeEndElement();

                        write.writeEndElement();
                    }
                } else {
                    logger.logDebug("Mapping created for Complex Relationship %1$s b/w MainEntity: %2$s, SubEntity: %3$s", rel.getRelationshipId(), rel.getMainEntityId(), rel.getSubEntityId());

                    //Handling the relations from the association table to the sub table. 
                    if (mainEntity.getId().equals(rel.getRelatedEntityId())) {
                        if (StringHelper.isEmpty(rel.getRelSubEntityKey()) || StringHelper.isEmpty(rel.getSubEntityId()))
                            logger.logError("Either of rel.getRelSubEntityKey() or rel.getSubEntityId() is empty while creating association relation for RelationShipId: %1$s", rel.getRelationshipId());
                        write.writeStartElement("many-to-one");
                        write.writeAttribute("name", rel.getRelationshipId());
                        write.writeAttribute("entity-name", MetaDataRegistry.getMetaEntity(rel.getSubEntityId()).getSystemName());
                        write.writeAttribute("insert", "false");
                        write.writeAttribute("update", "false");
                        write.writeAttribute("column", rel.getRelSubEntityKey());
                        write.writeAttribute("not-null", "true");
                        write.writeEndElement();
                    } else if (mainEntity.getId().equals(rel.getSubEntityId())) {
                        //Handling the relations from the sub table to the association table.
                        if (StringHelper.isEmpty(rel.getRelSubEntityKey()) || StringHelper.isEmpty(rel.getRelatedEntityId()))
                            logger.logError("Either of rel.getRelSubEntityKey() or rel.getRelatedEntityId() is empty while creating association relation for RelationShipId: %1$s", rel.getRelationshipId());

                        write.writeStartElement("bag");
                        write.writeAttribute("name", rel.getRelationshipId());
                        write.writeAttribute("lazy", "true");
                        write.writeAttribute("inverse", "true");

                        write.writeStartElement("key");
                        write.writeAttribute("column", rel.getRelSubEntityKey());
                        write.writeEndElement();

                        write.writeStartElement("one-to-many");
                        write.writeAttribute("entity-name", MetaDataRegistry.getMetaEntity(rel.getRelatedEntityId()).getSystemName());
                        write.writeEndElement();

                        write.writeEndElement();
                    }
                }
            }
        } catch (XMLStreamException e) {
            throw new MetaException(
                    "Error occured while writing mapping element for hbm mapping");
        }
    }
}
