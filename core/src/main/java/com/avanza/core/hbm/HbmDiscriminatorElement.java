package com.avanza.core.hbm;


public class HbmDiscriminatorElement extends HbmMetaFactory {

    public static String getElement(String column) {
        String elem = String.format("<discriminator column=\"%1$s\" insert=\"false\"></discriminator>", column);
        return elem;
    }
}