package com.avanza.core.article;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.product.ProductType;

public class ArticleFavourite extends DbObject implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8550951534847723086L;
    private String artFavId;
    private String articleId;
    private String loginId;

    public ArticleFavourite() {
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getArtFavId() {
        return artFavId;
    }

    public void setArtFavId(String artFavId) {
        this.artFavId = artFavId;
    }


}
