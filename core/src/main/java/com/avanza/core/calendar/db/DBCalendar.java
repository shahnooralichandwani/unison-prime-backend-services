package com.avanza.core.calendar.db;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.avanza.core.calendar.WorkType;
import com.avanza.core.data.DbObject;


public class DBCalendar extends DbObject {

    private String calendarId;

    private String calendarNamePRM;

    private String calendarNameSEC;

    private String fiscalYearStart;

    private String weekStart;

    private Set<DBCalendarInfo> calendarInfos = new HashSet<DBCalendarInfo>(0);


    public Set<DBCalendarInfo> getCalendarInfos() {
        return calendarInfos;
    }


    public void setCalendarInfos(Set<DBCalendarInfo> calendarInfos) {
        this.calendarInfos = calendarInfos;
    }


    public String getCalendarId() {
        return calendarId;
    }


    public void setCalendarId(String calendarId) {
        this.calendarId = calendarId;
    }


    public String getCalendarNamePRM() {
        return calendarNamePRM;
    }


    public void setCalendarNamePRM(String calendarNamePRM) {
        this.calendarNamePRM = calendarNamePRM;
    }


    public String getCalendarNameSEC() {
        return calendarNameSEC;
    }


    public void setCalendarNameSEC(String calendarNameSEC) {
        this.calendarNameSEC = calendarNameSEC;
    }


    public String getFiscalYearStart() {
        return fiscalYearStart;
    }


    public void setFiscalYearStart(String fiscalYearStart) {
        this.fiscalYearStart = fiscalYearStart;
    }


    public String getWeekStart() {
        return weekStart;
    }


    public void setWeekStart(String weekStart) {
        this.weekStart = weekStart;
    }

    public List<DBCalendarInfo> getDBCalendarInfoByType(WorkType type) {
        List<DBCalendarInfo> infoList = new ArrayList<DBCalendarInfo>(0);
        for (DBCalendarInfo info : calendarInfos) {
            if (WorkType.fromName(info.getWorkTypeId()) == type) {
                infoList.add(info);
            }
        }
        return infoList;
    }
}
