package com.avanza.core.calendar.db;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;


public class DBCalendarInfo extends DbObject {

    private String calendarInfoId;

    private String calendarId;

    private String workTypeId;

    private int year;

    private Date effectiveFrom;

    private Date effectiveTo;

    private boolean inverse;

    private String dayOfWeek;

    private Set<DBCalendarInfoDetail> calendarInfoDetails = new HashSet<DBCalendarInfoDetail>(0);


    public Set<DBCalendarInfoDetail> getCalendarInfoDetails() {
        return calendarInfoDetails;
    }


    public void setCalendarInfoDetails(Set<DBCalendarInfoDetail> calendarInfoDetails) {
        this.calendarInfoDetails = calendarInfoDetails;
    }


    public String getCalendarInfoId() {
        return calendarInfoId;
    }


    public void setCalendarInfoId(String calendarInfoId) {
        this.calendarInfoId = calendarInfoId;
    }


    public String getCalendarId() {
        return calendarId;
    }


    public void setCalendarId(String calendarId) {
        this.calendarId = calendarId;
    }


    public String getWorkTypeId() {
        return workTypeId;
    }


    public void setWorkTypeId(String workTypeId) {
        this.workTypeId = workTypeId;
    }


    public int getYear() {
        return year;
    }


    public void setYear(int year) {
        this.year = year;
    }


    public Date getEffectiveFrom() {
        return effectiveFrom;
    }


    public void setEffectiveFrom(Date effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }


    public Date getEffectiveTo() {
        return effectiveTo;
    }


    public void setEffectiveTo(Date effectiveTo) {
        this.effectiveTo = effectiveTo;
    }


    public boolean getInverse() {
        return inverse;
    }


    public void setInverse(boolean inverse) {
        this.inverse = inverse;
    }


    public String getDayOfWeek() {
        return dayOfWeek;
    }


    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getEffectiveFromString() {
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        return format.format(effectiveFrom);
    }

    public void setEffectiveFromString(String date) {

    }

    public String getEffectiveToString() {
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        return format.format(effectiveTo);
    }

    public void setEffectiveToString(String date) {

    }

}
