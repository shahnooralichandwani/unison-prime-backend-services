package com.avanza.core.calendar.task;

import com.avanza.component.calendar.Task;
import com.avanza.component.calendar.TaskUser;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.scheduler.JobImpl;
import com.avanza.notificationservice.adapter.UnisonAlertsProtocol;
import com.avanza.notificationservice.notification.NotificationSender;

public class TaskJob extends JobImpl {


    @Override
    public void executeInner() {

        String taskId = getId();
        DataBroker broker = DataRepository.getBroker(Task.class.getName());
        Task task = broker.findById(Task.class, taskId);

        try {
            for (TaskUser taskUser : task.getTaskUsers()) {

                if (task.getIsNotfAlert())
                    NotificationSender.sendAlert(UnisonAlertsProtocol.ALERTS_CHANNEL, "Followup Alert!", task.getDescription(), taskUser.getLoginId(), task.getDueDate());

                if (task.getIsNotfEmail())
                    NotificationSender.sendEmail("Followup Email", task.getDescription(), taskUser.getLoginId(), null, InboundLoader.getInstance().getAckChannel("Email"));

                if (task.getIsNotfSms())
                    NotificationSender.sendSMS(taskUser.getUser().getMobilePhone(), task.getDescription().toString(), InboundLoader.getInstance().getAckChannel("SMS"), "Followup Sms!");
            }
        } catch (RuntimeException e) {

            e.printStackTrace();
        }

        System.out.println("Task Job Event occured for " + taskId);


    }
}
