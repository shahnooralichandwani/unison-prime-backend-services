package com.avanza.core.calendar.task;

import java.util.ArrayList;
import java.util.List;

import com.avanza.component.calendar.NotificationStrategy;
import com.avanza.component.calendar.Task;
import com.avanza.component.calendar.TaskPriority;
import com.avanza.component.calendar.TaskStatus;
import com.avanza.component.calendar.TaskType;
import com.avanza.component.calendar.TaskUser;
import com.avanza.core.CoreException;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.data.sequence.SequenceManager;
import com.avanza.core.security.User;

public class TaskManager {

    private static TaskManager _instance = new TaskManager();
    public static final String TASK_KEY_GENERATOR = "TaskKeyGenerator";
    private List<NotificationStrategy> notificationStrategyList = new ArrayList<NotificationStrategy>(
            0);
    private List<TaskPriority> taskPriorityList = new ArrayList<TaskPriority>(0);
    private List<TaskStatus> taskStatusList = new ArrayList<TaskStatus>(0);

    private TaskManager() {

    }

    public static TaskManager getInstance() {
        return _instance;
    }

    /*
     * Shahbaz: Campaign&Outbound
     * Add Task for the current user from Campaign Call back Session
     */
    public Task addCurrentUserTask(String description, java.util.Date dueDate,
                                   boolean isSms, boolean isEmail, boolean isAlert, boolean pending) {

        Task task = new Task();
        TaskUser tUser = new TaskUser();

        try {

            User admin = ApplicationContext.getContext().get(
                    SessionKeyLookup.CURRENT_USER_KEY);
            String loginId = User.UNKNOWN_USER;

            // If the user is not found in the ApplicationContext then use the
            // Unknown user.
            if (admin != null)
                loginId = admin.getLoginId();

            tUser.setLoginId(loginId);
            tUser.setAssignedDate(new java.util.Date());
            tUser.setTask(task);

            String taskId = "TASKJOB#"
                    + SequenceManager.getInstance().getNextValue(
                    TaskManager.TASK_KEY_GENERATOR);
            task.setTaskId(taskId);
            task.setDueDate(dueDate);
            task.setTaskType(TaskType.InternalSystem.toString());
            task.setOwnerUserId(loginId);
            task.setDescription(description);
            task.setNotificationStrategy(this.getDefaultNotificationStrategy());
            task.setTaskPriority(this.getDefaultTaskPriority());
            task.setTaskStatus(this.getDefaultTaskStatus());
            task.setStartDate(new java.util.Date());
            task.setCompletionDate(null);
            task.setIsNotfAlert(isAlert);
            task.setIsNotfSms(isSms);
            task.setIsNotfEmail(isEmail);
            task.setIsPending(pending);
            task.getTaskUsers().add(tUser);

            this.persist(task);
            this.persist(tUser);

            return task;

        } catch (Throwable t) {
            throw new CoreException(t);
        }

    }

    public <T> List<T> findTasks(String loginId) {
        DataBroker broker = DataRepository.getBroker(TaskManager.class
                .getName());
        Search search = new Search();
        List tasksList = new ArrayList(0);
        search.clear();
        search.addCriterion(SimpleCriterion.equal("loginId", loginId));
        search.addFrom(TaskUser.class);
        List<TaskUser> list = broker.find(search);

        if (list != null && list.size() > 0) {
            for (TaskUser taskUser : list)
                if (taskUser != null)
                    tasksList.add(taskUser.getTask());
        }
        // Collections.sort(tasksList);

        return tasksList;
    }

    public <T> List<T> findAll(Class clazz) {
        DataBroker broker = DataRepository.getBroker(TaskManager.class
                .getName());
        Search search = new Search();
        search = new Search();
        search.addFrom(clazz);

        return broker.find(search);
    }

    public <T> void persist(T item) {
        DataBroker broker = DataRepository.getBroker(TaskManager.class
                .getName());

        broker.persist(item);
    }

    public <T> void delete(T item) {
        DataBroker broker = DataRepository.getBroker(TaskManager.class
                .getName());

        broker.delete(item);
    }

    public <T> T findById(Class clazz, String id) {
        DataBroker broker = DataRepository.getBroker(TaskManager.class
                .getName());

        return (T) broker.findById(clazz, id);
    }

    public List<NotificationStrategy> getNotificationStrategyList() {
        return notificationStrategyList;
    }

    public void setNotificationStrategyList(
            List<NotificationStrategy> notificationStrategyList) {
        this.notificationStrategyList = notificationStrategyList;
    }

    public List<TaskPriority> getTaskPriorityList() {
        return taskPriorityList;
    }

    public void setTaskPriorityList(List<TaskPriority> taskPriorityList) {
        this.taskPriorityList = taskPriorityList;
    }

    public List<TaskStatus> getTaskStatusList() {
        return taskStatusList;
    }

    public void setTaskStatusList(List<TaskStatus> taskStatusList) {
        this.taskStatusList = taskStatusList;
    }

    /**
     * @return default notification Strategy
     * @author Khawaja Muhammad Aamir
     */
    public NotificationStrategy getDefaultNotificationStrategy() {
        for (NotificationStrategy NS : this.notificationStrategyList) {
            if (NS.isIsDefault())
                return NS;
        }
        return null;
    }

    /**
     * @return default Default Task Status
     * @author Khawaja Muhammad Aamir
     */
    public TaskStatus getDefaultTaskStatus() {
        for (TaskStatus TS : this.taskStatusList) {
            if (TS.getIsDefault())
                return TS;
        }
        return null;
    }

    /**
     * @return default Task Priority
     * @author Khawaja Muhammad Aamir
     */
    public TaskPriority getDefaultTaskPriority() {
        for (TaskPriority TS : this.taskPriorityList) {
            if (TS.getIsDefault())
                return TS;
        }
        return null;
    }

}
