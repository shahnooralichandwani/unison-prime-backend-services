package com.avanza.core.calendar;

public enum WorkType {

    Weekend("Weekends"), Daily("DailyHours"), WeekendOverride("WeekendOverride"), DailyOverride("DailyHoursOverride"), Holiday(
            "Holidays"), HolidayOverride("HolidaysOverride");

    private String value;

    private WorkType(String value) {
        this.value = value;
    }

    public static WorkType fromName(String value) {
        if (value.equalsIgnoreCase(WorkType.Weekend.value))
            return WorkType.Weekend;
        if (value.equalsIgnoreCase(WorkType.Daily.value))
            return WorkType.Daily;
        if (value.equalsIgnoreCase(WorkType.DailyOverride.value))
            return WorkType.DailyOverride;
        if (value.equalsIgnoreCase(WorkType.WeekendOverride.value))
            return WorkType.WeekendOverride;
        if (value.equalsIgnoreCase(WorkType.Holiday.value))
            return WorkType.Holiday;
        if (value.equalsIgnoreCase(WorkType.HolidayOverride.value))
            return WorkType.HolidayOverride;
        return null;
    }

    public String getValue() {
        return value;
    }

}
