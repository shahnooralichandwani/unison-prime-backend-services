package com.avanza.core.calendar;

import java.util.HashMap;
import java.util.List;

import com.avanza.core.CoreException;
import com.avanza.core.calendar.db.DBCalendar;
import com.avanza.core.calendar.db.DBWorkType;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.util.configuration.ConfigSection;


public class CalendarCatalog {

    private static HashMap<String, DBCalendar> dbcalendars = new HashMap<String, DBCalendar>(0);
    private static HashMap<String, DBWorkType> dbcalendarsworkTypes = new HashMap<String, DBWorkType>(0);
    private static HashMap<String, Calendar> systemCalendars = new HashMap<String, Calendar>(0);
    private static Calendar defaultCalendar;
    private static String XML_Calendar = "Calendar";
    private static String XML_Calendar_Class = "class";
    private static String XML_Calendar_Key = "key";
    private static String XML_Calendar_Default = "isdefault";

    public static void load(ConfigSection brokerSection) {
        DataBroker broker = DataRepository.getBroker(CalendarCatalog.class.getName());
        List<DBCalendar> loadedCalendars = broker.findAll(DBCalendar.class);
        for (DBCalendar cal : loadedCalendars) {
            dbcalendars.put(cal.getCalendarId(), cal);
        }

        List<DBWorkType> workTypes = broker.findAll(DBWorkType.class);
        for (DBWorkType worktype : workTypes) {
            dbcalendarsworkTypes.put(worktype.getWorkTypeId(), worktype);
        }

        List<ConfigSection> childList = brokerSection.getChildSections(CalendarCatalog.XML_Calendar);

        if ((childList != null) && (childList.size() > 0)) {
            for (int idx = 0; idx < childList.size(); idx++) {

                ConfigSection calendar = childList.get(idx);
                String className = calendar.getTextValue(CalendarCatalog.XML_Calendar_Class);
                String name = calendar.getTextValue(CalendarCatalog.XML_Calendar_Key);
                boolean isDefault = calendar.getValue(CalendarCatalog.XML_Calendar_Default, false);
                try {

                    Object calendarObject = Class.forName(className).newInstance();
                    System.out.println(calendarObject.getClass().getName());
                    ((com.avanza.core.calendar.Calendar) calendarObject).load(dbcalendars.get(name));
                    systemCalendars.put(name, (com.avanza.core.calendar.Calendar) calendarObject);
                    if (isDefault) {
                        defaultCalendar = (com.avanza.core.calendar.Calendar) calendarObject;
                    }
                } catch (ClassNotFoundException e) {
                    throw new CoreException(e, "Calendar Class Not Found");
                } catch (InstantiationException e) {
                    throw new CoreException(e, "Calendar Class Can Not Be Instanciated");
                } catch (IllegalAccessException e) {
                    throw new CoreException(e, "Illegal Access Exception While Accessing Calendar Class");
                }

            }
        }
    }

    public static Calendar getCalendar(String calendarId) {

        Calendar calendar = systemCalendars.get(calendarId);
        if (calendar == null) return getDefaultCalendar();
        return calendar;

    }

    public static Calendar getDefaultCalendar() {
        return defaultCalendar;
    }
}
