package com.avanza.core.calendar;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.avanza.core.data.expression.ExpressionException;
import com.avanza.core.util.Convert;


public class CalendarFunctions {

    private static final String DAY = "day";
    private static final String MONTH = "month";
    private static final String YEAR = "year";
    private static String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private static String[] days = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    public static Date getDate(int hopValue) {
        GregorianCalendar calendar = new GregorianCalendar();
        if (hopValue > 0)
            calendar.add(Calendar.DATE, hopValue);
        return calendar.getTime();
    }

    public static Date getYesterday() {
        Calendar yesterDaysCal = Calendar.getInstance();
        yesterDaysCal.add(Calendar.DATE, -1);
        return yesterDaysCal.getTime();
    }

    @SuppressWarnings("deprecation")
    public static Date getCurrentMonthStart() {
        Calendar todaysCal = Calendar.getInstance();
        Date date = todaysCal.getTime();
        date.setDate(1);
        return date;
    }

    @SuppressWarnings("deprecation")
    public static Date getCurrentMonthEnd() {
        Calendar todaysCal = Calendar.getInstance();
        Date date = todaysCal.getTime();
        date.setDate(31);
        return date;
    }

    public static Date getLastMonthStart() {
        Calendar Cal = Calendar.getInstance();
        Cal.add(Calendar.MONTH, -1);
        Date date = Cal.getTime();
        date.setDate(1);
        return date;
    }

    public static Date getLastMonthEnd() {
        Calendar Cal = Calendar.getInstance();
        Cal.add(Calendar.MONTH, -1);
        Calendar Cal1 = Calendar.getInstance();
        Date currentMonthStart = getCurrentMonthStart();
        Cal1.setTime(currentMonthStart);
        Cal1.add(Calendar.DATE, -1);
        Date date = Cal.getTime();
        date.setDate(Cal1.getTime().getDate());
        return date;
    }

    private static Date dateAdd(String datePart, Object incrementNumber, String dateFormat, String date, Boolean add) {

        int incrementValue = 0;
        DateFormat df = new SimpleDateFormat(dateFormat);
        GregorianCalendar calendarDate = new GregorianCalendar();
        Class incrementNumberClass = incrementNumber.getClass();

        try {
            calendarDate.setTime(df.parse(date));
        } catch (ParseException e) {
            System.out.println("Unable to parse " + date);
        }

        if (incrementNumberClass == Double.class)
            incrementValue = Convert.toInteger(Math.floor((Double) incrementNumber));
        else if (incrementNumberClass == Integer.class || incrementNumberClass == Long.class)
            incrementValue = Convert.toInteger(incrementNumber);
        else
            throw new ExpressionException("'%1$s' is not a valid Data Type for incrementNumber", incrementNumberClass.getName());

        if (!add)
            incrementValue = incrementValue * (-1);

        if (datePart.toLowerCase().equals(DAY.toLowerCase()))
            calendarDate.add(Calendar.DATE, incrementValue);
        else if (datePart.toLowerCase().equals(MONTH.toLowerCase()))
            calendarDate.add(Calendar.MONTH, incrementValue);
        else if (datePart.toLowerCase().equals(YEAR.toLowerCase()))
            calendarDate.add(Calendar.YEAR, incrementValue);
        else
            throw new ExpressionException("'%1$s' is not a valid date part :only day,month and year are allowed", datePart);


        return calendarDate.getTime();
    }

    public static Date dateAdd(String datePart, Object incrementNumber, String dateFormat, String date) {

        return dateAdd(datePart, incrementNumber, dateFormat, date, true);
    }

    public static Date dateSub(String datePart, Object decrementNumber, String dateFormat, String date) {

        return dateAdd(datePart, decrementNumber, dateFormat, date, false);
    }

    public static String dateName(String datePart, String dateFormat, String date) {

        DateFormat df = new SimpleDateFormat(dateFormat);
        GregorianCalendar calendarDate = new GregorianCalendar();

        try {
            calendarDate.setTime(df.parse(date));
        } catch (ParseException e) {
            System.out.println("Unable to parse " + date);
        }

        if (datePart.toLowerCase().equals(DAY.toLowerCase()))
            return days[calendarDate.get(Calendar.DAY_OF_WEEK) - 1];
        else if (datePart.toLowerCase().equals(MONTH.toLowerCase()))
            return months[calendarDate.get(Calendar.MONTH)];
        else if (datePart.toLowerCase().equals(YEAR.toLowerCase()))
            return String.valueOf(calendarDate.get(Calendar.YEAR));
        else
            throw new ExpressionException("'%1$s' is not a valid date part :only day,month and year are allowed", datePart);


    }

    public static int dateDiff(String datePart, String dateFormat, String startDate, String endDate) {

        DateFormat df = new SimpleDateFormat(dateFormat);
        GregorianCalendar startDateValue = new GregorianCalendar();
        GregorianCalendar endDateValue = new GregorianCalendar();
        int diff = 0;

        try {
            startDateValue.setTime(df.parse(startDate));
            endDateValue.setTime(df.parse(endDate));
            if (startDateValue.after(endDateValue)) {
                GregorianCalendar tempDate = new GregorianCalendar();
                tempDate.setTime(startDateValue.getTime());
                startDateValue = endDateValue;
                endDateValue = tempDate;
            }

        } catch (ParseException e) {
            System.out.println("Unable to parse " + startDate + " and " + endDate);
        }

        if (datePart.toLowerCase().equals(DAY.toLowerCase())) {
            while (startDateValue.before(endDateValue))  //(c) on its own = 3 days diff
            {
                startDateValue.set(Calendar.DATE, startDateValue.get(Calendar.DATE) + 1);
                diff++;
            }
            return diff;
        } else if (datePart.toLowerCase().equals(MONTH.toLowerCase())) {
            startDateValue.set(Calendar.DATE, endDateValue.get(Calendar.DATE));
            while (startDateValue.before(endDateValue))  //(c) on its own = 3 days diff
            {
                startDateValue.set(Calendar.MONTH, startDateValue.get(Calendar.MONTH) + 1);
                diff++;
            }
            return diff;
        } else if (datePart.toLowerCase().equals(YEAR.toLowerCase()))
            return startDateValue.get(Calendar.YEAR) - endDateValue.get(Calendar.YEAR);
        else
            throw new ExpressionException("'%1$s' is not a valid date part :only day,month and year are allowed", datePart);


    }

}
