package com.avanza.core.calendar.task;

import com.avanza.core.util.configuration.ConfigSection;

/**
 * @author nasir.nawab
 * This class loads Task-Catalog section from unison-config.xml file.
 */

public class TaskCatalog {

    private static TaskCatalog _instance = new TaskCatalog();

    public static String TASK = "Task";
    public static String ID = "id";
    public static String CLASS_NAME = "className";

    private static String id;
    private static String className;

    public static TaskCatalog getInstance() {
        return _instance;
    }

    public static void load(ConfigSection section) {
        if (section != null) {
            ConfigSection childSection = section.getChildSections(TASK).get(0);
            if (childSection != null) {
                id = childSection.getTextValue(ID);
                className = childSection.getTextValue(CLASS_NAME);
            }
        }
    }

    public static String getId() {
        return id;
    }

    public static String getClassName() {
        return className;
    }


}
