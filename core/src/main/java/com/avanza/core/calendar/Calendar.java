package com.avanza.core.calendar;

import java.util.Date;

import com.avanza.core.calendar.db.DBCalendar;


public interface Calendar {

    public void load(DBCalendar calinfo);

    public long getAdjustedTime(Date startTime, long requiredWorkingTime);

    public long getAdjustedTime(long requiredWorkingTime);

    public long getWorkingTime(Date beginTime, Date endTime);

    public DBCalendar getInternalCalendar();

}
