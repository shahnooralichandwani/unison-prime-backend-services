package com.avanza.core.calendar.task;

import com.avanza.component.calendar.Task;
import com.avanza.component.calendar.TaskUser;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.scheduler.JobImpl;
import com.avanza.notificationservice.adapter.UnisonAlertsProtocol;
import com.avanza.notificationservice.notification.NotificationSender;

/**
 * @author nasir.nawab
 * This job is executed on due date of added task. It sends email and alerts to assigned users of task.
 */

public class AddTaskJob extends JobImpl {


    @Override
    public void executeInner() {

        String taskId = getId();
        DataBroker broker = DataRepository.getBroker(Task.class.getName());
        Task task = broker.findById(Task.class, taskId);

        try {
            for (TaskUser taskUser : task.getTaskUsers()) {

                if (task.getIsNotfEmail())
                    NotificationSender.sendEmail("Task Reminder", task.getDescription(), taskUser.getUser().getEmailUrl(), null, InboundLoader.getInstance().getAckChannel("Email"));
                if (task.getIsNotfAlert())
                    NotificationSender.sendAlert(UnisonAlertsProtocol.ALERTS_CHANNEL, "Task Reminder!", task.getDescription(), taskUser.getLoginId(), task.getDueDate());

            }
        } catch (RuntimeException e) {

            e.printStackTrace();
        }

        System.out.println("Add Task Job Event occured for " + taskId);


    }
}
