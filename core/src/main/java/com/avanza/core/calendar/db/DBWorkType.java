package com.avanza.core.calendar.db;

import com.avanza.core.data.DbObject;


public class DBWorkType extends DbObject {

    private String workTypeId;

    private String workTypeDescPRM;

    private String workTypeDescSEC;


    public String getWorkTypeId() {
        return workTypeId;
    }


    public void setWorkTypeId(String workTypeId) {
        this.workTypeId = workTypeId;
    }


    public String getWorkTypeDescPRM() {
        return workTypeDescPRM;
    }


    public void setWorkTypeDescPRM(String workTypeDescPRM) {
        this.workTypeDescPRM = workTypeDescPRM;
    }


    public String getWorkTypeDescSEC() {
        return workTypeDescSEC;
    }


    public void setWorkTypeDescSEC(String workTypeDescSEC) {
        this.workTypeDescSEC = workTypeDescSEC;
    }
}
