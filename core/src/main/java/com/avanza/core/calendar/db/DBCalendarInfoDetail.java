package com.avanza.core.calendar.db;

import java.util.Date;

import com.avanza.core.data.DbObject;


public class DBCalendarInfoDetail extends DbObject {

    private String calendarInfoId;

    private int detailSequence;

    private Date startTime;

    private Date endTime;

    public int getDetailSequence() {
        return detailSequence;
    }


    public void setDetailSequence(int detailSequence) {
        this.detailSequence = detailSequence;
    }


    public Date getStartTime() {
        return startTime;
    }


    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    public Date getEndTime() {
        return endTime;
    }


    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    public String getCalendarInfoId() {
        return calendarInfoId;
    }


    public void setCalendarInfoId(String calendarInfoId) {
        this.calendarInfoId = calendarInfoId;
    }
}
