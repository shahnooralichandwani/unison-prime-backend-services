package com.avanza.core.calendar.task;

import com.avanza.core.mail.EmailMessageListener;
import com.avanza.core.mail.MessagePoller;
import com.avanza.core.main.EdocumentConfigManager;
import com.avanza.core.scheduler.JobImpl;
import com.avanza.core.util.Logger;

public class POP3TaskJob extends JobImpl {

    private Logger logger = Logger.getLogger(POP3TaskJob.class);

    @Override
    public void executeInner() {
        logger.logInfo("POP3TaskJob Event occured");

        try {
            if (EdocumentConfigManager.isEnabled()) {

                String host = EdocumentConfigManager.getProperties().get("host").toString();
                String port = EdocumentConfigManager.getProperties().get("port").toString();
                String email = EdocumentConfigManager.getProperties().get("id").toString();
                String password = EdocumentConfigManager.getProperties().get("password").toString();

                int intPort = Integer.parseInt(port);
				
			/*	MessagePoller poller = new MessagePoller("pop3",host,intPort,email,password);
				EmailMessageListener listener = new EmailMessageListener();
				poller.addMessageListener(listener);
				
				poller.initialize();*/

            }

        } catch (RuntimeException e) {

            logger.LogException("[ Couldnot initialize pop3 service, POP3TaskJob.executeInner() ]", e);
        }

    }
}
