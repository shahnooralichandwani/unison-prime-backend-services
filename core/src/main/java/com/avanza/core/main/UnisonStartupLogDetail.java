package com.avanza.core.main;

import java.util.Date;

import com.avanza.core.data.DbObject;

public class UnisonStartupLogDetail extends DbObject {
    private static final long serialVersionUID = 1L;
    private String moduleId;
    private int logId;
    private Date startTime;
    private Date endTime;
    private boolean status = UnisonStartupLog.SUCCESS;
    private String exceptionMessage;

    public static final String MODULE_ID_CONFIG_FILE = "ConfigFile";
    public static final String MODULE_ID_INTEGRATION_CHANNELS = "IntegrationChannels";
    public static final String MODULE_ID_LICENSE_INFO = "LicenseInfo";
    public static final String MODULE_ID_EXCEPTION = "Exception";
    public static final String MODULE_ID_CLUSTERS_INFO = "ClustersInfo";
    public static final String MODULE_ID_TIME_LOGGER = "TimeLogger";
    public static final String MODULE_ID_RESOURCE_BUNDLES = "ResourceBundles";
    public static final String MODULE_ID_LOCALE_DEFINITIONS = "LocaleDefinitions";
    public static final String MODULE_ID_DATA_REPOSITORY = "DataRepository";
    public static final String MODULE_ID_DOCUMENT_REPOSITORY = "DocumentRepository";
    public static final String MODULE_ID_INTERACTION_TEMPLATE = "InteractionTemplateData";
    public static final String MODULE_ID_CALENDAR = "Calendar";
    public static final String MODULE_ID_SCHEDULER = "Scheduler";
    public static final String MODULE_ID_SECURITY_SECTION = "SecuritySection";
    public static final String MODULE_ID_VIEW_MANAGER = "ViewManager";
    public static final String MODULE_ID_SEQUENCE_MANAGER = "SequenceManager";
    public static final String MODULE_ID_ENTITY_MAPPINGS = "EntityMappings";
    public static final String MODULE_ID_MODULE_CATALOG = "ModuleCatalog";
    public static final String MODULE_ID_VALUE_TREE = "ValueTreeManager";
    public static final String MODULE_ID_PICK_LIST = "PickListManager";
    public static final String MODULE_ID_WORKFLOW = "WorkflowConfiguration";
    public static final String MODULE_ID_META_MESSAGE = "MetaMessageCatalog";
    public static final String MODULE_ID_ENTITY_MAPPER = "EntityMapperCatalog";
    public static final String MODULE_ID_MESSAGE_FORMAT_DATA = "MessageFormatDataCatalog";
    public static final String MODULE_ID_NOTIFICATION_CATALOG = "NotificationCatalog";
    public static final String MODULE_ID_TEMPLATES = "Templates";
    public static final String MODULE_ID_INBOUND_CONFIG = "InboundConfig";
    public static final String MODULE_ID_NOTIFICATION_CONFIG = "NotificationConfig";
    public static final String MODULE_ID_TASK_MANAGER_LISTS = "TaskManagerLists";
    public static final String MODULE_ID_ALERTS_CONFIG = "AlertsConfig";
    public static final String MODULE_ID_CAMPAIGN_TEMPLATE = "CampaignTemplate";
    public static final String MODULE_ID_DYNAMIC_CONFIGURATION = "DynamicConfiguration";
    public static final String MODULE_ID_Designer_CONFIGURATION = "DesignerConfiguration";
    public static final String MODULE_ID_EDOCUMENT_CONFIGURATION = "EDocumentConfiguration";
    public static final String MODULE_ID_MESSAGE_MAPPINGS = "MessageMappings";
    public static final String MODULE_ID_TASKS_CONFIGURATION = "TasksConfiguration";
    public static final String MODULE_ID_CAMPAIGN_CONFIGURATION = "campaignConfiguration";
    public static final String MODULE_ID_ACTIVITY_TYPE = "ActivityTypeCatalog";

    public UnisonStartupLogDetail(String moduleId) {
        startTime = new Date();
        this.moduleId = moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public void setLogId(int logId) {
        this.logId = logId;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getModuleId() {
        return moduleId;
    }

    public int getLogId() {
        return logId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }
}
