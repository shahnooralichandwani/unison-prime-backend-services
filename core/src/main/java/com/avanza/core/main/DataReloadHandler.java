package com.avanza.core.main;

import java.util.List;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.DynamicDataBroker;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.PickListManager;
import com.avanza.core.sdo.ValueTreeManager;
import com.avanza.core.util.Logger;
import com.avanza.integration.dbmapper.meta.DbMetaCatalog;
import com.avanza.ui.meta.ViewCatalog;
import com.avanza.workflow.configuration.ProcessCatalog;

public class DataReloadHandler {

    private static DataReloadHandler handler;
    private Logger logger = Logger.getLogger(DataReloadHandler.class);

    public static DataReloadHandler getInstance() {
        if (handler == null)
            handler = new DataReloadHandler();
        return handler;
    }

    public void reload() {
        try {
            logger.logInfo("[ Reloading MetaDataRegistry ]");
            MetaDataRegistry.reload();
        } catch (Exception e) {
            logger.logError("[ Exception While Reloading Meta ]", e);
        }
        try {

            logger.logInfo("[ Reloading DB MetaCatalog ]");
            DbMetaCatalog.getInstance().reload();
        } catch (Exception e) {
            logger.logError("[ Exception While Reloading Meta ]", e);
        }
        try {

            logger.logInfo("[ Reloading ViewCatalog ]");
            ViewCatalog.reLoad();
        } catch (Exception e) {
            logger.logError("[ Exception While Reloading Meta ]", e);
        }
        try {

            logger.logInfo("[ Reloading ProcessCatalog ]");
            ProcessCatalog.reLoad();
        } catch (Exception e) {
            logger.logError("[ Exception While Reloading Meta ]", e);
        }
        try {

            logger.logInfo("[ Reloading ValueTreeManager ]");
            ValueTreeManager.reload();
        } catch (Exception e) {
            logger.logError("[ Exception While Reloading Meta ]", e);
        }
        try {

            logger.logInfo("[ Reinitializing Dynamic Brokers ]");
            List<DynamicDataBroker> brokerList = DataRepository
                    .getDynamicBrokers();
            for (DynamicDataBroker broker : brokerList) {
                broker.reloadMappings();
            }
        } catch (Exception e) {
            logger.logError("[ Exception While Reloading Meta ]", e);
        }
        try {

            // load picklist
            PickListManager.load();

        } catch (Exception e) {
            logger.logError("[ Exception While Reloading Meta ]", e);
        }
    }
}
