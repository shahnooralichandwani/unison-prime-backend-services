package com.avanza.core.main;

public interface DynamicLoader {

    public void load(com.avanza.core.util.configuration.ConfigSection section);
}
