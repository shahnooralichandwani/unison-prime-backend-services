package com.avanza.core.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.DbObject;
import com.avanza.core.security.User;

public class UnisonStartupLog extends DbObject {
    private static final long serialVersionUID = 1L;
    public static final String APPLICATION_LOG_ID = "APPLICATION_LOG_ID";
    public static final String APPLICATION_ID_UNISON = "Unison";
    public static final boolean SUCCESS = true;
    public static final boolean FAIL = false;
    private int logId;
    private String applicationId;
    private Date startTime;
    private Date endTime;
    private boolean status = SUCCESS;
    private Set<UnisonStartupLogDetail> details = new LinkedHashSet<UnisonStartupLogDetail>(0);

    public UnisonStartupLog() {
        startTime = new Date();
    }

    public int getLogId() {
        return logId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void addDetail(UnisonStartupLogDetail detail) {
        detail.setEndTime(new Date());
        details.add(detail);
        if (detail.isStatus() == FAIL) {
            status = FAIL;
        }
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Set<UnisonStartupLogDetail> getDetails() {
        return details;
    }

    public void setDetails(Set<UnisonStartupLogDetail> details) {
        this.details = details;
    }

    public void setLogId(int logId) {
        this.logId = logId;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void saveLog() {
        endTime = new Date();
        try {
            User admin = ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);
            String loginId = User.UNKNOWN_USER;
            if (admin != null) loginId = admin.getLoginId();
            this.setCreation(loginId);
            DataBroker broker = DataRepository.getBroker(UnisonStartupLog.class.toString());
            broker.add(this);
            for (UnisonStartupLogDetail logDetail : details) {
                logDetail.setLogId(this.getLogId());
            }
            broker = DataRepository.getBroker(UnisonStartupLogDetail.class.toString());
            broker.persistAll(new ArrayList<UnisonStartupLogDetail>(details));

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}
