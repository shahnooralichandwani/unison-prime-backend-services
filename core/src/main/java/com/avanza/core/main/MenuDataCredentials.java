package com.avanza.core.main;

public class MenuDataCredentials {

    String id;
    String dataModuleNamePrm;
    String dataModuleNameSec;
    String onClick;
    boolean link;
    String linkUrl;
    boolean submit;

    public boolean isSubmit() {
        return submit;
    }

    public boolean getSubmit() {
        return submit;
    }

    public void setSubmit(boolean submit) {
        this.submit = submit;
    }

    public MenuDataCredentials() {
        super();
        this.id = "";
        this.dataModuleNamePrm = "";
        this.dataModuleNameSec = "";
        this.onClick = "";
        this.link = false;
        this.linkUrl = "";
        this.submit = false;
    }

    public MenuDataCredentials(String id, String dataModuleNamePrm, String dataModuleNameSec, String onClick) {
        super();
        this.id = id;
        this.dataModuleNamePrm = dataModuleNamePrm;
        this.dataModuleNameSec = dataModuleNameSec;
        this.onClick = onClick;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDataModuleNamePrm() {
        return dataModuleNamePrm;
    }

    public void setDataModuleNamePrm(String dataModuleNamePrm) {
        this.dataModuleNamePrm = dataModuleNamePrm;
    }

    public String getDataModuleNameSec() {
        return dataModuleNameSec;
    }

    public void setDataModuleNameSec(String dataModuleNameSec) {
        this.dataModuleNameSec = dataModuleNameSec;
    }

    public String getOnClick() {
        return onClick;
    }

    public void setOnClick(String onClick) {
        this.onClick = onClick;
    }

    public boolean isLink() {
        return link;
    }

    public boolean getLink() {
        return link;
    }

    public void setLink(boolean link) {
        this.link = link;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

}