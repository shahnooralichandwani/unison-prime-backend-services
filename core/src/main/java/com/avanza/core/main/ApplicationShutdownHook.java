package com.avanza.core.main;

import com.avanza.core.util.Logger;

/**
 * This thread is called finally at application bnormal ShutDown.
 *
 * @author kraza
 */
public class ApplicationShutdownHook extends Thread {

    private static Logger logger = Logger.getLogger(ApplicationShutdownHook.class);

    @Override
    public void run() {
        logger.logDebug("[Application Shutdown Hook Called]");
        ApplicationLoader.destroy();
    }
}
