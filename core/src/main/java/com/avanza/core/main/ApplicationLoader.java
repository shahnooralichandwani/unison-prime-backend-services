package com.avanza.core.main;

import com.avanza.core.CoreException;
import com.avanza.core.calendar.CalendarCatalog;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.DynamicDataBroker;
import com.avanza.core.data.db.StartupDatabase;
import com.avanza.core.data.sequence.SequenceManager;
import com.avanza.core.exception.ExceptionLoader;
import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaException;
import com.avanza.core.meta.PickListManager;
import com.avanza.core.product.ProductCatalog;
import com.avanza.core.sdo.ActivityTypeCatalog;
import com.avanza.core.sdo.ValueTreeManager;
import com.avanza.core.security.ProviderFactory;
import com.avanza.core.security.SecurityManager;
import com.avanza.core.security.audit.ActivityMessagesCatalog;
import com.avanza.core.util.*;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.configuration.ConfigurationCache;
import com.avanza.core.util.configuration.XmlConfigReader;
import com.avanza.core.web.config.LocaleCatalog;
import com.avanza.integration.dbmapper.meta.DbMetaCatalog;
import com.avanza.integration.formatter.MsgFormatDataCatalog;
import com.avanza.integration.meta.mapping.EntityMapperCatalog;
import com.avanza.integration.meta.messaging.MetaMessageCatalog;
import com.avanza.ui.ViewManager;
import com.avanza.ui.meta.ViewCatalog;
import com.avanza.ui.util.ResourceUtil;

import java.io.File;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

//import com.avanza.ui.designer.DesignerConfigCatalog;
/*import com.avanza.unison.admin.template.DBTemplate;
import com.avanza.unison.admin.template.NsTemplate;
import com.avanza.unison.admin.template.TemplateCatalog;
import com.avanza.unison.admin.template.TemplateOrderByMode;
import com.avanza.workflow.configuration.ProcessCatalog;
import com.avanza.workflow.configuration.action.ActionCatalog;
import com.avanza.workflow.configuration.org.OrganizationManager;*/

/**
 * This class sets up the application from the configuration file specified. The
 * config file in the Xml format.
 *
 * @author kraza
 */
public class ApplicationLoader {

    private static final Logger logger = Logger.getLogger(ApplicationLoader.class);

    // Different Xml Tags in the application Config File.
    public static String XML_DATA_REPOSITORY_SECTION = "Data-Repository";

    public static String XML_SECURITY_SECTION = "Security";

    public static String XML_CLUSTER_CONFIG = "Clusters-Conf";

    public static String XML_RESOURCE_BUNDLES = "Resource-Bundles";

    public static String XML_TIME_LOGGER = "timeLogger";

    public static String XML_LOCALE_SECTION = "Locale-Definition";

    public static String XML_VIEW_MANAGER_SECTION = "View-Defination";

    public static String XML_DOCUMENT_REPOSITORY_SECTION = "Document-Repository";

    public static String XML_NOTIFICATION_CONFIG = "NotificationSender";

    public static String XML_MODULE_CATALOG_SECTION = "Module-Catalog";

    public static String XML_MENU_CATALOG_SECTION = "Menu-Catalog";

    public static String XML_DESIGNER_CONF_SECTION = "Designer-Configuration";

    public static String XML_CALENDAR_SECTION = "Calendar-Catalog";

    public static String XML_SCHEDULER_SECTION = "Scheduler-Catalog";

    public static String BASE_APPLICATION_PATH = "base-webapp-path";

    public static String MESSAGE_FORMATTER_CATALOG = "Message-Formatter-Catalog";

    public static String MESSAGE_FORMATTER = "Message-Formatter";

    public static String CONFIG = "Config";

    public static String XML_ALERTS_CONFIG = "Alert";

    public static String XML_CAMPAIGN_TEMPLATE_SECTION = "Campaign-Template";

    public static String XML_INTEGRATION_CHANNEL_SECTION = "Integration-Channels";

    public static String XML_EDOCUMENT = "EDocument-Config";

    public static String XML_EXCEPTION_SECTION = "Exception";

    public static String XML_TASK_SECTION = "Task-Catalog";

    public static String XML_CAMPAIGN_SECTION = "campaign";

    public static String XML_ACTIVITY_LOG_PROVIDER = "Activity_Log_Provider";

    public static Integer ACTION_LOG_CONFIG_VALUE = 0;
    public static Integer IS_ENABLE_ACTION_LOG_VALUE = 0;
    public static String PRIMARY_COLUMN_EXTERNAL_SOURCE_VALUE = null;
    public static String SHOW_CAMP_DATA_IN_ALERT_VIEW = null;

    //private static NotificationManager notificationDistributor;
    private static FreeMarkerConfigurator templateConfig;

    private static Boolean isLoaded = false;
    private static Boolean isLicenseLoaded = false;

    private static Boolean isDestroyed = false;
    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock writeLock = readWriteLock.writeLock();
    private static String confFilePath;
    public static UnisonStartupLog log;

    /**
     * This method is called when the application exists either normally or
     * abnormally.
     *
     * @throws CoreException
     */
    public static void destroy() throws CoreException {

        if (isDestroyed)
            return;

        /*logger.logInfo("[Shutting down the Application]");

        try {
            CustomerLicenseManager.getInstance().onNodeDown();
        } catch (Exception e) {
            logger.logError("Error occur while license trying to down the node", e);
        }

        // TODO Do all the clean up here.
        ActivityLogAdaptorFactory.getAdaptor().clear();
        notificationDistributor.dispose();
        SchedulerCatalog.getScheduler().shutDownScheduler();
        DocumentManager.shutdownRepositories();*/
        isDestroyed = true;
    }

    /**
     * This method loads the application from the config file specified.
     *
     * @param configFilePath
     * @throws CoreException
     */
    public static void load(String configFilePath) throws CoreException {
        try {

            logger.logInfo("[Registering the ShutDown Hook.]");
            Runtime.getRuntime().addShutdownHook(new ApplicationShutdownHook());

            logger.logInfo("[Starting up the Application from the config file: '%1$s']",
                    configFilePath);

            if (ApplicationLoader.isLoaded) {
                logger.logInfo("[Application is already loaded]");
                return;
            }
            log = new UnisonStartupLog();
            log.setApplicationId(UnisonStartupLog.APPLICATION_ID_UNISON);
            // Need to add the base path to the Unison web app to the session.
            String baseAppPath = configFilePath.substring(0,
                    configFilePath.lastIndexOf(File.separator));
            BASE_APPLICATION_PATH = baseAppPath;

            confFilePath = configFilePath;

            // Reading and parsing the xml config file.
            ConfigSection configSection = loadConfigFile(configFilePath);

            // load Startup Database.
            loadStartupDatabase(configSection);

            // Loading the ClustersConfig
            // TODO clusters info will be moved to startup db.
            loadClustersInfo(configSection);

            // assignment of DB Config section
            ConfigSection dbConfigSection = StartupDatabase.getStartupDatabase().getConfigSection(
                    "local", "unison");

            /*
             * Shahbaz Dated 20-Sept-2010 During Campaign issues fixes Holds
             * Configuration Cache for future use start
             */

            ConfigurationCache.getInstance().setConfigurations(dbConfigSection);
            // end

            // Loading Integration Channels
            loadIntegrationChannels(dbConfigSection);

//			// Loading License Info
//			loadLicenseInfo();

            //Loading the TimeLogger
            loadTimeLogger(dbConfigSection);

            // Loading the resource bundles
            // TODO: on startup application
            loadResourceBundles(configSection);

            // Loading the application locales.
            loadLocaleDefinitions(dbConfigSection);

            // Loading the Data Repository Section.
            loadDataRepository(dbConfigSection);

            // Loading the Document Repository
            loadDocumentRepository(dbConfigSection);

            // Loading the Interaction Template Data cache.
            loadInteractionTemplateData();

            // Loading the Calendar.
            loadCalendar(dbConfigSection);

            // Loading the Scheduler.
            loadScheduler(dbConfigSection);

            // Loading the Security Section.
            loadSecuritySection(dbConfigSection);

            // Loading the Security Section.
            loadViewManager(dbConfigSection);

            // Loading Sequence Manager
            loadSequenceManager();

            // Loading Entity Mappings
            loadEntityMappings();

            // Loading Module Catalog
            // moved to permission table.
            //loadModuleCatalog(configSection);

            // Loading the ValueTree Manager
            loadValueTreeManager();

            // Loading Work flow Configuration
            loadWorkFlowConfiguration();

            // Loading the Meta Message Catalog Section.
            loadMetaMessageCatalog();

            // Loading the Message Entity Mapping Catalog Section.
            loadEntityMapperCatalog();

            // Loading the Message Format Data Catalog Section.
            loadMsgFormatDataCatalog();

            // Loading the Notification from the Database.
            loadNotificationCatalogs();

            // Loading Templates.
            loadTemplatesWithLog();

            // Loading the Inbound Configuration
            loadInboundConfig(configSection);

            // Loading the Notification Config
            loadNotificationConfig(dbConfigSection);

            // Loading the Document Repository
            loadTaskManagerLists();

            // Loading the Alerts Config
            loadAlertsConfig(dbConfigSection);

            // Loading Campaign Template Config
            loadCampaignTemplate(dbConfigSection);

            // Loading Campaign Configuration
            loadCampaignConfiguration(dbConfigSection);

            // Loading Dynamic Configurations i.e. Sale Plan Section,Campaign
            // etc.
            loadDynamicConfiguration();

            ProductCatalog.getInstance().load(dbConfigSection);

            loadDesignerConfiguration(dbConfigSection);

            // Loading Edocument Configurations
            loadEDocumentConfiguration(dbConfigSection);

            // Loading Message Mappings
            loadMessageMappings();

            // Loading Task Catalog section
            loadTasksConfiguration(dbConfigSection);

            // Loading the PickList Manager
            loadPickListManager(); // load pick-list data after loading meta-entity , so that picklist can load.
			
			/*HibernateDataBroker dataBroker = (HibernateDataBroker) DataRepository.getBroker(ActivityLog.class.getName());
			ActivityLog findById = dataBroker.findById(ActivityLog.class, "_2013-04-29_00000028335");

			findById.setUpdatedOn(new Date());
			Set<ActivityLogDetail> activityLogDetails = findById.getActivityLogDetails();

			for (ActivityLogDetail singleDetail : activityLogDetails) {
				singleDetail.setCreatedBy("syssssssssmmmm");
			}

			ApplicationContext.getContext().release();
			Session tempSession = dataBroker.getTempSession();

			tempSession.update(findById);
			tempSession.close();*/

            loadActivityTypes();

            //Loading SecActivityMessages
            loadActivityLoggerProperties(dbConfigSection);


            // Added by Shafique Rehman
            loadActionLogConfiguration();
            primaryColumnForSearchCustomerFromExternalSource();

            logger.logInfo("[Application loaded]");
        } catch (Exception e) {
            if (log != null) {
                log.setStatus(UnisonStartupLog.FAIL);
            }
            e.printStackTrace();
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            if (log != null) {
                log.setStatus(UnisonStartupLog.FAIL);
            }
            e.printStackTrace();
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);

            if (log != null) {
                log.saveLog();
                log = null;
            }

        } finally {
            ApplicationContext.getContext().release();

        }

    }

    /**
     * This method is used to load Action Logging Configuration.
     * If configuration value is 1 then schedule separate job else persist direct in DB
     *
     * @author shafique.rehman
     */
    private static void loadActionLogConfiguration() {

        List<ConfigSection> propertySections = ConfigurationCache.getInstance().getConfigSection("Property");
        for (ConfigSection section : propertySections) {
            if (section.getTextValue("key").equalsIgnoreCase("ACTION_LOG_CONFIGURATION")) {
                ACTION_LOG_CONFIG_VALUE = Integer.parseInt(section.getTextValue("value"));
            }
            if (section.getTextValue("key").equalsIgnoreCase("IS_ENABLE_ACTION_LOG")) {
                IS_ENABLE_ACTION_LOG_VALUE = Integer.parseInt(section.getTextValue("value"));
            }

        }


    }

    /**
     * This method is used to load column value from configuration_instance table.
     *
     * @author shafique.rehman
     */
    private static void primaryColumnForSearchCustomerFromExternalSource() {
        List<ConfigSection> propertySections = ConfigurationCache.getInstance().getConfigSection("Property");
        for (ConfigSection section : propertySections) {

            if (section.getTextValue("key").equalsIgnoreCase("SHOW_CAMP_DATA_IN_ALERT_VIEW")) {
                SHOW_CAMP_DATA_IN_ALERT_VIEW = section.getTextValue("value");
            }
            if (section.getTextValue("key").equalsIgnoreCase("PRIMARY_COLUMN_EXTERNAL_SOURCE")) {
                PRIMARY_COLUMN_EXTERNAL_SOURCE_VALUE = section.getTextValue("value");
            }
        }
    }

    private static void loadMessageMappings() {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_MESSAGE_MAPPINGS);
        try {
            logger.logInfo("[Loading Message Mappings Configuration ]");
            com.avanza.core.meta.messaging.MetaMessageCatalog.load();

        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadEDocumentConfiguration(ConfigSection configSection) {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_EDOCUMENT_CONFIGURATION);
        try {
            logger.logInfo("[Loading Edocument Configuration ]");
            ConfigSection emailConfig = configSection.getChildSections(XML_EDOCUMENT).get(0);
            EdocumentConfigManager.load(emailConfig);

        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    private static void loadIntegrationChannels(ConfigSection configSection) {
        /*UnisonStartupLogDetail logDetail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_INTEGRATION_CHANNELS);
        try {
            logger.logInfo("[Loading Integration Channels ]");
            ConfigSection integrationChannelConfig = configSection.getChild(XML_INTEGRATION_CHANNEL_SECTION);
            IntegrationChannelFactory.getInstance().load(integrationChannelConfig);

        } catch (Exception e) {
            logDetail.setExceptionMessage(e.getMessage());
            logDetail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            logDetail.setExceptionMessage(e.getMessage());
            logDetail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(logDetail);*/
    }

    private static void loadDesignerConfiguration(ConfigSection configSection) {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_Designer_CONFIGURATION);
        try {
            logger.logInfo("[Loading Designer Config ]");
            ConfigSection designerConfig = configSection.getChild(XML_DESIGNER_CONF_SECTION);
            DesignerConfigCatalog.load(designerConfig);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/

    }

    private static void loadClustersInfo(ConfigSection configSection) {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_CLUSTERS_INFO);
        try {
            logger.logInfo("[Loading Clusters Config ]");
            ConfigSection clusterConfig = configSection.getChild(XML_CLUSTER_CONFIG);
            ClustersConfigLoader.load(clusterConfig);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }


    private static void loadDocumentRepository(ConfigSection configSection) {
       /* UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_DOCUMENT_REPOSITORY);
        try {
            logger.logInfo("[Loading Document Repository]");
            ConfigSection documentSection = configSection.getChild(XML_DOCUMENT_REPOSITORY_SECTION);
            DocumentManager.load(documentSection);
            detail.setStatus(UnisonStartupLog.SUCCESS);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    private static void loadNotificationConfig(ConfigSection configSection) {
       /* UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_NOTIFICATION_CONFIG);
        try {
            logger.logInfo("[Loading Notification Config]");
            NotificationConfigLoader.load();
            NotificationSender.load(configSection.getChildSections(XML_NOTIFICATION_CONFIG).get(0));
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    private static void loadAlertsConfig(ConfigSection configSection) {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_ALERTS_CONFIG);
        try {
            logger.logInfo("[Loading Alerts Config]");
            AlertsConfig.load(configSection.getChildSections(XML_ALERTS_CONFIG).get(0));
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    private static void loadInboundConfig(ConfigSection configSection) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_INBOUND_CONFIG);
        try {
            logger.logInfo("[Loading TimeLogger Catalog]");
            InboundLoader.getInstance().load(configSection.getChild("Inbound-Config"));
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadPickListManager() {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_PICK_LIST);
        try {
            logger.logInfo("[Loading PickList Manager]");
            PickListManager.load();
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadActivityTypes() {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                "ActivityTypeCatalog");
        try {
            logger.logInfo("[Loading Activity Type]");
            ActivityTypeCatalog.getInstance().loadActivitiesByParentChildMap();
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(false);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(false);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadTimeLogger(ConfigSection configSection) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_TIME_LOGGER);
        try {
            logger.logInfo("[Loading TimeLogger Catalog]");

            List<ConfigSection> list = configSection.getChildSections();
            list.contains(XML_TIME_LOGGER);
            ConfigSection timmerCatalog = configSection.getChildSections(XML_TIME_LOGGER).get(0);
            TimeLogger.init(timmerCatalog);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadNotificationCatalogs() {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_NOTIFICATION_CATALOG);
        try {
            logger.logInfo("[Loading Notification Catalog]");
            ChannelCatalog.getInstance();
            notificationDistributor = NotificationDistributor.getInstance();
            MetaEventCatalog.getInstance().load(null);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    private static void loadTemplatesWithLog() {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_TEMPLATES);
        try {
            loadTemplates();
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    public static void loadTemplates() {
        try {
            loadAllTemplates();
        } catch (Exception e) {
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
    }

    private static void loadAllTemplates() {
        /*try {
            writeLock.lock();
            logger.logInfo("[Loading Templates]");
            templateConfig = FreeMarkerConfigurator.getInstance();
            List<NsTemplate> templateList = DBTemplate.getInstance().getAllTemplate(OrderType.ASC,
                    TemplateOrderByMode.templateType, TemplateOrderByMode.templateName);
            Iterator<NsTemplate> iterator = templateList.iterator();
            while (iterator.hasNext()) {
                NsTemplate template = iterator.next();
                if (StringHelper.isNotEmpty(template.getSubject()))
                    templateConfig.createTemplate(template.getTemplateId() + "_subject",
                            template.getSubject());
                if (StringHelper.isNotEmpty(template.getTemplateContent()))
                    templateConfig.createTemplate(template.getTemplateId() + "_content",
                            template.getTemplateContent());
                if (StringHelper.isNotEmpty(template.getHeaderBody()))
                    templateConfig.createTemplate(template.getTemplateId() + "_header",
                            template.getHeaderBody());
            }
        } finally {
            writeLock.unlock();
        }*/
    }

    private static void loadSequenceManager() {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_SEQUENCE_MANAGER);
        try {
            logger.logInfo("[Loading Sequence Manager]");
            SequenceManager sequenceManager = SequenceManager.getInstance();
            sequenceManager.load();
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadScheduler(ConfigSection configSection) {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_SCHEDULER);
        try {
            logger.logInfo("[Loading Scheduler]");
            ConfigSection schedulerCatalog = configSection.getChild(XML_SCHEDULER_SECTION);
            SchedulerCatalog.load(schedulerCatalog);
            logger.logInfo("[Starting Default Scheduler]");
            SchedulerCatalog.getScheduler().startScheduler();

        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    private static void loadInteractionTemplateData() {
       /* UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_INTERACTION_TEMPLATE);
        try {
            logger.logInfo("[Loading Interaction Template Data]");
            TemplateCatalog.load();
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    private static void loadCalendar(ConfigSection section) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_CALENDAR);
        try {
            logger.logInfo("[Loading Calendar]");
            ConfigSection calendarCatalog = section.getChild(XML_CALENDAR_SECTION);
            CalendarCatalog.load(calendarCatalog);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadMsgFormatDataCatalog() {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_MESSAGE_FORMAT_DATA);

        try {
            logger.logInfo("[Loading MsgFormatDataCatalog]");
            MsgFormatDataCatalog.Load();

        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadMetaMessageCatalog() {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_META_MESSAGE);

        try {
            logger.logInfo("[Loading Meta Message Catalog]");
            MetaMessageCatalog.load();

        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadEntityMapperCatalog() {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_ENTITY_MAPPER);
        try {
            logger.logInfo("[Loading Entity Mapper Catalog]");
            EntityMapperCatalog.load();

        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadWorkFlowConfiguration() {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_WORKFLOW);
        try {
            logger.logInfo("[Loading the Workflow Configuration]");
            ProcessCatalog.load();
            OrganizationManager.load();
            ActionCatalog.load();

        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    /**
     * private method loads the application locales definitions from xml config
     * file.
     *
     * @param configSection
     */
    private static void loadLocaleDefinitions(ConfigSection configSection) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_LOCALE_DEFINITIONS);
        try {
            logger.logInfo("[Loading the Locales Settings]");
            ConfigSection localeSection = configSection.getChild(XML_LOCALE_SECTION);
            LocaleCatalog.getInstance().load(localeSection);

        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    /**
     * private method loads the application locales definitions from xml config
     * file.
     *
     * @param configSection
     */
    private static void loadResourceBundles(ConfigSection configSection) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_RESOURCE_BUNDLES);
        try {
            logger.logInfo("[Loading the Resource Bundle Settings]");
            ConfigSection resourceSection = configSection.getChild(XML_RESOURCE_BUNDLES);
            ResourceUtil.load(resourceSection);

        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    /**
     * private method loads the application modules section from xml config
     * file.
     *
     * @param configSection
     */
    private static void loadModuleCatalog(ConfigSection configSection) {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_MODULE_CATALOG);
        try {
            logger.logInfo("[Loading the Modules Settings]");
            ConfigSection moduleCatalogSection = configSection.getChild(XML_MODULE_CATALOG_SECTION);
            ModuleCatalogImpl.getInstance().load(moduleCatalogSection);

            ConfigSection menuCatalogSection = configSection.getChild(XML_MENU_CATALOG_SECTION);
            MenuCatalog.getInstance().load(menuCatalogSection);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    /**
     * private method loads the application view manager section from xml config
     * file.
     *
     * @param configSection
     */
    public static void loadViewManager(ConfigSection configSection) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_VIEW_MANAGER);
        try {
            logger.logInfo("[Loading the ViewManager Settings]");
            ConfigSection viewManagerSection = configSection.getChild(XML_VIEW_MANAGER_SECTION);
            ViewManager.load(viewManagerSection);
            MetaDataRegistry.load();

            ViewCatalog.load();
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadEntityMappings() {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_ENTITY_MAPPINGS);
        try {
            List<DynamicDataBroker> brokerList = DataRepository.getDynamicBrokers();

            for (DynamicDataBroker broker : brokerList) {

                broker.loadMappings();
            }
            DbMetaCatalog.getInstance();

        } catch (MetaException e1) {
            e1.printStackTrace();
            detail.setExceptionMessage(e1.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.LogException("MetaDataBroker", e1);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e1);
        } catch (Exception e) {
            e.printStackTrace();
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.LogException("MetaDataBroker", e);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    /**
     * private method loads the application security section from xml config
     * file.
     *
     * @param configSection
     */
    private static void loadSecuritySection(ConfigSection configSection) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_SECURITY_SECTION);
        try {
            logger.logInfo("[Loading the security Settings]");
            ConfigSection securitySection = configSection.getChild(XML_SECURITY_SECTION);
            ProviderFactory.load(securitySection);
            SecurityManager.load(securitySection);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            e.printStackTrace();
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    /**
     * private method loads the application data repository section from xml
     * config file.
     *
     * @param configSection
     */
    private static void loadDataRepository(ConfigSection configSection) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_DATA_REPOSITORY);
        try {
            logger.logInfo("[Loading the Data Repositories]");
            ConfigSection dataRepository = configSection.getChild(XML_DATA_REPOSITORY_SECTION);
            DataRepository.load(dataRepository);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            e.printStackTrace();
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadStartupDatabase(ConfigSection configSection) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_DATA_REPOSITORY);
        try {
            logger.logInfo("[Loading StartupDatabase]");
            ConfigSection dataRepository = configSection.getChild(XML_DATA_REPOSITORY_SECTION);
            DataRepository.loadStartupDatabase(dataRepository);
        } catch (Exception e) {
            logger.LogException(e.getMessage(), e);
        }
    }

    /**
     * private method loads the valuetree manager.
     */
    private static void loadValueTreeManager() {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_VALUE_TREE);
        try {
            logger.logInfo("[Loading the Value Tree Manager]");
            ValueTreeManager.load();
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            e.printStackTrace();
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    /**
     * private method loads the application configuration from xml config file.
     */
    public static ConfigSection loadConfigFile(String configFilePath) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_CONFIG_FILE);
        try {
            logger.logInfo("[Parsing the config file]");
            XmlConfigReader xmlConfigReader = new XmlConfigReader();
            ConfigSection configSection = xmlConfigReader.load(configFilePath);
            Guard.checkNull(configSection, String.format("ApplicationLoader.load('%s')",
                    configFilePath));
            return configSection;
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
        return null;
    }

    public static FreeMarkerConfigurator getTemplateConfig() {
        return templateConfig;
    }

    /**
     * private method the task lists in TaskManager
     *
     * @author Khawaja Muhammad Aamir
     */
    private static void loadTaskManagerLists() {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_TASK_MANAGER_LISTS);
        List<NotificationStrategy> notificationStrategyList = new ArrayList<NotificationStrategy>(0);
        List<TaskPriority> taskPriorityList = new ArrayList<TaskPriority>(0);
        List<TaskStatus> taskStatusList = new ArrayList<TaskStatus>(0);
        TaskManager taskManager = TaskManager.getInstance();
        logger.logInfo("[loading task File]: loadTaskManagerList()");

        logger.logInfo("[loading Notification Strategy List]");
        notificationStrategyList = taskManager.findAll(NotificationStrategy.class);
        taskManager.setNotificationStrategyList(notificationStrategyList);

        logger.logInfo("[loading Task Status List]");
        taskPriorityList = taskManager.findAll(TaskPriority.class);
        taskManager.setTaskPriorityList(taskPriorityList);

        logger.logInfo("[loading Task Priority List]");
        taskStatusList = taskManager.findAll(TaskStatus.class);
        taskManager.setTaskStatusList(taskStatusList);
        log.addDetail(detail);*/
    }

    private static void loadCampaignTemplate(ConfigSection configSection) {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_CAMPAIGN_TEMPLATE);
        try {
            logger.logInfo("[Loading Campaign Template Config]");
            ConfigSection dataRepository = configSection.getChildSections(XML_CAMPAIGN_TEMPLATE_SECTION).get(0);
            CampaignTemplateManager.load(dataRepository);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

//	private static void loadLicenseInfo() throws Exception{
//		UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
//				UnisonStartupLogDetail.MODULE_ID_LICENSE_INFO);
//		
//		CustomerLicenseManager customerLicenseManager = null;
//		try {
//			logger.logInfo("[Loading Licensing Info]");
//			LicenseManager.load(); //TODO Remove old implementation
//			LicenseManager.getInstance().validateServer(); //TODO Remove old implementation
//			
//			customerLicenseManager = CustomerLicenseManager.getInstance();
//			LicenseLoader licenseLoader = StartupDatabase.getStartupDatabase().getLicenseLoader();
//			customerLicenseManager.load(licenseLoader);
//		
//			
//			
//			//Verify capacity of system
//			customerLicenseManager.verifyCapacity();
//			
//			
//			//Verify evaluation of license
//			customerLicenseManager.verifyEvaluation();
//			
//			//Load license info for display puppose
//			customerLicenseManager.loadLicneceInfoForDispaly();
//			
//			isLicenseLoaded = true;
//		}
//		catch (LicenseeException le ){
//			detail.setExceptionMessage(le.getMessage());
//			detail.setStatus(UnisonStartupLog.FAIL);
//			logger.logInfo("[Loading Licensing Info Failed Exception occoured]");
//			logger.LogException("Exception Occoured: %1$s", le);
//			isLicenseLoaded = false;
//			customerLicenseManager.logloadingError(le.getMessage());
//			
//			if(customerLicenseManager!=null){
//				customerLicenseManager.handelFailure(le);
//			}
//			throw le;
//
//		} catch (Exception e) {
//			detail.setExceptionMessage(e.getMessage());
//			detail.setStatus(UnisonStartupLog.FAIL);
//			logger.logInfo("[Loading Licensing Info Failed Exception occoured]");
//			logger.LogException("Exception Occoured: %1$s", e);
//			isLicenseLoaded = false;
//			customerLicenseManager.logloadingError(LicenseConstants.EXCEPTION.ERROR_MESSSAGE_GLOBAL);
//			throw e;
//		} catch (Error e) {
//			detail.setExceptionMessage(e.getMessage());
//			detail.setStatus(UnisonStartupLog.FAIL);
//			logger.logInfo("[Loading Licensing Info Failed Error occoured]");
//			logger.LogException("Exception Occoured: %1$s", e);
//			isLicenseLoaded = false;
//			customerLicenseManager.logloadingError(LicenseConstants.EXCEPTION.ERROR_MESSSAGE_GLOBAL);
//			throw e;
//		}
//		log.addDetail(detail);
//	}


    private static void loadDynamicConfiguration() {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_DYNAMIC_CONFIGURATION);
        try {
            logger.logInfo("[Loading Dynamic Loader Info]");
            ConfigSection configSection = loadConfigFile(confFilePath);
            List<ConfigSection> sections = configSection.getChildSections();
            for (ConfigSection section : sections) {
                if (section.hasAttribute("implClass")) {
                    String impClass = section.getTextValue("implClass");
                    DynamicLoader loader = (DynamicLoader) Class.forName(impClass).newInstance();
                    loader.load(section);
                }
            }
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Licensing Info Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Licensing Info Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    private static void loadException(ConfigSection configSection) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_EXCEPTION);
        try {
            logger.logInfo("[Loading Exception Config]");
            ConfigSection exceptionSection = configSection
                    .getChild(XML_EXCEPTION_SECTION);
            ExceptionLoader.load(exceptionSection);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

    private static void loadTasksConfiguration(ConfigSection configSection) {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_TASKS_CONFIGURATION);
        try {
            logger.logInfo("[Loading Task Config]");
            ConfigSection exceptionSection = configSection.getChild(XML_TASK_SECTION);
            TaskCatalog.load(exceptionSection);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }

    private static void loadCampaignConfiguration(ConfigSection configSection) {
        /*UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_CAMPAIGN_CONFIGURATION);
        try {
            logger.logInfo("[Loading Campaign Config]");
            List<ConfigSection> campaignSections = configSection.getChildSections(XML_CAMPAIGN_SECTION);
            if (campaignSections.size() > 0) {
                CampaignCatalog.getInstance().load(campaignSections.get(0));
            }
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);*/
    }


    private static void loadActivityLoggerProperties(ConfigSection configSection) {
        UnisonStartupLogDetail detail = new UnisonStartupLogDetail(
                UnisonStartupLogDetail.MODULE_ID_CAMPAIGN_CONFIGURATION);
        try {
            logger.logInfo("[Loading Activity Messages]");
            logger.logInfo("[Loading Designer Config ]");
            ConfigSection designerConfig = configSection.getChild(XML_ACTIVITY_LOG_PROVIDER);
            ActivityMessagesCatalog.load(designerConfig);
        } catch (Exception e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Exception occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        } catch (Error e) {
            detail.setExceptionMessage(e.getMessage());
            detail.setStatus(UnisonStartupLog.FAIL);
            logger.logInfo("[Loading Failed Error occoured]");
            logger.LogException("Exception Occoured: %1$s", e);
        }
        log.addDetail(detail);
    }

}
