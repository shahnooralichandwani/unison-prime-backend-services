package com.avanza.core.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.avanza.core.CoreException;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;

public class ClustersConfigLoader {

    private static Logger logger = Logger.getLogger(ClustersConfigLoader.class);

    private static String XML_CLUSTER_NODE = "Node";
    private static String XML_CLUSTER_NODE_NAME = "name";
    private static String XML_CLUSTER_NODE_IP = "ip";
    private static String XML_CLUSTER_NODE_PORT = "port";
    private static String XML_CLUSTER_NODE_APP_URL = "applicationUrl";
    private static String XML_CLUSTER_NODE_CURRENT = "iscurrent";
    private static String XML_CLUSTER_NODE_HTTPS = "ishttps";

    private static HashMap<String, ClusterInfo> clustersInfo = new HashMap<String, ClusterInfo>(0);

    public static void load(ConfigSection section) {
        try {
            if (section != null && section.getChildSections().size() > 0) {
                List<ConfigSection> childSections = section.getChildSections(XML_CLUSTER_NODE);
                for (ConfigSection sec : childSections) {
                    String name = sec.getTextValue(XML_CLUSTER_NODE_NAME);
                    String ip = sec.getTextValue(XML_CLUSTER_NODE_IP);
                    String port = sec.getTextValue(XML_CLUSTER_NODE_PORT);
                    String url = sec.getTextValue(XML_CLUSTER_NODE_APP_URL);
                    boolean current = sec.getBooleanValue(XML_CLUSTER_NODE_CURRENT);
                    boolean https = sec.getBooleanValue(XML_CLUSTER_NODE_HTTPS);
                    ClusterInfo info = new ClusterInfo(name, ip, url, port, current, https);
                    clustersInfo.put(name, info);
                }
            }
        } catch (Exception e) {
            logger.LogException("Exception while loading Clusters Config", e);
            throw new CoreException(e, "Exception while loading Clusters Config");
        }
        ClusterInfo info = getCurrentClusterInfo();
        if (info == null)
            throw new CoreException("No Current Cluster Info Found");
    }

    public static ClusterInfo getClusterInfo(String name) {
        return clustersInfo.get(name);
    }

    public static ClusterInfo getCurrentClusterInfo() {
        ClusterInfo retVal = null;
        for (ClusterInfo info : clustersInfo.values()) {
            if (info.isCurrent())
                retVal = info;
        }
        return retVal;
    }

    public static HashMap<String, ClusterInfo> getClustersInfo() {
        return clustersInfo;
    }

    public static List<ClusterInfo> getClustersInfoList() {
        List<ClusterInfo> list = new ArrayList<ClusterInfo>(clustersInfo.values());
        return list;
    }

}
