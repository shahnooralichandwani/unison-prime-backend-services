/**
 *
 */
package com.avanza.core.main;

import java.util.List;
import java.util.Properties;

import com.avanza.core.CoreException;
import com.avanza.core.calendar.task.POP3TaskJob;
import com.avanza.core.document.Document;
import com.avanza.core.document.ERepositoryDocument;
import com.avanza.core.meta.MetaCounter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.scheduler.JobID;
import com.avanza.core.scheduler.SchedulerCatalog;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.service.EdocumentService;
import com.avanza.core.service.MetaDataService;
import com.avanza.core.util.Cryptographer;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;

/**
 * @author shahbaz.ali
 *
 */
public class EdocumentConfigManager {

    private static int pollInterval;
    private static boolean enabled;
    private static String systemAccount;
    private static String templateId;
    private static String process;
    private static boolean enableAutoRespond;

    private static final String EDOCUMENT_TYPE_EMAIL = "0000000191";
    private static final String TEMPLATE_ID = "AutoRespondEmailTemplateId";
    private static final String EMAIL_PROCESS_CODE = "emailProcess";
    private static final String ENABLE_AUTO_RESPOND = "enableAutoRespond";


    private static Properties properties = new Properties();

    private final static Logger logger = Logger.getLogger(EdocumentConfigManager.class);

    public static int getPollIntervalMins() {
        return pollInterval;
    }

    public static void load(ConfigSection cfgSection) {

        Cryptographer cryptographer = new Cryptographer();

        logger.logInfo("[Loading Edocument Config ( EdocumentConfigManager.load() ) ]");

        try {
            templateId = cfgSection.getTextValue(TEMPLATE_ID);
            process = cfgSection.getTextValue(EMAIL_PROCESS_CODE);
            enableAutoRespond = cfgSection.getBooleanValue(ENABLE_AUTO_RESPOND);


            List<ConfigSection> childList = cfgSection.getChildSections();

            if (childList != null && childList.size() > 0) {
//			   ConfigSection childSection = cfgSection.getChild("Email-Polling");

//				if (cfgSection != null) {

                int jobCount = 0;
                for (ConfigSection childSection : childList) {

                    jobCount++;

                    pollInterval = childSection.getValue("poll-interval", 5);
                    enabled = childSection.getBooleanValue("enabled");
                    childSection.getTextValue("email");

                    properties.put("host", childSection.getTextValue("host"));
                    properties.put("port", childSection.getTextValue("port"));
                    properties.put("id", childSection.getTextValue("email"));
                    String plainPassword = "";

                    String encPass = childSection.getTextValue("password");

                    if (StringHelper.isNotEmpty(encPass))
                        plainPassword = cryptographer.decrypt(encPass);

                    properties.put("password", plainPassword);

                    systemAccount = properties.getProperty("id");

                    StringBuffer uid = new StringBuffer();
                    uid.append("POP3ServiceJob").append(jobCount);

                    if (enabled) {
                        SchedulerCatalog.getScheduler().scheduleMinutelyJob(new JobID(uid.toString()), "EmailPolling", POP3TaskJob.class.getName(), pollInterval);
                    }

                    // if not enabled then removed the existing one
                    else {
                        SchedulerCatalog.getScheduler().removeJob(new JobID(uid.toString()), "EmailPolling");
                    }
                }
            }
        } catch (Exception e) {
            logger.LogException("Loading Edocument Config Failed ( EdocumentConfigManager.load() )", e);
        }

    }

    public static boolean isEnabled() {

        return enabled;
    }

    public static DataObject prepareEdocumentObject(Document message) {
        logger.logInfo("[Preparing Edocument Object ( EdocumentConfigManager.prepareEdocumentObject() ) ]");

        ERepositoryDocument edoc = (ERepositoryDocument) message;

        // For EMAIL
        DataObject edocumentInstance = MetaDataRegistry.getMetaEntity(EDOCUMENT_TYPE_EMAIL).createEntity();
        edocumentInstance.setValue("EDOCUMENT_ID", (String) MetaDataRegistry.getNextCounterValue(MetaCounter.EDOCUMENT_COUNTER));
        edocumentInstance.setValue("EDOCUMENT_TYPE", EDOCUMENT_TYPE_EMAIL);
        edocumentInstance.setValue("CURRENT_STATE", edoc.getFromAddress());
        edocumentInstance.setValue("EDOCUMENT_SUBJECT", edoc.getDocumentTitle());
        edocumentInstance.setValue("RECEIVED_FROM", edoc.getFromAddress());
        edocumentInstance.setValue("SEND_TO", edoc.getToAddress());
        edocumentInstance.setValue("EDOCUMENT_REPOSITORY_ID", edoc.getDocumentId());
        edocumentInstance.setValue("IS_COMPOSED", false);
        edocumentInstance.setValue("IS_READ", false);
        edocumentInstance.setValue("IS_ARCHIVED", false);
        edocumentInstance.setValue("RECEIVED_ON", edoc.getReceivedDate());
        edocumentInstance.setValue("SENT_ON", edoc.getSentDate());

        return edocumentInstance;
    }

    public static void persistEDocument(DataObject dataObject) {
        logger.logInfo("[Persisting Edocument Object ( EdocumentConfigManager.persistEDocument() ) ]");

        try {

            MetaDataService service = new EdocumentService();
            service.save(EDOCUMENT_TYPE_EMAIL, dataObject);

        } catch (CoreException e) {
            logger.LogException("CoreException PersistingEdocument in ( EdocumentConfigManager.persistEDocument() ) ", e);
        } catch (Exception e) {
            logger.LogException("Exception PersistingEdocument in ( EdocumentConfigManager.persistEDocument() ) ", e);
        }

    }

    public static String getSystemAccount() {
        return systemAccount;
    }

    public static String getTemplateId() {
        return templateId;
    }

    public static String getProcess() {
        return process;
    }

    public static Properties getProperties() {
        return properties;
    }

    public static void setProperties(Properties properties) {

        EdocumentConfigManager.properties = properties;
    }

    public static boolean isEnableAutoRespond() {
        return enableAutoRespond;
    }

    public static void setEnableAutoRespond(boolean enableAutoRespond) {
        EdocumentConfigManager.enableAutoRespond = enableAutoRespond;
    }

}
