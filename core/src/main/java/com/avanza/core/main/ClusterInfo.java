package com.avanza.core.main;

public class ClusterInfo {

    private String name;

    private String ip;

    private String port;

    private String applicationURL;

    private boolean isCurrent;

    private boolean isHttps;

    public ClusterInfo() {

    }

    public ClusterInfo(String name, String ip, String appUrl, String port, boolean isCurrent, boolean isHttps) {
        this.name = name;
        this.applicationURL = appUrl;
        this.ip = ip;
        this.port = port;
        this.isCurrent = isCurrent;
        this.isHttps = isHttps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getApplicationURL() {
        return applicationURL;
    }

    public void setApplicationURL(String applicationURL) {
        this.applicationURL = applicationURL;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

    public boolean isHttps() {
        return isHttps;
    }

    public void setHttps(boolean isHttps) {
        this.isHttps = isHttps;
    }

}
