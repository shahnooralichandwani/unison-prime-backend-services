package com.avanza.core.constants;

public enum TemplateType {

    Attachment("Attachment"), EMail("E-Mail"), Fax("FAX"), SMS("SMS"), SocialMedia("SocialMedia"), Signature("Signature");

    private String value;

    private TemplateType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return value;
    }
}
