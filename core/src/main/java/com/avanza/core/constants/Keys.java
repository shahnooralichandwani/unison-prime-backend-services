package com.avanza.core.constants;

public interface Keys {

    public interface ViewRoot {
        String ROLES_LIST = "rolesList";
        String USER_STATUS_LIST = "userStatusList";
        String TEMPLATE_TYPE_LIST = "templateTypeList";
        String SEARCHED = "Searched";
        String GRID = "grid";
        String PREVIOUS_SEARCH = "preSearch";
    }

    public interface Request {
        String MODULE_ID = "module_id";
        String LIST_ADVANCE_SEARCH = "list_adv_opt";
    }

    public interface ComponentAttributes {
        String ADVANCE_SEARCH_ID = "advanceSearchId";
        String ADVANCE_SEARCH_DETAIL_ATTRIB_ID = "advanceSearchAttribId";
    }
}
