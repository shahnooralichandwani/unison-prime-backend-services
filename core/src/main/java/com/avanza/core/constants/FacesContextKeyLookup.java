package com.avanza.core.constants;

/**
 * This interface is used to maintain key constants against which respective Objects are stored in faces context.
 */
public enum FacesContextKeyLookup {

    BindingControllerKey("bindingController"), PageCount("PageCount"), CurrentPageNumber("CurrentPageNumber"), PageSize("PageSize"), PageDirection(
            "PageDirection"), PageInfo("PageInfo"), ResultSetCount("PageResultCount"), DynaControls("dynaControls_"), SelectedEmail("SelectedEmail"), ReplyEmail(
            "ReplyEmail"), EmailSearchCriteria("emailSearchCriteria"), ModuleId("moduleId"), SelectedTemplate("SelectedTemplate"), TemplateSearchCriteria(
            "TemplateSearchCriteria"), ScriptAdminSearchCriteria("ScriptAdminSearchCriteria"), SelectedScript("SelectedScript"), SelectedRole(
            "SelectedRole"), OrgSearchCriteria("OrgSearchCriteria"), SelectedOrgUnit("SelectedOrgUnit"), SelectedActivityType("SelectedActivityType"), SdoDataValueMap(
            "DATA_OBJECT_VALUE_MAP"), SdoCollection("DATA_OBJECT_COLLECTION"), PagingFastForwardButton("fastForwardButton"), PagingFastBackwardButton(
            "fastBackwardButton"), SelectedValueTree("SelectedValueTree"), CachedData("CachedData"), RecordSetNumber("recordSetNumber"), SelectedDataSource(
            "SelectedDataSource"), DataSourceSearchCriteria("DataSourceSearchCriteria"), ReferenceSearchCriteria("ReferenceSearchCriteria");

    private String value;

    FacesContextKeyLookup(String val) {
        this.value = val;
    }

    public String toString() {
        return this.value;
    }
}
