package com.avanza.core.constants;

import com.avanza.core.CoreException;
import com.avanza.core.security.User;


public enum ThreadContextLookup {

    ModuleId("ModuleId"), ActivityOperationType("ActivityOperationType"), ActivityDescription("ActivityDescription"), Current_User(User.class.getName()), Current_DataObject("Current_DataObject"),
    Current_MetaEntity("Current_MetaEntity"), ProductCode("Product_Code"), ProductMetaEntity("Product_MetaEntity"), ProductCategory("Product_Category"), ProductCategoryChild("Product_Category_Child"), DbErrorMesssage("DbErrorMessage"),
    ExternalResponseCode("ExternalResponseCode"), SystemName("SystemName"), ResponseMessage("ResponseMessage"), Indicator("Indicator"), CNIC("CNIC"), CustomerName("CustomerName"), InfoMessage("[Info]"), SuccessCode("00");


    private String value;

    private ThreadContextLookup(String val) {
        this.value = val;
    }

    public String getValue() {
        return this.value;
    }

    public static ThreadContextLookup fromString(String value) {

        ThreadContextLookup retVal;

        if (value.equalsIgnoreCase("ModuleId"))
            retVal = ThreadContextLookup.ModuleId;
        if (value.equalsIgnoreCase("SystemName"))
            retVal = ThreadContextLookup.SystemName;
        if (value.equalsIgnoreCase("CNIC"))
            retVal = ThreadContextLookup.CNIC;
        if (value.equalsIgnoreCase("CustomerName"))
            retVal = ThreadContextLookup.CustomerName;
        else if (value.equalsIgnoreCase("ActivityOperationType"))
            retVal = ThreadContextLookup.ActivityOperationType;
        else if (value.equalsIgnoreCase("ActivityDescription"))
            retVal = ThreadContextLookup.ActivityDescription;
        else if (value.equalsIgnoreCase(User.class.getName()))
            retVal = ThreadContextLookup.Current_User;
        else if (value.equalsIgnoreCase("Current_DataObject"))
            retVal = ThreadContextLookup.Current_DataObject;
        else if (value.equalsIgnoreCase("Current_MetaEntity"))
            retVal = ThreadContextLookup.Current_MetaEntity;
        else if (value.equalsIgnoreCase("Product_Code"))
            retVal = ThreadContextLookup.ProductCode;
        else if (value.equalsIgnoreCase("Product_MetaEntity"))
            retVal = ThreadContextLookup.ProductMetaEntity;
        else if (value.equalsIgnoreCase("DbErrorMesssage"))
            retVal = ThreadContextLookup.DbErrorMesssage;
        else if (value.equalsIgnoreCase("Product_Category"))
            retVal = ThreadContextLookup.ResponseMessage;
        else if (value.equalsIgnoreCase("Response_Message"))
            retVal = ThreadContextLookup.ProductCategory;
        else if (value.equalsIgnoreCase("Product_Category_Child"))
            retVal = ThreadContextLookup.ProductCategoryChild;
        else if (value.equalsIgnoreCase("InfoMessage"))
            retVal = ThreadContextLookup.InfoMessage;
        else if (value.equalsIgnoreCase("SuccessCode"))
            retVal = ThreadContextLookup.SuccessCode;
        else
            throw new CoreException("[%1$s] is not recognized as valid ThreadContextLookup", value);

        return retVal;
    }
}
