package com.avanza.core.constants;

/**
 * This interface is used to maintain key constants against which respective
 * Objects are stored in Session.
 */
public interface SessionKeyLookup {

    /**
     * Customer object whose session is under progress
     */
    String PRODUCT_ENTITY_NUM = "Product_Entity_Num";
    String CURRENT_CUSTOMER_KEY = "Current_Customer";
    String CURRENT_CUSTOMER_RIM = "Current_Customer_RIM";
    String CURRENT_CUSTOMER_TYPE = "Current_Customer_Type";
    String CURRENT_CUSTOMER_ID_KEY = "Current_Customer_Id";
    String CURRENT_CUSTOMER_NAME = "Current_Customer_Name";
    String UserCustomerCache = "UserCustomerCache";
    String PREVIOUS_VIEW_ID = "Prev_View_Id";
    String PREVIOUS_META_ENT_ID = "Prev_Meta_Id";
    String SESSION_INPUT_MAP = "InputMap";
    String IVR_CALL_ERROR = "IVRCALLERROR";
    String CURRENT_VALIDATE_SCRIPT = "Current_Validate_Script";
    String IS_OUTBOUND_MODULE = "IsOutboundModule";
    String MASKED_CONTROLS = "MaskedControls";
    String EDITABLE_CONTROLS = "EditableControls";
    String CURRENT_WORKFLOW_STATE = "CurrentWorkflowState";
    String SYSTEM_NAME = "SystemName";
    String PRODUCT_NAME = "Product_Name";

    String TRAN_RESPONSE_CODE = "Tran_Response_Code";

    String PROD_ID = "Prod_Id";
    String DEFAULT_EXPRESSION = "Default_Expression";

    String TREE_NODE_ID = "Tree_Node_ID";

    String ACCT_TYPE = "Acct_Type";
    String ACCT_CURRENCY = "Acct_Currency";
    String BANK_IMD = "Bank_IMD";

    String SMS_BODY = "Sms_Body";
    String SUCCESS = "Success";
    String FAILURE = "Failure";

    String ORG_UNIT_ID = "Org_Unit_Id";
    /**
     * Currently logged in User
     */
    String CURRENT_USER_KEY = "Current_User";
    String CURRENT_USER_LOGIN = "Current_User_LoginID";
    /**
     * Current Locale
     */
    String CurrentLocale = "CurrentLocale";

    /**
     * Customer Session History Objects.
     */
    String CUSTOMER_SESSION_HISTORY = "Current_CustomerSessionHistory";
    String CUSTOMER_session_historyDetail = "Current_CustomerSessionHistoryDetail";
    String CUSTOMER_SESSION_ISVALIDATE = "Current_Customer_isValid";
    String CUSTOMER_SESSION_HISTORY_TEMP = "Current_CustomerSessionHistoryTemp";

    /**
     * Current Interface Channel
     */
    String CURRENT_INTERFACE_CHANNEL = "Current_InterfaceChannel";

    /**
     * For tracing the audit trail
     */
    String ACTIVITY_LOG_KEY = "Current_ActivityLog";
    String ACTIVITY_LOG_DETAIL_KEY = "Current_ActivityLogDetail";

    /**
     * Current Contact List
     */
    String CURRENT_CONTACT = "Current_Contact";

    String CURRENT_TASK = "Current_Task";
    String CURRENT_MESSAGE = "Current_Message";

    /**
     * Print Service Lookups
     */
    String Statement_Transactions = "StatementTrxs";
    String Statement_from_date = "StatementFromDate";
    String Statement_to_date = "StatementToDate";
    String Loan_Payment_Schedule = "LoanPaymentSchedule";
    String DATA_MAP_FOR_TEMPLATE = "DATA_MAP_FOR_TEMPLATE";

    /* FEL @[Variable] */
    String CURRENT_OBJ = "currentObj";
    String CURRENT_OBJ_STATE = "currentObjState";
    String VIWE_CURRENT_OBJ = "viewCurrentObj";

    /** Added by KMA */
    /**
     * Is Current module customer profile
     */
    String IS_INBOUND_MODULE = "IsInboundModule";
    String SELECTED_PRODUCT = "selectedLeadProduct";
    String SelectedCampaign = "SelectedCampaign";
    String SelectedCampaignCustomer = "SelectedCampChannelCustomer";

    /**
     * Shahbaz: Campaign Outbound Session
     */
    String CAMPAIGN_OUTBOUND_SESSION = "Campaign_Outbound_Session";

    String CURRENT_MODULE = "current_module";
    String CUST_MOBILE_NUM = "Cust_Mobile_Num";

    // Added by Anam Siddiqui
    String COMBO_VALUE = "Combo_Value";
    // Added by Naushad Qamar
    String ADVANCE_SEARCH_DETAIL = "Current_AdvanceSearchDetail";
    String CURRENT_ADVANCE_SEARCH_ID = "Current_AdvanceSearchId";
    String CURRENT_ADVANCE_SEARCH_STATE = "Current_AdvanceSearchState";
    String ADVANCE_SEARCH_RENDERER = "AdvanceSearchRenderer";
    String CURRENT_ADVANCE_SEARCH_ENTITY_ID = "Current_AdvanceEntityId";

    String SESSION_DATA_MAP = "SESSION_DATA_MAP";
    String PRESAVE_TASKS = "PRESAVE_TASKS";
    String STRATEGIC_ACTIONS = "STRATEGIC_ACTIONS";
    String EVENT_SAVE_CURRENT_DOC_SAVE = "EVENT_SAVE_CURRENT_DOC_SAVE";
    String EVENT_REMOVE_ON_CURRENT_DOC_REMOVE = "EVENT_REMOVE_ON_CURRENT_DOC_REMOVE";
    String EVENT_REMOVE_ON_USER_LOG_OUT = "EVENT_REMOVE_ON_USER_LOG_OUT";

    // Refresh Patch
    String CHILD_RESPONSE_MESSAGES = "Child_Response_Messages";
    String FROM_JSP = "from_jsp";

}
