package com.avanza.core.constants;

/**
 * This interface is used to assign Id's to Components used through out in application
 *
 * @author Khawaja Muhammad Aamir
 */
public interface ComponentID {
    /**
     * Customer Search By RIM
     */
    final String CUSTOMER_SEARCH_RIM = "Customer_Search_By_RIM";
    final String CUSTOMER_SEARCH_RIM_HIDDEN_BUTTON = "Customer_Search_Hidden_Button";
}
