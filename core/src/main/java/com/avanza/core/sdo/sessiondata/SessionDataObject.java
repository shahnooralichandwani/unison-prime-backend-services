package com.avanza.core.sdo.sessiondata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.avanza.core.sdo.DataObject;

public class SessionDataObject {
    private DataObject dataObject;
    private String saveEvent;
    private String removeEvent;
    private String displayMessage;
    private String groupName;
    private boolean isRemoveFromSession = false;

    private Map<String, String> requiredParmList = new HashMap<String, String>();
    private List<String> removeSessionParmList = new ArrayList<String>();


    public SessionDataObject(DataObject dataObject) {
        this.dataObject = dataObject;
    }


    public String getGroupName() {
        return groupName;
    }


    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }


    public String getSaveEvent() {
        return saveEvent;
    }


    public void setSaveEvent(String saveEvent) {
        this.saveEvent = saveEvent;
    }


    public String getRemoveEvent() {
        return removeEvent;
    }


    public void setRemoveEvent(String removeEvent) {
        this.removeEvent = removeEvent;
    }


    public String getDisplayMessage() {
        return displayMessage;
    }


    public void setDisplayMessage(String displayMessage) {
        this.displayMessage = displayMessage;
    }


    public DataObject getDataObject() {
        return dataObject;
    }

    public Map<String, String> getRequiredParmMap() {
        return requiredParmList;
    }

    public void setRequiredParmList(Map<String, String> requiredParmList) {
        this.requiredParmList = requiredParmList;
    }


    public boolean getIsRemoveFromSession() {
        return isRemoveFromSession;
    }


    public void setIsRemoveFromSession(boolean isRemoveFromSession) {
        this.isRemoveFromSession = isRemoveFromSession;
    }


    public List<String> getRemoveSessionParmList() {
        return removeSessionParmList;
    }


    public void setRemoveSessionParmList(List<String> removeSessionParmList) {
        this.removeSessionParmList = removeSessionParmList;
    }


    public Map<String, String> getRequiredParmList() {
        return requiredParmList;
    }

}
