package com.avanza.core.sdo;

import com.avanza.core.meta.MetaAttribute;
import com.avanza.core.util.*;

import java.io.Serializable;
import java.util.Date;

public class Attribute implements java.lang.Comparable<Attribute>, Serializable {

    private static final long serialVersionUID = -3759428479638482045L;

    private Object value;

    private MetaAttribute metaAttrib;

    public Attribute(MetaAttribute metaAttrib, Object value) {

        this.metaAttrib = metaAttrib;
        if (value != null)
            this.value = ConvertUtil.ConvertTo(value, this.metaAttrib.getType().getJavaType());
        else
            this.value = null;
    }

    public Object getValue() {

        return this.value;
    }

    public void setValue(Object value) {
        if (value != null && value instanceof String) {
            // These are commented due to Arabic characters. Arabic characters are not saved correctly.
    		/*value = StringEscapeUtils.escapeHtml(value.toString());
    		value =  StringEscapeUtils.escapeJavaScript(value.toString());*/
            value = EscapeUtil.getEscapeValue(value.toString());
        }


        if (value == null) {
            this.value = value;
            return;
        }

        // TODO apply constraints check if any

        this.value = ConvertUtil.ConvertTo(value, this.metaAttrib.getType().getJavaType());
    }

    public boolean isPrimary() {

        return this.metaAttrib.getIsPrimary();
    }

    public void clear() {

        // TODO get default value from meta attribute
        //this.metaAttrib.getDefValue();
        throw new DataException("Attribute.clear() is not implemented");
    }

    // TODO Need to implement
    public boolean isValid() {
        throw new DataException("Attribute.isValid() is not implemented");
    }

    public boolean isNull() {

        return (this.value == null);
    }

    public boolean equals(Object value) {

        try {
            value = ConvertUtil.ConvertTo(value, this.metaAttrib.getType().getJavaType());
        } catch (ConvertException e) {
            return false;
        }

        return this.value.equals(value);
    }

    public String toString() {

        if (this.value == null)
            return StringHelper.EMPTY;

        return this.value.toString();
    }

    public boolean booleanValue() {
        return Convert.toBoolean(this.value);
    }

    public int intValue() {
        return Convert.toInteger(this.value);
    }

    public byte byteValue() {
        return Convert.toByte(this.value);
    }

    public short shortValue() {
        return Convert.toShort(this.value);
    }

    public long longValue() {
        return Convert.toLong(this.value);
    }

    public float floatValue() {
        return Convert.toFloat(this.value);
    }

    public double doubleValue() {
        return Convert.toDouble(this.value);
    }

    public Date dateValue() {
        return Convert.toDate(this.value);
    }

    public char charValue() {
        return Convert.toChar(this.value);
    }

    public void setValue(boolean value) {
        this.setValue(new Boolean(value));
    }

    public void setValue(byte value) {
        this.setValue(new Byte(value));
    }

    public void setValue(short value) {
        this.setValue(new Short(value));
    }

    public void setValue(int value) {
        this.setValue(new Integer(value));
    }

    public void setValue(long value) {
        this.setValue(new Long(value));
    }

    public void setValue(float value) {
        this.setValue(new Float(value));
    }

    public void setValue(double value) {
        this.setValue(new Double(value));
    }

    public void setValue(char value) {
        this.setValue(new Character(value));
    }

    public void setValue(String value) {
        this.setValue((Object) value);
    }

    public void setValue(Date value) {
        this.setValue((Object) value);
    }

    public MetaAttribute getMetaAttrib() {
        return this.metaAttrib;
    }

    protected void setMetaAttrib(MetaAttribute metaAttrib) {
        this.metaAttrib = metaAttrib;
    }

    @Override
    protected Object clone() {
        return new Attribute(this.metaAttrib, this.value);
    }

    public int compareTo(Attribute obj) {
        throw new DataException("Attribute.compareTo(Attribute) is not implemented");
    }
}
