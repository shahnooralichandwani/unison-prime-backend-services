package com.avanza.core.sdo.expression;

import java.util.ArrayList;

import com.avanza.core.meta.AttributeType;
import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;

public class Expression {

    private ExpressionType type;
    private String text;
    private ArrayList<Expression> expList;
    private AttributeType dataType;


    public Expression(String text, ExpressionType type) {

        this.init(text, type, AttributeType.None);
    }

    public Expression(String text, ExpressionType type, AttributeType dataType) {

        this.init(text, type, dataType);
    }

    private void init(String text, ExpressionType type, AttributeType dataType) {

        this.type = type;
        this.setText(text);
        this.dataType = dataType;
        this.expList = new ArrayList<Expression>(4);
    }

    public ExpressionType getType() {
        return this.type;
    }

    void setType(ExpressionType type) {
        this.type = type;
    }

    public AttributeType getDataType() {
        return this.dataType;
    }

    void setDataType(AttributeType dataType) {
        this.dataType = dataType;
    }

    public String getText() {
        return this.text;
    }

    void setText(String text) {

        Guard.checkNull(text, "Expression.setText()");

        text = text.trim();
        this.text = text;

        if (StringHelper.isEmpty(text)) return;

        int endIdx = text.length() - 1;

        if ((this.type == ExpressionType.Constant) && ((text.charAt(0) == '\'') && (text.charAt(0) == '\''))
                || ((text.charAt(endIdx) == '"') && (text.charAt(endIdx) == '"'))) {

            this.text = text.substring(1, endIdx - 1);
        }

    }

    public int size() {
        return this.expList.size();
    }

    public Expression get(int index) {
        return this.expList.get(index);
    }

    public void set(int index, Expression element) {
        this.expList.set(index, element);
    }

    public void add(Expression element) {
        if (this.type == ExpressionType.Function) this.expList.add(element);
    }

    public void remove(Expression element) {
        this.expList.remove(element);
    }

    public Expression remove(int idx) {
        return this.expList.remove(idx);
    }

    public String toString() {
        return this.text;
    }

    public boolean hasChildExpressions() {
        if (this.expList.get(0).getType() == ExpressionType.Function)
            return true;
        else
            return false;
    }
}
