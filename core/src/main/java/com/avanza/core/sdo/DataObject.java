package com.avanza.core.sdo;

import com.avanza.core.CoreException;
import com.avanza.core.cache.Cacheable;
import com.avanza.core.cache.RemoveReason;
import com.avanza.core.constants.FacesContextKeyLookup;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.constants.ThreadContextLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.db.sql.SqlBuilder;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.ObjectCriterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.data.validation.ValidationContext;
import com.avanza.core.data.validation.ValidationInfo;
import com.avanza.core.data.validation.ValidatorResultType;
import com.avanza.core.draft.Draft;
import com.avanza.core.draft.DraftService;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.json.JsonKeyLookup;
import com.avanza.core.json.JsonParser;
import com.avanza.core.meta.*;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.core.serialized.SerializedAttribute;
import com.avanza.core.util.Convert;
import com.avanza.core.util.Guard;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.config.ViewContext;
import com.avanza.core.web.config.WebContext;
import com.avanza.integration.dbmapper.meta.DataAccessMode;
import com.avanza.integration.transaction.TransactionPadBroker;
import com.avanza.ui.meta.MetaView;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.xerces.dom.DocumentImpl;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.IntegerType;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.*;
import java.util.Map.Entry;


public class DataObject implements java.lang.Cloneable, java.lang.Comparable<DataObject>, Serializable, Cacheable {

    private static final long serialVersionUID = 797285879343190081L;

    private static final Logger logger = Logger.getLogger(DataObject.class);

    public static final String REF_ENTITY_ID = "Ref_Entity_Id";

    // fields
    private HashMap<String, Attribute> attribList = new HashMap<String, Attribute>();// By
    // system
    // Name
    private HashMap<String, Attribute> attribIdMap = new HashMap<String, Attribute>();// By
    private String sessionId;                                                                            // Id
    // Map
    private HashMap<String, Boolean> isValueInMap = new HashMap<String, Boolean>();

    private HashMap<String, Object> oldValueList;

    private HashMap<String, SerializedAttribute> serializedAttribMap = new HashMap<String, SerializedAttribute>();

    private MetaEntity refMetaEnt;

    private DataState state;

    private DataState oldState;

    private long creationTimeStamp = System.currentTimeMillis();

    private Map<String, DataObjectCollection> relatedObjects = new Hashtable<String, DataObjectCollection>();
    private DataAccessMode dataAccessMode;

    private boolean isSerializationRequired = false;
    private boolean isSerialized = false;
    private String draftTicketNum;

    public Map<String, DataObjectCollection> createRelatedObjects() {
        return relatedObjects;
    }

    public DataState getState() {
        return state;
    }

    private DataObject() {
        this.dataAccessMode = DataAccessMode.Detail;
    }

    public DataObject(MetaEntity refMetaEnt) {

        Guard.checkNull(refMetaEnt, "DataObject.DataObject(MetaEntity)");
        this.refMetaEnt = refMetaEnt;
        this.state = DataState.New;
        this.oldState = DataState.New;
        this.dataAccessMode = DataAccessMode.Detail;
        this.prepareEntity();

    }

    public DataObject(MetaEntity refMetaEnt, HashMap<String, Object> values, DataAccessMode accessMode) {

        Guard.checkNull(refMetaEnt, "DataObject.DataObject(MetaEntity,HashMap)");
        this.refMetaEnt = refMetaEnt;
        this.state = DataState.Unchanged;
        this.oldState = DataState.Unchanged;
        this.dataAccessMode = accessMode;
        this.loadEntity(values);

    }

    public DataObject(Map<String, Object> values) {

        String refEntityId = (String) values.get("$type$");
        Guard.checkNull(refEntityId, "DataObject.DataObject(HashMap) for refEntityId.");
        this.refMetaEnt = MetaDataRegistry.getEntityBySystemName(refEntityId); // REFACTORED
        this.state = DataState.Unchanged;
        this.oldState = DataState.Unchanged;
        this.dataAccessMode = DataAccessMode.Detail;
        this.loadEntity(values);

    }


    public DataObject(MetaEntity refMetaEnt, HashMap<String, Object> values, DataAccessMode accessMode, DataState dataState) {

        Guard.checkNull(refMetaEnt, "DataObject.DataObject(MetaEntity,HashMap)");
        this.refMetaEnt = refMetaEnt;
        this.state = dataState;
        this.oldState = dataState;
        this.dataAccessMode = accessMode;
        this.loadEntity(values);
    }

    public String getIdValue() {

        MetaAttribute meta = this.refMetaEnt.getPrimaryKeyAttribute();
        return (this.getValue(meta) != null) ? this.getValue(meta).toString() : null;
    }

    public String getUniqueId() {

        return String.format("%1$s : %2$s", this.refMetaEnt.getId(), this.getIdValue());
    }

    public String getProcessCode() {

        try {
            return getAsString("PROCESS_CODE");
        } catch (DataException e) {
            return null;
        }
    }

    public Date getStateEntryTime() {

        try {
            return getAsDate("STATE_ENTRY_TIME");
        } catch (Exception e) {
            return new Date(0);
        }
    }

    public Date getProcessStartTime() {

        try {
            return getAsDate("PROCESS_START_TIME");
        } catch (Exception e) {
            return new Date(0);
        }
    }

    public String getCurrentOrgUnit() {

        try {
            return getAsString("CURRENT_ORG_UNIT");
        } catch (DataException e) {
            return null;
        }
    }

    public String getProductMetaEntity() {

        try {
            return getAsString("PRODUCT_ENTITY_ID");
        } catch (DataException e) {
            return null;
        }
    }

    public String getProductCode() {

        try {
            return getAsString("PRODUCT_CODE");
        } catch (DataException e) {
            return null;
        }
    }

    public Object getValue(String name, DataVersion version) {

        this.checkAttributeExist(name);
        return this.innerGetValue(name, version);
    }

    public Object getValue(String name) {
        return this.getValue(name, DataVersion.Current);
    }

    public Object getValue(MetaAttribute meta, DataVersion version) {

        Guard.checkNull(meta, "DataObject.getValue(MetAttribute, DataVersion)");
        return this.getValue(meta.getId(), version);
    }

    public Object getValue(MetaAttribute meta) {

        return this.getValue(meta.getSystemName(), DataVersion.Current);
    }

    public Map<String, Object> getValues(DataVersion version) {

        Iterator<String> itr = this.attribList.keySet().iterator();
        Map<String, Object> map = this.innerGetValues(itr, this.attribList.size(), version);
        return map;
    }

    public Map<String, Object> getValues() {
        return this.getValues(DataVersion.Current);
    }

    public Map<String, Object> getValues(List<String> nameList, DataVersion version) {

        Guard.checkNull(nameList, "DataObject.getValues(List<String>, DataVersion)");
        return this.innerGetValues(nameList.iterator(), nameList.size(), version);
    }

    public Map<String, Object> getValues(List<String> nameList) {

        Guard.checkNull(nameList, "DataObject.getValues(List<String>)");
        return this.innerGetValues(nameList.iterator(), nameList.size(), DataVersion.Current);
    }

    public Map<String, Object> getValue(MetaEntity metaEnt) {

        //TODO:Farhan
        //this.checkIsSubclass(metaEnt.getId());
        return this.getValues(metaEnt.getAttributeNames());
    }

    // type conversion methods
    public boolean getAsBoolean(String fieldName) {
        return this.innerGetAttribute(fieldName).booleanValue();
    }

    public byte getAsByte(String fieldName) {
        return this.innerGetAttribute(fieldName).byteValue();
    }

    public short getAsShort(String fieldName) {
        return this.innerGetAttribute(fieldName).shortValue();
    }

    public int getAsInteger(String fieldName) {
        return this.innerGetAttribute(fieldName).intValue();
    }

    public long getAsLong(String fieldName) {
        return this.innerGetAttribute(fieldName).longValue();
    }

    public float getAsFloat(String fieldName) {
        return this.innerGetAttribute(fieldName).floatValue();
    }

    public double getAsDouble(String fieldName) {
        return this.innerGetAttribute(fieldName).doubleValue();
    }

    public Date getAsDate(String fieldName) {
        return this.innerGetAttribute(fieldName).dateValue();
    }

    public char getAsCharacter(String fieldName) {
        return this.innerGetAttribute(fieldName).charValue();
    }

    public String getAsString(String fieldName) {

        if (this.innerGetAttribute(fieldName) == null)
            return null;
        else
            return this.innerGetAttribute(fieldName).toString();
    }

    // Methods that set the value of attributes

    public void setValue(String fieldName, Object value) {

        this.innerSetValue(fieldName, value);
    }

    public void setValue(MetaAttribute meta, Object value) {

        Guard.checkNull(meta, "DataObject.setValue(MetaAttribute, value");
        this.innerSetValue(meta.getSystemName(), value);
    }

    public void setValue(Map<String, Object> valueList) {

        Guard.checkNull(valueList, "DataObject.setValue(Map<String,Object>");

        for (Map.Entry<String, Object> item : valueList.entrySet()) {
            this.innerSetValue(item.getKey(), item.getValue());
        }
    }

    public void setValue(String fieldName, boolean value) {
        this.innerSetValue(fieldName, new Boolean(value));
    }

    public void setValue(String fieldName, byte value) {
        this.innerSetValue(fieldName, new Byte(value));
    }

    public void setValue(String fieldName, short value) {
        this.innerSetValue(fieldName, new Short(value));
    }

    public void setValue(String fieldName, int value) {
        this.innerSetValue(fieldName, new Integer(value));
    }

    public void setValue(String fieldName, long value) {
        this.innerSetValue(fieldName, new Long(value));
    }

    public void setValue(String fieldName, float value) {
        this.innerSetValue(fieldName, new Float(value));
    }

    public void setValue(String fieldName, double value) {
        this.innerSetValue(fieldName, new Double(value));
    }

    public void setValue(String fieldName, char value) {
        this.innerSetValue(fieldName, new Character(value));
    }

    public void setValue(String fieldName, String value) {
        this.innerSetValue(fieldName, value);
    }

    public void setValue(String fieldName, Date value) {
        this.innerSetValue(fieldName, value);
    }

    public void resetValue(String fieldName) {

        this.checkAttributeExist(fieldName);

        if (this.state == DataState.New) {
            this.innerGetAttribute(fieldName).clear();
        } else if (this.state == DataState.Modified) {

            if ((this.oldValueList != null) && (this.oldValueList.containsKey(fieldName))) {

                Object temp = this.oldValueList.remove(fieldName);
                this.attribList.get(fieldName).setValue(temp);
                if (this.oldValueList.size() == 0)
                    this.state = this.oldState;
            }
        }
    }

    private void acceptChanges() {

        if (this.oldValueList != null)
            this.oldValueList.clear();

        this.oldState = this.state;
        this.state = DataState.Unchanged;
    }

    public void rejectChanges() {

        if ((this.state == DataState.Unchanged))
            return;

        if (this.state == DataState.New)
            for (Attribute attrib : this.attribList.values()) {
                attrib.clear();
            }

        this.state = this.oldState;

        if (this.oldValueList != null) {

            for (Map.Entry<String, Object> item : this.oldValueList.entrySet()) {

                this.attribList.get(item.getKey()).setValue(item.getValue());
            }

            this.oldValueList.clear();
        }
    }

    public boolean equals(Object obj) {

        if (obj == null || !(obj instanceof DataObject))
            return false;
        DataObject dataObj = (DataObject) obj;
        if (dataObj.getMetaEntity() != this.getMetaEntity())
            return false;
        if (dataObj.attribList != this.attribList)
            return false;
        if (dataObj.attribIdMap != this.attribIdMap)
            return false;
        if (dataObj.relatedObjects != this.relatedObjects)
            return false;
        if (dataObj.state != this.state)
            return false;
        return true;
    }

    public boolean isNull(String fieldName) {

        return (this.attribList.get(fieldName) == null && this.attribIdMap.get(fieldName) == null);
    }

    public boolean isModified() {

        return (this.state != this.oldState);
    }

    public boolean isNew() {

        return (this.state == DataState.New);
    }

    public MetaEntity getMetaEntity() {
        return this.refMetaEnt;
    }

    public MetaEntity getNonAbstractParentEntity() {
        return MetaDataRegistry.getNonAbstractParentEntity(this.refMetaEnt);
    }


    public int compareTo(DataObject obj) {

        if (this.refMetaEnt != obj.refMetaEnt)
            return -1;

        return this.getIdValue().compareTo(obj.getIdValue());
    }

    @SuppressWarnings("unchecked")
    public Object clone() {

        DataObject clone = new DataObject();
        clone.refMetaEnt = this.refMetaEnt;

        clone.attribList = (HashMap<String, Attribute>) this.attribList.clone();
        clone.attribIdMap = (HashMap<String, Attribute>) this.attribIdMap.clone();
        if (this.oldValueList != null)
            clone.oldValueList = (HashMap<String, Object>) this.oldValueList.clone();

        clone.state = this.state;
        clone.oldState = this.oldState;

        clone.relatedObjects = this.relatedObjects;

        return clone;
    }

    public void save() {
        ApplicationContext.getContext().add("Data_Object", this);
        Collection<Attribute> attribCollection = attribList.values();

        // validate();
		
		/*Iterator<Attribute> attribIterator =  attribCollection.iterator();
		attribIterator.*/

        for (Attribute attrib : attribCollection) {
            MetaAttribute metaAttribute = attrib.getMetaAttrib();

            //add counter values
            if (this.state == DataState.New && metaAttribute.getCounterName() != null && attrib.getValue() == null) {
                attrib.setValue(MetaDataRegistry.getNextCounterValue(metaAttribute.getCounterName()));
            }

            if (metaAttribute.getScope() == AttributeScope.FirstTimeEdit
                    || metaAttribute.getScope() == AttributeScope.ReadOnly) {
                if (this.state == DataState.New) {
                    String calcExpression = metaAttribute.getCalculateExp();
                    if (!StringHelper.isEmpty(calcExpression)) {
                        Object value = null;
                        value = ExpressionHelper.getExpressionResult(calcExpression, Object.class);
                        attrib.setValue(value);
                    }
                }
            }

            //Shuwair
            if (metaAttribute.getIsSerialized() && metaAttribute.getSerializationType() != null && metaAttribute.getSerializationType().equalsIgnoreCase(JsonKeyLookup.JSON_SERIALIZATION)) {
                serializedAttribMap.put(attrib.getMetaAttrib().getSystemName(), new SerializedAttribute(attrib));
            }

        }
        //Shuwair
        serializedAttributes();

        DataBroker broker = DataRepository.getBroker(this.refMetaEnt.getSystemName());

        if (this.state == DataState.Deleted) {
            //first related objects need to be removed.
            saveRelatedObjects();
            if (oldState != DataState.New) {
                broker.delete(this);
            }
        } else if (this.state == DataState.New) {
            broker.add(this);
            acceptChanges();
            saveRelatedObjects();
        } else if (this.state == DataState.Modified) {
            broker.update(this);
            acceptChanges();
            saveRelatedObjects();
        } else if (this.state == DataState.Unchanged) {
            //children can be found modified.
            saveRelatedObjects();
            //New condition added for Transactions only as for transactions there is no need to call saveRelatedObjects()
        } else if (this.state == DataState.Transaction) {
            broker.add(this);
        }
        try {
            closeDraft();
        } catch (Exception e) {
            logger.logError(e.getMessage());
        }


    }

    private void saveRelatedObjects() {
        for (String relationName : relatedObjects.keySet()) {
            /******************** Separate Save as draft START **************/
            if (relationName.equals("EformDetailRel")) {
                if (attribList.get("JSONData") != null) {
                    Attribute jsonAttribute = attribList.get("JSONData");
                    String jsonStr = (String) jsonAttribute.getValue();
                    logger.logInfo("serialize json string from EformDetailRel:" + jsonStr);
                    this.deSerializedAttrubutes(jsonStr + "[{$postfix}]");

                    logger.logInfo("Data Broker loading for Unison.Document.Eform.JsonDetail");
                    DataBroker broker = DataRepository.getBroker("Unison.Document.Eform.JsonDetail");
                    logger.logInfo("Data Broker loaded for Unison.Document.Eform.JsonDetail");

                    DataObject tmpObj = new DataObject(MetaDataRegistry.getEntityBySystemName("Unison.Document.Eform.JsonDetail"));

                    //TODO: UNISONPRIME
                    /*UserDbImpl user = (UserDbImpl) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(
                            SessionKeyLookup.CURRENT_USER_KEY);*/

                    UserDbImpl user = new UserDbImpl();

                    tmpObj.setValue("JSONData", jsonStr);
                    tmpObj.setValue(this.getNonAbstractParentEntity().getRelationshipByName(relationName).getSubEntityKey(), this.getValue(this.getNonAbstractParentEntity().getRelationshipByName(relationName).getMainEntityKey()));
                    tmpObj.setValue("UPDATED_ON", Calendar.getInstance().getTime());
                    tmpObj.setValue("UPDATED_BY", user.getLoginId());
                    tmpObj.setValue("CREATED_ON", Calendar.getInstance().getTime());
                    tmpObj.setValue("CREATED_BY", user.getLoginId());

                    broker.add(tmpObj);

                }
            }
            /******************** Separate Save as draft END **************/
            DataObjectCollection dataObjectCollection = relatedObjects.get(relationName);

            Iterator<DataObject> iterator = dataObjectCollection.iterator();
            while (iterator.hasNext()) {
                DataObject dataObject = iterator.next();
                dataObject.setValue(this.getNonAbstractParentEntity().getRelationshipByName(relationName).getSubEntityKey(), this.getValue(this.getNonAbstractParentEntity().getRelationshipByName(relationName).getMainEntityKey()));
                dataObject.save();
                if (dataObject.state == DataState.Deleted) {
                    iterator.remove();
                }
            }
        }
    }

    public DataObjectCollection findByTransactionPad() {
        DataBroker broker = DataRepository.getBroker(this.refMetaEnt.getSystemName());

        if (!(broker instanceof TransactionPadBroker))
            return null;

        return ((TransactionPadBroker) broker).findBy(this);
    }

    public void markDelete() {
        this.oldState = this.state;
        this.state = DataState.Deleted;
    }

    public void setAttribute(String attributeSystemName, Object attrib) {
        attribList.put(attributeSystemName, (Attribute) attrib);
        attribIdMap.put(((Attribute) attrib).getMetaAttrib().getId(), (Attribute) attrib);
    }

    @Override
    public int hashCode() {
        return this.getUniqueId().hashCode() + attribList.hashCode();
    }

    private void checkAttributeExist(String name) {

        Guard.checkNullOrEmpty(name, "DataObject.checkAttributeExist(String)");

        if (!this.attribIdMap.containsKey(name) && !this.attribList.containsKey(name)) {

            throw new DataException("[%1$s] is not defined in Meta Entity [%2$s]", name, this.refMetaEnt
                    .getSystemName());
        }
    }

    private void checkIsSubclass(String name) {

        if (!this.refMetaEnt.isSubclass(name)) {

            throw new DataException("[%1$s] is not a identified as parent of Meta Entity [%2$s]", name, this.refMetaEnt
                    .getSystemName());
        }
    }

    private Object innerGetValue(String name, DataVersion version) {

        if ((version == DataVersion.Original) && (this.state == DataState.Modified)
                && (this.oldValueList.containsKey(name)))
            return this.oldValueList.get(name);

        if ((this.isValueInMap.get(name) != null && this.isValueInMap.get(name).booleanValue())
                || this.dataAccessMode.equals(DataAccessMode.Detail))
            return this.innerGetAttribute(name).getValue();
        else {
            DataBroker broker = DataRepository.getBroker(this.refMetaEnt.getSystemName());
            DataObject dbObj = broker.findById(this.refMetaEnt.getSystemName(), this.getIdValue());
            this.dataAccessMode = DataAccessMode.Detail;
            if (dbObj != null) {
                this.copyFrom(dbObj, true);
            }
            return this.innerGetAttribute(name).getValue();
        }
    }

    private Map<String, Object> innerGetValues(Iterator<String> itr, int size, DataVersion version) {

        HashMap<String, Object> result = new HashMap<String, Object>(size);

        while (itr.hasNext()) {
            String key = itr.next();
            result.put(key, this.innerGetValue(key, version));
        }

        return result;
    }

    private void addIntoRelatedObjects(String systemName, List<Map<String, Object>> list) {
        List<DataObject> objects = new ArrayList<DataObject>();
        MetaEntity subEntity = MetaDataRegistry.getEntityBySystemName(systemName);
        DataObject object = null;
        try {
            for (Map<String, Object> map : list) {
                object = new DataObject(subEntity);
                object.setValue(map);
                objects.add(object);
            }
            String relationshipId = this.getNonAbstractParentEntity().getRelationShipBySubEntity(subEntity.getId()).get(0).getRelationshipId();
            DataObjectCollection collection = this.relatedObjects.get(relationshipId);
            collection.addAll(objects);
        } catch (Exception e) {
            logger.LogException(e.getMessage(), e);
        }

    }

    private void innerSetValue(String name, Object value) {


        if (value instanceof List<?>) {
            addIntoRelatedObjects(name, (List<Map<String, Object>>) value);
            return;
        }

        Attribute temp = this.innerGetAttribute(name);

        Object oldValue = temp.getValue();

        if (oldValue != null && oldValue.equals(value))
            return;

        if ((this.state != DataState.New) && temp.isPrimary())
            throw new DataException(
                    "Value of Primary key [Field: %1$s] cannot be modified for persisted object. Entity = %2$s", name,
                    this.getUniqueId());

        temp.setValue(value);
        if (value != null) {
            this.isValueInMap.put(name, Boolean.TRUE);
            this.isValueInMap.put(temp.getMetaAttrib().getSystemName(), Boolean.TRUE);
        }

        // check if its a new object
        if ((this.state == DataState.New) || (this.state == DataState.Deleted))
            return;

        // loaded from the database
        this.state = DataState.Modified;

        if (this.oldValueList == null)
            this.oldValueList = new HashMap<String, Object>();

        this.oldValueList.put(name, oldValue);
    }

    private Attribute innerGetAttribute(String name) {

        this.checkAttributeExist(name);

        if ((this.isValueInMap.get(name) != null && this.isValueInMap.get(name).booleanValue())
                || this.dataAccessMode.equals(DataAccessMode.Detail)) {
            Attribute attribute = this.attribIdMap.get(name);
            if (attribute == null)
                attribute = this.attribList.get(name);
            return attribute;
        } else {
            DataBroker broker = DataRepository.getBroker(this.refMetaEnt.getSystemName());
            DataObject dbObj = broker.findById(this.refMetaEnt.getSystemName(), this.getIdValue());
            this.dataAccessMode = DataAccessMode.Detail;
            this.copyFrom(dbObj, true);
            return this.innerGetAttribute(name);
        }
    }

    private void prepareEntity() {

        Map<String, MetaAttribute> metaList = this.refMetaEnt.getAttributeList(true);

        for (MetaAttribute item : metaList.values()) {
            Attribute atrib = item.createAttribute();
            this.attribList.put(item.getSystemName(), atrib);
            attribIdMap.put(item.getId(), atrib);
        }
    }

    private void loadEntity(Map<String, Object> values) {

        if (values == null)
            return;
        Map<String, MetaAttribute> metaList = this.refMetaEnt.getAttributeList(true);

        for (MetaAttribute item : metaList.values()) {
            if (item.getIsSerialized())
                isSerializationRequired = true;
            Object value = values.get(item.getSystemName());
            if (value == null)
                value = values.get(item.getId());
            Attribute atrib = item.createAttribute(value);
            if (value != null) {
                this.isValueInMap.put(item.getId(), Boolean.TRUE);
                this.isValueInMap.put(item.getSystemName(), Boolean.TRUE);
            }
            this.attribList.put(item.getSystemName(), atrib);
            attribIdMap.put(item.getId(), atrib);
        }
        if (isSerializationRequired && !isSerialized)
            deSerializedAttrubutes();
    }

    @SuppressWarnings("unchecked")
    public static void applyPagination(Map ContextAttributes, Search eform, MetaView view, DataBroker broker) {
        SqlBuilder sqlBuilder = SqlBuilder.getInstance(broker.getProviderType());

        ViewContext ctx = ApplicationContext.getContext().get(ViewContext.class);
        if (view != null && view.getPageSize() > 0) {

            Integer pageNumber = (Integer) ctx.getAttribute(FacesContextKeyLookup.CurrentPageNumber.toString());
            Integer pageCount = (Integer) ctx.getAttribute(FacesContextKeyLookup.PageCount.toString());
            Integer pageSize = (Integer) ctx.getAttribute(FacesContextKeyLookup.PageSize.toString());
            Integer pageResultCount = (Integer) ctx.getAttribute(FacesContextKeyLookup.ResultSetCount.toString());

            WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
            Search oldSearch = webCtx.getAttribute("New-Search");
            webCtx.setAttribute("Old-Search", oldSearch);
            webCtx.setAttribute("New-Search", eform);

            String oldQueryString = (oldSearch == null || oldSearch.getRootCriterion() == null) ? "" : oldSearch
                    .getRootCriterion().toString();
            String newQueryString = (eform.getRootCriterion() == null) ? "" : eform.getRootCriterion().toString();
            ;

            if (pageNumber == null || !oldQueryString.equalsIgnoreCase(newQueryString)) {
                Session session = broker.getSession();
                SQLQuery query = session.createSQLQuery(sqlBuilder.preparePageCountQuery(eform));
                query.addScalar("COUNT1", IntegerType.INSTANCE);
                pageResultCount = (Integer) query.uniqueResult();
                //Issue: When first time, next, or any numbered link is clicked from UI,
                //first page is rendered.
                //Fixed -- shoaib.rehman
                pageNumber = pageNumber == null ? 0 : pageNumber;
                pageSize = view.getPageSize();
                pageCount = (int) Math.ceil(((float) pageResultCount / (float) pageSize)) - 1;

                ctx.setAttribute(FacesContextKeyLookup.CurrentPageNumber.toString(), pageNumber);
                ctx.setAttribute(FacesContextKeyLookup.PageCount.toString(), pageCount);
                ctx.setAttribute(FacesContextKeyLookup.PageSize.toString(), pageSize);
                ctx.setAttribute(FacesContextKeyLookup.ResultSetCount.toString(), pageResultCount);
            }
            ContextAttributes.put(FacesContextKeyLookup.PageInfo.toString(), String.format("Page %1$s of %2$s.",
                    pageNumber + 1, pageCount + 1));

            eform.setBatchNum(pageNumber);
            eform.setBatchSize(pageSize);
            eform.setResultCount(pageResultCount);
            if (StringHelper.isNotEmpty(view.getOrderByClause()))
                eform.setOrderClause(view.getOrderByClause());
            else if (StringHelper.isNotEmpty(view.getAggregateAttribId()))
                eform.setOrderClause(view.getAggregateAttribId());
        }
    }

    /**
     * This Functions returns a Search object for entity which is found at first
     * in the Entity List returned by MetaEntity Relation, because two entities
     * can be associated with each other in more than one relation. Like
     * FundTransfer entity will have two associations with Account (From Account
     * and To Account). This Functions throws exception if no relation with
     * given Entity Id is found
     */
    public DataObjectCollection getRelatedDataObjectCollection(String relationshipId, String orderClause) {

        return getRelatedDataObjectCollection(relationshipId, null, null, orderClause);
    }

    public DataObjectCollection getRelatedDataObjectCollection(String relationshipId, List<Criterion> critera,
                                                               String orderClause) {

        return getRelatedDataObjectCollection(relationshipId, null, critera, orderClause);
    }

    public DataObjectCollection getRelatedDataObjectCollection(String relationshipId, MetaView view,
                                                               List<Criterion> criterion, String orderClause) {

		/*if(this.state == DataState.New)		{
			//do data retrieving is required for new object.
			if(this.relatedObjects.containsKey(relationshipId)){
				return this.relatedObjects.get(relationshipId);
			}
			else{
				DataObjectCollection collection = new DataObjectCollection(this.getNonAbstractParentEntity().getRelationshipByName(relationshipId).getSubEntityId(), null, this);
				this.addDataObjectCollection(relationshipId, collection);
				return collection;
			}
		}
		
		MetaRelationship rel = this.getNonAbstractParentEntity().getRelationshipByName(relationshipId);*/

        /*Changed by Nida for getting related Objects irrespective of state-- Start*/
        MetaRelationship rel = this.getNonAbstractParentEntity().getRelationshipByName(relationshipId);
        if (this.state == DataState.New && !rel.isAlwaysExist()) {
            //do data retrieving is required for new object.
            if (this.relatedObjects.containsKey(relationshipId)) {
                return this.relatedObjects.get(relationshipId);
            } else {
                DataObjectCollection collection = new DataObjectCollection(this.getNonAbstractParentEntity().getRelationshipByName(relationshipId).getSubEntityId(), null, this);
                this.addDataObjectCollection(relationshipId, collection);
                return collection;
            }
        }
        /*Changed by Nida for getting related Objects irrespective of state-- End*/
        MetaEntity relSubEntity = MetaDataRegistry.getMetaEntity(rel.getSubEntityId());
        DataBroker broker = DataRepository.getBroker(relSubEntity.getSystemName());

        DataObjectCollection results = null;

        if (rel.getRelationType().equalsIgnoreCase(MetaRelationType.Direct.name())
                || rel.getRelationType().equalsIgnoreCase(MetaRelationType.Indirect.name())) {

            Search search = new Search();
            if (StringHelper.isNotEmpty(orderClause))
                search.setOrderClause(orderClause);
            search.addFrom(relSubEntity.getSystemName(), "mainEntity");
            search.addCriterion(SimpleCriterion.equal("mainEntity." + rel.getSubEntityKey(), this.getValue(rel
                    .getMainEntityKey())));
            ViewContext viewContext = ApplicationContext.getContext().get(ViewContext.class);
            if (viewContext != null) {
                viewContext.setAttribute(relSubEntity.getId() + "-"
                        + relSubEntity.getAttribute(rel.getSubEntityKey()).getId(), this.getValue(rel
                        .getMainEntityKey()));
                Map ContextAttributes = viewContext.getAttributesMap();
                if (null != broker.getProviderType())
                    applyPagination(ContextAttributes, search, view, broker);
            }
            if (criterion != null && criterion.size() > 0) {
                for (Criterion cri : criterion) {
                    search.addCriterion(cri);
                }
            }
            try {
                List<DataObject> listResult = broker.find(search);
                results = new DataObjectCollection(MetaDataRegistry.getMetaEntity(rel.getSubEntityId()), listResult,
                        this);
            } catch (Exception e) {
                throw new CoreException(e);
            }
            return results;
        } else if (rel.getRelationType().equalsIgnoreCase(MetaRelationType.Complex.name())) {

            Search search = new Search();
            if (StringHelper.isNotEmpty(orderClause))
                search.setOrderClause(orderClause);
            search.addFrom(relSubEntity.getSystemName(), "entityTwo");
            ObjectCriterion objCr = new ObjectCriterion("entityTwo", rel.getRelationshipId(), "entityOne");
            objCr.addCriterion(SimpleCriterion.equal("entityOne." + rel.getRelMainEntityKey(), this.getValue(rel
                    .getMainEntityKey())));
            search.addCriterion(objCr);
            if (criterion != null && criterion.size() > 0) {
                for (Criterion cri : criterion) {
                    search.addCriterion(cri);
                }
            }
            List<DataObject> listResult = broker.find(search);

            results = new DataObjectCollection(MetaDataRegistry.getMetaEntity(rel.getSubEntityId()), listResult, this);

            return results;
        }

        throw new DataException("Invalid Relationship Type Exception: " + rel.getRelationType());

    }

    public DataObjectCollection getRelatedObjectsByRelationId(String relationshipId) {

        if (this.relatedObjects.containsKey(relationshipId)) {
            return this.relatedObjects.get(relationshipId);
        }

        DataObjectCollection col = this.getRelatedDataObjectCollection(relationshipId, null);

        this.relatedObjects.put(relationshipId, col);

        return col;
    }

    /**
     * In case of more than one associations between (this) Entity and
     * SubEntity, function behaviour will be not be deterministic because it
     * will execute on first MetaEntity in List returned by
     * getRelationShipBySubEntity function.
     *
     * @param subEntityId
     * @return
     */
    public DataObjectCollection getRelatedObjects(String subEntityId) {

        return this.getRelatedObjects(subEntityId, null, true, null, null);
    }

    public DataObjectCollection getRelatedObjects(String subEntityId, String orderClause) {

        return this.getRelatedObjects(subEntityId, null, true, null, orderClause);
    }

    public DataObjectCollection getRelatedObjects(String subEntityId, List<Criterion> critera) {

        return this.getRelatedObjects(subEntityId, null, true, critera, null);
    }

    public DataObjectCollection getRelatedObjects(String subEntityId, List<Criterion> critera, String orderClause) {

        return this.getRelatedObjects(subEntityId, null, true, critera, orderClause);
    }

    public DataObjectCollection getRelatedObjects(String subEntityId, MetaView view, String orderClause) {

        return this.getRelatedObjects(subEntityId, view, true, null, orderClause);
    }

    public DataObjectCollection getRelatedObjects(String subEntityId, MetaView view, boolean fetchCachedRecord,
                                                  List<Criterion> critera, String orderClause) {

        List<MetaRelationship> relationsList = this.getNonAbstractParentEntity().getRelationShipBySubEntity(subEntityId);
        if (relationsList.isEmpty())
            logger.logError("Enable to get the Relation b/w the subEntity: %1$s and the MainEntity: %2$s.",
                    subEntityId, this.refMetaEnt.getId());

        MetaRelationship rel = relationsList.get(0);

        if (fetchCachedRecord && this.relatedObjects.containsKey(rel.getRelationshipId())) {
            DataObjectCollection objCollection = this.relatedObjects.get(rel.getRelationshipId());
            if (!objCollection.isExpired())
                return objCollection;
            else {
                this.relatedObjects.remove(rel.getRelationshipId());
                objCollection.onCachedItemRemoved(rel.getRelationshipId(), this.relatedObjects, RemoveReason.Expired);
            }
        }

        DataObjectCollection col = this.getRelatedDataObjectCollection(rel.getRelationshipId(), view, critera,
                orderClause);

        String dbErrorMessage = ApplicationContext.getContext().get(ThreadContextLookup.DbErrorMesssage.toString());

        if (col.isCacheable() && StringHelper.isEmpty(dbErrorMessage)) {
            this.relatedObjects.put(rel.getRelationshipId(), col);
        }

        return col;
    }

    public boolean isForeignKey(String attribId) {

        this.checkAttributeExist(attribId);

        for (MetaRelationship rel : this.refMetaEnt.getRelationList().values()) {

            if (rel.getSubEntityId().equalsIgnoreCase(this.refMetaEnt.getId())
                    && rel.getRelationType().equals(MetaRelationType.Direct.name())
                    && rel.getSubEntityKey().equalsIgnoreCase(attribId)) {

                return true;
            }
        }

        return false;
    }

    public MetaRelationship getRelationshipByForeignKey(String attribId) {

        this.checkAttributeExist(attribId);

        for (MetaRelationship rel : this.refMetaEnt.getRelationList().values()) {

            if (rel.getMainEntityId().equalsIgnoreCase(this.refMetaEnt.getId())
                    && rel.getRelationType().equalsIgnoreCase(MetaRelationType.Indirect.name())
                    && attribId.equalsIgnoreCase(rel.getMainEntityKey())) {

                return rel;
            }
        }

        throw new DataException("This Attribute is not a foreign key [%1$s]", attribId);
    }

    /**
     * Method to get relationship based on main-entity key
     * and sub-entity.
     *
     * @param mainEntityKey
     * @param subEntityId
     * @return MetaRelationship
     */
    public MetaRelationship getRelationship(String mainEntityKey, String subEntityId) {
        this.checkAttributeExist(mainEntityKey);
        for (MetaRelationship rel : refMetaEnt.getRelationList().values()) {
            if (rel.getMainEntityId().equalsIgnoreCase(refMetaEnt.getId())
                    && rel.getRelationType().equalsIgnoreCase(MetaRelationType.Direct.name())
                    && rel.getSubEntityId().equals(subEntityId)
                    && mainEntityKey.equalsIgnoreCase(rel.getMainEntityKey())) {
                return rel;
            }
        }
        throw new DataException("This Attribute is not a foreign key [%1$s]", mainEntityKey);
    }

    public DataObject getRelatedObjectByForeignKey(String attribId) {

        DataObjectCollection colec = this.getRelatedObjectsByRelationId(this.getRelationshipByForeignKey(attribId)
                .getRelationshipId());

        if (colec.isEmpty())
            throw new DataException("Foreign Key Violation - No [%1$s] Found For value [%2$s] of Key [%3$s]", this
                    .getRelationshipByForeignKey(attribId).getMainEntityId(), this.getAsShort(attribId), attribId);

        return colec.get(0);
    }

    public void addDataObjectCollection(String relationShipId, DataObjectCollection dataObjCollection) {
        relatedObjects.put(relationShipId, dataObjCollection);
    }

    /**
     * Gets the display name for the foriegn key passed as parameter.
     *
     * @param attribId            The name of the column of the foriegn key, its attributeId
     * @param foreignObjectAttrib The value of the foriegn key.
     * @return
     */
    public Object getRelatedObjectAttribByForeignKey(String attribId, String foreignObjectAttrib) {

        return this.getRelatedObjectByForeignKey(attribId).getValue(foreignObjectAttrib);
    }

    public Map<String, ValidationInfo> validate() {

        ValidationContext validationContext = ValidationContext.getContext();
        ApplicationContext.getContext().add("Data_Object", this);
        Collection<Attribute> attribCollection = attribList.values();

        Object validationResult = null;
        for (Attribute attrib : attribCollection) {
            MetaAttribute metaAttribute = attrib.getMetaAttrib();
            String constraintExpression = metaAttribute.getConstraintExp();
            if (!StringHelper.isEmpty(constraintExpression)) {
                validationResult = ExpressionHelper.getExpressionResult(constraintExpression, String.class);

                ArrayList<String> errorsList = null;

                errorsList = ExpressionHelper.getErrorsList(ExpressionClass.DataValidatorFunctions);

                if (Convert.toBoolean(validationResult) == true)
                    validationResult = ValidatorResultType.OK;
                else
                    validationResult = ValidatorResultType.ERROR;

                validationContext.addValidationInfo(metaAttribute.getSystemName(), errorsList, String
                        .valueOf(validationResult), metaAttribute);
                ExpressionHelper.resetErrorsList(ExpressionClass.DataValidatorFunctions);
            }
        }

        return validationContext.getValidationList();
    }

    public boolean isExpired() {
        if (getCacheAbsoluteTime() == null)
            return (System.currentTimeMillis() - this.creationTimeStamp) > this.getCacheTime();
        else
            return (getCacheAbsoluteTime().getTime() - System.currentTimeMillis()) < 0;
    }

    /**
     * returns the cache time in minutes
     */
    public long getCacheTime() {
        return this.refMetaEnt.getCacheTime() * CACHE_TIME_MULTIPLIER;
    }

    public boolean isCacheable() {
        return this.refMetaEnt.getCacheTime() > 0;
    }

    public void onCachedItemRemoved(String key, Object value, RemoveReason reason) {
        // TODO Auto-generated method stub
    }

    public Date getCacheAbsoluteTime() {
        return null;
    }

    public String getKey() {
        return new StringBuilder(this.refMetaEnt.getSystemName()).append("-").append(this.getIdValue()).toString();
    }

    public DataAccessMode getDataAccessMode() {
        return dataAccessMode;
    }

    public void copyFrom(DataObject obj, boolean copyPk) {
        if (copyPk)
            this.state = DataState.New;
        for (Entry<String, Object> entry : obj.getValues().entrySet()) {
            Object value = entry.getValue();
            if (value != null) {
                if (value instanceof String && "".equalsIgnoreCase((String) value))
                    continue;
                if (value instanceof Double && ((Double) value) == 0)
                    continue;
                if (value instanceof Long && ((Long) value) == 0)
                    continue;
                if (value instanceof Integer && ((Integer) value) == 0)
                    continue;
                this.setValue(entry.getKey(), entry.getValue());
            }
        }
        if (copyPk)
            this.state = DataState.Modified;
    }

    @Deprecated
    public String getCustomerRIM() {
        try {
            return getAsString("CUST_RIM");
        } catch (DataException e) {
            return null;
        }
    }

    public String getDocumentMedium() {
        return getAsString("DOC_MEDIUM");
    }

    public MetaEntity getRefMetaEnt() {
        return refMetaEnt;
    }

    public void markModified() {

        this.state = DataState.Modified;
    }

    /**
     * This method serializes the data Object and its related objects.
     */
    public String serialize(boolean omitXmlDeclaration) {
        logger.logInfo("[serialize data object and its related objects, DataObject.serialize()]");
        StringWriter writer = new StringWriter();
        try {
            Document doc = new DocumentImpl();
            Element detail = doc.createElement("Detail");
            doc.appendChild(detail);
            Iterator<String> keyIter = this.getValues().keySet().iterator();
            while (keyIter.hasNext()) {
                String key = (String) keyIter.next();
                Attribute attribute = innerGetAttribute(key);

                Element docAttriubte = doc.createElement("Attribute");
                detail.appendChild(docAttriubte);

                Attr id = doc.createAttribute("id");
                id.setValue(attribute.getMetaAttrib().getId());
                docAttriubte.setAttributeNode(id);
                Attr name = doc.createAttribute("name");
                name.setValue(attribute.getMetaAttrib().getSystemName());
                docAttriubte.setAttributeNode(name);
                String nodeValue = String.valueOf(this.getValue(key));

                if (attribute.getMetaAttrib().getPickListId() != null) {
                    nodeValue = PickListManager.getPickListValue(nodeValue,
                            attribute.getMetaAttrib().getPickListId())[0];
                }
                logger.logInfo("value is........." + StringEscapeUtils.escapeJava(nodeValue));


                docAttriubte.appendChild(doc.createTextNode(StringEscapeUtils.escapeJava(nodeValue)));

            }

            if (this.getNonAbstractParentEntity().getRelationList().size() > 0) {
                for (MetaRelationship relationship : this.getNonAbstractParentEntity()
                        .getRelationList().values()) {
                    DataObjectCollection relatedObjectCollection = getRelatedObjectsByRelationId(relationship
                            .getRelationshipId());
                    int i = 1;
                    for (DataObject dataObject : relatedObjectCollection
                            .getObjectList()) {
                        Element relatedElement = doc
                                .createElement("RelatedObject");
                        Attr name = doc.createAttribute("Name");
                        name.setValue(relatedObjectCollection.getRefEntity().getPrimaryName());
                        relatedElement.setAttributeNode(name);

                        detail.appendChild(relatedElement);
                        keyIter = dataObject.getValues().keySet().iterator();
                        while (keyIter.hasNext()) {
                            String key = (String) keyIter.next();
                            Attribute attribute = dataObject.innerGetAttribute(key);
                            String nodeValue = String.valueOf(dataObject
                                    .getValue(key));

                            if (attribute.getMetaAttrib().getPickListId() != null) {
                                nodeValue = PickListManager.getPickListValue(
                                        nodeValue, attribute.getMetaAttrib()
                                                .getPickListId())[0];
                            }
                            Element docAttriubte = doc.createElement("Attribute");
                            relatedElement.appendChild(docAttriubte);

                            docAttriubte.appendChild(doc.createTextNode(StringEscapeUtils.escapeJava(nodeValue)));

                            Attr id = doc.createAttribute("id");
                            id.setValue(attribute.getMetaAttrib().getId());
                            docAttriubte.setAttributeNode(id);
                            name = doc.createAttribute("name");
                            name.setValue(attribute.getMetaAttrib().getSystemName());
                            docAttriubte.setAttributeNode(name);

                        }
                        i++;
                    }
                }
            }
            // Serialize the document
            //StringWriter writer = new StringWriter();
            OutputFormat format = new OutputFormat(doc);
            format.setOmitXMLDeclaration(omitXmlDeclaration);
            format.setLineWidth(65);
            format.setIndenting(true);
            format.setIndent(2);
            format.setEncoding("UTF-8");

            XMLSerializer serializer = new XMLSerializer(writer, format);

            serializer.asDOMSerializer();
            serializer.serialize(doc);
        } catch (IOException e) {
            logger.LogException("DataObject is not serialized into XML", e);
            //e.printStackTrace();
        }

        return writer.toString();

    }

    public String serialize(boolean omitXmlDeclaration, boolean includeRelationData) {
        logger.logInfo("[serialize data object and its related objects, DataObject.serialize()]");

        if (includeRelationData) {
            return this.serialize(omitXmlDeclaration);

        } else {
            logger.logInfo("[serialize data object and its related objects, DataObject.serialize()]");
            StringWriter writer = new StringWriter();
            try {
                Document doc = new DocumentImpl();
                Element detail = doc.createElement("Detail");
                doc.appendChild(detail);
                Iterator<String> keyIter = this.getValues().keySet().iterator();
                while (keyIter.hasNext()) {
                    String key = (String) keyIter.next();
                    Attribute attribute = innerGetAttribute(key);

                    Element docAttriubte = doc.createElement("Attribute");
                    detail.appendChild(docAttriubte);

                    Attr id = doc.createAttribute("id");
                    id.setValue(attribute.getMetaAttrib().getId());
                    docAttriubte.setAttributeNode(id);
                    Attr name = doc.createAttribute("name");
                    name.setValue(attribute.getMetaAttrib().getSystemName());
                    docAttriubte.setAttributeNode(name);
                    String nodeValue = String.valueOf(this.getValue(key));

                    if (attribute.getMetaAttrib().getPickListId() != null) {
                        nodeValue = PickListManager.getPickListValue(nodeValue,
                                attribute.getMetaAttrib().getPickListId())[0];
                    }
                    logger.logInfo("value is........." + StringEscapeUtils.escapeJava(nodeValue));


                    docAttriubte.appendChild(doc.createTextNode(StringEscapeUtils.escapeJava(nodeValue)));

                }


                // Serialize the document
                //StringWriter writer = new StringWriter();
                OutputFormat format = new OutputFormat(doc);
                format.setOmitXMLDeclaration(omitXmlDeclaration);
                format.setLineWidth(65);
                format.setIndenting(true);
                format.setIndent(2);
                format.setEncoding("UTF-8");

                XMLSerializer serializer = new XMLSerializer(writer, format);

                serializer.asDOMSerializer();
                serializer.serialize(doc);
            } catch (IOException e) {
                logger.LogException("DataObject is not serialized into XML", e);
                //e.printStackTrace();
            }

            return writer.toString();
        }
    }




    /*
     * Shahbaz
     * For returning current state
     *
     */

    public String getCurrentState() {

        if (attribList.containsKey("CURRENT_STATE")) {
            return this.getAsString("CURRENT_STATE");
        }
        return null;
    }


    /*
     * End
     */

    public String getSessionId() {
        return sessionId;
    }

    public void createSessionId() {
        if (sessionId == null)
            sessionId = UUID.randomUUID().toString();
    }

    //Shuwair
    private void serializedAttributes() {
        if (!serializedAttribMap.isEmpty()) {
            for (SerializedAttribute serializedAttribute : serializedAttribMap.values()) {
                //	attribList.remove(serializedAttribute.getSystemName());
            }

            String jsonStr = new JsonParser().serialize(serializedAttribMap);
            if (attribList.get(JsonKeyLookup.JSON_ATTRIB) != null)
                attribList.get(JsonKeyLookup.JSON_ATTRIB).setValue(jsonStr);
        }
    }

    private void deSerializedAttrubutes() {
        String jsonStr = StringHelper.EMPTY;
        HashMap<String, SerializedAttribute> tempSerializedAttributeMap = new HashMap<String, SerializedAttribute>();
        if (attribList.get(JsonKeyLookup.JSON_ATTRIB) != null)
            jsonStr = (String) attribList.get(JsonKeyLookup.JSON_ATTRIB).getValue();

        if (StringHelper.isNotEmpty(jsonStr))
            tempSerializedAttributeMap = new JsonParser().deSerialize(jsonStr);

        for (SerializedAttribute jsonAttribute : tempSerializedAttributeMap.values())
            if (attribList.get(jsonAttribute.getSystemName()) != null)
                attribList.get(jsonAttribute.getSystemName()).setValue(jsonAttribute.getValue());

        isSerialized = true;
    }

    /**
     * This method is only used for drafts
     *
     * @param jsonStr
     */
    public void deSerializedAttrubutes(String jsonStr) {

        /******************** Separate Save as draft START **************/
        boolean check = false;
        Set<String> entityAttributeKeySet = null;
        Map<String, DataObjectCollection> relatedDataObjectCollectionMap = null;
        UserDbImpl user = null;

        if (jsonStr != null && !jsonStr.isEmpty() && jsonStr.endsWith("[{$postfix}]")) {
            logger.logInfo("called deSerializedAttrubutes (json string with postfix) :" + jsonStr);
            String[] strArr = jsonStr.split("[{$postfix}]");
            if (strArr[0] != null) {
                jsonStr = strArr[0];
                check = true;
            }

            entityAttributeKeySet = this.getValues().keySet();
            relatedDataObjectCollectionMap = new HashMap<String, DataObjectCollection>();

            //TODO: UNISONPRIME
            user = new UserDbImpl(); /*(UserDbImpl) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(
                    SessionKeyLookup.CURRENT_USER_KEY);*/
        }

        Map<String, MetaAttribute> jsonMetaAttribMap = MetaDataRegistry.getMetaEntity("1_0000000080").getAttributeList();
        /******************** Separate Save as draft END **************/

        //String jsonStr =draftJson
        HashMap<String, SerializedAttribute> tempSerializedAttributeMap = new HashMap<String, SerializedAttribute>();
		/*if(attribList.get(JsonKeyLookup.JSON_ATTRIB)!=null)
			jsonStr = (String)attribList.get(JsonKeyLookup.JSON_ATTRIB).getValue();*/

        if (StringHelper.isNotEmpty(jsonStr))
            tempSerializedAttributeMap = new JsonParser().deSerialize(jsonStr);

        for (SerializedAttribute jsonAttribute : tempSerializedAttributeMap.values())
        /******************** Separate Save as draft START **************/ {
            if (!(attribList.get(jsonAttribute.getSystemName()) != null)) {

                MetaAttribute metaAttrib = null;
                if (jsonMetaAttribMap.get(jsonAttribute.getMetaAttributeId()) != null)
                    metaAttrib = jsonMetaAttribMap.get(jsonAttribute.getMetaAttributeId());

                if (check) {

                    Object currentUserObject = ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);

                    if (entityAttributeKeySet.contains(metaAttrib.getSystemName())) { // check if view attribute exists in data object attribute list
                        this.setValue(metaAttrib.getId(), jsonAttribute.getValue());
                    } else {
                        if (relatedDataObjectCollectionMap.containsKey(metaAttrib.getMetaEntityId())) {
                            relatedDataObjectCollectionMap.get(metaAttrib.getMetaEntityId()).get(0).setValue(metaAttrib.getId(), jsonAttribute.getValue());
                        } else {
                            DataObjectCollection collection = this.getRelatedObjects(metaAttrib.getMetaEntityId()); // get the related data object collection if view attribute is not present in current data object
                            DataObject relatedObj;
                            if (collection.size() == 0) {
                                relatedObj = new DataObject(MetaDataRegistry.getMetaEntity(metaAttrib.getMetaEntityId()));
                                relatedObj.setValue("CREATED_ON", Calendar.getInstance().getTime());
                                relatedObj.setValue("UPDATED_BY", user.getLoginId());

                                collection.add(relatedObj);
                            } else {
                                relatedObj = collection.get(0);
                            }
                            relatedObj.setValue(metaAttrib.getId(), jsonAttribute.getValue());
                            relatedObj.setValue("UPDATED_ON", Calendar.getInstance().getTime());
                            relatedObj.setValue("UPDATED_BY", user.getLoginId());

                            List<MetaRelationship> listRelations = this.getNonAbstractParentEntity().getRelationShipBySubEntity(metaAttrib.getMetaEntityId());
                            this.addDataObjectCollection(listRelations.get(0).getRelationshipId(), collection);
                            relatedDataObjectCollectionMap.put(metaAttrib.getMetaEntityId(), collection);
                        }

                    }

                } //end check if

                if (metaAttrib != null)
                    this.setAttribute(jsonAttribute.getSystemName(), new Attribute(metaAttrib, jsonAttribute.getValue()));
            }
            /******************** Separate Save as draft END **************/
            if (attribList.get(jsonAttribute.getSystemName()) != null)
                attribList.get(jsonAttribute.getSystemName()).setValue(jsonAttribute.getValue());
            /******************** Separate Save as draft START **************/
        }

        //TODO: UNISONPRIME
        //FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("relatedDataObject", this);
        /******************** Separate Save as draft END **************/

    }

    public boolean isSerializationRequired() {
        return isSerializationRequired;
    }

    public void setSerializationRequired(boolean isSerializationRequired) {
        this.isSerializationRequired = isSerializationRequired;
    }

    public boolean isSerialized() {
        return isSerialized;
    }

    public void setSerialized(boolean isSerialized) {
        this.isSerialized = isSerialized;
    }

    public Draft saveAsDraft(String userLoginId, String draftTicketNum, MetaEntity entity) {
        ApplicationContext.getContext().add("Data_Object", this);
        Collection<Attribute> attribCollection = attribList.values();
        HashMap<String, SerializedAttribute> serializedDraftAttribMap = new HashMap<String, SerializedAttribute>();


        for (Attribute attrib : attribCollection) {
            MetaAttribute metaAttribute = attrib.getMetaAttrib();

            //add counter values
            if (this.state == DataState.New && metaAttribute.getCounterName() != null && attrib.getValue() == null) {
                //attrib.setValue(MetaDataRegistry.getNextCounterValue(metaAttribute.getCounterName()));
            }

            if (metaAttribute.getScope() == AttributeScope.FirstTimeEdit
                    || metaAttribute.getScope() == AttributeScope.ReadOnly) {
                if (this.state == DataState.New) {
                    String calcExpression = metaAttribute.getCalculateExp();
                    if (!StringHelper.isEmpty(calcExpression)) {
                        Object value = null;
                        value = ExpressionHelper.getExpressionResult(calcExpression, Object.class);
                        attrib.setValue(value);
                    }
                }


            }

            //Shuwair
            if (metaAttribute.getIsSerialized() && metaAttribute.getSerializationType() != null && metaAttribute.getSerializationType().equalsIgnoreCase(JsonKeyLookup.JSON_SERIALIZATION)) {
                serializedAttribMap.put(attrib.getMetaAttrib().getSystemName(), new SerializedAttribute(attrib));
            }
            //Serilize all data
            serializedDraftAttribMap.put(attrib.getMetaAttrib().getSystemName(), new SerializedAttribute(attrib));


        }
        //Shuwair
        serializedAttributes();
        String jsonStr = null;
        if (!serializedDraftAttribMap.isEmpty()) {
//			for(SerializedAttribute serializedAttribute:serializedDraftAttribMap.values())
//			{
//				attribList.remove(serializedAttribute.getSystemName());
//			}	
            jsonStr = new JsonParser().serialize(serializedDraftAttribMap);

        }

        DraftService draftService = new DraftService();
        Draft draft = new Draft();
        if (draftTicketNum == null) {
            String draftTicketId = (String) MetaDataRegistry.getNextCounterValue(Draft.DRAFT_TICKET_NO);
            draft.setDraftTicketNum(draftTicketId);
        } else {
            draft = draftService.findById(draftTicketNum);
        }

        draft.setJsonData(jsonStr);
        draft.setCurrentState(Draft.Status.DRAFT_STATUS.getStatus());
        draft.setCustomerName(getAsString("CUSTOMER_NAME"));
        draft.setCustRelationNum(getAsString("CUST_RELATION_NUM"));
        draft.setCurrentActor(userLoginId);
        draft.setProductEntityId(entity.getId());
        draft.setParentEntity(entity.getParentId());


        draftService.presist(draft);

        return draft;
        //TODO change the code given below, according to draft
/*		DataBroker broker = DataRepository.getBroker(this.refMetaEnt.getSystemName());
		
		if(this.state == DataState.Deleted)
		{
			//first related objects need to be removed.
			saveRelatedObjects();
			if(oldState != DataState.New) {
				broker.delete(this);
			}	
		} else if (this.state == DataState.New) {
			broker.add(this);
			acceptChanges();
			saveRelatedObjects();
		}else if (this.state == DataState.Modified) {
			broker.update(this);
			acceptChanges();
			saveRelatedObjects();
		}else if (this.state == DataState.Unchanged){
			//children can be found modified.
			saveRelatedObjects();
		}
	*/

    }

    public String getDraftTicketNum() {
        return draftTicketNum;
    }

    public void setDraftTicketNum(String draftTicketNum) {
        this.draftTicketNum = draftTicketNum;
    }

    public void closeDraft() throws Exception {
        if (this.draftTicketNum != null) {
            DraftService draftService = new DraftService();
            Draft draft = draftService.findById(draftTicketNum);
            if (Draft.Status.DRAFT_STATUS.getStatus().equals(draft.getCurrentState())) {
                draft.setCurrentState(Draft.Status.FORM_STATUS.getStatus());
                draftService.update(draft);
            }
        }
        /******************** Separate Save as draft START **************/
        //TODO: UNISONPRIME
        /*if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("relatedDataObject") != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("relatedDataObject");
        }*/
        /******************** Separate Save as draft END **************/
    }

    /******************** Separate Save as draft START **************/
    public HashMap<String, Attribute> getAttribList() {
        return attribList;
    }
    /******************** Separate Save as draft END **************/

}
