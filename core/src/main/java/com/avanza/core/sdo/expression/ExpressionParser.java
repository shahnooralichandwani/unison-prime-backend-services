package com.avanza.core.sdo.expression;


public class ExpressionParser {


    public static Expression parse(String validations) {
        Expression exp = null;

        if (validations.toLowerCase().startsWith("and(") || validations.toLowerCase().startsWith("or(")) {
            String exps = validations.substring(validations.toLowerCase().indexOf("and") + 4, validations.length() - 1);
            System.out.println(exps);
            String[] tokens = exps.split(";");
            String topFunctionName = validations.substring(0, validations.toLowerCase().indexOf("("));
            exp = new Expression(topFunctionName, ExpressionType.Function);
            for (String token : tokens) {
                if (isSingleParameter(token))
                    exp.add(getSingleParameterExpression(token));
                else
                    exp.add(getDoubleParameterExpression(token));
            }

        } else {
            if (isSingleParameter(validations))
                exp = getSingleParameterExpression(validations);
            else
                exp = getDoubleParameterExpression(validations);
        }
        return exp;
    }

    private static Expression getDoubleParameterExpression(String validations) {
        String func = validations.substring(0, validations.indexOf("("));
        String data = validations.substring(validations.indexOf("(") + 1, validations.indexOf(")"));
        String vals[] = data.split(",");
        Expression exp = new Expression(func, ExpressionType.Function);
        exp.add(new Expression(vals[0], ExpressionType.Constant));
        exp.add(new Expression(vals[1], ExpressionType.Constant));
        return exp;
    }

    private static Expression getSingleParameterExpression(String validations) {
        String func = validations.substring(0, validations.indexOf("("));
        String data = validations.substring(validations.indexOf("(") + 1, validations.indexOf(")"));
        Expression exp = new Expression(func, ExpressionType.Function);
        exp.add(new Expression(data, ExpressionType.Constant));
        return exp;
    }

    private static boolean isSingleParameter(String func) {
        if (func.toLowerCase().startsWith("regex")) return true;
        if (func.toLowerCase().startsWith("req")) return true;
        return false;
    }

    public static boolean isValidatorPresent(String validationExp, String functionName) {
        if (validationExp != null && functionName != null) {
            if (validationExp.toLowerCase().indexOf(functionName.toLowerCase()) != -1) return true;
        }
        return false;
    }
}
