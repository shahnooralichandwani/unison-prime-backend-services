package com.avanza.core.sdo;

import com.avanza.core.CoreException;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Order;
import com.avanza.core.data.expression.OrderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.inbound.InboundLoader;
import com.avanza.core.meta.MetaCounter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.ValueTreeNodeBase;
import com.avanza.core.sessionhistory.ActivityType;
import com.avanza.core.sessionhistory.SessionSource;
import com.avanza.core.util.StringHelper;

import java.util.*;

public final class ActivityTypeCatalog {

    private HashMap<String, ActivityType> treeMap;
    private HashMap<String, HashMap<String, List<ActivityType>>> activityRegistry;//document wise to activity type map.
    private HashMap<String, ActivityType> idMap;//document wise to activity type map.
    private Boolean isTreeLoaded = true;
    private Boolean isRegistryLoaded = true;
    private static ActivityTypeCatalog _instance;
    private ValueTreeNodeBase activityTypeTree;
    private static Map<String, List<ActivityType>> parentChildMap = new HashMap<String, List<ActivityType>>();

    private ActivityTypeCatalog() {
        this.treeMap = new HashMap<String, ActivityType>();
        this.activityRegistry = new HashMap<String, HashMap<String, List<ActivityType>>>();
        this.idMap = new HashMap<String, ActivityType>();
        this.load();
        this.loadActivitiesMap();
    }

    private ActivityTypeCatalog(boolean flag) {

    }

    public static ActivityTypeCatalog getInstance() {
        if (_instance == null)
            _instance = new ActivityTypeCatalog();
        return _instance;
    }

    public static ActivityTypeCatalog getInstance(boolean flag) {
        if (flag)
            _instance = new ActivityTypeCatalog(flag);
        else
            getInstance();
        return _instance;
    }

    public void loadActivitiesMap() {
        // Usman [2-9-2013]
        // initialize again to reload updated activities
        this.activityRegistry = new HashMap<String, HashMap<String, List<ActivityType>>>();
        this.idMap = new HashMap<String, ActivityType>();

        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());
        List<ActivityType> retList = broker.findAll(ActivityType.class);
        HashMap<String, List<ActivityType>> subMap = null;
        List<ActivityType> subList = null;
        for (ActivityType type : retList) {
            idMap.put(type.getActivityTypeId(), type);
            if (type.getOperType() != null && type.getDocTypeId() != null) {
                subMap = activityRegistry.get(type.getDocTypeId());
                if (subMap == null) {
                    subMap = new HashMap<String, List<ActivityType>>();
                    activityRegistry.put(type.getDocTypeId(), subMap);
                }
                subList = subMap.get(type.getOperType());
                if (subList == null) {
                    subList = new ArrayList<ActivityType>();
                    subMap.put(type.getOperType(), subList);
                }
                subList.add(type);
            }
        }
        isRegistryLoaded = true;
    }

    public Map<String, List<ActivityType>> loadActivitiesByParentChildMap() {
        if (parentChildMap.isEmpty()) {
            DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());
            List<ActivityType> retList = broker.findAll(ActivityType.class);

            for (ActivityType aType : retList) {
                if (aType.getParentId() == null)
                    continue;

                if (!(parentChildMap.containsKey(aType.getParentId()))) {
                    List<ActivityType> tempList = new ArrayList<ActivityType>();
                    tempList.add(aType);
                    parentChildMap.put(aType.getParentId(), tempList);
                } else if (parentChildMap.containsKey(aType.getParentId())) {
                    List<ActivityType> tempList = parentChildMap.get(aType.getParentId());
                    tempList.add(aType);
                    parentChildMap.put(aType.getParentId(), tempList);
                }
            }
        }
        return parentChildMap;
    }

    public void load() {

        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());
        List<ActivityType> retList = broker.findAll(ActivityType.class);
        Hashtable<String, ActivityType> tempMap = new Hashtable<String, ActivityType>(retList.size());

        for (ActivityType item : retList) {
            String key = item.getActivityTypeId().toLowerCase();
            tempMap.put(key, item);
            if (item.isRoot())
                this.treeMap.put(key, item);
        }
        for (ActivityType item : retList) {
            if (!StringHelper.isEmpty(item.getParentId())) {
                String key = item.getParentId().toLowerCase();
                ActivityType parentNode = tempMap.get(key);
                if (parentNode != null)
                    parentNode.add(item);
            }
        }

        treeMap.values().remove(null);
        activityTypeTree = getTree();
        isTreeLoaded = true;
    }

    public List<ActivityType> getSelectedActivitySuccessors(String activityTypeId, Integer hierarchyLevel) {

        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());
        List<ActivityType> retList = new ArrayList<ActivityType>(0);
        Search query = new Search(ActivityType.class);
        if (activityTypeId == null) {
            query.addCriterion(Criterion.equal("hierarchyLevel", new Integer(hierarchyLevel)));
        } else {
            //passing "parentId" instead of hierarchyLevel
            query.addCriterion(Criterion.and(Criterion.equal("parentId", activityTypeId),
                    Criterion.equal("hierarchyLevel", hierarchyLevel)));
        }
        query.addOrder(new Order("primaryName", OrderType.ASC));
        retList = broker.find(query);
        return retList;

    }

    public List<ActivityType> getSelectedEnabledActivitySuccessors(String activityTypeId, Integer hierarchyLevel) {

        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());
        List<ActivityType> retList = new ArrayList<ActivityType>(0);
        Search query = new Search(ActivityType.class);
        query.addCriterion(Criterion.equal("isEnabled", true));
        if (activityTypeId == null) {
            query.addCriterion(Criterion.equal("hierarchyLevel", new Integer(hierarchyLevel)));
        } else {
            //passing "parentId" instead of hierarchyLevel
            query.addCriterion(Criterion.and(Criterion.equal("parentId", activityTypeId),
                    Criterion.equal("hierarchyLevel", hierarchyLevel)));
        }
        query.addOrder(new Order("primaryName", OrderType.ASC));
        retList = broker.find(query);
        return retList;

    }

    public List<ActivityType> getActivitiesByLevel(Integer hierarchyLevel) {

        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());
        List<ActivityType> retList = new ArrayList<ActivityType>(0);

        if (hierarchyLevel == null) {
            retList = broker.findAll(ActivityType.class);
        } else {
            Search query = new Search(ActivityType.class);
            query.addCriterion(Criterion.equal("hierarchyLevel", hierarchyLevel));
            query.addOrder(new Order("primaryName", OrderType.ASC));
            retList = broker.find(query);
        }
        return retList;
    }

    public List<ActivityType> getEnabledActivitiesByLevel(Integer hierarchyLevel) {

        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());
        List<ActivityType> retList = new ArrayList<ActivityType>(0);
        Search query = new Search(ActivityType.class);
        query.addCriterion(Criterion.equal("isEnabled", true));
        if (hierarchyLevel == null) {
            retList = broker.findAll(ActivityType.class);
        } else {
            query.addCriterion(Criterion.equal("hierarchyLevel", hierarchyLevel));
            query.addOrder(new Order("primaryName", OrderType.ASC));
            retList = broker.find(query);
        }
        return retList;
    }

    public List<ActivityType> getAutoWrapUpActivities(Integer hierarchyLevel) {

        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());
        List<ActivityType> retList = new ArrayList<ActivityType>(0);
        Search query = new Search(ActivityType.class);
        query.addCriterion(Criterion.equal("isAutoWrapUp", true));
        if (hierarchyLevel == null) {
            retList = broker.findAll(ActivityType.class);
        } else {
            //query.addCriterion( Criterion.equal( "hierarchyLevel" , hierarchyLevel ) );
            query.addOrder(new Order("primaryName", OrderType.ASC));
            retList = broker.find(query);
        }
        return retList;
    }

    public void dispose() {

    }

    public void synchronize() {

    }

    public ActivityType getTree() {
        if (!isTreeLoaded)
            load();
        String key = InboundLoader.getInstance().getActivityTypeTreeRoot("ROOT-KEY");
        ActivityType retVal = this.treeMap.get(key.toLowerCase());
        if (retVal == null) {
            throw new DataException("No Activity Type found in catalog for key: [%1$s]", key);
        }
        return (ActivityType) updateValueTree(retVal);

    }

    public ActivityType getActivityFromTree(String activityTypeId) {

        if (this.treeMap == null)
            return null;
        return this.treeMap.get(activityTypeId);
    }

    public ValueTreeNodeBase updateValueTree(ValueTreeNodeBase parentNode) {
        ValueTreeNodeBase currNode = parentNode;

        if (currNode.getChildList() != null && currNode.getChildList().size() > 0) {
            List<ValueTreeNodeBase> childList = currNode.getChildList();
            for (ValueTreeNodeBase childNode : childList)
                updateValueTree(childNode);
            return currNode;
        } else {

            currNode.setValue(((ActivityType) currNode).getTypeDescription());
            //currNode.setCode(  ( ( ActivityType )currNode).getActivityTypeId() );
            return currNode;
        }
    }

    public ActivityType getActivityFromCatalog(String key) {

        String[] splitedKeys = key.split(":");
        String docId = splitedKeys[0];
        String operation = splitedKeys[1];
        String parentDocId = docId;

        List<ActivityType> typeList = (this.activityRegistry.get(docId) == null) ? null : this.activityRegistry.get(docId).get(operation);
        ActivityType type = (typeList == null || typeList.isEmpty()) ? null : typeList.get(0);

        if (type == null) {

            while (true) {
                if (MetaDataRegistry.getMetaEntity(parentDocId).getParent() != null) {
                    parentDocId = MetaDataRegistry.getMetaEntity(parentDocId).getParent().getId();
                    type = getParentActivity(parentDocId, operation);
                    if (type == null)
                        continue;
                    else
                        break;

                } else {
                    break;
                }
            }
        }

        if (type == null)
            throw new DataException(
                    "No Activity Type found in  internal activity map for key: [%1$s]",
                    key);
        return type;
    }

    private ActivityType getParentActivity(String docId, String operation) {
        List<ActivityType> retList = (activityRegistry.get(docId) == null) ? null : activityRegistry.get(docId).get(operation);
        return (retList == null || retList.isEmpty()) ? null : retList.get(0);
    }

    public List<ActivityType> getActivitiesFor(String metaEntityId) {

        Map<String, List<ActivityType>> activtyMap = this.activityRegistry.get(metaEntityId);

        if (activtyMap == null || activtyMap.isEmpty())
            return new ArrayList<ActivityType>();

        List<ActivityType> retValue = new ArrayList<ActivityType>();
        for (List<ActivityType> list : activtyMap.values()) {
            retValue.addAll(list);
        }
        Collections.sort(retValue);

        return retValue;
    }

    public ActivityType getActivityById(String activityId) {
        if (StringHelper.isEmpty(activityId)) return null;
        return this.idMap.get(activityId);
    }

    public void deleteActivityType(ActivityType currentActivityType) {
        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());
        TransactionContext txContext = ApplicationContext.getContext().getTxnContext();

        txContext.beginTransaction();
        try {
            broker.delete(currentActivityType);
            txContext.commit();

        } catch (Exception e) {
            txContext.rollback();
            throw new CoreException("Exception occured while Deleting Activity Type", e);
        }
    }

    public void persistActivityType(ActivityType currentActivityType) {
        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());

        if (StringHelper.isEmpty(currentActivityType.getActivityTypeId()))
            currentActivityType.setActivityTypeId((String) MetaDataRegistry.getNextCounterValue(MetaCounter.ACTIVITY_TYPE_COUNTER));

        broker.persist(currentActivityType);
        this.treeMap.put(currentActivityType.getActivityTypeId().toLowerCase(), currentActivityType);
        this.idMap.put(currentActivityType.getActivityTypeId().toLowerCase(), currentActivityType);
    }

    public Boolean getIsTreeLoaded() {
        return isTreeLoaded;
    }

    public Boolean getIsRegistryLoaded() {
        return isRegistryLoaded;
    }

    public HashMap<String, ActivityType> getTreeMap() {
        return treeMap;
    }

    public void setTreeMap(HashMap<String, ActivityType> treeMap) {
        this.treeMap = treeMap;
    }

    public void setIsTreeLoaded(Boolean isTreeLoaded) {
        this.isTreeLoaded = isTreeLoaded;
    }

    public void setIsRegistryLoaded(Boolean isRegistryLoaded) {
        this.isRegistryLoaded = isRegistryLoaded;
    }

    public ValueTreeNodeBase getActivityTypeTree() {
        return activityTypeTree;
    }

    public ActivityType getById(String typeId) {
        return this.idMap.get(typeId);
    }

    public List<ActivityType> getActivityTypeRoot() {

        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());

        Search query = new Search(ActivityType.class);
        query.addCriterion(Criterion.equal("parentId", "000000"));
        List<ActivityType> retList = broker.find(query);

        return retList;
    }

    public List<SessionSource> getAllSessionSource() {
        DataBroker broker = DataRepository.getBroker(SessionSource.class.getName());
        Search query = new Search(SessionSource.class);

        return broker.find(query);
    }

    public List<ActivityType> getActivityTypeBy(Search search) {
        DataBroker broker = DataRepository.getBroker(ActivityType.class.getName());
        return broker.find(search);
    }
}
