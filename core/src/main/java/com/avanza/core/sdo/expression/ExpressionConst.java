package com.avanza.core.sdo.expression;

abstract class ExpressionConst {

    static final char ATTRIB_START_TOKEN = '{';
    static final char ATTRIB_END_TOKEN = '}';
    static final char METHOD_START_TOKEN = '(';
    static final char METHOD_END_TOKEN = ')';
    static final char CONST_START_TOKEN = '[';
    static final char CONST_End_TOKEN = ']';
    static final char COMMA_TOKEN = ',';
    static final String SELF_REFERENCE = "this";
    static final int END = -1;
}
