package com.avanza.core.sdo;

import com.avanza.core.cache.Cacheable;
import com.avanza.core.cache.RemoveReason;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.util.Guard;

import java.io.Serializable;
import java.util.*;

public class DataObjectCollection implements List<DataObject>, Serializable, Cacheable {

    private static final long serialVersionUID = 2002752945763599574L;

    private ArrayList<DataObject> objectList = new ArrayList<DataObject>();
    private MetaEntity refEntity;
    private DataState state;
    private DataState oldState;
    private DataObject parent;
    private long creationTimeStamp = System.currentTimeMillis();

    private DataObjectCollection() {
    }

    public DataObjectCollection(String subEntityId, List<DataObject> objects, DataObject parent) {

        Guard.checkNull(subEntityId, "DataObjectCollection(subEntityId)");

        this.refEntity = MetaDataRegistry.getMetaEntity(subEntityId);
        Guard.checkNull(this.refEntity, "DataObjectCollection(MetaEntity)");

        this.addAll(objects);
        this.state = DataState.Unchanged;
    }

    public DataObjectCollection(MetaEntity refEntity, List<DataObject> objects, DataObject parent) {

        Guard.checkNull(refEntity, "DataObjectCollection(MetaEntity)");
        this.refEntity = refEntity;
        this.addAll(objects);
        this.state = DataState.Unchanged;
        this.parent = parent;
    }

    public DataObjectCollection(MetaEntity refEntity, DataObject parent) {

        Guard.checkNull(refEntity, "DataObjectCollection(MetaEntity)");
        this.refEntity = refEntity;
        this.state = DataState.New;
    }

    public DataObject getParent() {
        return parent;
    }

    public DataState getState() {
        return state;
    }

    public boolean isModified() {
        return (this.state != this.oldState);
    }

    public boolean isNew() {
        return (this.state == DataState.New);
    }

    public MetaEntity getRefEntity() {
        return refEntity;
    }

    public List<DataObject> getObjectList() {
        return objectList;
    }

    protected void setObjectList(List<DataObject> objectList) {

        this.objectList.clear();
        this.objectList.addAll(objectList);

        modifyState();
    }

    private void modifyState() {
        // check if its a new object
        if ((this.state == DataState.New) || (this.state == DataState.Deleted)) return;

        // loaded from the database
        this.state = DataState.Modified;
    }

    public boolean add(DataObject item) {

        this.checkMetaEntity(item);
        boolean retVal = this.objectList.add(item);

        modifyState();

        return retVal;
    }

    public void add(int index, DataObject item) {

        this.checkMetaEntity(item);
        this.objectList.add(index, item);

        modifyState();
    }

    public boolean addAll(Collection<? extends DataObject> itemList) {

        if (itemList == null) return true;

        for (DataObject item : itemList) {
            this.checkMetaEntity(item);
        }

        boolean retVal = this.objectList.addAll(itemList);

        modifyState();

        return retVal;
    }

    public boolean addAll(int index, Collection<? extends DataObject> itemList) {

        if (itemList == null) return true;

        for (DataObject item : itemList) {
            this.checkMetaEntity(item);
        }

        boolean retVal = this.objectList.addAll(index, itemList);

        modifyState();

        return retVal;
    }

    public void clear() {
        this.objectList.clear();
    }

    public boolean contains(Object o) {
        return this.objectList.contains(o);
    }

    public boolean containsAll(Collection<?> c) {
        return this.objectList.containsAll(c);
    }

    public DataObject get(int index) {
        return this.objectList.get(index);
    }

    public int indexOf(Object o) {
        return this.objectList.indexOf(o);
    }

    public boolean isEmpty() {
        return this.objectList.isEmpty();
    }

    public Iterator<DataObject> iterator() {
        return this.objectList.iterator();
    }

    public int lastIndexOf(Object o) {
        return this.objectList.lastIndexOf(o);
    }

    public ListIterator<DataObject> listIterator() {
        return this.objectList.listIterator();
    }

    public ListIterator<DataObject> listIterator(int index) {
        return this.objectList.listIterator(index);
    }

    public boolean remove(Object o) {
        return this.objectList.remove(o);
    }

    public DataObject remove(int index) {
        return this.objectList.remove(index);
    }

    public boolean removeAll(Collection<?> c) {
        return this.objectList.removeAll(c);
    }

    public boolean retainAll(Collection<?> c) {
        return this.objectList.retainAll(c);
    }

    public DataObject set(int index, DataObject element) {

        this.checkMetaEntity(element);
        return this.objectList.set(index, element);
    }

    public int size() {
        return this.objectList.size();
    }

    public List<DataObject> subList(int fromIndex, int toIndex) {
        return this.objectList.subList(fromIndex, toIndex);
    }

    public Object[] toArray() {
        return this.objectList.toArray();
    }

    public <T> T[] toArray(T[] a) {
        return this.objectList.toArray(a);
    }

    public List<Map<String, Object>> getcollectionsValueList() {
        List<Map<String, Object>> collection = new ArrayList<Map<String, Object>>();
        for (DataObject obj : objectList) {

            Map<String, Object> values = obj.getValues();
            values.put(DataObject.REF_ENTITY_ID, obj.getMetaEntity().getId());
            collection.add(values);
        }
        return collection;
    }

    public DataObject find(String primaryKey) {

        DataObject retVal = null;
        for (DataObject item : this.objectList) {
            if (item.getIdValue().compareTo(primaryKey) == 0) {
                retVal = item;
                break;
            }
        }

        return retVal;
    }

    private void checkMetaEntity(DataObject item) {

        if (!item.getMetaEntity().isSubclass(this.refEntity.getId()))
            throw new DataException("Can't add. Entity [%1$s] must be subclass of Meta entity [%2$s", item.getUniqueId(), this.refEntity
                    .getSystemName());
    }

    public Date getCacheAbsoluteTime() {
        return null;
    }

    public long getCacheTime() {
        return this.refEntity.getCacheTime() * CACHE_TIME_MULTIPLIER;
    }

    public boolean isCacheable() {
        return this.refEntity.getCacheTime() > 0;
    }

    public boolean isExpired() {
        if (getCacheAbsoluteTime() == null)
            return (System.currentTimeMillis() - this.creationTimeStamp) > this.getCacheTime();
        else
            return (getCacheAbsoluteTime().getTime() - System.currentTimeMillis()) < 0;
    }

    public void onCachedItemRemoved(String key, Object value, RemoveReason reason) {
        // TODO Auto-generated method stub
    }

    public String getKey() {
        return null;
    }


    public long getCreationTimeStamp() {
        return creationTimeStamp;
    }
}
