package com.avanza.core.sdo.sessiondata;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import javax.faces.context.FacesContext;

//import org.apache.commons.collections4.*;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.security.db.UserDbImpl;

public class SessionDataManager {

    private static Map<String, SessionDataObject> sessionDataMap;

    public static Map<String, SessionDataObject> getSessionDataMap() {
		/* [unisonprime] if(FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SessionKeyLookup.SESSION_DATA_MAP)==null)
		{
			sessionDataMap = new HashMap<String, SessionDataObject>();
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(SessionKeyLookup.SESSION_DATA_MAP,sessionDataMap);
			return sessionDataMap;
		}
		else
		{
			return (Map<String, SessionDataObject>)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SessionKeyLookup.SESSION_DATA_MAP);
		}*/
        return null;
    }

    public static void addSessionDataObject(SessionDataObject sessionDataObject) {
        if (sessionDataObject.getDataObject().getSessionId() == null)
            sessionDataObject.getDataObject().createSessionId();
        getSessionDataMap().put(sessionDataObject.getDataObject().getSessionId(), sessionDataObject);
    }

    public static void removeSessionDataObject(SessionDataObject sessionDataObject) {
        removeSessionParam(sessionDataObject);
        getSessionDataMap().remove(sessionDataObject.getDataObject().getSessionId());
    }

    public static Map<String, SessionDataObject> getSessionDataObjectByGroup(String groupName) {
        Map<String, SessionDataObject> tempSessionDataMap = new HashMap<String, SessionDataObject>();

        for (SessionDataObject sessionDataObject : getSessionDataMap().values()) {
            if (sessionDataObject.getGroupName().equals(groupName))
                tempSessionDataMap.put(sessionDataObject.getDataObject().getSessionId(), sessionDataObject);
        }
        return tempSessionDataMap;
    }

    public static Map<String, SessionDataObject> getSessionDataObjectBySaveEvent(String eventName) {
        Map<String, SessionDataObject> tempSessionDataMap = new HashMap<String, SessionDataObject>();

        for (SessionDataObject sessionDataObject : getSessionDataMap().values()) {
            if (sessionDataObject.getSaveEvent().equals(eventName))
                tempSessionDataMap.put(sessionDataObject.getDataObject().getSessionId(), sessionDataObject);
        }
        return tempSessionDataMap;
    }

    public static Map<String, SessionDataObject> getSessionDataObjectBySaveEventAndGroup(String eventName, String groupName) {
        Map<String, SessionDataObject> tempSessionDataMap = new HashMap<String, SessionDataObject>();

        for (SessionDataObject sessionDataObject : getSessionDataMap().values()) {
            if (sessionDataObject.getSaveEvent().equals(eventName) && sessionDataObject.getGroupName().equals(groupName))
                tempSessionDataMap.put(sessionDataObject.getDataObject().getSessionId(), sessionDataObject);
        }
        return tempSessionDataMap;
    }


    public static Map<String, SessionDataObject> getSessionDataObjectByRemoveEvent(String eventName) {
        Map<String, SessionDataObject> tempSessionDataMap = new HashMap<String, SessionDataObject>();

        for (SessionDataObject sessionDataObject : getSessionDataMap().values()) {
            if (sessionDataObject.getRemoveEvent().equals(eventName))
                tempSessionDataMap.put(sessionDataObject.getDataObject().getSessionId(), sessionDataObject);
        }
        return tempSessionDataMap;
    }


    public static void removeSessionDataObjectBySaveEventAndGroup(String eventName, String groupName) {
        List<String> tempSessionDataMapKeys = new ArrayList<String>();

        for (SessionDataObject sessionDataObject : getSessionDataMap().values()) {
            if (sessionDataObject.getSaveEvent().equals(eventName) && sessionDataObject.getGroupName().equals(groupName)) {
                removeSessionParam(sessionDataObject);
                tempSessionDataMapKeys.add(sessionDataObject.getDataObject().getSessionId());
            }
        }

        for (String key : tempSessionDataMapKeys) {
            getSessionDataMap().remove(key);
        }

    }

    public static void removeSessionDataObjectByRemoveEvent(String eventName) {
        List<String> tempSessionDataMapKeys = new ArrayList<String>();

        for (SessionDataObject sessionDataObject : getSessionDataMap().values()) {
            if (sessionDataObject.getRemoveEvent().equals(eventName)) {
                removeSessionParam(sessionDataObject);
                tempSessionDataMapKeys.add(sessionDataObject.getDataObject().getSessionId());
            }
        }

        for (String key : tempSessionDataMapKeys) {
            getSessionDataMap().remove(key);
        }

    }

    public static void removeSessionParam(SessionDataObject sessionDataObject) {
		/* [unisonprime] if(sessionDataObject.getRemoveSessionParmList().isEmpty())
			return;
		else
		{
			for(String paramKey :sessionDataObject.getRemoveSessionParmList())
			{
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(paramKey);
			}
			
		}*/
        return;
    }

    public static SessionDataObject fillRequiredParam(SessionDataObject sessionDataObject) {
        if (sessionDataObject.getRequiredParmMap().isEmpty())
            return sessionDataObject;
        else {
            for (String paramKey : sessionDataObject.getRequiredParmMap().keySet()) {
                sessionDataObject.getDataObject().setValue(sessionDataObject.getRequiredParmMap().get(paramKey), getParamValue(paramKey));
            }

        }

        return sessionDataObject;
    }

    public static Object getParamValue(String key) {

		/* [unisonprime] Object retVal = (Object) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);
		
		if (retVal == null) {
			retVal = (Object) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(key);
		}
		if (retVal == null) {
			retVal = (Object) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(key);
		}
		if (retVal == null) {
			retVal = (Object) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
		}

		return retVal;*/
        return null;
    }

    public static void saveSessionDataObject(SessionDataObject sessionDataObject) {
        fillRequiredParam(sessionDataObject);
		
	 	/*[unsisonprime]  UserDbImpl user = (UserDbImpl) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(SessionKeyLookup.CURRENT_USER_KEY);
		DataObject obj = sessionDataObject.getDataObject();
		
		 if (obj.getValue("CREATED_ON") == null || obj.getValue("CREATED_ON").toString().length() <= 0) 
		 {
             obj.setValue("CREATED_ON", Calendar.getInstance().getTime());
             obj.setValue("CREATED_BY", user.getLoginId());
		 }
		 obj.setValue("UPDATED_ON", Calendar.getInstance().getTime());
		 obj.setValue("UPDATED_BY", user.getLoginId());
		 obj.save();*/
    }

}
