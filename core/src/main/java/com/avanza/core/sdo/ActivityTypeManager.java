package com.avanza.core.sdo;

import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Order;
import com.avanza.core.data.expression.OrderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.sessionhistory.ActivityType;
import com.avanza.core.util.StringHelper;

import java.util.List;

public class ActivityTypeManager {

    private ActivityTypeCatalog treeCatalog;
    private static ActivityTypeManager _instance;

    public static ActivityTypeManager getInstance() {
        if (_instance == null) {
            _instance = new ActivityTypeManager();
        }
        return _instance;
    }

    public void load() {
        initialize();
        treeCatalog.load();
    }

    public void initialize() {
        if (treeCatalog != null) return;

        treeCatalog = ActivityTypeCatalog.getInstance();
    }

    public void dispose() {
        treeCatalog.dispose();
    }

    public void synchronize() {
        treeCatalog.synchronize();
    }

    public List<ActivityType> getTree(String activityTypeId, int hierarchyLevel) {
        if (hierarchyLevel == 0)
            return treeCatalog.getActivitiesByLevel(hierarchyLevel);
        return treeCatalog.getSelectedActivitySuccessors(activityTypeId, hierarchyLevel);
    }

    public void delete(ActivityType currentActivityType) {
        if (currentActivityType != null)
            treeCatalog.deleteActivityType(currentActivityType);
    }

    public void persist(ActivityType currentActivityType) {
        if (currentActivityType != null)
            treeCatalog.persistActivityType(currentActivityType);
    }

    public List<ActivityType> getActivityTypeRoot() {
        return treeCatalog.getActivityTypeRoot();
    }

    public List<ActivityType> seacrhActivityType(String activity) {
        Search search = new Search(ActivityType.class);
        if (StringHelper.isNotEmpty(activity)) {
            search.addCriterion(Criterion.like("primaryName", "%" + activity + "%"));
            search.addOrder(new Order("primaryName", OrderType.ASC));
            List<ActivityType> activityList = treeCatalog.getActivityTypeBy(search);
            return activityList;
        }
        return null;
    }
}
