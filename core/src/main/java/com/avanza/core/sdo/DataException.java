package com.avanza.core.sdo;

import com.avanza.core.CoreException;

public class DataException extends CoreException {

    private static final long serialVersionUID = 5;

    public DataException(String message) {

        super(message);
    }

    public DataException(String format, Object... args) {

        super(format, args);
    }

    public DataException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public DataException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public DataException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public DataException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}
