package com.avanza.core.sdo.expression;

import com.avanza.core.util.StringHelper;

public enum ExpressionType {

    None(0), Constant(1), Attribute(2), Function(3);

    private int intValue;

    private ExpressionType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static ExpressionType fromString(String value) {

        ExpressionType retVal;

        if (StringHelper.isEmpty(value))
            retVal = ExpressionType.None;
        else
            retVal = ExpressionType.valueOf(value);

        return retVal;
    }
}