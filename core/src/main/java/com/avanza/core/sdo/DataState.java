package com.avanza.core.sdo;

public enum DataState {

    Unchanged(0), New(1), Modified(2), Deleted(3),
    UnLoaded(4), Transaction(5);

    DataState(int value) {

        this.stateValue = value;
    }

    private int stateValue;

    public int toInt() {

        return this.stateValue;
    }

    public static DataState fromInt(int value) {

        DataState retVal;

        switch (value) {

            case 0:
                retVal = DataState.Unchanged;
                break;
            case 1:
                retVal = DataState.New;
                break;
            case 2:
                retVal = DataState.Modified;
                break;
            case 3:
                retVal = DataState.Deleted;
                break;
            case 4:
                retVal = DataState.UnLoaded;
                break;
            case 5:
                retVal = DataState.Transaction;
                break;
            default:
                throw new IllegalArgumentException(String.format("[%1$d] is not identified as valid DataState value.", value));
        }

        return retVal;
    }
}
