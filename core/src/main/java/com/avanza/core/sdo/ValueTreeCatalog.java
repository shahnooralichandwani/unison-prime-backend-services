package com.avanza.core.sdo;

import com.avanza.core.CoreException;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.TransactionContext;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Order;
import com.avanza.core.data.expression.OrderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.meta.MetaCounter;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.ValueTreeNode;
import com.avanza.core.util.StringHelper;

import java.util.*;

public class ValueTreeCatalog {

    /*
     * static final String SELECT_STMT = "SELECT TREE_ID, TREE_NODE_NAME_PRM,
     * TREE_NODE_NAME_SEC, PARENT_NODE, " + "HIERARCHY_LEVEL, CREATED_ON,
     * CREATED_BY, UPDATED_ON, UPDATED_BY " + "FROM VALUE_TREE_NODE";
     */

    private HashMap<String, ValueTreeNode> treeMap;

    public ValueTreeCatalog() {

        this.treeMap = new HashMap<String, ValueTreeNode>();
    }

    public void load() {
        treeMap = new HashMap<String, ValueTreeNode>();
        DataBroker broker = DataRepository.getBroker(ValueTreeNode.class
                .getName());
        List<ValueTreeNode> retList = broker.findAll(ValueTreeNode.class);
        Collections.sort(retList, new Comparator<ValueTreeNode>() {
            public int compare(ValueTreeNode o1, ValueTreeNode o2) {
                return o1.getPrimaryName().compareToIgnoreCase(
                        o2.getPrimaryName());
            }
        });
        Hashtable<String, ValueTreeNode> tempMap = new Hashtable<String, ValueTreeNode>(
                retList.size());

        for (ValueTreeNode item : retList) {

            String key = item.getId().toLowerCase();
            tempMap.put(key, item);

            if (item.isRoot())
                this.treeMap.put(key, item);
        }

        for (ValueTreeNode item : retList) {

            if (!StringHelper.isEmpty(item.getParentId())) {
                String key = item.getParentId().toLowerCase();
                ValueTreeNode parentNode = tempMap.get(key);

                if (parentNode != null)
                    parentNode.add(item);
            }
        }
    }

    public void dispose() {

    }

    public void synchronize() {

    }

    public ValueTreeNode getTree(String key) {

        ValueTreeNode retVal = this.treeMap.get(key.toLowerCase());

        if (retVal == null)
            throw new DataException(
                    "No Value Tree found in catalog for key: [%1$s]", key);

        return retVal;
    }

    public HashMap<String, ValueTreeNode> getTree() {
        return this.treeMap;
    }

    public HashMap<String, ValueTreeNode> getTreeMap() {
        return treeMap;
    }

    public List<ValueTreeNode> getSelectedSuccessors(String valueTreeId,
                                                     int hierarchyLevel) {

        DataBroker broker = DataRepository.getBroker(ValueTreeNode.class
                .getName());
        List<ValueTreeNode> retList = new ArrayList<ValueTreeNode>(0);

        if (StringHelper.isEmpty(valueTreeId)) {
            Search query = new Search(ValueTreeNode.class);
            query.addCriterion(Criterion
                    .equal("hierarchyLevel", new Integer(1)));
            query.addOrder(new Order("primaryName", OrderType.ASC));
            retList = broker.find(query);
        } else {
            Search query = new Search(ValueTreeNode.class);
            // passing "parentId" instead of hierarchyLevel
            query.addCriterion(Criterion.and(Criterion.equal("parentId",
                    valueTreeId), Criterion.equal("hierarchyLevel",
                    hierarchyLevel)));
            query.addOrder(new Order("primaryName", OrderType.ASC));
            retList = broker.find(query);
        }
        return retList;
    }

    public void persistValueTree(ValueTreeNode currentValueTree) {
        DataBroker broker = DataRepository.getBroker(ValueTreeNode.class
                .getName());

        if (StringHelper.isEmpty(currentValueTree.getId()))
            currentValueTree.setId((String) MetaDataRegistry
                    .getNextCounterValue(MetaCounter.VALUE_TREE_COUNTER));

        broker.persist(currentValueTree);
        if (currentValueTree.isRoot())
            this.treeMap.put(currentValueTree.getId().toLowerCase(),
                    currentValueTree);
        else {
            ValueTreeNode parentNode = this.treeMap.get(currentValueTree
                    .getParentId().toLowerCase());
            if (parentNode != null)
                parentNode.addChild(currentValueTree);
        }
    }

    public void deleteValueTree(ValueTreeNode currentValueTree) {
        DataBroker broker = DataRepository.getBroker(ValueTreeNode.class
                .getName());
        TransactionContext txContext = ApplicationContext.getContext()
                .getTxnContext();

        txContext.beginTransaction();
        try {
            currentValueTree = broker.findById(ValueTreeNode.class,
                    currentValueTree.getId());
            if (currentValueTree != null)
                broker.delete(currentValueTree);
            txContext.commit();
            if (currentValueTree.isRoot())
                this.treeMap.remove(currentValueTree.getId().toLowerCase());
            else {
                ValueTreeNode parentNode = this.treeMap.get(currentValueTree
                        .getParentId().toLowerCase());
                if (parentNode != null)
                    parentNode.removeChild(currentValueTree.getId()
                            .toLowerCase());
            }

        } catch (Exception e) {
            txContext.rollback();
            throw new CoreException(
                    "Exception occured while Deleting ValueTreeNode", e);
        }
    }

    public List<ValueTreeNode> getValueTreeNodeBy(Search search) {
        DataBroker broker = DataRepository.getBroker(ValueTreeNode.class.getName());
        return broker.find(search);
    }
}
