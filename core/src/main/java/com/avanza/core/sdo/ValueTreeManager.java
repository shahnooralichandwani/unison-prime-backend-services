package com.avanza.core.sdo;

import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Order;
import com.avanza.core.data.expression.OrderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.meta.ValueTreeNode;
import com.avanza.core.util.StringHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ValueTreeManager {

    private static ValueTreeCatalog treeCatalog;
    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock readLock = readWriteLock.readLock();
    private static Lock writeLock = readWriteLock.writeLock();

    public static void load() {
        try {
            writeLock.lock();
            treeCatalog = new ValueTreeCatalog();
            ValueTreeManager.treeCatalog.load();
        } finally {
            writeLock.unlock();
        }
    }

    public static void reload() {
        //Shuwair
        try {
            readLock.lock();
            treeCatalog = new ValueTreeCatalog();
            ValueTreeManager.treeCatalog.load();
        } finally {
            readLock.unlock();

        }
    }

    public static void dispose() {

        ValueTreeManager.treeCatalog.dispose();
    }

    public static void synchronize() {

        ValueTreeManager.treeCatalog.synchronize();
    }

    public static ValueTreeNode getTree(String key) {

        try {
            readLock.lock();
            return ValueTreeManager.treeCatalog.getTree(key);
        } finally {
            readLock.unlock();
        }
    }

    public static List<ValueTreeNode> getTree() {
        if (treeCatalog == null)
            return new ArrayList<ValueTreeNode>();
        return new ArrayList<ValueTreeNode>(treeCatalog.getTreeMap().values());
    }

    public static HashMap<String, ValueTreeNode> getTree1() {
        try {
            readLock.lock();
            return ValueTreeManager.treeCatalog.getTree();
        } finally {
            readLock.unlock();
        }
    }

    public static void delete(ValueTreeNode currentValueTree) {
        if (currentValueTree != null)
            treeCatalog.deleteValueTree(currentValueTree);
    }

    public static void persist(ValueTreeNode currentValueTree) {
        if (currentValueTree != null)
            treeCatalog.persistValueTree(currentValueTree);
    }

    public static List<ValueTreeNode> getTree(String id, int hierarchyLevel) {
        if (hierarchyLevel == 0)
            return getTree();
        return treeCatalog.getSelectedSuccessors(id, hierarchyLevel);
    }

    public static List<ValueTreeNode> searchValueTreeNode(String valueTree) {
        Search search = new Search(ValueTreeNode.class);
        if (StringHelper.isNotEmpty(valueTree)) {
            search.addCriterion(Criterion.like("primaryName", "%" + valueTree + "%"));
            search.addOrder(new Order("primaryName", OrderType.ASC));
            List<ValueTreeNode> valueTreeNode = treeCatalog.getValueTreeNodeBy(search);
            return valueTreeNode;
        }
        return null;
    }
}
