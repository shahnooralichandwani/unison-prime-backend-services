package com.avanza.core.function;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.avanza.core.CoreException;

public class FunctionCatalog {

    private Map<String, Method> methodInfos;

    FunctionCatalog() {
        this.methodInfos = new HashMap<String, Method>();
    }

    void setMethodInfo(Map<String, Method> methodInfos) {
        this.methodInfos = methodInfos;
    }

    void addMethodInfo(String name, Method method) {

        if (this.methodInfos.containsKey(name))
            throw new CoreException("The overloaded methods in ExpressionClass for method %1$s is not Supported.", name);

        this.methodInfos.put(name.toLowerCase(), method);
    }

    public Method getMethodInfo(String methodName) {

        return this.methodInfos.get(methodName.toLowerCase());
    }
}
