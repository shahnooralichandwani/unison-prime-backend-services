package com.avanza.core.function.expression;

import java.lang.reflect.Method;

import com.avanza.core.function.FunctionCatalog;
import com.avanza.core.util.StringHelper;

public class ExpressionParser {

    private ParserContext context;

    public ExpressionParser(String expression, Class<?> returnType, FunctionCatalog catalog) {

        this.context = new ParserContext(expression, returnType, catalog);
    }

    public Expression parse() {

        Expression retVal = null;
        this.context.findFirst();

        Class<?> dataType = context.getReturnType();

        if (context.hasMore()) {

            TokenType token = context.getToken();

            if (token == TokenType.ContextStart) {
                retVal = this.parseContext(dataType);
                this.context.findNext();
            } else if (token == TokenType.ConstantStart) {
                retVal = this.parseConstant(dataType);
                this.context.findNext();
            } else if (token == TokenType.MethodStart) {
                retVal = this.parseMethod(dataType);
                this.context.findNext();
            } else if (token == TokenType.Variable) {
                retVal = this.parseVariable(dataType);
                this.context.findNext();
            } else
                retVal = Expression.createConstant(this.context.getExpressionText(), dataType);
        } else
            retVal = Expression.createConstant(this.context.getExpressionText(), dataType);

        if (context.hasMore()) context.throwException("Only Single root expression is expected");

        return retVal;
    }

    private Expression parseVariable(Class<?> dataType) {

        this.context.findNext();
        return Expression.createVariable(this.context.getString(false).trim(), dataType);
    }

    private Expression parseMethod(Class<?> dataType) {

        String methodName = context.getString(true);
        Expression methodExp = null;

        if (methodName.compareToIgnoreCase(TokenType.IF_ELSE) == 0) {

            methodExp = Expression.createIfElse(methodName, dataType);
        } else {

            methodExp = Expression.createMethod(methodName, dataType);
            Method methodInfo = context.getMethodInfo(methodName);
            methodExp.setMethodInfo(methodInfo);
        }

        boolean methodEnd = false;
        boolean isVar = false;
        while (context.hasMore() && (!methodEnd)) {
            if (!isVar) this.context.findNext();
            isVar = false;
            TokenType token = this.context.getToken();

            if (token == TokenType.ContextStart) {

                Expression colExp = this.parseContext(null);
                methodExp.add(colExp);
            } else if (token == TokenType.MethodStart) {

                Expression childMethod = this.parseMethod(null);
                methodExp.add(childMethod);
            } else if (token == TokenType.MethodEnd) {

                // Add last parameter if any
                if (this.context.getLastToken() == TokenType.Parameter) {

                    Expression constExp = Expression.createConstant(this.context.getString(false), null);
                    methodExp.add(constExp);
                }

                if (!methodName.equalsIgnoreCase(TokenType.IF_ELSE)) this.fillRemainingParams(methodExp);
                methodEnd = true;
            } else if (token == TokenType.ConstantStart) {

                Expression constExp = this.parseConstant(null);
                methodExp.add(constExp);
            } else if (token == TokenType.Variable) {

                Expression constExp = this.parseVariable(null);
                methodExp.add(constExp);
                isVar = true;

            } else if (token == TokenType.Parameter) {

                if (this.context.getLastToken() == TokenType.Parameter) {

                    Expression constExp = Expression.createConstant(this.context.getString(false), null);
                    methodExp.add(constExp);
                }
            } else {

                String errMsg = String.format("Invalid Expression. Token [%1$s] is not expected during methodParsing", token.toString());

                this.context.throwException(errMsg);
            }
        }

        if (!methodEnd)
            context.throwException(String.format("Method expression must end with [%1$s]", TokenType.METHOD_END));


        return methodExp;
    }


    private Expression parseContext(Class<?> dataType) {

        return this.commonParse(TokenType.ContextEnd, ExpressionType.Context, dataType, "Expected Context end token");
    }

    private Expression parseConstant(Class<?> dataType) {

        return this.commonParse(TokenType.ConstantEnd, ExpressionType.Constant, dataType, "Expected Constant end token");
    }

    private Expression commonParse(TokenType endToken, ExpressionType type, Class<?> dataType, String errorMsg) {

        this.context.findNext();

        if (!this.context.hasMore()) this.context.throwException(errorMsg);

        TokenType token = this.context.getToken();

        if (!endToken.toString().equalsIgnoreCase(token.toString())) this.context.throwException(errorMsg);

        Expression constExp = new Expression(this.context.getString(false), type, dataType);

        return constExp;
    }

    private void fillRemainingParams(Expression methodExp) {

        if (methodExp.getMethodInfo().isVarArgs()) return;
        Class<?>[] paramList = methodExp.getMethodInfo().getParameterTypes();

        for (int index = methodExp.size(); index < paramList.length; index++) {

            Expression temp = Expression.createConstant(StringHelper.EMPTY, paramList[index]);
            methodExp.add(temp);
        }
    }

    /*
     * private Class<?> getParameterType(Class<?>[] paramList, int paramIdx, boolean isVarArgs) {
     *
     * if( (isVarArgs) && (paramIdx >= paramList.length) ) {
     *
     * String errMsg = String.format("More parameters defined in method expression. Expected [%d]", paramList.length);
     *
     * this.context.throwException(errMsg); }
     *
     * return paramList[paramIdx]; }
     */
}