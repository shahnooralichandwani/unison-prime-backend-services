package com.avanza.core.function.expression;

import com.avanza.core.CoreException;

public enum TokenType {

    None(0), ContextStart(1), ContextEnd(2), MethodStart(3), MethodEnd(4), Parameter(5),
    ConstantStart(6), ConstantEnd(7), Self(8), IfElse(9), Variable(10);

    private static final String NONE = "None";
    public static final String CONTEXT_START = "{";
    public static final String CONTEXT_END = "}";
    public static final String METHOD_START = "(";
    public static final String METHOD_END = ")";
    public static final String PARAMETER = ",";
    public static final String CONSTANT_START = "'";
    public static final String CONSTANT_END = "'";
    public static final String SELF = "this";
    public static final String IF_ELSE = "ifelse";
    public static final String VARIABLE = "@";


    public static final String REGEX_KEYWORDS = "[\\(\\)\\,\\{\\}\\'\\@]";

    private int intValue;

    private TokenType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public String toString() {

        String retVal = null;
        if (this == TokenType.ConstantStart)
            retVal = TokenType.CONSTANT_START;
        else if (this == TokenType.ContextEnd)
            retVal = TokenType.CONSTANT_END;
        else if (this == TokenType.MethodStart)
            retVal = TokenType.METHOD_START;
        else if (this == TokenType.MethodEnd)
            retVal = TokenType.METHOD_END;
        else if (this == TokenType.Parameter)
            retVal = TokenType.PARAMETER;
        else if (this == TokenType.ConstantStart)
            retVal = TokenType.CONSTANT_START;
        else if (this == TokenType.ConstantEnd)
            retVal = TokenType.CONSTANT_END;
        else if (this == TokenType.Self)
            retVal = TokenType.SELF;
        else if (this == TokenType.IfElse)
            retVal = TokenType.IF_ELSE;
        else if (this == TokenType.Variable)
            retVal = TokenType.VARIABLE;
        else
            retVal = TokenType.NONE;

        return retVal;
    }

    public static TokenType fromString(String value) {

        TokenType retVal = TokenType.None;

        if (TokenType.NONE.equalsIgnoreCase(value))
            retVal = TokenType.None;
        else if (TokenType.CONTEXT_START.compareTo(value) == 0)
            retVal = TokenType.ContextStart;
        else if (TokenType.CONTEXT_END.compareTo(value) == 0)
            retVal = TokenType.ContextEnd;
        else if (TokenType.METHOD_START.compareTo(value) == 0)
            retVal = TokenType.MethodStart;
        else if (TokenType.METHOD_END.compareTo(value) == 0)
            retVal = TokenType.MethodEnd;
        else if (TokenType.PARAMETER.compareTo(value) == 0)
            retVal = TokenType.Parameter;
        else if (TokenType.CONSTANT_START.compareTo(value) == 0)
            retVal = TokenType.ConstantStart;
        else if (TokenType.CONSTANT_END.compareTo(value) == 0)
            retVal = TokenType.ConstantEnd;
        else if (TokenType.SELF.compareTo(value) == 0)
            retVal = TokenType.Self;
        else if (TokenType.IF_ELSE.compareTo(value) == 0)
            retVal = TokenType.IfElse;
        else if (TokenType.VARIABLE.compareTo(value) == 0)
            retVal = TokenType.Variable;
        else
            throw new CoreException("[%1$s] is not recognized as valid Token", value);

        return retVal;
    }

    public static TokenType fromInteger(int value) {

        TokenType retVal = TokenType.None;

        if (value == 0)
            retVal = TokenType.None;
        else if (value == 1)
            retVal = TokenType.ContextStart;
        else if (value == 2)
            retVal = TokenType.ContextEnd;
        else if (value == 3)
            retVal = TokenType.MethodStart;
        else if (value == 4)
            retVal = TokenType.MethodEnd;
        else if (value == 5)
            retVal = TokenType.Parameter;
        else if (value == 6)
            retVal = TokenType.ConstantStart;
        else if (value == 7)
            retVal = TokenType.ConstantEnd;
        else if (value == 8)
            retVal = TokenType.Self;
        else if (value == 9)
            retVal = TokenType.IfElse;
        else if (value == 10)
            retVal = TokenType.Variable;
        else
            throw new CoreException("[%1$d] is not recognized as valid Token Type", value);

        return retVal;
    }
}