package com.avanza.core.function;

import com.avanza.core.CoreException;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.ExecutionContext;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Guard;
import com.avanza.core.web.config.WebContext;

//TODO: UNISONPRIME
/*import com.avanza.core.security.PermissionBinding;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.workflow.configuration.org.OrganizationManager;
import com.avanza.workflow.configuration.org.Role;*/

import java.lang.reflect.Method;
import java.util.List;

/**
 * This class contains the method that will return objects from the diffenent
 * context.
 *
 * @author kraza
 */
public class ContextFunction {

    private ContextFunction() {
    }

    public static <T> T sessionValue(String sessionKey) {

        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        return (T) webContext.getAttribute(sessionKey);
    }

    public static <T> T executionContextValue(String exCtxValue) {

        ExecutionContext executionContext = ContextFunction.sessionValue(ExecutionContext.EXECUTION_CONTEXT_KEY);

        if (executionContext == null) {
            return null;
        }

        return (T) executionContext.getValue(exCtxValue);
    }

    public static <T> T threadValue(String threadKey) {

        return (T) ApplicationContext.getContext().get(threadKey);
    }

    public static <T> T tempValue(String tempKey) {

        return (T) ApplicationContext.getContext().get("TEMP_" + tempKey);
    }

    /**
     * This method returns the property of the provided object.
     * getProperty(getProperty(sessionValue('Current_User'),'loginId'),'class')
     *
     * @param <R>
     * @param <T>
     * @param object
     * @param property property of the object, there must be the method getProperty() in
     *                 the object's Class.
     * @return
     * @throws Exception
     */
    public static <R, T> R getProperty(T object, String property)
            throws Exception {

        String propertyName = "get" + property.substring(0, 1).toUpperCase() + property.substring(1);
        Method method = object.getClass().getMethod(propertyName, new Class[]{});

        return (R) method.invoke(object, new Object[]{});
    }

    /**
     * This method is used to assign the value to the temporary attribute
     * created in the Thread Context.
     *
     * @param <T>
     * @param name
     * @param value
     * @return
     */
    public static <T> boolean assignTemp(String name, T value) {

        Guard.checkNull(value, "assign(T value)");
        Guard.checkNullOrEmpty(name, "assign(String name)");
        ApplicationContext.getContext().add("TEMP_" + name, value);
        return true;
    }

	/*public static <T> T viewrootValue(String key) {

		return (T) FacesContext.getCurrentInstance().getViewRoot().getAttributes().get(key);
	}

	public static <T> T getFacesValue(String key) {

		T retVal = (T) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);

		if (retVal == null) {
			retVal = (T) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(key);
		}
		if (retVal == null) {
			retVal = (T) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
		}

		return retVal;
	}*/

    public static <T> T getCxtObjValue(String objName, String key, String scope)
            throws Exception {

        T retVal = null;
        DataObject dbObj = null;
        Object obj = null;
        if (scope.equalsIgnoreCase("session"))
            obj = sessionValue(objName);
        else if (scope.equalsIgnoreCase("execution"))
            obj = executionContextValue(objName);
        else if (scope.equalsIgnoreCase("thread"))
            obj = threadValue(objName);
        else if (scope.equalsIgnoreCase("temp"))
            obj = tempValue(objName);
        else
            throw new CoreException("scope not supported.");

        if (obj instanceof DataObject) {
            dbObj = (DataObject) obj;
            retVal = (T) dbObj.getValue(key);
        } else {
            retVal = (T) getProperty(obj, key);
        }

        return retVal;
    }

    //TODO: UNISONPRIME
	/*public static boolean hasViewPermission(String permissionId, Boolean canView) {
		boolean retVal = false;
		UserDbImpl user = (UserDbImpl) ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);
		PermissionBinding permissionBinding = user.getPermission(permissionId);
		if (permissionBinding != null & canView)
			retVal = true;
		return retVal;
	}

	public static boolean hasCreatePermission(String permissionId,
			Boolean canCreate) {
		boolean retVal = false;
		UserDbImpl user = (UserDbImpl) ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);
		PermissionBinding permissionBinding = user.getPermission(permissionId);
		if (permissionBinding != null && canCreate.equals(permissionBinding.getCanCreate()))
			retVal = true;
		return retVal;
	}

	public static boolean hasUpdatePermission(String permissionId,
			Boolean canUpdate) {
		boolean retVal = false;
		UserDbImpl user = (UserDbImpl) ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);
		PermissionBinding permissionBinding = user.getPermission(permissionId);
		if (permissionBinding != null && canUpdate.equals(permissionBinding.getCanUpdate()))
			retVal = true;
		return retVal;
	}

	public static boolean hasDeletePermission(String permissionId,
			Boolean canDelete) {
		boolean retVal = false;
		UserDbImpl user = (UserDbImpl) ApplicationContext.getContext().get(SessionKeyLookup.CURRENT_USER_KEY);
		PermissionBinding permissionBinding = user.getPermission(permissionId);
		if (permissionBinding != null && canDelete.equals(permissionBinding.getCanDelete()))
			retVal = true;
		return retVal;
	}*/

    public static Boolean documentContains(String attribute, String... values) {

        boolean retVal = false;
        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        DataObject val = webCtx.getAttribute(SessionKeyLookup.CURRENT_OBJ);

        Object value = val.getValue(attribute);

        if (value != null) {

            String sValue = value.toString();

            for (int i = 0; i < values.length; i++) {

                if (values[i].equalsIgnoreCase(sValue))
                    retVal = true;
                break;
            }

        } else {
            return false;
        }

        return retVal;

    }

    //Return true if CURRENT_STATE is exits in given states
    public static Boolean isStateValid(String... values) {
        boolean retVal = false;
        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        DataObject val = webCtx.getAttribute(SessionKeyLookup.CURRENT_OBJ);

        Object value = val.getCurrentState();

        if (value != null) {
            String sValue = value.toString();

            for (int i = 0; i < values.length; i++) {
                if (values[i].equalsIgnoreCase(sValue)) {
                    retVal = true;
                    break;
                }
            }
        } else {
            return false;
        }

        return retVal;
    }


    //TODO: UNISONPRIME
    //Return true if logged in user role exits in given roles
	/*public static Boolean isRoleValid(String... values)
	{
		
		boolean retVal = false;
		WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
		DataObject user = webCtx.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
		
		List<Role> roles = OrganizationManager.getAllRoles(user.getAsString("LOGIN_ID"));
		for(Role role : roles)
		{
			for (int i = 0; i < values.length; i++) 
			{
				if(role.getRoleId().equalsIgnoreCase(values[i]))
				{
					retVal = true;
					break;
				}
			}
		}
		return retVal;
	}*/


    public static Object invokeMethod(Object instanceOrClassName, String methodName, Object... args)
            throws Exception {
        if (instanceOrClassName == null) {
            return null;
        }

        //manipulate instanceOrClassName
        Class<?> clazz;
        Object obj = null;
        if (instanceOrClassName instanceof String) {
            clazz = Class.forName((String) instanceOrClassName);
        } else {
            clazz = instanceOrClassName.getClass();
            obj = instanceOrClassName;
        }

        Method method = getMethod(clazz, methodName, args);
        if (method == null) {
            throw new Exception("Method Name: " + methodName + " not defined please note same method name with same number of argument is not supported");
        }

        return method.invoke(obj, args);
    }

    private static Method getMethod(Class<?> clazz, String methodName, Object... args) {
        int argumentCount = (args == null) ? 0 : args.length;
        for (Method method : clazz.getMethods()) {
            if (method.getName().equals(methodName)) {
                if (method.getParameterTypes().length == argumentCount) {
                    return method;
                }
            }
        }
        return null;
    }

    /*
     * Shahbaz
     * This function is to be used to verify if the the context is available for the current session
     *
     */
    public static Boolean isContextAvailable() {
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);

        return (webContext != null);
    }

}
