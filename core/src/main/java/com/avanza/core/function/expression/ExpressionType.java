package com.avanza.core.function.expression;


import com.avanza.core.util.StringHelper;

public enum ExpressionType {

    None(0), Constant(1), Context(2), Method(3), IfElse(4), Variable(5);

    private int intValue;

    private ExpressionType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static ExpressionType fromString(String value) {

        ExpressionType retVal;

        if (StringHelper.isEmpty(value))
            retVal = ExpressionType.None;
        else
            retVal = ExpressionType.valueOf(value);

        return retVal;
    }
}