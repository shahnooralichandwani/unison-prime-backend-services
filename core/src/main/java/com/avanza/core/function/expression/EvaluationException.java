package com.avanza.core.function.expression;

import com.avanza.core.CoreException;

public class EvaluationException extends CoreException {

    private static final long serialVersionUID = -4995102160837813670L;

    public EvaluationException(String message) {

        super(message);
    }

    public EvaluationException(String format, Object... args) {

        super(format, args);
    }

    public EvaluationException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public EvaluationException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public EvaluationException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public EvaluationException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}
