package com.avanza.core.function.expression;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Map;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Convert;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.Guard;
import com.avanza.core.web.config.WebContext;


public class ExpressionEvaluator {

    private static ExpressionEvaluator _instance = new ExpressionEvaluator();

    private Object currentCxtObject;
    private Object thisVal;


    public static ExpressionEvaluator getInstance() {

        return _instance;
    }

    public ExpressionEvaluator() {

        this.currentCxtObject = null;
        this.thisVal = null;
    }

    public ExpressionEvaluator(DataObject cxtObject, Object thisVal) {

        this.currentCxtObject = cxtObject;
        this.thisVal = thisVal;
    }

    public ExpressionEvaluator(Object thisVal) {

        this.thisVal = thisVal;
    }

    public ExpressionEvaluator(Map cxtObject, Object thisVal) {

        Guard.checkNull(cxtObject, "ExpressionEvaluator.ExpressionEvaluator(Map, Object)");
        this.currentCxtObject = cxtObject;
        this.thisVal = thisVal;
    }

    public Object evaluate(Expression exp) {


        Object retVal = null;
        try {
            ExpressionType type = exp.getType();

            if (type == ExpressionType.Constant)
                retVal = this.evaluateConstant(exp);
            else if (type == ExpressionType.Method)
                retVal = this.evaluateMethod(exp);
            else if (type == ExpressionType.IfElse)
                retVal = this.evaluateIfElse(exp);
            else if (type == ExpressionType.Context)
                retVal = this.evaluateContext(exp);
            else if (type == ExpressionType.Variable)
                retVal = this.evaluateVariable(exp);
            else
                retVal = ConvertUtil.ConvertTo(exp.getText(), exp.getReturnType());
        } catch (Exception e) {
            throw new EvaluationException(e, "Exception in " + exp.toString());
        }
        return retVal;
    }

    private Object evaluateVariable(Expression exp) {
        Object retVal = null;
        String name = exp.getText();

        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);

        if (name.equalsIgnoreCase("CurrentObj")) {
            retVal = webCtx.getAttribute(SessionKeyLookup.CURRENT_OBJ);
        } else if (name.equalsIgnoreCase("Customer")) {
            retVal = webCtx.getAttribute(SessionKeyLookup.CURRENT_CUSTOMER_KEY);
        } else if (name.equalsIgnoreCase("User")) {
            retVal = webCtx.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
        }

        if (retVal != null) retVal = ConvertUtil.ConvertTo(retVal, exp.getReturnType());
        return retVal;
    }

    private Object evaluateConstant(Expression exp) {

        Object retVal = null;
        Class<?> expRetType = exp.getReturnType();

        if (expRetType != null)
            retVal = ConvertUtil.parse(exp.getText(), expRetType);
        else
            retVal = exp.getText();

        return retVal;
    }

    private Object evaluateMethod(Expression exp) {

        Object retVal;
        Object[] expParams = null;
        Method methodInfo = exp.getMethodInfo();
        Class<?> expReturnType = exp.getReturnType();
        Class<?> fncReturnType = methodInfo.getReturnType();

        if (methodInfo.isVarArgs())
            expParams = this.prepareVarParams(exp);
        else
            expParams = this.prepareParams(exp);

        try {
            retVal = methodInfo.invoke(null, expParams);
            if ((expReturnType != null) && (fncReturnType != expReturnType))
                retVal = ConvertUtil.ConvertTo(retVal, expReturnType);
        } catch (Exception ex) {
            StringBuilder builder = new StringBuilder(100);
            builder.append("Failed to execute Fucnction ").append(methodInfo.toString());
            builder.append(". Parameter Values: [");
            for (int idx = 0; idx < expParams.length; idx++) {
                if (idx > 0) builder.append(',');
                builder.append(expParams[idx].toString());
            }

            builder.append("]");

            throw new ParserException(ex, builder.toString());
        }

        return retVal;
    }


    private Object evaluateContext(Expression exp) {

        Object retVal = null;
        String name = exp.getText();

        if (name.equalsIgnoreCase(TokenType.SELF)) {
            retVal = this.thisVal;
        } else if (this.currentCxtObject != null) {
            if (this.currentCxtObject instanceof DataObject)
                retVal = ((DataObject) this.currentCxtObject).getValue(name);
            if (this.currentCxtObject instanceof Map) retVal = ((Map) this.currentCxtObject).get(name);
        }
        if (retVal != null) retVal = ConvertUtil.ConvertTo(retVal, exp.getReturnType());
        return retVal;
    }

    private Object[] prepareParams(Expression exp) {

        int paramCnt = exp.size();

        Object[] expParams = new Object[paramCnt];
        ;

        for (int index = 0; index < paramCnt; index++) {

            Expression temp = exp.get(index);
            expParams[index] = this.evaluate(temp);
        }

        return expParams;
    }

    private Object evaluateIfElse(Expression exp) {

        int paramCnt = exp.size();
        Object retVal = null;
        int end = (paramCnt % 2) == 0 ? paramCnt : (paramCnt - 2);
        boolean condition = false;
        // Executing if & elseif clause.
        for (int index = 0; index < end; index = index + 2) {

            Expression evalTemp = exp.get(index);
            condition = Convert.toBoolean(this.evaluate(evalTemp));

            // break when first evaluation expression returns true
            if (condition) {
                Expression execTemp = exp.get(index + 1);
                retVal = this.evaluate(execTemp);
                break;
            }
        }

        // executing Elese clause
        if (paramCnt % 2 == 1 && condition == false) {

            Expression execTemp = exp.get(paramCnt - 1);
            retVal = this.evaluate(execTemp);
        }
        retVal = ConvertUtil.ConvertTo(retVal, exp.getReturnType());
        return retVal;

        /*
         * if (index % 2 == 0) { if (expParams[index] instanceof Boolean) conditionResult = (Boolean) expParams[index]; else throw new
         * ExpressionException( "Exception occured in ExpressionEvaluation.prepareIfElse(Expression exp): Even arguments must be of type
         * java.lang.Boolean"); } else if (conditionResult)
         *
         * return;
         *
         */
    }

    private Object[] prepareVarParams(Expression exp) {

        Class<?>[] paramList = exp.getParameterTypes();

        int paramCnt = paramList.length;
        int varParamCnt = exp.size() - paramCnt + 1;


        Class<?> arrayType = paramList[paramCnt - 1].getComponentType();
        Object varParamList = Array.newInstance(arrayType, varParamCnt);

        Object[] expParams = new Object[paramCnt];

        int paramIdx = 0;
        for (; paramIdx < paramCnt - 1; paramIdx++) {

            Expression temp = exp.get(paramIdx);
            expParams[paramIdx] = this.evaluate(temp);
        }

        for (int varIdx = 0; varIdx < varParamCnt; varIdx++, paramIdx++) {

            Expression temp = exp.get(paramIdx);
            Array.set(varParamList, varIdx, this.evaluate(temp));
        }

        expParams[paramCnt - 1] = varParamList;

        return expParams;
    }
}