package com.avanza.core.function.expression;

import java.lang.reflect.Method;
import java.util.ArrayList;

import com.avanza.core.util.Guard;


public class Expression {

    private ExpressionType type;
    private String text;
    private ArrayList<Expression> expList;
    private Class<?> returnType;
    private Method methodInfo;

    public Expression(String text, ExpressionType type, Class<?> dataType) {

        this.init(text, type, dataType);
    }

    public ExpressionType getType() {
        return this.type;
    }

    void setType(ExpressionType type) {
        this.type = type;
    }

    public Class<?> getReturnType() {

        return this.returnType;
    }

    void setReturnType(Class<?> returnType) {

        this.returnType = returnType;
    }

    public Method getMethodInfo() {

        return this.methodInfo;
    }

    Class<?>[] getParameterTypes() {

        return this.methodInfo.getParameterTypes();
    }

    void setMethodInfo(Method methodInfo) {

        Guard.checkNull(methodInfo, "Expression.setMethodInfo(Method)");
        this.methodInfo = methodInfo;
    }

    public String getText() {
        return this.text;
    }

    void setText(String text) {

        Guard.checkNull(text, "Expression.setText(String)");

        //text = text.trim();
        this.text = text;

        //if (StringHelper.isEmpty(text))
        //return;
    }

    public int size() {
        return this.expList.size();
    }

    public Expression get(int index) {
        return this.expList.get(index);
    }

    public void set(int index, Expression element) {
        this.expList.set(index, element);
    }

    public void add(Expression element) {

        if (this.type == ExpressionType.Method) {

            Class<?>[] paramList = this.methodInfo.getParameterTypes();
            int paramLen = paramList.length;
            int size = this.expList.size();
            boolean isVarArgs = this.methodInfo.isVarArgs();


            if ((!isVarArgs) && (paramLen < size)) {

                throw new ParserException("Invalid number of parameters. Method %1$s expects %2$d parameters", this.text, paramList.length);

            }

            if ((isVarArgs) && (size >= (paramLen - 1))) {

                Class<?> lastParamType = paramList[paramList.length - 1];
                element.setReturnType(lastParamType.getComponentType());
            } else {
                element.setReturnType(paramList[size]);
            }

            this.expList.add(element);

        } else if (this.type == ExpressionType.IfElse) {

            this.expList.add(element);
        }
    }

    public void remove(Expression element) {

        this.expList.remove(element);
    }

    public Expression remove(int idx) {
        return this.expList.remove(idx);
    }

    public String toString() {
        return this.text;
    }

    public static Expression createConstant(String text, Class<?> dataType) {

        return new Expression(text.trim(), ExpressionType.Constant, dataType);
    }

    public static Expression createMethod(String text, Class<?> dataType) {

        return new Expression(text, ExpressionType.Method, dataType);
    }

    public static Expression createIfElse(String text, Class<?> dataType) {

        return new Expression(text, ExpressionType.IfElse, dataType);
    }

    public static Expression createContext(String text, Class<?> dataType) {

        return new Expression(text, ExpressionType.Context, dataType);
    }

    public static Expression createVariable(String name, Class<?> dataType) {

        return new Expression(name, ExpressionType.Variable, dataType);
    }

    private void init(String text, ExpressionType type, Class<?> returnType) {

        this.type = type;
        this.setText(text);
        this.returnType = returnType;
        this.expList = new ArrayList<Expression>(4);
    }


}