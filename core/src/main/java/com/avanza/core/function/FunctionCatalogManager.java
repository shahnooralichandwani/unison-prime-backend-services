package com.avanza.core.function;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class maintains the list of the Function Classes.
 *
 * @author kraza
 */
public class FunctionCatalogManager {

    private static FunctionCatalogManager _instance;

    private Map<Class<?>, Map<String, Method>> functionsList;
    private Map<ExpressionClass[], FunctionCatalog> functionCatalog;

    public static synchronized FunctionCatalogManager getInstance() {
        if (_instance == null) {
            _instance = new FunctionCatalogManager();
            _instance.load();
        }
        return _instance;
    }

    protected void load() {

        this.functionsList = new HashMap<Class<?>, Map<String, Method>>();
        this.functionCatalog = new HashMap<ExpressionClass[], FunctionCatalog>();
        for (ExpressionClass expClass : ExpressionClass.values()) {

            Map<String, Method> methodList = new HashMap<String, Method>();
            for (Method method : expClass.getClassObject().getMethods()) {

                if (Modifier.isStatic(method.getModifiers()))
                    methodList.put(method.getName(), method);
            }
            this.functionsList.put(expClass.getClassObject(), methodList);
        }
    }

    public FunctionCatalog getFunctionCatalog(ExpressionClass... expClasses) {

        FunctionCatalog functionCatalog = this.functionCatalog.get(expClasses);

        if (functionCatalog != null)
            return functionCatalog;

        functionCatalog = new FunctionCatalog();

        for (ExpressionClass expClass : expClasses) {

            for (Entry<String, Method> entry : this.functionsList.get(expClass.getClassObject()).entrySet()) {

                functionCatalog.addMethodInfo(entry.getKey(), entry.getValue());
            }
        }

        return functionCatalog;
    }
}
