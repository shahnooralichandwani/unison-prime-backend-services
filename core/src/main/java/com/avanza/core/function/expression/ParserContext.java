package com.avanza.core.function.expression;

import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.avanza.core.function.FunctionCatalog;
import com.avanza.core.util.Guard;

class ParserContext {

    private String expressionText;
    private Matcher matchList;
    private int lastMatchEnd;
    private boolean more;
    private TokenType lastToken;
    private TokenType currentToken;
    private Class<?> returnType;
    private FunctionCatalog catalog;

    public ParserContext(String expressionText, Class<?> returnType, FunctionCatalog catalog) {

        Guard.checkNullOrEmpty(expressionText, "ParserContext.ParserContext(String, Class, FunctionCatalog)");
        Guard.checkNull(returnType, "ParserContext.ParserContext(String, Class, FunctionCatalog)");
        Guard.checkNull(catalog, "ParserContext.ParserContext(String, Class, FunctionCatalog)");

        this.expressionText = expressionText;
        Pattern pattern = Pattern.compile(TokenType.REGEX_KEYWORDS);
        this.matchList = pattern.matcher(this.expressionText.toLowerCase());
        this.lastToken = TokenType.None;
        this.currentToken = TokenType.None;
        this.returnType = returnType;
        this.catalog = catalog;
    }

    public String getExpressionText() {

        return this.expressionText;
    }

    public boolean hasMore() {

        return this.more;
    }

    public TokenType getToken() {

        return this.currentToken;
    }

    public TokenType getLastToken() {

        return this.lastToken;
    }

    public void findFirst() {

        this.more = this.matchList.find();

        if (this.more) {

            this.lastToken = this.currentToken;
            this.currentToken = TokenType.fromString(this.matchList.group());
        }
    }

    public void findNext() {

        this.lastMatchEnd = this.matchList.end();
        this.findFirst();
    }

    public String getString(Boolean isMethod) {

        String retVal;

        if (this.more)
            retVal = this.expressionText.substring(this.lastMatchEnd, this.matchList.start());
        else
            retVal = this.expressionText.substring(this.lastMatchEnd);

        if (isMethod)
            return retVal.trim();
        else
            return retVal;
    }

    public void throwException(String message) {

        throw new ParserException("Error at position %d. %s. Expression[%s]",
                this.matchList.start(), message, this.expressionText);
    }

    public Class<?> getReturnType() {

        return this.returnType;
    }

    public Method getMethodInfo(String methodName) {

        Method retVal = this.catalog.getMethodInfo(methodName);

        if (retVal == null)
            this.throwException(String.format("[%1$s] is not recognized as a method within the identified context", methodName));

        return retVal;
    }
}