package com.avanza.core.function;

import com.avanza.core.calendar.CalendarFunctions;
import com.avanza.core.data.validation.DataValidationFunctions;
import com.avanza.workflow.execution.WorkflowExcutionFunctions;


public enum ExpressionClass {

    ExpressionFunction(ExpressionFunction.class), ContextFunction(ContextFunction.class),
    WorkFlowFunctions(WorkflowExcutionFunctions.class), DataValidatorFunctions(DataValidationFunctions.class), ControlFunction(ControlFunction.class), CalendarFunctions(
            CalendarFunctions.class);


    private Class<?> clazz;

    ExpressionClass(Class<?> clazz) {
        this.clazz = clazz;
    }

    public Class<?> getClassObject() {
        return this.clazz;
    }
}
