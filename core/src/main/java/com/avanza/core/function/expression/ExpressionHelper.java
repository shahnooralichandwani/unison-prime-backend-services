package com.avanza.core.function.expression;

import java.util.ArrayList;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.validation.DataValidationFunctions;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.config.WebContext;

public class ExpressionHelper {

    public static Object getExpressionResult(String expression, Object object) {

        Object result = solveExpression(expression, object.getClass());

        return result;

    }

    public static Object getExpressionResult(String expression, Class object) {

        Object result = solveExpression(expression, object);

        return result;

    }

    private static Object solveExpression(String expression, Class object) {
        ExpressionParser parser;
        parser = new ExpressionParser(expression, object, FunctionCatalogManager.getInstance().getFunctionCatalog(ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction, ExpressionClass.ControlFunction, ExpressionClass.DataValidatorFunctions));
        Expression retVal = parser.parse();
        WebContext webCtx = ApplicationContext.getContext().get(WebContext.class);
        ExpressionEvaluator eval;

        // Shahbaz: check if webCtx is null
        if (webCtx == null) {
            eval = ExpressionEvaluator.getInstance();
        } else {
            DataObject val = webCtx.getAttribute(SessionKeyLookup.CURRENT_OBJ);
            if (val != null)
                eval = new ExpressionEvaluator(val, StringHelper.EMPTY);
            else
                eval = ExpressionEvaluator.getInstance();
        }

        Object result = eval.evaluate(retVal);
        return result;
    }

    public static Object getClassObject(ExpressionClass expClass) {

        return expClass.getClassObject();

    }

    public static void resetErrorsList(ExpressionClass expClass) {

        if (expClass == ExpressionClass.DataValidatorFunctions)
            DataValidationFunctions.resetErrorList();

    }

    public static ArrayList<String> getErrorsList(ExpressionClass expClass) {

        if (expClass == ExpressionClass.DataValidatorFunctions)
            return DataValidationFunctions.getErrorList();

        return null;

    }
}
