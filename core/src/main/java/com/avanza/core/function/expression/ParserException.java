package com.avanza.core.function.expression;

import com.avanza.core.CoreException;

public class ParserException extends CoreException {

    private static final long serialVersionUID = -9183233681330032089L;

    public ParserException(String message) {

        super(message);
    }

    public ParserException(String format, Object... args) {

        super(format, args);
    }

    public ParserException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public ParserException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public ParserException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public ParserException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}
