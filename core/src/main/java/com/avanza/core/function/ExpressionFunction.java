package com.avanza.core.function;

import com.avanza.core.CoreException;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.MetaDataBroker;
import com.avanza.core.meta.PickListManager;
import com.avanza.core.meta.PicklistData;
import com.avanza.core.product.ProductCatalog;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.Convert;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.Guard;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ExpressionFunction {

    private ExpressionFunction() {
    }

    public static int len(String value) {

        if (value == null)
            value = StringHelper.EMPTY;

        return value.length();
    }

    public static Object sum(Object... objects) {

        Class<?>[] dataTypes = {Double.class, Long.class, Integer.class};
        Boolean retTypeSet = false;
        Class<?> retClass = null;

        for (int dataTypeCount = 0; dataTypeCount < 3; dataTypeCount++) {
            retClass = dataTypes[dataTypeCount];
            for (Object object : objects) {
                if (object.getClass() == retClass) {
                    retTypeSet = true;
                    break;
                }
            }
            if (retTypeSet)
                break;
        }
        double retValue = 0.0;
        for (Object object : objects) {
            if (object.getClass() == dataTypes[0]) {
                retValue += Convert.toDouble(object);
            } else if (object.getClass() == dataTypes[1]) {
                retValue += Convert.toLong(object);
            } else if (object.getClass() == dataTypes[2]) {
                retValue += Convert.toInteger(object);
            } else
                retValue += Convert.toDouble(object);
            // throw new ExpressionException("DataType %1$s is not a valid
            // Datatype for this method", object.getClass().getName());
        }

        if (retClass.getName().equalsIgnoreCase(dataTypes[0].getName())) {
            return retValue;
        } else if (retClass.getName().equalsIgnoreCase(dataTypes[1].getName())) {
            return Convert.toLong(retValue);
        } else if (retClass.getName().equalsIgnoreCase(dataTypes[2].getName())) {
            return Convert.toInteger(retValue);
        }

        return null;
    }

    public static Object differnce(Object arg1, Object arg2) {

        double retValue = 0.0;
        Class<?>[] dataTypes = {Double.class, Long.class, Integer.class};
        Boolean retTypeSet = false;
        Class<?> retClass = null;

        if (arg1.getClass() == dataTypes[0] || arg2.getClass() == dataTypes[0])
            retClass = dataTypes[0];
        else if (arg1.getClass() == dataTypes[1]
                || arg2.getClass() == dataTypes[1])
            retClass = dataTypes[1];
        else if (arg1.getClass() == dataTypes[2]
                || arg2.getClass() == dataTypes[2])
            retClass = dataTypes[2];

        retValue = Convert.toDouble(arg1) - Convert.toDouble(arg2);

        if (retClass.getName().equalsIgnoreCase(dataTypes[0].getName())) {
            return retValue;
        } else if (retClass.getName().equalsIgnoreCase(dataTypes[1].getName())) {
            return Convert.toLong(retValue);
        } else if (retClass.getName().equalsIgnoreCase(dataTypes[2].getName())) {
            return Convert.toInteger(retValue);
        }

        return null;
    }

    public static String trim(String value) {

        return (value == null) ? StringHelper.EMPTY : value.trim();
    }

    public static String replace(String value, String oldValue, String newValue) {

        if (StringHelper.isEmpty(value) && StringHelper.isEmpty(oldValue))
            return newValue;

        if (StringHelper.isEmpty(value))
            return value;

        if (oldValue == null)
            return oldValue = StringHelper.EMPTY;

        if (newValue == null)
            newValue = StringHelper.EMPTY;

        return value.replace(oldValue, newValue);
    }

    /**
     * Given variable argument list of strings this method does the string
     * concatenation.
     *
     * @param strings
     * @return
     */
    public static String concat(String... strings) {

        if (strings == null || strings.length == 0)
            return StringHelper.EMPTY;

        StringBuilder sb = new StringBuilder(strings.length * 100);
        for (String str : strings) {
            sb.append(str);
        }

        return sb.toString();
    }

    /**
     * Given the string value this method returns the substring based on the
     * startPos and length specified.
     *
     * @param value
     * @param startPos
     * @param length
     * @return
     */
    public static String subString(String value, int startPos, int length) {

        if (StringHelper.isEmpty(value))
            return value;

        if (startPos < 0)
            startPos = 0;

        if (startPos > value.length())
            return StringHelper.EMPTY;
        else if (((startPos + length) > value.length()) || (length <= 0))
            return value.substring(startPos);
        else
            return value.substring(startPos, length);
    }

    public static String padLeft(String value, char padVal, int length) {

        return StringHelper.padLeft(value, padVal, length);
    }

    public static String padRight(String value, char padVal, int length) {

        return StringHelper.padRight(value, padVal, length);
    }

    public static String rTrim(String value, char trimVal) {

        return StringHelper.rTrim(value, trimVal);
    }

    public static String lTrim(String value, char trimVal) {

        return StringHelper.lTrim(value, trimVal);
    }

    public static int indexOf(String thisVal, String findVal) {

        if (StringHelper.isEmpty(thisVal))
            return -1;

        if (StringHelper.isEmpty(findVal))
            return -1;

        return thisVal.indexOf(findVal);
    }

    public static int lastIndexOf(String thisVal, String findVal) {

        if (StringHelper.isEmpty(thisVal))
            return -1;

        if (StringHelper.isEmpty(findVal))
            return -1;

        return thisVal.lastIndexOf(findVal);
    }

    public static String toUpper(String arg1) {

        if (StringHelper.isEmpty(arg1))
            return arg1;

        return arg1.toUpperCase(getCurrentLocaleInfo().getLocale());
    }

    public static String toLower(String arg1) {

        if (StringHelper.isEmpty(arg1))
            return arg1;

        return arg1.toLowerCase(getCurrentLocaleInfo().getLocale());
    }

    public static boolean inStr(String value, String lookup, int startPos) {

        if ((value == null) && (lookup == null))
            return true;

        if ((value != null) && (lookup == null))
            return false;

        if ((value == null) && (lookup != null))
            return false;

        if (lookup.length() == 0)
            return true;

        startPos = startPos - 1;

        if (startPos > 0)
            return (value.indexOf(lookup, startPos) != -1);

        return (value.indexOf(lookup) != -1);

    }

    public static boolean geq(Double lhs, Double rhs) {

        if ((lhs == null) && (rhs != null))

            return false;

        if ((lhs != null) && (rhs == null))

            return true;

        return (lhs >= rhs);

    }

    public static boolean gt(Double lhs, Double rhs) {

        if ((lhs == null) && (rhs != null))

            return false;

        if ((lhs != null) && (rhs == null))

            return true;

        return (lhs > rhs);

    }

    public static boolean leq(Double lhs, Double rhs) {

        return !(ExpressionFunction.gt(lhs, rhs));

    }

    public static boolean ls(Double lhs, Double rhs) {

        return !(ExpressionFunction.geq(lhs, rhs));

    }

    public static String replicate(String value, int repeatCount) {

        if (StringHelper.isEmpty(value))
            return value;

        StringBuilder builder = new StringBuilder(value.length() * repeatCount);

        for (int i = 0; i < repeatCount; i++)
            builder.append(value);

        return builder.toString();
    }

    public static Object noValue(Object thisVal, Object defaultVal) {

        if (thisVal instanceof String) {

            String temp = (String) thisVal;
            if (!StringHelper.isEmpty(temp))
                defaultVal = temp;
        } else if (thisVal != null)
            defaultVal = thisVal;

        return defaultVal;
    }

    public static Boolean equal(Object arg1, Object arg2) {

        if ((arg1 == null) && (arg2 == null))
            return true;

        if ((arg1 == null) && (arg2 != null))
            return false;

        if ((arg1 != null) && (arg2 == null))
            return false;

        Object newArg2 = ConvertUtil.ConvertTo(arg2, arg1.getClass());

        if (arg1 instanceof String)
            return ((String) arg1).equalsIgnoreCase((String) newArg2);
        else
            return arg1.equals(newArg2);
    }

    public static Boolean neq(Object arg1, Object arg2) {

        return !ExpressionFunction.equal(arg1, arg2);
    }

    /**
     * This method returns one of the Objects provided according to the given
     * Boolean argument Farhan: It must be deprecated
     *
     * @param check
     * @param arg1
     * @param arg2
     * @return
     */
    /*
     * public static Object ifElseIf(Boolean check, Object arg1, Object arg2) {
     * return check ? arg1 : arg2; }
     */

    /**
     * This method takes Map Interface and returns its respective Object against
     * provided key
     *
     * @param arg1
     * @param arg2
     * @return
     */
    public static Object getMapValue(Map arg1, Object arg2) {
        return arg1.get(arg2);
    }

    /**
     * This method takes List and returns its respective Object against provided
     * index
     *
     * @param arg1
     * @param arg2
     * @return
     */
    public static Object getListValue(List<?> arg1, Object arg2) {

        return arg1.get(Convert.toInteger(arg2));
    }

    /**
     * This method returns AND result of all Boolean arguments
     *
     * @param booleans
     * @return
     */
    public static Boolean and(Boolean arg1, Boolean arg2, Boolean... booleans) {

        if (arg1 == null)
            arg1 = false;

        if (arg2 == null)
            arg2 = false;

        Boolean retVal = (arg1 && arg2);

        if (!retVal)
            return retVal;

        for (Boolean bool : booleans) {

            if (bool == null)
                bool = false;

            retVal = retVal && bool;

            if (!retVal)
                break;
        }

        return retVal;
    }

    /**
     * This method returns OR result of all Boolean arguments
     *
     * @param booleans
     * @return
     */
    public static Boolean or(Boolean arg1, Boolean arg2, Boolean... booleans) {

        if (arg1 == null)
            arg1 = false;

        if (arg2 == null)
            arg2 = false;

        Boolean retVal = (arg1 || arg2);

        if (retVal)
            return retVal;

        for (Boolean bool : booleans) {

            if (bool == null)
                bool = false;

            retVal = retVal || bool;

            if (retVal)
                break;
        }

        return retVal;
    }

    /**
     * This method returns inverse of a Boolean value
     *
     * @param bool
     * @return
     */
    public static Boolean not(Boolean bool) {
        return !bool;
    }

    /**
     * This method returns current System DateTime
     *
     * @return
     */
    public static Object dateNow() {

        return Calendar.getInstance().getTime();
    }

    /**
     * This method returns provided date in the provided format
     *
     * @param arg1
     * @param arg2
     * @return
     */
    public static String formatDate(Date date, String dateFormat,
                                    String defaultValue) {

        if (date == null) {
            if (defaultValue != null)
                return defaultValue;
            else
                return StringHelper.EMPTY;
        }

        String retVal = new SimpleDateFormat(dateFormat).format(date);
        return retVal;
    }

    public static String formatString(String format, Object value) {

        String retVal = String.format(format, value);
        return retVal;
    }

    /**
     * This method returns if provided object is null
     *
     * @param arg1
     * @return
     */
    public static Boolean isNull(Object arg1) {
        return (arg1 == null);
    }

    public static Boolean isEmpty(String arg1) {
        return StringHelper.isEmpty(arg1);
    }

    /**
     * This method returns if provided object is not null
     *
     * @param arg1
     * @return
     */
    public static Boolean notNull(Object arg1) {
        return (arg1 != null);
    }

    public static Object getObject(String systemName, String id) {

        MetaDataBroker metaDataBroker = (MetaDataBroker) DataRepository
                .getBroker(systemName);

        return metaDataBroker.findById(systemName, id);
    }

    public static Object getObjectValue(String systemName, String id,
                                        String fieldName) {

        DataObject dataObject = (DataObject) getObject(systemName, id);

        return dataObject.getValue(fieldName);
    }

    private static LocaleInfo getCurrentLocaleInfo() {

        return (LocaleInfo) ContextFunction
                .sessionValue(SessionKeyLookup.CurrentLocale);
    }

    public static String formatPercentage(Double value, int precision,
                                          String defaultValue) {

        if (value == null) {
            if (defaultValue != null)

                return defaultValue;
            else
                return StringHelper.EMPTY;
        }

        String retVal;
        if (precision == 0)
            retVal = String.format("%,.2f %%", value);
        else
            retVal = String.format("%,." + precision + "f %%", value);

        return retVal;
    }

    public static String currencyName(String acronym) {

        Guard.checkNullOrEmpty(acronym, "acronym");

        if ("AED".equalsIgnoreCase(acronym))
            return "Arab Emirates Dirham";
        if ("AUD".equalsIgnoreCase(acronym))
            return "Australian Dollar";
        if ("BHD".equalsIgnoreCase(acronym))
            return "Bahraini Dinar";
        if ("CAD".equalsIgnoreCase(acronym))
            return "Canadian Dollar";
        if ("DKK".equalsIgnoreCase(acronym))
            return "Danish Krone";
        if ("EUR".equalsIgnoreCase(acronym))
            return "Euro";
        if ("HKD".equalsIgnoreCase(acronym))
            return "Hong Kong Dollar";
        if ("INR".equalsIgnoreCase(acronym))
            return "Indian Rupee";
        if ("JPY".equalsIgnoreCase(acronym))
            return "Japanese Yen";
        if ("JOD".equalsIgnoreCase(acronym))
            return "Jordanian Dinar";
        if ("KWD".equalsIgnoreCase(acronym))
            return "Kuwaiti Dinar";
        if ("LBP".equalsIgnoreCase(acronym))
            return "Lebanese Pound";
        if ("NOK".equalsIgnoreCase(acronym))
            return "Norwegian Kroner";
        if ("OMR".equalsIgnoreCase(acronym))
            return "Omani Rial";
        if ("PKR".equalsIgnoreCase(acronym))
            return "Pakistani Rupee";
        if ("GBP".equalsIgnoreCase(acronym))
            return "Pound Sterling";
        if ("QAR".equalsIgnoreCase(acronym))
            return "Qatari Riyal";
        if ("SAR".equalsIgnoreCase(acronym))
            return "Saudi Riyal";
        if ("SGD".equalsIgnoreCase(acronym))
            return "Singapore Dollar";
        if ("LKR".equalsIgnoreCase(acronym))
            return "Sri Lanka Rupee";
        if ("SEK".equalsIgnoreCase(acronym))
            return "Swedish Krona";
        if ("CHF".equalsIgnoreCase(acronym))
            return "Swiss Francs";
        if ("USD".equalsIgnoreCase(acronym))
            return "United States Dollar";
        else
            return "Unknown";
    }

    public static String formatAmount(Double value, String isoCode,
                                      String defaultValue, Boolean isAppend) {
        return formatPreciseAmt(value, null, isoCode, defaultValue, isAppend);
    }

    public static String formatPreciseAmt(Double value, Integer iprecision,
                                          String isoCode, String defaultValue, Boolean isAppend) {

        if (value == null) {

            if (defaultValue != null)
                return defaultValue;
            else
                return StringHelper.EMPTY;
        }

        if (StringHelper.isEmpty(isoCode)) {
            isoCode = "USD";
        }

        if (isAppend == null)
            isAppend = true;

        isoCode = isoCode.trim().toUpperCase();

        int isoIntCode = -1;
        int precision = 2;

        if (Character.isDigit(isoCode.charAt(0))) {
            try {
                isoIntCode = Integer.parseInt(isoCode);
            } catch (Exception e) {
            }
        }

        if (iprecision == null) {
            if (isoIntCode > -1) {
                switch (isoIntCode) {
                    case 950:
                    case 953:
                    case 990:
                    case 324:
                    case 108:
                    case 974:
                    case 152:
                    case 262:
                    case 392:
                    case 174:
                    case 410:
                    case 600:
                    case 646:
                    case 548:
                    case 952:
                        precision = 0;
                        break;
                    case 48:
                    case 400:
                    case 414:
                    case 434:
                    case 512:
                    case 788:
                    case 368:
                        precision = 3;
                        break;
                    default:
                        precision = 2;
                        break;
                }
            } else if (isoCode.length() == 3) {
                if (isoCode.equals("XAF") || isoCode.equals("XOF")
                        || isoCode.equals("XPF") || isoCode.equals("CLF")
                        || isoCode.equals("GNF") || isoCode.equals("BIF")
                        || isoCode.equals("BYR") || isoCode.equals("CLP")
                        || isoCode.equals("DJF") || isoCode.equals("JPY")
                        || isoCode.equals("KMF") || isoCode.equals("KRW")
                        || isoCode.equals("PYG") || isoCode.equals("RWF")
                        || isoCode.equals("VUV"))
                    precision = 0;
                else if (isoCode.equals("BHD") || isoCode.equals("IQD")
                        || isoCode.equals("JOD") || isoCode.equals("KWD")
                        || isoCode.equals("LYD") || isoCode.equals("QMR")
                        || isoCode.equals("TND"))
                    precision = 3;
                else
                    precision = 2;
            } else
                throw new CoreException(
                        "Invalid Arguements: formatAmount(value:%s, isoCode:%s)",
                        value, isoCode);
        } else
            precision = iprecision;
        NumberFormat nf = new DecimalFormat("##,###,###");/*
         * ((LocaleInfo)
         * ContextFunction.sessionValue(SessionKeyLookup.CurrentLocale)).getNumberFormat());
         */
        nf.setMaximumFractionDigits(precision);
        nf.setMinimumFractionDigits(precision);
        nf.setMinimumIntegerDigits(1);

        String retVal = nf.format(value);

        if (isAppend)
            retVal = isoCode + "  " + retVal;

        return retVal;
    }

    public static String FormatValue(Object value, String format) {

        if (value == null)
            return StringHelper.EMPTY;

        if (StringHelper.isEmpty(format))
            return value.toString();

        if ((value instanceof Byte) || (value instanceof Short)
                || (value instanceof Integer) || (value instanceof Long))
            return String.format("%" + format + "d", value);

        if ((value instanceof Float) || (value instanceof Double))
            return String.format("%" + format + "f", value);

        if (value instanceof Date)
            return String.format("%" + format + "t", value);

        return String.format("%" + format + "s", value);

    }

    public static String productName(String productType, String productCode) {

        WebContext webCtx = ApplicationContext.getContext().get(
                WebContext.class);
        LocaleInfo locInfo = webCtx
                .getAttribute(SessionKeyLookup.CurrentLocale);
        return ProductCatalog.getInstance().getSelectedTreeValue(productType,
                productCode, locInfo.isPrimary());
    }

    public static String PicklistValue(String picklistId, String code) {
        List<PicklistData> data = PickListManager.getPickListData(picklistId);
        for (PicklistData pd : data) {
            if (pd.getValue().equalsIgnoreCase(code)) {
                return pd.getDisplayPrimary();
            }
        }
        return code;
    }

    public static void main(String args[]) {

        System.out
                .println(String.format(" this is arg one : %,.2f %%", 8.5300));
    }

    /**
     * Given variable argument String, this method will remove the directory
     * path.
     *
     * @param strings
     * @return
     */
    public static String removePath(String value) {

        if (value == null || value.length() == 0)
            return StringHelper.EMPTY;
        return value.substring(value.lastIndexOf("/") + 1, value.length());
    }

    public static String mask(String source, String maskChar, int start, int length) {
        return (String) StringHelper.maskString(source, maskChar, start, length);
    }

    public static String getUUID() {
        return UUID.randomUUID().toString();
    }
}

/**
 * This Method returns object corresponds to its respective Boolean value
 *
 * @param unlimited
 * Number of objects 1. Odd Number of Objects must be of type Boolean
 * 2. Even Number of Objects must be of Same Type 3. Last Object is
 * default return object
 * @return object
 */
/*
 * public static Object decode(Object... objects) {
 *
 * int argCount; Object defaultObject = null; Class<?> clazz = null;
 *
 * if ((objects.length - 1) % 2 != 0) throw new ExpressionException("Number of
 * arguments must be ODD");
 *
 * for (argCount = 0; argCount < objects.length - 1; argCount++) { if (argCount %
 * 2 == 0) { if (!(objects[argCount] instanceof Boolean)) { throw new
 * ExpressionException("Odd Arguments must be of type Boolean"); } } else { if
 * (clazz != null) { if
 * (!clazz.getName().equals(objects[argCount].getClass().getName())) { throw new
 * ExpressionException( "Even Arguments must be of same type"); } } else clazz =
 * objects[argCount].getClass(); } }
 *
 * if (objects.length > 0) { defaultObject = objects[objects.length - 1]; }
 *
 * argCount = 0; while (argCount < objects.length - 2) { if ((Boolean)
 * objects[argCount]) return objects[argCount + 1]; argCount = argCount + 2; }
 *
 * return defaultObject; }
 */

