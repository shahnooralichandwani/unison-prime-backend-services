package com.avanza.core.inbound;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class InboundSearchCriteriaConfig implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = -5902821357619083512L;

    private String id;
    private String dataType;
    private String title;

    private List<InboundSearchSelectOption> selectOptions = new ArrayList<InboundSearchSelectOption>();

    public InboundSearchCriteriaConfig(String idCriteria, String dataType, String title) {
        this.id = idCriteria;
        this.dataType = dataType;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public List<InboundSearchSelectOption> getSelectOptions() {
        return selectOptions;
    }

    public String getDataType() {
        return dataType;
    }

    public void addSelectOption(InboundSearchSelectOption inbOpt) {
        this.selectOptions.add(inbOpt);
    }


    public String getTitle() {
        return title;
    }
}
