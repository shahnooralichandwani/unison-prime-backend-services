package com.avanza.core.inbound;

import java.util.Map;


public class InboundCustomerConfig {

    private String customerEntityId;
    private String nonCustomerViewId;
    private String customerViewId;
    private String customerModuleId;


    Map<String, String> attributeMap;
    Map<String, String> misellenousMap;

    public String getCustomerEntityId() {
        return customerEntityId;
    }


    public void setCustomerEntityId(String customerEntityId) {
        this.customerEntityId = customerEntityId;
    }


    public String getNonCustomerViewId() {
        return nonCustomerViewId;
    }


    public void setNonCustomerViewId(String nonCustomerViewId) {
        this.nonCustomerViewId = nonCustomerViewId;
    }


    public String getCustomerViewId() {
        return customerViewId;
    }


    public void setCustomerViewId(String customerViewId) {
        this.customerViewId = customerViewId;
    }


    public String getCustomerModuleId() {
        return customerModuleId;
    }


    public void setCustomerModuleId(String customerModuleId) {
        this.customerModuleId = customerModuleId;
    }


    public Map<String, String> getAttributeMap() {
        return attributeMap;
    }


    public void setAttributeMap(Map<String, String> attributeMap) {
        this.attributeMap = attributeMap;
    }


    public Map<String, String> getMisellenousMap() {
        return misellenousMap;
    }


    public void setMisellenousMap(Map<String, String> misellenousMap) {
        this.misellenousMap = misellenousMap;
    }


}
