package com.avanza.core.inbound;

import java.util.Map;

/**
 * This class store information of an entity defined in Unison-conf.xml under
 * <Inbound-Config> tag
 *
 * @author Khawaja Muhammad Aamir
 */

public class EntityConfig {

    private String EntityId;

    private String nonCustomerViewId;

    private String ViewId;

    private String ModuleId;

    private String detailViewId;

    Map<String, String> attributeMap;

    Map<String, String> cellsMap;

    Map<String, String> miscellaneousMap;

    public Map<String, String> getAttributeMap() {
        return attributeMap;
    }

    public void setAttributeMap(Map<String, String> attributeMap) {
        this.attributeMap = attributeMap;
    }

    public String getEntityId() {
        return EntityId;
    }

    public void setEntityId(String entityId) {
        EntityId = entityId;
    }

    public Map<String, String> getMiscellaneousMap() {
        return miscellaneousMap;
    }

    public void setMiscellaneousMap(Map<String, String> miscellaneousMap) {
        this.miscellaneousMap = miscellaneousMap;
    }

    public String getModuleId() {
        return ModuleId;
    }

    public void setModuleId(String moduleId) {
        ModuleId = moduleId;
    }

    public String getNonCustomerViewId() {
        return nonCustomerViewId;
    }

    public void setNonCustomerViewId(String nonCustomerViewId) {
        this.nonCustomerViewId = nonCustomerViewId;
    }

    public String getViewId() {
        return ViewId;
    }

    public void setViewId(String viewId) {
        ViewId = viewId;
    }

    public Map<String, String> getCellsMap() {
        return cellsMap;
    }

    public void setCellsMap(Map<String, String> cellsMap) {
        this.cellsMap = cellsMap;
    }

    public String getDetailViewId() {
        return detailViewId;
    }

    public void setDetailViewId(String detailViewId) {
        this.detailViewId = detailViewId;
    }
}
