package com.avanza.core.inbound;

import java.util.Map;

/**
 * @author Khawaja Muhammad Aamir
 */
public class InboundAccountConfig {

    private String accountEntityId;
    private String nonCustomerViewId;
    private String customerViewId;
    private String customerModuleId;


    Map<String, String> attributeMap;
    Map<String, String> misellenousMap;

}
