package com.avanza.core.inbound;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.avanza.core.data.expression.OperatorType;
import com.avanza.core.util.DataType;


public class InboundSearchConfig implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1404427295999017281L;

    private String id;
    private String moduleId;
    private String mapEntity;
    private String mapField;
    private String brokerKey;
    private String type;
    private String image;
    private String format;
    private String prefix;
    private String displayTextPrm;
    private String displayTextSec;
    private String onclick;
    private DataType mapFieldType;
    private OperatorType operatorType;
    private String min;
    private String max;
    private String inputType;

    private List<InboundSearchCriteriaConfig> searchCriteria = new ArrayList<InboundSearchCriteriaConfig>();


    public List<InboundSearchCriteriaConfig> getSearchCriteria() {
        return searchCriteria;
    }

    public InboundSearchConfig(String id, String moduleId, String mapEntity, String mapField, String mapFieldType, String type, String image, String format, String displayTextPrm,
                               String displayTextSec, String onclick, String opType, String brokerKey, String min, String max, String inputType, String prefix) {
        this.id = id;
        this.moduleId = moduleId;
        this.mapEntity = mapEntity;
        this.mapField = mapField;
        this.mapFieldType = DataType.fromString(mapFieldType);
        this.type = type;
        this.image = image;
        this.format = format;
        this.displayTextPrm = displayTextPrm;
        this.displayTextSec = displayTextSec;
        this.onclick = onclick;
        this.operatorType = OperatorType.parseToken(opType);
        this.brokerKey = brokerKey;
        this.min = min;
        this.max = max;
        this.inputType = inputType;
        this.prefix = prefix;
    }

    public String getId() {
        return id;
    }

    public String getMapEntity() {
        return mapEntity;
    }

    public String getMapField() {
        return mapField;
    }

    public String getType() {
        return type;
    }

    public String getImage() {
        return image;
    }

    public String getFormat() {
        return format;
    }

    public String getDisplayTextPrm() {
        return displayTextPrm;
    }

    public String getDisplayTextSec() {
        return displayTextSec;
    }

    public String getOnClick() {
        return onclick;
    }

    public void addCriteria(InboundSearchCriteriaConfig inbCrt) {
        this.searchCriteria.add(inbCrt);
    }

    public String getModuleId() {
        return moduleId;
    }

    public DataType getMapFieldType() {
        return mapFieldType;
    }

    public OperatorType getOperatorType() {
        return operatorType;
    }

    public String getBrokerKey() {
        return brokerKey;
    }

    public String getMin() {
        return min;
    }

    public String getMax() {
        return max;
    }

    public String getInputType() {
        return inputType;
    }


    public String getPrefix() {
        return prefix;
    }
}
