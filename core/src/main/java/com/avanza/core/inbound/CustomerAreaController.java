package com.avanza.core.inbound;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public abstract class CustomerAreaController implements Serializable {


    protected InboundSearchConfig accountSearchConfig;
    protected String currentCustomerId;
    protected InboundSearchConfig rimSearchConfig;
    protected String searchAccountNumber = "";
    protected String searchAccountType = "";
    protected String searchCreditCard = "";
    protected String searchDebitCard = "";
    protected String searchOldAccount = "";

    protected Map<String, String> searchCriterias = new HashMap<String, String>();
    protected String searchCustomerString = "";

    public CustomerAreaController() {
    }

    public abstract void beginSession(String customerId);

    //TODO: UNISONPRIME
    /*public abstract void endSession(ActionEvent actionEvent);
    
    public abstract void clearCustomer(ActionEvent actionEvent);
    
    public abstract void markFavorite(ActionEvent actionEvent);
    
    public abstract void searchCustomerByAccount(ActionEvent actionEvent);
    
    public abstract void searchCustomerByCriteria(ActionEvent actionEvent);
    
    public abstract void searchCustomerString(ActionEvent actionEvent);*/

    public abstract void initiateSession();

    public abstract void initiateSession(String customerId);

    public abstract String getSessionState();

    public abstract String getCustomerRim();

    public void setCurrentCustomerId(String currentCustomerId) {
        this.currentCustomerId = currentCustomerId.trim();
    }

    public void setSearchAccountNumber(String searchAccountNumber) {
        this.searchAccountNumber = searchAccountNumber.trim();
    }

    public void setSearchAccountType(String searchAccountType) {
        this.searchAccountType = searchAccountType.trim();
    }

    public void setSearchCreditCard(String searchCreditCard) {
        this.searchCreditCard = searchCreditCard.trim();
    }

    public void setSearchCustomerString(String searchCustomerString) {
        this.searchCustomerString = searchCustomerString.trim();
    }

    public void setSearchDebitCard(String searchDebitCard) {
        this.searchDebitCard = searchDebitCard.trim();
    }

    public String getCurrentCustomerId() {
        return currentCustomerId;
    }

    public String getSearchAccountNumber() {
        return searchAccountNumber;
    }

    public String getSearchAccountType() {
        return searchAccountType;
    }

    public String getSearchCreditCard() {
        return searchCreditCard;
    }

    public String getSearchCustomerString() {
        return searchCustomerString;
    }

    public String getSearchDebitCard() {
        return searchDebitCard;
    }

    public String getSearchOldAccount() {
        return searchOldAccount;
    }

    public void setSearchOldAccount(String searchOldAccount) {
        this.searchOldAccount = searchOldAccount;
    }

    public Map<String, String> getSearchCriterias() {
        return searchCriterias;
    }
}
