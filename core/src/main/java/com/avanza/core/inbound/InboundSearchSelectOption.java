package com.avanza.core.inbound;

import java.io.Serializable;


public class InboundSearchSelectOption implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1825632845893291451L;

    private String id;
    private String labelPrm;
    private String labelSec;

    public InboundSearchSelectOption(String idOption, String labelPrm, String labelSec) {
        this.id = idOption;
        this.labelPrm = labelPrm;
        this.labelSec = labelSec;
    }

    public String getId() {
        return id;
    }

    public String getLabelPrm() {
        return labelPrm;
    }

    public String getLabelSec() {
        return labelSec;
    }
}
