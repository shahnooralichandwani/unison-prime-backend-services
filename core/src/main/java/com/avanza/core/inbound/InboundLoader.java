package com.avanza.core.inbound;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.avanza.core.CoreInterface;
import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.sdo.ActivityTypeCatalog;
import com.avanza.core.security.Permission;
import com.avanza.core.security.PermissionBinding;
import com.avanza.core.security.User;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.web.config.WebContext;


public class InboundLoader implements CoreInterface {

    private static final Logger logger = Logger.getLogger(InboundLoader.class);
    private static final String XML_CUSTOMER_SEARCH = "customer-search";
    private static final String XML_SEARCH = "search";


    private static InboundLoader _instance = new InboundLoader();
    private Map<String, InboundSearchConfig> searchSectionsMap = new LinkedHashMap<String, InboundSearchConfig>();
    private List<InboundSearchConfig> searchSections = new ArrayList<InboundSearchConfig>();
    private boolean isLoaded = false;
    private EntityConfig customerConfig;
    private EntityConfig accountConfig;
    private EntityConfig metaViewMenuConfig;
    private String softphonePermissionId;
    private HashMap<String, String> ackChannels = new HashMap<String, String>(0);
    private int minCorrect;
    private int retryCount;
    private int timeDelay;

    private InboundLoader() {
    }

    public static InboundLoader getInstance() {

        return _instance;
    }

    public void dispose() {
    }

    public void load(ConfigSection section) {

        if (isLoaded) return;

        logger.logInfo("[Loading the Inbound Search Configurations]");
        ConfigSection searchCfg = section.getChild(XML_CUSTOMER_SEARCH);

        for (ConfigSection cfgSearch : searchCfg.getChildSections(XML_SEARCH)) {
            String id = cfgSearch.getTextValue("id");
            String moduleId = cfgSearch.getValue("module-id", "");
            String mapEntity = cfgSearch.getValue("map-entity", "");
            String mapField = cfgSearch.getValue("map-field", "");
            String mapFieldType = cfgSearch.getValue("map-field-type", "String");
            String type = cfgSearch.getTextValue("type");
            String image = cfgSearch.getValue("image", "");
            String format = cfgSearch.getValue("format", "");
            String displayTextPrm = cfgSearch.getValue("display-text-prm", "");
            String displayTextSec = cfgSearch.getValue("display-text-sec", "");
            String onclick = cfgSearch.getValue("onclick", "");
            String opType = cfgSearch.getValue("operator-type", "=");
            String brokerKey = cfgSearch.getValue("broker-key", "").trim();
            String min = cfgSearch.getValue("min", "").trim();
            String max = cfgSearch.getValue("max", "").trim();
            String inputType = cfgSearch.getValue("input-type", "alpha-numeric");
            String prefix = cfgSearch.getValue("prefix", "");

            InboundSearchConfig searchConfig = new InboundSearchConfig(id, moduleId, mapEntity, mapField, mapFieldType, type, image, format, displayTextPrm, displayTextSec, onclick, opType, brokerKey, min, max, inputType, prefix);

            for (ConfigSection cfgCriteria : cfgSearch.getChildSections("search-criteria")) {
                String idCriteria = cfgCriteria.getTextValue("id");
                String dataType = cfgCriteria.getTextValue("data-type");
                String title = cfgCriteria.getTextValue("title");

                InboundSearchCriteriaConfig inbCrt = new InboundSearchCriteriaConfig(idCriteria, dataType, title);
                searchConfig.addCriteria(inbCrt);

                for (ConfigSection cfgOption : cfgCriteria.getChildSections("option")) {
                    String idOption = cfgOption.getTextValue("id");
                    String labelPrm = cfgOption.getTextValue("label-prm");
                    String labelSec = cfgOption.getTextValue("label-sec");
                    InboundSearchSelectOption inbOpt = new InboundSearchSelectOption(idOption, labelPrm, labelSec);
                    inbCrt.addSelectOption(inbOpt);
                }
            }
            searchSectionsMap.put(id, searchConfig);
        }

        searchSections = new ArrayList<InboundSearchConfig>(searchSectionsMap.values());

        //	Loading customer Entity
        ConfigSection customerCfg = section.getChild("customer");
        this.customerConfig = new EntityConfig();
        customerConfig.setEntityId(customerCfg.getTextValue("entity-id"));
        customerConfig.setNonCustomerViewId(customerCfg.getTextValue("non-cust-view-id"));
        customerConfig.setViewId(customerCfg.getTextValue("view-id"));
        customerConfig.setModuleId(customerCfg.getTextValue("module-id"));
        customerConfig.setDetailViewId(customerCfg.getTextValue("detail-view-id"));

        Map<String, String> mapAttrib = new HashMap<String, String>();

        for (ConfigSection cfgChild : customerCfg.getChildSections("attribute")) {
            mapAttrib.put(cfgChild.getTextValue("id"), cfgChild.getTextValue("column"));
        }
        customerConfig.setAttributeMap(mapAttrib);

        //Loading Permission Ids

        Map<String, String> misellenousAttr = new HashMap<String, String>();

        ConfigSection permissionCfg = section.getChild("identity-permission");
        misellenousAttr.put(permissionCfg.getTextValue("id"), permissionCfg.getTextValue("value"));

        ConfigSection activityTreeRootCfg = section.getChild("activityType-tree-root");
        misellenousAttr.put(activityTreeRootCfg.getTextValue("id"), activityTreeRootCfg.getTextValue("value"));

        customerConfig.setMiscellaneousMap(misellenousAttr);

        ActivityTypeCatalog.getInstance();
        ConfigSection softphonePerm = section.getChild("softphone-permission");
        if (softphonePerm == null)
            softphonePermissionId = null;
        else
            softphonePermissionId = softphonePerm.getTextValue("id");
        ConfigSection customerValidation = section.getChild("customer-validation");
        if (customerValidation == null)
            minCorrect = 0;
        else
            minCorrect = customerValidation.getIntValue("minCorrect");

        ConfigSection softphoneivr = section.getChild("softphone-ivr");
        if (softphoneivr == null) {
            retryCount = 0;
            timeDelay = 0;
        } else {
            retryCount = softphoneivr.getIntValue("retryCount");
            timeDelay = softphoneivr.getIntValue("timeDelay");
        }
        ConfigSection ackChannel = section.getChild("AckChannels");
        List<ConfigSection> channelConf = ackChannel.getChildSections("channel");
        for (ConfigSection sec : channelConf) {
            String id = sec.getTextValue("id");
            String column = sec.getTextValue("column");
            ackChannels.put(id, column);
        }
        isLoaded = true;
        // load Accounts Entity
        this.loadInboundAccounts(section);
        this.loadMetaViewMenu(section);
    }

    /** Added by KMA */
    /**
     * Loading Accounts Entity
     */
    public void loadInboundAccounts(ConfigSection section) {
        ConfigSection accountsCfg = section.getChild("Account");
        this.accountConfig = new EntityConfig();
        accountConfig.setEntityId(accountsCfg.getTextValue("entity-id"));

        Map<String, String> mapAttrib = new HashMap<String, String>();
        for (ConfigSection cfgChild : accountsCfg.getChildSections("attribute")) {
            mapAttrib.put(cfgChild.getTextValue("id"), cfgChild.getTextValue("column"));
        }
        accountConfig.setAttributeMap(mapAttrib);
    }

    /** Added by KMA */
    /**
     * Loading Meta View Menu
     */
    public void loadMetaViewMenu(ConfigSection section) {
        ConfigSection metaViewMenusCfg = section.getChild("MetaViewMenu");
        this.metaViewMenuConfig = new EntityConfig();
        Map<String, String> mapCells = new HashMap<String, String>();
        // get Cells
        for (ConfigSection cfgChild : metaViewMenusCfg.getChildSections("cell")) {
            mapCells.put(cfgChild.getTextValue("id"), cfgChild.getTextValue("value"));
        }
        metaViewMenuConfig.setCellsMap(mapCells);
    }

    public List<InboundSearchConfig> getSearchSections() {
        return searchSections;
    }

    public InboundSearchConfig getSearchSectionById(String id) {
        return searchSectionsMap.get(id);
    }

    public String getCustomerEntityId() {
        return this.customerConfig.getEntityId();
    }

    public String getNonCustomerViewId() {
        return this.customerConfig.getNonCustomerViewId();
    }

    public String getCustomerViewId() {
        return this.customerConfig.getViewId();
    }

    public String getCustomerDetailViewId() {
        return this.customerConfig.getDetailViewId();
    }

    public String getCustomerModuleId() {
        return this.customerConfig.getModuleId();
    }

    public String getCustomerColumnName(String key) {
        String name = this.customerConfig.attributeMap.get(key);
        if (name == null) name = key;
        return name;
    }

    public Map getCustomerColumnNames() {

        return this.customerConfig.attributeMap;
    }

    public String getIdentityPermissionSystemKey(String key) {
        return this.customerConfig.miscellaneousMap.get(key);
    }

    public String getActivityTypeTreeRoot(String key) {
        return this.customerConfig.miscellaneousMap.get(key);
    }

    public Boolean hasSoftphonePermission() {
        if (StringHelper.isEmpty(this.softphonePermissionId))
            return false;
        WebContext ctx = ApplicationContext.getContext().get(WebContext.class);
        User user = ctx.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);
        PermissionBinding pb = user.getPermission(this.softphonePermissionId);
        if (pb == null)
            return false;
        Permission p = pb.getLinkPermission();
        if (p == null)
            return false;
        return true;
    }

    public int getMinCorrect() {
        return minCorrect;
    }

    public void setMinCorrect(int minCorrect) {
        this.minCorrect = minCorrect;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public int getTimeDelay() {
        return timeDelay;
    }

    public void setTimeDelay(int timeDelay) {
        this.timeDelay = timeDelay;
    }

    /** Added By KMA */
    /**
     * Return the accounts attribute in MAP
     */
    public Map getAccountColumnNames() {
        return this.accountConfig.attributeMap;
    }

    /** Added By KMA */
    /**
     * Return the meta View attributes in MAP
     */
    public Map getMetaViewCellNames() {
        if (this.metaViewMenuConfig != null)
            return this.metaViewMenuConfig.cellsMap;
        else
            return new HashMap();
    }

    public String getAckChannel(String id) {
        return ackChannels.get(id);
    }
}
