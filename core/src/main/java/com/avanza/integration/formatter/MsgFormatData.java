package com.avanza.integration.formatter;


import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.integration.meta.messaging.MetaMessage;


public class MsgFormatData extends DbObject implements java.io.Serializable {


    // Fields    

    private MsgFormatDataId id;
    private MetaMessage refMetaMsg;
    private String formatTypeId;
    private FormatType formatType;
    private String channelFormatParam;
    private HashMap<String, String> formatParamSet = new HashMap<String, String>();
    private Set<MsgFieldFormatData> msgFieldFormatData = new LinkedHashSet<MsgFieldFormatData>();


    // Constructors


    /**
     * default constructor
     */
    public MsgFormatData() {
    }

    /**
     * minimal constructor
     */
    public MsgFormatData(MsgFormatDataId id, String formatTypeId) {
        this.id = id;
        this.formatTypeId = formatTypeId;

    }

    /**
     * full constructor
     */
    public MsgFormatData(MsgFormatDataId id, String formatTypeId, Set<MsgFieldFormatData> msgFieldFormatData) {
        this.id = id;
        this.formatTypeId = formatTypeId;
        this.channelFormatParam = channelFormatParam;
        this.msgFieldFormatData = msgFieldFormatData;
    }


    // Property accessors


    public FormatType getFormatType() {
        return formatType;
    }

    public void setFormatType(FormatType formatType) {
        this.formatType = formatType;
    }

    public MetaMessage getRefMetaMsg() {
        return refMetaMsg;
    }

    public void setRefMetaMsg(MetaMessage refMetaMsg) {
        this.refMetaMsg = refMetaMsg;
    }

    public MsgFormatDataId getId() {
        return this.id;
    }

    public void setId(MsgFormatDataId id) {
        this.id = id;
    }

    public String getFormatTypeId() {
        return this.formatTypeId;
    }

    public void setFormatTypeId(String formatTypeId) {
        this.formatTypeId = formatTypeId;
    }

    public String getChannelFormatParam() {
        return this.channelFormatParam;
    }

    public void setChannelFormatParam(String channelFormatParam) {
        this.channelFormatParam = channelFormatParam;
    }

    public Set<MsgFieldFormatData> getMsgFieldFormatData() {
        return this.msgFieldFormatData;
    }

    public void setMsgFieldFormatData(Set<MsgFieldFormatData> msgFieldFormatData) {
        this.msgFieldFormatData = msgFieldFormatData;
    }


}
