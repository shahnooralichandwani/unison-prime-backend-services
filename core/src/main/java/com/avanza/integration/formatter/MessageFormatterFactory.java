package com.avanza.integration.formatter;

import java.util.HashMap;
import java.util.Map;

import com.avanza.core.util.TypeHelper;
import com.avanza.integration.IntegrationException;

public class MessageFormatterFactory {

    private static MessageFormatterFactory _instance = null;
    private static Map<MsgFormatDataId, MessageFormatter> formatterSet = new HashMap<MsgFormatDataId, MessageFormatter>();


    private MessageFormatterFactory() {

    }

    private static void createMessageFormatter(MsgFormatData metaMsg) {

        String className = metaMsg.getFormatType().getImpClass();
        try {
            Class<?> clazz = Class.forName(className);
            if (!formatterSet.containsKey(metaMsg.getId()))
                formatterSet.put(metaMsg.getId(), (MessageFormatter) MessageFormatterFactory
                        .getFormatter(className, clazz, metaMsg));
        } catch (Exception e) {
            throw new IntegrationException("%s Class not found", className);
        }
    }

    public static void dispose() {
    }

    private static <T> T getFormatter(String className, Class<T> clazz,
                                      MsgFormatData msgMeta) {

        T retVal = TypeHelper.createInstance(className, clazz, true);

        ((MessageFormatter) retVal).load(null, msgMeta);

        return retVal;
    }

    public static MessageFormatter getMessageFormatter(String channelId, String metaMsgId) {

        MsgFormatDataId msgFormatDataId = new MsgFormatDataId(channelId, metaMsgId);

        if (!formatterSet.containsKey(msgFormatDataId)) {
            MsgFormatData msgFormatData = MsgFormatDataCatalog.getMsgFormatData(channelId, metaMsgId);
            createMessageFormatter(msgFormatData);
        }

        return formatterSet.get(msgFormatDataId);
    }

    public static MessageFormatterFactory createObject() {
        if (_instance == null)
            _instance = new MessageFormatterFactory();
        return _instance;
    }
}