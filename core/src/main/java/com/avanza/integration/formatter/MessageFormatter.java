package com.avanza.integration.formatter;

import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;

import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.integration.Message;

public abstract class MessageFormatter {

    public abstract void load(ConfigSection section, MsgFormatData MsgMeta);

    public Message readMessage(String str) {

        return this.readMessage(new StringReader(str));
    }

    public abstract Message readMessage(Reader reader);

    public abstract void writeMessage(Message msg, Writer writer);

    public abstract MsgFormatData getMsgFormatData();
}