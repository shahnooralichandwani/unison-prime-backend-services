package com.avanza.integration.formatter;


public class MsgFormatDataId implements java.io.Serializable {


    // Fields    

    private String channelId;
    private String metaMsgId;


    // Constructors

    /**
     * default constructor
     */
    public MsgFormatDataId() {
    }


    /**
     * full constructor
     */
    public MsgFormatDataId(String channelId, String metaMsgId) {
        this.channelId = channelId;
        this.metaMsgId = metaMsgId;
    }


    // Property accessors

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMetaMsgId() {
        return this.metaMsgId;
    }

    public void setMetaMsgId(String metaMsgId) {
        this.metaMsgId = metaMsgId;
    }


    public boolean equals(Object other) {
        if ((this == other)) return true;
        if ((other == null)) return false;
        if (!(other instanceof MsgFormatDataId)) return false;
        MsgFormatDataId castOther = (MsgFormatDataId) other;

        return ((this.getChannelId() == castOther.getChannelId()) || (this.getChannelId() != null && castOther.getChannelId() != null && this.getChannelId().equals(castOther.getChannelId())))
                && ((this.getMetaMsgId() == castOther.getMetaMsgId()) || (this.getMetaMsgId() != null && castOther.getMetaMsgId() != null && this.getMetaMsgId().equals(castOther.getMetaMsgId())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (getChannelId() == null ? 0 : this.getChannelId().hashCode());
        result = 37 * result + (getMetaMsgId() == null ? 0 : this.getMetaMsgId().hashCode());
        return result;
    }


}
