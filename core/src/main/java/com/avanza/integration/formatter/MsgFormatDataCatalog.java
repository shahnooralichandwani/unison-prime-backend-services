package com.avanza.integration.formatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.meta.MetaException;

public class MsgFormatDataCatalog {

    // variables

    private static List<MsgFormatData> msgFormatDataSet = new ArrayList<MsgFormatData>();

    private static HashMap<MsgFieldFormatDataId, MsgFieldFormatData> msgFieldFormatDataSet = new HashMap<MsgFieldFormatDataId, MsgFieldFormatData>();

    // methods

    public static void Load() {
        try {
            DataBroker broker = DataRepository
                    .getBroker(MsgFormatDataCatalog.class.getName());
            List<MsgFormatData> msgFormatList = broker
                    .findAll(MsgFormatData.class);
            for (MsgFormatData msgFormatData : msgFormatList) {
                String formatTypeId = msgFormatData.getId().getChannelId();
                addMsgFormatData(msgFormatData);
            }
            List<MsgFieldFormatData> msgFieldFormatList = broker
                    .findAll(MsgFieldFormatData.class);
            for (MsgFieldFormatData msgFieldFormatData : msgFieldFormatList) {
                MsgFieldFormatDataId formatTypeId = msgFieldFormatData.getId();
                addMsgFieldFormatData(formatTypeId, msgFieldFormatData);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new MetaException(e,
                    "Error While Loading Meta Message Catalog");
        }
    }

    static void addMsgFormatData(MsgFormatData msgFormatData) {
        msgFormatDataSet.add(msgFormatData);
    }

    static void addMsgFieldFormatData(MsgFieldFormatDataId msgFieldFormatDataId, MsgFieldFormatData msgFieldFormatData) {
        msgFieldFormatDataSet.put(msgFieldFormatDataId, msgFieldFormatData);
    }

    public static List<MsgFormatData> getMsgFormatDataByType(String formatId) {
        List<MsgFormatData> retVal = new ArrayList<MsgFormatData>();
        for (MsgFormatData msgFormatData : msgFormatDataSet) {
            if (msgFormatData.getFormatTypeId().equals(formatId))
                retVal.add(msgFormatData);
        }
        return retVal;
    }

    public static List<MsgFormatData> getMsgFormatData(String channelId) {
        List<MsgFormatData> retVal = new ArrayList<MsgFormatData>();
        for (MsgFormatData msgFormatData : msgFormatDataSet) {
            if (msgFormatData.getId().getChannelId().equals(channelId))
                retVal.add(msgFormatData);
        }
        return retVal;
    }

    public static MsgFormatData getMsgFormatData(String channelId,
                                                 String metaMsgId) {

        for (MsgFormatData msgFormatData : msgFormatDataSet) {
            if (msgFormatData.getId().getChannelId().equals(channelId)
                    && msgFormatData.getId().getMetaMsgId().equals(metaMsgId))
                return msgFormatData;
        }
        return null;

    }

    public static MsgFieldFormatData getMsgFieldFormatData(String fieldId,
                                                           String metaMsgId, String ChannelId) {

        MsgFieldFormatDataId msgFieldFormatDataId = new MsgFieldFormatDataId(ChannelId, metaMsgId, fieldId);
        return msgFieldFormatDataSet.get(msgFieldFormatDataId);
    }
}
