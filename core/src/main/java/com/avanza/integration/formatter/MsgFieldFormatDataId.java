package com.avanza.integration.formatter;


public class MsgFieldFormatDataId implements java.io.Serializable {


    // Fields    

    private String channelId;
    private String metaMsgId;
    private String fieldId;


    // Constructors

    /**
     * default constructor
     */
    public MsgFieldFormatDataId() {
    }


    /**
     * full constructor
     */
    public MsgFieldFormatDataId(String channelId, String metaMsgId, String fieldId) {
        this.channelId = channelId;
        this.metaMsgId = metaMsgId;
        this.fieldId = fieldId;
    }


    // Property accessors

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMetaMsgId() {
        return this.metaMsgId;
    }

    public void setMetaMsgId(String metaMsgId) {
        this.metaMsgId = metaMsgId;
    }

    public String getFieldId() {
        return this.fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }


    public boolean equals(Object other) {
        if ((this == other)) return true;
        if ((other == null)) return false;
        if (!(other instanceof MsgFieldFormatDataId)) return false;
        MsgFieldFormatDataId castOther = (MsgFieldFormatDataId) other;

        return ((this.getChannelId() == castOther.getChannelId()) || (this.getChannelId() != null && castOther.getChannelId() != null && this.getChannelId().equals(castOther.getChannelId())))
                && ((this.getMetaMsgId() == castOther.getMetaMsgId()) || (this.getMetaMsgId() != null && castOther.getMetaMsgId() != null && this.getMetaMsgId().equals(castOther.getMetaMsgId())))
                && ((this.getFieldId() == castOther.getFieldId()) || (this.getFieldId() != null && castOther.getFieldId() != null && this.getFieldId().equals(castOther.getFieldId())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (getChannelId() == null ? 0 : this.getChannelId().hashCode());
        result = 37 * result + (getMetaMsgId() == null ? 0 : this.getMetaMsgId().hashCode());
        result = 37 * result + (getFieldId() == null ? 0 : this.getFieldId().hashCode());
        return result;
    }


}
