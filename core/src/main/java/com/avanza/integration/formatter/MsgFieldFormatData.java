package com.avanza.integration.formatter;

import com.avanza.core.data.DbObject;
import com.avanza.integration.meta.messaging.FieldType;
import com.avanza.integration.meta.messaging.MetaMessageField;

public class MsgFieldFormatData extends DbObject implements
        java.io.Serializable {

    // Fields

    /**
     *
     */
    private static final long serialVersionUID = -4469576617915989423L;

    private MsgFieldFormatDataId id;

    private MsgFormatData msgFormatData;

    private MetaMessageField metaMessageField;

    private Long sequenceNum;

    private Long startPosition;

    private Long length;

    private String columnName;

    private String xmlTag;

    private FieldType fieldType;

    private String fieldMetaMsgId;

    private String formatExp;

    // Constructors

    public String getFormatExp() {
        return formatExp;
    }

    public void setFormatExp(String formatExp) {
        this.formatExp = formatExp;
    }

    public String getFieldMetaMsgId() {
        return fieldMetaMsgId;
    }

    public void setFieldMetaMsgId(String fieldMetaMsgId) {
        this.fieldMetaMsgId = fieldMetaMsgId;
    }

    public MetaMessageField getMetaMessageField() {
        return metaMessageField;
    }

    public void setMetaMessageField(MetaMessageField metaMessageField) {
        this.metaMessageField = metaMessageField;
    }

    /**
     * default constructor
     */
    public MsgFieldFormatData() {
    }

    /**
     * minimal constructor
     */
    public MsgFieldFormatData(MsgFieldFormatDataId id,
                              MsgFormatData msgFormatData) {
        this.id = id;
        this.msgFormatData = msgFormatData;

    }

    /**
     * full constructor
     */
    public MsgFieldFormatData(MsgFieldFormatDataId id,
                              MsgFormatData msgFormatData, Long sequenceNum, Long startPosition,
                              Long length, String columnName, String xmlTag) {
        this.id = id;
        this.msgFormatData = msgFormatData;
        this.sequenceNum = sequenceNum;
        this.startPosition = startPosition;
        this.length = length;
        this.columnName = columnName;
        this.xmlTag = xmlTag;

    }

    // Property accessors

    public MsgFieldFormatDataId getId() {
        return this.id;
    }

    public void setId(MsgFieldFormatDataId id) {
        this.id = id;
    }

    public MsgFormatData getMsgFormatData() {
        return this.msgFormatData;
    }

    public void setMsgFormatData(MsgFormatData msgFormatData) {
        this.msgFormatData = msgFormatData;
    }

    public Long getSequenceNum() {
        return this.sequenceNum;
    }

    public void setSequenceNum(Long sequenceNum) {
        this.sequenceNum = sequenceNum;
    }

    public Long getStartPosition() {
        return this.startPosition;
    }

    public void setStartPosition(Long startPosition) {
        this.startPosition = startPosition;
    }

    public Long getLength() {
        return this.length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public String getColumnName() {
        return this.columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getXmlTag() {
        return this.xmlTag;
    }

    public void setXmlTag(String xmlTag) {
        this.xmlTag = xmlTag;
    }

    public String getFieldType() {
        return this.fieldType.toString();
    }

    public void setFieldType(String value) {
        this.fieldType = fieldType.fromString(value);
    }

}
