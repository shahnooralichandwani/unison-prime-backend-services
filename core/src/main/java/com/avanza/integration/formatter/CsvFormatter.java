package com.avanza.integration.formatter;

import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.reader.CsvReader;
import com.avanza.core.util.writer.CsvWriter;
import com.avanza.integration.Message;
import com.avanza.integration.MessageCollection;
import com.avanza.integration.meta.messaging.FieldType;
import com.avanza.integration.meta.messaging.MetaMessage;
import com.avanza.integration.meta.messaging.MetaMessageCatalog;
import com.avanza.integration.meta.messaging.MetaMessageField;

public class CsvFormatter extends MessageFormatter {

    private static final String FIELD_DELIMITER = "FieldDelimiter";

    private MsgFormatData msgFormatData;

    public void load(ConfigSection section, MsgFormatData MsgMeta) {

        this.msgFormatData = MsgMeta;

    }

    public MsgFormatData getMsgFormatData() {

        return msgFormatData;
    }

    public Message readMessage(Reader reader) {

        Map<String, String> fmtParamsMap = StringHelper
                .getPropertiesMapFrom(this.msgFormatData
                        .getChannelFormatParam());
        CsvReader csvReader = new CsvReader(fmtParamsMap.get(
                CsvFormatter.FIELD_DELIMITER).charAt(0), true);
        csvReader.load(reader);

        return readMessage(csvReader);
    }

    private Message readMessage(CsvReader csvReader) {

        MetaMessage metaMessage = this.msgFormatData.getRefMetaMsg();
        Set<MsgFieldFormatData> metaMessageFieldSet = this.msgFormatData
                .getMsgFieldFormatData();
        String lastReadField = null;
        Message message = new Message(metaMessage);

        Iterator iterator = metaMessageFieldSet.iterator();
        while (iterator.hasNext()) {
            MsgFieldFormatData msgFieldFormatData = (MsgFieldFormatData) iterator
                    .next();
            String formatExpression = msgFieldFormatData.getFormatExp();
            if (msgFieldFormatData.getFieldType() == FieldType.Single
                    .toString()) {
                String currentFieldValue = csvReader.readField();
                ApplicationContext.getContext().add(
                        msgFieldFormatData.getId().getFieldId(),
                        currentFieldValue);

                // Apply Format Expression
                if (!StringHelper.isEmpty(formatExpression)) {
                    currentFieldValue = (String) ExpressionHelper
                            .getExpressionResult(formatExpression,
                                    msgFieldFormatData.getClass());
                }

                message.setValue(msgFieldFormatData.getId().getFieldId(),
                        currentFieldValue);
            } else if (msgFieldFormatData.getFieldType() == FieldType.List
                    .toString()) {

                message.getChildMessageList().add(
                        msgFieldFormatData.getFieldMetaMsgId(),
                        new MessageCollection(MetaMessageCatalog
                                .getMetaMessage(msgFieldFormatData
                                        .getFieldMetaMsgId())));
                MessageFormatter formatter = MessageFormatterFactory
                        .getMessageFormatter(metaMessage.getTransaction()
                                .getChannelId(), msgFieldFormatData
                                .getFieldMetaMsgId());

                Map<String, String> childFmtParamsMap = StringHelper
                        .getPropertiesMapFrom(formatter.getMsgFormatData()
                                .getChannelFormatParam());

                CsvReader childReader = new CsvReader(childFmtParamsMap.get(
                        CsvFormatter.FIELD_DELIMITER).charAt(0), true);
                childReader.load(csvReader.getReader());

                MetaMessageField metaMsgField = MetaMessageCatalog
                        .getMetaMessageField(msgFieldFormatData.getId()
                                .getMetaMsgId(), msgFieldFormatData.getId()
                                .getFieldId());
                String calcExpression = metaMsgField.getCalcExp();
                String fieldName = null;

                int repeatCount = csvReader.getInteger(csvReader.getFieldCount() - 1);

                for (int index = 0; index < repeatCount; index++) {

                    message
                            .addChild(msgFieldFormatData.getFieldMetaMsgId(),
                                    ((CsvFormatter) formatter)
                                            .readMessage(childReader));
                }

            }
        }

        return message;
    }

    public void writeMessage(Message msg, Writer writer) {
        Map<String, String> fmtParamsMap = StringHelper
                .getPropertiesMapFrom(this.msgFormatData
                        .getChannelFormatParam());
        CsvWriter csvWriter = new CsvWriter(fmtParamsMap.get(
                CsvFormatter.FIELD_DELIMITER).charAt(0), true);

        csvWriter.open((StringWriter) writer);
        csvWriter.writeStartRecord();

        if (msg.getChildCount() > 0) {
            MessageCollection messageCollection = msg
                    .getChildList(this.msgFormatData.getRefMetaMsg()
                            .getSystemName());
            ParseMessage(writer, messageCollection);
        }

        ParseFields(msg, csvWriter);
        csvWriter.writeEndRecord();

    }

    public void ParseMessage(Writer writer, MessageCollection messageCollection) {

        Iterator iter = messageCollection.iterator();
        while (iter.hasNext()) {
            Message message = (Message) iter.next();
            String channelId = message.getMetaMessage().getTransaction()
                    .getChannelTransactions().iterator().next()
                    .getInterfaceChannel().getChannelId();
            String metaMsgId = message.getMetaMessage().getMetaMsgId();

            MessageFormatter formatter = MessageFormatterFactory
                    .getMessageFormatter(channelId, metaMsgId);
            formatter.writeMessage(message, writer);
        }

    }

    public void ParseFields(Message msg, CsvWriter writer) {
        Map<String, Object> fieldList = msg.getValues();
        Iterator iter = msg.getValues().keySet().iterator();

        while (iter.hasNext()) {
            // MessageField messageField = (MessageField)
            // fieldList.get(iter.next());
            String fieldKey = (String) iter.next();
            MsgFieldFormatData msgFieldFormatData = MsgFormatDataCatalog
                    .getMsgFieldFormatData(fieldKey, msg.getRefMetaMsg()
                            .getMetaMsgId(), msg.getRefMetaMsg()
                            .getTransaction().getChannelId());
            String formatExpression = msgFieldFormatData.getFormatExp();

            Object currentFieldValue = fieldList.get(fieldKey);
            if (currentFieldValue != null)
                ApplicationContext.getContext().add(fieldKey, currentFieldValue);
            if (formatExpression != null && !StringHelper.isEmpty(formatExpression)) {
                currentFieldValue = (String) ExpressionHelper
                        .getExpressionResult(formatExpression,
                                msgFieldFormatData.getClass());
            }
            if (currentFieldValue != null)
                writer.writeField(currentFieldValue);
        }
    }

}

/*
 * } else if ((msgFieldFormatData.getFieldType() == FieldType.List.toString())) {
 * String channelId =
 * msgFieldFormatData.getMsgFormatData().getId().getChannelId(); String
 * metaMsgId = msgFieldFormatData.getMsgFormatData().getId().getMetaMsgId();
 *
 * MessageFormatter formatter =
 * MessageFormatterFactory.getMessageFormatter(channelId, metaMsgId);
 *
 * Map<String, String> childFmtParamsMap =
 * StringHelper.getPropertiesMapFrom(formatter.getMsgFormatData().getChannelFormatParam());
 * CsvReader childReader = new
 * CsvReader(childFmtParamsMap.get(CsvFormatter.FIELD_DELIMITER).charAt(0),
 * true); childReader.load(csvReader.getReader());
 *
 * int repeatCount = Integer.parseInt(childReader.readField());
 *
 * for (int index = 0; index<repeatCount; index++) {
 *
 * message.addChild(this.msgFormatData.getRefMetaMsg().getSystemName(),
 * ((CsvFormatter) formatter).readMessage(childReader)); } }
 */
