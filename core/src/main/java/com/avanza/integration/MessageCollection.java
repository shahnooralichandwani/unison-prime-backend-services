package com.avanza.integration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.avanza.core.util.Guard;
import com.avanza.integration.meta.messaging.MetaMessage;

public class MessageCollection implements List<Message>, Serializable {

    private static final long serialVersionUID = -8498566954251938759L;

    private ArrayList<Message> messageList = new ArrayList<Message>();
    private MetaMessage refMetaMsg;


    public MetaMessage getMetaMessage() {
        return this.refMetaMsg;
    }

    public MessageCollection(MetaMessage refMetaMsg, List<Message> objects) {

        Guard.checkNull(refMetaMsg, "MessageCollection(MetaMessage)");
        this.refMetaMsg = refMetaMsg;
        this.addAll(objects);
    }

    public MessageCollection(MetaMessage refMetaMsg) {

        Guard.checkNull(refMetaMsg, "MessageCollection(MetaMessage)");
        this.refMetaMsg = refMetaMsg;
    }

    @SuppressWarnings("unchecked")
    public List<Message> getMessageList() {
        return (List<Message>) messageList.clone();
    }

    protected void setMessageList(List<Message> objectList) {

        this.messageList.clear();
        this.messageList.addAll(objectList);
    }

    public boolean add(Message item) {

        this.checkEntity(item);
        return this.messageList.add(item);
    }

    public void add(int index, Message item) {

        this.checkEntity(item);
        this.messageList.add(index, item);
    }

    public boolean addAll(Collection<? extends Message> itemList) {

        if (itemList == null) return true;

        for (Message item : itemList) {
            this.checkEntity(item);
        }

        return this.messageList.addAll(itemList);
    }

    public boolean addAll(int index, Collection<? extends Message> itemList) {

        if (itemList == null) return true;

        for (Message item : itemList) {
            this.checkEntity(item);
        }

        return this.messageList.addAll(index, itemList);
    }

    public void clear() {
        this.messageList.clear();
    }

    public boolean contains(Object o) {
        return this.messageList.contains(o);
    }

    public boolean containsAll(Collection<?> c) {
        return this.messageList.containsAll(c);
    }

    public Message get(int index) {
        return this.messageList.get(index);
    }

    public Message get(String uniqueId) {

        Message retVal = null;

        for (Message item : this.messageList) {

            if (item.getUniqueId().compareTo(uniqueId) == 0) {

                retVal = item;
                break;
            }
        }

        return retVal;
    }

    public int indexOf(Object o) {
        return this.messageList.indexOf(o);
    }

    public boolean isEmpty() {
        return this.messageList.isEmpty();
    }

    public Iterator<Message> iterator() {
        return this.messageList.iterator();
    }

    public int lastIndexOf(Object o) {
        return this.messageList.lastIndexOf(o);
    }

    public ListIterator<Message> listIterator() {
        return this.messageList.listIterator();
    }

    public ListIterator<Message> listIterator(int index) {
        return this.messageList.listIterator(index);
    }

    public boolean remove(Object o) {
        return this.messageList.remove(o);
    }

    public Message remove(int index) {
        return this.messageList.remove(index);
    }

    public boolean removeAll(Collection<?> c) {
        return this.messageList.removeAll(c);
    }

    public boolean retainAll(Collection<?> c) {
        return this.messageList.retainAll(c);
    }

    public Message set(int index, Message element) {

        this.checkEntity(element);
        return this.messageList.set(index, element);
    }

    public int size() {
        return this.messageList.size();
    }

    public List<Message> subList(int fromIndex, int toIndex) {
        return this.messageList.subList(fromIndex, toIndex);
    }

    public Object[] toArray() {
        return this.messageList.toArray();
    }

    public <T> T[] toArray(T[] a) {
        return this.messageList.toArray(a);
    }

    public Message find(String primaryKey) {

        Message retVal = null;

        for (Message item : this.messageList) {

            if (item.getUniqueId().compareTo(primaryKey) == 0) {
                retVal = item;
                break;
            }
        }

        return retVal;
    }

    private void checkEntity(Message item) {

        String temp = item.getMetaMessage().getSystemName();

        if (temp.compareToIgnoreCase(this.refMetaMsg.getSystemName()) != 0)
            throw new IntegrationException("Can't add. Message object [%1$s] must be of type [%2$s]",
                    item.getUniqueId(), this.refMetaMsg.getSystemName());
    }
}