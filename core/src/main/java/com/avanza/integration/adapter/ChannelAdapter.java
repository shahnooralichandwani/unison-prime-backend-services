package com.avanza.integration.adapter;

import com.avanza.core.data.DbObject;
import com.avanza.core.sessionhistory.InterfaceChannel;

public class ChannelAdapter extends DbObject implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7728364104026721206L;

    private ChannelAdapterId id;
    private CommAdapter commAdapter;
    private InterfaceChannel interfaceChannel;
    private String channelAdapterParams;

    public ChannelAdapter() {
    }

    public ChannelAdapterId getId() {
        return this.id;
    }

    public void setId(ChannelAdapterId id) {
        this.id = id;
    }

    public CommAdapter getCommAdapter() {
        return this.commAdapter;
    }

    public void setCommAdapter(CommAdapter commAdapter) {
        this.commAdapter = commAdapter;
    }

    public InterfaceChannel getInterfaceChannel() {
        return this.interfaceChannel;
    }

    public void setInterfaceChannel(InterfaceChannel interfaceChannel) {
        this.interfaceChannel = interfaceChannel;
    }

    public String getChannelAdapterParams() {
        return this.channelAdapterParams;
    }

    public void setChannelAdapterParams(String channelAdapterParams) {
        this.channelAdapterParams = channelAdapterParams;
    }
}
