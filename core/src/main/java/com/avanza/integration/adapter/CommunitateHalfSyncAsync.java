package com.avanza.integration.adapter;

import com.avanza.integration.Message;


public interface CommunitateHalfSyncAsync {

    Message sendMessage(Message protocolMessage);

    void dispatchMessage(String threadId, StringBuilder sbuilder);
}
