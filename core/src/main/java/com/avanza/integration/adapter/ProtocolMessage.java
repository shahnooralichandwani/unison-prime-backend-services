package com.avanza.integration.adapter;

import java.util.HashMap;
import java.util.Map;

/**
 * The Generic Header class protocols can use it to override.
 *
 * @author kraza
 */
public abstract class ProtocolMessage {

    private String uniqueId;
    private Map<String, String> headerValues;
    private StringBuilder requestMessage;

    public ProtocolMessage() {
        headerValues = new HashMap<String, String>();
        requestMessage = new StringBuilder();
    }

    public abstract String getHeaderString();

    public abstract String toString();

    public StringBuilder getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(StringBuilder requestMessage) {
        this.requestMessage = requestMessage;
    }

    public String getHeaderValue(String key) {
        return headerValues.get(key);
    }

    public void setHeaderValue(String key, String headerValue) {
        this.headerValues.put(key, headerValue);
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Map<String, String> getHeaderValues() {
        return this.headerValues;
    }

    /**
     * Gets the size of the message in byte.
     *
     * @return
     */
    public int getMessageSize() {
        int messageSize = uniqueId.length() * 2;
        messageSize += this.getHeaderString().length();
        messageSize += this.getRequestMessage().length();
        return messageSize;
    }
}
