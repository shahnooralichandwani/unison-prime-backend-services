package com.avanza.integration.adapter;

import com.avanza.core.data.DbObject;
import com.avanza.core.sessionhistory.InterfaceChannel;

public class ChannelTranResponse extends DbObject implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5952977073859530575L;

    private String channelId;
    private InterfaceChannel interfaceChannel;
    private String responseCode;
    private String responseDescPrm;
    private String responseDescSec;

    public ChannelTranResponse() {
    }

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public InterfaceChannel getInterfaceChannel() {
        return this.interfaceChannel;
    }

    public void setInterfaceChannel(InterfaceChannel interfaceChannel) {
        this.interfaceChannel = interfaceChannel;
    }

    public String getResponseCode() {
        return this.responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDescPrm() {
        return this.responseDescPrm;
    }

    public void setResponseDescPrm(String responseDescPrm) {
        this.responseDescPrm = responseDescPrm;
    }

    public String getResponseDescSec() {
        return this.responseDescSec;
    }

    public void setResponseDescSec(String responseDescSec) {
        this.responseDescSec = responseDescSec;
    }

}
