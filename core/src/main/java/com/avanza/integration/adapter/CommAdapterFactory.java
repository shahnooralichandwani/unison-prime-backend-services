package com.avanza.integration.adapter;

import java.util.HashMap;
import java.util.Map;

import com.avanza.core.util.Logger;

/**
 * @author kraza
 */
public class CommAdapterFactory {

    private static final Logger logger = Logger.getLogger(CommAdapterFactory.class);
    private static CommAdapterFactory _instance;
    private static Map<String, CommunicationAdapter> communicationAdapters = new HashMap<String, CommunicationAdapter>();

    protected CommAdapterFactory() {
    }

    public static synchronized CommAdapterFactory getInstance() {
        if (_instance == null) {
            _instance = new CommAdapterFactory();
        }
        return _instance;
    }

    // returns the singleton object
    public CommunicationAdapter createAdapter(String implClass) {
        CommunicationAdapter impl = communicationAdapters.get(implClass);

        if (impl != null)
            return impl;

        Class CommunicationAdapterClass = null;
        try {
            CommunicationAdapterClass = Class.forName(implClass);
            impl = (CommunicationAdapter) CommunicationAdapterClass.newInstance();
            communicationAdapters.put(implClass, impl);
        } catch (ClassNotFoundException e) {
            logger.LogException(String.format("CommunitationAdapter Impl Class : %1$s not found.", implClass), e);
        } catch (InstantiationException e) {
            logger.LogException(String.format("CommunitationAdapter Impl Class : %1$s Instance can't created.", implClass), e);
        } catch (IllegalAccessException e) {
            logger.LogException(String.format("CommunitationAdapter Impl Class : %1$s Instance illegal Access.", implClass), e);
        }
        return impl;
    }
}
