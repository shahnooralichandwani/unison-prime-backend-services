package com.avanza.integration.adapter;

import java.io.BufferedOutputStream;


/**
 * Used to handle the callback events occoured.
 *
 * @author kraza
 */
public interface CommunicationCallback {

    public Object handleResponse(BufferedOutputStream response);
}
