package com.avanza.integration.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.core.data.DataRepository;
import com.avanza.core.data.HibernateDataBroker;
import com.avanza.core.sessionhistory.InterfaceChannel;

/**
 * Loads up the commAdapters from the db.
 *
 * @author kraza
 */
public class CommAdapterCatalog {

    private static CommAdapterCatalog _instance;
    private Map<String, CommAdapter> commAdapters;
    private HibernateDataBroker broker;

    protected CommAdapterCatalog() {
    }

    public static synchronized CommAdapterCatalog getInstance() {
        if (_instance == null) {
            _instance = new CommAdapterCatalog();
            _instance.load();
        }
        return _instance;
    }

    public CommAdapter getCommAdapter(String commAdapterId) {
        return commAdapters.get(commAdapterId);
    }

    public CommunicationAdapter getCommAdapterImpl(String commAdapterId) {
        return this.getCommAdapter(commAdapterId).getCommAdapterInstance();
    }

    public CommunicationAdapter getCommAdapterImplForChannel(String channelId) {

        InterfaceChannel ichannel = broker.findById(InterfaceChannel.class, channelId);

        if (ichannel == null)
            return null;

        return getCommAdapterImpl(ichannel);
    }

    private CommunicationAdapter getCommAdapterImpl(InterfaceChannel ichannel) {
        if (ichannel.getChannelAdapters().isEmpty())
            return null;

        ChannelAdapter cAdpt = ichannel.getChannelAdapters().iterator().next();

        return cAdpt.getCommAdapter().getCommAdapterInstance();
    }

    private void load() {
        commAdapters = new HashMap<String, CommAdapter>();
        broker = (HibernateDataBroker) DataRepository.getBroker(CommAdapterCatalog.class.getName());
        List<CommAdapter> commAdps = broker.findAll(CommAdapter.class);
        for (CommAdapter commAdp : commAdps) {
            commAdapters.put(commAdp.getCommAdapterId(), commAdp);
        }
    }
}
