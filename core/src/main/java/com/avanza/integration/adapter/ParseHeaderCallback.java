package com.avanza.integration.adapter;

import java.io.BufferedInputStream;


/**
 * Used to handle the callback events occoured.
 *
 * @author kraza
 */
public interface ParseHeaderCallback {

    public Object handle(BufferedInputStream response);
}
