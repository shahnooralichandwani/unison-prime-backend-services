package com.avanza.integration.adapter;

/**
 * The Interface to the communication Adapter.
 *
 * @author kraza
 */
public interface CommunicationAdapter {

    public void setCommunicationStrategy(CommunicationStrategy strategy);

    public CommunicationStrategy getCommunicationStrategy();

    public String getChannelAdapterParams();

    public String getHeaderSeparator();

    public void load(String adapterParams);

    public void dispose();
}
