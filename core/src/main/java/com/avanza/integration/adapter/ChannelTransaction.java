package com.avanza.integration.adapter;

import com.avanza.core.data.DbObject;
import com.avanza.core.sessionhistory.InterfaceChannel;
import com.avanza.integration.meta.messaging.MsgTransaction;

public class ChannelTransaction extends DbObject implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5957413954414803148L;

    private ChannelTransactionId id;
    private InterfaceChannel interfaceChannel;
    private MsgTransaction transaction;
    private String requestTranCode;
    private String responseTranCode;

    public ChannelTransaction() {
    }

    public ChannelTransactionId getId() {
        return this.id;
    }

    public void setId(ChannelTransactionId id) {
        this.id = id;
    }

    public InterfaceChannel getInterfaceChannel() {
        return this.interfaceChannel;
    }

    public void setInterfaceChannel(InterfaceChannel interfaceChannel) {
        this.interfaceChannel = interfaceChannel;
    }

    public String getRequestTranCode() {
        return this.requestTranCode;
    }

    public void setRequestTranCode(String requestTranCode) {
        this.requestTranCode = requestTranCode;
    }

    public String getResponseTranCode() {
        return this.responseTranCode;
    }

    public void setResponseTranCode(String responseTranCode) {
        this.responseTranCode = responseTranCode;
    }

    public MsgTransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(MsgTransaction transaction) {
        this.transaction = transaction;
    }
}
