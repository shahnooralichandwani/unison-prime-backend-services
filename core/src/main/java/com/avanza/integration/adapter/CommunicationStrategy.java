package com.avanza.integration.adapter;

import com.avanza.integration.IntegrationException;


public enum CommunicationStrategy {

    Sync(0), Async(1), HalfAsync(2);

    private int intValue;

    private CommunicationStrategy(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static CommunicationStrategy fromString(String value) {

        CommunicationStrategy retVal;

        if (value.equalsIgnoreCase("Sync"))
            retVal = CommunicationStrategy.Sync;
        else if (value.equalsIgnoreCase("Async"))
            retVal = CommunicationStrategy.Async;
        else if (value.equalsIgnoreCase("HalfAsync"))
            retVal = CommunicationStrategy.HalfAsync;
        else
            throw new IntegrationException("[%1$s] is not recognized as valid CommunicationStrategy", value);

        return retVal;
    }
}
