package com.avanza.integration.adapter;

public class ChannelAdapterId implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5124909867976797213L;

    private String commAdapterId;
    private String channelId;

    public ChannelAdapterId() {
    }

    public ChannelAdapterId(String commAdapterId, String channelId) {
        this.commAdapterId = commAdapterId;
        this.channelId = channelId;
    }

    public String getCommAdapterId() {
        return this.commAdapterId;
    }

    public void setCommAdapterId(String commAdapterId) {
        this.commAdapterId = commAdapterId;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public boolean equals(Object other) {
        if ((this == other))
            return true;
        if ((other == null))
            return false;
        if (!(other instanceof ChannelAdapterId))
            return false;
        ChannelAdapterId castOther = (ChannelAdapterId) other;

        return ((this.getCommAdapterId() == castOther.getCommAdapterId()) || (this
                .getCommAdapterId() != null
                && castOther.getCommAdapterId() != null && this
                .getCommAdapterId().equals(castOther.getCommAdapterId())))
                && ((this.getChannelId() == castOther.getChannelId()) || (this
                .getChannelId() != null
                && castOther.getChannelId() != null && this
                .getChannelId().equals(castOther.getChannelId())));
    }

    public int hashCode() {
        int result = 17;

        result = 37
                * result
                + (getCommAdapterId() == null ? 0 : this.getCommAdapterId()
                .hashCode());
        result = 37 * result
                + (getChannelId() == null ? 0 : this.getChannelId().hashCode());
        return result;
    }

}
