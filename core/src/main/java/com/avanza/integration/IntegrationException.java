package com.avanza.integration;

import com.avanza.core.CoreException;

public class IntegrationException extends CoreException {

    private static final long serialVersionUID = -606122954338075010L;

    public IntegrationException(String message) {

        super(message);
    }

    public IntegrationException(String format, Object... args) {

        super(format, args);
    }

    public IntegrationException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public IntegrationException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public IntegrationException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public IntegrationException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}