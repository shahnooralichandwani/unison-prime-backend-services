package com.avanza.integration.transaction;

import java.io.Serializable;
import java.util.List;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.DataSourceException;
import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.sdo.DataException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.DataObjectCollection;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.integration.Message;
import com.avanza.integration.meta.mapping.EntityMapperCatalog;
import com.avanza.integration.meta.mapping.OperationType;

/**
 * @author kraza
 */
public class TransactionPadBroker implements DataBroker {

    private static final Logger logger = Logger.getLogger(TransactionPadBroker.class);
    private static final String XML_VAKUE = "value";

    private String brokerKey;
    private DataBroker dataBroker;
    private String dataSource;
    private ProviderType providerType;

    public void load(ConfigSection configSection) {

        this.brokerKey = configSection.getTextValue(XML_VAKUE);
        this.dataBroker = DataRepository.getBroker(this.brokerKey);
        this.dataSource = configSection.getTextValue("dataSource");
        this.providerType = ProviderType.parse(configSection.getTextValue("providerType"));
    }

    public <T> T getSession() {

        return (T) dataBroker.getSession();
    }

    public <R, T> R findBy(T item) {

        this.checkType(item);

        DataObject dataObject = (DataObject) item;
        DataObjectCollection retVal = null;

        Message request = EntityMapperCatalog.prepareRequest(dataObject, OperationType.FindBy);

        //Need to add the TransactionLog to DB also.
        dataBroker.add(dataObject);

        Message response = TransactionManager.execute(request);

        DataObjectCollection dataObjectCollection = EntityMapperCatalog.updateObject(response, dataObject, OperationType.FindBy, false, null);


        dataBroker.update(dataObject);

        return (R) dataObjectCollection;
    }

    public <T> void add(T item) {
        try {
            this.checkType(item);

            DataObject dataObjet = (DataObject) item;

            Message request = EntityMapperCatalog.prepareRequest(dataObjet, OperationType.Add);

            //Need to add the TransactionLog to DB also.
            dataBroker.add(dataObjet);

            Message response = TransactionManager.execute(request);

            EntityMapperCatalog.updateObject(response, dataObjet, OperationType.Add, false, null);

            //Need to add the response message to the transaction logs tables.
            dataBroker.update(dataObjet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public <T> void delete(T item) {

        throw new TransactionException("TransactionPadBroker.delete(T) is not implemented");
    }

    public void dispose() {

        this.dataBroker.dispose();
    }

    public <T> List<T> find(String query) {

        return dataBroker.find(query);
    }

    public <T> List<T> find(Search query) {

        return this.dataBroker.find(query);
    }

    public <T> List<T> findAll(Class<T> item) {

        return dataBroker.findAll(item);
    }

    public <T, ID extends Serializable> T findById(Class<T> t, ID id) {

        return dataBroker.findById(t, id);
    }

    public <ID, T extends DataObject> T findById(String entityId, ID id) {

        return (T) dataBroker.findById(entityId, id);
    }

    public <T> void persist(T item) {

        this.checkType(item);
        DataObject obj = (DataObject) item;
        if (obj.isNew())
            this.add(item);
        else
            this.update(item);
    }

    public void synchronize() {

    }

    public <T> void update(T item) {

        throw new TransactionException("TransactionPadBroker.update(T) is not implemented");
    }

    private <T> void checkType(T item) {

        if (!(item instanceof DataObject)) {

            throw new TransactionException("[%1$s] must be either of type DataObject or derived from it", item.getClass().getName());
        }
    }

    public String getDataSource() {
        return dataSource;
    }

    public ProviderType getProviderType() {
        return this.providerType;
    }

    public <T> void persistAll(List<T> items) {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> List<T> findItemsByRange(Search search, int pageNo, int pageSize,
                                        String orderBy, boolean isAsc, int[] returnTotalRowCount) {
        throw new DataSourceException("Method TransactionPadBroker.findItemsByRange is not implemented]");

    }

    @Override
    public int getCriteriaCount(Search criteria) {
        throw new DataException("Not Implemented");
    }

    @Override
    public int deleteBySearch(Search search) {
        // TODO Auto-generated method stub
        return 0;

    }
}