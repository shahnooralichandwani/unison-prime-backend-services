package com.avanza.integration.transaction;

import com.avanza.integration.IntegrationException;

public enum TransactionLogField {

    STAN(0), TransactionId(1), ChannelId(2), RequestDateTime(3), ResponseDateTime(4), RequestTxnCode(5),
    ResponseTxnCode(6), ResponseStatusCode(7), ChannelSessionToken(8), LoginId(9), MetaEntityId(10),
    ParentTxnId(11), RequestErrorType(12), ResponseErrorType(13), ResponseDescription(14);

    private int intValue;

    private TransactionLogField(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static TransactionLogField fromString(String value) {

        TransactionLogField retVal;

        if (value.equalsIgnoreCase("STAN"))
            retVal = TransactionLogField.STAN;
        else if (value.equalsIgnoreCase("TransactionId"))
            retVal = TransactionLogField.TransactionId;
        else if (value.equalsIgnoreCase("ChannelId"))
            retVal = TransactionLogField.ChannelId;
        else if (value.equalsIgnoreCase("RequestDateTime"))
            retVal = TransactionLogField.RequestDateTime;
        else if (value.equalsIgnoreCase("ResponseDateTime"))
            retVal = TransactionLogField.ResponseDateTime;
        else if (value.equalsIgnoreCase("RequestTxnCode"))
            retVal = TransactionLogField.RequestTxnCode;
        else if (value.equalsIgnoreCase("ResponseTxnCode"))
            retVal = TransactionLogField.ResponseTxnCode;
        else if (value.equalsIgnoreCase("ResponseStatusCode"))
            retVal = TransactionLogField.ResponseStatusCode;
        else if (value.equalsIgnoreCase("ChannelSessionToken"))
            retVal = TransactionLogField.ChannelSessionToken;
        else if (value.equalsIgnoreCase("LoginId"))
            retVal = TransactionLogField.LoginId;
        else if (value.equalsIgnoreCase("ParentTxnId"))
            retVal = TransactionLogField.ParentTxnId;
        else if (value.equalsIgnoreCase("RequestErrorType"))
            retVal = TransactionLogField.RequestErrorType;
        else if (value.equalsIgnoreCase("ResponseErrorType"))
            retVal = TransactionLogField.ResponseErrorType;
        else if (value.equalsIgnoreCase("ResponseDescription"))
            retVal = TransactionLogField.ResponseDescription;

        else
            throw new IntegrationException("[%1$s] is not recognized as valid TransactionLogField", value);

        return retVal;
    }
}
