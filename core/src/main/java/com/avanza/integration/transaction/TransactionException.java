package com.avanza.integration.transaction;

import com.avanza.core.CoreException;

public class TransactionException extends CoreException {

    private static final long serialVersionUID = 884047135815320460L;

    public TransactionException(String message) {

        super(message);
    }

    public TransactionException(String format, Object... args) {

        super(format, args);
    }

    public TransactionException(RuntimeException innerExcep, String message) {

        super(message, innerExcep);
    }

    public TransactionException(RuntimeException innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }

    public TransactionException(Exception innerExcep, String message) {

        super(innerExcep, message);
    }

    public TransactionException(Exception innerExcep, String format, Object... args) {

        super(innerExcep, format, args);
    }
}