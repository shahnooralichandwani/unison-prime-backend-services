package com.avanza.integration.transaction;

import com.avanza.core.util.Guard;
import com.avanza.core.util.Logger;
import com.avanza.integration.Message;
import com.avanza.integration.adapter.CommAdapterCatalog;
import com.avanza.integration.adapter.CommunicationAdapter;
import com.avanza.integration.adapter.CommunicationStrategy;
import com.avanza.integration.adapter.CommunitateHalfSyncAsync;

/**
 * This class takes the request message and executes the transaction and returns the response message.
 *
 * @author kraza
 */
public class TransactionManager {

    private static final Logger logger = Logger.getLogger(TransactionManager.class);

    public static Message execute(Message request) {

        Guard.checkNull(request, "TramsactionManager's RequestMessage");

        Message responseMessage = null;

        try {
            //Getting the request msg formatter.
            String channelId = request.getMetaMessage().getTransaction().getChannelTransactions().iterator().next().getInterfaceChannel().getChannelId();

            // Now sending the message throught RDV adapter.
            CommunicationAdapter commAdapter = CommAdapterCatalog.getInstance().getCommAdapterImplForChannel(channelId);

            if (!commAdapter.getCommunicationStrategy().equals(CommunicationStrategy.HalfAsync) && commAdapter instanceof CommunitateHalfSyncAsync)
                throw new TransactionException("Communication Strategy other than HalfAsync is not supported.");

            CommunitateHalfSyncAsync strategicComAdapter = (CommunitateHalfSyncAsync) commAdapter;

            responseMessage = strategicComAdapter.sendMessage(request);

        } catch (Exception e) {
            logger.logDebug("Exception while sending the request to RDV STAN: %1$s.", request.getStan());
            logger.LogException("Exception occoured in TransactionManager.execute() method", e);
        }

        return responseMessage;
    }
}
