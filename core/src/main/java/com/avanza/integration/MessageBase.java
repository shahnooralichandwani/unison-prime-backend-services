package com.avanza.integration;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.avanza.core.util.Guard;
import com.avanza.integration.meta.messaging.MetaMessageField;

public abstract class MessageBase implements Serializable {

    private static final long serialVersionUID = -8351250604738095953L;

    protected Hashtable<String, MessageFieldBase> fieldList = new Hashtable<String, MessageFieldBase>();

    @SuppressWarnings("unused")
    protected MessageBase() {

    }

    public Object getValue(String name) {

        this.checkFieldExist(name);
        return this.innerGetValue(name);
    }

    public Map<String, Object> getValues(List<String> nameList) {

        Guard.checkNull(nameList, "Message.getValues(List<String>)");
        return this.innerGetValues(nameList.iterator(), nameList.size());
    }

    // type conversion methods
    public boolean getAsBoolean(String fieldName) {
        return this.innerGetField(fieldName).booleanValue();
    }

    public byte getAsByte(String fieldName) {
        return this.innerGetField(fieldName).byteValue();
    }

    public short getAsShort(String fieldName) {
        return this.innerGetField(fieldName).shortValue();
    }

    public int getAsInteger(String fieldName) {
        return this.innerGetField(fieldName).intValue();
    }

    public long getAsLong(String fieldName) {
        return this.innerGetField(fieldName).longValue();
    }

    public float getAsFloat(String fieldName) {
        return this.innerGetField(fieldName).floatValue();
    }

    public double getAsDouble(String fieldName) {
        return this.innerGetField(fieldName).doubleValue();
    }

    public Date getAsDate(String fieldName) {
        return this.innerGetField(fieldName).dateValue();
    }

    public char getAsCharacter(String fieldName) {
        return this.innerGetField(fieldName).charValue();
    }

    public String getAsString(String fieldName) {
        return this.innerGetField(fieldName).toString();
    }

    // Methods that set the value of attributes
    public void setValue(String fieldName, Object value) {
        this.innerSetValue(fieldName, value);
    }

    void setValue(MetaMessageField meta, Object value) {

        Guard.checkNull(meta, "Message.setValue(MetaAttribute, value");
        this.innerSetValue(meta.getSystemName(), value);
    }

    public void setValue(Map<String, Object> valueList) {

        Guard.checkNull(valueList, "Message.setValue(Map<String,Object>");

        for (Map.Entry<String, Object> item : valueList.entrySet()) {
            this.innerSetValue(item.getKey(), item.getValue());
        }
    }

    public void setValue(String fieldName, boolean value) {
        this.innerSetValue(fieldName, new Boolean(value));
    }

    public void setValue(String fieldName, byte value) {
        this.innerSetValue(fieldName, new Byte(value));
    }

    public void setValue(String fieldName, short value) {
        this.innerSetValue(fieldName, new Short(value));
    }

    public void setValue(String fieldName, int value) {
        this.innerSetValue(fieldName, new Integer(value));
    }

    public void setValue(String fieldName, long value) {
        this.innerSetValue(fieldName, new Long(value));
    }

    public void setValue(String fieldName, float value) {
        this.innerSetValue(fieldName, new Float(value));
    }

    public void setValue(String fieldName, double value) {
        this.innerSetValue(fieldName, new Double(value));
    }

    public void setValue(String fieldName, char value) {
        this.innerSetValue(fieldName, new Character(value));
    }

    public void setValue(String fieldName, String value) {
        this.innerSetValue(fieldName, value);
    }

    public void setValue(String fieldName, Date value) {
        this.innerSetValue(fieldName, value);
    }

    protected Map<String, Object> innerGetValues(Iterator<String> itr, int size) {

        HashMap<String, Object> result = new HashMap<String, Object>(size);

        while (itr.hasNext()) {
            String key = itr.next();
            result.put(key, this.innerGetValue(key));
        }

        return result;
    }

    protected abstract void prepareMessage();

    protected abstract void loadEntity(HashMap<String, Object> values);

    public Map<String, Object> getValues() {

        Iterator<String> itr = this.fieldList.keySet().iterator();
        return this.innerGetValues(itr, this.fieldList.size());
    }

    protected void checkFieldExist(String name) {

        Guard.checkNullOrEmpty(name, "Message.checkFieldExist(String)");

        if (!this.fieldList.containsKey(name))
            throw new IntegrationException("[%1$s] is not defined.", name);
    }

    protected Object innerGetValue(String name) {

        this.checkFieldExist(name);
        return this.fieldList.get(name).getValue();
    }

    protected void innerSetValue(String name, Object value) {

        this.checkFieldExist(name);
        MessageFieldBase temp = this.fieldList.get(name);
        temp.setValue(value);
    }

    protected MessageFieldBase innerGetField(String name) {

        this.checkFieldExist(name);
        return this.fieldList.get(name);
    }
}