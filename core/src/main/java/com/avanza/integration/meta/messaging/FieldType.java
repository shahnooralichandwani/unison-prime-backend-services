package com.avanza.integration.meta.messaging;

import com.avanza.integration.IntegrationException;


public enum FieldType {

    None(0), Single(1), List(2);

    private int intValue;

    private FieldType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static FieldType fromString(String value) {

        FieldType retVal;

        if (value.equalsIgnoreCase("None"))
            retVal = FieldType.None;
        else if (value.equalsIgnoreCase("Single"))
            retVal = FieldType.Single;
        else if (value.equalsIgnoreCase("List"))
            retVal = FieldType.List;
        else
            throw new IntegrationException("[%1$s] is not recognized as valid MessageType", value);

        return retVal;
    }


}

