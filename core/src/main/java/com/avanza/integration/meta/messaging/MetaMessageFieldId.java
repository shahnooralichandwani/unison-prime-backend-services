package com.avanza.integration.meta.messaging;


public class MetaMessageFieldId implements java.io.Serializable {


    // Fields    

    private String metaMsgId;
    private String fieldId;


    // Constructors

    /**
     * default constructor
     */
    public MetaMessageFieldId() {
    }


    /**
     * full constructor
     */
    public MetaMessageFieldId(String metaMsgId, String fieldId) {
        this.metaMsgId = metaMsgId;
        this.fieldId = fieldId;
    }


    // Property accessors

    public String getMetaMsgId() {
        return this.metaMsgId;
    }

    public void setMetaMsgId(String metaMsgId) {
        this.metaMsgId = metaMsgId;
    }

    public String getFieldId() {
        return this.fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public boolean equals(Object other) {
        if ((this == other)) return true;
        if ((other == null)) return false;
        if (!(other instanceof MetaMessageFieldId)) return false;
        MetaMessageFieldId castOther = (MetaMessageFieldId) other;

        return ((this.getMetaMsgId() == castOther.getMetaMsgId()) || (this.getMetaMsgId() != null && castOther.getMetaMsgId() != null && this.getMetaMsgId().equals(castOther.getMetaMsgId())))
                && ((this.getFieldId() == castOther.getFieldId()) || (this.getFieldId() != null && castOther.getFieldId() != null && this.getFieldId().equals(castOther.getFieldId())));
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (getMetaMsgId() == null ? 0 : this.getMetaMsgId().hashCode());
        result = 37 * result + (getFieldId() == null ? 0 : this.getFieldId().hashCode());
        return result;
    }
}
