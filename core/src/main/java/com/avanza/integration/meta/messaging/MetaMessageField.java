package com.avanza.integration.meta.messaging;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.DataType;
import com.avanza.integration.MessageField;


public class MetaMessageField extends DbObject implements MetaMessageFieldInterface, java.io.Serializable {

    private static final long serialVersionUID = 7139777666007151246L;

    private MetaMessageFieldId id;
    private MetaMessage metaMessage;
    private String displayName;
    private DataType type;
    private String calcExp;
    private String description;

    // Constructors

    @SuppressWarnings("unused")
    public MetaMessageField() {
    }

    public MetaMessageField(MetaMessageFieldId id, MetaMessage metaMessage, String displayName, DataType dataType) {
        this.id = id;
        this.metaMessage = metaMessage;
        this.displayName = displayName;
        this.type = dataType;
    }

    MetaMessageField(MetaMessageFieldId id, MetaMessage metaMessage, String displayName, DataType dataType, String calcExp, String description) {
        this.id = id;
        this.metaMessage = metaMessage;
        this.displayName = displayName;
        this.type = dataType;
        this.calcExp = calcExp;
        this.description = description;
    }

    // Property accessors

    public String getSystemName() {
        return this.getId().getFieldId();
    }

    public MetaMessageFieldId getId() {
        return this.id;
    }

    public MetaMessage getMetaMessage() {
        return this.metaMessage;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getType() {
        return this.type.toString();
    }

    public void setType(String dataType) {
        this.type = DataType.fromString(dataType);
    }

    public String getCalcExp() {
        return this.calcExp;
    }

    public void setCalcExp(String calcExp) {
        this.calcExp = calcExp;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MessageField createField() {

        return new MessageField(this, null);

    }

    public MessageField createField(Object value) {

        return new MessageField(this, value);
    }

    private void setId(MetaMessageFieldId id) {
        this.id = id;
    }


    private void setMetaMessage(MetaMessage metaMessage) {
        this.metaMessage = metaMessage;
    }
}