package com.avanza.integration.meta.messaging;

import com.avanza.integration.MessageFieldBase;

public interface MetaMessageFieldInterface {

    String getSystemName();

    MessageFieldBase createField();

    MessageFieldBase createField(Object value);

}
