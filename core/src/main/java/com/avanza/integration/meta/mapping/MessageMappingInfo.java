package com.avanza.integration.meta.mapping;

import java.io.Serializable;

/**
 * This class holds the message to the Entity Mappings.
 *
 * @author kraza
 */
public class MessageMappingInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1057773433843019864L;

    private String metaMessageId;
    private String metaEntityId;

    public MessageMappingInfo(MsgEntityMapping msgEntityMapping) {

        this.metaMessageId = msgEntityMapping.getId().getMetaMsgId();
        this.metaEntityId = msgEntityMapping.getId().getMetaEntId();
    }

    public String getMetaMessageId() {
        return metaMessageId;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }
}
