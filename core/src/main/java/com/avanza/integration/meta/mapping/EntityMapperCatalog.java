package com.avanza.integration.meta.mapping;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.function.expression.ExpressionHelper;
import com.avanza.core.meta.*;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.DataObjectCollection;
import com.avanza.core.security.User;
import com.avanza.core.sessionhistory.SessionHistory;
import com.avanza.core.util.*;
import com.avanza.core.web.config.WebContext;
import com.avanza.integration.IntegrationException;
import com.avanza.integration.Message;
import com.avanza.integration.MessageCollection;
import com.avanza.integration.meta.messaging.MetaMessage;
import com.avanza.integration.meta.messaging.MetaMessageCatalog;
import com.avanza.integration.meta.messaging.MetaMessageField;
import com.avanza.integration.meta.messaging.MsgTransaction;
import com.avanza.integration.transaction.TransactionLogField;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This class provides the mapping b/w the Meta Entity TXN, Meta Entity Attribs to Fields and MetaEntity to Message
 */
public class EntityMapperCatalog {

    private static final Logger logger = Logger.getLogger(EntityMapperCatalog.class);

    // Variables
    private static List<TransactionMappingInfo> txnEntityMapping = new ArrayList<TransactionMappingInfo>();

    private static List<MessageMappingInfo> msgEntityMapping = new ArrayList<MessageMappingInfo>();

    private static List<FieldAttributeMapping> msgFieldMapping = new ArrayList<FieldAttributeMapping>();

    // Methods
    public static void load() {

        logger.logDebug("Loading the EntityMapperCatalog ...");
        try {
            DataBroker broker = DataRepository.getBroker(EntityMapperCatalog.class.getName());

            // maintaining the map from metaentity to txn.
            List<EntityTxnMapping> entityTxnList = broker.findAll(EntityTxnMapping.class);
            for (EntityTxnMapping entityTxnMapping : entityTxnList) {
                TransactionMappingInfo txnMappingInfo = new TransactionMappingInfo(entityTxnMapping);
                txnEntityMapping.add(txnMappingInfo);
            }

            // maintaining the map from meta ent and msg.
            List<MsgEntityMapping> msgEntityList = broker.findAll(MsgEntityMapping.class);
            for (MsgEntityMapping msgEntMapping : msgEntityList) {
                MessageMappingInfo messageMappingInfo = new MessageMappingInfo(msgEntMapping);
                msgEntityMapping.add(messageMappingInfo);
            }

            // maintaining the map for the Field to attribute mapping.
            List<FieldAttributeMapping> fieldAttribMappingList = broker.findAll(FieldAttributeMapping.class);
            for (FieldAttributeMapping fieldAttributeMapping : fieldAttribMappingList) {
                msgFieldMapping.add(fieldAttributeMapping);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new MetaException(e, "Error While Loading EntityMapperCatalog");
        }
    }

    /**
     * This method searches for the first Transactionbased on the specified params.
     *
     * @param metaEntityId
     * @param opType
     * @return
     */
    public static TransactionMappingInfo getTxnFrom(String metaEntityId, OperationType opType) {

        Guard.checkNullOrEmpty(metaEntityId, "metaEntityId");
        Guard.checkNull(opType, "operationType");

        for (TransactionMappingInfo txnMapInfo : txnEntityMapping) {
            if (txnMapInfo.getMetaEntityId().equals(metaEntityId) && txnMapInfo.getOperationType() == opType) {
                return txnMapInfo;
            }
        }

        return null;
    }

    /**
     * This method returns the first meta message based on the metaEntityId
     *
     * @param metaEntityId
     * @return
     */
    public static MessageMappingInfo getMetaMsgInfoFrom(String metaEntityId) {

        Guard.checkNullOrEmpty(metaEntityId, "metaEntityId");

        for (MessageMappingInfo msgMappingInfo : msgEntityMapping) {
            if (msgMappingInfo.getMetaEntityId().equals(metaEntityId)) {
                return msgMappingInfo;
            }
        }

        return null;
    }

    /**
     * This method returns the first meta message based on the metaEntityId
     *
     * @param metaEntityId
     * @return
     */
    public static MessageMappingInfo getMetaEntityInfoFrom(String metaMsgId) {

        Guard.checkNullOrEmpty(metaMsgId, "metaMsgId");

        for (MessageMappingInfo msgMappingInfo : msgEntityMapping) {
            if (msgMappingInfo.getMetaMessageId().equals(metaMsgId)) {
                return msgMappingInfo;
            }
        }

        return null;
    }

    /**
     * This method finds the meta message and the field based on specified params.
     *
     * @param metaEntityId
     * @param metaEntAttribId
     * @return
     */
    public static FieldAttributeMapping getMessageFieldFrom(String metaEntityId, String metaEntAttribId) {

        Guard.checkNullOrEmpty(metaEntityId, "metaEntityId");
        Guard.checkNullOrEmpty(metaEntAttribId, "metaEntAttribId");

        for (FieldAttributeMapping fieldAttributeMapping : msgFieldMapping) {
            if (/*
             * fieldAttributeMapping.getMetaEntId().equals(metaEntityId) &&
             */fieldAttributeMapping.getAttributeId().equals(metaEntAttribId)) {
                return fieldAttributeMapping;
            }
        }

        return null;
    }

    /**
     * This method finds the meta entity and the attribute based on specified params.
     *
     * @param metaMsgId
     * @param metaMsgFieldId
     * @return
     */
    public static FieldAttributeMapping getEntityAttribFrom(String metaMsgId, String metaMsgFieldId) {

        Guard.checkNullOrEmpty(metaMsgId, "metaMsgId");
        Guard.checkNullOrEmpty(metaMsgFieldId, "metaMsgFieldId");

        for (FieldAttributeMapping fieldAttributeMapping : msgFieldMapping) {
            if (fieldAttributeMapping.getMetaMsgId().equals(metaMsgId) && fieldAttributeMapping.getFieldId().equals(metaMsgFieldId)) {
                return fieldAttributeMapping;
            }
        }

        return null;
    }

    /**
     * This method prepares the request tobe sent to the channel adapter.
     *
     * @param dataObjet
     * @param operationType
     * @return
     */
    public static Message prepareRequest(DataObject dataObject, OperationType operationType) {


        Guard.checkNull(dataObject, "dataObject");

        Message message = null;

        MetaEntity metaEntity = dataObject.getMetaEntity();
        TransactionMappingInfo txnMappingInfo = EntityMapperCatalog.getTxnFrom(metaEntity.getId(), operationType);
        MsgTransaction txn = MetaMessageCatalog.getTransaction(txnMappingInfo.getTransactionId());
        MetaMessage metaMsg = txn.getReqMetaMessage();
        ApplicationContext.getContext().add(metaEntity.getId(), dataObject);

        message = new Message(metaMsg);
        Map<String, Object> dataValues = dataObject.getValues();

        for (MetaMessageField metaMessageField : metaMsg.getFieldList().values()) {
            FieldAttributeMapping fieldAttributeMapping = EntityMapperCatalog.getEntityAttribFrom(metaMsg.getMetaMsgId(), metaMessageField.getId()
                    .getFieldId());

            if (fieldAttributeMapping == null) continue;

            MetaAttribute metaAttribute = metaEntity.getAttribute(fieldAttributeMapping.getAttributeId());

            // TODO KAZIM Validation checking here.

            String calcExpression = metaMessageField.getCalcExp();
            if (!StringHelper.isEmpty(calcExpression)) {
                String value = (String) ExpressionHelper.getExpressionResult(calcExpression, String.class);
                dataValues.put(metaAttribute.getId(), value);
            }

            message.setValue(metaMessageField.getSystemName(), dataValues.get(metaAttribute.getId()));
        }

        // Now we need to fill the dataObject for relevant fields for logging.
        WebContext webContext = ApplicationContext.getContext().get(WebContext.class);
        SessionHistory sessionHistory = webContext.getAttribute(SessionKeyLookup.CUSTOMER_SESSION_HISTORY);
        User user = webContext.getAttribute(SessionKeyLookup.CURRENT_USER_KEY);

        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String uniqueId = df.format(date) + CounterUtils.getNextTxnUniqueId();
        Map<String, MetaAttribute> attributeNames = metaEntity.getAttributeList();
        for (MetaAttribute attribute : attributeNames.values()) {
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.STAN.toString()))
                dataObject.setValue(TransactionLogField.STAN.toString(), uniqueId);
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.TransactionId.toString()))
                dataObject.setValue(TransactionLogField.TransactionId.toString(), txn.getTransactionId());
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.ChannelId.toString()))
                dataObject.setValue(TransactionLogField.ChannelId.toString(), txn.getChannelId());
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.RequestDateTime.toString()))
                dataObject.setValue(TransactionLogField.RequestDateTime.toString(), date);
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.RequestTxnCode.toString()))
                dataObject.setValue(TransactionLogField.RequestTxnCode.toString(), txn.getChannelTransaction().getRequestTranCode());
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.ChannelSessionToken.toString()))
                dataObject.setValue(TransactionLogField.ChannelSessionToken.toString(), sessionHistory == null ? null : sessionHistory.getSessionId());
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.LoginId.toString()))
                dataObject.setValue(TransactionLogField.LoginId.toString(), user.getLoginId());
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.MetaEntityId.toString()))
                dataObject.setValue(TransactionLogField.MetaEntityId.toString(), metaEntity.getId());

            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.ResponseDateTime.toString()))
                dataObject.setValue(TransactionLogField.ResponseDateTime.toString(), (Date) null);
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.ResponseTxnCode.toString()))
                dataObject.setValue(TransactionLogField.ResponseTxnCode.toString(), (String) null);
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.ResponseStatusCode.toString()))
                dataObject.setValue(TransactionLogField.ResponseStatusCode.toString(), (String) null);
            if (attribute.getSystemName().equalsIgnoreCase(TransactionLogField.ResponseErrorType.toString()))
                dataObject.setValue(TransactionLogField.ResponseErrorType.toString(), (Integer) null);
        }

        message.setDateTime(date);
        message.setStan(uniqueId);

        return message;


    }

    /**
     * This method prepares the response got from the channel adapter into the dataobject values.
     *
     * @param response
     * @param dataObjet
     * @param operationType
     */
    public static DataObjectCollection updateObject(Message response, DataObject dataObject, OperationType operationType, boolean isChild,
                                                    Message parentMessage) {

        Guard.checkNull(dataObject, "dataObjet");
        Guard.checkNull(response, "response");
        MetaEntity metaEntity = dataObject.getMetaEntity();
        DataObjectCollection dataObjectCollection = null;
        TransactionMappingInfo txnMappingInfo = EntityMapperCatalog.getTxnFrom(metaEntity.getId(), operationType);
        MsgTransaction txn = MetaMessageCatalog.getTransaction(txnMappingInfo.getTransactionId());
        MetaMessage metaMsg = txn.getResMetaMessage();
        ApplicationContext.getContext().add(response.getRefMetaMsg().getMetaMsgId(), response);

        if (!isChild) {
            if (!metaMsg.getMetaMsgId().equals(response.getMetaMessage().getMetaMsgId()))
                throw new IntegrationException("Response message is not mapped correctly to the returned response.");
        } else {
            ApplicationContext.getContext().add(parentMessage.getRefMetaMsg().getMetaMsgId(), parentMessage);
        }

        Map<String, Object> msgValues = response.getValues();
        for (MetaAttribute metaAttribute : metaEntity.getAttributeList().values()) {
            FieldAttributeMapping fieldAttributeMapping = EntityMapperCatalog.getMessageFieldFrom(metaEntity.getId(), metaAttribute.getId());

            if (fieldAttributeMapping == null)
                continue;
            else {
                // check if Convert Exp is defined or not.
            }

            if (!isChild) {
                if (!metaMsg.getMetaMsgId().equals(fieldAttributeMapping.getMetaMsgId())) continue;
            }

            MetaMessageField metaMessageField = MetaMessageCatalog.getMetaMessageField(fieldAttributeMapping.getMetaMsgId(), fieldAttributeMapping
                    .getFieldId());
            ApplicationContext.getContext().add("CurrentMetaMessageField", metaMessageField);
            // TODO KAZIM Validation checking here, Error and status code
            // checking?????.
            String calcExpression = metaMessageField.getCalcExp();
            if (!StringHelper.isEmpty(calcExpression)) {
                String value = (String) ExpressionHelper.getExpressionResult(calcExpression, String.class);
                msgValues.put(metaMessageField.getSystemName(), value);
            }
            if (!metaAttribute.getSystemName().equalsIgnoreCase("STAN"))
                dataObject.setValue(metaAttribute.getSystemName(), msgValues.get(metaMessageField.getSystemName()));
        }
        StringMap<MessageCollection> childMessageCollections = response.getChildMessageList();

        for (int messageCollectionCount = 0; messageCollectionCount < response.getChildCount(); messageCollectionCount++) {
            MessageCollection messageCollection = childMessageCollections.get(messageCollectionCount);
            List<DataObject> dataObjectList = new ArrayList<DataObject>();
            MetaEntity subMetaEntity = null;
            for (Message message : messageCollection.getMessageList()) {
                subMetaEntity = MetaDataRegistry.getMetaEntity(getMetaEntityInfoFrom(message.getRefMetaMsg().getMetaMsgId()).getMetaEntityId());
                DataObject dataObj = new DataObject(subMetaEntity);
                Iterator<Object> objectIter = dataObject.getValues().values().iterator();
                Iterator<String> keyIter = dataObject.getValues().keySet().iterator();
                while (keyIter.hasNext()) {
                    String key = (String) keyIter.next();
                    Object obj = (Object) objectIter.next();
                    if (dataObj.getValues().containsKey(key)) dataObj.setValue(key, obj);
                }
                dataObjectList.add(dataObj);
                updateObject(message, dataObj, operationType, true, response);
                dataObj.save();
            }
            if (subMetaEntity != null)
                dataObjectCollection = new DataObjectCollection(subMetaEntity, dataObjectList, dataObject);
            MetaRelationship metaRelation = metaEntity.getRelationShipBySubEntity(subMetaEntity.getId()).get(0);
            String relationShipId = metaRelation.getRelationshipId();
            dataObject.addDataObjectCollection(relationShipId, dataObjectCollection);
        }

        // TODO handle the child messages to dbobject conversion.

        // Now we need to fill the dataObject for relevant fields for logging.
        dataObject.setValue(TransactionLogField.ResponseDateTime.toString(), response.getDateTime());
        dataObject.setValue(TransactionLogField.ResponseTxnCode.toString(), txn.getChannelTransaction().getResponseTranCode());
        dataObject.setValue(TransactionLogField.ResponseStatusCode.toString(), response.getMsgCode());
        dataObject.setValue(TransactionLogField.ResponseErrorType.toString(), Integer.parseInt(response.getErrorType() == null ? "0" : response
                .getErrorType()));
        dataObject.setValue(TransactionLogField.ResponseDescription.toString(), response.getResponseDescription());

        return dataObjectCollection;
    }

    public static DataObject getResults(Message response, OperationType operationType) {
        return null;
    }


}
