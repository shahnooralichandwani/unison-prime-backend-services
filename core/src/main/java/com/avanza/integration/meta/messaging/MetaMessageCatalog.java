package com.avanza.integration.meta.messaging;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.meta.MetaException;
import com.avanza.integration.adapter.ChannelTransaction;
import com.avanza.integration.adapter.ChannelTransactionId;
import com.avanza.integration.formatter.MsgFormatData;
import com.avanza.integration.formatter.MsgFormatDataCatalog;

@Deprecated
public class MetaMessageCatalog {

//variables

    private static Hashtable<MetaMessageFieldId, MetaMessageField> metaMsgFields = new Hashtable<MetaMessageFieldId, MetaMessageField>();
    private static Hashtable<String, MetaMessage> metaMessages = new Hashtable<String, MetaMessage>();
    private static Hashtable<String, MsgTransaction> transactions = new Hashtable<String, MsgTransaction>();
    // TODO For what reason this is used.
    private static Map<ChannelTransactionId, List<MsgFormatData>> channelMsgFormatData = new HashMap<ChannelTransactionId, List<MsgFormatData>>();

    //Setters and Getters

    public Hashtable<String, MetaMessage> getMetaMessages() {
        return metaMessages;
    }

    public Hashtable<String, MsgTransaction> getTransactions() {
        return transactions;
    }


    //Methods

    public static void load() {

        try {
            DataBroker broker = DataRepository.getBroker(MetaMessageCatalog.class.getName());
            //For Loading messages at Start
            List<MetaMessage> msgList = broker.findAll(MetaMessage.class);
            for (MetaMessage msg : msgList) {
                String Id = msg.getMetaMsgId();
                addMetaMessages(Id, msg);
            }
            List<MsgTransaction> txnList = broker.findAll(MsgTransaction.class);
            for (MsgTransaction txn : txnList) {
                String txnId = txn.getTransactionId();
                addTransactions(txnId, txn);
                txn.getReqMetaMessage().setTransaction(txn);
                txn.getResMetaMessage().setTransaction(txn);
                addTxnToChildMsgs(txn.getResMetaMessage(), txn);
            }

            List<MetaMessageField> metaMsgFieldList = broker.findAll(MetaMessageField.class);
            for (MetaMessageField field : metaMsgFieldList) {
                metaMsgFields.put(field.getId(), field);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new MetaException(e,
                    "Error While Loading Meta Message Catalog");
        }
    }

    private static void addTxnToChildMsgs(MetaMessage metaMessage, MsgTransaction txn) {

        for (MetaMessage metaMsg : metaMessage.getChildMetaMsgs()) {
            metaMsg.setTransaction(txn);
            Iterator<MetaMessage> iter = metaMsg.getChildMetaMsgs().iterator();
            while (iter.hasNext()) {
                MetaMessage meta_Message = (MetaMessage) iter.next();
                addTxnToChildMsgs(meta_Message, txn);
            }
        }
    }

    static void addMetaMessages(String msgId, MetaMessage metaMessage) {
        metaMessages.put(msgId, metaMessage);
    }

    static void addTransactions(String Id, MsgTransaction txn) {
        transactions.put(Id, txn);
        for (ChannelTransaction channelTxn : txn.getChannelTransactions()) {
            addChannelMsgFormatData(channelTxn);
        }
    }

    static void addChannelMsgFormatData(ChannelTransaction channelTxn) {
        channelMsgFormatData.put(channelTxn.getId(), MsgFormatDataCatalog.getMsgFormatData(channelTxn.getId().getChannelId()));
    }

    public static List<MsgFormatData> getMsgFormatData(String ChannelId, String TransactionId) {
        Set<ChannelTransactionId> channelTxnId = channelMsgFormatData.keySet();
        Iterator<ChannelTransactionId> iterator = channelTxnId.iterator();
        while (iterator.hasNext()) {
            ChannelTransactionId channelTransId = ((ChannelTransactionId) iterator.next());
            if (channelTransId.getChannelId().equals(ChannelId) && channelTransId.getTransactionId().equals(TransactionId)) {
                return channelMsgFormatData.get(channelTransId);
            }

        }
        return null;
    }

    public static MetaMessageField getMetaMessageField(String metaMsgId, String fieldId) {
        return metaMsgFields.get(new MetaMessageFieldId(metaMsgId, fieldId));
    }

    public static MsgTransaction getTransaction(String transactionId) {
        return transactions.get(transactionId);
    }

    public static MetaMessage getMetaMessage(String messageId) {
        return metaMessages.get(messageId);
    }

    public static MetaMessage getResponseMessage(String transactionId) {
        return transactions.get(transactionId).getResMetaMessage();
    }

    public static MetaMessage getRequestMessage(String transactionId) {
        return transactions.get(transactionId).getReqMetaMessage();
    }

    public static String getResponseDescription(String respCode, String TransactionId) {

    /*    return transactions.get(TransactionId)
                    .getChannelTransaction().getInterfaceChannel().getChannelTranResponses().get(respCode).getResponseDescPrm();*/

        //TODO:: UNISONPRIME
        return null;
    }

}
