package com.avanza.integration.meta.mapping;


import com.avanza.core.data.DbObject;
import com.avanza.core.meta.MetaAttributeImpl;
import com.avanza.integration.meta.messaging.MetaMessageField;

public class FieldAttributeMapping extends DbObject implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8094013128332233521L;

    //Variables    
    private String metaMsgId;
    private String fieldId;
    private String attributeId;
    private MetaAttributeImpl metaEntityAttrib;
    private MetaMessageField metaMessageField;
    private String convertExp;


    //Constructors

    /**
     * default constructor
     */
    public FieldAttributeMapping() {
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public MetaAttributeImpl getMetaEntityAttrib() {
        return this.metaEntityAttrib;
    }

    public void setMetaEntityAttrib(MetaAttributeImpl metaEntityAttrib) {
        this.metaEntityAttrib = metaEntityAttrib;
    }

    public MetaMessageField getMetaMessageField() {
        return this.metaMessageField;
    }

    public void setMetaMessageField(MetaMessageField metaMessageField) {
        this.metaMessageField = metaMessageField;
    }

    public String getConvertExp() {
        return this.convertExp;
    }

    public void setConvertExp(String convertExp) {
        this.convertExp = convertExp;
    }

    public String getMetaMsgId() {
        return this.metaMsgId;
    }

    public void setMetaMsgId(String metaMsgId) {
        this.metaMsgId = metaMsgId;
    }
}
