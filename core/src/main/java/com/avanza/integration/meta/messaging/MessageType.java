package com.avanza.integration.meta.messaging;

import com.avanza.integration.IntegrationException;


public enum MessageType {

    None(0), Single(1), List(2);

    private int intValue;

    private MessageType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static MessageType fromString(String value) {

        MessageType retVal;

        if (value.equalsIgnoreCase("None"))
            retVal = MessageType.None;
        else if (value.equalsIgnoreCase("Single"))
            retVal = MessageType.Single;
        else if (value.equalsIgnoreCase("List"))
            retVal = MessageType.List;
        else
            throw new IntegrationException("[%1$s] is not recognized as valid MessageType", value);

        return retVal;
    }


}

