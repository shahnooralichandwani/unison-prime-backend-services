package com.avanza.integration.meta.mapping;

import java.io.Serializable;

/**
 * This class holds the TXN to Entity Mapping information.
 *
 * @author kraza
 */
public class TransactionMappingInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7370354989286130627L;

    private String txnMappingId;
    private String metaEntityId;
    private OperationType operationType;
    //    private MappingType mappingType;
    private String transactionId;

    public TransactionMappingInfo(EntityTxnMapping entityTxnMapping) {
        this.txnMappingId = entityTxnMapping.getEntTxnMapId();
        this.metaEntityId = entityTxnMapping.getReqMetaEntityId();
        this.operationType = OperationType.fromString(entityTxnMapping.getOpType());
        this.transactionId = entityTxnMapping.getMsgTransactionId();
//        this.mappingType = MappingType.fromString(entityTxnMapping.getMappingType());
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getTxnMappingId() {
        return txnMappingId;
    }

//    public MappingType getMappingType() {
//        return mappingType;
//    }
}
