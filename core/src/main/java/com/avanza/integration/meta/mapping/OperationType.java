package com.avanza.integration.meta.mapping;

import com.avanza.integration.IntegrationException;

public enum OperationType {

    Add(0), FindBy(1), Find(2), FindById(3);

    private int intValue;

    private OperationType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static OperationType fromString(String value) {

        OperationType retVal;

        if (value.equalsIgnoreCase("Add"))
            retVal = OperationType.Add;
        else if (value.equalsIgnoreCase("FindBy"))
            retVal = OperationType.FindBy;
        else if (value.equalsIgnoreCase("Find"))
            retVal = OperationType.Find;
        else if (value.equalsIgnoreCase("FindById"))
            retVal = OperationType.FindById;
        else
            throw new IntegrationException("[%1$s] is not recognized as valid OperationType", value);

        return retVal;
    }
}
