package com.avanza.integration.meta.messaging;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.avanza.core.data.DbObject;
import com.avanza.integration.meta.mapping.MsgEntityMapping;

/**
 * @author <B>ndanish<B>
 */

public class MetaMessage extends DbObject implements java.io.Serializable {

    private static final long serialVersionUID = 3099687476557429709L;

    // Fields
    private String metaMsgId;
    private String parentMsgId;
    private MetaMessage derivedParentMetaMsg;
    private String metaMsgName;
    private String description;
    private MsgTransaction transaction;
    private MessageType msgType;
    private Map<String, MetaMessageField> metaMessageFields = new HashMap<String, MetaMessageField>();
    private Set<MetaMessage> childMetaMsgs = new HashSet<MetaMessage>();
    private Set<MetaMessage> derivedMetaMsgs = new HashSet<MetaMessage>();
    private Set<MsgEntityMapping> msgEntityMappings = new HashSet<MsgEntityMapping>();
    private MetaMessage parentMetaMsg;

    // Transient members
    private int fieldCount;


    // Constructors

    /**
     * default constructor
     */
    public MetaMessage() {
    }

    /**
     * minimal constructor
     */
    public MetaMessage(String metaMsgId, String metaMsgName, MessageType msgType) {
        this.metaMsgId = metaMsgId;
        this.metaMsgName = metaMsgName;
        this.msgType = msgType;
    }

    // Property accessors
    public String getSystemName() {

        return this.getMetaMsgId();
    }

    public String getMetaMsgId() {

        return this.metaMsgId;
    }

    public void setMetaMsgId(String metaMsgId) {
        this.metaMsgId = metaMsgId;
    }

    public String getMetaMsgName() {
        return this.metaMsgName;
    }

    public void setMetaMsgName(String metaMsgName) {
        this.metaMsgName = metaMsgName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMsgType() {
        return this.msgType.toString();
    }

    public void setMsgType(String value) {

        this.msgType = MessageType.fromString(value);
    }

    private Map<String, MetaMessageField> getMetaMessageFields() {
        return this.metaMessageFields;
    }

    private void setMetaMessageFields(Map<String, MetaMessageField> metaMessageFields) {
        this.metaMessageFields = metaMessageFields;
    }

    public MsgTransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(MsgTransaction transaction) {
        this.transaction = transaction;
    }

    public Set<MetaMessage> getChildMetaMsgs() {
        return childMetaMsgs;
    }

    public void setChildMetaMsgs(Set<MetaMessage> childMetaMsgs) {
        this.childMetaMsgs = childMetaMsgs;
    }

    public String getParentMsgId() {
        return parentMsgId;
    }

    public void setParentMsgId(String parentMsgId) {
        this.parentMsgId = parentMsgId;
    }

    public Set<MsgEntityMapping> getMsgEntityMappings() {
        return msgEntityMappings;
    }

    public void setMsgEntityMappings(Set<MsgEntityMapping> msgEntityMappings) {
        this.msgEntityMappings = msgEntityMappings;
    }

    public MetaMessage getParentMetaMsg() {
        return parentMetaMsg;
    }

    public void setParentMetaMsg(MetaMessage parentMetaMsg) {
        this.parentMetaMsg = parentMetaMsg;
    }

    public void setMsgType(MessageType msgType) {
        this.msgType = msgType;
    }

    public Set<MetaMessage> getDerivedMetaMsgs() {
        return derivedMetaMsgs;
    }

    public void setDerivedMetaMsgs(Set<MetaMessage> derivedMetaMsgs) {
        this.derivedMetaMsgs = derivedMetaMsgs;
    }

    public MetaMessage getDerivedParentMetaMsg() {
        return derivedParentMetaMsg;
    }

    public void setDerivedParentMetaMsg(MetaMessage derivedParentMetaMsg) {
        this.derivedParentMetaMsg = derivedParentMetaMsg;
    }

    public Map<String, MetaMessageField> getFieldList(boolean complete) {

        Hashtable<String, MetaMessageField> retVal;

        if (!complete) {

            retVal = new Hashtable<String, MetaMessageField>(this.metaMessageFields.size());
            retVal.putAll(this.metaMessageFields);
            return retVal;
        } else {

            retVal = new Hashtable<String, MetaMessageField>(this.getFieldCount());
            Stack<MetaMessage> stack = this.getMessageStack();
            while (!stack.empty()) {

                MetaMessage message = stack.pop();
                retVal.putAll(message.metaMessageFields);
            }
        }

        return retVal;
    }

    public Map<String, MetaMessageField> getFieldList() {

        return this.getFieldList(true);
    }

    private Stack<MetaMessage> getMessageStack() {

        Stack<MetaMessage> stack = new Stack<MetaMessage>();

        MetaMessage message = this;
        while (message != null) {

            stack.push(message);
            message = message.derivedParentMetaMsg;
        }

        return stack;
    }

    public int size(boolean complete) {

        int retVal;

        if (complete) {

            if (this.fieldCount == -1) this.fieldCount = this.getFieldCount();

            retVal = this.fieldCount;
        } else
            retVal = this.metaMessageFields.size();

        return retVal;
    }

    public int size() {

        return this.size(true);
    }

    private int getFieldCount() {

        MetaMessage message = this;
        int retVal = 0;

        while (message != null) {
            retVal = message.metaMessageFields.size();
            message = this.derivedParentMetaMsg;
        }

        return retVal;
    }
}
