package com.avanza.integration.meta.mapping;

import com.avanza.integration.IntegrationException;

public enum MappingType {

    Unknown(0);

    private int intValue;

    private MappingType(int intValue) {
        this.intValue = intValue;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public static MappingType fromString(String value) {

        MappingType retVal;

        if (value.equalsIgnoreCase("Unknown"))
            retVal = MappingType.Unknown;
        else
            throw new IntegrationException("[%1$s] is not recognized as valid MappingType", value);

        return retVal;
    }
}
