package com.avanza.integration;

import java.io.Serializable;

import com.avanza.core.util.ConvertException;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.DataType;
import com.avanza.integration.meta.messaging.MetaMessageField;

public class MessageField extends MessageFieldBase implements java.lang.Comparable<MessageField>, Serializable {

    private static final long serialVersionUID = 7404676078654036687L;

    private MetaMessageField metaField;

    public MessageField(MetaMessageField metaField, Object value) {

        this.metaField = metaField;
        this.setValue(value);
    }

    public void setValue(Object value) {

        if (value == null) {
            this.value = value;
            return;
        }

        // TODO need to get datatype from MetaMessageField
        this.value = ConvertUtil.ConvertTo(value, DataType.fromString(this.metaField.getType()).getJavaType());
    }

    public void clear() {

        // TODO get default value from meta attribute
        //this.metaAttrib.getDefValue();
        throw new IntegrationException("MessageField.clear() is not implemented");
    }

    // TODO Need to implement
    public boolean isValid() {

        throw new IntegrationException("MessageField.isValid() is not implemented");
    }

    public boolean equals(Object value) {

        try {
            value = ConvertUtil.ConvertTo(value, DataType.fromString(this.metaField.getType()).getJavaType());
        } catch (ConvertException e) {
            return false;
        }

        return this.value.equals(value);
    }

    public MetaMessageField getMetaMessageField() {
        return this.metaField;
    }

    protected void setMetaAttrib(MetaMessageField metaField) {
        this.metaField = metaField;
    }

    @Override
    protected Object clone() {
        return new MessageField(this.metaField, this.value);
    }

    public int compareTo(MessageField obj) {
        throw new IntegrationException("MessageField.compareTo(Attribute) is not implemented");
    }
}