package com.avanza.integration.dbmapper.meta;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.DataType;

/**
 * This class holds the defination of the parameter passed to the DbMetaEntitySource.
 *
 * @author kraza
 */
public class DbMetaParams extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = -2336795663953989890L;

    private long dbMetaEntId;
    private String systemName;
    private DbMetaEntity dbMetaEntity;
    private DataType dataType;
    private DbMetaParamType parameterType;
    private String mappingColumnName;
    private short paramOrder; // order starts from "1"
    private String convertExp;

    public DbMetaParams() {
    }

    public long getDbMetaEntId() {
        return this.dbMetaEntId;
    }

    public void setDbMetaEntId(long dbMetaEntId) {
        this.dbMetaEntId = dbMetaEntId;
    }

    public String getSystemName() {
        return this.systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public DbMetaEntity getDbMetaEntity() {
        return this.dbMetaEntity;
    }

    public void setDbMetaEntity(DbMetaEntity dbMetaEntity) {
        this.dbMetaEntity = dbMetaEntity;
    }

    private String getDataType() {
        return this.dataType.toString();
    }

    public DataType getType() {
        return this.dataType;
    }

    private void setDataType(String dataType) {
        this.dataType = DataType.fromString(dataType);
    }

    public String getConvertExp() {
        return this.convertExp;
    }

    public void setConvertExp(String convertExp) {
        this.convertExp = convertExp;
    }

    public short getParamOrder() {
        return this.paramOrder;
    }

    public void setParamOrder(short paramOrder) {
        this.paramOrder = paramOrder;
    }

    public boolean isOutput() {
        return this.parameterType == DbMetaParamType.Output;
    }

    public boolean isInput() {
        return this.parameterType == DbMetaParamType.Input;
    }

    public boolean isInOut() {
        return this.parameterType == DbMetaParamType.InOut;
    }

    public int getParameterType() {
        return parameterType.getValue();
    }


    public void setParameterType(int parameterType) {
        this.parameterType = DbMetaParamType.fromInt(parameterType);
    }
}
