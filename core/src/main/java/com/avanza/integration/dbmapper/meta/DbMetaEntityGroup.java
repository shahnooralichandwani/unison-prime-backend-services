package com.avanza.integration.dbmapper.meta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avanza.integration.meta.mapping.OperationType;


public class DbMetaEntityGroup {

    private Map<OperationType, List<DbMetaEntity>> dbMetaEntities;

    public DbMetaEntityGroup() {
        this.dbMetaEntities = new HashMap<OperationType, List<DbMetaEntity>>();
    }

    public void add(OperationType opType, DbMetaEntity dbMetaEntity) {

        List<DbMetaEntity> entityList = this.dbMetaEntities.get(opType);
        if (entityList == null) {
            entityList = new ArrayList<DbMetaEntity>();
            this.dbMetaEntities.put(opType, entityList);
        }
        entityList.add(dbMetaEntity);
    }

    public void add(String opType, DbMetaEntity dbMetaEntity) {
        this.add(OperationType.fromString(opType), dbMetaEntity);
    }

    public List<DbMetaEntity> get(OperationType opType) {
        return this.dbMetaEntities.get(opType);
    }
}
