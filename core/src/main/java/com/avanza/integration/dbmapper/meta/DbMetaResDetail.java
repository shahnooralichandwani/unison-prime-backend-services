package com.avanza.integration.dbmapper.meta;

import com.avanza.core.data.DbObject;

/**
 * This class is used for mapping the resultset value to more than one metaEntityAttribute value in DataObject.
 *
 * @author kraza
 */
public class DbMetaResDetail extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 6640543707806095608L;

    private long dbMetaResultId;
    private String metaEntityId;
    private String metaAttribId;
    private DbMetaResult dbMetaResult;


    public DbMetaResDetail() {
    }

    public String getMetaEntityId() {
        return this.metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

    public String getMetaAttribId() {
        return this.metaAttribId;
    }

    public void setMetaAttribId(String metaAttribId) {
        this.metaAttribId = metaAttribId;
    }

    public DbMetaResult getDbMetaResult() {
        return this.dbMetaResult;
    }

    public void setDbMetaResult(DbMetaResult dbMetaResult) {
        this.dbMetaResult = dbMetaResult;
    }


    public long getDbMetaResultId() {
        return dbMetaResultId;
    }


    public void setDbMetaResultId(long dbMetaResultId) {
        this.dbMetaResultId = dbMetaResultId;
    }
}
