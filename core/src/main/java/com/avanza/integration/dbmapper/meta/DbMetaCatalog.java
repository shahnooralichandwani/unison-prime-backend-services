package com.avanza.integration.dbmapper.meta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.avanza.core.CoreInterface;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.db.sql.SqlParameter;
import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.integration.meta.mapping.OperationType;

/**
 * The catalog for fetching the DbMetaEntity.
 *
 * @author kraza
 */
public class DbMetaCatalog implements CoreInterface {

    private static final Logger logger = Logger.getLogger(DbMetaCatalog.class);

    private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static Lock readLock = readWriteLock.readLock();
    private static Lock writeLock = readWriteLock.writeLock();

    private static DbMetaCatalog _instance;
    private DataBroker broker;
    private Map<String, DbMetaEntityGroup> dbMetaEntityMap;
    private Map<Long, DbMetaEntity> dbMetaEntityIdMap;

    protected DbMetaCatalog() {
        dbMetaEntityMap = new HashMap<String, DbMetaEntityGroup>();
        dbMetaEntityIdMap = new HashMap<Long, DbMetaEntity>();
    }

    public static synchronized DbMetaCatalog getInstance() {

        if (_instance == null) {
            _instance = new DbMetaCatalog();
            _instance.load(null);
        }

        return _instance;
    }

    public void load(ConfigSection section) {
        this.broker = DataRepository.getBroker(DbMetaEntity.class.getName());
        List<DbMetaEntity> dbMetaEntityList = this.broker.findAll(DbMetaEntity.class);
        for (DbMetaEntity dbMetaEntity : dbMetaEntityList) {
            DbMetaEntityGroup grp = this.dbMetaEntityMap.get(dbMetaEntity.getMetaEntId());
            if (grp == null) {
                grp = new DbMetaEntityGroup();
                this.dbMetaEntityMap.put(dbMetaEntity.getMetaEntId(), grp);
            }
            grp.add(dbMetaEntity.getOperationType(), dbMetaEntity);
            this.dbMetaEntityIdMap.put(dbMetaEntity.getDbMetaEntId(), dbMetaEntity);
        }
    }

    public List<DbMetaEntity> getDbMetaEntity(OperationType operationType, String metaEntityId) {
        return this.dbMetaEntityMap.get(metaEntityId).get(operationType);
    }

    public List<DbMetaEntity> getDbMetaEntity(OperationType operationType, String metaEntityId, List<SqlParameter> paramNames) {

        List<DbMetaEntity> retEntities = new ArrayList<DbMetaEntity>();
        List<DbMetaEntity> dbEntities = this.dbMetaEntityMap.get(metaEntityId).get(operationType);

        if (dbEntities == null)
            return retEntities;

        for (DbMetaEntity entity : dbEntities) {
            // This case is not clear, it can be used later
			/*if (paramNames.size() == entity.getDbMetaParams().values().size()) {

				boolean isValidEntity = true;
				for (SqlParameter pname : paramNames) {
					DbMetaParams dbParam = entity.getDbMetaParams().get(pname.getName());
					if (dbParam == null)
						isValidEntity = false;
				}
				if (isValidEntity)
					retEntities.add(entity);
			} else
				continue;*/
        }

        return retEntities;
    }

    public DbMetaEntity getDbMetaEntity(long dbMetaEntityId) {
        return this.dbMetaEntityIdMap.get(new Long(dbMetaEntityId));
    }

    public void dispose() {
    }

    public void reload() {
        this.broker = DataRepository.getBroker(DbMetaEntity.class.getName());
        //Shuwiar
        try {
            readLock.lock();
            dbMetaEntityMap = new HashMap<String, DbMetaEntityGroup>(0);
            dbMetaEntityIdMap = new HashMap<Long, DbMetaEntity>(0);

            List<DbMetaEntity> dbMetaEntityList = this.broker.findAll(DbMetaEntity.class);
            for (DbMetaEntity dbMetaEntity : dbMetaEntityList) {
                DbMetaEntityGroup grp = this.dbMetaEntityMap.get(dbMetaEntity.getMetaEntId());
                if (grp == null) {
                    grp = new DbMetaEntityGroup();
                    this.dbMetaEntityMap.put(dbMetaEntity.getMetaEntId(), grp);
                }
                grp.add(dbMetaEntity.getOperationType(), dbMetaEntity);
                this.dbMetaEntityIdMap.put(dbMetaEntity.getDbMetaEntId(), dbMetaEntity);
            }
        } finally {
            readLock.unlock();
        }
    }
}
