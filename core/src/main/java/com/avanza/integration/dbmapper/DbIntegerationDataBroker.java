package com.avanza.integration.dbmapper;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.avanza.core.CoreException;
import com.avanza.core.cache.Cacheable;
import com.avanza.core.cache.CustomerSessionCacheUtil;
import com.avanza.core.constants.ThreadContextLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.DataSourceException;
import com.avanza.core.data.SessionHibernateImpl;
import com.avanza.core.data.db.DataSourceDownException;
import com.avanza.core.data.db.DbConnection;
import com.avanza.core.data.db.ProviderType;
import com.avanza.core.data.db.sql.SqlBuilder;
import com.avanza.core.data.db.sql.SqlParameter;
import com.avanza.core.data.expression.ComplexCriterion;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.ObjectCriterion;
import com.avanza.core.data.expression.OperatorType;
import com.avanza.core.data.expression.Search;
import com.avanza.core.data.expression.SimpleCriterion;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalog;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataException;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.util.ConvertUtil;
import com.avanza.core.util.Guard;
import com.avanza.core.util.Logger;
import com.avanza.core.util.StringHelper;
import com.avanza.core.util.TimeLogger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.integration.dbmapper.meta.DbMetaCatalog;
import com.avanza.integration.dbmapper.meta.DbMetaEntity;
import com.avanza.integration.dbmapper.meta.DbMetaEntityType;
import com.avanza.integration.dbmapper.meta.DbMetaParamType;
import com.avanza.integration.dbmapper.meta.DbMetaParams;
import com.avanza.integration.dbmapper.meta.DbMetaResDetail;
import com.avanza.integration.dbmapper.meta.DbMetaResult;
import com.avanza.integration.meta.mapping.OperationType;

/**
 * @author kraza
 */
public class DbIntegerationDataBroker implements DataBroker {

    private static final Logger logger = Logger.getLogger(DbIntegerationDataBroker.class);
    private static final String ERROR_MSG = "Exception occoured in DbIntegerationDataBroker while invoking method %1$s";

    private String brokerKey;
    private String dataSource;
    private ProviderType providerType;
    private Criterion complexCriterion = null;

    public <T> void add(T item) {

        logger.logDebug("DbIntegerationDataBroker.add(item) called, for item: %s", item.toString());

        if (!(item instanceof DataObject)) throw new DataException("Not supported item must be instance of DataObject");

        TimeLogger timer = TimeLogger.init();
        timer.begin(DbIntegerationDataBroker.class, "add(item)");

        DataObject dbObj = (DataObject) item;

        MetaEntity metaEntity = dbObj.getMetaEntity();
        Guard.checkNull(metaEntity, "item.getMetaEntity()");

        List<DbMetaEntity> dbEntities = this.findDBMetaEntityFrom(OperationType.Add, metaEntity, null);
        if (dbEntities == null || dbEntities.isEmpty())
            throw new DataException("DbMetaEntities found null while expection one entity for: %s.", metaEntity.getId());

        if (dbEntities.size() != 1)
            throw new DataException("Currently the mapping of multiple SP is not supported for Add operation in DbIntegeration for: %s.", metaEntity
                    .getId());

        DbMetaEntity dbEntity = dbEntities.get(0);
        if (dbEntity.getType().equals(DbMetaEntityType.Query)) {

            timer.end();
            throw new DataException("Query Type DbMetaEntity not supported currently for Add(item).");
        }
        if (dbEntity.getType().equals(DbMetaEntityType.View)) {

            timer.end();
            throw new DataException("View Type DbMetaEntity not supported currently for Add(item).");
        } else if (dbEntity.getType().equals(DbMetaEntityType.StoredProcedure)) {

            SqlBuilder sqlBuilder = SqlBuilder.getInstance(this.getProviderType(), dbEntity.getDataSource());
            ArrayList<SqlParameter> inputParams = new ArrayList<SqlParameter>(dbEntity.getDbMetaParams().size());
            for (int idx = 0; idx < dbEntity.getDbMetaParams().size(); idx++) {
                inputParams.add(null);
            }

            for (DbMetaParams dbMetaParam : dbEntity.getDbMetaParams().values()) {
                inputParams.set(dbMetaParam.getParamOrder() - 1, new SqlParameter(dbMetaParam.getSystemName(), dbObj.getValue(dbMetaParam
                        .getSystemName())));
            }

            String procedureCall = prepareStoredProcdureCall(dbEntity, inputParams, sqlBuilder.getConnection());
            logger.logDebug("Stored Procedure called : %s .", procedureCall);
            sqlBuilder.executeProcedureInsertCmd(procedureCall, inputParams);

            timer.end();
            return;
        }

        timer.end();
        throw new DataException("Not Implemented");
    }

    public <T> void delete(T item) {
        throw new DataException("Not Implemented");
    }

    public void dispose() {
        throw new DataException("Not Implemented");
    }

    public <T> List<T> find(String query) {
        throw new DataException("Not Implemented");
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> find(Search query) {
        complexCriterion = null;
        logger.logDebug("DbIntegerationDataBroker.find(Search) called, Search Query String: %s", query.toString());
        TimeLogger timer = TimeLogger.init();
        timer.begin(DbIntegerationDataBroker.class, String.format("find(Search) for %s.", query.getFirstFrom()));
        List<T> retVal = null;

        String metaEntityId = query.getFirstFrom().getName();
        if (StringHelper.isEmpty(metaEntityId)) {
            timer.end();
            throw new DataException("Error while getting the metaEntityId from search 'From' Object, %s.", query.toString());
        }

        List<Map<String, Object>> results = null;
        HashMap<Object, DataObject> mappedDbObjects = new HashMap<Object, DataObject>();
        MetaEntity metaEntity = MetaDataRegistry.getEntityBySystemName(metaEntityId);// REFACTORED

        List<SqlParameter> inpParam = new ArrayList<SqlParameter>();
        //if (query.getRootCriterion() != null) this.prepareInputParameters(null, query.getRootCriterion(), inpParam, null);
        List<DbMetaEntity> dbEntities = this.findDBMetaEntityFrom(OperationType.Find, metaEntity, null);

        for (DbMetaEntity dbEntity : dbEntities) {

            if (dbEntity.getType().equals(DbMetaEntityType.Query)) {

                results = handleFindByQuery(query, dbEntity, null);
            } else if (dbEntity.getType().equals(DbMetaEntityType.View)) {

                timer.end();
                throw new DataException(
                        "Views Type DbMetaEntity not supported currently, cover it through the MetaEntity Views or through DbMetaEntity Query type (select * from view)");
            } else if (dbEntity.getType().equals(DbMetaEntityType.StoredProcedure)) {

                results = handleFindBySP(query, dbEntity);
            }

            if (results != null) {
                retVal = (List<T>) MetaDataRegistry.getDataObjects(metaEntity, results, metaEntity.getDiscriminatorCol(), dbEntity
                        .getDataAccessMode());
                for (T type : retVal) {
                    DataObject dbObj = (DataObject) type;
                    String idValue = String.valueOf(dbObj.hashCode());

                    DataObject mapedobj = mappedDbObjects.get(idValue);
                    if (mapedobj != null) {
                        dbObj.copyFrom(mapedobj, true);
                    }
                    mappedDbObjects.put(idValue, dbObj);
                }
            }
        }

        if (retVal != null) {
            CustomerSessionCacheUtil.updateCache((List<Cacheable>) retVal);
            timer.end();
            return retVal;
        }

        timer.end();
        throw new DataException("Not Implemented");
    }

    private List<DbMetaEntity> findDBMetaEntityFrom(OperationType opType, MetaEntity metaEntity, List<SqlParameter> paramNames) {

        List<DbMetaEntity> dbEntity = null;
        if (StringHelper.isNotEmpty(metaEntity.getId())) {
            if (paramNames != null)
                dbEntity = DbMetaCatalog.getInstance().getDbMetaEntity(opType, metaEntity.getId(), paramNames);
            else
                dbEntity = DbMetaCatalog.getInstance().getDbMetaEntity(opType, metaEntity.getId());
        }
        if (dbEntity == null) {
            if (metaEntity.getParent() != null)
                dbEntity = findDBMetaEntityFrom(opType, metaEntity.getParent(), paramNames);
        }

        return dbEntity;
    }

    private List<Map<String, Object>> handleFindByQuery(Search query, DbMetaEntity dbEntity, DataObject dataObj) {

        SqlBuilder sqlBuilder = SqlBuilder.getInstance(this.getProviderType(), dbEntity.getDataSource());
        ResultSet rset = null;
        List<Map<String, Object>> results = null;
        try {
            String sqlQuery = dbEntity.getSqlQuery();
            if (StringHelper.isEmpty(sqlQuery))
                throw new DataException("SQL Query cannot be null for dbMetaEntity: %s", dbEntity.getDbMetaEntId());

            ArrayList<SqlParameter> inputParams = new ArrayList<SqlParameter>(dbEntity.getDbMetaParams().size());
            for (int idx = 0; idx < dbEntity.getDbMetaParams().size(); idx++) {
                inputParams.add(null);
            }
            this.prepareInputParameters(dbEntity, query.getRootCriterion(), inputParams, dataObj);
            // TODO Add the default values for parameters not specified in search object.
            // Search sqlSearch = Search.parseSearch(sqlQuery, false);
            // TODO Handle pagination tasks.

            rset = sqlBuilder.executeSelectCmd(sqlQuery, inputParams);
            results = this.prepareResultList(dbEntity, rset);
        } catch (DataSourceDownException d) {
            logger.LogException("Exception Occoured.", d);
            addToExceptionMessages(d, dbEntity.getDataSource());
        } catch (Throwable t) {
            logger.LogException("Exception occoured while calling the findByQuery in DBIntegerationBroker.", t);
            throw new CoreException(t);
        } finally {
            if (rset != null) {
                try {
                    rset.close();
                } catch (Throwable t) {
                    logger.LogException("Exception while closing the connection", t);
                    addToExceptionMessages(t, dbEntity.getDataSource());
                }
                sqlBuilder.close();
            }
        }
        return results;
    }

    private List<Map<String, Object>> handleFindBySP(Search query, DbMetaEntity dbEntity) {

        SqlBuilder sqlBuilder = SqlBuilder.getInstance(this.getProviderType(), dbEntity.getDataSource());
        ResultSet rset = null;
        List<Map<String, Object>> results = null;

        try {
            ArrayList<SqlParameter> inputParams = new ArrayList<SqlParameter>(dbEntity.getDbMetaParams().size());
            for (int idx = 0; idx < dbEntity.getDbMetaParams().size(); idx++) {
                inputParams.add(null);
            }
            if (inputParams != null && !inputParams.isEmpty()) {
                this.prepareInputParameters(dbEntity, query.getRootCriterion(), inputParams, null);
                this.prepareInOutParameters(dbEntity, inputParams);
            }

            String procedureCall = prepareStoredProcdureCall(dbEntity, inputParams, sqlBuilder.getConnection());
            logger.logDebug("Stored Procedure called : %s .", procedureCall);

            rset = sqlBuilder.executeProcedureCallCmd(procedureCall, inputParams);
            results = this.prepareResultList(dbEntity, rset);

            return results;
        } catch (DataSourceDownException d) {
            logger.LogException("Exception Occoured.", d);
            addToExceptionMessages(d, dbEntity.getDataSource());
        } catch (Throwable t) {
            logger.LogException("Exception Occoured.", t);
            throw new CoreException(t);
        } finally {
            if (rset != null) {
                try {
                    rset.close();
                } catch (Throwable t) {
                    logger.LogException("Exception while closing the connection", t);
                    addToExceptionMessages(t, dbEntity.getDataSource());
                }
                sqlBuilder.close();
            }
        }

        return null;
    }

    private void prepareInOutParameters(DbMetaEntity dbEntity, ArrayList<SqlParameter> inputParams) {

        for (DbMetaParams dbParam : dbEntity.getDbMetaParams().values()) {
            SqlParameter sqlParam = inputParams.get(dbParam.getParamOrder() - 1);
            if (sqlParam == null && dbParam.isOutput()) {
                sqlParam = new SqlParameter(dbParam.getType(), dbParam.getSystemName());
                inputParams.set(dbParam.getParamOrder() - 1, sqlParam);
            } else if (dbParam.isInOut()) {
                sqlParam.setParamType(DbMetaParamType.InOut);
            } else if (dbParam.isInput()) {
                // TODO Fill Default Value.
            }
        }
    }

    private String prepareStoredProcdureCall(DbMetaEntity dbEntity, ArrayList<SqlParameter> inputParams, DbConnection dbConn) {

        StringBuilder sb = new StringBuilder("{CALL %s(");
        for (int i = 0; i < inputParams.size(); i++) {
            sb.append("?");
            if (i < (inputParams.size() - 1)) {
                sb.append(",");
            }
        }
        sb.append(")}");

        ApplicationContext.getContext().add("DataSource", dbConn.getDbInfo().getName());
        FunctionCatalog functionCatalog = FunctionCatalogManager.getInstance().getFunctionCatalog(ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction);
        ExpressionParser expParser = new ExpressionParser(dbEntity.getSystemName(), String.class, functionCatalog);
        String procName = (String) ExpressionEvaluator.getInstance().evaluate(expParser.parse());

        return String.format(sb.toString(), procName);
    }

    private List<Map<String, Object>> prepareResultList(DbMetaEntity dbEntity, ResultSet rset) {

        try {
            FunctionCatalog functionCatalog = FunctionCatalogManager.getInstance().getFunctionCatalog(ExpressionClass.ExpressionFunction,
                    ExpressionClass.ContextFunction);
            Object value = null;
            Object retValue = null;
            List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
            while (rset.next()) {
                Map<String, Object> result = new HashMap<String, Object>();
                for (DbMetaResDetail detail : dbEntity.getDbMetaResultDetails()) {

                    DbMetaResult dbMetaResult = detail.getDbMetaResult();
                    if (!dbMetaResult.isCalculated()) {
                        try {
                            logger.logDebug("Getting Rset for %s : %s", dbMetaResult.getSystemName(), dbMetaResult.getType().toString());
                            value = rset.getObject(dbMetaResult.getSystemName());
                            logger.logDebug("Value is %s", value);
                            if (value != null)
                                retValue = ConvertUtil.ConvertTo(value, dbMetaResult.getType().getJavaType());
                            else
                                retValue = null;
                            result.put(detail.getMetaAttribId(), retValue);
                        } catch (Throwable t) {
                            logger.LogException(String.format(
                                    "Exception occoured when parsing the resultSet from %s DBIntegeration, system name = %s, value = %s", dbEntity
                                            .getSystemName(), dbMetaResult.getSystemName(), value), t);
                        }
                    } else {
                        if (StringHelper.isNotEmpty(dbMetaResult.getConversionExp())) {
                            ExpressionParser expParser = new ExpressionParser(dbMetaResult.getConversionExp(), dbMetaResult.getType().getJavaType(), functionCatalog);
                            ExpressionEvaluator eval = new ExpressionEvaluator(retValue);
                            retValue = eval.evaluate(expParser.parse());
                            result.put(detail.getMetaAttribId(), retValue);
                        }
                    }
                }
                results.add(result);
            }
            return results;
        } catch (Throwable t) {
            throw new DataException("Exception occoured while prepareResultList(ResultSet).");
        }
    }

    private void prepareInputParameters(DbMetaEntity dbEntity, Criterion criterion, List<SqlParameter> inputParams, DataObject dataObj) {
        if (complexCriterion == null)
            complexCriterion = criterion;
        FunctionCatalog functionCatalog = FunctionCatalogManager.getInstance().getFunctionCatalog(ExpressionClass.ExpressionFunction,
                ExpressionClass.ContextFunction);
        int idx = 0;

        if (criterion != null && criterion.isSimple()) {
            if (criterion instanceof SimpleCriterion) {

                SimpleCriterion<?> simpleCr = ((SimpleCriterion<?>) criterion);
                if (simpleCr.getOperator() != OperatorType.None || !(criterion instanceof ObjectCriterion)) {

                    if (dbEntity != null) {
                        // dbParam =
                        // dbEntity.getDbMetaParams().get(simpleCr.getFieldName());
                        for (Entry<String, DbMetaParams> entry : dbEntity.getDbMetaParams().entrySet()) {

                            DbMetaParams dbParam = entry.getValue();

                            idx = dbParam.getParamOrder() - 1;
                            Object convertedValue = null;

                            if (simpleCr.getFieldName().equalsIgnoreCase(dbParam.getSystemName())) {
                                convertedValue = simpleCr.getValue();
                                if (StringHelper.isNotEmpty(dbParam.getConvertExp())) {
                                    ApplicationContext.getContext().add(dbParam.getSystemName(), convertedValue);
                                    ExpressionParser expParser = new ExpressionParser(dbParam.getConvertExp(), dbParam.getType().getJavaType(), functionCatalog);
                                    if (dataObj != null) {
                                        ExpressionEvaluator eval = new ExpressionEvaluator(dataObj, convertedValue);
                                        convertedValue = eval.evaluate(expParser.parse());
                                    } else {
                                        ExpressionEvaluator eval = new ExpressionEvaluator(convertedValue);
                                        convertedValue = eval.evaluate(expParser.parse());
                                    }

                                }

                                // parameter is other then criteria
                            } else {

                                if (StringHelper.isNotEmpty(dbParam.getConvertExp())) {
                                    ExpressionParser expParser = new ExpressionParser(dbParam.getConvertExp(), dbParam.getType().getJavaType(), functionCatalog);
                                    if (dataObj != null) {
                                        ExpressionEvaluator eval = new ExpressionEvaluator(dataObj, convertedValue);
                                        convertedValue = eval.evaluate(expParser.parse());
                                    } else {
                                        ExpressionEvaluator eval = new ExpressionEvaluator(convertedValue);
                                        convertedValue = eval.evaluate(expParser.parse());
                                    }


                                }

                            }
                            if (convertedValue != null)
                                ApplicationContext.getContext().add(dbParam.getSystemName(), convertedValue);

                            SqlParameter sqlParam = new SqlParameter(dbParam.getType(), dbParam.getSystemName(), convertedValue, DbMetaParamType.Input);
                            if (convertedValue != null)
                                inputParams.set(idx, sqlParam);
                            logger.logDebug("Input parameters for dbEntity: %s is: ", dbEntity.getSystemName(), sqlParam.toString());
                        }
                    } else {
                        SqlParameter sqlParam = new SqlParameter(simpleCr.getFieldName(), simpleCr.getValue());
                        inputParams.add(sqlParam);
                    }
                }
            } else { // Handle join form
                prepareInputParameters(dbEntity, ((ObjectCriterion) criterion).getRootCriterion(), inputParams, null);
            }
        } else if (complexCriterion != null && complexCriterion.isComplex()) {
            ComplexCriterion complexCr = ((ComplexCriterion) criterion);
            prepareInputParameters(dbEntity, complexCr.getLhs(), inputParams, null);
            prepareInputParameters(dbEntity, complexCr.getRhs(), inputParams, null);
        }
    }

    public <T> List<T> findAll(Class<T> item) {
        throw new DataException("Not Implemented");
    }

    public <T, ID extends Serializable> T findById(Class<T> t, ID id) {
        throw new DataException("Not Implemented");
    }

    public <ID, T extends DataObject> T findById(String metaEntityId, ID id) throws DataSourceException {

        logger.logDebug("DbIntegerationDataBroker.findById(%s, %s) is called", metaEntityId, id);
        Guard.checkNullOrEmpty(metaEntityId, "metaEntityId");
        Guard.checkNull(id, "id");

        TimeLogger timer = TimeLogger.init();
        timer.begin(DbIntegerationDataBroker.class, String.format("findById(%s)", metaEntityId));

        DataObject retVal = null;
        List<Map<String, Object>> results = null;
        MetaEntity metaEntity = MetaDataRegistry.getEntityBySystemName(metaEntityId);// REFACTORED

        SqlParameter sp = new SqlParameter(metaEntity.getPrimaryKeyAttribute().getSystemName(), id);
        List<SqlParameter> sqlParams = new ArrayList<SqlParameter>();
        sqlParams.add(sp);
        List<DbMetaEntity> dbEntities = this.findDBMetaEntityFrom(OperationType.FindById, metaEntity, null);
        dbEntities = this.sortDbMetaEntities(dbEntities);
        for (DbMetaEntity dbEntity : dbEntities) {
            if (dbEntity.getType().equals(DbMetaEntityType.Query)) {

                Search query = new Search();
                query.addFrom(metaEntityId);
                query.addCriterion(SimpleCriterion.equal(metaEntity.getPrimaryKeyAttribute().getSystemName(), id));
                results = handleFindByQuery(query, dbEntity, retVal);
            } else if (dbEntity.getType().equals(DbMetaEntityType.View)) {

                timer.end();
                throw new DataException(
                        "Views Type DbMetaEntity not supported currently, cover it through the MetaEntity Views or through DbMetaEntity Query type (select * from view)");
            } else if (dbEntity.getType().equals(DbMetaEntityType.StoredProcedure)) {

                Search query = new Search();
                query.addFrom(metaEntityId);
                query.addCriterion(SimpleCriterion.equal(metaEntity.getPrimaryKeyAttribute().getSystemName(), id));
                results = handleFindBySP(query, dbEntity);
            }

            if (results != null) {

                DataObject retObj = null;
                List<DataObject> dobList = MetaDataRegistry.getDataObjects(metaEntity, results, metaEntity.getDiscriminatorCol(), dbEntity
                        .getDataAccessMode());
                if (!dobList.isEmpty() && dobList.size() >= 1) retObj = (DataObject) dobList.get(0);
                if (retVal == null)
                    retVal = retObj;
                else if (retObj != null) retVal.copyFrom(retObj, true);
            }
        }

        // Copying the Basic level values from the cache if found and not expired.
        if (retVal != null) {

            DataObject basicObj = (DataObject) CustomerSessionCacheUtil.cacheLookUp(retVal.getKey());
            CustomerSessionCacheUtil.updateCache((Cacheable) retVal);
            // if (basicObj != null && !basicObj.isExpired() && basicObj.getDataAccessMode().equals(DataAccessMode.Basic)) {
            // retVal.copyFrom(basicObj, true);
            // }
            timer.end();
            return (T) retVal;
        }

        timer.end();
        return null;
    }

    public String getDataSource() {
        return this.dataSource;
    }

    public ProviderType getProviderType() {
        return this.providerType;
    }

    public <T> T getSession() {
        throw new DataException("Not Implemented");
    }

    public void load(ConfigSection configSection) {

        try {
            this.brokerKey = configSection.getTextValue("key");
            this.dataSource = configSection.getTextValue("dataSource");
            this.providerType = ProviderType.parse(configSection.getTextValue("providerType"));
        } catch (Throwable t) {
            logger.LogException(ERROR_MSG, t, "load(ConfigSection configSection)");
            throw new CoreException(t);
        }
    }

    public <T> void persist(T item) {
        throw new DataException("Not Implemented");
    }

    public void synchronize() {
        throw new DataException("Not Implemented - Ever");
    }

    public <T> void update(T item) {
        throw new DataException("Not Implemented");
    }

    private void addToExceptionMessages(Throwable t, String database) {
        ApplicationContext.getContext().add(ThreadContextLookup.DbErrorMesssage.toString(), String.format("DataSource: %s is down.", database));
    }

    private List<DbMetaEntity> sortDbMetaEntities(List<DbMetaEntity> dbEntities) {
        List<DbMetaEntity> orderedEntities = new ArrayList<DbMetaEntity>();
        orderedEntities.addAll(dbEntities);
        Collections.sort(orderedEntities, new Comparator<DbMetaEntity>() {

            public int compare(DbMetaEntity o1, DbMetaEntity o2) {
                if (o1.getExec_seq() > o2.getExec_seq())
                    return 1;
                else if (o1.getExec_seq() < o2.getExec_seq()) return -1;
                return 0;
            }
        });

        return orderedEntities;
    }

    public <T> void persistAll(List<T> items) {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> List<T> findItemsByRange(Search query, int pageNo, int pageSize,
                                        String orderBy, boolean isAsc, int[] returnTotalRowCount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getCriteriaCount(Search criteria) {
        throw new DataException("Not Implemented");
    }

    @Override
    public int deleteBySearch(Search search) {
        // TODO Auto-generated method stub
        return 0;

    }


}
