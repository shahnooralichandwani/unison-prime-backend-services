package com.avanza.integration.dbmapper.meta;

import java.util.HashSet;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.DataType;

/**
 * This class is mapped to the Resultset fetched by the execution of the DbMetaEntity Source.
 *
 * @author kraza
 */
public class DbMetaResult extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 2048817534775338615L;

    private long dbMetaResultId;
    private String systemName;
    private DbMetaEntity dbMetaEntity;
    private DataType dataType;
    private boolean calculated;
    private String conversionExp;
    private Set<DbMetaResDetail> dbMetaResDetails = new HashSet<DbMetaResDetail>(0);

    public DbMetaResult() {
    }

    public String getSystemName() {
        return this.systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public DbMetaEntity getDbMetaEntity() {
        return this.dbMetaEntity;
    }

    public void setDbMetaEntity(DbMetaEntity dbMetaEntity) {
        this.dbMetaEntity = dbMetaEntity;
    }

    private String getDataType() {
        return this.dataType.toString();
    }

    public DataType getType() {
        return this.dataType;
    }

    private void setDataType(String dataType) {
        this.dataType = DataType.fromString(dataType);
    }

    public boolean isCalculated() {
        return this.calculated;
    }

    public void setCalculated(boolean calculated) {
        this.calculated = calculated;
    }

    public Set<DbMetaResDetail> getDbMetaResDetails() {
        return this.dbMetaResDetails;
    }

    public void setDbMetaResDetails(Set<DbMetaResDetail> dbMetaResDetails) {
        this.dbMetaResDetails = dbMetaResDetails;
    }

    public String getConversionExp() {
        return conversionExp;
    }

    public void setConversionExp(String conversionExp) {
        this.conversionExp = conversionExp;
    }

    public long getDbMetaResultId() {
        return dbMetaResultId;
    }

    public void setDbMetaResultId(long dbMetaResultId) {
        this.dbMetaResultId = dbMetaResultId;
    }
}
