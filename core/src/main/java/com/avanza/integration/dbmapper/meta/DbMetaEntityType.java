package com.avanza.integration.dbmapper.meta;

/**
 * This enums holds the differnt executors type like Query Executor and stored procedure executor.
 *
 * @author kraza
 */
public enum DbMetaEntityType {

    StoredProcedure("StoredProcedure"), Query("Query"), View("View");

    private String value;

    DbMetaEntityType(String value) {
        this.value = value;
    }

    public String toString() {
        return this.value;
    }
}
