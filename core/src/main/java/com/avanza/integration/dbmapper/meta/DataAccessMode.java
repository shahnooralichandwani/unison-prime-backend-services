package com.avanza.integration.dbmapper.meta;


public enum DataAccessMode {

    Basic("Basic"), Detail("Detail");

    private String mode;

    private DataAccessMode(String mode) {
        this.mode = mode;
    }

    @Override
    public String toString() {
        return mode;
    }
}
