package com.avanza.integration.dbmapper.meta;


public enum DbMetaParamType {

    Unknown(0), Input(1), Output(2), InOut(3);

    private int val;

    DbMetaParamType(int val) {
        this.val = val;
    }

    public int getValue() {
        return this.val;
    }

    public static DbMetaParamType fromInt(int type) {
        switch (type) {
            case 1:
                return Input;
            case 2:
                return Output;
            case 3:
                return InOut;
            default:
                return Unknown;
        }
    }

    public boolean isOutput() {
        return this.val == Output.val;
    }

    public boolean isInput() {
        return this.val == Input.val;
    }

    public boolean isInOut() {
        return this.val == InOut.val;
    }

}
