package com.avanza.integration.dbmapper.meta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.avanza.core.data.DbObject;
import com.avanza.core.util.DataType;
import com.avanza.core.util.StringHelper;

/**
 * This class is for mapping the Views, SQLQueries and stored procedures to the DataObjects.
 *
 * @author kraza
 */
public class DbMetaEntity extends DbObject {

    /**
     *
     */
    private static final long serialVersionUID = 6724879759119608890L;

    private long dbMetaEntId;
    private String metaEntId;
    private int exec_seq;
    private DbMetaEntityType dbEntType;
    private String operationType;
    private String systemName;
    private String dataSource; //datasource serve as a key to get the dbConnection from DbCatalog
    private String sqlQuery;
    private DataType returnType;
    private DataAccessMode accessMode;
    private Set<DbMetaResult> dbMetaResults = new HashSet<DbMetaResult>(0);
    private Map<String, DbMetaParams> dbMetaParams = new HashMap<String, DbMetaParams>(0);
    private List<DbMetaResDetail> dbMetaResultDetails;

    public DbMetaEntity() {
    }

    public long getDbMetaEntId() {
        return this.dbMetaEntId;
    }

    public void setDbMetaEntId(long dbMetaEntId) {
        this.dbMetaEntId = dbMetaEntId;
    }

    public String getMetaEntId() {
        return this.metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

    private String getDbEntType() {
        return this.dbEntType.toString();
    }

    public DbMetaEntityType getType() {
        return this.dbEntType;
    }

    private void setDbEntType(String dbEntType) {
        this.dbEntType = DbMetaEntityType.valueOf(dbEntType);
    }

    public String getOperationType() {
        return this.operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getSystemName() {
        return this.systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getSqlQuery() {
        return this.sqlQuery;
    }

    public void setSqlQuery(String sqlQuery) {
        this.sqlQuery = sqlQuery;
    }

    private String getReturnType() {
        return this.returnType.toString();
    }

    private DataType getReturnDataType() {
        return this.returnType;
    }

    private void setReturnType(String returnType) {
        this.returnType = DataType.fromString(StringHelper.isEmpty(returnType) ? "None" : returnType);
    }

    public Set<DbMetaResult> getDbMetaResults() {
        return this.dbMetaResults;
    }

    public void setDbMetaResults(Set<DbMetaResult> dbMetaResults) {
        this.dbMetaResults = dbMetaResults;
    }

    public Map<String, DbMetaParams> getDbMetaParams() {
        return this.dbMetaParams;
    }

    public void setDbMetaParams(Map<String, DbMetaParams> dbMetaParams) {
        this.dbMetaParams = dbMetaParams;
    }

    public List<DbMetaResDetail> getDbMetaResultDetails() {
        if (dbMetaResultDetails != null)
            return dbMetaResultDetails;

        dbMetaResultDetails = new ArrayList<DbMetaResDetail>();
        for (DbMetaResult result : this.getDbMetaResults()) {
            for (DbMetaResDetail detail : result.getDbMetaResDetails()) {
                dbMetaResultDetails.add(detail);
            }
        }
        return dbMetaResultDetails;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public DataAccessMode getDataAccessMode() {
        return accessMode;
    }

    public String getAccessMode() {
        return accessMode.toString();
    }

    public void setAccessMode(String accessMode) {
        this.accessMode = DataAccessMode.valueOf(accessMode);
    }


    public int getExec_seq() {
        return exec_seq;
    }


    public void setExec_seq(int exec_seq) {
        this.exec_seq = exec_seq;
    }
}
