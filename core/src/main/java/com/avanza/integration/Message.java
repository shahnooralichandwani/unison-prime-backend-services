package com.avanza.integration;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.avanza.core.util.Guard;
import com.avanza.core.util.StringMap;
import com.avanza.integration.meta.messaging.MetaMessage;
import com.avanza.integration.meta.messaging.MetaMessageField;

public class Message extends MessageBase implements java.lang.Cloneable, java.lang.Comparable<Message>, Serializable {

    private static final long serialVersionUID = -8351250604738095953L;

    // TODO also modify the clone method if you made any change in the member fields
    private String stan;
    private int serialNum;
    private Date dateTime;
    private String msgCode;
    private String responseDescription;
    private String errorType;

    private MetaMessage refMetaMsg;
    private StringMap<MessageCollection> childMessageList = new StringMap<MessageCollection>(0);

    public MetaMessage getRefMetaMsg() {
        return refMetaMsg;
    }

    @SuppressWarnings("unused")
    private Message() {
    }

    public Message(MetaMessage refMetaMsg) {

        Guard.checkNull(refMetaMsg, "Message.Message(MetaMessage)");
        this.refMetaMsg = refMetaMsg;
        this.prepareMessage();
    }

    public Message(MetaMessage refMetaMsg, HashMap<String, Object> values) {

        this.init(refMetaMsg);
        this.loadEntity(values);
    }

    private void init(MetaMessage refMetaMsg) {

        Guard.checkNull(refMetaMsg, "Message.init(MetaMessage)");
        this.refMetaMsg = refMetaMsg;
        this.fieldList = new Hashtable<String, MessageFieldBase>();
        this.childMessageList = new StringMap<MessageCollection>();
    }

    public String getUniqueId() {

        String retVal;

        if (this.serialNum == 0)
            retVal = String.format("%1$s : %2$s", this.refMetaMsg.getSystemName(), this.stan);
        else
            retVal = String.format("%1$s : %2$s-%3$d", this.refMetaMsg.getSystemName(), this.stan, this.serialNum);

        return retVal;
    }

    public int getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(int serialNum) {
        this.serialNum = serialNum;
    }

    public String getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public String getStan() {
        return stan;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public int getChildCount() {
        return this.childMessageList.size();
    }

    public MessageCollection getChildList(String systemName) {

        MessageCollection retVal = this.childMessageList.get(systemName);

        if (retVal == null)
            throw new IntegrationException("[%1$s] is not identified as valid child collection of Message type [%2$s]",
                    systemName, this.refMetaMsg.getSystemName());

        return retVal;
    }

    public StringMap<MessageCollection> getChildMessageList() {

        return childMessageList;
    }

    public void addChild(String systemName, Message msg) {

        MessageCollection temp = this.getChildList(systemName);
        temp.add(msg);
    }

    public Message getChild(String systemName, String uniqueId) {

        MessageCollection temp = this.getChildList(systemName);
        return temp.get(uniqueId);
    }

    public boolean equals(Object obj) {

        if (!(obj instanceof Message))
            return false;

        Message rhs = (Message) obj;

        return (this.getUniqueId().compareTo(rhs.getUniqueId()) == 0);
    }

    // TODO need to implement
    public boolean isNull(String fieldName) {

        throw new IntegrationException("Message.isNull(String) is not implemented");
    }


    public MetaMessage getMetaMessage() {
        return this.refMetaMsg;
    }

    public int compareTo(Message obj) {

        return this.getUniqueId().compareTo(obj.getUniqueId());
    }

    @SuppressWarnings("unchecked")
    public Object clone() {

        // TODO need to implement
        return null;
    }

    @Override
    public int hashCode() {

        return this.getUniqueId().hashCode();
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    protected void prepareMessage() {

        Map<String, MetaMessageField> metaList = this.refMetaMsg.getFieldList(true);

        for (MetaMessageField item : metaList.values()) {

            this.fieldList.put(item.getSystemName(), item.createField());
        }
    }

    protected void loadEntity(HashMap<String, Object> values) {

        if (values == null)
            return;

        Map<String, MetaMessageField> metaList = this.refMetaMsg.getFieldList(true);

        for (MetaMessageField item : metaList.values()) {
            Object value = values.get(item.getId().getFieldId());
            this.fieldList.put(item.getSystemName(), item.createField(value));
        }
    }

}