package com.avanza.integration;

import java.io.Serializable;
import java.util.Date;

import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;

public abstract class MessageFieldBase implements Serializable {

    private static final long serialVersionUID = 7404676078654036687L;

    protected Object value;

    public MessageFieldBase() {
    }

    public Object getValue() {

        return this.value;
    }

    public abstract void setValue(Object value);

    public boolean isNull() {

        return (this.value == null);
    }

    public String toString() {

        if (this.value == null)
            return StringHelper.EMPTY;

        return this.value.toString();
    }

    public boolean booleanValue() {
        return Convert.toBoolean(this.value);
    }

    public int intValue() {
        return Convert.toInteger(this.value);
    }

    public byte byteValue() {
        return Convert.toByte(this.value);
    }

    public short shortValue() {
        return Convert.toShort(this.value);
    }

    public long longValue() {
        return Convert.toLong(this.value);
    }

    public float floatValue() {
        return Convert.toFloat(this.value);
    }

    public double doubleValue() {
        return Convert.toDouble(this.value);
    }

    public Date dateValue() {
        return Convert.toDate(this.value);
    }

    public char charValue() {
        return Convert.toChar(this.value);
    }

    public void setValue(boolean value) {
        this.setValue(new Boolean(value));
    }

    public void setValue(byte value) {
        this.setValue(new Byte(value));
    }

    public void setValue(short value) {
        this.setValue(new Short(value));
    }

    public void setValue(int value) {
        this.setValue(new Integer(value));
    }

    public void setValue(long value) {
        this.setValue(new Long(value));
    }

    public void setValue(float value) {
        this.setValue(new Float(value));
    }

    public void setValue(double value) {
        this.setValue(new Double(value));
    }

    public void setValue(char value) {
        this.setValue(new Character(value));
    }

    public void setValue(String value) {
        this.setValue((Object) value);
    }

    public void setValue(Date value) {
        this.setValue((Object) value);
    }
}