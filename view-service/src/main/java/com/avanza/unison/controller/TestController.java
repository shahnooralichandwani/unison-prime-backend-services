package com.avanza.unison.controller;

//import com.avanza.core.data.DataBroker;
//import com.avanza.core.data.DataRepository;
//import com.avanza.core.data.expression.Search;
//import com.avanza.core.meta.MetaDataRegistry;
//import com.avanza.core.meta.MetaEntity;
//import com.avanza.core.meta.PickListManager;
//import com.avanza.core.meta.PicklistData;
//import com.avanza.core.sdo.DataObject;
//import com.avanza.ui.meta.MetaView;
//import com.avanza.ui.meta.MetaViewAttribute;
//import com.avanza.ui.meta.ViewCatalog;
//import com.avanza.unison.data.DataUtil;
//import com.avanza.unison.data.KeyValueData;
//import com.avanza.unison.entity.MetaViewAttrib;
//import com.fasterxml.jackson.databind.util.JSONPObject;
//import com.google.gson.Gson;
//import com.sun.org.apache.xpath.internal.operations.Bool;
//import org.json.JSONArray;
//import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class TestController {
 /*   private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    String systemName, filterText = "";
    private List<String> users;
    private List<ColumnModel> columns;
    private List<String> columnTemplate = new ArrayList<String>();
    private List<String> validColumns = new ArrayList<String>();

    @Autowired
    DataUtil dataUtil;

    @RequestMapping("/greeting")
    public Object greeting() {
        MetaEntity metaEntity = MetaDataRegistry.getMetaEntity("0000000043");
        systemName = metaEntity.getSystemName();

        MetaView view = ViewCatalog.getMetaView("0000000147");
        view.getAttribute(view.getOrderedAttributes().get(0).getMetaAttributeId()).getMetaAttribSystemName();

        List<MetaViewAttribute> viewAttribs = view.getOrderedAttributes();
        StringBuilder sb = new StringBuilder();

        List<Map<String, Object>> listHeaderMap = new ArrayList<>();
        Map<String, Object> headerMap = new HashMap<String, Object>();
        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();

        for (MetaViewAttribute vAttrib : viewAttribs) {
            //sb.append("{");
            headerMap.put("key", vAttrib.getMetaAttribSystemName());
            headerMap.put("value", vAttrib.getDisplayNamePimary());
            headerMap.put("type", (vAttrib.getMetaAttribute().getPickListId() == null || vAttrib.getMetaAttributeId().equals("0000001073")) ? vAttrib.getComponentType() : "ComboBox");
            headerMap.put("editable", Boolean.toString(vAttrib.isReadOnly()));
            headerMap.put("values", (vAttrib.getMetaAttribute().getPickListId() == null || vAttrib.getMetaAttributeId().equals("0000001073")) ? null : KeyValueData.getComboData(PickListManager.getPickListData(vAttrib.getMetaAttribute().getPickListId())));
            listHeaderMap.add(headerMap);
            headerMap = new HashMap<String, Object>();
        }

        DataBroker broker = DataRepository.getBroker(this.systemName);
        Search search = new Search();
        search.setOrderClause("ORDER BY EFORM_TICKET_NUM DESC");
        search.addFrom(this.systemName);
        List<DataObject> listResult = broker.find(search);
        List<Map<String, Object>> listBodyMap = new ArrayList<>();
        Map<String, Object> bodyMap = new HashMap<String, Object>();
        for (DataObject data : listResult) {
            bodyMap.put("EFORM_TICKET_NUM", data.getAttribList().get("EFORM_TICKET_NUM").toString());
            bodyMap.put("CUST_RELATION_NUM", data.getAttribList().get("CUST_RELATION_NUM").toString());
            bodyMap.put("PRODUCT_CODE", data.getAttribList().get("PRODUCT_CODE").toString());
            bodyMap.put("PRODUCT_ENTITY_ID", data.getAttribList().get("PRODUCT_ENTITY_ID").toString());
            bodyMap.put("EFORM_TYPE", data.getAttribList().get("EFORM_TYPE").toString());
            bodyMap.put("PRODUCT_NUMBER", data.getAttribList().get("PRODUCT_NUMBER").toString());
            bodyMap.put("CURRENT_STATE", data.getAttribList().get("CURRENT_STATE").toString());
            bodyMap.put("CUST_CALLBACK_PHONE", data.getAttribList().get("CUST_CALLBACK_PHONE").toString());
            bodyMap.put("STATE_ENTRY_TIME", data.getAttribList().get("STATE_ENTRY_TIME").toString());
            listBodyMap.add(bodyMap);
            bodyMap = new HashMap<String, Object>();
        }

        Map<String, List<Map<String, Object>>> jsonMap = new HashMap<String, List<Map<String, Object>>>();
        jsonMap.put("attribute", listHeaderMap);
        jsonMap.put("data", listBodyMap);
        //obj.put("",jsonMap);

      *//*  List<DataObject> removeUnFilterObjects = new ArrayList<DataObject>();
        for (DataObject xyz : listResult) {
            *//**//*
         * xyz.getAttribList().get("EFORM_TYPE").getValue().equals("00040821000221222")
         *//**//*
            for (Field field : xyz.getClass().getDeclaredFields()) {
                // field.setAccessible(true); // if you want to modify private fields
                if (validColumns.isEmpty()) {
                    System.out.println(field.getName());
                    Set<String> keySet = xyz.getAttribList().keySet();
                    validColumns.addAll(keySet);
                }

            }
        }

        createDynamicColumns();
        if (!removeUnFilterObjects.isEmpty()) {
            listResult = removeUnFilterObjects;
        }
        List<Map<String, String>> rows = new ArrayList<Map<String, String>>();

        for (DataObject d : listResult) {
            Map<String, String> row = new HashMap<String, String>();
            for (ColumnModel c : columns) {

                row.put(c.getHeader(), String.valueOf(d.getValue(c.getProperty())));
            }
            rows.add(row);
        }*//*

        // return new GenericResponse(columns, rows);
        return jsonMap;
    }

    private void createDynamicColumns() {

        if (columnTemplate.isEmpty()) {
            for (String colmnName : validColumns) {
                columnTemplate.add(colmnName);
            }
        }
        // String[] columnKeys = columnTemplate.split(" ");
        columns = new ArrayList<ColumnModel>();

        for (String columnKey : columnTemplate) {
            String key = columnKey.trim();

            if (validColumns.contains(key)) {
                columns.add(new ColumnModel(columnKey.toUpperCase(), columnKey));
            }
        }
    }

    static public class ColumnModel implements Serializable {

        private String header;
        private String property;

        public ColumnModel(String header, String property) {
            this.header = header;
            this.property = property;
        }

        public String getHeader() {
            return header;
        }

        public String getProperty() {
            return property;
        }
    }

    public static class GenericResponse {
        List<ColumnModel> headers;
        List<Map<String, String>> rows;

        public GenericResponse(List<ColumnModel> headers, List<Map<String, String>> rows) {
            super();
            this.headers = headers;
            this.rows = rows;
        }

        public List<ColumnModel> getHeaders() {
            return headers;
        }

        public void setHeaders(List<ColumnModel> headers) {
            this.headers = headers;
        }

        public List<Map<String, String>> getRows() {
            return rows;
        }

        public void setRows(List<Map<String, String>> rows) {
            this.rows = rows;
        }

    }*/
}
