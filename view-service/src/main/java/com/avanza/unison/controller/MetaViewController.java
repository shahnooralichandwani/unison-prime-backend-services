package com.avanza.unison.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avanza.unison.entity.MetaView;
import com.avanza.unison.service.MetaViewService;

@RestController
@RequestMapping("/metaView")
public class MetaViewController {

    @Autowired
    private MetaViewService metaViewService;

    @GetMapping("/{metaViewId}")
    public MetaView getMetaView(@PathVariable String metaViewId) {
        return metaViewService.findById(metaViewId);
    }
}
