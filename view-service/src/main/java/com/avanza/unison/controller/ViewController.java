package com.avanza.unison.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avanza.unison.dto.View.View;
import com.avanza.unison.service.ViewTransformerService;

@RestController
@RequestMapping("/view")
@CrossOrigin
public class ViewController {

    @Autowired
    ViewTransformerService viewRenderService;

    @GetMapping(produces = {"application/json"}, path = "/{metaViewId}")
    public View getUIView(@PathVariable String metaViewId) {
		/*ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		try {
			return mapper.writeValueAsString(uiViewRenderService.renderByMetaViewId(metaViewId));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";*/
        return viewRenderService.transformByMetaViewId(metaViewId);
    }
}
