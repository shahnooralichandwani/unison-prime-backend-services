package com.avanza.unison.service;

import com.avanza.unison.entity.MetaView;

public interface MetaViewService {
    public MetaView findById(String id);
}
