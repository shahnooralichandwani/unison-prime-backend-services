package com.avanza.unison.service.impl;

import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.avanza.unison.service.BaseURLService;

@Service
public class BaseURLServiceImpl implements BaseURLService {

    private @Autowired
    HttpServletRequest request;

    @Override
    public String getBaseUrl() throws UnknownHostException {
        String scheme = request.getScheme();
        String ip;
        ip = request.getServerName();
        int port = request.getServerPort();
        return scheme + "://" + ip + ((request.getScheme().equals("http") && port != 80) || (request.getScheme().equals("https") && port != 443) ? ":" + port : "") + request.getServletContext().getContextPath();

    }

    @Override
    public String getScheme() {
        return request.getScheme();
    }

    @Override
    public String getContextPath() {

        return request.getServletContext().getContextPath();
    }

    @Override
    public String getPort() {
        return String.valueOf(request.getServerPort());
    }


}
