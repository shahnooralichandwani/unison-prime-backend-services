package com.avanza.unison.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avanza.unison.entity.MetaView;
import com.avanza.unison.repository.MetaViewRepository;
import com.avanza.unison.service.MetaViewService;

import java.util.Optional;

@Service
public class MetaViewServiceImpl implements MetaViewService {

    @Autowired
    private MetaViewRepository metaViewRep;

    @Transactional
    @Override
    public MetaView findById(String id) {
        // TODO Auto-generated method stub

        Optional<MetaView> opt= metaViewRep.findById(id);
       if(opt.isPresent()) return opt.get();

        return null;
    }

}
