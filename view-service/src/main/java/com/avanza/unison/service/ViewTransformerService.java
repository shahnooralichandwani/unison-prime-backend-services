package com.avanza.unison.service;

import com.avanza.unison.dto.View.View;

public interface ViewTransformerService {

    View transformByMetaViewId(String metaViewId);

}
