package com.avanza.unison.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.avanza.unison.component.transformer.EntityTransformer;
import com.avanza.unison.dto.View.View;
import com.avanza.unison.entity.MetaView;
import com.avanza.unison.entity.MetaViewMenu;
import com.avanza.unison.service.MetaViewService;
import com.avanza.unison.service.ViewTransformerService;

@Service
public class ViewTransformerServiceImpl implements ViewTransformerService {

    @Autowired
    MetaViewService metaViewService;
    @Autowired
    private ApplicationContext context;

    @Transactional
    @Override
    public View transformByMetaViewId(String metaViewId) {

        MetaView metaView = metaViewService.findById(metaViewId);

        return transform(metaView);
    }

    public View transform(MetaView metaView) {
        View view = null;
        try {
            String viewType = Character.toLowerCase(metaView.getViewType().charAt(0)) + metaView.getViewType().substring(1);
            EntityTransformer entityTransformer = (EntityTransformer) context.getBean(viewType + EntityTransformer.TRANSFORMER);
            view = entityTransformer.transform(metaView);

            for (MetaView childView : metaView.getChildViews()) {
                entityTransformer.associateViews(view, transform(childView));
            }

            for (MetaViewMenu metaViewMenu : metaView.getMetaViewMenu()) {
                entityTransformer.associateViews(view, transform(metaViewMenu));
            }
        } catch (BeansException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return view;
    }

    public View transform(MetaViewMenu metaViewMenu) {
        String menuType = Character.toLowerCase(metaViewMenu.getMenuType().charAt(0)) + metaViewMenu.getMenuType().substring(1);
        EntityTransformer entityTransformer = (EntityTransformer) context.getBean(menuType + EntityTransformer.TRANSFORMER);
        View view = entityTransformer.transform(metaViewMenu);

        for (MetaViewMenu childMenu : metaViewMenu.getChildMenus()) {
            entityTransformer.associateViews(view, transform(childMenu));
        }

        return view;
    }
}
