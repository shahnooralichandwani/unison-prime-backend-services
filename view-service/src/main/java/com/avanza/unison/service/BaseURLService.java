package com.avanza.unison.service;

import java.net.UnknownHostException;

public interface BaseURLService {
    public String getBaseUrl() throws UnknownHostException;

    public String getScheme();

    public String getContextPath();

    public String getPort();
}
