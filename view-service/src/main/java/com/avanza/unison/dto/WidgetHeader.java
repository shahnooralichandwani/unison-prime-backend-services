/**
 *
 */
package com.avanza.unison.dto;

import java.util.List;

import com.avanza.unison.dto.View.Section;

/**
 * @author shahnoor.ali
 *
 */
public class WidgetHeader implements Section {

    private String displayName;
    private List<Action> actions;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

}
