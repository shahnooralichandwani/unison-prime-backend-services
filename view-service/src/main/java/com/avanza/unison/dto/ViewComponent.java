package com.avanza.unison.dto;

import java.util.Map;

import com.avanza.unison.dto.View.View;

public class ViewComponent implements View {
    String type;
    Map<String, String> data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

}
