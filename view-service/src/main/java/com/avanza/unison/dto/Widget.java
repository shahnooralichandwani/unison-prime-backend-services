package com.avanza.unison.dto;

import java.util.Map;

import com.avanza.unison.dto.View.View;

public class Widget implements View {
    private Map<String, String> properties;
    private WidgetHeader header;
    private WidgetBody body;
    private WidgetFooter footer;

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public WidgetHeader getHeader() {
        return header;
    }

    public void setHeader(WidgetHeader header) {
        this.header = header;
    }

    public WidgetBody getBody() {
        return body;
    }

    public void setBody(WidgetBody body) {
        this.body = body;
    }

    public WidgetFooter getFooter() {
        return footer;
    }

    public void setFooter(WidgetFooter footer) {
        this.footer = footer;
    }
}
