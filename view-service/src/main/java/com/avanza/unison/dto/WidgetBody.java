package com.avanza.unison.dto;

import java.util.List;

import com.avanza.unison.dto.View.Section;

public class WidgetBody implements Section {
    List<ViewComponent> components;

    public List<ViewComponent> getComponents() {
        return components;
    }

    public void setComponents(List<ViewComponent> components) {
        this.components = components;
    }
}
