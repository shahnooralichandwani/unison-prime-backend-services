package com.avanza.unison.dto;

import java.util.List;

import com.avanza.unison.dto.View.View;

public class MegaMenu implements View {
    List<MegaMenuItem> menuItems;

    public List<MegaMenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MegaMenuItem> menuItems) {
        this.menuItems = menuItems;
    }

}
