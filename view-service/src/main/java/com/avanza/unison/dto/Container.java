package com.avanza.unison.dto;

import java.util.List;

import com.avanza.unison.dto.View.View;

public class Container implements View {
    List<Widget> widgets;

    public List<Widget> getWidgets() {
        return widgets;
    }

    public void setWidgets(List<Widget> widgets) {
        this.widgets = widgets;
    }
}
