package com.avanza.unison.dto;

import java.util.List;

import com.avanza.unison.dto.View.ViewMenu;

public class MegaMenuItem implements ViewMenu {
    private String displayName;
    private String metaViewId;
    private String url;
    private List<MegaMenuItem> subMenuItems;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getMetaViewId() {
        return metaViewId;
    }

    public void setMetaViewId(String metaViewId) {
        this.metaViewId = metaViewId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<MegaMenuItem> getSubMenuItems() {
        return subMenuItems;
    }

    public void setSubMenuItems(List<MegaMenuItem> subMenuItems) {
        this.subMenuItems = subMenuItems;
    }
}
