package com.avanza.unison.dto;

import java.util.List;

import com.avanza.unison.dto.View.ViewMenu;

public class Action implements ViewMenu {
    private String type;
    private String displayName;
    private int displayOrder;
    private String action;

    private List<Action> subActions;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public List<Action> getSubActions() {
        return subActions;
    }

    public void setSubActions(List<Action> subActions) {
        this.subActions = subActions;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
