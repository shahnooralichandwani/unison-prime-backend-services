package com.avanza.unison.component.transformer.view;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.avanza.unison.component.transformer.EntityTransformer;
import com.avanza.unison.dto.Widget;
import com.avanza.unison.dto.WidgetBody;
import com.avanza.unison.dto.WidgetFooter;
import com.avanza.unison.dto.WidgetHeader;
import com.avanza.unison.dto.View.View;
import com.avanza.unison.entity.DBEntity;
import com.avanza.unison.entity.MetaView;
import com.avanza.unison.entity.MetaViewParam;

@Component
public class WidgetTransformer implements EntityTransformer {

    @Override
    public View transform(DBEntity entity) {
        Widget widget = new Widget();
        List<MetaViewParam> metaViewParams = ((MetaView) entity).getMetaViewParams();
        Map<String, String> properties = new LinkedHashMap<>();
        for (MetaViewParam metaViewParam : metaViewParams) {
            properties.put(metaViewParam.getParameterName(), metaViewParam.getParameterValue());
        }
        widget.setProperties(properties);
        return widget;
    }

    @Override
    public void associateViews(View parentView, View childView) {
        Widget widget = (Widget) parentView;

        if (childView instanceof WidgetHeader) {
            widget.setHeader((WidgetHeader) childView);
        }

        if (childView instanceof WidgetBody) {
            widget.setBody((WidgetBody) childView);
        }

        if (childView instanceof WidgetFooter) {
            widget.setFooter((WidgetFooter) childView);
        }
    }

}
