package com.avanza.unison.component.transformer.menu;

import java.util.ArrayList;

import com.avanza.unison.component.transformer.EntityTransformer;
import com.avanza.unison.dto.Action;
import com.avanza.unison.dto.View.View;
import com.avanza.unison.entity.DBEntity;
import com.avanza.unison.entity.MetaViewMenu;

public abstract class AbstractActionTransformer implements EntityTransformer {

    @Override
    public View transform(DBEntity entity) {
        MetaViewMenu metaViewMenu = (MetaViewMenu) entity;
        Action action = new Action();
        action.setDisplayName(metaViewMenu.getDisplayNamePrm());
        action.setType(metaViewMenu.getMenuType());
        action.setDisplayOrder(metaViewMenu.getDisplayOrder());
        action.setAction(metaViewMenu.getActionListener());
        return action;
    }

    @Override
    public void associateViews(View parentView, View childView) {
        Action action = (Action) parentView;

        if (action.getSubActions() == null) {
            action.setSubActions(new ArrayList<Action>());
        }

        action.getSubActions().add((Action) childView);
    }

}
