package com.avanza.unison.component.transformer.view;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.avanza.unison.component.transformer.EntityTransformer;
import com.avanza.unison.dto.ViewComponent;
import com.avanza.unison.dto.WidgetBody;
import com.avanza.unison.dto.View.View;
import com.avanza.unison.entity.DBEntity;

@Component
public class WidgetBodyTransformer implements EntityTransformer {

    @Override
    public View transform(DBEntity entity) {
        WidgetBody widgetBody = new WidgetBody();
        return widgetBody;
    }

    @Override
    public void associateViews(View parentView, View childView) {
        WidgetBody widgetBody = (WidgetBody) parentView;
        if (childView instanceof ViewComponent) {
            if (widgetBody.getComponents() == null) {
                widgetBody.setComponents(new ArrayList<ViewComponent>());
            }
            widgetBody.getComponents().add((ViewComponent) childView);
        }
    }

}
