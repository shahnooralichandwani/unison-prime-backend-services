package com.avanza.unison.component.transformer.view;

import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.avanza.unison.component.transformer.EntityTransformer;
import com.avanza.unison.dto.ViewComponent;
import com.avanza.unison.dto.View.View;
import com.avanza.unison.entity.DBEntity;
import com.avanza.unison.entity.MetaView;
import com.avanza.unison.entity.MetaViewParam;
import com.avanza.unison.service.BaseURLService;

@Component
public class ListTransformer implements EntityTransformer {

    @Autowired
    BaseURLService baseURLService;

    @Override
    public View transform(DBEntity entity) {
        ViewComponent component = new ViewComponent();
        MetaView metaView = (MetaView) entity;
        Map<String, String> dataMap = new LinkedHashMap<String, String>();
        dataMap.put("metaEntityId", metaView.getMetaEntityId());
        dataMap.put("metaViewId", metaView.getMetaViewId());
        for (MetaViewParam viewParam : metaView.getMetaViewParams()) {
            if (viewParam.getParameterName().equalsIgnoreCase("url") && !viewParam.getParameterValue().startsWith("http")) {
                try {
                    dataMap.put(viewParam.getParameterName(), baseURLService.getBaseUrl() + viewParam.getParameterValue());
                } catch (UnknownHostException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                dataMap.put(viewParam.getParameterName(), viewParam.getParameterValue());
            }
        }
        component.setType(metaView.getViewType());
        component.setData(dataMap);
        return component;
    }

    @Override
    public void associateViews(View parentView, View childView) {
        // TODO Auto-generated method stub

    }

}
