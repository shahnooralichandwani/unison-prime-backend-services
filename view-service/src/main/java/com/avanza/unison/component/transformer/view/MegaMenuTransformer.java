package com.avanza.unison.component.transformer.view;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.avanza.unison.component.transformer.EntityTransformer;
import com.avanza.unison.dto.MegaMenu;
import com.avanza.unison.dto.MegaMenuItem;
import com.avanza.unison.dto.View.View;
import com.avanza.unison.entity.DBEntity;

@Component
public class MegaMenuTransformer implements EntityTransformer {

    @Override
    public View transform(DBEntity entity) {
        MegaMenu megaMenu = new MegaMenu();
        return megaMenu;
    }

    @Override
    public void associateViews(View parentView, View childView) {
        MegaMenu megaMenu = (MegaMenu) parentView;
        if (megaMenu.getMenuItems() == null) {
            megaMenu.setMenuItems(new ArrayList<MegaMenuItem>());
        }
        megaMenu.getMenuItems().add((MegaMenuItem) childView);
    }

}
