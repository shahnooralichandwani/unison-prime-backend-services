package com.avanza.unison.component.transformer.menu;

import java.net.UnknownHostException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.avanza.unison.component.transformer.EntityTransformer;
import com.avanza.unison.dto.MegaMenuItem;
import com.avanza.unison.dto.View.View;
import com.avanza.unison.entity.DBEntity;
import com.avanza.unison.entity.MetaViewMenu;
import com.avanza.unison.service.BaseURLService;

@Component
public class MegaMenuItemTransformer implements EntityTransformer {

    @Autowired
    BaseURLService baseURLService;

    @Override
    public View transform(DBEntity entity) {
        MetaViewMenu viewMenu = (MetaViewMenu) entity;
        MegaMenuItem menuItem = new MegaMenuItem();
        menuItem.setDisplayName(viewMenu.getDisplayNamePrm());
        menuItem.setMetaViewId(viewMenu.getDisplayAttrib());
        String menuURL = viewMenu.getMenuUrl();
        if (menuURL != null && !menuURL.startsWith("http")) {
            try {
                menuURL = baseURLService.getBaseUrl() + menuURL;
            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        menuItem.setUrl(menuURL);
        return menuItem;
    }

    @Override
    public void associateViews(View parentView, View childView) {
        MegaMenuItem megaMenuItem = (MegaMenuItem) parentView;
        if (megaMenuItem.getSubMenuItems() == null) {
            megaMenuItem.setSubMenuItems(new ArrayList<MegaMenuItem>());
        }
        megaMenuItem.getSubMenuItems().add((MegaMenuItem) childView);
    }

}
