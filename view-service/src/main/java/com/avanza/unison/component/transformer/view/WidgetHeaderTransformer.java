package com.avanza.unison.component.transformer.view;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.avanza.unison.component.transformer.EntityTransformer;
import com.avanza.unison.dto.Action;
import com.avanza.unison.dto.WidgetHeader;
import com.avanza.unison.dto.View.View;
import com.avanza.unison.entity.DBEntity;
import com.avanza.unison.entity.MetaView;

@Component
public class WidgetHeaderTransformer implements EntityTransformer {

    @Override
    public View transform(DBEntity entity) {
        WidgetHeader header = new WidgetHeader();
        MetaView metaView = (MetaView) entity;
        header.setDisplayName(metaView.getViewNamePrm());
        return header;
    }

    @Override
    public void associateViews(View parentView, View childView) {
        WidgetHeader header = (WidgetHeader) parentView;

        if (header.getActions() == null) {
            header.setActions(new ArrayList<Action>());
        }

        header.getActions().add((Action) childView);
    }

}
