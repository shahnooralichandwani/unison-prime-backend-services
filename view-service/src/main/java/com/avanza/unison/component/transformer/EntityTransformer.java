package com.avanza.unison.component.transformer;

import com.avanza.unison.dto.View.View;
import com.avanza.unison.entity.DBEntity;

public interface EntityTransformer {

    public static final String TRANSFORMER = "Transformer";

    View transform(DBEntity entity);

    void associateViews(View parentView, View childView);
}
