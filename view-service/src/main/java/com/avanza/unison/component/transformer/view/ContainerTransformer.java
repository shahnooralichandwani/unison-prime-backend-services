package com.avanza.unison.component.transformer.view;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.avanza.unison.component.transformer.EntityTransformer;
import com.avanza.unison.dto.Container;
import com.avanza.unison.dto.Widget;
import com.avanza.unison.dto.View.View;
import com.avanza.unison.entity.DBEntity;

@Component
public class ContainerTransformer implements EntityTransformer {

    @Override
    public View transform(DBEntity entity) {
        Container container = new Container();
        // TODO Auto-generated method stub
        return container;
    }

    @Override
    public void associateViews(View parentView, View childView) {
        Container container = (Container) parentView;
        if (container.getWidgets() == null) {
            container.setWidgets(new ArrayList<Widget>());
        }
        container.getWidgets().add((Widget) childView);
    }

}
