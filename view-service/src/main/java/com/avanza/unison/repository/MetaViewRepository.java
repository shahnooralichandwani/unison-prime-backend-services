package com.avanza.unison.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.avanza.unison.entity.MetaView;

public interface MetaViewRepository extends JpaRepository<MetaView, String> {
    List<MetaView> findByViewType(@Param("viewType") String viewType);
}
