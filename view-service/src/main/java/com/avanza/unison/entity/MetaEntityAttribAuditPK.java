package com.avanza.unison.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class MetaEntityAttribAuditPK implements Serializable {
    private String attributeId;
    private Integer rev;

    @Column(name = "ATTRIBUTE_ID")
    @Id
    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    @Column(name = "REV")
    @Id
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaEntityAttribAuditPK that = (MetaEntityAttribAuditPK) o;
        return Objects.equals(attributeId, that.attributeId) &&
                Objects.equals(rev, that.rev);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attributeId, rev);
    }
}
