package com.avanza.unison.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class MetaViewAttribParamPK implements Serializable {
    private String parameterName;
    private String viewAttribId;

    @Column(name = "PARAMETER_NAME")
    @Id
    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    @Column(name = "VIEW_ATTRIB_ID")
    @Id
    public String getViewAttribId() {
        return viewAttribId;
    }

    public void setViewAttribId(String viewAttribId) {
        this.viewAttribId = viewAttribId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewAttribParamPK that = (MetaViewAttribParamPK) o;
        return Objects.equals(parameterName, that.parameterName) &&
                Objects.equals(viewAttribId, that.viewAttribId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parameterName, viewAttribId);
    }
}
