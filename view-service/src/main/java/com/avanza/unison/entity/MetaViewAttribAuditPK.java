package com.avanza.unison.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class MetaViewAttribAuditPK implements Serializable {
    private String viewAttribId;
    private Integer rev;

    @Column(name = "VIEW_ATTRIB_ID")
    @Id
    public String getViewAttribId() {
        return viewAttribId;
    }

    public void setViewAttribId(String viewAttribId) {
        this.viewAttribId = viewAttribId;
    }

    @Column(name = "REV")
    @Id
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewAttribAuditPK that = (MetaViewAttribAuditPK) o;
        return Objects.equals(viewAttribId, that.viewAttribId) &&
                Objects.equals(rev, that.rev);
    }

    @Override
    public int hashCode() {
        return Objects.hash(viewAttribId, rev);
    }
}
