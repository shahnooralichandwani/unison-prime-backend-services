package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_ENTITY_CATEGORY_AUDIT")
@IdClass(MetaEntityCategoryAuditPK.class)
public class MetaEntityCategoryAudit {
    private String entCategoryId;
    private Integer rev;
    private Byte revtype;
    private String metaEntId;
    private String treeNodeId;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private String actionClass;

    @Id
    @Column(name = "ENT_CATEGORY_ID")
    public String getEntCategoryId() {
        return entCategoryId;
    }

    public void setEntCategoryId(String entCategoryId) {
        this.entCategoryId = entCategoryId;
    }

    @Id
    @Column(name = "REV")
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Basic
    @Column(name = "REVTYPE")
    public Byte getRevtype() {
        return revtype;
    }

    public void setRevtype(Byte revtype) {
        this.revtype = revtype;
    }

    @Basic
    @Column(name = "META_ENT_ID")
    public String getMetaEntId() {
        return metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

    @Basic
    @Column(name = "TREE_NODE_ID")
    public String getTreeNodeId() {
        return treeNodeId;
    }

    public void setTreeNodeId(String treeNodeId) {
        this.treeNodeId = treeNodeId;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "ACTION_CLASS")
    public String getActionClass() {
        return actionClass;
    }

    public void setActionClass(String actionClass) {
        this.actionClass = actionClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaEntityCategoryAudit that = (MetaEntityCategoryAudit) o;
        return Objects.equals(entCategoryId, that.entCategoryId) &&
                Objects.equals(rev, that.rev) &&
                Objects.equals(revtype, that.revtype) &&
                Objects.equals(metaEntId, that.metaEntId) &&
                Objects.equals(treeNodeId, that.treeNodeId) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy) &&
                Objects.equals(actionClass, that.actionClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entCategoryId, rev, revtype, metaEntId, treeNodeId, createdOn, createdBy, updatedOn, updatedBy, actionClass);
    }
}
