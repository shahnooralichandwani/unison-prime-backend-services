package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_ENTITY_AUDIT")
@IdClass(MetaEntityAuditPK.class)
public class MetaEntityAudit {
    private String metaEntId;
    private Integer rev;
    private Byte revtype;
    private String systemName;
    private String parentEntity;
    private String entityName;
    private String tableName;
    private Boolean isAbstract;
    private Boolean isSystem;
    private Boolean templateEnabled;
    private String actionLogTable;
    private String activityLogTable;
    private String majorType;
    private String description;
    private String metaEntNamePrm;
    private String metaEntNameSec;
    private String discriminatorColumn;
    private String discriminatorValue;
    private Integer cacheTime;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private String rootNodeId;
    private Boolean isVisibleToChannels;

    @Id
    @Column(name = "META_ENT_ID")
    public String getMetaEntId() {
        return metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

    @Id
    @Column(name = "REV")
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Basic
    @Column(name = "REVTYPE")
    public Byte getRevtype() {
        return revtype;
    }

    public void setRevtype(Byte revtype) {
        this.revtype = revtype;
    }

    @Basic
    @Column(name = "SYSTEM_NAME")
    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Basic
    @Column(name = "PARENT_ENTITY")
    public String getParentEntity() {
        return parentEntity;
    }

    public void setParentEntity(String parentEntity) {
        this.parentEntity = parentEntity;
    }

    @Basic
    @Column(name = "ENTITY_NAME")
    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    @Basic
    @Column(name = "TABLE_NAME")
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Basic
    @Column(name = "IS_ABSTRACT")
    public Boolean getAbstract() {
        return isAbstract;
    }

    public void setAbstract(Boolean anAbstract) {
        isAbstract = anAbstract;
    }

    @Basic
    @Column(name = "IS_SYSTEM")
    public Boolean getSystem() {
        return isSystem;
    }

    public void setSystem(Boolean system) {
        isSystem = system;
    }

    @Basic
    @Column(name = "TEMPLATE_ENABLED")
    public Boolean getTemplateEnabled() {
        return templateEnabled;
    }

    public void setTemplateEnabled(Boolean templateEnabled) {
        this.templateEnabled = templateEnabled;
    }

    @Basic
    @Column(name = "ACTION_LOG_TABLE")
    public String getActionLogTable() {
        return actionLogTable;
    }

    public void setActionLogTable(String actionLogTable) {
        this.actionLogTable = actionLogTable;
    }

    @Basic
    @Column(name = "ACTIVITY_LOG_TABLE")
    public String getActivityLogTable() {
        return activityLogTable;
    }

    public void setActivityLogTable(String activityLogTable) {
        this.activityLogTable = activityLogTable;
    }

    @Basic
    @Column(name = "MAJOR_TYPE")
    public String getMajorType() {
        return majorType;
    }

    public void setMajorType(String majorType) {
        this.majorType = majorType;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "META_ENT_NAME_PRM")
    public String getMetaEntNamePrm() {
        return metaEntNamePrm;
    }

    public void setMetaEntNamePrm(String metaEntNamePrm) {
        this.metaEntNamePrm = metaEntNamePrm;
    }

    @Basic
    @Column(name = "META_ENT_NAME_SEC")
    public String getMetaEntNameSec() {
        return metaEntNameSec;
    }

    public void setMetaEntNameSec(String metaEntNameSec) {
        this.metaEntNameSec = metaEntNameSec;
    }

    @Basic
    @Column(name = "DISCRIMINATOR_COLUMN")
    public String getDiscriminatorColumn() {
        return discriminatorColumn;
    }

    public void setDiscriminatorColumn(String discriminatorColumn) {
        this.discriminatorColumn = discriminatorColumn;
    }

    @Basic
    @Column(name = "DISCRIMINATOR_VALUE")
    public String getDiscriminatorValue() {
        return discriminatorValue;
    }

    public void setDiscriminatorValue(String discriminatorValue) {
        this.discriminatorValue = discriminatorValue;
    }

    @Basic
    @Column(name = "CACHE_TIME")
    public Integer getCacheTime() {
        return cacheTime;
    }

    public void setCacheTime(Integer cacheTime) {
        this.cacheTime = cacheTime;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "ROOT_NODE_ID")
    public String getRootNodeId() {
        return rootNodeId;
    }

    public void setRootNodeId(String rootNodeId) {
        this.rootNodeId = rootNodeId;
    }

    @Basic
    @Column(name = "IS_VISIBLE_TO_CHANNELS")
    public Boolean getVisibleToChannels() {
        return isVisibleToChannels;
    }

    public void setVisibleToChannels(Boolean visibleToChannels) {
        isVisibleToChannels = visibleToChannels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaEntityAudit that = (MetaEntityAudit) o;
        return Objects.equals(metaEntId, that.metaEntId) &&
                Objects.equals(rev, that.rev) &&
                Objects.equals(revtype, that.revtype) &&
                Objects.equals(systemName, that.systemName) &&
                Objects.equals(parentEntity, that.parentEntity) &&
                Objects.equals(entityName, that.entityName) &&
                Objects.equals(tableName, that.tableName) &&
                Objects.equals(isAbstract, that.isAbstract) &&
                Objects.equals(isSystem, that.isSystem) &&
                Objects.equals(templateEnabled, that.templateEnabled) &&
                Objects.equals(actionLogTable, that.actionLogTable) &&
                Objects.equals(activityLogTable, that.activityLogTable) &&
                Objects.equals(majorType, that.majorType) &&
                Objects.equals(description, that.description) &&
                Objects.equals(metaEntNamePrm, that.metaEntNamePrm) &&
                Objects.equals(metaEntNameSec, that.metaEntNameSec) &&
                Objects.equals(discriminatorColumn, that.discriminatorColumn) &&
                Objects.equals(discriminatorValue, that.discriminatorValue) &&
                Objects.equals(cacheTime, that.cacheTime) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy) &&
                Objects.equals(rootNodeId, that.rootNodeId) &&
                Objects.equals(isVisibleToChannels, that.isVisibleToChannels);
    }

    @Override
    public int hashCode() {
        return Objects.hash(metaEntId, rev, revtype, systemName, parentEntity, entityName, tableName, isAbstract, isSystem, templateEnabled, actionLogTable, activityLogTable, majorType, description, metaEntNamePrm, metaEntNameSec, discriminatorColumn, discriminatorValue, cacheTime, createdOn, createdBy, updatedOn, updatedBy, rootNodeId, isVisibleToChannels);
    }
}
