package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_VIEW_ATTRIB_AUDIT")
@IdClass(MetaViewAttribAuditPK.class)
public class MetaViewAttribAudit {
    private String viewAttribId;
    private Integer rev;
    private Byte revtype;
    private String metaViewId;
    private String attributeId;
    private String metaEntId;
    private String systemName;
    private Boolean isReadOnly;
    private Integer displayOrder;
    private String styleClass;
    private Boolean isAggregated;
    private String componentType;
    private String displayNamePrm;
    private String displayNameSec;
    private String formatExpression;
    private String formatExpressionSec;
    private Boolean isForeignValDisplay;
    private String foreignDisplayAttribId;
    private String linkParams;
    private String constraintExp;
    private Boolean isPostback;
    private String postbackExp;
    private String linkUrl;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private Boolean renderAsPopup;
    private String selectExp;
    private String clientScriptExp;
    private String factoryClass;
    private String editExpression;
    private String events;
    private String eventActionClasses;
    private String eventRerenderComponents;

    @Id
    @Column(name = "VIEW_ATTRIB_ID")
    public String getViewAttribId() {
        return viewAttribId;
    }

    public void setViewAttribId(String viewAttribId) {
        this.viewAttribId = viewAttribId;
    }

    @Id
    @Column(name = "REV")
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Basic
    @Column(name = "REVTYPE")
    public Byte getRevtype() {
        return revtype;
    }

    public void setRevtype(Byte revtype) {
        this.revtype = revtype;
    }

    @Basic
    @Column(name = "META_VIEW_ID")
    public String getMetaViewId() {
        return metaViewId;
    }

    public void setMetaViewId(String metaViewId) {
        this.metaViewId = metaViewId;
    }

    @Basic
    @Column(name = "ATTRIBUTE_ID")
    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    @Basic
    @Column(name = "META_ENT_ID")
    public String getMetaEntId() {
        return metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

    @Basic
    @Column(name = "SYSTEM_NAME")
    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Basic
    @Column(name = "IS_READ_ONLY")
    public Boolean getReadOnly() {
        return isReadOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        isReadOnly = readOnly;
    }

    @Basic
    @Column(name = "DISPLAY_ORDER")
    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Basic
    @Column(name = "STYLE_CLASS")
    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    @Basic
    @Column(name = "IS_AGGREGATED")
    public Boolean getAggregated() {
        return isAggregated;
    }

    public void setAggregated(Boolean aggregated) {
        isAggregated = aggregated;
    }

    @Basic
    @Column(name = "COMPONENT_TYPE")
    public String getComponentType() {
        return componentType;
    }

    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    @Basic
    @Column(name = "DISPLAY_NAME_PRM")
    public String getDisplayNamePrm() {
        return displayNamePrm;
    }

    public void setDisplayNamePrm(String displayNamePrm) {
        this.displayNamePrm = displayNamePrm;
    }

    @Basic
    @Column(name = "DISPLAY_NAME_SEC")
    public String getDisplayNameSec() {
        return displayNameSec;
    }

    public void setDisplayNameSec(String displayNameSec) {
        this.displayNameSec = displayNameSec;
    }

    @Basic
    @Column(name = "FORMAT_EXPRESSION")
    public String getFormatExpression() {
        return formatExpression;
    }

    public void setFormatExpression(String formatExpression) {
        this.formatExpression = formatExpression;
    }

    @Basic
    @Column(name = "FORMAT_EXPRESSION_SEC")
    public String getFormatExpressionSec() {
        return formatExpressionSec;
    }

    public void setFormatExpressionSec(String formatExpressionSec) {
        this.formatExpressionSec = formatExpressionSec;
    }

    @Basic
    @Column(name = "IS_FOREIGN_VAL_DISPLAY")
    public Boolean getForeignValDisplay() {
        return isForeignValDisplay;
    }

    public void setForeignValDisplay(Boolean foreignValDisplay) {
        isForeignValDisplay = foreignValDisplay;
    }

    @Basic
    @Column(name = "FOREIGN_DISPLAY_ATTRIB_ID")
    public String getForeignDisplayAttribId() {
        return foreignDisplayAttribId;
    }

    public void setForeignDisplayAttribId(String foreignDisplayAttribId) {
        this.foreignDisplayAttribId = foreignDisplayAttribId;
    }

    @Basic
    @Column(name = "LINK_PARAMS")
    public String getLinkParams() {
        return linkParams;
    }

    public void setLinkParams(String linkParams) {
        this.linkParams = linkParams;
    }

    @Basic
    @Column(name = "CONSTRAINT_EXP")
    public String getConstraintExp() {
        return constraintExp;
    }

    public void setConstraintExp(String constraintExp) {
        this.constraintExp = constraintExp;
    }

    @Basic
    @Column(name = "IS_POSTBACK")
    public Boolean getPostback() {
        return isPostback;
    }

    public void setPostback(Boolean postback) {
        isPostback = postback;
    }

    @Basic
    @Column(name = "POSTBACK_EXP")
    public String getPostbackExp() {
        return postbackExp;
    }

    public void setPostbackExp(String postbackExp) {
        this.postbackExp = postbackExp;
    }

    @Basic
    @Column(name = "LINK_URL")
    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "RENDER_AS_POPUP")
    public Boolean getRenderAsPopup() {
        return renderAsPopup;
    }

    public void setRenderAsPopup(Boolean renderAsPopup) {
        this.renderAsPopup = renderAsPopup;
    }

    @Basic
    @Column(name = "SELECT_EXP")
    public String getSelectExp() {
        return selectExp;
    }

    public void setSelectExp(String selectExp) {
        this.selectExp = selectExp;
    }

    @Basic
    @Column(name = "CLIENT_SCRIPT_EXP")
    public String getClientScriptExp() {
        return clientScriptExp;
    }

    public void setClientScriptExp(String clientScriptExp) {
        this.clientScriptExp = clientScriptExp;
    }

    @Basic
    @Column(name = "FACTORY_CLASS")
    public String getFactoryClass() {
        return factoryClass;
    }

    public void setFactoryClass(String factoryClass) {
        this.factoryClass = factoryClass;
    }

    @Basic
    @Column(name = "EDIT_EXPRESSION")
    public String getEditExpression() {
        return editExpression;
    }

    public void setEditExpression(String editExpression) {
        this.editExpression = editExpression;
    }

    @Basic
    @Column(name = "EVENTS")
    public String getEvents() {
        return events;
    }

    public void setEvents(String events) {
        this.events = events;
    }

    @Basic
    @Column(name = "EVENT_ACTION_CLASSES")
    public String getEventActionClasses() {
        return eventActionClasses;
    }

    public void setEventActionClasses(String eventActionClasses) {
        this.eventActionClasses = eventActionClasses;
    }

    @Basic
    @Column(name = "EVENT_RERENDER_COMPONENTS")
    public String getEventRerenderComponents() {
        return eventRerenderComponents;
    }

    public void setEventRerenderComponents(String eventRerenderComponents) {
        this.eventRerenderComponents = eventRerenderComponents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewAttribAudit that = (MetaViewAttribAudit) o;
        return Objects.equals(viewAttribId, that.viewAttribId) &&
                Objects.equals(rev, that.rev) &&
                Objects.equals(revtype, that.revtype) &&
                Objects.equals(metaViewId, that.metaViewId) &&
                Objects.equals(attributeId, that.attributeId) &&
                Objects.equals(metaEntId, that.metaEntId) &&
                Objects.equals(systemName, that.systemName) &&
                Objects.equals(isReadOnly, that.isReadOnly) &&
                Objects.equals(displayOrder, that.displayOrder) &&
                Objects.equals(styleClass, that.styleClass) &&
                Objects.equals(isAggregated, that.isAggregated) &&
                Objects.equals(componentType, that.componentType) &&
                Objects.equals(displayNamePrm, that.displayNamePrm) &&
                Objects.equals(displayNameSec, that.displayNameSec) &&
                Objects.equals(formatExpression, that.formatExpression) &&
                Objects.equals(formatExpressionSec, that.formatExpressionSec) &&
                Objects.equals(isForeignValDisplay, that.isForeignValDisplay) &&
                Objects.equals(foreignDisplayAttribId, that.foreignDisplayAttribId) &&
                Objects.equals(linkParams, that.linkParams) &&
                Objects.equals(constraintExp, that.constraintExp) &&
                Objects.equals(isPostback, that.isPostback) &&
                Objects.equals(postbackExp, that.postbackExp) &&
                Objects.equals(linkUrl, that.linkUrl) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy) &&
                Objects.equals(renderAsPopup, that.renderAsPopup) &&
                Objects.equals(selectExp, that.selectExp) &&
                Objects.equals(clientScriptExp, that.clientScriptExp) &&
                Objects.equals(factoryClass, that.factoryClass) &&
                Objects.equals(editExpression, that.editExpression) &&
                Objects.equals(events, that.events) &&
                Objects.equals(eventActionClasses, that.eventActionClasses) &&
                Objects.equals(eventRerenderComponents, that.eventRerenderComponents);
    }

    @Override
    public int hashCode() {
        return Objects.hash(viewAttribId, rev, revtype, metaViewId, attributeId, metaEntId, systemName, isReadOnly, displayOrder, styleClass, isAggregated, componentType, displayNamePrm, displayNameSec, formatExpression, formatExpressionSec, isForeignValDisplay, foreignDisplayAttribId, linkParams, constraintExp, isPostback, postbackExp, linkUrl, createdOn, createdBy, updatedOn, updatedBy, renderAsPopup, selectExp, clientScriptExp, factoryClass, editExpression, events, eventActionClasses, eventRerenderComponents);
    }
}
