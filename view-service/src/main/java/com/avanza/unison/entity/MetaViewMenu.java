package com.avanza.unison.entity;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "META_VIEW_MENU")
public class MetaViewMenu implements DBEntity {
    private String viewMenuId;
    private String systemName;
    private String styleClass;
    private String menuType;
    private String displayNamePrm;
    private String displayNameSec;
    private String displayAttrib;
    private String permissionExp;
    private String menuUrl;
    private String urlParams;
    private String search;
    private String actionListener;
    private Integer displayOrder;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private String optionFilterClause;
    private Boolean isDefaultOption;
    private String styleClassLabel;

    private MetaViewMenu parentMenu;


    private List<MetaViewMenu> childMenus;

    @Id
    @Column(name = "VIEW_MENU_ID")
    public String getViewMenuId() {
        return viewMenuId;
    }

    public void setViewMenuId(String viewMenuId) {
        this.viewMenuId = viewMenuId;
    }

    @Basic
    @Column(name = "SYSTEM_NAME")
    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Basic
    @Column(name = "STYLE_CLASS")
    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    @Basic
    @Column(name = "MENU_TYPE")
    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    @Basic
    @Column(name = "DISPLAY_NAME_PRM")
    public String getDisplayNamePrm() {
        return displayNamePrm;
    }

    public void setDisplayNamePrm(String displayNamePrm) {
        this.displayNamePrm = displayNamePrm;
    }

    @Basic
    @Column(name = "DISPLAY_NAME_SEC")
    public String getDisplayNameSec() {
        return displayNameSec;
    }

    public void setDisplayNameSec(String displayNameSec) {
        this.displayNameSec = displayNameSec;
    }

    @Basic
    @Column(name = "DISPLAY_ATTRIB")
    public String getDisplayAttrib() {
        return displayAttrib;
    }

    public void setDisplayAttrib(String displayAttrib) {
        this.displayAttrib = displayAttrib;
    }

    @Basic
    @Column(name = "PERMISSION_EXP")
    public String getPermissionExp() {
        return permissionExp;
    }

    public void setPermissionExp(String permissionExp) {
        this.permissionExp = permissionExp;
    }

    @Basic
    @Column(name = "MENU_URL")
    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    @Basic
    @Column(name = "URL_PARAMS")
    public String getUrlParams() {
        return urlParams;
    }

    public void setUrlParams(String urlParams) {
        this.urlParams = urlParams;
    }

    @Basic
    @Column(name = "SEARCH")
    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    @Basic
    @Column(name = "ACTION_LISTENER")
    public String getActionListener() {
        return actionListener;
    }

    public void setActionListener(String actionListener) {
        this.actionListener = actionListener;
    }

    @Basic
    @Column(name = "DISPLAY_ORDER")
    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "OPTION_FILTER_CLAUSE")
    public String getOptionFilterClause() {
        return optionFilterClause;
    }

    public void setOptionFilterClause(String optionFilterClause) {
        this.optionFilterClause = optionFilterClause;
    }

    @Basic
    @Column(name = "IS_DEFAULT_OPTION")
    public Boolean getDefaultOption() {
        return isDefaultOption;
    }

    public void setDefaultOption(Boolean defaultOption) {
        isDefaultOption = defaultOption;
    }

    @Basic
    @Column(name = "STYLE_CLASS_LABEL")
    public String getStyleClassLabel() {
        return styleClassLabel;
    }

    public void setStyleClassLabel(String styleClassLabel) {
        this.styleClassLabel = styleClassLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewMenu that = (MetaViewMenu) o;
        return Objects.equals(viewMenuId, that.viewMenuId) &&
                Objects.equals(systemName, that.systemName) &&
                Objects.equals(styleClass, that.styleClass) &&
                Objects.equals(menuType, that.menuType) &&
                Objects.equals(displayNamePrm, that.displayNamePrm) &&
                Objects.equals(displayNameSec, that.displayNameSec) &&
                Objects.equals(displayAttrib, that.displayAttrib) &&
                Objects.equals(permissionExp, that.permissionExp) &&
                Objects.equals(menuUrl, that.menuUrl) &&
                Objects.equals(urlParams, that.urlParams) &&
                Objects.equals(search, that.search) &&
                Objects.equals(actionListener, that.actionListener) &&
                Objects.equals(displayOrder, that.displayOrder) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy) &&
                Objects.equals(optionFilterClause, that.optionFilterClause) &&
                Objects.equals(isDefaultOption, that.isDefaultOption) &&
                Objects.equals(styleClassLabel, that.styleClassLabel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(viewMenuId, systemName, styleClass, menuType, displayNamePrm, displayNameSec, displayAttrib, permissionExp, menuUrl, urlParams, search, actionListener, displayOrder, createdOn, createdBy, updatedOn, updatedBy, optionFilterClause, isDefaultOption, styleClassLabel);
    }

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "PARENT_MENU")
    public MetaViewMenu getParentMenu() {
        return parentMenu;
    }

    public void setParentMenu(MetaViewMenu parentMenu) {
        this.parentMenu = parentMenu;
    }

    @OneToMany(mappedBy = "parentMenu", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("displayOrder")
    public List<MetaViewMenu> getChildMenus() {
        return childMenus;
    }

    public void setChildMenus(List<MetaViewMenu> childMenus) {
        this.childMenus = childMenus;
    }
}
