package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_VIEW_PARAM")
@IdClass(MetaViewParamPK.class)
public class MetaViewParam implements DBEntity {
    private String parameterName;
    private String metaViewId;
    private String parameterValue;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;

    @Id
    @Column(name = "PARAMETER_NAME")
    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    @Id
    @Column(name = "META_VIEW_ID")
    public String getMetaViewId() {
        return metaViewId;
    }

    public void setMetaViewId(String metaViewId) {
        this.metaViewId = metaViewId;
    }

    @Basic
    @Column(name = "PARAMETER_VALUE")
    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewParam that = (MetaViewParam) o;
        return Objects.equals(parameterName, that.parameterName) &&
                Objects.equals(metaViewId, that.metaViewId) &&
                Objects.equals(parameterValue, that.parameterValue) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parameterName, metaViewId, parameterValue, createdOn, createdBy, updatedOn, updatedBy);
    }
}
