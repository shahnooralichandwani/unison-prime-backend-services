package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_ENTITY_ATTRIB_AUDIT")
@IdClass(MetaEntityAttribAuditPK.class)
public class MetaEntityAttribAudit {
    private String attributeId;
    private Integer rev;
    private Byte revtype;
    private String systemName;
    private String metaEntId;
    private String attributeName;
    private String tableColumn;
    private String attributeType;
    private String description;
    private String pickList;
    private String counterName;
    private Boolean isMandatory;
    private Boolean isSearch;
    private Boolean isAudit;
    private Boolean isSummary;
    private Boolean isInternalPrimary;
    private Boolean isPrimary;
    private Boolean templateEnabled;
    private String attributeScope;
    private String constraintExp;
    private String constraintType;
    private String defaultExp;
    private String calculateExp;
    private String operatorType;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private String menuId;
    private Boolean isSerialized;
    private String serializationType;

    @Id
    @Column(name = "ATTRIBUTE_ID")
    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    @Id
    @Column(name = "REV")
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Basic
    @Column(name = "REVTYPE")
    public Byte getRevtype() {
        return revtype;
    }

    public void setRevtype(Byte revtype) {
        this.revtype = revtype;
    }

    @Basic
    @Column(name = "SYSTEM_NAME")
    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Basic
    @Column(name = "META_ENT_ID")
    public String getMetaEntId() {
        return metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

    @Basic
    @Column(name = "ATTRIBUTE_NAME")
    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    @Basic
    @Column(name = "TABLE_COLUMN")
    public String getTableColumn() {
        return tableColumn;
    }

    public void setTableColumn(String tableColumn) {
        this.tableColumn = tableColumn;
    }

    @Basic
    @Column(name = "ATTRIBUTE_TYPE")
    public String getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(String attributeType) {
        this.attributeType = attributeType;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "PICK_LIST")
    public String getPickList() {
        return pickList;
    }

    public void setPickList(String pickList) {
        this.pickList = pickList;
    }

    @Basic
    @Column(name = "COUNTER_NAME")
    public String getCounterName() {
        return counterName;
    }

    public void setCounterName(String counterName) {
        this.counterName = counterName;
    }

    @Basic
    @Column(name = "IS_MANDATORY")
    public Boolean getMandatory() {
        return isMandatory;
    }

    public void setMandatory(Boolean mandatory) {
        isMandatory = mandatory;
    }

    @Basic
    @Column(name = "IS_SEARCH")
    public Boolean getSearch() {
        return isSearch;
    }

    public void setSearch(Boolean search) {
        isSearch = search;
    }

    @Basic
    @Column(name = "IS_AUDIT")
    public Boolean getAudit() {
        return isAudit;
    }

    public void setAudit(Boolean audit) {
        isAudit = audit;
    }

    @Basic
    @Column(name = "IS_SUMMARY")
    public Boolean getSummary() {
        return isSummary;
    }

    public void setSummary(Boolean summary) {
        isSummary = summary;
    }

    @Basic
    @Column(name = "IS_INTERNAL_PRIMARY")
    public Boolean getInternalPrimary() {
        return isInternalPrimary;
    }

    public void setInternalPrimary(Boolean internalPrimary) {
        isInternalPrimary = internalPrimary;
    }

    @Basic
    @Column(name = "IS_PRIMARY")
    public Boolean getPrimary() {
        return isPrimary;
    }

    public void setPrimary(Boolean primary) {
        isPrimary = primary;
    }

    @Basic
    @Column(name = "TEMPLATE_ENABLED")
    public Boolean getTemplateEnabled() {
        return templateEnabled;
    }

    public void setTemplateEnabled(Boolean templateEnabled) {
        this.templateEnabled = templateEnabled;
    }

    @Basic
    @Column(name = "ATTRIBUTE_SCOPE")
    public String getAttributeScope() {
        return attributeScope;
    }

    public void setAttributeScope(String attributeScope) {
        this.attributeScope = attributeScope;
    }

    @Basic
    @Column(name = "CONSTRAINT_EXP")
    public String getConstraintExp() {
        return constraintExp;
    }

    public void setConstraintExp(String constraintExp) {
        this.constraintExp = constraintExp;
    }

    @Basic
    @Column(name = "CONSTRAINT_TYPE")
    public String getConstraintType() {
        return constraintType;
    }

    public void setConstraintType(String constraintType) {
        this.constraintType = constraintType;
    }

    @Basic
    @Column(name = "DEFAULT_EXP")
    public String getDefaultExp() {
        return defaultExp;
    }

    public void setDefaultExp(String defaultExp) {
        this.defaultExp = defaultExp;
    }

    @Basic
    @Column(name = "CALCULATE_EXP")
    public String getCalculateExp() {
        return calculateExp;
    }

    public void setCalculateExp(String calculateExp) {
        this.calculateExp = calculateExp;
    }

    @Basic
    @Column(name = "OPERATOR_TYPE")
    public String getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(String operatorType) {
        this.operatorType = operatorType;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "menu_id")
    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    @Basic
    @Column(name = "IS_SERIALIZED")
    public Boolean getSerialized() {
        return isSerialized;
    }

    public void setSerialized(Boolean serialized) {
        isSerialized = serialized;
    }

    @Basic
    @Column(name = "SERIALIZATION_TYPE")
    public String getSerializationType() {
        return serializationType;
    }

    public void setSerializationType(String serializationType) {
        this.serializationType = serializationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaEntityAttribAudit that = (MetaEntityAttribAudit) o;
        return Objects.equals(attributeId, that.attributeId) &&
                Objects.equals(rev, that.rev) &&
                Objects.equals(revtype, that.revtype) &&
                Objects.equals(systemName, that.systemName) &&
                Objects.equals(metaEntId, that.metaEntId) &&
                Objects.equals(attributeName, that.attributeName) &&
                Objects.equals(tableColumn, that.tableColumn) &&
                Objects.equals(attributeType, that.attributeType) &&
                Objects.equals(description, that.description) &&
                Objects.equals(pickList, that.pickList) &&
                Objects.equals(counterName, that.counterName) &&
                Objects.equals(isMandatory, that.isMandatory) &&
                Objects.equals(isSearch, that.isSearch) &&
                Objects.equals(isAudit, that.isAudit) &&
                Objects.equals(isSummary, that.isSummary) &&
                Objects.equals(isInternalPrimary, that.isInternalPrimary) &&
                Objects.equals(isPrimary, that.isPrimary) &&
                Objects.equals(templateEnabled, that.templateEnabled) &&
                Objects.equals(attributeScope, that.attributeScope) &&
                Objects.equals(constraintExp, that.constraintExp) &&
                Objects.equals(constraintType, that.constraintType) &&
                Objects.equals(defaultExp, that.defaultExp) &&
                Objects.equals(calculateExp, that.calculateExp) &&
                Objects.equals(operatorType, that.operatorType) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy) &&
                Objects.equals(menuId, that.menuId) &&
                Objects.equals(isSerialized, that.isSerialized) &&
                Objects.equals(serializationType, that.serializationType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attributeId, rev, revtype, systemName, metaEntId, attributeName, tableColumn, attributeType, description, pickList, counterName, isMandatory, isSearch, isAudit, isSummary, isInternalPrimary, isPrimary, templateEnabled, attributeScope, constraintExp, constraintType, defaultExp, calculateExp, operatorType, createdOn, createdBy, updatedOn, updatedBy, menuId, isSerialized, serializationType);
    }
}
