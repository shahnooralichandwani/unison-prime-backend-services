package com.avanza.unison.entity;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "META_VIEW")
public class MetaView implements DBEntity {

    @Id
    @Column(name = "META_VIEW_ID")
    private String metaViewId;

    @Column(name = "META_ENT_ID")
    private String metaEntityId;

    private String viewNamePrm;
    private String systemName;
    private String viewNameSec;
    private String viewType;
    private String dimension;
    private String placeholderPosition;
    private Boolean isMain;
    private Boolean isDefault;
    private Boolean isReadOnly;
    private Boolean isPrimaryLink;
    private Integer displayOrder;
    private String factoryClass;
    private String selectCriteria;
    private String associatedSearch;
    private Integer pageSize;
    private Timestamp createdOn;
    private String aggregatedAttribId;
    private String createdBy;
    private String updatedBy;
    private Timestamp updatedOn;
    private String orderByClause;
    private String dataProcessorClass;
    private Boolean isConfirmationAction;
    private Boolean isCollapsableView;
    private Boolean isCollapsedState;
    private String iframeUrl;
    private Boolean stayOnSave;

    /*
     * @ManyToOne
     *
     * @JoinColumn(name = "META_ENT_ID", referencedColumnName = "META_ENT_ID")
     * private MetaEntity metaEntityByMetaEntId;
     */

    @OneToMany
    @JoinColumn(name = "META_VIEW_ID")
    private List<MetaViewParam> metaViewParams;

    @OneToMany
    @JoinColumn(name = "META_VIEW_ID")
    private List<MetaViewAttrib> metaViewAttribs;

    @ManyToMany
    @JoinTable(
            name = "META_VIEW_RELATIONSHIP",
            joinColumns = @JoinColumn(name = "PARENT_VIEW_ID"),
            inverseJoinColumns = @JoinColumn(name = "CHILD_VIEW_ID")
    )
    private List<MetaView> childViews;

    @OneToMany
    @JoinColumn(name = "META_VIEW_ID")
    @OrderBy("displayOrder")
    private List<MetaViewMenu> metaViewMenu;

    public String getMetaViewId() {
        return metaViewId;
    }

    public void setMetaViewId(String metaViewId) {
        this.metaViewId = metaViewId;
    }

    public String getMetaEntityId() {
        return metaEntityId;
    }

    public void setMetaEntityId(String metaEntityId) {
        this.metaEntityId = metaEntityId;
    }

    @Column(name = "VIEW_NAME_PRM")
    public String getViewNamePrm() {
        return viewNamePrm;
    }

    public void setViewNamePrm(String viewNamePrm) {
        this.viewNamePrm = viewNamePrm;
    }

    @Basic
    @Column(name = "SYSTEM_NAME")
    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Basic
    @Column(name = "VIEW_NAME_SEC")
    public String getViewNameSec() {
        return viewNameSec;
    }

    public void setViewNameSec(String viewNameSec) {
        this.viewNameSec = viewNameSec;
    }

    @Basic
    @Column(name = "VIEW_TYPE")
    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    @Basic
    @Column(name = "DIMENSION")
    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    @Basic
    @Column(name = "PLACEHOLDER_POSITION")
    public String getPlaceholderPosition() {
        return placeholderPosition;
    }

    public void setPlaceholderPosition(String placeholderPosition) {
        this.placeholderPosition = placeholderPosition;
    }

    @Basic
    @Column(name = "IS_MAIN")
    public Boolean getMain() {
        return isMain;
    }

    public void setMain(Boolean main) {
        isMain = main;
    }

    @Basic
    @Column(name = "IS_DEFAULT")
    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    @Basic
    @Column(name = "IS_READ_ONLY")
    public Boolean getReadOnly() {
        return isReadOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        isReadOnly = readOnly;
    }

    @Basic
    @Column(name = "IS_PRIMARY_LINK")
    public Boolean getPrimaryLink() {
        return isPrimaryLink;
    }

    public void setPrimaryLink(Boolean primaryLink) {
        isPrimaryLink = primaryLink;
    }

    @Basic
    @Column(name = "DISPLAY_ORDER")
    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Basic
    @Column(name = "FACTORY_CLASS")
    public String getFactoryClass() {
        return factoryClass;
    }

    public void setFactoryClass(String factoryClass) {
        this.factoryClass = factoryClass;
    }

    @Basic
    @Column(name = "SELECT_CRITERIA")
    public String getSelectCriteria() {
        return selectCriteria;
    }

    public void setSelectCriteria(String selectCriteria) {
        this.selectCriteria = selectCriteria;
    }

    @Basic
    @Column(name = "ASSOCIATED_SEARCH")
    public String getAssociatedSearch() {
        return associatedSearch;
    }

    public void setAssociatedSearch(String associatedSearch) {
        this.associatedSearch = associatedSearch;
    }

    @Basic
    @Column(name = "PAGE_SIZE")
    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "AGGREGATED_ATTRIB_ID")
    public String getAggregatedAttribId() {
        return aggregatedAttribId;
    }

    public void setAggregatedAttribId(String aggregatedAttribId) {
        this.aggregatedAttribId = aggregatedAttribId;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "ORDER_BY_CLAUSE")
    public String getOrderByClause() {
        return orderByClause;
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    @Basic
    @Column(name = "DATA_PROCESSOR_CLASS")
    public String getDataProcessorClass() {
        return dataProcessorClass;
    }

    public void setDataProcessorClass(String dataProcessorClass) {
        this.dataProcessorClass = dataProcessorClass;
    }

    @Basic
    @Column(name = "IS_CONFIRMATION_ACTION")
    public Boolean getConfirmationAction() {
        return isConfirmationAction;
    }

    public void setConfirmationAction(Boolean confirmationAction) {
        isConfirmationAction = confirmationAction;
    }

    @Basic
    @Column(name = "IS_COLLAPSABLE_VIEW")
    public Boolean getCollapsableView() {
        return isCollapsableView;
    }

    public void setCollapsableView(Boolean collapsableView) {
        isCollapsableView = collapsableView;
    }

    @Basic
    @Column(name = "IS_COLLAPSED_STATE")
    public Boolean getCollapsedState() {
        return isCollapsedState;
    }

    public void setCollapsedState(Boolean collapsedState) {
        isCollapsedState = collapsedState;
    }

    @Basic
    @Column(name = "IFRAME_URL")
    public String getIframeUrl() {
        return iframeUrl;
    }

    public void setIframeUrl(String iframeUrl) {
        this.iframeUrl = iframeUrl;
    }

    @Basic
    @Column(name = "stay_on_save")
    public Boolean getStayOnSave() {
        return stayOnSave;
    }

    public void setStayOnSave(Boolean stayOnSave) {
        this.stayOnSave = stayOnSave;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaView metaView = (MetaView) o;
        return Objects.equals(metaViewId, metaView.metaViewId) &&
                Objects.equals(viewNamePrm, metaView.viewNamePrm) &&
                Objects.equals(systemName, metaView.systemName) &&
                Objects.equals(viewNameSec, metaView.viewNameSec) &&
                Objects.equals(viewType, metaView.viewType) &&
                Objects.equals(dimension, metaView.dimension) &&
                Objects.equals(placeholderPosition, metaView.placeholderPosition) &&
                Objects.equals(isMain, metaView.isMain) &&
                Objects.equals(isDefault, metaView.isDefault) &&
                Objects.equals(isReadOnly, metaView.isReadOnly) &&
                Objects.equals(isPrimaryLink, metaView.isPrimaryLink) &&
                Objects.equals(displayOrder, metaView.displayOrder) &&
                Objects.equals(factoryClass, metaView.factoryClass) &&
                Objects.equals(selectCriteria, metaView.selectCriteria) &&
                Objects.equals(associatedSearch, metaView.associatedSearch) &&
                Objects.equals(pageSize, metaView.pageSize) &&
                Objects.equals(createdOn, metaView.createdOn) &&
                Objects.equals(aggregatedAttribId, metaView.aggregatedAttribId) &&
                Objects.equals(createdBy, metaView.createdBy) &&
                Objects.equals(updatedBy, metaView.updatedBy) &&
                Objects.equals(updatedOn, metaView.updatedOn) &&
                Objects.equals(orderByClause, metaView.orderByClause) &&
                Objects.equals(dataProcessorClass, metaView.dataProcessorClass) &&
                Objects.equals(isConfirmationAction, metaView.isConfirmationAction) &&
                Objects.equals(isCollapsableView, metaView.isCollapsableView) &&
                Objects.equals(isCollapsedState, metaView.isCollapsedState) &&
                Objects.equals(iframeUrl, metaView.iframeUrl) &&
                Objects.equals(stayOnSave, metaView.stayOnSave);
    }

    @Override
    public int hashCode() {
        return Objects.hash(metaViewId, viewNamePrm, systemName, viewNameSec, viewType, dimension, placeholderPosition, isMain, isDefault, isReadOnly, isPrimaryLink, displayOrder, factoryClass, selectCriteria, associatedSearch, pageSize, createdOn, aggregatedAttribId, createdBy, updatedBy, updatedOn, orderByClause, dataProcessorClass, isConfirmationAction, isCollapsableView, isCollapsedState, iframeUrl, stayOnSave);
    }


    /*
     * public MetaEntity getMetaEntityByMetaEntId() { return metaEntityByMetaEntId;
     * }
     *
     * public void setMetaEntityByMetaEntId(MetaEntity metaEntityByMetaEntId) {
     * this.metaEntityByMetaEntId = metaEntityByMetaEntId; }
     */

    public List<MetaViewParam> getMetaViewParams() {
        return metaViewParams;
    }

    public void setMetaViewParams(List<MetaViewParam> metaViewParams) {
        this.metaViewParams = metaViewParams;
    }


    public List<MetaViewAttrib> getMetaViewAttribs() {
        return metaViewAttribs;
    }

    public void setMetaViewAttribs(List<MetaViewAttrib> metaViewAttribs) {
        this.metaViewAttribs = metaViewAttribs;
    }

    public List<MetaView> getChildViews() {
        return childViews;
    }

    public void setChildViews(List<MetaView> childViews) {
        this.childViews = childViews;
    }

    public List<MetaViewMenu> getMetaViewMenu() {
        return metaViewMenu;
    }

    public void setMetaViewMenu(List<MetaViewMenu> metaViewMenu) {
        this.metaViewMenu = metaViewMenu;
    }
}
