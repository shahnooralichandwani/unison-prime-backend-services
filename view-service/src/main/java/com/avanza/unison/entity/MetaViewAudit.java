package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_VIEW_AUDIT")
@IdClass(MetaViewAuditPK.class)
public class MetaViewAudit {
    private String metaViewId;
    private Integer rev;
    private Byte revtype;
    private String viewNamePrm;
    private String systemName;
    private String viewNameSec;
    private String viewType;
    private String metaEntId;
    private String dimension;
    private String placeholderPosition;
    private Boolean isMain;
    private Boolean isDefault;
    private Boolean isReadOnly;
    private Boolean isPrimaryLink;
    private Integer displayOrder;
    private String factoryClass;
    private String selectCriteria;
    private String associatedSearch;
    private Integer pageSize;
    private Timestamp createdOn;
    private String aggregatedAttribId;
    private String createdBy;
    private String updatedBy;
    private Timestamp updatedOn;
    private String orderByClause;
    private String dataProcessorClass;
    private Boolean isConfirmationAction;
    private Boolean isCollapsableView;
    private Boolean isCollapsedState;
    private String iframeUrl;
    private Boolean stayOnSave;

    @Id
    @Column(name = "META_VIEW_ID")
    public String getMetaViewId() {
        return metaViewId;
    }

    public void setMetaViewId(String metaViewId) {
        this.metaViewId = metaViewId;
    }

    @Id
    @Column(name = "REV")
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Basic
    @Column(name = "REVTYPE")
    public Byte getRevtype() {
        return revtype;
    }

    public void setRevtype(Byte revtype) {
        this.revtype = revtype;
    }

    @Basic
    @Column(name = "VIEW_NAME_PRM")
    public String getViewNamePrm() {
        return viewNamePrm;
    }

    public void setViewNamePrm(String viewNamePrm) {
        this.viewNamePrm = viewNamePrm;
    }

    @Basic
    @Column(name = "SYSTEM_NAME")
    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Basic
    @Column(name = "VIEW_NAME_SEC")
    public String getViewNameSec() {
        return viewNameSec;
    }

    public void setViewNameSec(String viewNameSec) {
        this.viewNameSec = viewNameSec;
    }

    @Basic
    @Column(name = "VIEW_TYPE")
    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    @Basic
    @Column(name = "META_ENT_ID")
    public String getMetaEntId() {
        return metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

    @Basic
    @Column(name = "DIMENSION")
    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    @Basic
    @Column(name = "PLACEHOLDER_POSITION")
    public String getPlaceholderPosition() {
        return placeholderPosition;
    }

    public void setPlaceholderPosition(String placeholderPosition) {
        this.placeholderPosition = placeholderPosition;
    }

    @Basic
    @Column(name = "IS_MAIN")
    public Boolean getMain() {
        return isMain;
    }

    public void setMain(Boolean main) {
        isMain = main;
    }

    @Basic
    @Column(name = "IS_DEFAULT")
    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    @Basic
    @Column(name = "IS_READ_ONLY")
    public Boolean getReadOnly() {
        return isReadOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        isReadOnly = readOnly;
    }

    @Basic
    @Column(name = "IS_PRIMARY_LINK")
    public Boolean getPrimaryLink() {
        return isPrimaryLink;
    }

    public void setPrimaryLink(Boolean primaryLink) {
        isPrimaryLink = primaryLink;
    }

    @Basic
    @Column(name = "DISPLAY_ORDER")
    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Basic
    @Column(name = "FACTORY_CLASS")
    public String getFactoryClass() {
        return factoryClass;
    }

    public void setFactoryClass(String factoryClass) {
        this.factoryClass = factoryClass;
    }

    @Basic
    @Column(name = "SELECT_CRITERIA")
    public String getSelectCriteria() {
        return selectCriteria;
    }

    public void setSelectCriteria(String selectCriteria) {
        this.selectCriteria = selectCriteria;
    }

    @Basic
    @Column(name = "ASSOCIATED_SEARCH")
    public String getAssociatedSearch() {
        return associatedSearch;
    }

    public void setAssociatedSearch(String associatedSearch) {
        this.associatedSearch = associatedSearch;
    }

    @Basic
    @Column(name = "PAGE_SIZE")
    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "AGGREGATED_ATTRIB_ID")
    public String getAggregatedAttribId() {
        return aggregatedAttribId;
    }

    public void setAggregatedAttribId(String aggregatedAttribId) {
        this.aggregatedAttribId = aggregatedAttribId;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "ORDER_BY_CLAUSE")
    public String getOrderByClause() {
        return orderByClause;
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    @Basic
    @Column(name = "DATA_PROCESSOR_CLASS")
    public String getDataProcessorClass() {
        return dataProcessorClass;
    }

    public void setDataProcessorClass(String dataProcessorClass) {
        this.dataProcessorClass = dataProcessorClass;
    }

    @Basic
    @Column(name = "IS_CONFIRMATION_ACTION")
    public Boolean getConfirmationAction() {
        return isConfirmationAction;
    }

    public void setConfirmationAction(Boolean confirmationAction) {
        isConfirmationAction = confirmationAction;
    }

    @Basic
    @Column(name = "IS_COLLAPSABLE_VIEW")
    public Boolean getCollapsableView() {
        return isCollapsableView;
    }

    public void setCollapsableView(Boolean collapsableView) {
        isCollapsableView = collapsableView;
    }

    @Basic
    @Column(name = "IS_COLLAPSED_STATE")
    public Boolean getCollapsedState() {
        return isCollapsedState;
    }

    public void setCollapsedState(Boolean collapsedState) {
        isCollapsedState = collapsedState;
    }

    @Basic
    @Column(name = "IFRAME_URL")
    public String getIframeUrl() {
        return iframeUrl;
    }

    public void setIframeUrl(String iframeUrl) {
        this.iframeUrl = iframeUrl;
    }

    @Basic
    @Column(name = "stay_on_save")
    public Boolean getStayOnSave() {
        return stayOnSave;
    }

    public void setStayOnSave(Boolean stayOnSave) {
        this.stayOnSave = stayOnSave;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewAudit that = (MetaViewAudit) o;
        return Objects.equals(metaViewId, that.metaViewId) &&
                Objects.equals(rev, that.rev) &&
                Objects.equals(revtype, that.revtype) &&
                Objects.equals(viewNamePrm, that.viewNamePrm) &&
                Objects.equals(systemName, that.systemName) &&
                Objects.equals(viewNameSec, that.viewNameSec) &&
                Objects.equals(viewType, that.viewType) &&
                Objects.equals(metaEntId, that.metaEntId) &&
                Objects.equals(dimension, that.dimension) &&
                Objects.equals(placeholderPosition, that.placeholderPosition) &&
                Objects.equals(isMain, that.isMain) &&
                Objects.equals(isDefault, that.isDefault) &&
                Objects.equals(isReadOnly, that.isReadOnly) &&
                Objects.equals(isPrimaryLink, that.isPrimaryLink) &&
                Objects.equals(displayOrder, that.displayOrder) &&
                Objects.equals(factoryClass, that.factoryClass) &&
                Objects.equals(selectCriteria, that.selectCriteria) &&
                Objects.equals(associatedSearch, that.associatedSearch) &&
                Objects.equals(pageSize, that.pageSize) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(aggregatedAttribId, that.aggregatedAttribId) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedBy, that.updatedBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(orderByClause, that.orderByClause) &&
                Objects.equals(dataProcessorClass, that.dataProcessorClass) &&
                Objects.equals(isConfirmationAction, that.isConfirmationAction) &&
                Objects.equals(isCollapsableView, that.isCollapsableView) &&
                Objects.equals(isCollapsedState, that.isCollapsedState) &&
                Objects.equals(iframeUrl, that.iframeUrl) &&
                Objects.equals(stayOnSave, that.stayOnSave);
    }

    @Override
    public int hashCode() {
        return Objects.hash(metaViewId, rev, revtype, viewNamePrm, systemName, viewNameSec, viewType, metaEntId, dimension, placeholderPosition, isMain, isDefault, isReadOnly, isPrimaryLink, displayOrder, factoryClass, selectCriteria, associatedSearch, pageSize, createdOn, aggregatedAttribId, createdBy, updatedBy, updatedOn, orderByClause, dataProcessorClass, isConfirmationAction, isCollapsableView, isCollapsedState, iframeUrl, stayOnSave);
    }
}
