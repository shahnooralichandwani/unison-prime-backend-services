package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_VIEW_RELATIONSHIP_AUDIT")
@IdClass(MetaViewRelationshipAuditPK.class)
public class MetaViewRelationshipAudit {
    private String relationId;
    private Integer rev;
    private Byte revtype;
    private String parentViewId;
    private String childViewId;
    private Integer childOrderInParent;
    private String relationType;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;

    @Id
    @Column(name = "RELATION_ID")
    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    @Id
    @Column(name = "REV")
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Basic
    @Column(name = "REVTYPE")
    public Byte getRevtype() {
        return revtype;
    }

    public void setRevtype(Byte revtype) {
        this.revtype = revtype;
    }

    @Basic
    @Column(name = "PARENT_VIEW_ID")
    public String getParentViewId() {
        return parentViewId;
    }

    public void setParentViewId(String parentViewId) {
        this.parentViewId = parentViewId;
    }

    @Basic
    @Column(name = "CHILD_VIEW_ID")
    public String getChildViewId() {
        return childViewId;
    }

    public void setChildViewId(String childViewId) {
        this.childViewId = childViewId;
    }

    @Basic
    @Column(name = "CHILD_ORDER_IN_PARENT")
    public Integer getChildOrderInParent() {
        return childOrderInParent;
    }

    public void setChildOrderInParent(Integer childOrderInParent) {
        this.childOrderInParent = childOrderInParent;
    }

    @Basic
    @Column(name = "RELATION_TYPE")
    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewRelationshipAudit that = (MetaViewRelationshipAudit) o;
        return Objects.equals(relationId, that.relationId) &&
                Objects.equals(rev, that.rev) &&
                Objects.equals(revtype, that.revtype) &&
                Objects.equals(parentViewId, that.parentViewId) &&
                Objects.equals(childViewId, that.childViewId) &&
                Objects.equals(childOrderInParent, that.childOrderInParent) &&
                Objects.equals(relationType, that.relationType) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(relationId, rev, revtype, parentViewId, childViewId, childOrderInParent, relationType, createdOn, createdBy, updatedOn, updatedBy);
    }
}
