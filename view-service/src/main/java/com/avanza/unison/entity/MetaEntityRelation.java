package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_ENTITY_RELATION")
public class MetaEntityRelation {
    private String relationshipId;
    private String systemName;
    private String relationType;
    private String mainEntityKey;
    private String relMainEntityKey;
    private String relSubEntityKey;
    private String subEntityKey;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private Boolean alwaysExist;
    private MetaEntity metaEntityBySubEntityId;
    private MetaEntity metaEntityByMainEntityId;
    private MetaEntity metaEntityByRelMetaEntity;

    @Id
    @Column(name = "RELATIONSHIP_ID")
    public String getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(String relationshipId) {
        this.relationshipId = relationshipId;
    }

    @Basic
    @Column(name = "SYSTEM_NAME")
    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    @Basic
    @Column(name = "RELATION_TYPE")
    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    @Basic
    @Column(name = "MAIN_ENTITY_KEY")
    public String getMainEntityKey() {
        return mainEntityKey;
    }

    public void setMainEntityKey(String mainEntityKey) {
        this.mainEntityKey = mainEntityKey;
    }

    @Basic
    @Column(name = "REL_MAIN_ENTITY_KEY")
    public String getRelMainEntityKey() {
        return relMainEntityKey;
    }

    public void setRelMainEntityKey(String relMainEntityKey) {
        this.relMainEntityKey = relMainEntityKey;
    }

    @Basic
    @Column(name = "REL_SUB_ENTITY_KEY")
    public String getRelSubEntityKey() {
        return relSubEntityKey;
    }

    public void setRelSubEntityKey(String relSubEntityKey) {
        this.relSubEntityKey = relSubEntityKey;
    }

    @Basic
    @Column(name = "SUB_ENTITY_KEY")
    public String getSubEntityKey() {
        return subEntityKey;
    }

    public void setSubEntityKey(String subEntityKey) {
        this.subEntityKey = subEntityKey;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "ALWAYS_EXIST")
    public Boolean getAlwaysExist() {
        return alwaysExist;
    }

    public void setAlwaysExist(Boolean alwaysExist) {
        this.alwaysExist = alwaysExist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaEntityRelation that = (MetaEntityRelation) o;
        return Objects.equals(relationshipId, that.relationshipId) &&
                Objects.equals(systemName, that.systemName) &&
                Objects.equals(relationType, that.relationType) &&
                Objects.equals(mainEntityKey, that.mainEntityKey) &&
                Objects.equals(relMainEntityKey, that.relMainEntityKey) &&
                Objects.equals(relSubEntityKey, that.relSubEntityKey) &&
                Objects.equals(subEntityKey, that.subEntityKey) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy) &&
                Objects.equals(alwaysExist, that.alwaysExist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(relationshipId, systemName, relationType, mainEntityKey, relMainEntityKey, relSubEntityKey, subEntityKey, createdOn, createdBy, updatedOn, updatedBy, alwaysExist);
    }

    @ManyToOne
    @JoinColumn(name = "SUB_ENTITY_ID", referencedColumnName = "META_ENT_ID", nullable = false)
    public MetaEntity getMetaEntityBySubEntityId() {
        return metaEntityBySubEntityId;
    }

    public void setMetaEntityBySubEntityId(MetaEntity metaEntityBySubEntityId) {
        this.metaEntityBySubEntityId = metaEntityBySubEntityId;
    }

    @ManyToOne
    @JoinColumn(name = "MAIN_ENTITY_ID", referencedColumnName = "META_ENT_ID", nullable = false)
    public MetaEntity getMetaEntityByMainEntityId() {
        return metaEntityByMainEntityId;
    }

    public void setMetaEntityByMainEntityId(MetaEntity metaEntityByMainEntityId) {
        this.metaEntityByMainEntityId = metaEntityByMainEntityId;
    }

    @ManyToOne
    @JoinColumn(name = "REL_META_ENTITY", referencedColumnName = "META_ENT_ID")
    public MetaEntity getMetaEntityByRelMetaEntity() {
        return metaEntityByRelMetaEntity;
    }

    public void setMetaEntityByRelMetaEntity(MetaEntity metaEntityByRelMetaEntity) {
        this.metaEntityByRelMetaEntity = metaEntityByRelMetaEntity;
    }
}
