package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_VIEW_ATTRIB_PARAM")
@IdClass(MetaViewAttribParamPK.class)
public class MetaViewAttribParam {
    private String parameterName;
    private String viewAttribId;
    private String parameterValue;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private String selectCriteria;
    private String selectCriteria2;
    private String keyValue;
    private String filterClass;

    @Id
    @Column(name = "PARAMETER_NAME")
    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    @Id
    @Column(name = "VIEW_ATTRIB_ID")
    public String getViewAttribId() {
        return viewAttribId;
    }

    public void setViewAttribId(String viewAttribId) {
        this.viewAttribId = viewAttribId;
    }

    @Basic
    @Column(name = "PARAMETER_VALUE")
    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "SELECT_CRITERIA")
    public String getSelectCriteria() {
        return selectCriteria;
    }

    public void setSelectCriteria(String selectCriteria) {
        this.selectCriteria = selectCriteria;
    }

    @Basic
    @Column(name = "SELECT_CRITERIA2")
    public String getSelectCriteria2() {
        return selectCriteria2;
    }

    public void setSelectCriteria2(String selectCriteria2) {
        this.selectCriteria2 = selectCriteria2;
    }

    @Basic
    @Column(name = "KEY_VALUE")
    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    @Basic
    @Column(name = "FILTER_CLASS")
    public String getFilterClass() {
        return filterClass;
    }

    public void setFilterClass(String filterClass) {
        this.filterClass = filterClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewAttribParam that = (MetaViewAttribParam) o;
        return Objects.equals(parameterName, that.parameterName) &&
                Objects.equals(viewAttribId, that.viewAttribId) &&
                Objects.equals(parameterValue, that.parameterValue) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy) &&
                Objects.equals(selectCriteria, that.selectCriteria) &&
                Objects.equals(selectCriteria2, that.selectCriteria2) &&
                Objects.equals(keyValue, that.keyValue) &&
                Objects.equals(filterClass, that.filterClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parameterName, viewAttribId, parameterValue, createdOn, createdBy, updatedOn, updatedBy, selectCriteria, selectCriteria2, keyValue, filterClass);
    }
}
