package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_VIEW_RELATIONSHIP")
public class MetaViewRelationship {
    private String relationId;
    private Integer childOrderInParent;
    private String relationType;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private MetaView metaViewByParentViewId;
    private MetaView metaViewByChildViewId;

    @Id
    @Column(name = "RELATION_ID")
    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    @Basic
    @Column(name = "CHILD_ORDER_IN_PARENT")
    public Integer getChildOrderInParent() {
        return childOrderInParent;
    }

    public void setChildOrderInParent(Integer childOrderInParent) {
        this.childOrderInParent = childOrderInParent;
    }

    @Basic
    @Column(name = "RELATION_TYPE")
    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewRelationship that = (MetaViewRelationship) o;
        return Objects.equals(relationId, that.relationId) &&
                Objects.equals(childOrderInParent, that.childOrderInParent) &&
                Objects.equals(relationType, that.relationType) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(relationId, childOrderInParent, relationType, createdOn, createdBy, updatedOn, updatedBy);
    }

    @ManyToOne
    @JoinColumn(name = "PARENT_VIEW_ID", referencedColumnName = "META_VIEW_ID", nullable = false)
    public MetaView getMetaViewByParentViewId() {
        return metaViewByParentViewId;
    }

    public void setMetaViewByParentViewId(MetaView metaViewByParentViewId) {
        this.metaViewByParentViewId = metaViewByParentViewId;
    }

    @ManyToOne
    @JoinColumn(name = "CHILD_VIEW_ID", referencedColumnName = "META_VIEW_ID", nullable = false)
    public MetaView getMetaViewByChildViewId() {
        return metaViewByChildViewId;
    }

    public void setMetaViewByChildViewId(MetaView metaViewByChildViewId) {
        this.metaViewByChildViewId = metaViewByChildViewId;
    }
}
