package com.avanza.unison.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "META_VIEW_AUDIT_LOG")
public class MetaViewAuditLog {
    private String auditLogId;

    @Id
    @Column(name = "AUDIT_LOG_ID")
    public String getAuditLogId() {
        return auditLogId;
    }

    public void setAuditLogId(String auditLogId) {
        this.auditLogId = auditLogId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewAuditLog that = (MetaViewAuditLog) o;
        return Objects.equals(auditLogId, that.auditLogId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(auditLogId);
    }
}
