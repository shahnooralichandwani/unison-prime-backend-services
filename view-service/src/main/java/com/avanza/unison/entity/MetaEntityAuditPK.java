package com.avanza.unison.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class MetaEntityAuditPK implements Serializable {
    private String metaEntId;
    private Integer rev;

    @Column(name = "META_ENT_ID")
    @Id
    public String getMetaEntId() {
        return metaEntId;
    }

    public void setMetaEntId(String metaEntId) {
        this.metaEntId = metaEntId;
    }

    @Column(name = "REV")
    @Id
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaEntityAuditPK that = (MetaEntityAuditPK) o;
        return Objects.equals(metaEntId, that.metaEntId) &&
                Objects.equals(rev, that.rev);
    }

    @Override
    public int hashCode() {
        return Objects.hash(metaEntId, rev);
    }
}
