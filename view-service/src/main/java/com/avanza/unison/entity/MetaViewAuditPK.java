package com.avanza.unison.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class MetaViewAuditPK implements Serializable {
    private String metaViewId;
    private Integer rev;

    @Column(name = "META_VIEW_ID")
    @Id
    public String getMetaViewId() {
        return metaViewId;
    }

    public void setMetaViewId(String metaViewId) {
        this.metaViewId = metaViewId;
    }

    @Column(name = "REV")
    @Id
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewAuditPK that = (MetaViewAuditPK) o;
        return Objects.equals(metaViewId, that.metaViewId) &&
                Objects.equals(rev, that.rev);
    }

    @Override
    public int hashCode() {
        return Objects.hash(metaViewId, rev);
    }
}
