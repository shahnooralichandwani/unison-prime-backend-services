package com.avanza.unison.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class MetaViewRelationshipAuditPK implements Serializable {
    private String relationId;
    private Integer rev;

    @Column(name = "RELATION_ID")
    @Id
    public String getRelationId() {
        return relationId;
    }

    public void setRelationId(String relationId) {
        this.relationId = relationId;
    }

    @Column(name = "REV")
    @Id
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewRelationshipAuditPK that = (MetaViewRelationshipAuditPK) o;
        return Objects.equals(relationId, that.relationId) &&
                Objects.equals(rev, that.rev);
    }

    @Override
    public int hashCode() {
        return Objects.hash(relationId, rev);
    }
}
