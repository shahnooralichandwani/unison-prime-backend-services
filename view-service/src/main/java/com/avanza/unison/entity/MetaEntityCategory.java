package com.avanza.unison.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "META_ENTITY_CATEGORY")
public class MetaEntityCategory {
    private String entCategoryId;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp updatedOn;
    private String updatedBy;
    private String actionClass;
    private MetaEntity metaEntityByMetaEntId;

    @Id
    @Column(name = "ENT_CATEGORY_ID")
    public String getEntCategoryId() {
        return entCategoryId;
    }

    public void setEntCategoryId(String entCategoryId) {
        this.entCategoryId = entCategoryId;
    }

    @Basic
    @Column(name = "CREATED_ON")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "UPDATED_ON")
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Basic
    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "ACTION_CLASS")
    public String getActionClass() {
        return actionClass;
    }

    public void setActionClass(String actionClass) {
        this.actionClass = actionClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaEntityCategory that = (MetaEntityCategory) o;
        return Objects.equals(entCategoryId, that.entCategoryId) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedOn, that.updatedOn) &&
                Objects.equals(updatedBy, that.updatedBy) &&
                Objects.equals(actionClass, that.actionClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entCategoryId, createdOn, createdBy, updatedOn, updatedBy, actionClass);
    }

    @ManyToOne
    @JoinColumn(name = "META_ENT_ID", referencedColumnName = "META_ENT_ID", nullable = false)
    public MetaEntity getMetaEntityByMetaEntId() {
        return metaEntityByMetaEntId;
    }

    public void setMetaEntityByMetaEntId(MetaEntity metaEntityByMetaEntId) {
        this.metaEntityByMetaEntId = metaEntityByMetaEntId;
    }
}
