package com.avanza.unison.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class MetaEntityCategoryAuditPK implements Serializable {
    private String entCategoryId;
    private Integer rev;

    @Column(name = "ENT_CATEGORY_ID")
    @Id
    public String getEntCategoryId() {
        return entCategoryId;
    }

    public void setEntCategoryId(String entCategoryId) {
        this.entCategoryId = entCategoryId;
    }

    @Column(name = "REV")
    @Id
    public Integer getRev() {
        return rev;
    }

    public void setRev(Integer rev) {
        this.rev = rev;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaEntityCategoryAuditPK that = (MetaEntityCategoryAuditPK) o;
        return Objects.equals(entCategoryId, that.entCategoryId) &&
                Objects.equals(rev, that.rev);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entCategoryId, rev);
    }
}
