package com.avanza.unison.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class MetaViewParamPK implements Serializable {
    private String parameterName;
    private String metaViewId;

    @Column(name = "PARAMETER_NAME")
    @Id
    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    @Column(name = "META_VIEW_ID")
    @Id
    public String getMetaViewId() {
        return metaViewId;
    }

    public void setMetaViewId(String metaViewId) {
        this.metaViewId = metaViewId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaViewParamPK that = (MetaViewParamPK) o;
        return Objects.equals(parameterName, that.parameterName) &&
                Objects.equals(metaViewId, that.metaViewId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parameterName, metaViewId);
    }
}
