package com.avanza.coreservices.controller;

import com.avanza.core.data.ApplicationContext;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.meta.PickListManager;
import com.avanza.core.sdo.DataObject;
import com.avanza.coreservices.data.DataUtil;
import com.avanza.coreservices.service.CoreDataService;
import com.avanza.ui.FormatterFactory;
import com.avanza.ui.meta.MetaView;
import com.avanza.ui.meta.MetaViewAttribute;
import com.avanza.ui.meta.ViewCatalog;
import com.avanza.unison.data.KeyValueData;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/data")
@CrossOrigin
public class DataController {

    @Autowired
    private CoreDataService coreDataService;

    @Autowired
    DataUtil dataUtil;

    @RequestMapping("/{metaEntityId}/list/{metaViewId}")
    public Object getListViewData(@PathVariable String metaEntityId, @PathVariable String metaViewId) {
        Map<String, List<Map<String, Object>>> jsonMap = new HashMap<String, List<Map<String, Object>>>();
        try {
            MetaEntity metaEntity = MetaDataRegistry.getMetaEntity(metaEntityId);

            MetaView view = ViewCatalog.getMetaView(metaViewId);
            List<MetaViewAttribute> viewAttribs = view.getOrderedAttributes();

            List<Map<String, Object>> listHeaderMap = new ArrayList<>();
            Map<String, Object> headerMap = new HashMap<String, Object>();
            JSONObject obj = new JSONObject();
            JSONArray arr = new JSONArray();

            for (MetaViewAttribute vAttrib : viewAttribs) {
                headerMap.put("key", vAttrib.getMetaAttribSystemName());
                headerMap.put("value", vAttrib.getDisplayNamePimary());
                headerMap.put("type", (vAttrib.getMetaAttribute().getPickListId() == null || vAttrib.getMetaAttributeId().equals("0000001073")) ? vAttrib.getComponentType() : "ComboBox");
                headerMap.put("editable", Boolean.toString(vAttrib.isReadOnly()));
                headerMap.put("values", (vAttrib.getMetaAttribute().getPickListId() == null || vAttrib.getMetaAttributeId().equals("0000001073")) ? null : KeyValueData.getComboData(PickListManager.getPickListData(vAttrib.getMetaAttribute().getPickListId())));
                listHeaderMap.add(headerMap);
                headerMap = new HashMap<String, Object>();
            }

            List<DataObject> listResult = coreDataService.fetchEformList(metaEntityId, "ORDER BY" + view.getOrderByClause());
            List<Map<String, Object>> listBodyMap = new ArrayList<>();
            Map<String, Object> bodyMap = new HashMap<String, Object>();


            for (DataObject data : listResult) {
                listBodyMap.add(dataUtil.getViewAttribData(data, viewAttribs));
                bodyMap = new HashMap<String, Object>();
            }

            //Apply Formatting
            FormatterFactory.ApplyFormatters(viewAttribs, listBodyMap);


            jsonMap.put("attribute", listHeaderMap);
            jsonMap.put("data", listBodyMap);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ApplicationContext.getContext().release();
        }
        return jsonMap;
    }


    @RequestMapping(value = "/{metaEntityId}", method = RequestMethod.PUT)
    public Object updateEntity(@PathVariable String metaEntityId, @RequestBody Map<String, Object> jsondata) {

        try {
            MetaEntity entity = MetaDataRegistry.getMetaEntity(metaEntityId);

            if (entity == null)
                entity = MetaDataRegistry.getEntityBySystemName(metaEntityId);

            String primaryKeyAttrib = entity.getPrimaryKeyAttribute().getSystemName();
            Map<String, String> filterMap = new HashMap<>();
            filterMap.put(entity.getPrimaryKeyAttribute().getSystemName().toString(), jsondata.get(entity.getPrimaryKeyAttribute().getSystemName().toString()).toString());
            List<DataObject> listObj = coreDataService.fetchDocumentByTicket(metaEntityId, filterMap);
            DataObject obj = listObj.get(0);
            if (obj != null) {
                for (Map.Entry<String, Object> entry : jsondata.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    obj.setValue(key, value);
                }
            }
            obj.save();
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return jsondata;
    }

}
