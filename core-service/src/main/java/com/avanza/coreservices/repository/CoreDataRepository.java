package com.avanza.coreservices.repository;

import com.avanza.core.data.DataBroker;
import com.avanza.core.data.DataRepository;
import com.avanza.core.data.expression.Criterion;
import com.avanza.core.data.expression.Search;
import com.avanza.core.meta.MetaDataRegistry;
import com.avanza.core.meta.MetaEntity;
import com.avanza.core.sdo.DataObject;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class CoreDataRepository {

    /*
     *  It will return all data against
     *  specified entity
     *
     * */
    public List<DataObject> findAll(String metaEntId, String orderByClause) {

        if (metaEntId != null) {
            MetaEntity metaEntity = MetaDataRegistry.getMetaEntity(metaEntId);
            String systemName = metaEntity.getSystemName();
            DataBroker broker = DataRepository.getBroker(systemName);
            Search search = new Search();
            search.setOrderClause(orderByClause);
            search.addFrom(systemName);
            return broker.find(search);
        }
        return null;
    }

    /*
     *  It will return filtered data against
     *  specified entity
     *
     * */
    public List<DataObject> findOne(String metaEntId, Map<String, String> filterByKeyValue) {
        if (metaEntId != null) {
            MetaEntity metaEntity = MetaDataRegistry.getMetaEntity(metaEntId);
            String systemName = metaEntity.getSystemName();
            DataBroker broker = DataRepository.getBroker(systemName);
            Search search = new Search();

            for (Map.Entry<String, String> entry : filterByKeyValue.entrySet()) {
                search.addCriterion(Criterion.equal(entry.getKey(), entry.getValue()));
            }
            search.addFrom(systemName);
            return broker.find(search);
        }
        return null;
    }

}
