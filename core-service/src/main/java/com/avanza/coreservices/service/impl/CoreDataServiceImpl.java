package com.avanza.coreservices.service.impl;

import java.util.List;
import java.util.Map;

import com.avanza.coreservices.service.CoreDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avanza.core.sdo.DataObject;
import com.avanza.coreservices.repository.CoreDataRepository;
import com.avanza.coreservices.service.CoreDataService;

@Service
public class CoreDataServiceImpl implements CoreDataService {

    @Autowired
    CoreDataRepository coreDataRepository;

    @Override
    public List<DataObject> fetchEformList(String metaEntId, String orderBy) {
        return coreDataRepository.findAll(metaEntId, orderBy);
    }

    @Override
    public List<DataObject> fetchDocumentByTicket(String metaEntId, Map<String, String> filterByKeyValue) {
        return coreDataRepository.findOne(metaEntId, filterByKeyValue);
    }
}
