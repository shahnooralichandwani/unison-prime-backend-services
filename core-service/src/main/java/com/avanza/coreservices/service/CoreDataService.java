package com.avanza.coreservices.service;

import com.avanza.core.sdo.DataObject;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


public interface CoreDataService {

    List<DataObject> fetchEformList(String metaEntId, String orderBy);

    List<DataObject> fetchDocumentByTicket(String metaEntId, Map<String, String> filterByKeyValue);
}
