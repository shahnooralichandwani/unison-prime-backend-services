package com.avanza.unison.data;

import com.avanza.core.meta.PicklistData;

import java.util.ArrayList;
import java.util.List;

public class KeyValueData {

    private String key;

    private String value;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public KeyValueData(String key, String val) {
        this.setKey(key);
        this.setValue(val);
    }

    public static List<KeyValueData> getComboData(List<PicklistData> pData) {
        List<KeyValueData> keyValList = new ArrayList<>();
        if (pData != null && pData.size() > 0) {
            for (PicklistData data : pData) {
                keyValList.add(new KeyValueData(data.getValue(), data.getDisplayPrimary()));
            }
        }
        return keyValList;
    }
}
