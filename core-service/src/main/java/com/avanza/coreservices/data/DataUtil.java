package com.avanza.coreservices.data;

import com.avanza.core.constants.SessionKeyLookup;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.function.ExpressionClass;
import com.avanza.core.function.FunctionCatalogManager;
import com.avanza.core.function.expression.Expression;
import com.avanza.core.function.expression.ExpressionEvaluator;
import com.avanza.core.function.expression.ExpressionParser;
import com.avanza.core.meta.*;
import com.avanza.core.sdo.DataObject;
import com.avanza.core.sdo.DataObjectCollection;
import com.avanza.core.util.Convert;
import com.avanza.core.util.StringHelper;
import com.avanza.core.web.config.LocaleInfo;
import com.avanza.core.web.config.WebContext;
import com.avanza.ui.ControlType;
import com.avanza.ui.meta.MetaView;
import com.avanza.ui.meta.MetaViewAttribute;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class DataUtil {


    public static List<Map<String, Object>> ApplyTransformations(DataObjectCollection collection, List<Map<String, Object>> valueList,
                                                                 MetaView metaView) {
        int recordSize;
        /**metaView.getOrderedAttributes() replace metaView.getAttributes() to display ordered grid columns*/
        for (MetaViewAttribute viewAttributeObj : metaView.getOrderedAttributes()) {

            MetaViewAttribute viewAttribute = (MetaViewAttribute) viewAttributeObj;
            MetaEntity entity = MetaDataRegistry.getMetaEntity(viewAttribute.getDocumentId());

            MetaAttribute attrib = entity.getAttribute(viewAttribute.getMetaAttributeId());
            if (ControlType.fromString(viewAttributeObj.getComponentType()) == ControlType.Link) {
                String linkExp = viewAttributeObj.getLinkParams();
                if (linkExp != null && linkExp.length() > 0) {
                    for (Map obj : valueList) {
                        ExpressionParser expParser = new ExpressionParser(linkExp, String.class, FunctionCatalogManager.getInstance()
                                .getFunctionCatalog(ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction));
                        Expression retVal = expParser.parse();
                        ExpressionEvaluator eval = new ExpressionEvaluator(obj, StringHelper.EMPTY);
                        Object newVal = eval.evaluate(retVal);
                        obj.put(viewAttributeObj.getMetaAttribSystemName() + "-linkURLParam", newVal);
                    }
                }
            }
            //Naushad : ReadUnread Work
/*            if (ControlType.fromString(viewAttributeObj.getComponentType()) == ControlType.MarkLink) {
                String linkExp = viewAttributeObj.getLinkParams();
                if (linkExp != null && linkExp.length() > 0) {
                    for (Map obj : valueList) {
                        ExpressionParser expParser = new ExpressionParser(linkExp, String.class, FunctionCatalogManager.getInstance()
                                .getFunctionCatalog(ExpressionClass.ExpressionFunction, ExpressionClass.ContextFunction));
                        Expression retVal = expParser.parse();
                        ExpressionEvaluator eval = new ExpressionEvaluator(obj, StringHelper.EMPTY);
                        Object newVal = eval.evaluate(retVal);
                        obj.put(viewAttributeObj.getMetaAttribSystemName() + "-linkURLParam", newVal);
                    }
                }
            }*/

            // chk if attr is foriegn key
            if (viewAttribute.isDisplayForeignValue()) {

                // Now change their values to reflect the foreign key
                // values.
                int index = 0;
                Iterator<Map<String, Object>> iterDObj = valueList.iterator();
                while (iterDObj.hasNext()) {
                    Map<String, Object> valuesMap = iterDObj.next();
                    Object value = collection.get(index).getRelatedObjectAttribByForeignKey(viewAttribute.getMetaAttributeId(),
                            viewAttribute.getForeignDisplayAttributeId());
                    // replace all data objs values.
                    valuesMap.remove(viewAttribute.getMetaAttributeId());
                    valuesMap.put(viewAttribute.getMetaAttributeId(), value);
                    index++;
                }

            } else if (attrib.getType() == AttributeType.PickList) {
                List<PicklistData> data = PickListManager.getPickListData(attrib.getPickListId());
                int index = 0;
                Iterator<Map<String, Object>> iterDObj = valueList.iterator();
                while (iterDObj.hasNext()) {
                    Map<String, Object> valuesMap = iterDObj.next();
                    Object oldValue = valuesMap.get(viewAttribute.getMetaAttribute().getSystemName());
                    Object newValue = getDisplayValue(data, oldValue);
                    if (newValue == null) newValue = oldValue;
                    if (newValue != null) {
                        valuesMap.remove(viewAttribute.getMetaAttribute().getSystemName());
                        valuesMap.put(viewAttribute.getMetaAttribute().getSystemName(), newValue);
                    }
                    index++;
                }
            }
            for (Map valueObj : valueList) {
                Object value = valueObj.get(viewAttribute.getMetaAttribute().getSystemName());
                if (value == null || (value != null && value.toString().trim().length() <= 0)) {
                    valueObj.put(viewAttribute.getMetaAttribute().getSystemName(), "&nbsp;");
                }
            }
        }

        // GridControl.addSelectionField(valueList);
        recordSize = valueList.size();
        //    valueList = applyAggregation(valueList, metaView.getAttributes(), metaView.getAggregateAttribId(), metaView.getId());
        for (Map valueObj : valueList) {
            Object id = valueObj.get(DataObject.REF_ENTITY_ID);
            if (id != null && ((String) id).length() > 0) {
                MetaEntity ent = MetaDataRegistry.getMetaEntity((String) id);
                Object value = valueObj.get(ent.getPrimaryKeyAttribute().getSystemName());
                valueObj.put(ent.getPrimaryKeyAttribute().getSystemName() + "-noformat", value);
            }
        }
        return valueList;
    }

    private static boolean isCSV(Object value) {
        return Convert.toList(value.toString()).size() > 1 ? true : false;
    }

    public static Object getDisplayValue(List<PicklistData> dataList, Object value) {
        if (value != null) {
            if (isCSV(value)) {
                String retVal = "";
                List<String> vals = Convert.toList(value.toString());
                for (String val : vals) {
                    boolean istransformed = false;
                    for (PicklistData data : dataList) {
                        if (data.getValue() != null) {
                            if (data.getValue().equalsIgnoreCase(val)) {
                                istransformed = true;
                                WebContext webContext = ApplicationContext.getContext().get(WebContext.class.getName());
                                boolean isPrimaryLocale = ((LocaleInfo) webContext.getAttribute(SessionKeyLookup.CurrentLocale)).isPrimary();
                                if (isPrimaryLocale)
                                    retVal += data.getDisplayPrimary().trim() + ", ";
                                else
                                    retVal += data.getDisplaySecondary().trim() + ", ";
                            }
                        }
                    }
                    if (!istransformed) {
                        retVal += val + ", ";
                    }
                }
                if (retVal.length() > 0) {
                    return retVal.subSequence(0, retVal.length() - 2);
                }
            } else {
                for (PicklistData data : dataList) {
                    if (data.getValue() != null) {
                        if (data.getValue().equalsIgnoreCase(value.toString())) {
                            WebContext webContext = ApplicationContext.getContext().get(WebContext.class.getName());
                            boolean isPrimaryLocale = ((LocaleInfo) webContext.getAttribute(SessionKeyLookup.CurrentLocale)).isPrimary();
                            if (isPrimaryLocale)
                                return data.getDisplayPrimary();
                            else
                                return data.getDisplaySecondary();
                        }
                    }
                }
            }
        }
        return null;
    }


    public Map<String, Object> getViewAttribData(DataObject dObject, List<MetaViewAttribute> viewAttribs) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        try {
            if (dObject != null && (viewAttribs != null && viewAttribs.size() > 0)) {
                for (MetaViewAttribute vAttrib : viewAttribs) {
                    dataMap.put(vAttrib.getMetaAttribSystemName(), dObject.getAttribList().get(vAttrib.getMetaAttribSystemName()).toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataMap;
    }


}
