package com.avanza.coreservices;

import com.avanza.core.main.ApplicationLoader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AvanzaCoreInitializer {


    @Value("${avanza.core.properties.path}")
    private String filePath;

    @PostConstruct
    public void load() {
        ApplicationLoader.load(filePath);
    }

}
